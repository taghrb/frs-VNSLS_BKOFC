﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : Page
{
    DatabaseHelperClass dbHlpr;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["isLoggedIn"] != null && (bool)Session["isLoggedIn"] == true)
            {
                Response.Redirect("~/Admin/Home.aspx");
            }
        }

        if (Request.Form.AllKeys.Contains("txtUserName") && Request.Form.AllKeys.Contains("txtPassword") )
        {
            if (!Request.Form["txtUserName"].Trim().Equals("") && !Request.Form["txtPassword"].Trim().Equals(""))
            {
                //string strActiveDivision = ddlDivision.SelectedValue.ToString();
                string strEmail = Request.Form["txtUserName"].Trim();
                string strPassword = Request.Form["txtPassword"].Trim();

                LoginUser(strEmail, strPassword);
            }
        }
    }

    protected void LogIn(object sender, EventArgs e)
    {
        if (IsValid && IsPostBack)
        {
            //string strActiveDivision = ddlDivision.SelectedValue.ToString();
            string strEmail = txtUserName.Text;
            string strPassword = txtPassword.Text;

            LoginUser(strEmail, strPassword);
        }
    }

    protected void btn_Login_Click(object sender, EventArgs e)
    {
        if (!txtUserName.Text.Trim().Equals("") && !txtPassword.Text.Trim().Equals(""))
        {
            LogIn(sender, e);
        }
    }

    private void LoginUser(string strEmail, string strPassword)
    {
        //Session["ActiveCompany"] = strActiveDivision;
        dbHlpr = new DatabaseHelperClass();

        string qry = "SELECT * FROM STP_MSTR_USERS WHERE USR_ID = @username AND USR_PSWRD = @password; ";
        KeyValuePair<string, string>[] prms = new KeyValuePair<string, string>[2];
        prms[0] = new KeyValuePair<string, string>("username", strEmail);
        prms[1] = new KeyValuePair<string, string>("password", strPassword);

        DataTable dtUsr = dbHlpr.FetchParameterizedData(qry, CommandType.Text, prms);

        // Validate the user password
        if (dtUsr.Rows.Count > 0)
        {
            if (dtUsr.Rows[0]["USR_BOACS"] == DBNull.Value || dtUsr.Rows[0]["USR_BOACS"].ToString() == string.Empty || dtUsr.Rows[0]["USR_BOACS"].ToString() != "1")
            {
                lblMessageError.Text = "Invalid username or password.";
                lblMessageError.Visible = true;
                return;
            }

            Session["isLoggedIn"] = true;
            //  Session["ADC"] = strActiveDivision + "_"; // Active division code, will be used throughout the system

            Session["userObject"] = dtUsr;
            Session["UserId"] = dtUsr.Rows[0]["USR_ID"].ToString();
            Session["userMenuCode"] = dtUsr.Rows[0]["USR_MNCOD"].ToString();

            DateTime dtNow = DateTime.Now;
            Session["userLastLogin"] = dtNow.ToString("ddd, dd MMM, yyyy hh:mm:ss tt");

            DataTable dtLastLogin = dbHlpr.FetchData("SELECT LGN_TS FROM SYS_USR_LSLGN WHERE LGN_USRID = '" + dtUsr.Rows[0]["USR_ID"].ToString() + "' ");
            if (dtLastLogin.Rows.Count > 0)
            {
                try
                {
                    DateTime lstLoginTmStmp = Convert.ToDateTime(dtLastLogin.Rows[0]["LGN_TS"]);
                    Session["userLastLogin"] = lstLoginTmStmp.ToString("ddd, dd MMM, yyyy hh:mm:ss tt");
                }
                catch (Exception ex) { }
            }
            dbHlpr.ExecuteNonQuery("DELETE FROM SYS_USR_LSLGN WHERE LGN_USRID = '" + dtUsr.Rows[0]["USR_ID"].ToString() + "' ");
            dbHlpr.ExecuteNonQuery("INSERT INTO SYS_USR_LSLGN (LGN_USRID, LGN_TS) VALUES('" + dtUsr.Rows[0]["USR_ID"].ToString() + "', '" + dtNow + "') ");


            if (dtUsr.Rows[0]["USR_DTGOV"] == DBNull.Value || dtUsr.Rows[0]["USR_DTGOV"].ToString() == "")
            {
                Session["userEditMastersAccess"] = false;
            }
            else
            {
                Session["userEditMastersAccess"] = dtUsr.Rows[0]["USR_DTGOV"].ToString() == "1" ? true : false;
            }

            if (dtUsr.Rows[0]["USR_SYACS"].ToString().Equals("1"))
            {
                string allBranchCodes = "'" + dtUsr.Rows[0]["USR_PRMBR"].ToString();

                DataTable dtBranchCodes = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE");
                foreach (DataRow brnRw in dtBranchCodes.Rows)
                {
                    allBranchCodes += " " + brnRw["STM_CODE"];
                }
                allBranchCodes = allBranchCodes.Replace(" ", "', '");
                allBranchCodes = allBranchCodes + "'";

                Session["userBranchCode"] = allBranchCodes;
            }
            else
            {
                string allBranchCodes = "'" + dtUsr.Rows[0]["USR_PRMBR"].ToString();

                DataTable dtBranchCodes = dbHlpr.FetchData("SELECT * FROM STP_LNK_USRBR WHERE UBR_USRID = '" + dtUsr.Rows[0]["USR_ID"].ToString() + "' ");
                foreach (DataRow brnRw in dtBranchCodes.Rows)
                {
                    allBranchCodes += " " + brnRw["UBR_STRNO"];
                }
                allBranchCodes = allBranchCodes.Replace(" ", "', '");
                allBranchCodes = allBranchCodes + "'";

                Session["userBranchCode"] = allBranchCodes;
            }

            //DataTable dtMenuInfo = dbHlpr.FetchData("SELECT * FROM STP_LNK_MNACS JOIN STP_MNU_PAGES ON MPG_PGID = PAG_ID");
            DataTable dtMenuInfo = dbHlpr.FetchData("SELECT * FROM STP_LNK_MNACS JOIN STP_MNU_PAGES ON MNA_PGCOD = PAG_CODE");
            Session["userMenuDataTable"] = dtMenuInfo;

            Response.Redirect("~/Admin/Home.aspx");

            //var MPG_KEY_arr = dtMenuInfo.AsEnumerable().Select(r => r.Field<string>("MPG_KEY")).ToArray();
            //var MPG_PGID_arr = dtMenuInfo.AsEnumerable().Select(r => r.Field<string>("MPG_PGID")).ToArray();
            //var MNA_PGCOD_arr = dtMenuInfo.AsEnumerable().Select(r => r.Field<string>("MNA_PGCOD")).ToArray();
            //var MNA_PGNAM_arr = dtMenuInfo.AsEnumerable().Select(r => r.Field<string>("MNA_PGNAM")).ToArray();
            //var MNA_PGNAM_arr = dtMenuInfo.AsEnumerable().Select(r => r.Field<string>("MNA_PGNAM")).ToArray();
            //var MNA_MNCOD_arr = dtMenuInfo.AsEnumerable().Select(r => r.Field<string>("MNA_MNCOD")).ToArray();
            //var PAG_URL_arr = dtMenuInfo.AsEnumerable().Select(r => r.Field<string>("PAG_URL")).ToArray();
            //var PAG_PARNT_arr = dtMenuInfo.AsEnumerable().Select(r => r.Field<string>("PAG_PARNT")).ToArray();

            //Session["userMenuKey"] = string.Join(",", MPG_KEY_arr);
            //Session["userMenuPageId"] = string.Join(",", MPG_PGID_arr);
            //Session["userMenuPageCode"] = string.Join(",", MNA_PGCOD_arr);
            //Session["userMenuPageName"] = string.Join(",", MNA_PGNAM_arr);
            //Session["userMenuPageType"] = string.Join(",", MNA_PGNAM_arr);
            //Session["userMenuCode"] = string.Join(",", MNA_MNCOD_arr);
            //Session["userMenuPageUrl"] = string.Join(",", PAG_URL_arr);
            //Session["userMenuPageParent"] = string.Join(",", PAG_PARNT_arr);
        }
        else
        {
            lblMessageError.Text = "Invalid username or password.";
            lblMessageError.Visible = true;
        }
    }


}