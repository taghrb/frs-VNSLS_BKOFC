﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mobile/MobileSiteAdmin.master" AutoEventWireup="true" CodeFile="MobileOtpRequests.aspx.cs" Inherits="Mobile_MobileOtpRequests" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceholder" runat="Server">

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>

    <link href='<%# ResolveUrl("~/Content/footable/css/footable.bootstrap.min.css") %>' runat="server" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src='<%# ResolveUrl("~/Content/footable/js/footable.min.js") %>'></script>

    <script type="text/javascript">
        $(function () {
            $('#tblMobileOtpRequests').footable();
        });
    </script>
    <style type="text/css">
        .footable-detail-row {
            background: gray;
        }

            .footable-detail-row tr {
                background: gray !important;
                color: white;
            }

        td.footable-first-visible, td.footable-last-visible {
            font-size: 18px;
        }

        .footable-details th {
            vertical-align: middle !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">
    <h4 class="text-center" style="text-decoration: underline;">OTP Requests</h4>

    <!-- The modal -->
    <div class="modal fade" id="smallShoes" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modalLabelSmall">OTP for Request ID :
                        <dx:ASPxLabel Font-Size="Medium" CssClass="modal-title" runat="server" ID="modalRequestId" Text="-" />
                    </h4>
                </div>

                <div class="modal-body">
                    <h2>
                        <dx:ASPxLabel ForeColor="Red" Font-Size="X-Large" runat="server" ID="modalMessage" Text="" />
                    </h2>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <asp:SqlDataSource runat="server" ID="DxGridSqlDataSource1" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>

            <asp:Repeater ID="rptMobileOtpRequests" runat="server" DataSourceID="DxGridSqlDataSource1">
                <HeaderTemplate>
                    <table id="tblMobileOtpRequests" class="table table-condensed table-striped footable">
                        <thead>
                            <tr>
                                <th data-class="expand">Request ID</th>
                                <th scope="col">Van</th>
                                <th style="display: table-cell;" data-breakpoints="all" data-hide="phone">OTP</th>
                                <th style="display: table-cell;" data-breakpoints="all" data-hide="phone">Received Date</th>
                                <th style="display: table-cell;" data-breakpoints="all" data-hide="phone">Ticket #</th>
                                <th style="display: table-cell;" data-breakpoints="all" data-hide="phone">Ticket Date</th>
                                <th style="display: table-cell;" data-breakpoints="all" data-hide="phone">Customer</th>
                                <th style="display: table-cell;" data-breakpoints="all" data-hide="phone">Item</th>
                                <th style="display: table-cell;" data-breakpoints="all" data-hide="phone">UoM</th>
                                <th style="display: table-cell;" data-breakpoints="all" data-hide="phone">Quantity</th>
                                <th style="display: table-cell;" data-breakpoints="all" data-hide="phone">Old Price</th>
                                <th style="display: table-cell;" data-breakpoints="all" data-hide="phone">New Price</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("OTR_REQID") %></td>
                        <td><%# Eval("OTR_ROUTE").ToString() + " - " + Eval("OTR_USRID").ToString() %></td>
                        <td>
                            <dx:ASPxButton ID="btnShowOtp" runat="server"
                                Text="Generate OTP" CssClass="btn btn-success btn-block"
                                CommandArgument='<%#Eval("OTR_REQID")%>'
                                OnClick="btnShowOtp_Click" />
                        </td>
                        <td>
                            <%# Convert.ToDateTime(Eval("OTR_RCVDT")).ToString("dd-MMM-yyyy") + " " + Eval("OTR_RCVTM") %>
                        </td>
                        <td><%# Eval("OTR_TKTNO") %></td>
                        <td>
                            <%# Eval("OTR_TKTDT").ToString() %>
                        </td>
                        <td><%# Eval("OTR_CUSNO") + " - " + Eval("CUS_NAME").ToString() %></td>
                        <td><%# Eval("OTR_ITMNO") + " - " + Eval("PRO_DESC1").ToString() %></td>
                        <td><%# Eval("OTR_UOM") %></td>
                        <td><%# Eval("OTR_QTY") %></td>
                        <td><%# Eval("OTR_OLDPR") %></td>
                        <td><%# Eval("OTR_NEWPR") %></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>

        </div>
    </div>
</asp:Content>

