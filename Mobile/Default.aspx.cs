﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Mobile_Default : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["isLoggedIn"] != null && (bool)Session["isLoggedIn"] == true)
            {
                Response.Redirect("~/Mobile/MobileHome.aspx");
            }
        }
    }

    protected void LogIn(object sender, EventArgs e)
    {
        if (IsValid && IsPostBack)
        {
            //string strActiveDivision = ddlDivision.SelectedValue.ToString();
            string strEmail = txtUserName.Text;
            string strPassword = txtPassword.Text;

            //Session["ActiveCompany"] = strActiveDivision;
            dbHlpr = new DatabaseHelperClass();
            DataTable dtUsr = dbHlpr.FetchData("SELECT * FROM STP_MSTR_USERS WHERE USR_ID = '" + strEmail + "' AND USR_PSWRD = '" + strPassword + "' AND USR_MGACS = '1';");

            // Validate the user password
            if (dtUsr.Rows.Count > 0)
            {
                if (dtUsr.Rows[0]["USR_BOACS"] == DBNull.Value || dtUsr.Rows[0]["USR_BOACS"].ToString() == string.Empty || dtUsr.Rows[0]["USR_BOACS"].ToString() != "1")
                {
                    lblMessageError.Text = "Invalid username or password.";
                    lblMessageError.Visible = true;
                    return;
                }

                Session["isLoggedIn"] = true;
                //  Session["ADC"] = strActiveDivision + "_"; // Active division code, will be used throughout the system

                Session["userObject"] = dtUsr;
                Session["UserId"] = dtUsr.Rows[0]["USR_ID"].ToString();
                Session["userMenuCode"] = dtUsr.Rows[0]["USR_MNCOD"].ToString();

                if (dtUsr.Rows[0]["USR_DTGOV"] == DBNull.Value || dtUsr.Rows[0]["USR_DTGOV"].ToString() == "")
                {
                    Session["userEditMastersAccess"] = false;
                }
                else
                {
                    Session["userEditMastersAccess"] = dtUsr.Rows[0]["USR_DTGOV"].ToString() == "1" ? true : false;
                }

                if (dtUsr.Rows[0]["USR_SYACS"].ToString().Equals("1"))
                {
                    string allBranchCodes = "'" + dtUsr.Rows[0]["USR_PRMBR"].ToString();

                    DataTable dtBranchCodes = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE");
                    foreach (DataRow brnRw in dtBranchCodes.Rows)
                    {
                        allBranchCodes += " " + brnRw["STM_CODE"];
                    }
                    allBranchCodes = allBranchCodes.Replace(" ", "', '");
                    allBranchCodes = allBranchCodes + "'";

                    Session["userBranchCode"] = allBranchCodes;
                }
                else
                {
                    string allBranchCodes = "'" + dtUsr.Rows[0]["USR_PRMBR"].ToString();

                    DataTable dtBranchCodes = dbHlpr.FetchData("SELECT * FROM STP_LNK_USRBR WHERE UBR_USRID = '" + dtUsr.Rows[0]["USR_ID"].ToString() + "' ");
                    foreach (DataRow brnRw in dtBranchCodes.Rows)
                    {
                        allBranchCodes += " " + brnRw["UBR_STRNO"];
                    }
                    allBranchCodes = allBranchCodes.Replace(" ", "', '");
                    allBranchCodes = allBranchCodes + "'";

                    Session["userBranchCode"] = allBranchCodes;
                }

                DataTable dtMenuInfo = dbHlpr.FetchData("SELECT * FROM STP_LNK_MNACS JOIN STP_MNU_PAGES ON MNA_PGCOD = PAG_CODE");
                Session["userMenuDataTable"] = dtMenuInfo;

                Response.Redirect("~/Mobile/MobileHome.aspx");
            }
            else
            {
                lblMessageError.Text = "Invalid username or password.";
                lblMessageError.Visible = true;
            }
        }
    }

    protected void btn_Login_Click(object sender, EventArgs e)
    {
        if (!txtUserName.Text.Trim().Equals("") && !txtPassword.Text.Trim().Equals(""))
        {
            LogIn(sender, e);
        }
    }
}