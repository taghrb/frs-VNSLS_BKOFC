﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Mobile_MobileOtpRequests : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        string StrNos = Session["userBranchCode"].ToString();

        DateTime dtNow = DateTime.Now;
        string dateToday = dtNow.ToString("yyyy-MM-dd");
        string timeOneHourAgo = dtNow.AddHours(-1).ToString("HH:mm");

        string qry = "SELECT ";
        qry += " OTR_REQID, OTR_USRID, OTR_STRNG, OTR_CODE, OTR_RCVDT, ";
        qry += " OTR_RCVTM, OTR_ROUTE, OTR_TKTNO, OTR_TKTDT, ";
        qry += " OTR_CUSNO, OTR_ITMNO, OTR_UOM, OTR_QTY, ";
        qry += " OTR_OLDPR, OTR_NEWPR, ";
        qry += " CUS_NAME, PRO_DESC1 ";
        qry += " FROM SLS_OTP_RQUST ";
        qry += " LEFT JOIN STP_MSTR_SLREP ON OTR_ROUTE = SRP_CODE ";
        qry += " LEFT JOIN STP_MST_CSTMR ON OTR_CUSNO = CUS_NO ";
        qry += " LEFT JOIN INV_MSTR_PRODT ON OTR_ITMNO = PRO_CODE ";
        qry += " WHERE ";
        qry += " OTR_RCVDT = '" + dateToday + "' ";
        qry += " AND OTR_RCVTM > '" + timeOneHourAgo + "' ";
        qry += " AND SRP_STRNO IN (" + StrNos + ") ";
        qry += " ORDER BY OTR_REQID DESC ";

        DxGridSqlDataSource1.SelectCommand = qry;
    }

    protected void btnShowOtp_Click(object sender, EventArgs e)
    {
        ASPxButton btnClicked = sender as ASPxButton;
        int requestId;
        if (int.TryParse(btnClicked.CommandArgument, out requestId))
        {
            DateTime dtNow = DateTime.Now;
            string dateToday = dtNow.ToString("yyyy-MM-dd");
            string timeOneHourAgo = dtNow.AddHours(-1).ToString("HH:mm");

            string StrNos = Session["userBranchCode"].ToString();

            string qry = "SELECT ";
            qry += " OTR_CODE, OTR_STRNG, OTR_RCVDT, OTR_RCVTM FROM SLS_OTP_RQUST ";
            qry += " LEFT JOIN STP_MSTR_SLREP ON OTR_ROUTE = SRP_CODE ";
            qry += " WHERE ";
            qry += " OTR_RCVDT = '" + dateToday + "' ";
            qry += " AND OTR_RCVTM > '" + timeOneHourAgo + "' ";
            qry += " AND SRP_STRNO IN (" + StrNos + ") ";
            qry += " AND OTR_REQID = '" + requestId + "' ";
            qry += " ORDER BY OTR_REQID DESC ";

            DataTable dtOtpInfo = dbHlpr.FetchData(qry);

            if (dtOtpInfo.Rows.Count > 0)
            {
                string StringToHash = dtOtpInfo.Rows[0]["OTR_STRNG"].ToString();
                string OTP = dtOtpInfo.Rows[0]["OTR_CODE"].ToString();

                string genDate = dtNow.ToString("yyyy-MM-dd");
                string genTime = dtNow.ToString("HH:mm");
                string requestDate = DateTime.MinValue.ToString("yyyy-MM-dd");
                string requestTime = DateTime.MinValue.ToString("HH:mm");

                try
                {
                    requestDate = Convert.ToDateTime(dtOtpInfo.Rows[0]["OTR_RCVDT"]).ToString("yyyy-MM-dd");
                    requestTime = Convert.ToDateTime(dtOtpInfo.Rows[0]["OTR_RCVTM"]).ToString("HH:mm");
                }
                catch (Exception ex) { }

                string qryToRun = "INSERT INTO SLS_OTP_CODES ( "
                    + " OTP_USRID, OTP_STRNG, OTP_CODE, OTP_DATE, OTP_TIME, OTP_RCVDT, OTP_RCVTM "
                    + " ) VALUES ( "
                    + " '" + Session["UserId"] + "', '" + StringToHash + "', '" + OTP + "', '" + genDate + "', '" + genTime + "', '" + requestDate + "', '" + requestTime + "' "
                    + " ) ";
                dbHlpr.ExecuteNonQuery(qryToRun);

                modalRequestId.Text = requestId.ToString();
                modalMessage.Text = "OTP Code : " + OTP;
            }
            else
            {
                modalRequestId.Text = requestId.ToString();
                modalMessage.Text = "Request information was not found. Invalid ID or request is expired.";
            }

            string script = "$(function() { $('#smallShoes').modal('toggle'); });";
            ClientScript.RegisterStartupScript(this.GetType(), "ShowHideModal", script, true);
        }
    }
}