﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Mobile/MobileSiteAdmin.master" AutoEventWireup="true" CodeFile="MobileHome.aspx.cs" Inherits="Mobile_MobileHome" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <style type="text/css">
        .otp-label {
            display: block;
            width: 100%;
            margin-bottom: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">
    <h4 class="text-center" style="text-decoration: underline;">OTP Generator</h4>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Van #</label>
                <dx:ASPxComboBox ID="cmbxRouteID" ClientInstanceName="cmbxClientRouteID" DataSourceID="cmbxRouteSqlDataSource1" ValueField="SRP_CODE" TextField="SRP_CODE" runat="server" ValueType="System.String" TabIndex="1" CssClass="form-control" Width="100%">
                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="Text" ErrorTextPosition="Bottom" SetFocusOnError="true">
                        <ErrorFrameStyle Font-Size="Smaller" />
                        <RequiredField IsRequired="true" ErrorText="Route is Required." />
                    </ValidationSettings>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="cmbxRouteSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT SRP_CODE FROM STP_MSTR_SLREP ORDER BY SRP_CODE"></asp:SqlDataSource>
            </div>

            <div class="form-group">
                <label class="control-label">Ticket #</label>
                <dx:ASPxTextBox ID="txtTicketNo" runat="server" TabIndex="2" CssClass="form-control" Width="100%">
                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                        <ErrorFrameStyle Font-Size="Smaller" />
                        <RegularExpression ValidationExpression="\d+" ErrorText="Invalid Ticket Number" />
                        <RequiredField IsRequired="true" ErrorText="Ticket Number is Required" />
                    </ValidationSettings>
                    <MaskSettings Mask="<999999999>" />
                </dx:ASPxTextBox>
            </div>

            <div class="form-group">
                <label class="control-label">Ticket Date</label>
                <dx:ASPxDateEdit ID="dtTmTicketDate" runat="server" TabIndex="3" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy" CssClass="form-control" Width="100%">
                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                        <ErrorFrameStyle Font-Size="Smaller" />
                        <RequiredField IsRequired="true" ErrorText="Ticket Date is Required" />
                    </ValidationSettings>
                </dx:ASPxDateEdit>
            </div>

            <div class="form-group">
                <label class="control-label">Customer No.</label>
                <dx:ASPxGridLookup ID="lkUpCustNo" runat="server" TabIndex="4" TextFormatString="{0}" SelectionMode="Single" DataSourceID="CustNoSqlDataSource1" KeyFieldName="CUS_NO" CssClass="form-control" Width="100%">
                    <GridViewProperties>
                        <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                        <SettingsPager PageSize="10" />
                    </GridViewProperties>
                    <Columns>
                        <dx:GridViewDataColumn Caption="Cust #" FieldName="CUS_NO" Width="20%" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="CUS_NAME" Width="30%" />
                        <dx:GridViewDataColumn Caption="Address" FieldName="CUS_ADDR1" Visible="false" Width="30%" />
                        <dx:GridViewDataColumn Caption="Catg" FieldName="CUS_CATG" Width="10%" />
                        <dx:GridViewDataColumn Caption="S. Rep" FieldName="CUS_SLSRP" Width="10%" />
                    </Columns>
                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                        <ErrorFrameStyle Font-Size="Smaller" />
                        <RequiredField IsRequired="true" ErrorText="Cust number is Required" />
                    </ValidationSettings>
                </dx:ASPxGridLookup>
                <asp:SqlDataSource ID="CustNoSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT CUS_NO, CUS_NAME, CUS_ADDR1, CUS_CATG, CUS_SLSRP FROM STP_MST_CSTMR ORDER BY CUS_NO"></asp:SqlDataSource>
            </div>

            <div class="form-group">
                <label class="control-label">Item No.</label>
                <dx:ASPxGridLookup ID="lkUpItemNo" runat="server" TabIndex="5" TextFormatString="{0}" SelectionMode="Single" DataSourceID="ItemNoSqlDataSource1" KeyFieldName="PRO_CODE" CssClass="form-control" Width="100%">
                    <GridViewProperties>
                        <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                        <SettingsPager PageSize="10" />
                    </GridViewProperties>
                    <Columns>
                        <dx:GridViewDataColumn Caption="Item #" FieldName="PRO_CODE" Width="20%" />
                        <dx:GridViewDataColumn Caption="Desc" FieldName="PRO_DESC1" Width="30%" />
                        <dx:GridViewDataColumn Caption="Catg" FieldName="PRO_CATG" Visible="false" Width="30%" />
                        <dx:GridViewDataColumn Caption="Price" FieldName="PRO_PRICE" Width="10%" />
                        <dx:GridViewDataColumn Caption="Weight" FieldName="PRO_WT" Width="10%" />
                    </Columns>
                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                        <ErrorFrameStyle Font-Size="Smaller" />
                        <RequiredField IsRequired="true" ErrorText="Item number is Required" />
                    </ValidationSettings>
                </dx:ASPxGridLookup>
                <asp:SqlDataSource ID="ItemNoSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT PRO_CODE, PRO_DESC1, PRO_CATG, PRO_PRICE, PRO_WT FROM INV_MSTR_PRODT ORDER BY PRO_CODE"></asp:SqlDataSource>
            </div>

            <div class="form-group">
                <label class="control-label">UoM</label>
                <asp:SqlDataSource ID="CmBxUomSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT DISTINCT UOM_CODE FROM STP_MSTR_UOM"></asp:SqlDataSource>
                <dx:ASPxComboBox ID="cmbxUom" runat="server" DataSourceID="CmBxUomSqlDataSource1" TextField="UOM_CODE" ValueField="UOM_CODE" CssClass="form-control" Width="100%">
                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="Text" ErrorTextPosition="Bottom" SetFocusOnError="true">
                        <RequiredField IsRequired="true" />
                    </ValidationSettings>
                </dx:ASPxComboBox>
            </div>

            <div class="form-group">
                <label class="control-label">Quantity</label>
                <dx:ASPxTextBox ID="txtQty" runat="server" TabIndex="8" CssClass="form-control" Width="100%">
                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                        <ErrorFrameStyle Font-Size="Smaller" />
                        <RequiredField IsRequired="true" ErrorText="Quantity is Required" />
                    </ValidationSettings>
                    <MaskSettings Mask="<0..99999>.<00..999>" />
                </dx:ASPxTextBox>
            </div>

            <div class="form-group">
                <label class="control-label">Price Change Reason</label>
                <dx:ASPxGridLookup ID="lkUpPrChngRsnCd" runat="server" TabIndex="5" TextFormatString="{0}" SelectionMode="Single" DataSourceID="PrChngRsnCdSqlDataSource1" KeyFieldName="PRS_CODE" CssClass="form-control" Width="100%">
                    <GridViewProperties>
                        <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                        <SettingsPager PageSize="10" />
                    </GridViewProperties>
                    <Columns>
                        <dx:GridViewDataColumn Caption="Code" FieldName="PRS_CODE" Width="20%" />
                        <dx:GridViewDataColumn Caption="Desc" FieldName="PRS_TITLE" Width="80%" />
                    </Columns>
                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                        <ErrorFrameStyle Font-Size="Smaller" />
                        <RequiredField IsRequired="true" ErrorText="Price Change Reason is Required" />
                    </ValidationSettings>
                </dx:ASPxGridLookup>
                <asp:SqlDataSource ID="PrChngRsnCdSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT PRS_CODE, PRS_TITLE FROM STP_MST_PRSNS ORDER BY PRS_CODE"></asp:SqlDataSource>
            </div>

            <div class="form-group">
                <label class="control-label">Expiry Date</label>
                <dx:ASPxDateEdit ID="dtTmExpiryDate" runat="server" TabIndex="3" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy" CssClass="form-control" Width="100%">
                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                        <ErrorFrameStyle Font-Size="Smaller" />
                        <RequiredField IsRequired="true" ErrorText="Expiry date is Required" />
                    </ValidationSettings>
                </dx:ASPxDateEdit>
            </div>

            <div class="form-group">
                <label class="control-label">Old Price</label>
                <dx:ASPxTextBox ID="txtOldPrice" runat="server" TabIndex="8" CssClass="form-control" Width="100%">
                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                        <ErrorFrameStyle Font-Size="Smaller" />
                        <RequiredField IsRequired="true" ErrorText="Old Price is Required" />
                    </ValidationSettings>
                    <MaskSettings Mask="<0..99999>.<00..99>" />
                </dx:ASPxTextBox>
            </div>

            <div class="form-group">
                <label class="control-label">New Price</label>
                <dx:ASPxTextBox ID="txtNewPrice" runat="server" TabIndex="9" CssClass="form-control" Width="100%">
                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                        <ErrorFrameStyle Font-Size="Smaller" />
                        <RequiredField IsRequired="true" ErrorText="New Price is Required" />
                    </ValidationSettings>
                    <MaskSettings Mask="<0..99999>.<00..99>" />
                </dx:ASPxTextBox>
            </div>

            <div class="form-group">
                <dx:ASPxLabel ID="lblOtp" ClientVisible="false" ClientInstanceName="lblOtp" runat="server" CssClass="alert alert-success otp-label" Text=""></dx:ASPxLabel>

                <asp:Button ID="btnGenerateOtp" runat="server"
                    Text="Generate OTP" CssClass="btn btn-success btn-block"
                    CausesValidation="true"
                    OnClientClick="javascript:return ASPxClientEdit.ValidateGroup(null)"
                    OnClick="btnGenerateOtp_Click" />
            </div>
        </div>
    </div>
</asp:Content>

