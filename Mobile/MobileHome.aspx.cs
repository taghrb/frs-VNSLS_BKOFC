﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Mobile_MobileHome : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        dtTmTicketDate.Date = DateTime.Now;
    }

    protected void btnGenerateOtp_Click(object sender, EventArgs e)
    {
        DateTime tktDate = dtTmTicketDate.Date;
        DateTime expiryDate = dtTmExpiryDate.Date;
        string dateSelected = tktDate.ToString("yyyyMMdd");
        string expiryDateSelected = expiryDate.ToString("yyyyMMdd");
        string StringToHash =
                      cmbxRouteID.Value.ToString().Trim() + "|"
                    + txtTicketNo.Text.Trim() + "|"
                    + dateSelected.Trim() + "|"
                    + lkUpCustNo.Value.ToString().Trim() + "|"
                    + lkUpItemNo.Value.ToString().Trim() + "|"
                    + cmbxUom.Value.ToString().Trim() + "|"
                    + txtQty.Text.Trim().Replace(",", "") + "|"
                    + txtOldPrice.Text.Trim().Replace(",", "") + "|"
                    + txtNewPrice.Text.Trim().Replace(",", "") + "|"
                    + lkUpPrChngRsnCd.Value.ToString().Trim() + "|"
                    + expiryDateSelected.Trim();
        lblOtp.Text = "OTP Generated : " + dbHlpr.GetStableHash(StringToHash).ToString();
        lblOtp.ClientVisible = true;
    }
}