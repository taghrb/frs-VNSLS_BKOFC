﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Mobile_MobileSiteAdmin : System.Web.UI.MasterPage
{
    string userMenuCode = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["userMenuCode"] == null)
        {
            RedirectToLoginPage();
        }
        else
        {
            DataTable dtUsr = (DataTable)Session["userObject"];
            if (dtUsr.Rows[0]["USR_MGACS"] == DBNull.Value || dtUsr.Rows[0]["USR_MGACS"].ToString() == string.Empty || dtUsr.Rows[0]["USR_MGACS"].ToString() != "1")
            {
                RedirectToLoginPage();
            }
        }
    }

    private void RedirectToLoginPage()
    {
        if (Page.IsCallback)
        {
            ASPxWebControl.RedirectOnCallback("~/Mobile/");
        }
        else
        {
            Response.Redirect("~/Mobile/");
        }
    }
}
