﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class MasterPages_SiteAdmin : System.Web.UI.MasterPage
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
    protected string sideMenu;
    string userMenuCode = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["userMenuCode"] == null || Session["userMenuCode"].ToString() == "")
        {
            if (Page.IsCallback)
            {
                ASPxWebControl.RedirectOnCallback("~/");
            }
            else
            {
                Response.Redirect("~/");
            }
            return;
        }

        // Check for User Rights
        string CurrentPage = ForsanHelperMethods.CurrentPage();
        userMenuCode = (string)Session["userMenuCode"];

        bool isCommonPage = false;
        switch (CurrentPage.ToLower())
        {
            case "home.aspx":
                isCommonPage = true;
                break;
            case "home":
                isCommonPage = true;
                break;
            default:
                break;
        }

        if (!isCommonPage)
        {
            //DataTable dtMenuDetail = dbHlpr.FetchData("SELECT * FROM STP_LNK_MNACS JOIN STP_MNU_PAGES ON MPG_PGID = PAG_ID WHERE MNA_MNCOD = '" + userMenuCode + "' AND (PAG_URL IN ('" + CurrentPage + "', '" + CurrentPage + ".aspx'))");
            DataTable dtMenuDetail = dbHlpr.FetchData("SELECT * FROM STP_LNK_MNACS JOIN STP_MNU_PAGES ON MNA_PGCOD = PAG_CODE WHERE MNA_MNCOD = '" + userMenuCode + "' AND (PAG_URL IN ('" + CurrentPage + "', '" + CurrentPage + ".aspx'))");
            if (dtMenuDetail.Rows.Count <= 0)
            {
                if (Page.IsCallback)
                {
                    //ASPxWebControl.RedirectOnCallback("~/ErrorPages/AccessDenied.aspx");
                }
                else
                {
                    //Response.Redirect("~/ErrorPages/AccessDenied.aspx");
                }
            }
        }

        TopRepeater.DataBind();
    }

    protected void TopRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        // Sidemenu html
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView row = (DataRowView)e.Item.DataItem;
            string topId = row.Row["MNA_PGID"].ToString();

            //DataTable dtPgs = dbHlpr.FetchData("SELECT * FROM STP_MNU_PAGES WHERE PAG_PARNT = '" + grpId + "' ORDER BY PAG_SEQNO");
            //DataTable dtPgs = dbHlpr.FetchData("SELECT STP_LNK_MNACS.*, STP_MNU_PAGES.PAG_URL FROM STP_LNK_MNACS JOIN STP_MNU_PAGES ON MPG_PGID = PAG_ID WHERE PAG_PARNT = '" + grpId + "' AND MNA_MNCOD = '" + userMenuCode + "' ORDER BY PAG_SEQNO");

            // Below line commented specifically for allowing Orders Merged Page access, but skip from listing in menu
            //DataTable dtPgs = dbHlpr.FetchData("SELECT STP_LNK_MNACS.*, STP_MNU_PAGES.PAG_URL FROM STP_LNK_MNACS JOIN STP_MNU_PAGES ON MNA_PGCOD = PAG_CODE WHERE PAG_PARNT = '" + grpId + "' AND MNA_MNCOD = '" + userMenuCode + "' ORDER BY PAG_SEQNO");
            DataTable dtPgs = dbHlpr.FetchData("SELECT STP_LNK_MNACS.*, STP_MNU_PAGES.PAG_URL, STP_MNU_PAGES.PAG_ICON FROM STP_LNK_MNACS JOIN STP_MNU_PAGES ON MNA_PGCOD = PAG_CODE WHERE PAG_PARNT = '" + topId + "' AND MNA_MNCOD = '" + userMenuCode + "' AND MNA_PGNAM != 'Merged Orders' AND MNA_PGTYP IN ('G', 'P') ORDER BY PAG_SEQNO");

            Repeater rptr = (Repeater)e.Item.FindControl("GrpRepeater");
            rptr.DataSource = dtPgs;
            rptr.DataBind();

        }
    }

    protected void GrpRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        // Sidemenu html
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView row = (DataRowView)e.Item.DataItem;
            string grpId = row.Row["MNA_PGID"].ToString();

            //DataTable dtPgs = dbHlpr.FetchData("SELECT * FROM STP_MNU_PAGES WHERE PAG_PARNT = '" + grpId + "' ORDER BY PAG_SEQNO");
            //DataTable dtPgs = dbHlpr.FetchData("SELECT STP_LNK_MNACS.*, STP_MNU_PAGES.PAG_URL FROM STP_LNK_MNACS JOIN STP_MNU_PAGES ON MPG_PGID = PAG_ID WHERE PAG_PARNT = '" + grpId + "' AND MNA_MNCOD = '" + userMenuCode + "' ORDER BY PAG_SEQNO");

            // Below line commented specifically for allowing Orders Merged Page access, but skip from listing in menu
            //DataTable dtPgs = dbHlpr.FetchData("SELECT STP_LNK_MNACS.*, STP_MNU_PAGES.PAG_URL FROM STP_LNK_MNACS JOIN STP_MNU_PAGES ON MNA_PGCOD = PAG_CODE WHERE PAG_PARNT = '" + grpId + "' AND MNA_MNCOD = '" + userMenuCode + "' ORDER BY PAG_SEQNO");
            DataTable dtPgs = dbHlpr.FetchData("SELECT STP_LNK_MNACS.*, STP_MNU_PAGES.PAG_URL, STP_MNU_PAGES.PAG_ICON FROM STP_LNK_MNACS JOIN STP_MNU_PAGES ON MNA_PGCOD = PAG_CODE WHERE PAG_PARNT = '" + grpId + "' AND MNA_MNCOD = '" + userMenuCode + "' AND MNA_PGNAM != 'Merged Orders' ORDER BY PAG_SEQNO");

            Repeater rptr = (Repeater)e.Item.FindControl("PgRepeater");
            rptr.DataSource = dtPgs;
            rptr.DataBind();

        }
    }

    protected void TopRepeater_Init(object sender, EventArgs e)
    {
        if (Session["userMenuCode"] == null || Session["userMenuCode"].ToString() == "")
        {
            if (Page.IsCallback)
            {
                ASPxWebControl.RedirectOnCallback("~/");
            }
            else
            {
                Response.Redirect("~/");
            }
            return;
        }

        if (Session["userMenuCode"] != null)
        {
            userMenuCode = (string)Session["userMenuCode"];

            //DataTable dtSidebarMenuGrps = dbHlpr.FetchData("SELECT STP_LNK_MNACS.*, STP_MNU_PAGES.PAG_SEQNO, STP_MNU_PAGES.PAG_URL FROM STP_LNK_MNACS JOIN STP_MNU_PAGES ON MPG_PGID = PAG_ID WHERE MNA_MNCOD = '" + userMenuCode + "' AND MNA_PGTYP = 'GRP' ORDER BY PAG_SEQNO");
            DataTable dtSidebarMenuGrps = dbHlpr.FetchData("SELECT STP_LNK_MNACS.*, STP_MNU_PAGES.PAG_SEQNO, STP_MNU_PAGES.PAG_URL, STP_MNU_PAGES.PAG_ICON FROM STP_LNK_MNACS JOIN STP_MNU_PAGES ON MNA_PGCOD = PAG_CODE WHERE MNA_MNCOD = '" + userMenuCode + "' AND MNA_PGTYP = 'T' ORDER BY PAG_SEQNO");
            TopRepeater.DataSource = dtSidebarMenuGrps;
            TopRepeater.DataBind();
        }
    }


    // Lookup Code
    //protected void lookupBtn_Click(object sender, EventArgs e)
    //{
    //    lookupPopUp.Visible = true;
    //    gvLookUp.DataSource = dbHlpr.FetchData("SELECT * FROM [GLB_DIV_MASTR]");
    //    gvLookUp.DataBind();
    //    gvLookUp.KeyFieldName = Session["LKUP_keyFld"].ToString();

    //    //gvLookUp.KeyFieldName = "DIV_CODE";
    //}
    protected void gvLookUp_SelectionChanged(object sender, EventArgs e)
    {
        string fieldToGet = Session["LKUP_getFld"].ToString();
        string gridViewToFind = Session["LKUP_grdVwId"].ToString();
        string controlToSetText = Session["LKUP_setCntrlText"].ToString();


        ASPxGridView grdVw = (ASPxGridView)sender;
        List<object> values = grdVw.GetSelectedFieldValues(fieldToGet);
        string codeSelected = values[0].ToString();
        ASPxGridView gv = (ASPxGridView)ForsanHelperMethods.FindControlRecursive(Page, gridViewToFind);
        ASPxTextBox CatDivision = (ASPxTextBox)ForsanHelperMethods.FindControlRecursive(gv, controlToSetText);
        CatDivision.Text = codeSelected;

        //ASPxGridView grdVw = (ASPxGridView)sender;
        //List<object> values = grdVw.GetSelectedFieldValues("DIV_CODE");
        //string codeSelected = values[0].ToString();
        //ASPxTextBox CatDivision = (ASPxTextBox)gvCategoriesInquery.FindEditFormTemplateControl("CatEditDivision");
        //CatDivision.Text = codeSelected;
        //lookupPopUp.Visible = false;
    }
}
