﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;

public partial class MasterPages_AuthMasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Check for Logged in State
        if (Session["isLoggedIn"] == null || (bool)Session["isLoggedIn"] != true)
        {
            if (Page.IsCallback)
            {
                ASPxWebControl.RedirectOnCallback("~/");
            }
            else
            {
                Response.Redirect("~/");
            }

        }

        this.Page.Form.Enctype = "multipart/form-data";

        Page.Header.DataBind();
    }
}
