﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="testID1" CssClass="overlay" runat="server">
        <div class="PageDiv clearfix">
            <!-- header starts here -->
            <div class="wrapper clearfix">
                <!-- header sidebar here -->
                <!-- header Content here -->
                <div class="Content" style="text-align: center; margin: 0px auto; overflow: hidden">
                    <div class="mainContent">
                        <asp:Panel ID="Panel1" CssClass="overlay loginoverlay" runat="server">
                            <div>
                                <div class="FormLogin">
                                    <%--<div class="formHeaderDiv">
                                        <h3>Login</h3>
                                    </div>--%>
                                    <table>
                                        <tr>
                                            <td>
                                                <img src="img/forsan.jpg" width="180" height="300" />
                                            </td>
                                            <td>
                                                <fieldset>
                                                    <asp:Label ForeColor="Red" Text="" ID="lblMessageError" runat="server"></asp:Label>
                                                    <table>
                                                        <%--<tr>
                                                            <td>VanSales</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="DivReq">
                                                                    <asp:DropDownList ID="ddlDivision" runat="server">
                                                                        <asp:ListItem Value="GIT" Text="Grill IT" />
                                                                        <asp:ListItem Value="GRC" Text="Groceria" />
                                                                        <asp:ListItem Value="PIZ" Text="Pizzaiolo" />
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </td>
                                                        </tr>--%>
                                                        <tr>
                                                            <td>UserName</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="DivReq">
                                                                    <asp:TextBox ID="txtUserName" runat="server" Text="admin" Width="170px" TabIndex="1"></asp:TextBox>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Password</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <div class="DivReq">
                                                                    <asp:TextBox ID="txtPassword" runat="server" Width="170px" Text="admin123" TextMode="Password" TabIndex="3"></asp:TextBox>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <hr style="color: skyblue" />
                                                    <div class="LoginBtnWrapper">
                                                        <asp:Button ID="btn_Login" runat="server" Text="Login" OnClick="btn_Login_Click" class="btnPopup btn btn-success"></asp:Button>
                                                    </div>
                                                </fieldset>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
            <!-- header footer here -->
        </div>
    </asp:Panel>
    <p>
        <a href="mailto:zia@forsan.com.sa">Contact Admin</a> if you don't have an account.
    </p>
</asp:Content>
