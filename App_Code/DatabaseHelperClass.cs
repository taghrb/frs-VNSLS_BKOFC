﻿using DevExpress.Export;
using DevExpress.Web;
using DevExpress.XtraPrinting;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for DatabaseHelperClass
/// </summary>
public class DatabaseHelperClass
{
    private SqlConnection SQL_CONNECTION;

    public DatabaseHelperClass()
    {
        string connStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        SQL_CONNECTION = new SqlConnection(connStr);
    }

    public object ExecuteScalarQuery(string qry)
    {
        try
        {
            SqlCommand cmd = new SqlCommand(qry, SQL_CONNECTION);

            if (SQL_CONNECTION.State != ConnectionState.Open)
            {
                SQL_CONNECTION.Open();
            }

            return cmd.ExecuteScalar();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            SQL_CONNECTION.Close();
        }
    }

    public int ExecuteNonQuery(string qry)
    {
        try
        {
            SqlCommand cmd = new SqlCommand(qry, SQL_CONNECTION);

            if (SQL_CONNECTION.State != ConnectionState.Open)
            {
                SQL_CONNECTION.Open();
            }

            return cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            SQL_CONNECTION.Close();
        }
    }

    public DataTable FetchParameterizedData(string qry, CommandType cmdType, params KeyValuePair<string, string>[] prms)
    {
        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
        SqlCommand sqlCommand = new SqlCommand(qry);
        sqlCommand.Connection = SQL_CONNECTION;

        try
        {
            if (SQL_CONNECTION.State != ConnectionState.Open)
            {
                SQL_CONNECTION.Open();
            }

            sqlCommand.CommandType = cmdType;
            foreach (KeyValuePair<string, string> param in prms)
            {
                sqlCommand.Parameters.AddWithValue("@" + param.Key, param.Value);
            }

            sqlDataAdapter.SelectCommand = sqlCommand;
            DataTable sqlDataTable = new DataTable();
            sqlDataAdapter.Fill(sqlDataTable);

            return sqlDataTable;
        }
        catch (Exception err)
        {
            throw err;
        }
        finally
        {
            SQL_CONNECTION.Close();
        }
    }

    public void ExecuteParameterizedQuery(string qry, params KeyValuePair<string, string>[] prms)
    {
        SqlCommand sqlCommand = new SqlCommand(qry);

        try
        {
            if (SQL_CONNECTION.State != ConnectionState.Open)
            {
                SQL_CONNECTION.Open();
            }

            sqlCommand.Connection = SQL_CONNECTION;
            sqlCommand.CommandType = CommandType.StoredProcedure;

            foreach (KeyValuePair<string, string> param in prms)
            {
                sqlCommand.Parameters.AddWithValue("@" + param.Key, param.Value);
            }

            sqlCommand.ExecuteNonQuery();
        }
        catch (Exception err)
        {
            throw err;
        }
        finally
        {
            SQL_CONNECTION.Close();
        }
    }

    public DataTable FetchData(string qry)
    {
        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(qry, SQL_CONNECTION);

        try
        {
            SQL_CONNECTION.Open();

            DataTable sqlDataTable = new DataTable();
            sqlDataAdapter.Fill(sqlDataTable);

            return sqlDataTable;
        }
        catch (Exception err)
        {
            throw err;
        }
        finally
        {
            SQL_CONNECTION.Close();
        }
    }

    public String GetRptSeqNo()
    {
        int rptno = 0;
        int rpt_len = 0;
        string NewRPTNo = string.Empty;
        try
        {
            String NewQuery = "SELECT RPTNO FROM SYS_RPT_SEQNO ";
            DataTable dt = new DataTable();
            SqlDataAdapter DAdp = new SqlDataAdapter(NewQuery, SQL_CONNECTION);

            if (SQL_CONNECTION.State != ConnectionState.Open)
            {
                SQL_CONNECTION.Open();
            }

            DAdp.Fill(dt);
            if (dt.Rows[0][0] != DBNull.Value)
            {
                rptno = Convert.ToInt32(dt.Rows[0][0]);
            }
            else
            {
                NewRPTNo = "00001";
            }
            NewRPTNo = Convert.ToString(rptno + 1);
            rpt_len = NewRPTNo.Length;

            if (rpt_len == 1)
            { NewRPTNo = "0000" + NewRPTNo; }
            else if (rpt_len == 2)
            { NewRPTNo = "000" + NewRPTNo; }
            else if (rpt_len == 3)
            { NewRPTNo = "00" + NewRPTNo; }
            else if (rpt_len == 4)
            { NewRPTNo = "0" + NewRPTNo; }


            string UpdateRPTNo = "UPDATE SYS_RPT_SEQNO SET RPTNO ='" + NewRPTNo + "'";
            ExecuteNonQuery(UpdateRPTNo);

            return NewRPTNo;
        }

        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            SQL_CONNECTION.Close();
        }
    }

    /*** Uncommon Functions, to handle DB Actions ***/
    public void CreateDownloadRecord(DataTable DTBL, string VanNo, string TableName, string Operation)
    {
        DataTable dtVans;
        if (VanNo == "ALL")
        {
            dtVans = this.FetchData("SELECT SRP_CODE FROM STP_MSTR_SLREP ORDER BY SRP_CODE ");
        }
        else
        {
            dtVans = this.FetchData("SELECT '" + VanNo + "' AS SRP_CODE ");
        }

        foreach (DataRow rwVan in dtVans.Rows)
        {
            for (int i = 0; i < DTBL.Rows.Count; i++)
            {
                string qry = "INSERT INTO DS_DLD_DTL (DSD_VAN, DSD_TBLNM, DSD_OPRTN, DSD_SYNCD ";
                for (int j = 0; j < DTBL.Columns.Count; j++)
                {
                    qry += ", " + DTBL.Columns[j].ColumnName;
                }
                qry += " ) VALUES ( ";
                qry += " '" + rwVan["SRP_CODE"].ToString() + "', '" + TableName + "', '" + Operation + "', 'N' ";
                for (int j = 0; j < DTBL.Columns.Count; j++)
                {
                    qry += ", '" + DTBL.Rows[i][DTBL.Columns[j].ColumnName].ToString() + "' ";
                }
                qry += " ) ";

                this.ExecuteNonQuery(qry);
            }
        }
    }

    public static string ConvertToArabic(string input)
    {
        System.Text.UTF8Encoding utf8Encoder = new UTF8Encoding();
        System.Text.Decoder utf8Decoder = utf8Encoder.GetDecoder();
        System.Text.StringBuilder convertedChars = new System.Text.StringBuilder();
        char[] convertedChar = new char[1];
        byte[] bytes = new byte[] { 217, 160 };
        char[] inputCharArray = input.ToCharArray();
        foreach (char c in inputCharArray)
        {
            if (char.IsDigit(c))
            {
                bytes[1] = Convert.ToByte(160 + char.GetNumericValue(c));
                utf8Decoder.GetChars(bytes, 0, 2, convertedChar, 0);
                convertedChars.Append(convertedChar[0]);
            }
            else
            {
                if (c == 44)
                {
                    convertedChars.Append(Convert.ToChar(184));
                }
                else if (c == 46)
                {
                    convertedChars.Append(Convert.ToChar(44));
                }
                else
                {
                    convertedChars.Append(c);
                }
            }
        }
        string strr = convertedChars.ToString();
        return convertedChars.ToString();
    }

    public void ExportToFormattedXls1997(System.Web.UI.Page PAGE_INSTANCE, ASPxGridView GV)
    {
        ASPxGridViewExporter exporter = new ASPxGridViewExporter();
        exporter.GridViewID = GV.ID;
        exporter.ExportedRowType = GridViewExportedRowType.All;
        exporter.DataBind();
        exporter.RenderBrick += gridExport_RenderBrick;

        PAGE_INSTANCE.Controls.Add(exporter);

        DevExpress.XtraPrinting.XlsExportOptionsEx Options = new DevExpress.XtraPrinting.XlsExportOptionsEx();
        Options.ExportMode = DevExpress.XtraPrinting.XlsExportMode.SingleFile;
        Options.ExportType = ExportType.WYSIWYG;
        Options.TextExportMode = TextExportMode.Value;

        exporter.WriteXlsToResponse(Options);
    }

    public void ExportToFormattedXlsx2007(System.Web.UI.Page PAGE_INSTANCE, ASPxGridView GV)
    {
        ASPxGridViewExporter exporter = new ASPxGridViewExporter();
        exporter.GridViewID = GV.ID;
        exporter.ExportedRowType = GridViewExportedRowType.All;
        exporter.DataBind();
        exporter.RenderBrick += gridExport_RenderBrick;

        PAGE_INSTANCE.Controls.Add(exporter);

        DevExpress.XtraPrinting.XlsxExportOptionsEx Options = new DevExpress.XtraPrinting.XlsxExportOptionsEx();
        Options.ExportMode = DevExpress.XtraPrinting.XlsxExportMode.SingleFile;
        Options.ExportType = ExportType.WYSIWYG;
        Options.TextExportMode = TextExportMode.Value;

        exporter.WriteXlsxToResponse(Options);
    }

    protected void gridExport_RenderBrick(object sender, ASPxGridViewExportRenderingEventArgs e)
    {
        e.BrickStyle.BorderColor = Color.Black;
        e.BrickStyle.BorderWidth = 1;
        if (e.RowType == GridViewRowType.Header)
        {
            e.BrickStyle.ForeColor = Color.White;
            e.BrickStyle.Font = new Font("Segoe UI", 12, FontStyle.Bold);
            e.BrickStyle.BackColor = Color.FromArgb(198, 89, 17);
        }
        else if (e.VisibleIndex % 2 == 0)
        {
            e.BrickStyle.Font = new Font("Segoe UI", 11, FontStyle.Regular);
            e.BrickStyle.BackColor = Color.FromArgb(221, 235, 247);
        }
        else if (e.VisibleIndex % 2 != 0)
        {
            e.BrickStyle.Font = new Font("Segoe UI", 11, FontStyle.Regular);
        }
        if (e.Column != null)
        {
            e.Column.ExportWidth = 150;
        }
        e.BrickStyle.StringFormat = new BrickStringFormat(new StringFormat(StringFormatFlags.NoWrap));
    }

    public long GetStableHash(string s)
    {
        string SALTSECRET_KEY = "SALTSECRET_KEY";
        s = s + SALTSECRET_KEY;

        long hash = 0;
        // if you care this can be done much faster with unsafe 
        // using fixed char* reinterpreted as a byte*
        foreach (byte b in System.Text.Encoding.Unicode.GetBytes(s))
        {
            hash += b;
            hash += (hash << 10);
            hash ^= (hash >> 6);
        }
        // final avalanche
        hash += (hash << 3);
        hash ^= (hash >> 11);
        hash += (hash << 15);
        // helpfully we only want positive integer < MUST_BE_LESS_THAN
        // so simple truncate cast is ok if not perfect
        long tortn = (long)(hash % 1000000);
        return tortn < 0 ? tortn * -1 : tortn;
    }
}