﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FMR001.Startup))]
namespace FMR001
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
