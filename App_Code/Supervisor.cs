﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for Supervisor
/// </summary>
[WebService(Namespace = "http://forsan.vs/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class Supervisor : System.Web.Services.WebService
{

    DatabaseHelperClass dbHelper = new DatabaseHelperClass();

    public Supervisor()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    private void PrintResponseWithAll(string jsonString)
    {
        Context.Response.Clear();
        Context.Response.StatusCode = 201;
        Context.Response.ContentType = "application/json";
        //Context.Response.AddHeader("content-length", jsonString.Length.ToString());
        Context.Response.AddHeader("connection", "close");
        //Context.Response.Flush();
        Context.Response.Write(jsonString);
        //HttpContext.Current.ApplicationInstance.CompleteRequest();
    }

    [WebMethod]
    public void Login(string UserId, string Password)
    {
        string qryToRun = "SELECT * FROM STP_MSTR_USERS WHERE USR_ID = '" + UserId + "' AND USR_PSWRD = '" + Password + "' AND USR_MGACS = '1';";
        DataTable userInfo = dbHelper.FetchData(qryToRun);

        MyResponseObject response = new MyResponseObject();
        response.success = userInfo.Rows.Count > 0 ? true : false;
        response.data = userInfo;

        PrintResponseWithAll(JsonConvert.SerializeObject(response));
    }

    [WebMethod]
    public void RecordOtp(string UserId, string StringToHash, string OTP, string OtpDateTime)
    {
        DateTime dtNow = DateTime.Now;
        string requestDate = dtNow.ToString("yyyy-MM-dd");
        string requestTime = dtNow.ToString("HH:mm");
        string genDate = dtNow.ToString("yyyy-MM-dd");
        string genTime = dtNow.ToString("HH:mm");

        try
        {
            DateTime dtGeneratedAt = Convert.ToDateTime(OtpDateTime);
            genDate = dtGeneratedAt.ToString("yyyy-MM-dd");
            genTime = dtGeneratedAt.ToString("HH:mm");
        }
        catch (Exception ex) { }

        string qryToRun = "INSERT INTO SLS_OTP_CODES ( "
            + " OTP_USRID, OTP_STRNG, OTP_CODE, OTP_DATE, OTP_TIME, OTP_RCVDT, OTP_RCVTM "
            + " ) VALUES ( "
            + " '" + UserId + "', '" + StringToHash + "', '" + OTP + "', '" + genDate + "', '" + genTime + "', '" + requestDate + "', '" + requestTime + "' "
            + " ) ";
        dbHelper.ExecuteNonQuery(qryToRun);

        MyResponseObject response = new MyResponseObject();
        response.success = true;
        response.message = "OTP Recorded Successfully.";

        PrintResponseWithAll(JsonConvert.SerializeObject(response));
    }

    [WebMethod]
    public void DownloadItems()
    {
        string qryToRun = " SELECT "
            + " LTRIM(RTRIM(PRO_CODE)) AS PRO_CODE, "
            + " LTRIM(RTRIM(PRO_DESC1)) AS PRO_DESC1, "
            + " LTRIM(RTRIM(PRO_DESC2)) AS PRO_DESC2, "
            + " PRO_PRICE "
            + " FROM INV_MSTR_PRODT ";
        DataTable dtItems = dbHelper.FetchData(qryToRun);

        MyResponseObject response = new MyResponseObject();
        response.success = true;
        response.data = dtItems;

        PrintResponseWithAll(JsonConvert.SerializeObject(response));
    }

    [WebMethod]
    public void DownloadCustomers()
    {
        string qryToRun = " SELECT "
            + " CUS_NO, CUS_NAME, CUS_ADDR1, CUS_ADDR2, "
            + " CUS_CITY, CUS_CNTC1, CUS_CNTC2, CUS_PHON1, "
            + " CUS_PHON2, CUS_EMAIL, CUS_SLSRP, CUS_TYPE, "
            + " CUS_CATG, CUS_VATNO, CUS_TXABL, CUS_GRPCD, "
            + " CUS_NAMAR, CUS_GEOLA, CUS_GEOLN "
            + " FROM STP_MST_CSTMR ";
        DataTable dtCustomers = dbHelper.FetchData(qryToRun);

        MyResponseObject response = new MyResponseObject();
        response.success = true;
        response.data = dtCustomers;

        PrintResponseWithAll(JsonConvert.SerializeObject(response));
    }

    [WebMethod]
    public MyResponseObject RequestOtp(string UserId, string strReceived)
    {
        DateTime dtNow = DateTime.Now;
        string strDateNow = dtNow.ToString("yyyy-MM-dd");
        string strTimeNow = dtNow.ToString("HH:mm:ss");

        string[] strArr = strReceived.Split('|');

        string RouteNo = strArr[0];
        string TktNo = strArr[1];
        string TktDate = strArr[2];
        string CusNo = strArr[3];
        string ItemNo = strArr[4];
        string UoM = strArr[5];
        string Qty = strArr[6];
        string OldPrice = strArr[7];
        string NewPrice = strArr[8];
        string ReasonCode = strArr[9];
        string ExpiryDate = strArr[10];

        string StringToHash = strReceived;

        // VAN0665011826320201110VS-510531F-1188BOX8.000100.00164.00SALTSECRET_KEY
        string otpCode = dbHelper.GetStableHash(StringToHash).ToString();

        string otpRqstQry = "INSERT INTO SLS_OTP_RQUST ( "
            + " OTR_USRID, OTR_STRNG, OTR_CODE, OTR_RCVDT, "
            + " OTR_RCVTM, OTR_ROUTE, OTR_TKTNO, OTR_TKTDT, "
            + " OTR_CUSNO, OTR_ITMNO, OTR_UOM, OTR_QTY, "
            + " OTR_OLDPR, OTR_NEWPR "
            + " ) OUTPUT INSERTED.OTR_REQID VALUES ( "
            + " '" + UserId + "', '" + StringToHash + "', '" + otpCode + "', '" + strDateNow + "', "
            + " '" + strTimeNow + "', '" + RouteNo + "', '" + TktNo + "', '" + TktDate + "', "
            + " '" + CusNo + "', '" + ItemNo + "', '" + UoM + "', '" + Qty + "', "
            + " '" + OldPrice + "', '" + NewPrice + "' "
            + " ) ";
        string requestID = dbHelper.ExecuteScalarQuery(otpRqstQry).ToString();


        MyResponseObject response = new MyResponseObject();
        response.success = true;
        response.data = "OTP requested successfully.\nYour Request ID is : " + requestID;

        //PrintResponseWithAll(JsonConvert.SerializeObject(response));
        return response;
    }

    [WebMethod]
    public void ListAllOtpRequests(string UserId)
    {
        DataTable dtUsr = dbHelper.FetchData("SELECT * FROM STP_MSTR_USERS WHERE USR_ID = '" + UserId + "'; ");

        string StrNos = "";
        // Validate the user password
        if (dtUsr.Rows.Count > 0)
        {
            if (dtUsr.Rows[0]["USR_SYACS"].ToString().Equals("1"))
            {
                string allBranchCodes = "'" + dtUsr.Rows[0]["USR_PRMBR"].ToString();

                DataTable dtBranchCodes = dbHelper.FetchData("SELECT * FROM STP_MSTR_STORE ");
                foreach (DataRow brnRw in dtBranchCodes.Rows)
                {
                    allBranchCodes += " " + brnRw["STM_CODE"];
                }
                allBranchCodes = allBranchCodes.Replace(" ", "', '");
                allBranchCodes = allBranchCodes + "'";

                StrNos = allBranchCodes;
            }
            else
            {
                StrNos = "'" + dtUsr.Rows[0]["USR_PRMBR"].ToString() + "'";
            }
        }

        DateTime dtNow = DateTime.Now;
        string dateToday = dtNow.ToString("yyyy-MM-dd");
        string timeOneHourAgo = dtNow.AddHours(-1).ToString("HH:mm");

        string qryToRun = " SELECT "
                + " * "
                + " FROM SLS_OTP_RQUST "
                + " LEFT JOIN STP_MSTR_SLREP ON OTR_ROUTE = SRP_CODE "
                + " WHERE OTR_RCVDT = '" + dateToday + "' "
                + " AND OTR_RCVTM > '" + timeOneHourAgo + "' "
                + " AND SRP_STRNO IN (" + StrNos + ") ";
        DataTable dtOtpRequests = dbHelper.FetchData(qryToRun);

        MyResponseObject response = new MyResponseObject();
        response.success = true;
        response.data = dtOtpRequests;

        PrintResponseWithAll(JsonConvert.SerializeObject(response));
    }

    [WebMethod]
    public MyResponseObject TestConnection(string msg)
    {
        MyResponseObject response = new MyResponseObject();
        response.success = true;
        response.data = "Successfully received : " + msg;

        //PrintResponseWithAll(JsonConvert.SerializeObject(response));
        return response;
    }

    [WebMethod]
    public void GetAllFrom(string tblName)
    {
        DataTable DtTbl = dbHelper.FetchData("SELECT * FROM " + tblName + "");

        MyResponseObject response = new MyResponseObject();
        response.success = true;
        response.data = DtTbl;

        PrintResponseWithAll(JsonConvert.SerializeObject(response));
    }//end method

    [WebMethod]
    public void GetAllFromWhere(string tblName, string Where)
    {
        DataTable DtTbl = dbHelper.FetchData("SELECT * FROM " + tblName + " " + Where);

        MyResponseObject response = new MyResponseObject();
        response.success = true;
        response.data = DtTbl;

        PrintResponseWithAll(JsonConvert.SerializeObject(response));
    }

    // A simple class to send response
    public class MyResponseObject
    {
        public bool success;
        public string message;
        public object data;
    }
}
