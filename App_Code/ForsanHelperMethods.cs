﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for ForsanHelperMethods
/// </summary>
public static class ForsanHelperMethods
{

    public static void FillColumnsDropDown(ASPxGridView gridViewControl, ASPxComboBox cmBox)
    {
        ASPxComboBox cbFilter = (ASPxComboBox)gridViewControl.FindStatusBarTemplateControl("DDLFilterByColumn");
        if (cbFilter == null)
        {
            return;
        }
        cbFilter.Items.Clear();
        //cbFilter.Items.Add(new ListEditItem("All", "*"));
        foreach (GridViewColumn column in gridViewControl.Columns)
        {
            if (column is GridViewDataColumn)
            {
                GridViewDataColumn dataColumn = (GridViewDataColumn)column;
                if (dataColumn.Visible == true)
                {
                    cbFilter.Items.Add(new ListEditItem(dataColumn.Caption, dataColumn.FieldName));
                }
            } //end if
        } //end foreach
        cbFilter.DataBindItems();
        if (cmBox != null && !cmBox.Text.Equals(String.Empty))
        {
            cbFilter.Text = cmBox.Text;
            cbFilter.SelectedIndex = cmBox.SelectedIndex;
        }
        else
        {
            cbFilter.Text = "All";
            cbFilter.SelectedIndex = 0;
        }
    }

    public static string GetOrderStatus(object status, bool forClient = true)
    {
        string className = "";
        string statusString = "";

        switch (status.ToString())
        {
            case "5":
                className = "label-rejected white-clr";
                statusString = "Rejected";
                break;
            case "0":
                className = "label-pending";
                statusString = "Pending";
                break;
            case "1":
                className = "label-approved";
                statusString = "Approved";
                break;
            case "2":
                className = "label-queue";
                statusString = "In Queue";
                break;
            case "3":
                className = "label-readyfrpickup";
                statusString = "Ready for Pickup";
                break;
            case "4":
                className = "label-pickedup";
                statusString = "Picked up";
                break;
            case "6":
                className = "label-inkitchen";
                statusString = "In Kitchen";
                break;
            case "7":
                className = "label-readytodeliver";
                statusString = "Ready to Deliver";
                break;
            case "8":
                className = "label-delivered";
                statusString = "Delivered";
                break;
            case "9":
                className = "label-ontheway";
                statusString = "On the way";
                break;
            case "10":
                className = "label-onhold white-clr";
                statusString = "On Hold";
                break;
            case "11":
                className = "label-rejected white-clr";
                statusString = "UnDelivered";
                break;
            case "12":
                className = "label-rejected white-clr";
                statusString = "Cancelled";
                break;
            default:
                className = "";
                statusString = "";
                break;
        }

        if (!forClient)
        {
            return statusString;
        }

        return "<span class='" + className + "'>" + statusString + "</span>";
    }

    public static string GetOrderStatusColumns(object status)
    {
        string columnPrefix = "";

        switch (status.ToString())
        {
            case "5":
                columnPrefix = "ORD_RJT";
                break;
            case "0":
                columnPrefix = "ORD_PEN";
                break;
            case "1":
                columnPrefix = "ORD_APV";
                break;
            case "2":
                columnPrefix = "ORD_INQ";
                break;
            case "3":
                columnPrefix = "ORD_R2P";
                break;
            case "4":
                columnPrefix = "ORD_PKU";
                break;
            case "6":
                columnPrefix = "ORD_INK";
                break;
            case "7":
                columnPrefix = "ORD_R2D";
                break;
            case "8":
                columnPrefix = "ORD_DLV";
                break;
            case "9":
                columnPrefix = "ORD_ONW";
                break;
            case "10":
                columnPrefix = "ORD_HLD";
                break;
            case "11":
                columnPrefix = "ORD_UDL";
                break;
            case "12":
                columnPrefix = "ORD_CNL";
                break;
            default:
                columnPrefix = "";
                break;
        }

        return columnPrefix;
    }

    // Process all files in the directory passed in, recurse on any directories 
    // that are found, and process the files they contain.
    public static DataTable ProcessDirectory(string targetDirectory)
    {
        DataTable filesListToReturn = new DataTable();
        filesListToReturn.Columns.Add("FILE_PATH");

        // Process the list of files found in the directory.
        string[] fileEntries = Directory.GetFiles(targetDirectory);
        foreach (string fileName in fileEntries)
        {
            DataRow tempRow = filesListToReturn.NewRow();
            tempRow["FILE_PATH"] = fileName;
            filesListToReturn.Rows.Add(tempRow);
        }

        // Recurse into subdirectories of this directory.
        string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
        foreach (string subdirectory in subdirectoryEntries)
        {
            filesListToReturn.Merge(ProcessDirectory(subdirectory));
        }

        return filesListToReturn;
    }

    public static string CurrentPage()
    {
        string CurrentPagepath = HttpContext.Current.Request.FilePath;

        CurrentPagepath = CurrentPagepath.Replace("Admin", "");
        CurrentPagepath = CurrentPagepath.Trim('/');
        //lastDotIndex = CurrentPagepath.LastIndexOf('/');
        //firstPart = CurrentPagepath.Remove(lastDotIndex);
        //secondPart = CurrentPagepath.Substring(lastDotIndex + 1, CurrentPagepath.Length - firstPart.Length - 1);

        //return secondPart.Trim();
        return CurrentPagepath;
    }

    public static Control FindControlRecursive(this Control control, string id)
    {
        if (control == null) return null;

        Control ctrl = control.FindControl(id);
        if (ctrl == null)
        {
            foreach (Control child in control.Controls)
            {
                ctrl = FindControlRecursive(child, id);

                if (ctrl != null) break;
            }
        }

        return ctrl;
    }
}