﻿<%@ Application Language="C#" %>
<%@ Import Namespace="FMR001" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Import Namespace="System.Web.Routing" %>

<script RunAt="server">

    void Application_Start(object sender, EventArgs e)
    {
        RouteConfig.RegisterRoutes(RouteTable.Routes);
        BundleConfig.RegisterBundles(BundleTable.Bundles);
    }

    protected void Application_BeginRequest(object sender, EventArgs e)
    {
        string origionalpath = Request.Url.ToString();
        string subPath = string.Empty;

        if (origionalpath.Contains("Admin/Orders") && !origionalpath.Contains("Admin/Orders/Merged"))
        {
            if (origionalpath.Length >= 22)
            {
                subPath = origionalpath.Substring(22);
                if (subPath.Length >= 1)
                {
                    //Context.RewritePath("~/Admin/Orders/Merged.aspx?asa=" + subPath);
                    Context.Response.Redirect("~/Admin/Orders/Merged.aspx?asa=" + subPath);
                }
            }
        }
    }
</script>
