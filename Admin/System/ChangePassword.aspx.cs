﻿using CrystalDecisions.CrystalReports.Engine;
using DevExpress.Web;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

public partial class Admin_System_ChangePassword : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {

    }


    protected void btnChangePassword_Click(object sender, EventArgs e)
    {
        if (!ValidateFormFields())
        {
            return;
        }

        DataTable myDT = dbHlpr.FetchData("SELECT * FROM STP_MSTR_USERS WHERE USR_ID = '" + Session["UserId"].ToString() + "'");

        if (myDT.Rows.Count <= 0)
        {
            lblErrorMessage.Text = "Error, user not found Exist.";
            return;
        }
        else
        {
            if (myDT.Rows[0]["USR_PSWRD"].ToString() != txtOldPass.Text)
            {
                lblErrorMessage.Text = "Error, Old Password is incorrect.";
                return;
            }
            string updateQry = "UPDATE STP_MSTR_USERS SET "
                + " USR_PSWRD = '" + txtNewPass.Text + "', "
                + " USR_CHGAT = GETDATE() "
                + " WHERE USR_ID = '" + Session["UserId"].ToString() + "'";
            dbHlpr.ExecuteNonQuery(updateQry);

            lblErrorMessage.Text = "Password Updated Successfully.";
        }
    }
    private bool ValidateFormFields()
    {
        if (txtOldPass.Text.Trim().Length <= 0)
        {
            lblErrorMessage.Text = "Old Password is required.";
            return false;
        }

        if (txtNewPass.Text.Trim().Length <= 0)
        {
            lblErrorMessage.Text = "Password is Required";
            return false;
        }

        if (txtCnfPass.Text.Trim().Length <= 0)
        {
            lblErrorMessage.Text = "Confirm Password is Required";
            return false;
        }

        if (!txtNewPass.Text.Equals(txtCnfPass.Text))
        {
            lblErrorMessage.Text = "Password and Confirm Password donot Match";
            return false;
        }

        return true;
    }
}