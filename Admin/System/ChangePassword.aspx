﻿<%@ Page Title="Admin Panel - Change Password" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="Admin_System_ChangePassword" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <script>
        // Code to create a custom multiselect ComboBox, Dropdown
        var textSeparator = ","; // Between each checked item in ComboBox
        function OnListBoxSelectionChanged(listBox, args) {
            if (args.index == 0)
                args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
            if (listBox.name.indexOf("lstBxJobLocations") != -1) {
                IsAllSelected(checkJobLocationsListBox) ? checkJobLocationsListBox.SelectIndices([0]) : checkJobLocationsListBox.UnselectIndices([0]);
                UpdateText(ClientJobLocations, checkJobLocationsListBox);
            } else if (listBox.name.indexOf("lstBxDepartments") != -1) {
                IsAllSelected(checkDepartmentsListBox) ? checkDepartmentsListBox.SelectIndices([0]) : checkDepartmentsListBox.UnselectIndices([0]);
                UpdateText(ClientDepartments, checkDepartmentsListBox);
            } else if (listBox.name.indexOf("lstBxEmployees") != -1) {
                IsAllSelected(checkEmployeesListBox) ? checkEmployeesListBox.SelectIndices([0]) : checkEmployeesListBox.UnselectIndices([0]);
                UpdateText(ClientEmployees, checkEmployeesListBox);
            }
        }
        function IsAllSelected(chkLstBx) {
            var selectedDataItemCount = chkLstBx.GetItemCount() - (chkLstBx.GetItem(0).selected ? 0 : 1);
            return chkLstBx.GetSelectedItems().length == selectedDataItemCount;
        }
        function UpdateText(ddObj, lstBxObj) {
            var selectedItems = lstBxObj.GetSelectedItems();
            ddObj.SetText(GetSelectedItemsText(selectedItems));
        }
        function SynchronizeListBoxValues(dropDown, args) {
            var texts = dropDown.GetText().split(textSeparator);
            if (dropDown.name.indexOf("JobLocations") != -1) {
                checkJobLocationsListBox.UnselectAll();
                checkJobLocationsListBox.SelectValues(texts);
                IsAllSelected(checkJobLocationsListBox) ? checkJobLocationsListBox.SelectIndices([0]) : checkJobLocationsListBox.UnselectIndices([0]);
                UpdateText(ClientJobLocations, checkJobLocationsListBox);
            } else if (dropDown.name.indexOf("Departments") != -1) {
                checkDepartmentsListBox.UnselectAll();
                checkDepartmentsListBox.SelectValues(texts);
                IsAllSelected(checkDepartmentsListBox) ? checkDepartmentsListBox.SelectIndices([0]) : checkDepartmentsListBox.UnselectIndices([0]);
                UpdateText(ClientDepartments, checkDepartmentsListBox);
            } else if (dropDown.name.indexOf("Employees") != -1) {
                checkEmployeesListBox.UnselectAll();
                checkEmployeesListBox.SelectValues(texts);
                IsAllSelected(checkEmployeesListBox) ? checkEmployeesListBox.SelectIndices([0]) : checkEmployeesListBox.UnselectIndices([0]);
                UpdateText(ClientEmployees, checkEmployeesListBox);
            }
        }
        function GetSelectedItemsText(items) {
            var texts = [];
            for (var i = 0; i < items.length; i++)
                if (items[i].index != 0)
                    texts.push(items[i].value);
            return texts.join(textSeparator);
        }
        function GetValuesByTexts(texts, chkLstBx) {
            var actualValues = [];
            var item;
            for (var i = 0; i < texts.length; i++) {
                item = chkLstBx.FindItemByText(texts[i]);
                if (item != null)
                    actualValues.push(item.value);
            }
            return actualValues;
        }
    </script>
</asp:Content>
<asp:Content ID="contentTopHere1" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
            <li>
                <asp:LinkButton ID="LinkBtnPDF" runat="server">
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonXLX" runat="server">
                <img src="~/img/FilesIcons/excel.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonXxls" runat="server">
                <img src="~/img/FilesIcons/xlsx.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonRtf" runat="server">
                <img src="~/img/FilesIcons/rtf.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonCvs" runat="server">
                <img src="~/img/FilesIcons/cvs.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonWord" runat="server">
                <img src="~/img/FilesIcons/word.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonHtml" runat="server">
                <img src="~/img/icons/searchtop.jpg" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton6" runat="server">               
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton7" runat="server">
                <img src="~/img/FilesIcons/word.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton8" runat="server">
                <img src="~/img/FilesIcons/excel.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton9" OnClientClick="window.print();" runat="server">
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton10" runat="server">
                <img src="~/img/FilesIcons/torrent.png" runat="server" />
                </asp:LinkButton>
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">
    <div class="formDiv">
        <div class="formHeaderDiv">
            <h3>Change Password</h3>
        </div>
        <div class="fieldsetWrapper">
            <fieldset>
                <legend></legend>
                <table style="margin: 0 auto 10px;">
                    <tr>
                        <td>
                            <dx:ASPxLabel ID="lblErrorMessage" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
                        </td>
                    </tr>
                    <tr>
                        <td>Old Password :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxTextBox ID="txtOldPass" Password="true" ClientInstanceName="txtClientOldPass" runat="server" MinLength="6" MaxLength="8" Width="170px" TabIndex="1">
                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="true" ErrorText="Old Password is Required" />
                                    </ValidationSettings>
                                    <NullTextStyle Font-Size="Small" />
                                </dx:ASPxTextBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>New Password :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxTextBox ID="txtNewPass" Password="true" ClientInstanceName="txtClientNewPass" runat="server" MinLength="6" MaxLength="8" Width="170px" TabIndex="2">
                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="true" ErrorText="New Password is Required" />
                                    </ValidationSettings>
                                    <NullTextStyle Font-Size="Small" />
                                </dx:ASPxTextBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Confirm Password :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxTextBox ID="txtCnfPass" Password="true" ClientInstanceName="txtClientCnfPass" runat="server" MinLength="6" MaxLength="8" Width="170px" TabIndex="3">
                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="true" ErrorText="Confirm Password is Required" />
                                    </ValidationSettings>
                                    <NullTextStyle Font-Size="Small" />
                                </dx:ASPxTextBox>
                            </div>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </fieldset>
            <hr style="color: skyblue" />
            <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                <dx:ASPxButton ID="btnChangePassword" runat="server" Text="Submit" CssClass="btnPopup btn btn-success" CausesValidation="true" OnClick="btnChangePassword_Click" />
                <dx:ASPxButton ID="btnPopUpClear" runat="server" Text="Clear" CssClass="btnPopup btn btn-warning" CausesValidation="false" AutoPostBack="false">
                    <ClientSideEvents Click="function(s, e) { CallBackCode.PerformCallback(CatClientEditCode.GetText()); }" />
                </dx:ASPxButton>
            </div>

        </div>
    </div>
</asp:Content>

