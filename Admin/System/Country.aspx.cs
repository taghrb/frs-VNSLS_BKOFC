﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;

public partial class Admin_System_Country : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT CTR_CODE, CTR_TITLE, CTR_CRNCY, CTR_SYMBL, CTR_XNGRT FROM STP_MST_CNTRY";
    }

    protected void categoryListingGridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvInquery_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string CntryCode = e.Keys["CTR_CODE"].ToString().Trim();

        dbHlpr.ExecuteNonQuery("DELETE FROM STP_MST_CNTRY WHERE CTR_CODE = '" + CntryCode + "'");

        e.Cancel = true;
        gvInquery.CancelEdit();
    }
    protected void ACB_CodeCheck_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (e.Parameter.Length > 0)
        {
            DataTable myDT = new DataTable();
            myDT = dbHlpr.FetchData("SELECT * FROM STP_MST_CNTRY WHERE CTR_CODE = '" + e.Parameter.Trim() + "'");
            JavaScriptSerializer myserializer = new JavaScriptSerializer();
            if (myDT.Rows.Count > 0)
            {
                e.Result = myserializer.Serialize(myDT.Rows[0].ItemArray);
            }
            else
            {
                e.Result = myserializer.Serialize("");
            }
        }
    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxTextBox txtBxCode = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("EditCode"));
        string CntryCode = "0";

        if (txtBxCode.Text.Length > 0)
        {
            CntryCode = txtBxCode.Text.Trim();
        }

        string CntryName = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("EditName")).Text;
        string Currency = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("EditCurrency")).Text;
        string Symbol = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("EditSymbol")).Text;
        string ExchangeRate = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("EditExchangeRate")).Text;

        DataTable myDT = new DataTable();

        myDT = myDT = dbHlpr.FetchData("SELECT * FROM STP_MST_CNTRY WHERE CTR_CODE = '" + CntryCode + "'");
        if (myDT.Rows.Count <= 0)
        {

            dbHlpr.ExecuteNonQuery("INSERT INTO STP_MST_CNTRY (CTR_CODE, CTR_TITLE, CTR_CRNCY, CTR_SYMBL, CTR_XNGRT) "
            + " VALUES ('" + CntryCode + "', '" + CntryName + "', '" + Currency + "', '" + Symbol + "', '" + ExchangeRate + "')");
        }
        else
        {
            dbHlpr.ExecuteNonQuery("UPDATE STP_MST_CNTRY SET "
            + " CTR_TITLE = '" + CntryName + "', "
            + " CTR_CRNCY = '" + Currency + "', "
            + " CTR_SYMBL = '" + Symbol + "', "
            + " CTR_XNGRT = '" + ExchangeRate + "' "
            + " WHERE CTR_CODE = '" + CntryCode + "'");
        }

        gvInquery.CancelEdit();
        gvInquery.DataBind();
    }
    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

    }
    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            string CntryId = e.CommandArgs.CommandArgument.ToString();

            if (CntryId != "") // Code is sent via request
            {
                dbHlpr.ExecuteNonQuery("DELETE FROM STP_MST_CNTRY WHERE CTR_CODE = '" + CntryId + "'");

                gvInquery.CancelEdit();
                gvInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid Country Code. Unable to delete Country.";
            }
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }
    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }
    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }


    protected void EditCode_Init(object sender, EventArgs e)
    {
        if (!gvInquery.IsNewRowEditing)
        {
            ASPxTextBox txtCode = (ASPxTextBox)sender;
            txtCode.ReadOnly = true;
        }
        else
        {
            ASPxTextBox txtCode1 = (ASPxTextBox)sender;
            txtCode1.ReadOnly = false;
        }
    }
}