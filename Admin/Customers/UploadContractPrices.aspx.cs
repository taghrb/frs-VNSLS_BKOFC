﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using System.Data.OleDb;

public partial class Admin_Customers_UploadContractPrices : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {

    }

    protected void btnDownloadTemplate_Click(object sender, EventArgs e)
    {
        ASPxGridView gvTemplateToExport = new ASPxGridView();
        gvTemplateToExport.ID = "Template_CustContractPrice_Upload";
        gvTemplateToExport.AutoGenerateColumns = true;

        this.Controls.Add(gvTemplateToExport);

        gvTemplateToExport.DataSource = dbHlpr.FetchData(
            "SELECT " +
            " '' AS [Cust No], '' AS [Item No], '' AS [UoM], '0.00' AS [Price], '0.00' AS [Min Price], '0.00' AS [Max Price] " +
            " WHERE 1 = 0 "
            );
        gvTemplateToExport.DataBind();

        dbHlpr.ExportToFormattedXls1997(this, gvTemplateToExport);
        this.Controls.Remove(gvTemplateToExport);
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        lblErrorMessage.Text = "";
        lblResult.Text = "";

        if (UploadedXlsFile.HasFile)
        {
            try
            {
                string fileNameWithExt = Path.GetFileName(UploadedXlsFile.FileName);
                string fileNameWithoutExt = Path.GetFileNameWithoutExtension(UploadedXlsFile.FileName);
                string fileExtension = fileNameWithExt.Replace(fileNameWithoutExt, "");
                if (fileExtension.Equals(".xls"))
                {
                    DateTime nw = DateTime.Now;
                    string timeStamp = nw.Year + nw.Month + nw.Day + "$" + nw.Hour + nw.Minute;
                    string saveAt = Server.MapPath("~/Uploads/ContractPrices/") + timeStamp + "_" + fileNameWithExt;
                    UploadedXlsFile.SaveAs(saveAt);

                    string resultMessage = "File Uploaded Successfully";


                    OleDbConnection oleDbConn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + saveAt + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"");

                    oleDbConn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    DataSet ds = new DataSet();

                    DataTable dtSheets;
                    dtSheets = oleDbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string sheetName = dtSheets.Rows[0]["TABLE_NAME"].ToString();

                    cmd.Connection = oleDbConn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                    oleda = new OleDbDataAdapter(cmd);
                    oleda.Fill(ds, "dsS1no");
                    DataTable dt = ds.Tables["dsS1no"];

                    DatabaseHelperClass dbhlpr = new DatabaseHelperClass();
                    int rowsInserted = 0;
                    int duplicatesFound = 0;
                    int rowsUpdated = 0;
                    int rowsSkipped = 0;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string CustomerNo = dt.Rows[i][0].ToString().Trim().Equals("") ? "*NULL" : dt.Rows[i][0].ToString().Trim();
                        string ItemNo = dt.Rows[i][1].ToString().Trim().Equals("") ? "*NULL" : dt.Rows[i][1].ToString().Trim();
                        string UoM = dt.Rows[i][2].ToString().Trim().Equals("") ? "*NULL" : dt.Rows[i][2].ToString().Trim();
                        string Price = dt.Rows[i][3].ToString().Trim().Equals("") ? "0" : dt.Rows[i][3].ToString().Trim();
                        string MinPrice = dt.Rows[i][4].ToString().Trim().Equals("") ? "0" : dt.Rows[i][4].ToString().Trim();
                        string MaxPrice = dt.Rows[i][5].ToString().Trim().Equals("") ? "0" : dt.Rows[i][5].ToString().Trim();
                        
                        DataTable dtCustomerVans = dbHlpr.FetchData("SELECT DISTINCT CUS_SLSRP FROM ( "
                            + " SELECT DISTINCT CUS_SLSRP FROM STP_MST_CSTMR WHERE CUS_NO = '" + CustomerNo + "' "
                            + " UNION ALL "
                            + " SELECT DISTINCT RTC_ROUTE FROM STP_LNK_RTCUS WHERE RTC_CUSNO = '" + CustomerNo + "' "
                            + " ) tbl ");
                        if (Convert.ToDouble(MaxPrice) >= Convert.ToDouble(MinPrice))
                        {
                            DataTable dtExisting = dbhlpr.FetchData("SELECT * "
                            + " FROM INV_PRC_CSTMR "
                            + " WHERE "
                            + " CSPRC_ITMCD = '" + ItemNo + "' "
                            + " AND CSPRC_CUSCD = '" + CustomerNo + "' "
                            + " AND CSPRC_UOM = '" + UoM + "' ");

                            if (dtExisting.Rows.Count > 0)
                            {
                                duplicatesFound++;
                            }

                            string key = CustomerNo + ItemNo + UoM;
                            if (dtExisting.Rows.Count == 0)
                            {
                                dbHlpr.ExecuteNonQuery("INSERT INTO INV_PRC_CSTMR ( "
                                    + " CSPRC_KEY, CSPRC_CUSCD, CSPRC_ITMCD, CSPRC_UOM, CSPRC_PRICE, CSPRC_MIN, CSPRC_MAX "
                                    + " ) VALUES ( "
                                    + " '" + key + "', '" + CustomerNo + "', '" + ItemNo + "', '" + UoM + "', '" + Price + "', '" + MinPrice + "', '" + MaxPrice + "' "
                                    + " )");
                                rowsInserted++;

                                DataTable dt1 = dbHlpr.FetchData("SELECT "
                                    + " CSPRC_KEY AS TEXT01, CSPRC_CUSCD AS TEXT02, CSPRC_ITMCD AS TEXT03, CSPRC_UOM AS TEXT04, "
                                    + " CSPRC_PRICE AS NUM01, CSPRC_MIN AS NUM02, CSPRC_MAX AS NUM03 "
                                    + " FROM INV_PRC_CSTMR "
                                    + " WHERE CSPRC_KEY = '" + key + "' ");
                                foreach (DataRow drw in dtCustomerVans.Rows)
                                {
                                    dbHlpr.CreateDownloadRecord(dt1, drw["CUS_SLSRP"].ToString(), "INV_PRC_CSTMR", "CRT");
                                }
                            }
                            else if (dtExisting.Rows.Count > 0) // && chkBxUpdateDuplicates.Checked)
                            {
                                string updateQry = "UPDATE INV_PRC_CSTMR SET "
                                    + " CSPRC_KEY = '" + key + "', CSPRC_CUSCD = '" + CustomerNo + "', CSPRC_ITMCD = '" + ItemNo + "', CSPRC_UOM = '" + UoM + "', "
                                    + " CSPRC_PRICE = '" + Price + "', CSPRC_MIN = '" + MinPrice + "', CSPRC_MAX = '" + MaxPrice + "' "
                                    + " WHERE "
                                    + " CSPRC_ITMCD = '" + ItemNo + "' "
                                    + " AND CSPRC_CUSCD = '" + CustomerNo + "' "
                                    + " AND CSPRC_UOM = '" + UoM + "' ";
                                rowsUpdated += dbhlpr.ExecuteNonQuery(updateQry);

                                DataTable dt1 = dbHlpr.FetchData("SELECT "
                                    + " CSPRC_KEY AS TEXT01, CSPRC_CUSCD AS TEXT02, CSPRC_ITMCD AS TEXT03, CSPRC_UOM AS TEXT04, "
                                    + " CSPRC_PRICE AS NUM01, CSPRC_MIN AS NUM02, CSPRC_MAX AS NUM03 "
                                    + " FROM INV_PRC_CSTMR "
                                    + " WHERE CSPRC_KEY = '" + key + "' ");
                                foreach (DataRow drw in dtCustomerVans.Rows)
                                {
                                    dbHlpr.CreateDownloadRecord(dt1, drw["CUS_SLSRP"].ToString(), "INV_PRC_CSTMR", "UPD");
                                }
                            }
                        }
                        else
                        {
                            rowsSkipped++;
                        }
                    }

                    lblResult.Text = resultMessage;
                    lblResult.Text += "\n" + rowsInserted + " records inserted successfully.";
                    lblResult.Text += "\n" + duplicatesFound + " duplicate records found.";
                    lblResult.Text += "\n" + rowsUpdated + " records updated successfully.";
                    lblResult.Text += "\n" + rowsSkipped + " records skipped. Max Price cannot be less than Min Price.";
                }
                else
                {
                    lblErrorMessage.Text = "Please Select a valid .xls file";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        else
        {
            lblErrorMessage.Text = "Please Select a file to Upload.";
        }
    }

}