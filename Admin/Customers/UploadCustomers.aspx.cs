﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using System.Data.OleDb;

public partial class Admin_Customers_UploadCustomers : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {

    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        lblErrorMessage.Text = "";
        lblResult.Text = "";

        if (UploadedXlsFile.HasFile)
        {
            try
            {
                string fileNameWithExt = Path.GetFileName(UploadedXlsFile.FileName);
                string fileNameWithoutExt = Path.GetFileNameWithoutExtension(UploadedXlsFile.FileName);
                string fileExtension = fileNameWithExt.Replace(fileNameWithoutExt, "");
                if (fileExtension.Equals(".xls"))
                {
                    DateTime nw = DateTime.Now;
                    string timeStamp = nw.Year + nw.Month + nw.Day + "$" + nw.Hour + nw.Minute;
                    string saveAt = Server.MapPath("~/Uploads/Customers/") + timeStamp + "_" + fileNameWithExt;
                    UploadedXlsFile.SaveAs(saveAt);

                    string resultMessage = "File Uploaded Successfully";

                    OleDbConnection oleDbConn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + saveAt + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"");

                    oleDbConn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    DataSet ds = new DataSet();

                    DataTable dtSheets;
                    dtSheets = oleDbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string sheetName = dtSheets.Rows[0]["TABLE_NAME"].ToString();

                    cmd.Connection = oleDbConn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                    oleda = new OleDbDataAdapter(cmd);
                    oleda.Fill(ds, "dsS1no");
                    DataTable dt = ds.Tables["dsS1no"];

                    DatabaseHelperClass dbhlpr = new DatabaseHelperClass();
                    int rowsInserted = 0;
                    int duplicatesFound = 0;
                    int rowsUpdated = 0;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string CustomerNo = dt.Rows[i][0].ToString().Trim().Equals("") ? "*NULL" : dt.Rows[i][0].ToString().Trim();
                        string Name       = dt.Rows[i][1].ToString().Trim().Equals("") ? "*NULL" : dt.Rows[i][1].ToString().Trim();
                        string Addr1      = dt.Rows[i][2].ToString().Trim().Equals("") ? "" : dt.Rows[i][2].ToString().Trim();
                        string Addr2      = dt.Rows[i][3].ToString().Trim().Equals("") ? "" : dt.Rows[i][3].ToString().Trim();
                        string City       = dt.Rows[i][4].ToString().Trim().Equals("") ? "" : dt.Rows[i][4].ToString().Trim();
                        string Cntact1    = dt.Rows[i][5].ToString().Trim().Equals("") ? "" : dt.Rows[i][5].ToString().Trim();
                        string Cntact2    = dt.Rows[i][6].ToString().Trim().Equals("") ? "" : dt.Rows[i][6].ToString().Trim();
                        string Phone1     = dt.Rows[i][7].ToString().Trim().Equals("") ? "" : dt.Rows[i][7].ToString().Trim();
                        string Phone2     = dt.Rows[i][8].ToString().Trim().Equals("") ? "" : dt.Rows[i][8].ToString().Trim();
                        string Email      = dt.Rows[i][9].ToString().Trim().Equals("") ? "" : dt.Rows[i][9].ToString().Trim();
                        string Srep       = dt.Rows[i][10].ToString().Trim().Equals("") ? "" : dt.Rows[i][10].ToString().Trim();
                        string Type       = "CASH";
                        if (dt.Rows[i][11].ToString().Trim().Equals("1")) {
                            Type          = "CASH";
                        } else if (dt.Rows[i][11].ToString().Trim().Equals("2")) {
                            Type          = "A/R";
                        }
                        string Catg       = dt.Rows[i][12].ToString().Trim().Equals("") ? "" : dt.Rows[i][12].ToString().Trim();
                        string VatNo      = dt.Rows[i][13].ToString().Trim().Equals("") ? "" : dt.Rows[i][13].ToString().Trim();
                        string IsTaxable  = dt.Rows[i][14].ToString().Trim().Equals("") ? "Y" : dt.Rows[i][14].ToString().Trim();
                        string GrpCode    = dt.Rows[i][15].ToString().Trim().Equals("") ? "" : dt.Rows[i][15].ToString().Trim();
                        string NameAr     = dt.Rows[i][16].ToString().Trim().Equals("") ? "" : dt.Rows[i][16].ToString().Trim();
                        string GeoLat     = dt.Rows[i][17].ToString().Trim().Equals("") ? "" : dt.Rows[i][17].ToString().Trim();
                        string GeoLng     = dt.Rows[i][18].ToString().Trim().Equals("") ? "" : dt.Rows[i][18].ToString().Trim();

                        DataTable dtExisting = dbhlpr.FetchData("SELECT * "
                            + " FROM STP_MST_CSTMR "
                            + " WHERE "
                            + " CUS_NO = '" + CustomerNo + "' ");

                        if (dtExisting.Rows.Count > 0)
                        {
                            duplicatesFound++;
                        }

                        if (dtExisting.Rows.Count == 0)
                        {
                            string qry = "INSERT INTO STP_MST_CSTMR ( "
                                + " CUS_NO, CUS_NAME, CUS_ADDR1, CUS_ADDR2, "
                                + " CUS_CITY, CUS_CNTC1, CUS_CNTC2, CUS_PHON1, "
                                + " CUS_PHON2, CUS_EMAIL, CUS_SLSRP, CUS_TYPE, "
                                + " CUS_CATG, CUS_VATNO, CUS_TXABL, CUS_GRPCD, "
                                + " CUS_NAMAR, CUS_GEOLA, CUS_GEOLN "
                                + " ) VALUES ( "
                                + " N'" + CustomerNo + "', N'" + Name + "', " + " N'" + Addr1 + "', N'" + Addr2 + "', "
                                + " N'" + City + "', N'" + Cntact1 + "', " + " N'" + Cntact2 + "', N'" + Phone1 + "', "
                                + " N'" + Phone2 + "', N'" + Email + "', " + " N'" + Srep + "', N'" + Type + "', "
                                + " N'" + Catg + "', N'" + VatNo + "', " + " N'" + IsTaxable + "', N'" + GrpCode + "', "
                                + " N'" + NameAr + "', N'" + GeoLat + "', " + " N'" + GeoLng + "' "
                                + " ) ";
                            dbhlpr.ExecuteNonQuery(qry);
                            rowsInserted++;
                        }
                        else if (dtExisting.Rows.Count > 0) // && chkBxUpdateDuplicates.Checked)
                        {
                            string updateQry = "UPDATE STP_MST_CSTMR SET "
                                + " CUS_NAME  = N'" + Name + "', "
                                + " CUS_ADDR1 = N'" + Addr1 + "', "
                                + " CUS_ADDR2 = N'" + Addr2 + "', "
                                + " CUS_CITY  = N'" + City + "', "
                                + " CUS_CNTC1 = N'" + Cntact1 + "', "
                                + " CUS_CNTC2 = N'" + Cntact2 + "', "
                                + " CUS_PHON1 = N'" + Phone1 + "', "
                                + " CUS_PHON2 = N'" + Phone2 + "', "
                                + " CUS_EMAIL = N'" + Email + "', "
                                + " CUS_SLSRP = N'" + Srep + "', "
                                + " CUS_TYPE  = N'" + Type + "', "
                                + " CUS_CATG  = N'" + Catg + "', "
                                + " CUS_VATNO = N'" + VatNo + "', "
                                + " CUS_TXABL = N'" + IsTaxable + "', "
                                + " CUS_GRPCD = N'" + GrpCode + "', "
                                + " CUS_NAMAR = N'" + NameAr + "', "
                                + " CUS_GEOLA = N'" + GeoLat + "', "
                                + " CUS_GEOLN = N'" + GeoLng + "' "
                                + " WHERE "
                                + " CUS_NO = '" + CustomerNo + "' ";
                            rowsUpdated += dbhlpr.ExecuteNonQuery(updateQry);
                        }
                    }

                    lblResult.Text = resultMessage;
                    lblResult.Text += "\n" + rowsInserted + " records inserted successfully.";
                    lblResult.Text += "\n" + duplicatesFound + " duplicate records found.";
                    lblResult.Text += "\n" + rowsUpdated + " records updated successfully.";
                }
                else
                {
                    lblErrorMessage.Text = "Please Select a valid .xls file";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        else
        {
            lblErrorMessage.Text = "Please Select a file to Upload.";
        }
    }

}