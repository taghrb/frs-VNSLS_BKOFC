﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;

public partial class Admin_CustomersCatg_ContractPrices : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT CPR_CUSNO, CPR_ITMNO, CPR_UOM, CPR_PRICE, CUS_NAME, PRO_DESC1 "
            + " FROM CUS_CNT_PRICE "
            + " JOIN INV_MSTR_PRODT ON CPR_ITMNO = PRO_CODE "
            + " JOIN STP_MST_CSTMR ON CPR_CUSNO = CUS_NO ";
    }

    protected void categoryListingGridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvInquery_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string custNo = e.Keys["CPR_CUSNO"].ToString().Trim();
        string itemNo = e.Keys["CPR_ITMNO"].ToString().Trim();
        string uom = e.Keys["CPR_UOM"].ToString().Trim();

        dbHlpr.ExecuteNonQuery("DELETE FROM CUS_CNT_PRICE WHERE CPR_CUSNO = '" + custNo + "' AND CPR_ITMNO = '" + itemNo + "' AND CPR_UOM = '" + uom + "'");

        e.Cancel = true;
        gvInquery.CancelEdit();
    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxGridLookup lkUpCustomer = ((ASPxGridLookup)gvInquery.FindEditFormTemplateControl("CustomerCode"));
        ASPxGridLookup lkUpItem = ((ASPxGridLookup)gvInquery.FindEditFormTemplateControl("ItemNo"));

        string custNo = lkUpCustomer.Value == null ? "" : lkUpCustomer.Value.ToString().Trim();
        string itemNo = lkUpItem.Value == null ? "" : lkUpItem.Value.ToString().Trim();

        if (custNo.Length <= 0 || itemNo.Length <= 0)
        {
            gvInquery.CancelEdit();
            gvInquery.DataBind();
            return;
        }

        System.Web.UI.HtmlControls.HtmlTableRow row1 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw1"));
        System.Web.UI.HtmlControls.HtmlTableRow row2 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw2"));
        System.Web.UI.HtmlControls.HtmlTableRow row3 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw3"));

        dbHlpr.ExecuteNonQuery("DELETE FROM CUS_CNT_PRICE WHERE CPR_CUSNO = '" + custNo + "' AND CPR_ITMNO = '" + itemNo + "' ");
        if (row1 != null)
        {
            Label lblUom1 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom1"));
            ASPxTextBox txtPrice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice1"));

            string uom1 = lblUom1.Text;
            string price1 = txtPrice1.Text;

            if (uom1.Trim().Length > 0 && price1.Trim().Length > 0)
            {
                dbHlpr.ExecuteNonQuery("INSERT INTO CUS_CNT_PRICE ( "
                    + " CPR_CUSNO, CPR_ITMNO, CPR_UOM, CPR_PRICE "
                    + " ) VALUES ( "
                    + " '" + custNo + "', '" + itemNo + "', '" + uom1 + "', '" + price1 + "' "
                    + " )");
            }
        }

        if (row2 != null)
        {
            Label lblUom2 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom2"));
            ASPxTextBox txtPrice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice2"));

            string uom2 = lblUom2.Text;
            string price2 = txtPrice2.Text;

            if (uom2.Trim().Length > 0 && price2.Trim().Length > 0)
            {
                dbHlpr.ExecuteNonQuery("INSERT INTO CUS_CNT_PRICE ( "
                    + " CPR_CUSNO, CPR_ITMNO, CPR_UOM, CPR_PRICE "
                    + " ) VALUES ( "
                    + " '" + custNo + "', '" + itemNo + "', '" + uom2 + "', '" + price2 + "' "
                    + " )");
            }
        }

        if (row3 != null)
        {
            Label lblUom3 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom3"));
            ASPxTextBox txtPrice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice3"));

            string uom3 = lblUom3.Text;
            string price3 = txtPrice3.Text;

            if (uom3.Trim().Length > 0 && price3.Trim().Length > 0)
            {
                dbHlpr.ExecuteNonQuery("INSERT INTO CUS_CNT_PRICE ( "
                    + " CPR_CUSNO, CPR_ITMNO, CPR_UOM, CPR_PRICE "
                    + " ) VALUES ( "
                    + " '" + custNo + "', '" + itemNo + "', '" + uom3 + "', '" + price3 + "' "
                    + " )");
            }
        }

        gvInquery.CancelEdit();
        gvInquery.DataBind();
    }
    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

    }
    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        {
            string args = e.CommandArgs.CommandArgument.ToString();
            string[] prms = args.Split(',');

            string custNo = prms[0];
            string itemNo = prms[1];
            string uom = prms[2];

            dbHlpr.ExecuteNonQuery("DELETE FROM CUS_CNT_PRICE WHERE CPR_CUSNO = '" + custNo + "' AND CPR_ITMNO = '" + itemNo + "' AND CPR_UOM = '" + uom + "'");

            gvInquery.CancelEdit();
            gvInquery.DataBind();
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }

    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }
    protected void SelectedValueChanged(object sender, EventArgs e)
    {
        ASPxGridLookup lkUpCustomer = ((ASPxGridLookup)gvInquery.FindEditFormTemplateControl("CustomerCode"));
        ASPxGridLookup lkUpItem = ((ASPxGridLookup)gvInquery.FindEditFormTemplateControl("ItemNo"));

        System.Web.UI.HtmlControls.HtmlTableRow row1 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw1"));
        System.Web.UI.HtmlControls.HtmlTableRow row2 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw2"));
        System.Web.UI.HtmlControls.HtmlTableRow row3 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw3"));

        row1.Visible = false;
        row2.Visible = false;
        row3.Visible = false;

        DataTable dtItemUoms = new DataTable();
        if (lkUpItem.Value != null)
        {
            dtItemUoms = dbHlpr.FetchData("SELECT "
                + " ALU_ITMCD, ALU_UOMCD, '' AS PRICE, ALU_MAIN "
                + " FROM INV_ITM_ALUOM "
                + " WHERE ALU_ITMCD = '" + lkUpItem.Value.ToString().Trim() + "' "
                + " ORDER BY ALU_MAIN DESC, ALU_UOMCD ");
        }

        if (lkUpCustomer.Value != null && lkUpItem.Value != null)
        {
            DataTable dtCntctPrices = dbHlpr.FetchData(" SELECT "
                + " CPR_CUSNO, CPR_ITMNO, CPR_UOM, CPR_PRICE "
                + " FROM CUS_CNT_PRICE "
                + " WHERE CPR_CUSNO = '" + lkUpCustomer.Value.ToString().Trim() + "' "
                + " AND CPR_ITMNO = '" + lkUpItem.Value.ToString().Trim() + "'; ");

            for (int i = 0; i < dtCntctPrices.Rows.Count; i++)
            {
                for (int j = 0; j < dtItemUoms.Rows.Count; j++)
                {
                    if (dtCntctPrices.Rows[i]["CPR_UOM"].ToString().Trim().ToLower() == dtItemUoms.Rows[j]["ALU_UOMCD"].ToString().Trim().ToLower())
                    {
                        dtItemUoms.Rows[j]["PRICE"] = dtCntctPrices.Rows[i]["CPR_PRICE"];
                    }
                }
            }
        }

        if (dtItemUoms.Rows.Count > 0)
        {
            Label uom1 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom1"));
            ASPxTextBox price1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice1"));

            uom1.Text = dtItemUoms.Rows[0]["ALU_UOMCD"].ToString();
            price1.Text = dtItemUoms.Rows[0]["PRICE"].ToString();

            row1.Visible = true;
        }

        if (dtItemUoms.Rows.Count > 1)
        {
            Label uom2 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom2"));
            ASPxTextBox price2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice2"));

            uom2.Text = dtItemUoms.Rows[1]["ALU_UOMCD"].ToString();
            price2.Text = dtItemUoms.Rows[1]["PRICE"].ToString();

            row2.Visible = true;
        }

        if (dtItemUoms.Rows.Count > 2)
        {
            Label uom3 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom3"));
            ASPxTextBox price3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice3"));

            uom3.Text = dtItemUoms.Rows[2]["ALU_UOMCD"].ToString();
            price3.Text = dtItemUoms.Rows[2]["PRICE"].ToString();

            row3.Visible = true;
        }
    }
}