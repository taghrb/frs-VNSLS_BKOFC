﻿<%@ Page Title="Manage Customers Contract Prices" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="ContractPrices.aspx.cs" Inherits="Admin_CustomersCatg_ContractPrices" %>


<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <script type="text/javascript">

        function grid_customizationWindowCloseUp(s, e) {
            gvClientInquery.ShowCustomizationWindow();
        }
    </script>

</asp:Content>
<asp:Content ID="contentTopHere1" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
            <li>
                <asp:LinkButton ID="LinkBtnPDF" OnClick="LinkBtnPDF_Click" runat="server">
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonXLX" OnClick="LinkButtonXLX_Click" runat="server">
                <img src="~/img/FilesIcons/excel.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonXxls" OnClick="LinkButtonXxls_Click" runat="server">
                <img src="~/img/FilesIcons/xlsx.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonRtf" OnClick="LinkButtonRtf_Click" runat="server">
                <img src="~/img/FilesIcons/rtf.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonCvs" OnClick="LinkButtonCvs_Click" runat="server">
                <img src="~/img/FilesIcons/cvs.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonWord" OnClick="LinkButtonRtf_Click" runat="server">
                <img src="~/img/FilesIcons/word.png" runat="server" />
                </asp:LinkButton>
            </li>

            <li>
                <asp:LinkButton ID="LinkButtonHtml" OnClick="LinkButtonHtml_Click" runat="server">
                <img src="~/img/icons/searchtop.jpg" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton6" OnClick="LinkButton3_Click" runat="server">               
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton7" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/word.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton8" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/excel.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton9" OnClick="LinkButton3_Click" OnClientClick="window.print();" runat="server">
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton10" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/torrent.png" runat="server" />
                </asp:LinkButton>
            </li>
        </ul>
        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvInquery" ExportedRowType="All"></dx:ASPxGridViewExporter>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">

    <div class="formDiv">
        <div class="formHeaderDiv">
            <h3>Customers Contract Prices</h3>
        </div>
        <asp:UpdatePanel runat="server" ID="UpdatePanelOrdersPage" ChildrenAsTriggers="true">
            <ContentTemplate>
                <asp:SqlDataSource runat="server" ID="DxGridSqlDataSource1"
                    ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
                <dx:ASPxGridView ID="gvInquery" CssClass="FromGridView" ClientInstanceName="gvClientInquery" Theme="Office2010Black" runat="server" AutoGenerateColumns="False" KeyFieldName="CPR_CUSNO;CPR_ITMNO;CPR_UOM"
                    DataSourceID="DxGridSqlDataSource1" OnCustomButtonCallback="gvInquery_CustomButtonCallback"
                    OnRowCommand="gvInquery_RowCommand"
                    OnDataBound="gvInquery_DataBound"
                    OnBeforeGetCallbackResult="gvInquery_BeforeGetCallbackResult"
                    OnRowDeleting="gvInquery_RowDeleting">

                    <SettingsContextMenu Enabled="True" EnableColumnMenu="True" EnableRowMenu="True"></SettingsContextMenu>
                    <SettingsSearchPanel Visible="False" CustomEditorID="CustomSearchPanelTxtBx" />
                    <Columns>
                        <dx:GridViewCommandColumn ShowNewButtonInHeader="false" ShowSelectButton="true" Width="110" Caption=" " AllowDragDrop="False" VisibleIndex="1"></dx:GridViewCommandColumn>
                        <dx:GridViewCommandColumn ShowNewButtonInHeader="true" ShowEditButton="true" Width="110" VisibleIndex="2" AllowDragDrop="False"></dx:GridViewCommandColumn>
                        <dx:GridViewCommandColumn ShowNewButtonInHeader="false" ShowDeleteButton="true" Width="130" Caption=" " AllowDragDrop="False" VisibleIndex="3"></dx:GridViewCommandColumn>

                        <dx:GridViewDataTextColumn FieldName="CPR_CUSNO" Caption="Cust #" VisibleIndex="4">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                            <EditFormSettings VisibleIndex="0" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="CUS_NAME" Caption="Cust Name" VisibleIndex="5">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                            <EditFormSettings VisibleIndex="0" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="CPR_ITMNO" Caption="Item #" VisibleIndex="6">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="PRO_DESC1" Caption="Item Name" VisibleIndex="7">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                            <EditFormSettings VisibleIndex="0" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="CPR_UOM" Caption="UoM" VisibleIndex="8">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="CPR_PRICE" Caption="Price" VisibleIndex="9">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <Settings HorizontalScrollBarMode="Auto" />

                    <SettingsCommandButton>
                        <NewButton ButtonType="Button" Text="New" Styles-Style-CssClass="btna btn btn-primary  btn-sm">
                            <Image runat="server" Url="~/img/icons/white-icn/Add.png" Width="15px"></Image>
                        </NewButton>

                        <SelectButton ButtonType="Button" Text="View" Styles-Style-CssClass="btna btn btn-success  btn-sm">
                            <Image ToolTip="View button here" Url="~/img/icons/white-icn/eyee1.png" Width="15px" />
                        </SelectButton>

                        <EditButton ButtonType="Button" Text="Edit" Styles-Style-CssClass="btna btn btn-info  btn-sm">
                            <Image runat="server" Url="~/img/icons/white-icn/Pen Tool.png" Width="15px"></Image>
                        </EditButton>

                        <DeleteButton ButtonType="Button" Text="Delete" Styles-Style-CssClass="btna btn btn-danger btn-sm btn-danger-variant">
                            <Image runat="server" Url="~/img/icons/white-icn/Trash.png" Width="15px"></Image>
                        </DeleteButton>

                        <CancelButton ButtonType="Button" Text="Close" Styles-Style-CssClass="btnPopup btn btn-primary">
                        </CancelButton>
                        <UpdateButton ButtonType="Button" Text="Update">
                        </UpdateButton>
                    </SettingsCommandButton>
                    <SettingsEditing EditFormColumnCount="2" />
                    <SettingsPopup>
                        <EditForm VerticalAlign="WindowCenter" AllowResize="True" Modal="True" HorizontalAlign="WindowCenter"></EditForm>
                    </SettingsPopup>

                    <SettingsPager Position="Bottom">
                        <PageSizeItemSettings Caption="Records per Page" Items="10, 20, 50, 100" ShowAllItem="true" Position="Right" Visible="true">
                        </PageSizeItemSettings>
                    </SettingsPager>
                    <Styles>
                        <CommandColumn Spacing="0px" Wrap="False" />
                        <EditFormDisplayRow HorizontalAlign="Center" VerticalAlign="Middle"></EditFormDisplayRow>
                        <AlternatingRow CssClass="alterRowGV" Enabled="True"></AlternatingRow>
                        <Row CssClass="rowHeight"></Row>
                    </Styles>
                    <Settings ShowGroupPanel="true" VerticalScrollBarMode="Auto" />
                    <SettingsLoadingPanel Mode="ShowOnStatusBar" />
                    <SettingsBehavior EnableCustomizationWindow="true" AllowDragDrop="true" ConfirmDelete="True" ColumnResizeMode="Control" />
                    <ClientSideEvents ColumnStartDragging="grid_customizationWindowCloseUp" EndCallback="GridOnEndCallBack" />

                    <Templates>
                        <EditForm>
                            <div class="overlay">
                                <div class="FormPopup">
                                    <div class="formHeaderDiv">
                                        <h3>Customer Contract Prices</h3>
                                    </div>
                                    <fieldset>
                                        <legend></legend>
                                        <table>
                                            <tr>
                                                <td>Customer : </td>
                                                <td>
                                                    <div class="DivReq">
                                                        <asp:SqlDataSource runat="server" ID="DS_Customers" SelectCommand="SELECT CUS_NO, CUS_NAME FROM STP_MST_CSTMR" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
                                                        <dx:ASPxGridLookup AutoPostBack="true" OnDataBound="SelectedValueChanged" OnValueChanged="SelectedValueChanged" ID="CustomerCode" ClientInstanceName="ClientCustomerCode" runat="server" TabIndex="1" DataSourceID="DS_Customers" TextFormatString="{0}" Value='<%# Bind("CPR_CUSNO") %>' KeyFieldName="CUS_NO" SelectionMode="Single">
                                                            <Columns>
                                                                <dx:GridViewDataColumn Caption="Code" FieldName="CUS_NO"></dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="Name" FieldName="CUS_NAME"></dx:GridViewDataColumn>
                                                            </Columns>
                                                            <GridViewProperties Settings-ShowFilterRow="true" />
                                                            <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                                <RequiredField IsRequired="true" ErrorText="Customer is Required" />
                                                            </ValidationSettings>
                                                        </dx:ASPxGridLookup>
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Item : </td>
                                                <td>
                                                    <div class="DivReq">
                                                        <asp:SqlDataSource runat="server" ID="DS_Items" SelectCommand="SELECT LTRIM(RTRIM(PRO_CODE)) AS PRO_CODE, PRO_DESC1, PRO_CATG, PRO_UOM, PRO_PRICE FROM INV_MSTR_PRODT" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
                                                        <dx:ASPxGridLookup AutoPostBack="true" OnDataBound="SelectedValueChanged" OnValueChanged="SelectedValueChanged" ID="ItemNo" ClientInstanceName="ClientItemNo" runat="server" TabIndex="1" DataSourceID="DS_Items" TextFormatString="{0}" Value='<%# Bind("CPR_ITMNO") %>' KeyFieldName="PRO_CODE" SelectionMode="Single">
                                                            <Columns>
                                                                <dx:GridViewDataColumn Caption="Code" FieldName="PRO_CODE"></dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="Name" FieldName="PRO_DESC1"></dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="Catg" FieldName="PRO_CATG"></dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="UoM" FieldName="PRO_UOM"></dx:GridViewDataColumn>
                                                                <dx:GridViewDataColumn Caption="Price" FieldName="PRO_PRICE"></dx:GridViewDataColumn>
                                                            </Columns>
                                                            <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                                <RequiredField IsRequired="true" ErrorText="Item is Required" />
                                                            </ValidationSettings>
                                                        </dx:ASPxGridLookup>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr runat="server" id="trw1">
                                                <td>
                                                    <asp:Label ID="lblUom1" runat="server"></asp:Label>
                                                    Price : </td>
                                                <td>
                                                    <div class="DivReq">
                                                        <dx:ASPxTextBox ID="txtPrice1" ClientInstanceName="ClientPrice1" runat="server" MaxLength="13" Width="170px" TabIndex="3">
                                                            <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                                <ErrorFrameStyle Font-Size="Smaller" />
                                                                <RegularExpression ValidationExpression="^[0-9]((\d+)?(.\d+)?)?$" ErrorText="Please enter a valid number." />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr runat="server" id="trw2">
                                                <td>
                                                    <asp:Label ID="lblUom2" runat="server"></asp:Label>
                                                    Price : </td>
                                                <td>
                                                    <div class="DivReq">
                                                        <dx:ASPxTextBox ID="txtPrice2" ClientInstanceName="ClientPrice2" runat="server" MaxLength="13" Width="170px" TabIndex="3">
                                                            <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                                <ErrorFrameStyle Font-Size="Smaller" />
                                                                <RegularExpression ValidationExpression="^[0-9]((\d+)?(.\d+)?)?$" ErrorText="Please enter a valid number." />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr runat="server" id="trw3">
                                                <td>
                                                    <asp:Label ID="lblUom3" runat="server"></asp:Label>
                                                    Price : </td>
                                                <td>
                                                    <div class="DivReq">
                                                        <dx:ASPxTextBox ID="txtPrice3" ClientInstanceName="ClientPrice3" runat="server" MaxLength="13" Width="170px" TabIndex="3">
                                                            <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                                <ErrorFrameStyle Font-Size="Smaller" />
                                                                <RegularExpression ValidationExpression="^[0-9]((\d+)?(.\d+)?)?$" ErrorText="Please enter a valid number." />
                                                            </ValidationSettings>
                                                        </dx:ASPxTextBox>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                    <hr style="color: skyblue" />
                                    <dx:ASPxLabel ID="lblErrorMessage" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
                                    <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                                        <dx:ASPxButton ID="btnAdd1" runat="server" Text="Save" CssClass="btnPopup btn btn-success" CausesValidation="true" OnClick="btnAdd1_Click" />
                                        <dx:ASPxButton ID="btnPopUpClear" runat="server" Text="Cancel" CssClass="btnPopup btn btn-warning" CausesValidation="false" AutoPostBack="false">
                                            <ClientSideEvents Click="function(s, e) { CallBackCode.PerformCallback(CatClientEditCode.GetText()); }" />
                                        </dx:ASPxButton>
                                        <dx:ASPxButton ID="btnPopUpDelete" runat="server" CommandArgument='<%# Eval("CPR_CUSNO") + "," + Eval("CPR_ITMNO") + "," + Eval("CPR_UOM") %>' CommandName="PopUpDelete" Text="Delete" CssClass="btnPopup btn btn-danger btn-danger-variant" CausesValidation="false">
                                            <ClientSideEvents Click="function(s, e) { 
                                                if (!confirm('Confirm Delete?')) { 
                                                    e.processOnServer = false;  
                                                } 
                                        }" />
                                        </dx:ASPxButton>
                                        <dx:ASPxButton ID="btnPopUpPrint" runat="server" Text="Print" CssClass="btnPopup btn btn-info btn-info-variant" AutoPostBack="false" CausesValidation="false">
                                            <ClientSideEvents Click="function(s, e) { printDoc(s, e); }" />
                                        </dx:ASPxButton>
                                        <dx:ASPxGridViewTemplateReplacement ID="btnPopUpClose" ReplacementType="EditFormCancelButton" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </EditForm>
                    </Templates>

                    <Templates>
                        <StatusBar>
                            <table>
                                <tr>
                                    <td>
                                        <%--<dx:ASPxButton ID="btnNewRecord" runat="server" Text="Add New Item" AutoPostBack="false">
                                    <ClientSideEvents Click="function (s, e){ FromGridView.AddNewRow();  }" />
                                </dx:ASPxButton>--%>
                                    </td>
                                    <td>Search: </td>
                                    <td>
                                        <dx:ASPxButtonEdit runat="server" ID="CustomSearchPanelTxtBx" />
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>Search In:</td>
                                    <td>
                                        <dx:ASPxComboBox AutoPostBack="true" ID="DDLFilterByColumn" runat="server" OnSelectedIndexChanged="DDLFilterByColumn_SelectedIndexChanged">
                                            <Items>
                                                <dx:ListEditItem Text="All" Value="*" />
                                            </Items>
                                        </dx:ASPxComboBox>
                                    </td>

                                </tr>
                            </table>
                        </StatusBar>
                    </Templates>

                </dx:ASPxGridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        window.onload = function () {
            $('form').on('click', '.btnPopupClose', function () {
                $('.overlay').hide();
            });
        };

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //if (prm != null) {
        prm.add_endRequest(function (sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.overlay').show();
            }
        });
        prm.add_endRequest(GridOnEndCallBack);
        //};


        function OnTestValidation(s, e) {
            //var testData = e.value;
            //if (!testData) {
            lblError.SetVisible(!e.isValid);
            //  return;
        }

        function CB_categoryCodeLostFocus(s, e) {
            //var RowVals = JSON.parse(e.result);
            //if (RowVals.length > 0) {
            //    CatClientEditName.SetText(RowVals[1]);
            //    CatClientEditDivision.SetValue(RowVals[4]);
            //    CatClientEditSeqNo.SetText(RowVals[5]);
            //    $(CatClientEditImageIMAGE).attr('src', MyBaseUrl_JS + RowVals[3]);
            //    CatClientEditImageOldPath.SetText(RowVals[3]);
            //} else {
            //    CatClientEditName.SetText("");
            //    CatClientEditDivision.SetValue("");
            //    CatClientEditSeqNo.SetText("");
            //    $(CatClientEditImageIMAGE).removeAttr('src');
            //    CatClientEditImageOldPath.SetText("");
            //}
        }
    </script>
</asp:Content>

