﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;

public partial class Admin_Customers_CustShipTo : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    private string EditRoute_SLCTD = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT SHP_CUSNO, SHP_NO, SHP_NAME, SHP_ADDRS, SHP_CITY, SHP_CNTNO, SHP_CNTNM, SHP_ROUTE, CUS_NAME "
            + " FROM STP_MST_SHPTO "
            + " JOIN STP_MST_CSTMR ON SHP_CUSNO = CUS_NO ";
    }

    protected void gvInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string custNo = e.Keys["SHP_CUSNO"].ToString().Trim();
        string shipToNo = e.Keys["SHP_NO"].ToString().Trim();

        dbHlpr.ExecuteNonQuery("DELETE FROM STP_MST_SHPTO WHERE SHP_CUSNO = '" + custNo + "' AND SHP_NO = '" + shipToNo + "' ");
        DataTable dtDlt = dbHlpr.FetchData("SELECT '" + custNo + "' AS TEXT01, '" + shipToNo + "' AS TEXT02 ");
        dbHlpr.CreateDownloadRecord(dtDlt, "ALL", "STP_MST_SHPTO", "DLT");

        e.Cancel = true;
        gvInquery.CancelEdit();
    }

    protected void gvInquery_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        ASPxGridView gv = (ASPxGridView)sender;
        EditRoute_SLCTD = gv.GetRowValues(gv.EditingRowVisibleIndex, "SHP_ROUTE").ToString();
    }

    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxGridLookup lkUpCustomer = ((ASPxGridLookup)gvInquery.FindEditFormTemplateControl("CustomerCode"));

        string custNo = lkUpCustomer.Value == null ? "" : lkUpCustomer.Value.ToString().Trim();

        if (custNo.Length <= 0)
        {
            gvInquery.CancelEdit();
            gvInquery.DataBind();
            return;
        }

        string shipToNo = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtShipToNo")).Text;
        string shipToName = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtShipToName")).Text;
        string shipToAddress = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtShipToAddress")).Text;
        string shipToCity = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtShipToCity")).Text;
        string shipToCntcNo = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtShipToContNo")).Text;
        string shipToCntcNm = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtShipToContNm")).Text;
        ASPxComboBox cmbxShipToRoute = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxRoute"));
        string shipToRoute = cmbxShipToRoute.Value == null ? "" : cmbxShipToRoute.Value.ToString();


        if (true) // Validation to be added later
        {
            string oldShipToRoute = "";
            DataTable exstngRoute = dbHlpr.FetchData("SELECT SHP_ROUTE FROM STP_MST_SHPTO WHERE SHP_CUSNO = '" + custNo + "' AND SHP_NO = '" + shipToNo + "' ");
            if (exstngRoute.Rows.Count > 0)
            {
                oldShipToRoute = exstngRoute.Rows[0]["SHP_ROUTE"].ToString();
            }
            dbHlpr.ExecuteNonQuery("DELETE FROM STP_MST_SHPTO WHERE SHP_CUSNO = '" + custNo + "' AND SHP_NO = '" + shipToNo + "' ");

            if (oldShipToRoute.Length > 0)
            {
                DataTable dtDlt = dbHlpr.FetchData("SELECT '" + custNo + "' AS TEXT01, '" + shipToNo + "' AS TEXT02 ");
                dbHlpr.CreateDownloadRecord(dtDlt, oldShipToRoute, "STP_MST_SHPTO", "DLT");
            }

            dbHlpr.ExecuteNonQuery("INSERT INTO STP_MST_SHPTO ( "
                + " SHP_CUSNO, SHP_NO, SHP_NAME, "
                + " SHP_ADDRS, SHP_CITY, SHP_CNTNO, "
                + " SHP_CNTNM, SHP_ROUTE "
                + " ) VALUES ( "
                + " '" + custNo + "', '" + shipToNo + "', '" + shipToName + "', "
                + " '" + shipToAddress + "', '" + shipToCity + "', '" + shipToCntcNo + "', "
                + " '" + shipToCntcNm + "', '" + shipToRoute + "' "
                + " )");
            DataTable dt = dbHlpr.FetchData("SELECT "
                    + " SHP_CUSNO AS TEXT01, SHP_NO AS TEXT02, SHP_NAME AS TEXT03, SHP_ADDRS AS TEXT04, "
                    + " SHP_CITY AS TEXT05, SHP_CNTNO AS TEXT06, SHP_CNTNM AS TEXT07, SHP_ROUTE AS TEXT08 "
                    + " FROM STP_MST_SHPTO "
                    + " WHERE SHP_CUSNO = '" + custNo + "' AND SHP_NO = '" + shipToNo + "' ");
            dbHlpr.CreateDownloadRecord(dt, shipToRoute, "STP_MST_SHPTO", "CRT");
        }

        gvInquery.CancelEdit();
        gvInquery.DataBind();
    }
    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }
    protected void cmbxRoute_Init(object sender, EventArgs e)
    {
        ASPxComboBox cmbxRoute = (ASPxComboBox)sender;

        cmbxRoute.DataSource = dbHlpr.FetchData("SELECT SRP_CODE, SRP_NAME, SRP_CMNTS FROM STP_MSTR_SLREP");
        cmbxRoute.ValueField = "SRP_CODE";
        cmbxRoute.TextField = "SRP_NAME";
        cmbxRoute.DataBind();
        cmbxRoute.Value = EditRoute_SLCTD;
    }
    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

    }
    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        {
            string args = e.CommandArgs.CommandArgument.ToString();
            string[] prms = args.Split(',');

            string custNo = prms[0];
            string shipToNo = prms[1];

            dbHlpr.ExecuteNonQuery("DELETE FROM STP_MST_SHPTO WHERE SHP_CUSNO = '" + custNo + "' AND SHP_NO = '" + shipToNo + "' ");
            DataTable dtDlt = dbHlpr.FetchData("SELECT '" + custNo + "' AS TEXT01, '" + shipToNo + "' AS TEXT02 ");
            dbHlpr.CreateDownloadRecord(dtDlt, "ALL", "STP_MST_SHPTO", "DLT");

            gvInquery.CancelEdit();
            gvInquery.DataBind();
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }

    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }
}