﻿using DevExpress.Web;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Customers_Default : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT "
            + " CUS_NO, CUS_NAME, CUS_ADDR1, CUS_ADDR2, CUS_CITY, "
            + " CUS_CNTC1, CUS_CNTC2, CUS_PHON1, CUS_PHON2, "
            + " CUS_EMAIL, CUS_SLSRP, CUS_TYPE, CUS_CATG, "
            + " CUS_VATNO, CUS_TXABL, CUS_GRPCD, CUS_NAMAR, CUS_STATS, CUS_GEOLA, CUS_GEOLN "
            + " FROM STP_MST_CSTMR "
            + " UNION ALL "
            + " SELECT "
            + " CUS_NO, CUS_NAME, CUS_ADDR1, CUS_ADDR2, CUS_CITY, "
            + " CUS_CNTC1, CUS_CNTC2, CUS_PHON1, CUS_PHON2, "
            + " CUS_EMAIL, CUS_SLSRP, CUS_TYPE, CUS_CATG, "
            + " CUS_VATNO, CUS_TXABL, CUS_GRPCD, CUS_NAMAR, CUS_STATS, CUS_GEOLA, CUS_GEOLN "
            + " FROM STP_MST_DLCUS ";

        if (!Convert.ToBoolean(Session["userEditMastersAccess"]))
        {
            gvCustomerInquery.Columns[0].Visible = false;
            gvCustomerInquery.Columns[1].Visible = false;
            gvCustomerInquery.Columns[2].Visible = false;
        }
    }

    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxTextBox txtBxCode = ((ASPxTextBox)gvCustomerInquery.FindEditFormTemplateControl("txtCode"));
        string CCode = "0";
        //  string CatTyp = "0";
        if (txtBxCode.Text.Length > 0)
        {
            CCode = txtBxCode.Text.Trim();
        }

        string CName = ((ASPxTextBox)gvCustomerInquery.FindEditFormTemplateControl("txtName")).Text;
        string CAabicName = ((ASPxTextBox)gvCustomerInquery.FindEditFormTemplateControl("txtArabicName")).Text;
        string CCity = ((ASPxTextBox)gvCustomerInquery.FindEditFormTemplateControl("txtCity")).Text;
        string CAddress1 = ((ASPxTextBox)gvCustomerInquery.FindEditFormTemplateControl("txtAddress1")).Text;
        string CAddress2 = ((ASPxTextBox)gvCustomerInquery.FindEditFormTemplateControl("txtAddress2")).Text;
        string CContact1 = ((ASPxTextBox)gvCustomerInquery.FindEditFormTemplateControl("txtContact1")).Text;
        string CContact2 = ((ASPxTextBox)gvCustomerInquery.FindEditFormTemplateControl("txtContact2")).Text;

        string CPhone1 = ((ASPxTextBox)gvCustomerInquery.FindEditFormTemplateControl("txtPhone1")).Text;
        string CPhone2 = ((ASPxTextBox)gvCustomerInquery.FindEditFormTemplateControl("txtPhone2")).Text;
        string CEmail = ((ASPxTextBox)gvCustomerInquery.FindEditFormTemplateControl("txtEmail")).Text;
        string CVATNo = ((ASPxTextBox)gvCustomerInquery.FindEditFormTemplateControl("txtVatNo")).Text;

        ASPxComboBox cbxSlsRep = ((ASPxComboBox)gvCustomerInquery.FindEditFormTemplateControl("cmbxSLSRP"));
        ASPxComboBox cbxType = ((ASPxComboBox)gvCustomerInquery.FindEditFormTemplateControl("cmBxType"));
        ASPxComboBox cbxCatg = ((ASPxComboBox)gvCustomerInquery.FindEditFormTemplateControl("cmbxCatg"));
        ASPxComboBox cbxTaxable = ((ASPxComboBox)gvCustomerInquery.FindEditFormTemplateControl("cmbxTaxable"));
        ASPxComboBox cbxCusGrp = ((ASPxComboBox)gvCustomerInquery.FindEditFormTemplateControl("cmbxCusGroup"));
        ASPxComboBox cbxStatus = ((ASPxComboBox)gvCustomerInquery.FindEditFormTemplateControl("cmbxStatus"));
        ASPxDropDownEdit ddRoutes = ((ASPxDropDownEdit)gvCustomerInquery.FindEditFormTemplateControl("AssgndRouteNo"));

        string CSlsREP = cbxSlsRep.SelectedItem != null ? cbxSlsRep.SelectedItem.Value.ToString() : cbxSlsRep.Text;
        string CType = cbxType.SelectedItem != null ? cbxType.SelectedItem.Value.ToString() : cbxType.Text;
        string CCtag = cbxCatg.SelectedItem != null ? cbxCatg.SelectedItem.Value.ToString() : cbxCatg.Text;
        string CTaxable = cbxTaxable.SelectedItem != null ? cbxTaxable.SelectedItem.Value.ToString() : cbxTaxable.Text;
        string CGrpCode = cbxCusGrp.SelectedItem != null ? cbxCusGrp.SelectedItem.Value.ToString() : cbxCusGrp.Text;
        string CAssgndRoutes = ddRoutes.Value != null ? ddRoutes.Value.ToString() : "";
        string CStatus = cbxStatus.SelectedItem != null ? cbxStatus.SelectedItem.Value.ToString() : cbxStatus.Text;

        //string CName = ((ASPxTextBox)gvCustomerInquery.FindEditFormTemplateControl("CatEditName")).Text;
        //string CName = ((ASPxTextBox)gvCustomerInquery.FindEditFormTemplateControl("CatEditName")).Text;
        //string CName = ((ASPxTextBox)gvCustomerInquery.FindEditFormTemplateControl("CatEditName")).Text;

        DataTable myDT = new DataTable();

        myDT = myDT = dbHlpr.FetchData("SELECT * FROM STP_MST_CSTMR WHERE CUS_NO = '" + CCode + "'");
        if (myDT.Rows.Count <= 0)
        {

            dbHlpr.ExecuteNonQuery("INSERT INTO STP_MST_CSTMR ( "
                + " CUS_NO, CUS_NAME, CUS_NAMAR, CUS_CITY, CUS_ADDR1, CUS_ADDR2, CUS_CNTC1, CUS_CNTC2, CUS_PHON1, CUS_PHON2, "
                + " CUS_EMAIL, CUS_SLSRP, CUS_TYPE, CUS_CATG, CUS_VATNO, CUS_TXABL, CUS_GRPCD, CUS_STATS "
                + " ) VALUES ( "
                + " '" + CCode + "',  '" + CName + "',  N'" + CAabicName + "', '" + CCity + "', '" + CAddress1 + "',  '" + CAddress2 + "',  '" + CContact1 + "',  '" + CContact2 + "'"
            + " ,  '" + CPhone1 + "',  '" + CPhone2 + "',  '" + CEmail + "',  '" + CSlsREP + "' ,  '" + CType + "',  '" + CCtag + "',  '" + CVATNo + "'"
            + ",  '" + CTaxable + "',  '" + CGrpCode + "',  '" + CStatus + "')");

            // Assign Routes
            this.AssignRoutes(CCode, CSlsREP, CAssgndRoutes, CStatus);
        }
        else
        {
            dbHlpr.ExecuteNonQuery("UPDATE [STP_MST_CSTMR] SET "
            + "  CUS_NAME = '" + CName + "' "
            + " ,CUS_NAMAR = N'" + CAabicName + "' "
            + " ,CUS_CITY = '" + CCity + "' "
            + " ,CUS_ADDR1 = '" + CAddress1 + "' "
            + " ,CUS_ADDR2 = '" + CAddress2 + "' "
            + " ,CUS_CNTC1 = '" + CContact1 + "' "
            + " ,CUS_CNTC2 = '" + CContact2 + "' "
            + " ,CUS_PHON1 = '" + CPhone1 + "' "
            + " ,CUS_PHON2 = '" + CPhone2 + "' "
            + " ,CUS_EMAIL = '" + CEmail + "' "
            + " ,CUS_SLSRP = '" + CSlsREP + "' "
            + " ,CUS_TYPE = '" + CType + "' "
            + " ,CUS_CATG = '" + CCtag + "' "
            + " ,CUS_VATNO = '" + CVATNo + "' "
            + " ,CUS_TXABL = '" + CTaxable + "' "
            + " ,CUS_GRPCD = '" + CGrpCode + "' "
            + " ,CUS_STATS = '" + CStatus + "' "

            + " WHERE CUS_NO = '" + CCode + "'");

            // Assign Routes
            this.AssignRoutes(CCode, CSlsREP, CAssgndRoutes, CStatus);
        }

        dbHlpr.ExecuteNonQuery("DELETE FROM STP_MST_DLCUS WHERE CUS_NO = '" + CCode + "' ");
        if (!CStatus.ToLower().Equals("active")) // if "NOT" Active
        {
            dbHlpr.ExecuteNonQuery("INSERT INTO STP_MST_DLCUS SELECT * FROM STP_MST_CSTMR WHERE CUS_NO = '" + CCode + "' ");
            dbHlpr.ExecuteNonQuery("DELETE FROM STP_MST_CSTMR WHERE CUS_NO = '" + CCode + "' ");
        }

        gvCustomerInquery.CancelEdit();
        gvCustomerInquery.DataBind();
    }

    private void AssignRoutes(string CustCode, string MainSlsRep, string AssignedRoutes, string CustomerStatus)
    {
        dbHlpr.ExecuteNonQuery("DELETE FROM STP_LNK_RTCUS WHERE RTC_CUSNO = '" + CustCode + "' ");
        DataTable dtDlt = dbHlpr.FetchData("SELECT "
                    + " CUS_NO AS TEXT01, CUS_NAME AS TEXT02, CUS_ADDR1 AS TEXT03, CUS_ADDR2 AS TEXT04, "
                    + " CUS_CITY AS TEXT05, CUS_CNTC1 AS TEXT06, CUS_CNTC2 AS TEXT07, CUS_PHON1 AS TEXT08, "
                    + " CUS_PHON2 AS TEXT09, CUS_EMAIL AS TEXT10, CUS_SLSRP AS TEXT11, CUS_TYPE AS TEXT12, "
                    + " CUS_CATG AS TEXT13, CUS_VATNO AS TEXT14, CUS_TXABL AS TEXT15, CUS_GRPCD AS TEXT16, "
                    + " CUS_NAMAR AS TEXT17, CUS_GEOLA AS TEXT18, CUS_GEOLN AS TEXT19, CUS_BALNC AS NUM01 "
                    + " FROM STP_MST_CSTMR WHERE CUS_NO = '" + CustCode + "' ");
        dbHlpr.CreateDownloadRecord(dtDlt, "ALL", "STP_MST_CSTMR", "DLT");

        if (!CustomerStatus.ToLower().Equals("active"))
        {
            return;
        }

        string mainKey = MainSlsRep + CustCode;
        dbHlpr.ExecuteNonQuery("INSERT INTO STP_LNK_RTCUS (RTC_KEY, RTC_ROUTE, RTC_CUSNO "
            + " ) VALUES ( "
            + " '" + mainKey + "','" + MainSlsRep + "','" + CustCode + "') ");
        DataTable dt = dbHlpr.FetchData("SELECT "
            + " CUS_NO AS TEXT01, CUS_NAME AS TEXT02, CUS_ADDR1 AS TEXT03, CUS_ADDR2 AS TEXT04, "
            + " CUS_CITY AS TEXT05, CUS_CNTC1 AS TEXT06, CUS_CNTC2 AS TEXT07, CUS_PHON1 AS TEXT08, "
            + " CUS_PHON2 AS TEXT09, CUS_EMAIL AS TEXT10, CUS_SLSRP AS TEXT11, CUS_TYPE AS TEXT12, "
            + " CUS_CATG AS TEXT13, CUS_VATNO AS TEXT14, CUS_TXABL AS TEXT15, CUS_GRPCD AS TEXT16, "
            + " CUS_NAMAR AS TEXT17, CUS_GEOLA AS TEXT18, CUS_GEOLN AS TEXT19, CUS_BALNC AS NUM01 "
            + " FROM STP_MST_CSTMR WHERE CUS_NO = '" + CustCode + "' ");
        dbHlpr.CreateDownloadRecord(dt, MainSlsRep, "STP_MST_CSTMR", "CRT");

        if (AssignedRoutes.Trim().Length > 0)
        {
            string[] assgndRoutesArr = AssignedRoutes.Trim().Split(',');
            foreach (string routeNo in assgndRoutesArr)
            {
                string key = (routeNo + CustCode);
                dbHlpr.ExecuteNonQuery("DELETE FROM STP_LNK_RTCUS WHERE RTC_KEY = '" + key + "' ");
                dbHlpr.ExecuteNonQuery("INSERT INTO STP_LNK_RTCUS (RTC_KEY, RTC_ROUTE, RTC_CUSNO "
                    + " ) VALUES ( "
                    + " '" + key + "','" + routeNo + "','" + CustCode + "') ");

                if (routeNo != MainSlsRep)
                {
                    DataTable dt1 = dbHlpr.FetchData("SELECT "
                        + " CUS_NO AS TEXT01, CUS_NAME AS TEXT02, CUS_ADDR1 AS TEXT03, CUS_ADDR2 AS TEXT04, "
                        + " CUS_CITY AS TEXT05, CUS_CNTC1 AS TEXT06, CUS_CNTC2 AS TEXT07, CUS_PHON1 AS TEXT08, "
                        + " CUS_PHON2 AS TEXT09, CUS_EMAIL AS TEXT10, CUS_SLSRP AS TEXT11, CUS_TYPE AS TEXT12, "
                        + " CUS_CATG AS TEXT13, CUS_VATNO AS TEXT14, CUS_TXABL AS TEXT15, CUS_GRPCD AS TEXT16, "
                        + " CUS_NAMAR AS TEXT17, CUS_GEOLA AS TEXT18, CUS_GEOLN AS TEXT19, CUS_BALNC AS NUM01 "
                        + " FROM STP_MST_CSTMR WHERE CUS_NO = '" + CustCode + "' ");
                    dbHlpr.CreateDownloadRecord(dt1, routeNo, "STP_MST_CSTMR", "CRT");
                }
            }
        }
    }//end method

    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }//end method


    protected void gvCustomerInquery_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }//end method

    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }//end method

    protected void ClBk_CodeLostFocus_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (e.Parameter.Length > 0)
        {
            DataTable myDT = new DataTable();
            myDT = dbHlpr.FetchData("SELECT * FROM STP_MST_CSTMR WHERE CUS_NO = '" + e.Parameter.ToString() + "' ");

            myDT.Columns.Add("ASSGND_ROUTES");
            DataTable dtAssgndRoutes = dbHlpr.FetchData("SELECT RTC_ROUTE FROM STP_LNK_RTCUS WHERE RTC_CUSNO = '" + e.Parameter.ToString() + "' ");
            if (myDT.Rows.Count > 0)
            {
                myDT.Rows[0]["ASSGND_ROUTES"] = "";
                foreach (DataRow dtRw in dtAssgndRoutes.Rows)
                {
                    myDT.Rows[0]["ASSGND_ROUTES"] += dtRw["RTC_ROUTE"].ToString() + ",";
                }
                myDT.Rows[0]["ASSGND_ROUTES"] = myDT.Rows[0]["ASSGND_ROUTES"].ToString().Trim().Trim(',').Trim();
            }

            e.Result = JsonConvert.SerializeObject(myDT);
        }
    }

    protected void gvCustomerInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string Code = e.Keys["CUS_NO"].ToString().Trim();

        dbHlpr.ExecuteNonQuery("DELETE FROM [STP_MST_CSTMR] WHERE CUS_NO = '" + Code + "'");

        DataTable dtDlt = dbHlpr.FetchData("SELECT '" + Code + "' AS TEXT01 ");
        dbHlpr.CreateDownloadRecord(dtDlt, "ALL", "STP_MST_CSTMR", "DLT");

        e.Cancel = true;
        gvCustomerInquery.CancelEdit();
    }//end method

    protected void gvCustomerInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            int cstId;
            if (int.TryParse(e.CommandArgs.CommandArgument.ToString(), out cstId)) // Code is sent via request
            {
                dbHlpr.ExecuteNonQuery("DELETE FROM STP_MST_CSTMR WHERE CUS_NO = '" + cstId + "'");

                DataTable dtDlt = dbHlpr.FetchData("SELECT '" + cstId + "' AS TEXT01 ");
                dbHlpr.CreateDownloadRecord(dtDlt, "ALL", "STP_MST_CSTMR", "DLT");

                gvCustomerInquery.CancelEdit();
                gvCustomerInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvCustomerInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid Customer ID. Unable to delete Customer.";
            }
        }
    }//end method

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvCustomerInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvCustomerInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvCustomerInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvCustomerInquery.SearchPanelFilter = txtFilter.Text;
        gvCustomerInquery.DataBind();
    }//end method

    protected void gvCustomerInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvCustomerInquery, CBX_filter);
    }//end method

    protected void gvCustomerInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvCustomerInquery, CBX_filter);
    }//end method

    protected void gvCustomerInquery_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
    }//end method

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }//end method

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvCustomerInquery);
    }//end method

    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }//end method

    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvCustomerInquery);
    }//end method

    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }//end method

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }//end method
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }//end method
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

        ////html export here
        //gridExport.ExportHtmlToResponse();
        //string str;
        //MemoryStream ms = new MemoryStream();
        //try
        //{
        //    gridExport.ExportToHtml(ms);
        //    ms.Seek(0, SeekOrigin.Begin);
        //    StreamReader sr = new StreamReader(ms);
        //    str = sr.ReadToEnd();
        //}
        //finally
        //{
        //    ms.Close();
        //}

    }//end method

    protected void txtCode_Init(object sender, EventArgs e)
    {
        if (!gvCustomerInquery.IsNewRowEditing)
        {
            ASPxTextBox txtCode = (ASPxTextBox)sender;
            txtCode.ReadOnly = true;
        }
        else
        {
            ASPxTextBox txtCode1 = (ASPxTextBox)sender;
            txtCode1.ReadOnly = false;
        }
    }

    protected void AssgndRouteNo_Init(object sender, EventArgs e)
    {
        ASPxDropDownEdit ddRoutes = (ASPxDropDownEdit)sender;
        ASPxListBox lstBxRoutes = (ASPxListBox)ddRoutes.FindControl("lstBxRouteItems");

        DataTable dtRoutes = dbHlpr.FetchData("SELECT SRP_CODE, SRP_NAME FROM STP_MSTR_SLREP ORDER BY SRP_NAME ");

        string CustNo = "";
        if (!gvCustomerInquery.IsNewRowEditing)
        {
            CustNo = gvCustomerInquery.GetRowValues(gvCustomerInquery.EditingRowVisibleIndex, "CUS_NO").ToString();
        }
        DataTable assignedRoutes = dbHlpr.FetchData("SELECT RTC_ROUTE FROM STP_LNK_RTCUS WHERE RTC_CUSNO = '" + CustNo + "' ");
        string tempStr = "";
        for (int i = 0; i < assignedRoutes.Rows.Count; i++)
        {
            tempStr += assignedRoutes.Rows[i]["RTC_ROUTE"].ToString() + ",";
        }
        tempStr = tempStr.Trim(',');

        DataRow dr = dtRoutes.NewRow();
        dr["SRP_CODE"] = "0";
        dr["SRP_NAME"] = "All";
        dtRoutes.Rows.InsertAt(dr, 0);

        lstBxRoutes.DataSource = dtRoutes;
        lstBxRoutes.ValueField = "SRP_CODE";
        lstBxRoutes.TextField = "SRP_NAME";
        lstBxRoutes.DataBind();

        ddRoutes.Value = tempStr;
    }
}//end class