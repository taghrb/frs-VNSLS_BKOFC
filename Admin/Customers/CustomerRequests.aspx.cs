﻿using DevExpress.Web;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Customers_CustomerRequests : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT "
            + " CUS_NO, CUS_NAME, CUS_ADDR1, CUS_ADDR2, CUS_CITY, "
            + " CUS_CNTC1, CUS_CNTC2, CUS_PHON1, CUS_PHON2, "
            + " CUS_EMAIL, CUS_SLSRP, CUS_TYPE, CUS_CATG, "
            + " CUS_VATNO, CUS_TXABL, CUS_GRPCD, CUS_NAMAR, "
            + " CUS_GEOLA, CUS_GEOLN, CUS_CPCOD "
            + " FROM CUS_TMP_CREAT ";
    }

    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }//end method


    protected void gvCustomerInquery_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }//end method

    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }//end method

    protected void gvCustomerInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string Code = e.Keys["CUS_NO"].ToString().Trim();

        dbHlpr.ExecuteNonQuery("DELETE FROM [CUS_TMP_CREAT] WHERE CUS_NO = '" + Code + "'");

        e.Cancel = true;
        gvCustomerInquery.CancelEdit();
    }//end method

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvCustomerInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvCustomerInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvCustomerInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvCustomerInquery.SearchPanelFilter = txtFilter.Text;
        gvCustomerInquery.DataBind();
    }//end method

    protected void gvCustomerInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvCustomerInquery, CBX_filter);
    }//end method

    protected void gvCustomerInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvCustomerInquery, CBX_filter);
    }//end method

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }//end method

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        string filename = "VS_CustRequests_" + DateTime.Now.ToString("ddMMMyyyy$HH:mm:ss") + ".xls";

        var exportOptions = new XlsxExportOptionsEx();
        exportOptions.ShowGridLines = false;
        exportOptions.ExportType = DevExpress.Export.ExportType.WYSIWYG;
        //exportOptions.CustomizeCell += exportOptions_CustomizeCell;
        gridExport.Styles.Cell.Wrap = DevExpress.Utils.DefaultBoolean.False;
        gridExport.Styles.Default.Wrap = DevExpress.Utils.DefaultBoolean.False;
        gridExport.Styles.Header.Wrap = DevExpress.Utils.DefaultBoolean.False;
        gridExport.WriteXlsxToResponse(filename, exportOptions);
    }//end method

    protected void gridExport_RenderBrick(object sender, ASPxGridViewExportRenderingEventArgs e)
    {
        e.BrickStyle.BorderWidth = 1;
        e.BrickStyle.BorderColor = Color.Black;

        if (e.RowType == GridViewRowType.Header)
        {
            e.BrickStyle.BackColor = Color.FromArgb(198, 89, 17);
            e.BrickStyle.ForeColor = Color.White;
            e.BrickStyle.Font = new Font("Segoe UI", 12, FontStyle.Bold);
        }
        else if (e.RowType == GridViewRowType.Data)
        {
            e.BrickStyle.Font = new Font("Segoe UI", 12, FontStyle.Regular);
            if (e.VisibleIndex % 2 == 0)
            {
                e.BrickStyle.BackColor = Color.FromArgb(221, 235, 247);
            }
            else
            {
                e.BrickStyle.BackColor = Color.White;
            }
        }
        else
        {
            e.BrickStyle.BorderWidth = 0;
            e.BrickStyle.BorderColor = Color.Transparent;
        }
    }

    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }//end method

    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvCustomerInquery);
    }//end method

    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }//end method

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }//end method
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }//end method
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

    }//end method

}//end class