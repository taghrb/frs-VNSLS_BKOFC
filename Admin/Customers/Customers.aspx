﻿<%@ Page Title="Manage Customers" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="Customers.aspx.cs" Inherits="Admin_Customers_Default" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content4" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <script type="text/javascript">

        function grid_customizationWindowCloseUp(s, e) {
            //if(FromGridView.IsCustomizationWindowVisible())
            FromGridView.ShowCustomizationWindow();
        }

        // Code to create a custom multiselect ComboBox, Dropdown
        var textSeparator = ","; // Between each checked item in ComboBox
        function OnListBoxSelectionChanged(listBox, args) {
            if (args.index == 0)
                args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
            if (listBox.name.indexOf("lstBxRouteItems") != -1) {
                IsAllSelected(checkRouteListBox) ? checkRouteListBox.SelectIndices([0]) : checkRouteListBox.UnselectIndices([0]);
                UpdateText(DraClientRouteNo, checkRouteListBox);
            }
        }
        function IsAllSelected(chkLstBx) {
            var selectedDataItemCount = chkLstBx.GetItemCount() - (chkLstBx.GetItem(0).selected ? 0 : 1);
            return chkLstBx.GetSelectedItems().length == selectedDataItemCount;
        }
        function UpdateText(ddObj, lstBxObj) {
            var selectedItems = lstBxObj.GetSelectedItems();
            ddObj.SetText(GetSelectedItemsText(selectedItems));
        }
        function SynchronizeListBoxValues(dropDown, args) {
            var texts = dropDown.GetText().split(textSeparator);
            if (dropDown.name.indexOf("AssgndRouteNo") != -1) {
                checkRouteListBox.UnselectAll();
                checkRouteListBox.SelectValues(texts);
                IsAllSelected(checkRouteListBox) ? checkRouteListBox.SelectIndices([0]) : checkRouteListBox.UnselectIndices([0]);
                UpdateText(DraClientRouteNo, checkRouteListBox);
            }
        }
        function GetSelectedItemsText(items) {
            var texts = [];
            for (var i = 0; i < items.length; i++)
                if (items[i].index != 0)
                    texts.push(items[i].value);
            return texts.join(textSeparator);
        }
        function GetValuesByTexts(texts, chkLstBx) {
            var actualValues = [];
            var item;
            for (var i = 0; i < texts.length; i++) {
                item = chkLstBx.FindItemByText(texts[i]);
                if (item != null)
                    actualValues.push(item.value);
            }
            return actualValues;
        }

    </script>

</asp:Content>
<asp:Content ID="contentTopHere1" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
            <li>
                <asp:LinkButton ID="LinkBtnPDF" OnClick="LinkBtnPDF_Click" runat="server">
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonXLX" OnClick="LinkButtonXLX_Click" runat="server">
                <img src="~/img/FilesIcons/excel.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonXxls" OnClick="LinkButtonXxls_Click" runat="server">
                <img src="~/img/FilesIcons/xlsx.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonRtf" OnClick="LinkButtonRtf_Click" runat="server">
                <img src="~/img/FilesIcons/rtf.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonCvs" OnClick="LinkButtonCvs_Click" runat="server">
                <img src="~/img/FilesIcons/cvs.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonWord" OnClick="LinkButtonRtf_Click" runat="server">
                <img src="~/img/FilesIcons/word.png" runat="server" />
                </asp:LinkButton>
            </li>

            <li>
                <asp:LinkButton ID="LinkButtonHtml" OnClick="LinkButtonHtml_Click" runat="server">
                <img src="~/img/icons/searchtop.jpg" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton6" OnClick="LinkButton3_Click" runat="server">               
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton7" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/word.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton8" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/excel.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton9" OnClick="LinkButton3_Click" OnClientClick="window.print();" runat="server">
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton10" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/torrent.png" runat="server" />
                </asp:LinkButton>
            </li>
        </ul>
        <%--<span>Search:
                                   <asp:TextBox ID="txtSearchContent" runat="server" placeholder="Search.." ToolTip="Enter Text to search at Content Page"></asp:TextBox>
                            </span>--%>

        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvCustomerInquery" ExportedRowType="All"></dx:ASPxGridViewExporter>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">

    <div class="formDiv">
        <div class="formHeaderDiv">
            <h3>Customers</h3>
        </div>


        <asp:SqlDataSource runat="server" ID="DxGridSqlDataSource1"
            ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
        <dx:ASPxGridView ID="gvCustomerInquery" CssClass="FromGridView" ClientInstanceName="FromGridView" Theme="Office2010Black" runat="server" AutoGenerateColumns="False" KeyFieldName="CUS_NO"
            DataSourceID="DxGridSqlDataSource1" OnCustomButtonCallback="gvCustomerInquery_CustomButtonCallback"
            OnRowCommand="gvCustomerInquery_RowCommand"
            OnDataBound="gvCustomerInquery_DataBound"
            OnBeforeGetCallbackResult="gvCustomerInquery_BeforeGetCallbackResult"
            OnCustomColumnDisplayText="gvCustomerInquery_CustomColumnDisplayText"
            OnRowDeleting="gvCustomerInquery_RowDeleting">

            <SettingsContextMenu Enabled="True" EnableColumnMenu="True" EnableRowMenu="True"></SettingsContextMenu>
            <SettingsSearchPanel Visible="False" CustomEditorID="CustomSearchPanelTxtBx" />
            <Columns>
                <dx:GridViewCommandColumn ShowNewButtonInHeader="false" ShowSelectButton="true" Width="110" Caption=" " AllowDragDrop="False" VisibleIndex="1"></dx:GridViewCommandColumn>
                <dx:GridViewCommandColumn ShowNewButtonInHeader="true" ShowEditButton="true" Width="110" VisibleIndex="2" AllowDragDrop="False"></dx:GridViewCommandColumn>
                <dx:GridViewCommandColumn ShowNewButtonInHeader="false" ShowDeleteButton="true" Width="130" Caption=" " AllowDragDrop="False" VisibleIndex="3"></dx:GridViewCommandColumn>

                <dx:GridViewDataTextColumn FieldName="CUS_NO" Caption="Code" VisibleIndex="4">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                    <EditFormSettings VisibleIndex="0" />
                </dx:GridViewDataTextColumn>
                <%--<dx:GridViewDataTextColumn FieldName="CUS_STATS" Caption="Status" VisibleIndex="5">
                    <Settings FilterMode="DisplayText" SortMode="DisplayText" AllowAutoFilter="True" AllowHeaderFilter="True" AllowGroup="True" AllowSort="True" />
                    <DataItemTemplate>
                        <dx:ASPxLabel ID="CustomerStatus" runat="server" Text='<%# Eval("CUS_STATS") %>' ClientInstanceName="CustomerStatusClient">
                            <ClientSideEvents Init="function(s, e) { setCustomerStatus(s, e); }" />
                        </dx:ASPxLabel>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>--%>
                <dx:GridViewDataTextColumn FieldName="CUS_NAME" Caption="Name" VisibleIndex="6">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CUS_NAMAR" Caption="Arabic Name" VisibleIndex="7" Visible="true">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <%-- <dx:GridViewDataTextColumn FieldName="CUS_ANAME" Caption="Arabic Name" VisibleIndex="7">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>--%>
                <dx:GridViewDataTextColumn FieldName="CUS_ADDR1" Caption="Address 1" VisibleIndex="8">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CUS_ADDR2" Caption="Address 2" VisibleIndex="9">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CUS_CITY" Caption="City" VisibleIndex="9" Visible="false">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CUS_CNTC1" Caption="Contact 1" VisibleIndex="10">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CUS_CNTC2" Caption="Contact 2" VisibleIndex="11">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CUS_PHON1" Caption="Phone 1" VisibleIndex="12">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CUS_CNTC2" Caption="Phone 2" VisibleIndex="13">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CUS_EMAIL" Caption="Email" VisibleIndex="14">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CUS_SLSRP" Caption="Sales Rep" VisibleIndex="15">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn FieldName="CUS_TYPE" Caption="TYPE" VisibleIndex="16">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CUS_CATG" Caption="Catg" VisibleIndex="17">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CUS_VATNO" Caption="VAT #" VisibleIndex="18" Visible="true">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CUS_TXABL" Caption="Taxable" VisibleIndex="19" Visible="true">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CUS_GRPCD" Caption="Group Code" VisibleIndex="20" Visible="true">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CUS_GEOLA" Caption="Latitude" VisibleIndex="21" Visible="true">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CUS_GEOLN" Caption="Longitude" VisibleIndex="22" Visible="true">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>



                <%-- <dx:GridViewDataTextColumn FieldName="CUS_NOTES" Caption="Customer Notes" VisibleIndex="13" Visible="false">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>--%>
            </Columns>

            <Settings HorizontalScrollBarMode="Auto" />

            <SettingsCommandButton>
                <NewButton ButtonType="Button" Text="New" Styles-Style-CssClass="btna btn btn-primary  btn-sm">
                    <Image runat="server" Url="~/img/icons/white-icn/Add.png" Width="15px"></Image>
                </NewButton>

                <SelectButton ButtonType="Button" Text="View" Styles-Style-CssClass="btna btn btn-success  btn-sm">
                    <Image ToolTip="View button here" Url="~/img/icons/white-icn/eyee1.png" Width="15px" />
                </SelectButton>

                <EditButton ButtonType="Button" Text="Edit" Styles-Style-CssClass="btna btn btn-info  btn-sm">
                    <Image runat="server" Url="~/img/icons/white-icn/Pen Tool.png" Width="15px"></Image>
                </EditButton>

                <DeleteButton ButtonType="Button" Text="Delete" Styles-Style-CssClass="btna btn btn-danger btn-sm btn-danger-variant">
                    <Image runat="server" Url="~/img/icons/white-icn/Trash.png" Width="15px"></Image>
                </DeleteButton>

                <CancelButton ButtonType="Button" Text="Close" Styles-Style-CssClass="btnPopup btn btn-primary">
                </CancelButton>
                <UpdateButton ButtonType="Button" Text="Update">
                </UpdateButton>
            </SettingsCommandButton>
            <SettingsEditing EditFormColumnCount="2" />
            <SettingsPopup>
                <EditForm VerticalAlign="WindowCenter" AllowResize="True" Modal="True" HorizontalAlign="WindowCenter"></EditForm>
            </SettingsPopup>

            <SettingsPager Position="Bottom">
                <PageSizeItemSettings Caption="Records per Page" Items="10, 20, 50, 100" ShowAllItem="true" Position="Right" Visible="true">
                </PageSizeItemSettings>
            </SettingsPager>
            <Styles>
                <CommandColumn Spacing="0px" Wrap="False" />
                <EditFormDisplayRow HorizontalAlign="Center" VerticalAlign="Middle"></EditFormDisplayRow>
                <AlternatingRow CssClass="alterRowGV" Enabled="True"></AlternatingRow>
                <Row CssClass="rowHeight"></Row>
            </Styles>
            <Settings ShowGroupPanel="true" VerticalScrollBarMode="Auto" />
            <SettingsLoadingPanel Mode="ShowOnStatusBar" />
            <SettingsBehavior EnableCustomizationWindow="true" AllowDragDrop="true" ConfirmDelete="True" ColumnResizeMode="Control" />
            <ClientSideEvents ColumnStartDragging="grid_customizationWindowCloseUp" EndCallback="GridOnEndCallBack" />

            <Templates>
                <EditForm>
                    <div class="overlay">
                        <div class="FormPopup">
                            <div class="formHeaderDiv">
                                <h3>Customers</h3>
                            </div>
                            <fieldset>
                                <legend></legend>
                                <table>
                                    <tr>
                                        <td>Code : </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox Enabled="true" ID="txtCode" ClientInstanceName="txtClientCode" OnInit="txtCode_Init" runat="server" Width="170px" Text='<%# Bind("CUS_NO") %>' TabIndex="1" MaxLength="12">
                                                    <NullTextStyle Font-Size="Small" />
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="Code is Required" />
                                                        <RegularExpression ValidationExpression="[A-Z0-9\-]{0,12}" ErrorText="Invalid Code." />
                                                    </ValidationSettings>
                                                    <ClientSideEvents LostFocus="function(s, e) { ClBk_CodeLostFocus.PerformCallback(txtClientCode.GetText()); }" />
                                                </dx:ASPxTextBox>
                                                <dx:ASPxCallback ID="ClBk_CodeLostFocus" runat="server" ClientInstanceName="ClBk_CodeLostFocus" OnCallback="ClBk_CodeLostFocus_Callback">
                                                    <ClientSideEvents CallbackComplete="function(s,e) { OnCallbackComplete(s, e); }" />
                                                </dx:ASPxCallback>
                                            </div>
                                        </td>
                                         <td>Status : </td>
                                        <td>
                                            <dx:ASPxComboBox ID="cmbxStatus" ClientInstanceName="cmbxClientStatus" runat="server" Width="170px" TabIndex="2" SelectionMode="Single" Text='<%# Bind("CUS_STATS") %>' Value='<%# Bind("CUS_STATS") %>'>
                                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <RequiredField IsRequired="true" ErrorText="Status is Required" />
                                                </ValidationSettings>
                                                <Items>
                                                    <dx:ListEditItem Value="Active" Text="Active" />
                                                    <dx:ListEditItem Value="Inactive" Text="Inactive" />
                                                </Items>
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Name: </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtName" ClientInstanceName="txtClientName" runat="server" Width="170px" Text='<%# Bind("CUS_NAME") %>' TabIndex="3">
                                                    <NullTextStyle Font-Size="Small" />
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="Name is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                        <td>Arabic Name: </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtArabicName" ClientInstanceName="txtClientArabicName" runat="server" Width="170px" Text='<%# Bind("CUS_NAMAR") %>' TabIndex="3">
                                                    <NullTextStyle Font-Size="Small" />
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <RequiredField IsRequired="false" ErrorText="Arabic Name is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Address 1: </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtAddress1" ClientInstanceName="txtClientAddress1" runat="server" Width="170px" Text='<%# Bind("CUS_ADDR1") %>' TabIndex="4" MaxLength="255">
                                                    <NullTextStyle Font-Size="Small" />
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <RequiredField IsRequired="false" ErrorText="Address 1 is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                        <td>Address 2: </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtAddress2" ClientInstanceName="txtClientAddress2" runat="server" Width="170px" Text='<%# Bind("CUS_ADDR2") %>' TabIndex="5" MaxLength="255">
                                                    <NullTextStyle Font-Size="Small" />
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <RequiredField IsRequired="false" ErrorText="Address 2 is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>City : </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtCity" ClientInstanceName="txtClientCity" runat="server" Width="170px" Text='<%# Bind("CUS_CITY") %>' TabIndex="6" MaxLength="50">
                                                    <NullTextStyle Font-Size="Small" />
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <RequiredField IsRequired="false" ErrorText="City is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Contact 1: </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtContact1" ClientInstanceName="txtClientContact1" runat="server" Width="170px" Text='<%# Bind("CUS_CNTC1") %>' TabIndex="7" MaxLength="50">
                                                    <NullTextStyle Font-Size="Small" />
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <RequiredField IsRequired="false" ErrorText="Contact 1 is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                        <td>Phone 1: </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtPhone1" ClientInstanceName="txtClientPhone1" runat="server" Width="170px" Text='<%# Bind("CUS_PHON1") %>' TabIndex="8" MaxLength="50">
                                                    <NullTextStyle Font-Size="Small" />
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <RequiredField IsRequired="false" ErrorText="Phone 1 is Required" />
                                                        <RegularExpression ValidationExpression="[0-9]{6,15}" ErrorText="Invalid Phone number." />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Contact 2: </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtContact2" ClientInstanceName="txtClientContact2" runat="server" Width="170px" Text='<%# Bind("CUS_CNTC2") %>' TabIndex="9" MaxLength="50">
                                                    <NullTextStyle Font-Size="Small" />
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <RequiredField IsRequired="false" ErrorText="Contact 2 is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                        <td>Phone 2: </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtPhone2" ClientInstanceName="txtClientPhone2" runat="server" Width="170px" Text='<%# Bind("CUS_PHON2") %>' TabIndex="10" MaxLength="50">
                                                    <NullTextStyle Font-Size="Small" />
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <RequiredField IsRequired="false" ErrorText="Phone 2 is Required" />
                                                        <RegularExpression ValidationExpression="[0-9]{6,15}" ErrorText="Invalid Phone number." />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Email : </td>
                                        <td>
                                            <dx:ASPxTextBox ID="txtEmail" ClientInstanceName="txtClientEmail" runat="server" Width="170px" Text='<%# Bind("CUS_EMAIL") %>' TabIndex="11" MaxLength="50">
                                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <RequiredField IsRequired="false" ErrorText="Email is Required" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>

                                        </td>
                                        <td>Taxable : </td>
                                        <td>
                                            <dx:ASPxComboBox ID="cmbxTaxable" ClientInstanceName="cmbxClientTaxable" runat="server" Width="170px" Value='<%# Bind("CUS_TXABL") %>' TabIndex="12" MaxLength="3">
                                                <Items>
                                                    <dx:ListEditItem Value="Y" Text="Yes" />
                                                    <dx:ListEditItem Value="N" Text="No" />
                                                </Items>
                                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <RequiredField IsRequired="true" ErrorText="Taxable is Required" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sales Rep :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmbxSLSRP" ClientInstanceName="cmbxClientSLSRP" DataSourceID="CmBxSLSREPSqlDataSource1" ValueField="SRP_CODE" TextField="SRP_NAME" runat="server" Width="170px" ValueType="System.String" Value='<%# Bind("CUS_SLSRP") %>' TabIndex="13">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Sales Rep is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="CmBxSLSREPSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT [SRP_CODE] ,[SRP_NAME] FROM  [STP_MSTR_SLREP]"></asp:SqlDataSource>
                                            </div>
                                        </td>
                                        <td>Type : </td>
                                        <td>
                                            <dx:ASPxComboBox ID="cmBxType" ClientInstanceName="cmbxClientType" runat="server" Width="170px" Value='<%# Bind("CUS_TYPE") %>' TabIndex="14">
                                                <Items>
                                                    <dx:ListEditItem Value="CASH" Text="Cash" />
                                                    <dx:ListEditItem Value="A/R" Text="Credit" />
                                                </Items>
                                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <RequiredField IsRequired="true" ErrorText="Customer type is Required" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Catg :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmbxCatg" ClientInstanceName="cmbxClientCatg" DataSourceID="CmBxCatgSqlDataSource1" ValueField="CUS_CATCD" TextField="CUS_CATNM" runat="server" Width="170px" ValueType="System.String" Value='<%# Bind("CUS_CATG") %>' TabIndex="15">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="false" ErrorText="Category is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="CmBxCatgSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT [CUS_CATCD] ,[CUS_CATNM] FROM [CUS_MST_CATG]"></asp:SqlDataSource>
                                            </div>
                                        </td>
                                        <td>VAT # :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtVatNo" ClientInstanceName="txtClientVatNo" runat="server" Width="170px" Text='<%# Bind("CUS_VATNO") %>' TabIndex="16" MaxLength="50">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <RequiredField IsRequired="false" ErrorText="VAT # is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Customer Group :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmbxCusGroup" ClientInstanceName="cmbxClientCusGroup" DataSourceID="CmBxCusGroupSqlDataSource1" ValueField="CGP_CODE" TextField="CGP_NAME" runat="server" Width="170px" ValueType="System.String" Value='<%# Bind("CUS_GRPCD") %>' TabIndex="17">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="false" ErrorText="Customer Group is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="CmBxCusGroupSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT [CGP_CODE] ,[CGP_NAME] FROM [STP_MSTR_CUSGP]"></asp:SqlDataSource>
                                            </div>
                                        </td>
                                        <td>Assign to :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxDropDownEdit ID="AssgndRouteNo" OnInit="AssgndRouteNo_Init" ClientInstanceName="DraClientRouteNo" CssClass="multiselect_combobox" runat="server" AnimationType="Auto">
                                                    <DropDownWindowTemplate>
                                                        <dx:ASPxListBox Width="170px" ID="lstBxRouteItems" ClientInstanceName="checkRouteListBox" SelectionMode="CheckColumn" runat="server">
                                                            <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged" />
                                                        </dx:ASPxListBox>
                                                    </DropDownWindowTemplate>
                                                    <ClientSideEvents TextChanged="SynchronizeListBoxValues" DropDown="SynchronizeListBoxValues" />
                                                </dx:ASPxDropDownEdit>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="3">
                                            <dx:ASPxLabel ID="lblErrorMessage" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                            </fieldset>
                                <hr style="color: skyblue" />
                            <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                                <dx:ASPxButton ID="btnAdd1" runat="server" Text="Save" CssClass="btnPopup btn btn-success" CausesValidation="true" OnClick="btnAdd1_Click" />
                                <dx:ASPxButton ID="btnPopUpClear" runat="server" Text="Cancel" CssClass="btnPopup btn btn-warning" CausesValidation="false" AutoPostBack="false">
                                    <ClientSideEvents Click="function(s, e) { CallBackCode.PerformCallback(txtClientCode.GetText()); }" />
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="btnPopUpDelete" runat="server" CommandArgument='<%# Eval("CUS_NO") %>' CommandName="PopUpDelete" Text="Delete" CssClass="btnPopup btn btn-danger btn-danger-variant" CausesValidation="false">
                                    <ClientSideEvents Click="function(s, e) { 
                                                if (!confirm('Confirm Delete?')) { 
                                                    e.processOnServer = false;  
                                                } 
                                        }" />
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="btnPopUpPrint" runat="server" Text="Print" CssClass="btnPopup btn btn-info btn-info-variant" AutoPostBack="false" CausesValidation="false">
                                    <ClientSideEvents Click="function(s, e) { printDoc(s, e); }" />
                                </dx:ASPxButton>
                                <dx:ASPxGridViewTemplateReplacement ID="btnPopUpClose" ReplacementType="EditFormCancelButton" runat="server" />
                            </div>
                        </div>
                    </div>
                </EditForm>
            </Templates>

            <Templates>
                <StatusBar>
                    <table>
                        <tr>
                            <td>
                                <%--<dx:ASPxButton ID="btnNewRecord" runat="server" Text="Add New Item" AutoPostBack="false">
                                    <ClientSideEvents Click="function (s, e){ FromGridView.AddNewRow();  }" />
                                </dx:ASPxButton>--%>
                            </td>
                            <td>Search: </td>
                            <td>
                                <dx:ASPxButtonEdit runat="server" ID="CustomSearchPanelTxtBx" />
                            </td>
                            <td>&nbsp;</td>
                            <td>Search In:</td>
                            <td>
                                <dx:ASPxComboBox AutoPostBack="true" ID="DDLFilterByColumn" runat="server" OnSelectedIndexChanged="DDLFilterByColumn_SelectedIndexChanged">
                                    <Items>
                                        <dx:ListEditItem Text="All" Value="*" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>

                        </tr>
                    </table>
                </StatusBar>
            </Templates>

        </dx:ASPxGridView>
    </div>

    <script type="text/javascript">
        window.onload = function () {
            $('form').on('click', '.btnPopupClose', function () {
                //$('.overlay').hide();
            });
        };

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //if (prm != null) {
        prm.add_endRequest(function (sender, e) {
            //if (sender._postBackSettings.panelsToUpdate != null) {            
            //$('.overlay').show();
            //}
        });
        prm.add_endRequest(GridOnEndCallBack);
        //};

        function setCustomerStatus(s, e) {
            var className = "";
            var statusString = "";

            switch (s.GetText()) {
                case "-1":
                    className = "label-rejected white-clr";
                    statusString = "Blacklisted";
                    break;
                case "0":
                    className = "label-pending";
                    statusString = "Inactive";
                    break;
                case "1":
                    className = "label-approved";
                    statusString = "Active";
                    break;
                default:
                    className = "";
                    statusString = s.GetText();
                    break;
            }

            s.SetValue(statusString);
            s.SetText(statusString);
            s.GetMainElement().className = "customer_status_label label " + className;
        }


        function OnTestValidation(s, e) {
            //var testData = e.value;
            //if (!testData) {
            lblError.SetVisible(!e.isValid);
            //  return;
        }

        function OnCallbackComplete(s, e) {
            txtClientName.SetText("");
            txtClientArabicName.SetText("");
            txtClientAddress1.SetText("");
            txtClientAddress2.SetText("");
            txtClientCity.SetText("");
            txtClientContact1.SetText("");
            txtClientPhone1.SetText("");
            txtClientContact2.SetText("");
            txtClientPhone2.SetText("");
            txtClientEmail.SetText("");

            cmbxClientTaxable.SetValue("N");
            cmbxClientSLSRP.SetValue("");
            cmbxClientType.SetValue("CASH");
            cmbxClientCatg.SetValue("");

            txtClientVatNo.SetText("");

            cmbxClientCusGroup.SetValue("");

            DraClientRouteNo.SetText("");

            var jsonData = JSON.parse(e.result);
            console.log(jsonData);
            if (jsonData.length > 0) {
                txtClientName.SetText(jsonData[0]["CUS_NAME"]);
                txtClientArabicName.SetText(jsonData[0]["CUS_NAMAR"]);
                txtClientAddress1.SetText(jsonData[0]["CUS_ADDR1"]);
                txtClientAddress2.SetText(jsonData[0]["CUS_ADDR2"]);
                txtClientCity.SetText(jsonData[0]["CUS_CITY"]);
                txtClientContact1.SetText(jsonData[0]["CUS_CNTC1"]);
                txtClientPhone1.SetText(jsonData[0]["CUS_PHON1"]);
                txtClientContact2.SetText(jsonData[0]["CUS_CNTC2"]);
                txtClientPhone2.SetText(jsonData[0]["CUS_PHON2"]);
                txtClientEmail.SetText(jsonData[0]["CUS_EMAIL"]);

                cmbxClientTaxable.SetValue(jsonData[0]["CUS_TXABL"]);
                cmbxClientSLSRP.SetValue(jsonData[0]["CUS_SLSRP"]);
                cmbxClientType.SetValue(jsonData[0]["CUS_TYPE"]);
                cmbxClientCatg.SetValue(jsonData[0]["CUS_CATG"]);

                txtClientVatNo.SetText(jsonData[0]["CUS_VATNO"]);

                cmbxClientCusGroup.SetValue(jsonData[0]["CUS_GRPCD"]);

                DraClientRouteNo.SetText(jsonData[0]["ASSGND_ROUTES"]);
            }
        }
    </script>
</asp:Content>

