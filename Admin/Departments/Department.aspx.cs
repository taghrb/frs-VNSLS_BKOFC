﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Departments_Default : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
    
    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT * FROM STP_USR_DEPTS";
    }

    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxTextBox txtBxCode = ((ASPxTextBox)gvDepartmentInquery.FindEditFormTemplateControl("DeptEditCode"));
        string DeptCode = "0";
        if (txtBxCode.Text.Length > 0)
        {
            DeptCode = txtBxCode.Text;
        }

        string DeptName = ((ASPxTextBox)gvDepartmentInquery.FindEditFormTemplateControl("DeptEditName")).Text;
        int DeptSeqNo = int.Parse(((ASPxTextBox)gvDepartmentInquery.FindEditFormTemplateControl("DeptEditSeqNo")).Text);

        DataTable myDT = new DataTable();

        myDT = dbHlpr.FetchData("SELECT * FROM STP_USR_DEPTS WHERE DPT_CODE = '" + DeptCode + "'");
        if (myDT.Rows.Count <= 0)
        {
            dbHlpr.ExecuteNonQuery("INSERT INTO STP_USR_DEPTS (DPT_CODE, DPT_NAME, DPT_SEQNO) "
            + " VALUES ('" + DeptCode + "', '" + DeptName + "', '" + DeptSeqNo + "')");
        }
        else
        {
            dbHlpr.ExecuteNonQuery("UPDATE STP_USR_DEPTS SET "
            + " DPT_CODE = '" + DeptCode + "', "
            + " DPT_NAME = '" + DeptName + "', "
            + " DPT_SEQNO = '" + DeptSeqNo + "' "
            + " WHERE DPT_CODE = '" + DeptCode + "'");
        }

        gvDepartmentInquery.CancelEdit();
        gvDepartmentInquery.DataBind();
    }
    protected void btnUpdate1_Click(object sender, EventArgs e)
    {
        
    }
    protected void ACB_CodeCheck_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (e.Parameter.Length > 0)
        {
            DataTable myDT = new DataTable();
            myDT = dbHlpr.FetchData("SELECT * FROM STP_USR_DEPTS WHERE DPT_CODE = '" + e.Parameter.Trim() + "'");
            JavaScriptSerializer myserializer = new JavaScriptSerializer();
            if (myDT.Rows.Count > 0)
            {
                e.Result = myserializer.Serialize(myDT.Rows[0].ItemArray);
            }
            else
            {
                e.Result = myserializer.Serialize("");
            }
        }
    }

    protected void gvDepartmentInquery_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvDepartmentInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string deptCode = e.Keys["DPT_CODE"].ToString();

        dbHlpr.ExecuteNonQuery("DELETE FROM STP_USR_DEPTS WHERE DPT_CODE = '" + deptCode + "'");

        e.Cancel = true;
        gvDepartmentInquery.CancelEdit();
    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvDepartmentInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvDepartmentInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

        ////html export here
        //gridExport.ExportHtmlToResponse();
        //string str;
        //MemoryStream ms = new MemoryStream();
        //try
        //{
        //    gridExport.ExportToHtml(ms);
        //    ms.Seek(0, SeekOrigin.Begin);
        //    StreamReader sr = new StreamReader(ms);
        //    str = sr.ReadToEnd();
        //}
        //finally
        //{
        //    ms.Close();
        //}

    }
    protected void gvDepartmentInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            string deptartmentId = e.CommandArgs.CommandArgument.ToString().Trim();
            if (deptartmentId.Length > 0) // Code is sent via request
            {
                dbHlpr.ExecuteNonQuery("DELETE FROM STP_USR_DEPTS WHERE DPT_CODE = '" + deptartmentId + "'");

                gvDepartmentInquery.CancelEdit();
                gvDepartmentInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvDepartmentInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid Department ID. Unable to delete Department.";
            }
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvDepartmentInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvDepartmentInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvDepartmentInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvDepartmentInquery.SearchPanelFilter = txtFilter.Text;
        gvDepartmentInquery.DataBind();
    }
    protected void gvDepartmentInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvDepartmentInquery, CBX_filter);
    }
    protected void gvDepartmentInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvDepartmentInquery, CBX_filter);
    }
    protected void DeptEditCode_Init(object sender, EventArgs e)
    {
        if (!gvDepartmentInquery.IsNewRowEditing)
        {
            ASPxTextBox txtCode = (ASPxTextBox)sender;
            txtCode.ReadOnly = true;
        }
        else
        {
            ASPxTextBox txtCode1 = (ASPxTextBox)sender;
            txtCode1.ReadOnly = false;
        }     
    }
}