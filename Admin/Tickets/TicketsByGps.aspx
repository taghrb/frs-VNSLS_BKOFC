﻿<%@ Page Title="Tickets By Gps" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="TicketsByGps.aspx.cs" Inherits="Admin_Tickets_TicketsByGps" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <style>
        #map-wrapper {
            width: 100%;
            height: 200px;
        }
    </style>
    <script type="text/javascript">
        jQuery = jQuery.noConflict();
        $ = jQuery.noConflict();
        function grid_customizationWindowCloseUp(s, e) {
            //if(FromGridView.IsCustomizationWindowVisible())
            FromGridView.ShowCustomizationWindow();
        }

        $(function () {
            $('.testing123').dialog();
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topBarContent1" runat="Server">
    <div class="topContentBar">
        <ul>
            <li>
                <asp:ImageButton ID="LinkBtnPDF" OnClick="LinkBtnPDF_Click" runat="server" ImageUrl="~/img/FilesIcons/acrobat.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonXLX" OnClick="LinkButtonXLX_Click" runat="server" ImageUrl="~/img/FilesIcons/excel.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonXxls" OnClick="LinkButtonXxls_Click" runat="server" ImageUrl="~/img/FilesIcons/xlsx.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonRtf" OnClick="LinkButtonRtf_Click" runat="server" ImageUrl="~/img/FilesIcons/rtf.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonCvs" OnClick="LinkButtonCvs_Click" runat="server" ImageUrl="~/img/FilesIcons/cvs.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonWord" OnClick="LinkButtonRtf_Click" runat="server" ImageUrl="~/img/FilesIcons/word.png" Width="50" Height="40"></asp:ImageButton>
            </li>

            <li>
                <asp:ImageButton ID="LinkButtonHtml" OnClick="LinkButtonHtml_Click" runat="server" ImageUrl="~/img/icons/searchtop.jpg" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton6" OnClick="LinkButton3_Click" runat="server" ImageUrl="~/img/FilesIcons/acrobat.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton7" OnClick="LinkButton3_Click" runat="server" ImageUrl="~/img/FilesIcons/word.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton8" OnClick="LinkButton3_Click" runat="server" ImageUrl="~/img/FilesIcons/excel.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton9" OnClick="LinkButton3_Click" OnClientClick="window.print();" runat="server" ImageUrl="~/img/FilesIcons/acrobat.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton10" OnClick="LinkButton3_Click" runat="server" ImageUrl="~/img/FilesIcons/torrent.png" Width="50" Height="40"></asp:ImageButton>
            </li>
        </ul>

        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvInquery" ExportedRowType="All"></dx:ASPxGridViewExporter>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">

    <div class="formDiv">
        <div class="formHeaderDiv">
            <h3>Tickets By GPS</h3>
        </div>
        <div class="fieldsetWrapper">
            <fieldset>
                <legend></legend>
                <table style="margin: 0 auto;">
                    <tr>
                        <td>Store # :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxComboBox NullText="All" NullTextStyle-ForeColor="Black" ID="TktInqryStrNo" OnInit="TktInqryStrNo_Init" ClientInstanceName="TktInqryClientStrNo" runat="server" AnimationType="Auto" OnValueChanged="TktInqryStrNo_ValueChanged" AutoPostBack="true">
                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="false" ErrorText="Store No. is Required." />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                            </div>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>Register # :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxComboBox NullText="All" NullTextStyle-ForeColor="Black" ID="TktInqryRegNo" OnInit="TktInqryRegNo_Init" ClientInstanceName="TktInqryClientRegNo" runat="server" AnimationType="Auto">
                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="false" ErrorText="Register No. is Required." />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Date From :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxDateEdit runat="server" ID="TktInqryDateFrom" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy" NullText="Earliest" NullTextStyle-ForeColor="Black"></dx:ASPxDateEdit>
                            </div>
                        </td>
                        <td></td>

                        <td>Date To :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxDateEdit runat="server" ID="TktInqryDateTo" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy" NullText="Latest" NullTextStyle-ForeColor="Black"></dx:ASPxDateEdit>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Customer # :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxComboBox NullText="All" NullTextStyle-ForeColor="Black" ID="TktInqryCusNo" OnInit="TktInqryCusNo_Init" ClientInstanceName="TktInqryClientCusNo" runat="server" AnimationType="Auto">
                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="false" ErrorText="Customer No. is Required." />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                            </div>
                        </td>
                        <td></td>
                        <td>Ticket No. :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxTextBox ID="TktInqryTicketNo" ClientInstanceName="TktInqryClientTicketNo" runat="server" AnimationType="Auto">
                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="false" ErrorText="Ticket No. is Required." />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </fieldset>
            <hr style="color: skyblue" />
            <dx:ASPxLabel ID="lblErrorMessage" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
            <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                <dx:ASPxButton ID="btnViewTicket" runat="server" Text="Submit" CssClass="btnPopup btn btn-success" CausesValidation="true" OnClick="btnViewTicket_Click" />
                <dx:ASPxButton ID="btnPopUpClear" runat="server" Text="Clear" CssClass="btnPopup btn btn-warning" CausesValidation="false" AutoPostBack="false" />
            </div>

        </div>
        <asp:UpdatePanel runat="server" ID="UpdatePanelOrdersPage" ChildrenAsTriggers="true">
            <ContentTemplate>
                <asp:SqlDataSource runat="server" ID="DxGridSqlDataSource1"
                    ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
                <asp:SqlDataSource runat="server" ID="DxGridSqlDataSource2"
                    ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
                <dx:ASPxGridView ID="gvInquery" CssClass="FromGridView" ClientInstanceName="FromGridView" Theme="Office2010Black" runat="server"
                    DataSourceID="DxGridSqlDataSource1"
                    AutoGenerateColumns="false"
                    KeyFieldName="TKH_HKEY"
                    OnRowCommand="gvInquery_RowCommand"
                    OnDataBound="gvInquery_DataBound"
                    OnBeforeGetCallbackResult="gvInquery_BeforeGetCallbackResult"
                    Settings-ShowTitlePanel="false"
                    SettingsText-Title="Sales">


                    <Settings ColumnMinWidth="150" UseFixedTableLayout="false" ShowGroupPanel="true" VerticalScrollBarMode="Auto" HorizontalScrollBarMode="Visible" />
                    <SettingsContextMenu Enabled="True" EnableColumnMenu="True" EnableRowMenu="True" />
                    <SettingsSearchPanel Visible="False" CustomEditorID="CustomSearchPanelTxtBx" />
                    <Columns>
                        <dx:GridViewDataTextColumn FieldName="TKH_TKTNO" UnboundType="Integer" Settings-SortMode="Value" Caption="Sales ID" VisibleIndex="2" CellStyle-HorizontalAlign="Center">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                            <EditFormSettings VisibleIndex="0" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="TKH_TTYPE" Caption="Ticket Type" VisibleIndex="3">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="TKH_CUSNO" Caption="Customer #" Visible="true" VisibleIndex="4">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="TKH_CUSNM" Caption="Name" VisibleIndex="5">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn FieldName="TKH_DATE" Caption="Sales Date" VisibleIndex="6" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataDateColumn>
                        <dx:GridViewDataTimeEditColumn FieldName="TKH_TIME" Caption="Sales Time" VisibleIndex="7" Visible="false">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTimeEditColumn>
                        <dx:GridViewDataTextColumn FieldName="TKH_PAYCD" Caption="Pay Code" VisibleIndex="9" Visible="false">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="TKH_SREP" Caption="Sales Rep" VisibleIndex="10" Visible="true">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="TKH_STRNO" Caption="Store #" VisibleIndex="11">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="TKH_REGNO" Caption="Register #" VisibleIndex="12" Visible="false">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="TKH_CRTDT" Caption="Created Date" VisibleIndex="15" Visible="false">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="TKH_CRTIM" Caption="Created Time" VisibleIndex="16" Visible="false">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="TKH_CRTBY" Caption="Created By" VisibleIndex="17" Visible="false">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="TKH_GEOLA" Caption="Latitude" VisibleIndex="25">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="TKH_GEOLN" Caption="Longitude" VisibleIndex="26">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <SettingsCommandButton>
                        <NewButton ButtonType="Button" Text="New" Styles-Style-CssClass="btna btn btn-primary  btn-sm">
                            <Image runat="server" Url="~/img/icons/white-icn/Add.png" Width="15px"></Image>
                        </NewButton>
                        <SelectButton ButtonType="Button" Text="Select" Styles-Style-CssClass="btna btn btn-success  btn-sm">
                            <Image ToolTip="Select button here" Url="~/img/icons/white-icn/eyee1.png" Width="15px" />
                        </SelectButton>
                        <EditButton ButtonType="Button" Text="Edit" Styles-Style-CssClass="btna btn btn-info btn-sm">
                            <Image runat="server" Url="~/img/icons/white-icn/Pen Tool.png" Width="15px"></Image>
                        </EditButton>
                        <DeleteButton ButtonType="Button" Text="Delete" Styles-Style-CssClass="btna btn btn-danger btn-sm btn-danger-variant">
                            <Image runat="server" Url="~/img/icons/white-icn/Trash.png" Width="15px"></Image>
                        </DeleteButton>
                        <CancelButton ButtonType="Button" Text="Close" Styles-Style-CssClass="btnPopup btn btn-primary">
                            <%--<Image ToolTip="Cancel button here" Url="~/img/icons/close.png" />--%>
                        </CancelButton>
                        <UpdateButton ButtonType="Button" Text="Update">
                        </UpdateButton>
                    </SettingsCommandButton>
                    <SettingsEditing EditFormColumnCount="2" />
                    <SettingsPopup>
                        <EditForm VerticalAlign="WindowCenter" AllowResize="True" Modal="True" HorizontalAlign="WindowCenter"></EditForm>
                    </SettingsPopup>
                    <%--<SettingsPager Mode="ShowAllRecords"></SettingsPager>--%>
                    <SettingsPager Position="Bottom">
                        <PageSizeItemSettings Caption="Records per Page" Items="10, 20, 50, 100" ShowAllItem="true" Position="Right" Visible="true">
                        </PageSizeItemSettings>
                    </SettingsPager>
                    <Styles>
                        <CommandColumn Spacing="0px" Wrap="False" />
                        <EditFormDisplayRow HorizontalAlign="Center" VerticalAlign="Middle"></EditFormDisplayRow>
                        <AlternatingRow CssClass="alterRowGV" Enabled="True"></AlternatingRow>
                        <Row CssClass="rowHeight"></Row>
                        <Cell Wrap="False" />
                    </Styles>

                    <SettingsLoadingPanel Mode="ShowOnStatusBar" />
                    <SettingsBehavior SortMode="DisplayText" AllowSelectSingleRowOnly="false" AllowSelectByRowClick="true" EnableCustomizationWindow="true" AllowDragDrop="true" ConfirmDelete="true" ColumnResizeMode="Control" />
                    <ClientSideEvents ColumnStartDragging="grid_customizationWindowCloseUp" EndCallback="GridOnEndCallBack" />
                    <SettingsDetail ShowDetailRow="false" AllowOnlyOneMasterRowExpanded="true" />
                    <Templates>
                        <EditForm>

                        </EditForm>
                    </Templates>
                    <Templates>
                        <StatusBar>
                            <table>
                                <tr>
                                    <td>Search: </td>
                                    <td>
                                        <dx:ASPxButtonEdit runat="server" ID="CustomSearchPanelTxtBx" />
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>Search In:</td>
                                    <td>
                                        <dx:ASPxComboBox AutoPostBack="true" ID="DDLFilterByColumn" runat="server" OnSelectedIndexChanged="DDLFilterByColumn_SelectedIndexChanged">
                                            <Items>
                                                <dx:ListEditItem Text="All" Value="*" />
                                            </Items>
                                        </dx:ASPxComboBox>
                                    </td>
                                </tr>
                            </table>
                        </StatusBar>
                    </Templates>
                </dx:ASPxGridView>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>




    <script type="text/javascript">
        window.onload = function () {
            $('form').on('click', '.btnPopupClose', function () {
                //$('.overlay').hide();
            });


        };

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //if (prm != null) {
        prm.add_endRequest(function (sender, e) {
            //if (sender._postBackSettings.panelsToUpdate != null) {            
            //$('.overlay').show();
            //}
        });
        prm.add_endRequest(GridOnEndCallBack);
        //};

        function setOrderStatus(s, e) {
            var className = "";
            var statusString = "";
            window.blabla = s;
            switch (s.GetText()) {
                case "5":
                    className = "label-rejected white-clr";
                    statusString = "Rejected";
                    break;
                case "0":
                    className = "label-pending";
                    statusString = "Pending";
                    break;
                case "1":
                    className = "label-approved";
                    statusString = "Approved";
                    break;
                case "2":
                    className = "label-queue";
                    statusString = "In Queue";
                    break;
                case "3":
                    className = "label-readyfrpickup";
                    statusString = "Ready for Pickup";
                    break;
                case "4":
                    className = "label-pickedup";
                    statusString = "Picked up";
                    break;
                case "6":
                    className = "label-inkitchen";
                    statusString = "In Kitchen";
                    break;
                case "7":
                    className = "label-readytodeliver";
                    statusString = "Ready to Deliver";
                    break;
                case "8":
                    className = "label-delivered";
                    statusString = "Delivered";
                    break;
                case "9":
                    className = "label-ontheway";
                    statusString = "On the way";
                    break;
                case "10":
                    className = "label-onhold white-clr";
                    statusString = "On Hold";
                    break;
                case "11":
                    className = "label-rejected white-clr";
                    statusString = "UnDelivered";
                    break;
                case "12":
                    className = "label-rejected white-clr";
                    statusString = "Cancelled";
                    break;
                default:
                    className = "";
                    statusString = s.GetText();
                    break;
            }

            s.SetValue(statusString);
            s.SetText(statusString);
            s.GetMainElement().className = "order_status_label label " + className;
        }

        function OnTestValidation(s, e) {
            //var testData = e.value;
            //if (!testData) {
            lblError.SetVisible(!e.isValid);
            //  return;
        }


        function initialize() {
            var myLat = document.getElementById("LatHidden").value;
            var myLng = document.getElementById("LngHidden").value;
            //var myLatlng = new google.maps.LatLng(-34.397, 150.644);
            var myLatlng = new google.maps.LatLng(myLat, myLng);
            var myOptions = {
                zoom: 15,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var map = new google.maps.Map(document.getElementById("OrderEditDeliveryLocationCode_map"), myOptions);

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: 'Delivery location!'
            });
        }

        function loadMap(spanClicked) {
            $(spanClicked).hide();

            $('script#googleMapScript').remove();
            $('#OrderEditDeliveryLocationCode_map').css("height", "180px");
            $('#OrderEditDeliveryLocationCode_map').css("width", "200px");
            $('#OrderEditDeliveryLocationCode_map').css("border", "1px solid");
            $('#OrderEditDeliveryLocationCode_map').css("text-align", "center");

            $('#OrderEditDeliveryLocationCode_map').html("Loading...");

            var script = document.createElement("script");
            script.type = "text/javascript";
            script.id = "googleMapScript";
            script.src = "http://maps.google.com/maps/api/js?key=AIzaSyCbpqKGLep4wAkxiDu7zWjNwhAyqGz8-oM&zoom=&sensor=false&callback=initialize";
            document.body.appendChild(script);
        }

    </script>


    <%--    <script>
        function initMap() {
            var mapDiv = document.getElementById('map-wrapper');
            var map = new google.maps.Map(mapDiv, {
                center: { lat: 44.540, lng: -78.546 },
                zoom: 8
            });
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCbpqKGLep4wAkxiDu7zWjNwhAyqGz8-oM"></script>--%>
</asp:Content>

