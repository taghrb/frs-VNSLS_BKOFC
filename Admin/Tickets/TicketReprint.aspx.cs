﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using System.Drawing;
using DevExpress.XtraPrinting;
using DevExpress.Export.Xl;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Globalization;

public partial class Admin_Reports_TicketReprint : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
    ReportsDS ReportsDataSet = new ReportsDS();

    //string TKT_NO = string.Empty;
    string SalesType;

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["Rpt"] != null && IsPostBack)
        {
            RptVwr.ReportSource = Session["Rpt"];
        }
    }

    protected void btnPopUpClear_Click(object sender, EventArgs e)
    {
        //TktRprntRptStrNo.Value = null;
        //TktRprntRptRegNo.Value = null;
        //TktRprntRptTicketNo.Value = null;

        //RptVwr.ReportSource = null;
        //Session["Rpt"] = null;
    }

    protected void btnExportTicketReprint_Click(object sender, EventArgs e)
    {
        if (TktRprntRptStrNo.Value == null || TktRprntRptStrNo.Value.ToString() == "")
        {
            lblErrorMessage.Text = "Please Select Store no.";
            RptVwr.ReportSource = null;
            Session["Rpt"] = null;

            return;
        }

        if (TktRprntRptRegNo.Value == null || TktRprntRptRegNo.Value.ToString() == "")
        {
            lblErrorMessage.Text = "Please Select Register no.";
            RptVwr.ReportSource = null;
            Session["Rpt"] = null;

            return;
        }

        if (TktRprntRptTicketNo.Value == null || TktRprntRptTicketNo.Value.ToString() == "")
        {
            lblErrorMessage.Text = "Please Select Ticket no.";
            RptVwr.ReportSource = null;
            Session["Rpt"] = null;

            return;
        }

        PrepareReportData();

        if (TktRprntRptPrintType.Value.ToString() == "RCPT")
        {
            ReceiptReportGen();
        }
        else if (TktRprntRptPrintType.Value.ToString() == "INVC")
        {
            A4ReportGen();
        }
    }

    public void PrepareReportData()
    {
        ReportsDataSet = new ReportsDS();

        string STR_NO = TktRprntRptStrNo.Value.ToString().Trim();
        string REG_NO = TktRprntRptRegNo.Value.ToString().Trim();
        string TKT_NO = TktRprntRptTicketNo.Value.ToString().Trim();

        DataTable SLSHdrDT = new DataTable();
        DataTable SLSLinDT = new DataTable();
        SLSHdrDT = dbHlpr.FetchData("SELECT "
            + " TKH_TKTNO, TKH_CUSNO, TKH_CUSNM, TKH_CUSVT, TKH_SREP,TKH_DATE, TKH_TIME, "
            + " TKH_CRTDT, TKH_CRTIM, TKH_CRTBY, TKH_SLAMT,TKH_DSCNT, TKH_NTAMT, TKH_VTAMT, "
            + " TKH_AMT, TKH_AMTPD, TKH_PAYCD, TKH_CHNGE, TKH_BLANC, TKH_TTYPE, '0' AS TKH_VATPR, "
            + " TKH_STRNO, TKH_REGNO, TKH_SHPTO "
            + " FROM SLS_TKH_HIST "
            + " WHERE TKH_STRNO = '" + STR_NO + "' AND TKH_REGNO = '" + REG_NO + "' AND TKH_HKEY = '" + TKT_NO + "' ");
        SLSLinDT = dbHlpr.FetchData("SELECT "
            + " TKL_TKTNO, TKL_ITMNO, TKL_DESC, TKL_EXPDT, TKL_UOM, TKL_QTY, TKL_PRICE, "
            + " TKL_DSCNT, TKL_EXPRC, TKL_TAXCD, TKL_TXFAC, TKL_VATPRC, TKL_CUSNO, TKL_CUSNM, "
            + " TKL_SREP, TKL_DATE, TKL_TIME, TKL_CRTDT, TKL_CRTIM, TKL_CRTBY, TKL_TTYPE "
            + " FROM SLS_TKL_HIST "
            + " WHERE TKL_STRNO = '" + STR_NO + "' AND TKL_REGNO = '" + REG_NO + "' AND TKL_HKEY = '" + TKT_NO + "' ");

        this.SalesType = SLSHdrDT.Rows.Count > 0 ? SLSHdrDT.Rows[0]["TKH_TTYPE"].ToString() : "Sales";

        string TaxFactr = string.Empty;
        for (int i = 0; i < SLSLinDT.Rows.Count; i++)
        {
            if (i == 0)
            {
                TaxFactr = Convert.ToDouble(SLSLinDT.Rows[i]["TKL_TXFAC"]).ToString("f0");
            }

            if (TaxFactr != Convert.ToDouble(SLSLinDT.Rows[i]["TKL_TXFAC"]).ToString("f0"))
            {
                TaxFactr = "Mixed";
            }
        }
        if (SLSHdrDT.Rows.Count > 0)
        {
            SLSHdrDT.Rows[0]["TKH_VATPR"] = TaxFactr + " %";
        }

        if (TktRprntRptPrintType.Value == "RCPT")
        {
            //Add to Temp Table 
            ReceiptReportTempData(SLSHdrDT, 1);
            ReceiptReportTempData(SLSLinDT, 2);
        }
        else if (TktRprntRptPrintType.Value == "INVC")
        {
            //Add to Temp Table 
            A4ReportTempData(SLSHdrDT, 1);
            A4ReportTempData(SLSLinDT, 2);
        }
    }

    public DataTable CompanyDetails(string CompCode)
    {
        DataTable CompDetailsDT = new DataTable();
        CompDetailsDT.Columns.Add("CMP_CODE");
        CompDetailsDT.Columns.Add("CMP_NAME");
        CompDetailsDT.Columns.Add("CMP_ANAME");
        CompDetailsDT.Columns.Add("CMP_VATNO");
        CompDetailsDT.Columns.Add("CMP_ADRES");
        CompDetailsDT.Columns.Add("CMP_CNTCT");
        CompDetailsDT.Columns.Add("CMP_MOBIL");

        if (CompCode == "FORSAN")
        {
            DataRow dr = CompDetailsDT.NewRow();
            dr["CMP_CODE"] = "FORSAN";
            dr["CMP_NAME"] = "Forsan Foods & Consumer Products Co. Ltd";
            dr["CMP_ANAME"] = "الغذائىة الفرسان - مبيعات فان";
            dr["CMP_VATNO"] = " 300056053500003 ";
            dr["CMP_ADRES"] = "Pri. Sultan Abdul Aziz St ,Riyadh";
            dr["CMP_CNTCT"] = "Tel # (011) 416-4422";
            dr["CMP_MOBIL"] = "Mobile # 055-816-1122";

            CompDetailsDT.Rows.Add(dr);
        }

        return CompDetailsDT;
    }

    public string GetItemArabicDesc(string ProdCode)
    {
        DataTable dtProdDesc2 = dbHlpr.FetchData("SELECT PRO_CODE, PRO_DESC1,PRO_DESC2, PRO_UOM, PRO_PRICE FROM INV_MSTR_PRODT "
           + "  WHERE PRO_CODE='" + ProdCode + "'");
        if (dtProdDesc2.Rows.Count < 1)
            return "";
        else
            return dtProdDesc2.Rows[0]["PRO_DESC2"].ToString();
    }

    public string GetItemArabicUoM(string UomCode)
    {
        DataTable dtUom = dbHlpr.FetchData("SELECT UOM_CODE, UOM_NAME, UOM_ANAME FROM STP_MSTR_UOM "
           + "  WHERE UOM_CODE='" + UomCode + "'");
        if (dtUom.Rows.Count < 1)
            return "";
        else
            return dtUom.Rows[0]["UOM_ANAME"].ToString();
    }

    public string CustomerDetails(string CustCode, string ColName)
    {
        DataTable dtCust = dbHlpr.FetchData("SELECT "
           + " CUS_NO, CUS_NAME, CUS_NAMAR, CUS_ADDR1, CUS_ADDR2, CUS_CITY, CUS_CNTC1, CUS_CNTC2, CUS_PHON1, "
           + " CUS_PHON2, CUS_EMAIL, CUS_SLSRP, CUS_TYPE, CUS_CATG, CUS_VATNO, CUS_TXABL "
           + " FROM STP_MST_CSTMR WHERE CUS_NO='" + CustCode + "'");
        if (dtCust.Rows.Count < 1)
            return "";
        else
            return dtCust.Rows[0][ColName].ToString();
    }

    public string ShipToDetails(string CustCode, string ShipToNo, string ColName)
    {
        DataTable dt = dbHlpr.FetchData("SELECT "
           + " SHP_CUSNO, SHP_NO, SHP_NAME, SHP_ADDRS, SHP_CITY, SHP_CNTNO, SHP_CNTNM, SHP_ROUTE "
           + " FROM STP_MST_SHPTO WHERE SHP_CUSNO = '" + CustCode + "' AND SHP_NO = '" + ShipToNo + "' ");
        if (dt.Rows.Count < 1)
            return "";
        else
            return dt.Rows[0][ColName].ToString();
    }

    private string GetCpTypeDate(string date)
    {
        // Accepted format is dd-MMM-yyyy
        string dayOfMonth = date.Substring(0, 2);
        string month = date.Substring(3, 3);
        string year = date.Substring(7, 4);

        string[] monthAbbrev = CultureInfo.CurrentCulture.DateTimeFormat.AbbreviatedMonthNames;
        int index = Array.IndexOf(monthAbbrev, month) + 1;
        return year + index.ToString("0#") + dayOfMonth;
    }

    public void A4ReportTempData(DataTable dt, int A)
    {
        if (A != 1)
        {
            DataTable DT_LINES = dt;
            for (int i = 0; i < DT_LINES.Rows.Count; i++)
            {
                DataRow dr = ReportsDataSet.TEMP_TABLE.NewRow();   //Tables["TEMP_TABLE"]. 

                if (DT_LINES.Rows[i]["TKL_TKTNO"] != null && DT_LINES.Rows[i]["TKL_TKTNO"] != "")
                {
                    dr["Text01"] = DatabaseHelperClass.ConvertToArabic(DT_LINES.Rows[i]["TKL_TKTNO"].ToString()); // Ticket No
                }
                else
                {
                    dr["Text01"] = "";
                }

                if (DT_LINES.Rows[i]["TKL_ITMNO"] != null && DT_LINES.Rows[i]["TKL_ITMNO"] != "")
                {
                    dr["Text02"] = DT_LINES.Rows[i]["TKL_ITMNO"].ToString(); // Item No
                    dr["Text03"] = DatabaseHelperClass.ConvertToArabic(DT_LINES.Rows[i]["TKL_ITMNO"].ToString()); // Item No Arabic
                }
                else
                {
                    dr["Text02"] = "";
                    dr["Text03"] = "";
                }

                if (DT_LINES.Rows[i]["TKL_DESC"] != null && DT_LINES.Rows[i]["TKL_DESC"] != "")
                {
                    dr["Text04"] = DT_LINES.Rows[i]["TKL_DESC"].ToString(); // +" " + dtgv.Rows[i]["DESC_LIN_2"].ToString(); //  Desc
                }
                else
                {
                    dr["Text04"] = "";
                }
                if (DT_LINES.Rows[i]["TKL_ITMNO"] != null && DT_LINES.Rows[i]["TKL_ITMNO"] != "")
                {
                    dr["Text05"] = GetItemArabicDesc(DT_LINES.Rows[i]["TKL_ITMNO"].ToString()); //aRABIC Desc
                }
                else
                {
                    dr["Text05"] = "";
                }

                if (DT_LINES.Rows[i]["TKL_TXFAC"] != null && DT_LINES.Rows[i]["TKL_TXFAC"] != "")
                {
                    dr["Text06"] = DatabaseHelperClass.ConvertToArabic(DT_LINES.Rows[i]["TKL_TXFAC"].ToString());// Tax Code
                    dr["Text08"] = DT_LINES.Rows[i]["TKL_TXFAC"].ToString(); // Tax Code
                }
                else
                {
                    dr["Text06"] = "";
                }

                if (DT_LINES.Rows[i]["TKL_UOM"] != null && DT_LINES.Rows[i]["TKL_UOM"] != "")
                {
                    dr["Text07"] = DT_LINES.Rows[i]["TKL_UOM"].ToString();// Unit / UOM
                    dr["Text20"] = GetItemArabicUoM(DT_LINES.Rows[i]["TKL_UOM"].ToString());// Unit / UOM Arabic
                    //DataTable myUOMDT = dbHlprACCDB.FetchData("SELECT * FROM STP_MST_UOM WHERE UOM = '" + dtgv.Rows[i]["STK_UNIT"].ToString().Trim() + "'");
                    //if (myUOMDT.Rows.Count > 0)
                    //{
                    //    dr["Text20"] = myUOMDT.Rows[0]["AUOM"].ToString();// Unit / UOM Arabic
                    //}
                }
                else
                {
                    dr["Text07"] = "";
                    dr["Text20"] = "";
                }

                ////////////////////////// numbers starts here /////////////////////////////

                if (DT_LINES.Rows[i]["TKL_QTY"] != null && DT_LINES.Rows[i]["TKL_QTY"] != "")
                {
                    dr["Text10"] = DatabaseHelperClass.ConvertToArabic(Convert.ToDouble(DT_LINES.Rows[i]["TKL_QTY"].ToString()).ToString("###,###.000"));// QTY
                    dr["Text15"] = Convert.ToDouble(DT_LINES.Rows[i]["TKL_QTY"].ToString()).ToString("###,###.000");// QTY                         
                }
                else
                {
                    dr["Text10"] = "0.000";
                }

                if (DT_LINES.Rows[i]["TKL_PRICE"] != null && DT_LINES.Rows[i]["TKL_PRICE"] != "")
                {
                    dr["Text11"] = DatabaseHelperClass.ConvertToArabic(Convert.ToDouble(DT_LINES.Rows[i]["TKL_PRICE"].ToString()).ToString("###,###.00"));//  Price
                    dr["Text16"] = Convert.ToDouble(DT_LINES.Rows[i]["TKL_PRICE"].ToString()).ToString("###,###.00");//  Price
                }
                else
                {
                    dr["Text11"] = "0.00";
                }

                double EXTPrice = 0;
                double VATPrice = 0;
                if (DT_LINES.Rows[i]["TKL_EXPRC"] != null && DT_LINES.Rows[i]["TKL_EXPRC"] != "")
                {
                    EXTPrice = Convert.ToDouble(DT_LINES.Rows[i]["TKL_EXPRC"].ToString());
                    dr["Text12"] = DatabaseHelperClass.ConvertToArabic(Convert.ToDouble(DT_LINES.Rows[i]["TKL_EXPRC"].ToString()).ToString("###,###.00"));// Ext Price
                    dr["Text17"] = Convert.ToDouble(DT_LINES.Rows[i]["TKL_EXPRC"].ToString()).ToString("###,###.00");// Ext Price
                }
                else
                {
                    dr["Text12"] = "0.00";
                }

                if (DT_LINES.Rows[i]["TKL_VATPRC"] != null && DT_LINES.Rows[i]["TKL_VATPRC"] != "")
                {
                    VATPrice = Convert.ToDouble(DT_LINES.Rows[i]["TKL_VATPRC"].ToString()) - EXTPrice;
                    dr["Text13"] = DatabaseHelperClass.ConvertToArabic(VATPrice.ToString("###,###.00"));//VAt Amount Price
                    dr["Text18"] = Convert.ToDouble(VATPrice.ToString()).ToString("###,###.00");//VAt Amount Price                        
                }
                else
                {
                    dr["Text13"] = "0.00";
                }
                if (DT_LINES.Rows[i]["TKL_VATPRC"] != null && DT_LINES.Rows[i]["TKL_VATPRC"] != "")
                {
                    double ProdTotalPrice = Convert.ToDouble(DT_LINES.Rows[i]["TKL_VATPRC"].ToString());
                    dr["Text14"] = DatabaseHelperClass.ConvertToArabic(Convert.ToDouble(ProdTotalPrice.ToString()).ToString("###,###.00"));  //VAt Amount Price
                    dr["Text19"] = Convert.ToDouble(ProdTotalPrice.ToString()).ToString("###,###.00");  //VAt Amount Price
                }
                else
                {
                    dr["Text14"] = "0.00";
                }
                dr["Num02"] = Convert.ToDouble(Convert.ToDouble(154884445).ToString("###,###.00")); // Total Amt

                ReportsDataSet.TEMP_TABLE.Rows.Add(dr);
            }
        }
        else
        {
            DataTable DT_HDR = dt;
            string tktNo = DT_HDR.Rows[0]["TKH_TKTNO"].ToString().Trim();
            string custNo = DT_HDR.Rows[0]["TKH_CUSNO"].ToString().Trim();
            string shipToNo = DT_HDR.Rows[0]["TKH_SHPTO"].ToString().Trim();
            string custName = DT_HDR.Rows[0]["TKH_CUSNM"].ToString().Trim();
            string custNameAR = CustomerDetails(custNo, "CUS_NAMAR");
            string custName2EN = DT_HDR.Rows[0]["TKH_CUSNM"].ToString().Trim();
            string custVat = CustomerDetails(custNo, "CUS_VATNO");
            string custAddress = CustomerDetails(custNo, "CUS_ADDR1");
            string custAddress2 = CustomerDetails(custNo, "CUS_ADDR2");
            string salesRep = DT_HDR.Rows[0]["TKH_SREP"].ToString();
            string createdBy = DT_HDR.Rows[0]["TKH_CRTBY"].ToString();
            string salesAmt = DT_HDR.Rows[0]["TKH_SLAMT"].ToString().Trim();
            string discount = DT_HDR.Rows[0]["TKH_DSCNT"].ToString().Trim();
            string NetsalesAmt = DT_HDR.Rows[0]["TKH_NTAMT"].ToString().Trim();
            string netTotal = DT_HDR.Rows[0]["TKH_AMT"].ToString().Trim();
            string vatPercent = DT_HDR.Rows[0]["TKH_VATPR"].ToString().Trim();
            string vatAmt = DT_HDR.Rows[0]["TKH_VTAMT"].ToString().Trim();
            string tktDate = Convert.ToDateTime(DT_HDR.Rows[0]["TKH_DATE"].ToString()).ToString("dd-MMM-yyyy");
            string tktTime = DT_HDR.Rows[0]["TKH_TIME"].ToString();
            string ShipToName = ShipToDetails(custNo, shipToNo, "SHP_NAME"); //custName2EN; // lblShipTo.Text;
            string ShipAddress = ShipToDetails(custNo, shipToNo, "SHP_ADDRS"); //lblShipAddress.Text;
            string ShipVia = DT_HDR.Rows[0]["TKH_SREP"].ToString(); //lblShipVia.Text;
            string ShipDate = Convert.ToDateTime(DT_HDR.Rows[0]["TKH_DATE"].ToString()).ToString("dd-MMM-yyyy"); //lblShipDate.Text;
            string PaymentType = DT_HDR.Rows[0]["TKH_PAYCD"].ToString(); //lblPayType.Text;
            string Payment1 = DT_HDR.Rows[0]["TKH_AMTPD"].ToString();
            string TotalQty = ""; //lblTotalQty.Text;
            string RegNo = DT_HDR.Rows[0]["TKH_REGNO"].ToString();
            string POno = ""; //lblPOno.Text;
            string BranchNo = DT_HDR.Rows[0]["TKH_STRNO"].ToString();
            string BranchManager = ""; //lblbranchManager.Text;

            /////////////////////////////////////////////////////
            ////////// Header Param table start here ////////////
            /////////////////////////////////////////////////////
            DataRow dr = ReportsDataSet.PARAM_TABLE.NewRow();   //Tables["TEMP_TABLE"]. 

            if (tktNo != "")
            {
                //dr["param01"] = DatabaseHelperClass.ConvertToArabic(RegNo) + DatabaseHelperClass.ConvertToArabic(tktNo); // Ticket Code
                //dr["param41"] = RegNo + tktNo; // Ticket Code english
                dr["param01"] = DatabaseHelperClass.ConvertToArabic(tktNo); // Ticket Code
                dr["param41"] = tktNo; // Ticket Code english
            }
            else
            {
                dr["param01"] = "";
                dr["param41"] = "";
            }
            if (tktDate != "")
            {
                string cptypeTktDate = GetCpTypeDate(tktDate);
                string a = cptypeTktDate.Substring(0, 4);
                string b = cptypeTktDate.Substring(4, 2);
                string c = cptypeTktDate.Substring(6, 2);
                dr["param02"] = DatabaseHelperClass.ConvertToArabic(c) + "-" + DatabaseHelperClass.ConvertToArabic(b) + "-" + DatabaseHelperClass.ConvertToArabic(a); //ticket  Date
                dr["param42"] = c + "-" + DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(Convert.ToInt32(b)) + "-" + a; //ticket  Date
            }
            else
            {
                dr["param02"] = "";
                dr["param42"] = "";
            }
            if (tktTime != "")
            {
                dr["param03"] = DatabaseHelperClass.ConvertToArabic(tktTime); //ticket Time
            }
            else
            {
                dr["param03"] = "";
            }
            if (custNo != "")
            {
                dr["param04"] = custNo; // CustNo Code
                dr["param44"] = DatabaseHelperClass.ConvertToArabic(custNo); // CustNo Code Arabic
            }
            else
            {
                dr["param04"] = "";
                dr["param44"] = ""; // CustNo Code Arabic
            }
            if (custName.Trim() != "")
            {
                dr["param05"] = "Customer Name : " + custName; // Cust Name English                  
            }
            else
            {
                dr["param05"] = custName2EN;

            }
            if (custNameAR.Trim() != "")
            {
                dr["param06"] = " أسم العميل" + " : " + DatabaseHelperClass.ConvertToArabic(custNameAR) + ""; // +": أسم العميل "; // Cust Name Arabic                   
            }
            else
            {
                dr["param06"] = "";
            }
            if (custAddress != "")
            {
                dr["param50"] = DatabaseHelperClass.ConvertToArabic(custAddress); // Cust Address Arabic
                dr["param07"] = custAddress; // Cust Address 
            }
            else
            {
                dr["param50"] = "";
                dr["param07"] = "";
            }
            if (custAddress2 != "")
            {
                dr["param51"] = DatabaseHelperClass.ConvertToArabic(custAddress2); // Cust Address2 Arabic
                dr["param19"] = custAddress2; // Cust Address2 
            }
            else
            {
                dr["param51"] = "";
                dr["param19"] = "";
            }
            if (custVat != "")
            {
                dr["param45"] = DatabaseHelperClass.ConvertToArabic(custVat) + ": رقم الضريبة "; // Cust VAT # Arabic
                dr["param08"] = "VAT No: " + custVat; // Cust VAT #
            }
            else
            {
                dr["param45"] = ""; // Cust VAT # Arabic
                dr["param08"] = "";
            }
            if (salesRep != "")
            {
                dr["param46"] = DatabaseHelperClass.ConvertToArabic(salesRep) + ": مندوب المبيعات  "; // SalesRep Arabic
                dr["param09"] = "Salesman: " + salesRep; // SalesRep
            }
            else
            {
                dr["param09"] = "";
                dr["param46"] = ""; // SalesRep Arabic
            }
            if (createdBy != "")
            {
                dr["param20"] = createdBy; // UserID
            }
            else
            {
                dr["param20"] = "";
            }

            ////////////////////////////////////////


            if (shipToNo != "")
            {
                dr["param49"] = DatabaseHelperClass.ConvertToArabic(shipToNo); //Ship TO Arabic
                dr["param10"] = shipToNo; //Ship TO
            }
            else
            {
                dr["param49"] = ""; //Ship TO Arabic
                dr["param10"] = "";
            }
            if (ShipVia != "")
            {
                dr["param53"] = DatabaseHelperClass.ConvertToArabic(ShipVia) + ": الشحن ب "; //Ship Via Arabic
                dr["param11"] = "Ship Via : " + ShipVia; //Ship Via
            }
            else
            {
                dr["param53"] = "";
                dr["param11"] = "";
            }
            if (ShipDate != "")
            {
                string cptypeShipDate = GetCpTypeDate(ShipDate);
                string a = cptypeShipDate.Substring(0, 4);
                string b = cptypeShipDate.Substring(4, 2);
                string c = cptypeShipDate.Substring(6, 2);
                dr["param12"] = "تاريخ الشحن " + " : " + DatabaseHelperClass.ConvertToArabic(a) + "-" + DatabaseHelperClass.ConvertToArabic(b) + "-" + DatabaseHelperClass.ConvertToArabic(c); //Ship Date
                dr["param43"] = "Ship Date : " + c + "-" + DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(Convert.ToInt32(b)) + "-" + a; //Ship Date
            }
            else
            {
                dr["param12"] = "";
                dr["param43"] = ""; //Ship Date eng
            }
            if (ShipToName != "" || ShipAddress != "")
            {
                dr["param48"] = DatabaseHelperClass.ConvertToArabic(ShipToName + " " + ShipAddress); //Ship To(Address)/City/ State / Zip Arabic
                dr["param13"] = ShipToName + " " + ShipAddress; //Ship To(Address)/City/ State / Zip
            }
            else
            {
                dr["param48"] = "";
                dr["param13"] = "";
            }
            if (RegNo != "")
            {
                //dr["param14"] = DatabaseHelperClass.ConvertToArabic(RegNo); //Reg No Arabic
                dr["param14"] = DatabaseHelperClass.ConvertToArabic(RegNo); //Reg No
                dr["param38"] = RegNo; //Reg No
            }
            else
            {
                dr["param14"] = "";
                dr["param38"] = "";
            }
            if (POno != "")
            {
                if (POno.Trim() != "")
                {
                    dr["param47"] = DatabaseHelperClass.ConvertToArabic(POno) + " : رقم طلب الشراء"; //PO No Arabic
                    dr["param15"] = "PO # : " + POno; //PO No
                }
            }
            else
            {
                dr["param47"] = ""; //PO No Arabic
                dr["param15"] = "";
            }
            if (discount != "" && discount != "0.00" && discount != "0")
            {
                dr["param16"] = "الخصم"; //Arabic Discount Label
            }
            else
            {
                dr["param16"] = "";
            }
            if (discount != "" && discount != "0.00" && discount != "0")
            {
                dr["param17"] = "Discount    :"; //English Discount Label
            }
            else
            {
                dr["param17"] = "";
            }

            if (PaymentType != "")
            {
                dr["param31"] = PaymentType + " :"; //Payment TYpe
                //DataTable PymentDT = dbHlprACCDB.FetchData("Select * from STP_MST_PYMNT WHERE PMT_CODE='" + PaymentType.Trim() + "'");
                //if (PymentDT.Rows.Count > 0)
                //{
                //    dr["param31"] = PymentDT.Rows[0]["PMT_DESC1"].ToString() + " :"; //Payment TYpe                                                     
                //}
            }
            else
            {
                dr["param31"] = "";
            }
            if (Payment1 != "")
            {
                dr["param32"] = DatabaseHelperClass.ConvertToArabic(Payment1); //Payment 1
                dr["param36"] = Convert.ToDouble(Payment1).ToString("###,###.00"); //Payment 1
            }
            else
            {
                dr["param32"] = "";
                dr["param36"] = "";
            }
            if (BranchNo != "")
            {
                dr["param33"] = DatabaseHelperClass.ConvertToArabic(BranchNo); // Branch Code
                dr["param37"] = BranchNo; // Branch Code
            }
            else
            {
                dr["param33"] = "";
                dr["param37"] = "";
            }
            if (BranchManager != "")
            {
                dr["param34"] = BranchManager; //Branch Manager / Approved By                    
            }
            else
            {
                dr["param34"] = "";
            }


            //////////////////////////////////
            ///////////////////////////////////


            if (salesAmt != "")
            {
                dr["param21"] = DatabaseHelperClass.ConvertToArabic(Convert.ToDouble(salesAmt).ToString("###,###.00")); //Total sales Amt
                //   dr["param21"] = DatabaseHelperClass.ConvertToArabic(Convert.ToDouble(salesAmt).ToString()); //Total sales Amt
                dr["param26"] = Convert.ToDouble(salesAmt).ToString("###,###.00");
            }
            else
            {
                dr["param21"] = "";
                dr["param26"] = "";
            }


            if (discount != "" && discount != "0.00" && discount != "0")
            {
                dr["param22"] = DatabaseHelperClass.ConvertToArabic(Convert.ToDouble(discount).ToString("###,###.00")); // discount AMT
                //   dr["param22"] = DatabaseHelperClass.ConvertToArabic(Convert.ToDouble(discount).ToString()); // discount AMT
                dr["param27"] = Convert.ToDouble(discount).ToString("###,###.00"); // discount AMT
            }
            else
            {
                dr["param22"] = "";
                dr["param27"] = "";
            }
            if (NetsalesAmt != "")
            {
                dr["param23"] = DatabaseHelperClass.ConvertToArabic(Convert.ToDouble(NetsalesAmt).ToString("###,###.00")); //Net sales Amt -٣,٦٣٠.٠٠
                //  dr["param23"] = DatabaseHelperClass.ConvertToArabic(Convert.ToDouble(NetsalesAmt).ToString()); //Net sales Amt 
                dr["param28"] = Convert.ToDouble(NetsalesAmt).ToString("###,###.00"); //Net sales Amt
            }
            else
            {
                dr["param23"] = "";
                dr["param28"] = ""; //Net sales Amt
            }
            if (vatPercent != "")
            {
                if (vatPercent == "Mixed")
                {
                    dr["param58"] = "متعدد";
                    dr["param59"] = vatPercent;
                }
                else
                {
                    try
                    {
                        dr["param58"] = DatabaseHelperClass.ConvertToArabic(Convert.ToDouble(vatPercent.Replace("%", "").Trim()).ToString("###,###.00")); // VAT Percentage
                    }
                    catch (Exception x)
                    {
                        dr["param58"] = vatPercent;
                    }
                    try
                    {
                        dr["param59"] = Convert.ToDouble(Convert.ToDouble(vatPercent.Replace("%", "").Trim())).ToString("###,###.00"); // VAT Percentage
                    }
                    catch (Exception de)
                    {
                        dr["param59"] = vatPercent;
                    }
                }
            }
            else
            {
                dr["param58"] = "";
                dr["param59"] = "";
            }
            if (vatAmt != "")
            {
                dr["param24"] = DatabaseHelperClass.ConvertToArabic(Convert.ToDouble(vatAmt).ToString("###,###.00")); // VAT Amt
                // dr["param24"] = DatabaseHelperClass.ConvertToArabic(Convert.ToDouble(vatAmt).ToString()); // VAT Amt
                dr["param29"] = Convert.ToDouble(vatAmt).ToString("###,###.00"); // VAT Amt
            }
            else
            {
                dr["param24"] = "";
                dr["param29"] = "";
            }
            if (netTotal != "")
            {
                dr["param25"] = DatabaseHelperClass.ConvertToArabic(Convert.ToDouble(netTotal).ToString("###,###.00")); // Total Amt
                //   dr["param25"] = string.Format(new System.Globalization.CultureInfo("ar-SA"), "{0:C}", dr["param25"]);


                // dr["param25"] = DatabaseHelperClass.ConvertToArabic(Convert.ToDouble(netTotal).ToString()); // Total Amt
                dr["param30"] = Convert.ToDouble(netTotal).ToString("###,###.00"); // Total Amt
            }
            else
            {
                dr["param25"] = "";
                dr["param30"] = "";
            }


            if (TotalQty != "")
            {
                dr["param39"] = DatabaseHelperClass.ConvertToArabic(Convert.ToDouble(TotalQty).ToString("###,###.000")); // Total Qty
                // dr["param39"] = DatabaseHelperClass.ConvertToArabic(Convert.ToDouble(TotalQty).ToString()); // Total Qty
                dr["param35"] = Convert.ToDouble(TotalQty).ToString("###,###.000"); // Total Qty
            }
            else
            {
                dr["param39"] = "";
                dr["param35"] = ""; // Total Qty
            }

            if (Convert.ToDouble(salesAmt) < 0)
            {
                dr["param54"] = "Credit Note"; // Report Type
                dr["param55"] = "مردود مبيعات"; // Report Type
                dr["param56"] = "** Return ** "; // Report Type
                dr["param57"] = "** ارجاع ** "; // Report Type
            }
            else
            {
                dr["param54"] = "Invoice";// Report Type
                dr["param55"] = "الفاتورة"; // Report Type
                dr["param56"] = "";
                dr["param57"] = "";
            }

            ReportsDataSet.PARAM_TABLE.Rows.Add(dr);

        }
    }

    public void ReceiptReportTempData(DataTable DTTemptbl, int A)
    {
        if (this.SalesType == "Sales")
        {
            if (A != 1)
            {
                for (int i = 0; i < DTTemptbl.Rows.Count; i++)
                {
                    DataRow dr = ReportsDataSet.TEMP_TABLE.NewRow();   //Tables["TEMP_TABLE"]. 
                    if (DTTemptbl.Rows[i]["TKL_TKTNO"] != null && DTTemptbl.Rows[i]["TKL_TKTNO"] != "")
                    {
                        dr["Text01"] = DTTemptbl.Rows[i]["TKL_TKTNO"].ToString(); // Ticket No
                    }
                    else
                    {
                        dr["Text01"] = "";
                    }
                    if (DTTemptbl.Rows[i]["TKL_ITMNO"] != null && DTTemptbl.Rows[i]["TKL_ITMNO"] != "")
                    {
                        dr["Text02"] = DTTemptbl.Rows[i]["TKL_ITMNO"].ToString(); // Item No
                        string ProdDesc2 = GetItemArabicDesc(DTTemptbl.Rows[i]["TKL_ITMNO"].ToString());
                        dr["Text11"] = ProdDesc2; // Customer Contact
                    }
                    else
                    {
                        dr["Text02"] = "";
                        dr["Text11"] = "";
                    }


                    if (DTTemptbl.Rows[i]["TKL_DESC"] != null && DTTemptbl.Rows[i]["TKL_DESC"] != "")
                    {
                        dr["Text03"] = DTTemptbl.Rows[i]["TKL_DESC"].ToString(); // PROD Catg
                    }
                    else
                    {
                        dr["Text03"] = "";
                    }
                    if (DTTemptbl.Rows[i]["TKL_UOM"] != null && DTTemptbl.Rows[i]["TKL_UOM"] != "")
                    {
                        dr["Text04"] = DTTemptbl.Rows[i]["TKL_UOM"].ToString(); // UOM
                    }
                    else
                    {
                        dr["Text04"] = "";
                    }

                    if (DTTemptbl.Rows[i]["TKL_CUSNO"] != null && DTTemptbl.Rows[i]["TKL_CUSNO"] != "")
                    {
                        dr["Text05"] = DTTemptbl.Rows[i]["TKL_CUSNO"].ToString(); // Catg No
                    }
                    else
                    {
                        dr["Text05"] = "";
                    }
                    if (DTTemptbl.Rows[i]["TKL_CUSNM"] != null && DTTemptbl.Rows[i]["TKL_CUSNM"] != "")
                    {
                        dr["Text06"] = DTTemptbl.Rows[i]["TKL_CUSNM"].ToString(); // cust name
                    }
                    else
                    {
                        dr["Text06"] = "";
                    }
                    if (DTTemptbl.Rows[i]["TKL_SREP"] != null && DTTemptbl.Rows[i]["TKL_SREP"] != "")
                    {
                        dr["Text07"] = DTTemptbl.Rows[i]["TKL_SREP"].ToString().ToUpper(); // SLS rep
                    }
                    else
                    {
                        dr["Text07"] = "";
                    }
                    if (DTTemptbl.Rows[i]["TKL_DATE"] != null && DTTemptbl.Rows[i]["TKL_DATE"] != "")
                    {
                        dr["Text08"] = DTTemptbl.Rows[i]["TKL_DATE"].ToString(); // Date
                    }
                    else
                    {
                        dr["Text08"] = "";
                    }
                    if (DTTemptbl.Rows[i]["TKL_TIME"] != null && DTTemptbl.Rows[i]["TKL_TIME"] != "")
                    {
                        dr["Text09"] = DTTemptbl.Rows[i]["TKL_TIME"].ToString(); //Time
                    }
                    else
                    {
                        dr["Text09"] = "";
                    }
                    if (DTTemptbl.Rows[i]["TKL_TTYPE"] != null && DTTemptbl.Rows[i]["TKL_TTYPE"] != "")
                    {
                        dr["Text10"] = DTTemptbl.Rows[i]["TKL_TTYPE"].ToString(); // TRX Type
                    }
                    else
                    {
                        dr["Text10"] = "";
                    }
                    /////////////// num starts here /////////////////
                    if (DTTemptbl.Rows[i]["TKL_QTY"] != null && DTTemptbl.Rows[i]["TKL_QTY"] != "")
                    {
                        dr["Num01"] = DTTemptbl.Rows[i]["TKL_QTY"].ToString();// QTY
                    }
                    else
                    {
                        dr["Num01"] = "0.00";
                    }
                    if (DTTemptbl.Rows[i]["TKL_PRICE"] != null && DTTemptbl.Rows[i]["TKL_PRICE"] != "")
                    {
                        dr["Num02"] = DTTemptbl.Rows[i]["TKL_PRICE"].ToString();// Price
                    }
                    else
                    {
                        dr["Num02"] = "0.00";
                    }
                    if (DTTemptbl.Rows[i]["TKL_EXPRC"] != null && DTTemptbl.Rows[i]["TKL_EXPRC"] != "")
                    {
                        dr["Num03"] = DTTemptbl.Rows[i]["TKL_EXPRC"].ToString();// Ex Price
                    }
                    else
                    {
                        dr["Num03"] = "0.00";
                    }
                    if (DTTemptbl.Rows[i]["TKL_VATPRC"] != null && DTTemptbl.Rows[i]["TKL_VATPRC"] != "")
                    {
                        dr["Num04"] = DTTemptbl.Rows[i]["TKL_VATPRC"].ToString();// VAT Price
                    }
                    else
                    {
                        dr["Num04"] = "0.00";
                    }
                    ReportsDataSet.TEMP_TABLE.Rows.Add(dr);
                }
            }
            else
            {
                /////////////////////
                ////////// Header Param table start here
                /////////////////////
                DataRow dr = ReportsDataSet.PARAM_TABLE.NewRow();   //Tables["TEMP_TABLE"]. 
                //Company Details starts
                DataTable CompDetailsDT = CompanyDetails("FORSAN");
                //
                if (CompDetailsDT.Rows[0]["CMP_NAME"] != null && CompDetailsDT.Rows[0]["CMP_NAME"] != "")
                {
                    dr["param11"] = CompDetailsDT.Rows[0]["CMP_NAME"].ToString(); // Company Name
                }
                else
                {
                    dr["param11"] = "Forsan Foods & Consumer Products Co. Ltd";
                }
                if (CompDetailsDT.Rows[0]["CMP_ANAME"] != null && CompDetailsDT.Rows[0]["CMP_ANAME"] != "")
                {
                    dr["param12"] = CompDetailsDT.Rows[0]["CMP_ANAME"].ToString(); // Arabic Name
                }
                else
                {
                    dr["param12"] = "الغذائىة الفرسان - مبيعات فان";
                }
                if (CompDetailsDT.Rows[0]["CMP_VATNO"] != null && CompDetailsDT.Rows[0]["CMP_VATNO"] != "")
                {
                    dr["param13"] = "VAT # " + CompDetailsDT.Rows[0]["CMP_VATNO"].ToString(); // VAT #
                }
                else
                {
                    dr["param13"] = "VAT # 300056053500003 ";
                }
                if (CompDetailsDT.Rows[0]["CMP_ADRES"] != null && CompDetailsDT.Rows[0]["CMP_ADRES"] != "")
                {
                    dr["param14"] = CompDetailsDT.Rows[0]["CMP_ADRES"].ToString(); // Address
                }
                else
                {
                    dr["param14"] = "Pri. Sultan Abdul Aziz St ,Riyadh";
                }
                if (CompDetailsDT.Rows[0]["CMP_CNTCT"] != null && CompDetailsDT.Rows[0]["CMP_CNTCT"] != "")
                {
                    dr["param15"] = "Tel # " + CompDetailsDT.Rows[0]["CMP_CNTCT"].ToString(); // Telephone
                }
                else
                {
                    dr["param15"] = "Tel # (011) 416-4422";
                }
                if (CompDetailsDT.Rows[0]["CMP_MOBIL"] != null && CompDetailsDT.Rows[0]["CMP_MOBIL"] != "")
                {
                    dr["param16"] = "Mobile # " + CompDetailsDT.Rows[0]["CMP_MOBIL"].ToString(); // Mobile
                }
                else
                {
                    dr["param16"] = "Mobile # 055-816-1122";
                }
                //changed
                if (DTTemptbl.Rows[0]["TKH_REGNO"] != null && DTTemptbl.Rows[0]["TKH_REGNO"].ToString() != "")
                {
                    dr["param17"] = DTTemptbl.Rows[0]["TKH_REGNO"];  // lblRegisterNo.Text; // Time
                }
                else
                {
                    dr["param17"] = "";
                }
                if (Session["UserId"] != null && Session["UserId"].ToString() != "")
                {
                    dr["param18"] = Session["UserId"].ToString().ToUpper(); // lblDrawerNo.Text; // Time
                }
                else
                {
                    dr["param18"] = "";
                }
                //changed end

                //Company Details Ends

                if (DTTemptbl.Rows[0]["TKH_CUSNO"] != null && DTTemptbl.Rows[0]["TKH_CUSNO"] != "")
                {
                    dr["param19"] = DTTemptbl.Rows[0]["TKH_CUSNO"].ToString(); // Ticket Code
                }
                else
                {
                    dr["param19"] = "-";
                }
                if (DTTemptbl.Rows[0]["TKH_SREP"] != null && DTTemptbl.Rows[0]["TKH_SREP"] != "")
                {
                    dr["param20"] = DTTemptbl.Rows[0]["TKH_SREP"].ToString().ToUpper(); // Ticket Code
                }
                else
                {
                    dr["param20"] = "-";
                }


                if (DTTemptbl.Rows[0]["TKH_TKTNO"] != null && DTTemptbl.Rows[0]["TKH_TKTNO"] != "")
                {
                    dr["param01"] = DTTemptbl.Rows[0]["TKH_TKTNO"].ToString(); // Ticket Code
                }
                else
                {
                    dr["param01"] = "";
                }
                //if (DTTemptbl.Rows[0]["TKH_CUSNO"] != null && DTTemptbl.Rows[0]["TKH_CUSNO"] != "")
                //{
                //    dr["param02"] = DTTemptbl.Rows[0]["TKH_CUSNO"].ToString(); // Customer No
                //}
                //else
                //{
                //    dr["param02"] = "";
                //}
                if (DTTemptbl.Rows[0]["TKH_CUSNO"] != null && DTTemptbl.Rows[0]["TKH_CUSNO"] != "")
                {
                    dr["param02"] = DTTemptbl.Rows[0]["TKH_CUSNO"].ToString(); // Customer No

                    dr["param09"] = CustomerDetails(DTTemptbl.Rows[0]["TKH_CUSNO"].ToString(), "CUS_CNTC1");
                    dr["param05"] = CustomerDetails(DTTemptbl.Rows[0]["TKH_CUSNO"].ToString(), "CUS_NAMAR");
                }
                else
                {
                    dr["param02"] = "";
                    dr["param09"] = "";
                    dr["param05"] = "";
                }

                if (DTTemptbl.Rows[0]["TKH_CUSNM"] != null && DTTemptbl.Rows[0]["TKH_CUSNM"] != "")
                {
                    dr["param03"] = DTTemptbl.Rows[0]["TKH_CUSNM"].ToString(); // Cust No
                }
                else
                {
                    dr["param03"] = "";
                }
                if (DTTemptbl.Rows[0]["TKH_CUSVT"] != null && DTTemptbl.Rows[0]["TKH_CUSVT"] != "")
                {
                    dr["param04"] = DTTemptbl.Rows[0]["TKH_CUSVT"].ToString(); // CUS VAT #
                }
                else
                {
                    dr["param04"] = "";
                }
                if (DTTemptbl.Rows[0]["TKH_DATE"] != null && DTTemptbl.Rows[0]["TKH_DATE"] != "")
                {
                    DateTime DTtmp = Convert.ToDateTime(DTTemptbl.Rows[0]["TKH_DATE"].ToString());
                    dr["param06"] = DTtmp.ToString("dd-MMM-yyyy, HH:mm"); // Date                   
                }
                else
                {
                    dr["param06"] = "";
                }
                if (DTTemptbl.Rows[0]["TKH_TIME"] != null && DTTemptbl.Rows[0]["TKH_TIME"] != "")
                {
                    dr["param07"] = DTTemptbl.Rows[0]["TKH_TIME"].ToString(); // Time
                }
                else
                {
                    dr["param07"] = "";
                }
                if (DTTemptbl.Rows[0]["TKH_SHPTO"] != null && DTTemptbl.Rows[0]["TKH_SHPTO"] != "")
                {
                    string shiptoname = ShipToDetails(DTTemptbl.Rows[0]["TKH_CUSNO"].ToString(), DTTemptbl.Rows[0]["TKH_SHPTO"].ToString(), "SHP_NAME");
                    string shiptoaddress = ShipToDetails(DTTemptbl.Rows[0]["TKH_CUSNO"].ToString(), DTTemptbl.Rows[0]["TKH_SHPTO"].ToString(), "SHP_ADDRS");

                    dr["param30"] = shiptoname + " " + shiptoaddress;
                }
                else
                {
                    dr["param30"] = "";
                }
                if (DTTemptbl.Rows[0]["TKH_SLAMT"] != null && DTTemptbl.Rows[0]["TKH_SLAMT"] != "")
                {
                    dr["param31"] = Convert.ToDouble(DTTemptbl.Rows[0]["TKH_SLAMT"].ToString()).ToString("n2"); // sales amount
                }
                else
                {
                    dr["param31"] = "";
                }

                if (DTTemptbl.Rows[0]["TKH_DSCNT"] != null && DTTemptbl.Rows[0]["TKH_DSCNT"] != "")
                {
                    dr["param32"] = Convert.ToDouble(DTTemptbl.Rows[0]["TKH_DSCNT"].ToString()).ToString("n2"); //Discount
                }
                else
                {
                    dr["param32"] = "";
                }

                if (DTTemptbl.Rows[0]["TKH_NTAMT"] != null && DTTemptbl.Rows[0]["TKH_NTAMT"] != "")
                {
                    dr["param33"] = Convert.ToDouble(DTTemptbl.Rows[0]["TKH_NTAMT"].ToString()).ToString("n2");// net amount
                }
                else
                {
                    dr["param33"] = "0.00";
                }


                if (DTTemptbl.Rows[0]["TKH_VTAMT"] != null && DTTemptbl.Rows[0]["TKH_VTAMT"] != "")
                {
                    dr["param34"] = Convert.ToDouble(DTTemptbl.Rows[0]["TKH_VTAMT"].ToString()).ToString("n2"); // VAT Amount
                }
                else
                {
                    dr["param34"] = "";
                }

                if (DTTemptbl.Rows[0]["TKH_AMT"] != null && DTTemptbl.Rows[0]["TKH_AMT"] != "")
                {
                    dr["param35"] = Convert.ToDouble(DTTemptbl.Rows[0]["TKH_AMT"].ToString()).ToString("n2");// Total Amount
                }
                else
                {
                    dr["param35"] = "0.00";
                }
                if (DTTemptbl.Rows[0]["TKH_AMTPD"] != null && DTTemptbl.Rows[0]["TKH_AMTPD"] != "")
                {
                    dr["param36"] = Convert.ToDouble(DTTemptbl.Rows[0]["TKH_AMTPD"].ToString()).ToString("n2");// Amount Paid
                }
                else
                {
                    dr["param36"] = "0.00";
                }
                if (DTTemptbl.Rows[0]["TKH_CHNGE"] != null && DTTemptbl.Rows[0]["TKH_CHNGE"] != "")
                {
                    dr["param37"] = Convert.ToDouble(DTTemptbl.Rows[0]["TKH_CHNGE"].ToString()).ToString("n2");// Change Due
                }
                else
                {
                    dr["param37"] = "0.00";
                }//TKH_BLANC
                if (DTTemptbl.Rows[0]["TKH_BLANC"] != null && DTTemptbl.Rows[0]["TKH_BLANC"] != "")
                {
                    dr["param39"] = Convert.ToDouble(DTTemptbl.Rows[0]["TKH_BLANC"].ToString()).ToString("n2");// Balance Due
                }
                else
                {
                    dr["param39"] = "0.00";
                }//TKH_BLANC

                if (DTTemptbl.Rows[0]["TKH_VATPR"] != null && DTTemptbl.Rows[0]["TKH_VATPR"] != "")
                {
                    dr["param28"] = DTTemptbl.Rows[0]["TKH_VATPR"].ToString();// Balance Due
                }
                else
                {
                    dr["param28"] = "-";
                }//TKH_Tax Factor

                if (DTTemptbl.Rows[0]["TKH_TTYPE"] != null && DTTemptbl.Rows[0]["TKH_TTYPE"] != "")
                {
                    dr["param08"] = DTTemptbl.Rows[0]["TKH_TTYPE"].ToString();// TYPE
                }
                else
                {
                    dr["param08"] = "0.00";
                }

                if (DTTemptbl.Rows[0]["TKH_PAYCD"] != null && DTTemptbl.Rows[0]["TKH_PAYCD"] != "")
                {
                    if (DTTemptbl.Rows[0]["TKH_PAYCD"] == "A/R")
                    {
                        dr["param10"] = "Credit";
                    }
                    else
                    {
                        dr["param10"] = DTTemptbl.Rows[0]["TKH_PAYCD"].ToString();
                    }
                }
                else
                {
                    dr["param10"] = "-";
                }

                if (DTTemptbl.Rows[0]["TKH_PAYCD"] != null && !DTTemptbl.Rows[0]["TKH_PAYCD"].ToString().Equals("A/R"))
                {
                    dr["param21"] = "Amount Paid";
                }
                else
                {
                    dr["param21"] = "";
                }

                if (DTTemptbl.Rows[0]["TKH_PAYCD"] != null && !DTTemptbl.Rows[0]["TKH_PAYCD"].ToString().Equals("A/R"))
                {
                    dr["param22"] = "Change Due";
                }
                else
                {
                    dr["param22"] = "";
                    dr["param36"] = DBNull.Value;
                    dr["param37"] = DBNull.Value;
                }

                ReportsDataSet.PARAM_TABLE.Rows.Add(dr);

            }
        }
        else
        {
            if (A != 1)
            {
                for (int i = 0; i < DTTemptbl.Rows.Count; i++)
                {
                    DataRow dr = ReportsDataSet.TEMP_TABLE.NewRow();   //Tables["TEMP_TABLE"]. 
                    if (DTTemptbl.Rows[i]["TKL_TKTNO"] != null && DTTemptbl.Rows[i]["TKL_TKTNO"].ToString() != "")
                    {
                        dr["Text01"] = DTTemptbl.Rows[i]["TKL_TKTNO"].ToString(); // Ticket No
                    }
                    else
                    {
                        dr["Text01"] = "";
                    }
                    if (DTTemptbl.Rows[i]["TKL_ITMNO"] != null && DTTemptbl.Rows[i]["TKL_ITMNO"] != "")
                    {
                        dr["Text02"] = DTTemptbl.Rows[i]["TKL_ITMNO"].ToString(); // Item No
                        string ProdDesc2 = GetItemArabicDesc(DTTemptbl.Rows[i]["TKL_ITMNO"].ToString());
                        dr["Text11"] = ProdDesc2; // Customer Contact
                    }
                    else
                    {
                        dr["Text02"] = "";
                        dr["Text11"] = "";
                    }

                    if (DTTemptbl.Rows[i]["TKL_DESC"] != null && DTTemptbl.Rows[i]["TKL_DESC"].ToString() != "")
                    {
                        dr["Text03"] = DTTemptbl.Rows[i]["TKL_DESC"].ToString(); // PROD Catg
                    }
                    else
                    {
                        dr["Text03"] = "";
                    }
                    if (DTTemptbl.Rows[i]["TKL_UOM"] != null && DTTemptbl.Rows[i]["TKL_UOM"].ToString() != "")
                    {
                        dr["Text04"] = DTTemptbl.Rows[i]["TKL_UOM"].ToString(); // UOM
                    }
                    else
                    {
                        dr["Text04"] = "";
                    }
                    if (DTTemptbl.Rows[i]["TKL_CUSNO"] != null && DTTemptbl.Rows[i]["TKL_CUSNO"] != "")
                    {
                        dr["Text05"] = DTTemptbl.Rows[i]["TKL_CUSNO"].ToString(); // Catg No
                    }
                    else
                    {
                        dr["Text05"] = "";
                    }
                    if (DTTemptbl.Rows[i]["TKL_CUSNM"] != null && DTTemptbl.Rows[i]["TKL_CUSNM"] != "")
                    {
                        dr["Text06"] = DTTemptbl.Rows[i]["TKL_CUSNM"].ToString(); // cust name
                    }
                    else
                    {
                        dr["Text06"] = "";
                    }
                    if (DTTemptbl.Rows[i]["TKL_SREP"] != null && DTTemptbl.Rows[i]["TKL_SREP"] != "")
                    {
                        dr["Text07"] = DTTemptbl.Rows[i]["TKL_SREP"].ToString().ToUpper(); // SLS rep
                    }
                    else
                    {
                        dr["Text07"] = "";
                    }
                    if (DTTemptbl.Rows[i]["TKL_DATE"] != null && DTTemptbl.Rows[i]["TKL_DATE"] != "")
                    {
                        dr["Text08"] = DTTemptbl.Rows[i]["TKL_DATE"].ToString(); // Date
                    }
                    else
                    {
                        dr["Text08"] = "";
                    }
                    if (DTTemptbl.Rows[i]["TKL_TIME"] != null && DTTemptbl.Rows[i]["TKL_TIME"] != "")
                    {
                        dr["Text09"] = DTTemptbl.Rows[i]["TKL_TIME"].ToString(); //Time
                    }
                    else
                    {
                        dr["Text09"] = "";
                    }
                    if (DTTemptbl.Rows[i]["TKL_TTYPE"] != null && DTTemptbl.Rows[i]["TKL_TTYPE"] != "")
                    {
                        dr["Text10"] = DTTemptbl.Rows[i]["TKL_TTYPE"].ToString(); // TRX Type
                        this.SalesType = DTTemptbl.Rows[i]["TKL_TTYPE"].ToString();
                    }
                    else
                    {
                        dr["Text10"] = "";
                        this.SalesType = "";
                    }
                    //if (DTTemptbl.Rows[i]["TKL_TIME"] != null && DTTemptbl.Rows[i]["TKL_TIME"] != "")
                    //{
                    //    dr["Text11"] = DTTemptbl.Rows[i]["TKL_TIME"].ToString(); //Time
                    //}
                    //else
                    //{
                    //    dr["Text11"] = "";
                    //}
                    //if (DTTemptbl.Rows[i]["TKL_TTYPE"] != null && DTTemptbl.Rows[i]["TKL_TTYPE"] != "")
                    //{
                    //    dr["Text12"] = DTTemptbl.Rows[i]["TKL_TTYPE"].ToString(); // TRX Type
                    //}
                    //else
                    //{
                    //    dr["Text12"] = "";
                    //}
                    /////////////// num starts here /////////////////
                    if (DTTemptbl.Rows[i]["TKL_QTY"] != null && DTTemptbl.Rows[i]["TKL_QTY"] != "")
                    {
                        dr["Num01"] = DTTemptbl.Rows[i]["TKL_QTY"].ToString();// QTY
                    }
                    else
                    {
                        dr["Num01"] = "0.00";
                    }
                    if (DTTemptbl.Rows[i]["TKL_PRICE"] != null && DTTemptbl.Rows[i]["TKL_PRICE"] != "")
                    {
                        dr["Num02"] = Convert.ToDouble(DTTemptbl.Rows[i]["TKL_PRICE"].ToString()).ToString("n2");// Price
                    }
                    else
                    {
                        dr["Num02"] = "0.00";
                    }
                    if (DTTemptbl.Rows[i]["TKL_EXPRC"] != null && DTTemptbl.Rows[i]["TKL_EXPRC"] != "")
                    {
                        dr["Num03"] = Convert.ToDouble(DTTemptbl.Rows[i]["TKL_EXPRC"].ToString()).ToString("n2");// Ex Price
                    }
                    else
                    {
                        dr["Num03"] = "0.00";
                    }
                    if (DTTemptbl.Rows[i]["TKL_VATPRC"] != null && DTTemptbl.Rows[i]["TKL_VATPRC"] != "")
                    {
                        dr["Num04"] = Convert.ToDouble(DTTemptbl.Rows[i]["TKL_VATPRC"].ToString()).ToString("n2");// VAT Price
                    }
                    else
                    {
                        dr["Num04"] = "0.00";
                    }
                    ReportsDataSet.TEMP_TABLE.Rows.Add(dr);
                }
            }
            else
            {
                /////////////////////
                ////////// Header Param table start here
                /////////////////////

                DataRow dr = ReportsDataSet.PARAM_TABLE.NewRow();   //Tables["TEMP_TABLE"]. 

                // Company Details Start
                //Company Details starts
                DataTable CompDetailsDT = CompanyDetails("FORSAN");
                //
                if (CompDetailsDT.Rows[0]["CMP_NAME"] != null && CompDetailsDT.Rows[0]["CMP_NAME"] != "")
                {
                    dr["param11"] = CompDetailsDT.Rows[0]["CMP_NAME"].ToString(); // Company Name
                }
                else
                {
                    dr["param11"] = "Forsan Foods & Consumer Products Co. Ltd";
                }
                if (CompDetailsDT.Rows[0]["CMP_ANAME"] != null && CompDetailsDT.Rows[0]["CMP_ANAME"] != "")
                {
                    dr["param12"] = CompDetailsDT.Rows[0]["CMP_ANAME"].ToString(); // Arabic Name
                }
                else
                {
                    dr["param12"] = "الغذائىة الفرسان - مبيعات فان";
                }
                if (CompDetailsDT.Rows[0]["CMP_VATNO"] != null && CompDetailsDT.Rows[0]["CMP_VATNO"] != "")
                {
                    dr["param13"] = "VAT # " + CompDetailsDT.Rows[0]["CMP_VATNO"].ToString(); // VAT #
                }
                else
                {
                    dr["param13"] = "VAT # 300056053500003 ";
                }
                if (CompDetailsDT.Rows[0]["CMP_ADRES"] != null && CompDetailsDT.Rows[0]["CMP_ADRES"] != "")
                {
                    dr["param14"] = CompDetailsDT.Rows[0]["CMP_ADRES"].ToString(); // Address
                }
                else
                {
                    dr["param14"] = "Pri. Sultan Abdul Aziz St ,Riyadh";
                }
                if (CompDetailsDT.Rows[0]["CMP_CNTCT"] != null && CompDetailsDT.Rows[0]["CMP_CNTCT"] != "")
                {
                    dr["param15"] = "Tel # " + CompDetailsDT.Rows[0]["CMP_CNTCT"].ToString(); // Telephone
                }
                else
                {
                    dr["param15"] = "Tel # (011) 416-4422";
                }
                if (CompDetailsDT.Rows[0]["CMP_MOBIL"] != null && CompDetailsDT.Rows[0]["CMP_MOBIL"] != "")
                {
                    dr["param16"] = "Mobile # " + CompDetailsDT.Rows[0]["CMP_MOBIL"].ToString(); // Mobile
                }
                else
                {
                    dr["param16"] = "Mobile # 055-816-1122";
                }
                // Company Details End

                if (DTTemptbl.Rows[0]["TKH_CUSNO"] != null && DTTemptbl.Rows[0]["TKH_CUSNO"] != "")
                {
                    dr["param19"] = DTTemptbl.Rows[0]["TKH_CUSNO"].ToString(); // Ticket Code                       
                }
                else
                {
                    dr["param19"] = "-";
                }
                if (DTTemptbl.Rows[0]["TKH_SREP"] != null && DTTemptbl.Rows[0]["TKH_SREP"] != "")
                {
                    dr["param20"] = DTTemptbl.Rows[0]["TKH_SREP"].ToString().ToUpper(); // Ticket Code
                }
                else
                {
                    dr["param20"] = "-";
                }

                if (DTTemptbl.Rows[0]["TKH_TKTNO"] != null && DTTemptbl.Rows[0]["TKH_TKTNO"] != "")
                {
                    dr["param01"] = DTTemptbl.Rows[0]["TKH_TKTNO"].ToString(); // Ticket Code
                }
                else
                {
                    dr["param01"] = "";
                }
                if (DTTemptbl.Rows[0]["TKH_CUSNO"] != null && DTTemptbl.Rows[0]["TKH_CUSNO"] != "")
                {
                    dr["param02"] = DTTemptbl.Rows[0]["TKH_CUSNO"].ToString(); // Customer No

                    dr["param09"] = CustomerDetails(DTTemptbl.Rows[0]["TKH_CUSNO"].ToString(), "CUS_CNTC1");
                    dr["param05"] = CustomerDetails(DTTemptbl.Rows[0]["TKH_CUSNO"].ToString(), "CUS_NAMAR");
                }
                else
                {
                    dr["param02"] = "";
                    dr["param09"] = "";
                    dr["param05"] = "";
                }

                if (DTTemptbl.Rows[0]["TKH_CUSNM"] != null && DTTemptbl.Rows[0]["TKH_CUSNM"] != "")
                {
                    dr["param03"] = DTTemptbl.Rows[0]["TKH_CUSNM"].ToString(); // Cust No
                }
                else
                {
                    dr["param03"] = "";
                }
                if (DTTemptbl.Rows[0]["TKH_CUSVT"] != null && DTTemptbl.Rows[0]["TKH_CUSVT"] != "")
                {
                    dr["param04"] = DTTemptbl.Rows[0]["TKH_CUSVT"].ToString(); // CUS VAT #
                }
                else
                {
                    dr["param04"] = "";
                }
                if (DTTemptbl.Rows[0]["TKH_SREP"] != null && DTTemptbl.Rows[0]["TKH_SREP"] != "")
                {
                    dr["param05"] = DTTemptbl.Rows[0]["TKH_SREP"].ToString().ToUpper(); // Sales Rep
                }
                else
                {
                    dr["param05"] = "";
                }
                if (DTTemptbl.Rows[0]["TKH_DATE"] != null && DTTemptbl.Rows[0]["TKH_DATE"] != "")
                {
                    DateTime DTtmp = Convert.ToDateTime(DTTemptbl.Rows[0]["TKH_DATE"].ToString());
                    dr["param06"] = DTtmp.ToString("dd-MMM-yyyy, HH:mm");
                }
                else
                {
                    dr["param06"] = "";
                }
                if (DTTemptbl.Rows[0]["TKH_TIME"] != null && DTTemptbl.Rows[0]["TKH_TIME"] != "")
                {
                    dr["param07"] = DTTemptbl.Rows[0]["TKH_TIME"].ToString(); // Time
                }
                else
                {
                    dr["param07"] = "";
                }
                if (DTTemptbl.Rows[0]["TKH_SHPTO"] != null && DTTemptbl.Rows[0]["TKH_SHPTO"] != "")
                {
                    string shiptoname = ShipToDetails(DTTemptbl.Rows[0]["TKH_CUSNO"].ToString(), DTTemptbl.Rows[0]["TKH_SHPTO"].ToString(), "SHP_NAME");
                    string shiptoaddress = ShipToDetails(DTTemptbl.Rows[0]["TKH_CUSNO"].ToString(), DTTemptbl.Rows[0]["TKH_SHPTO"].ToString(), "SHP_ADDRS");

                    dr["param30"] = shiptoname + " " + shiptoaddress;
                }
                else
                {
                    dr["param30"] = "";
                }

                //changed
                if (DTTemptbl.Rows[0]["TKH_REGNO"] != null && DTTemptbl.Rows[0]["TKH_REGNO"].ToString() != "")
                {
                    dr["param17"] = DTTemptbl.Rows[0]["TKH_REGNO"];  // lblRegisterNo.Text; // Time
                }
                else
                {
                    dr["param17"] = "";
                }
                if (Session["UserId"] != null && Session["UserId"].ToString() != "")
                {
                    dr["param18"] = Session["UserId"].ToString().ToUpper(); // lblDrawerNo.Text; // Time
                }
                else
                {
                    dr["param18"] = "";
                }
                //changed end

                if (DTTemptbl.Rows[0]["TKH_SLAMT"] != null && DTTemptbl.Rows[0]["TKH_SLAMT"] != "")
                {
                    dr["param31"] = Convert.ToDouble(DTTemptbl.Rows[0]["TKH_SLAMT"].ToString()).ToString("n2"); // sales amount
                }
                else
                {
                    dr["param31"] = "";
                }

                if (DTTemptbl.Rows[0]["TKH_DSCNT"] != null && DTTemptbl.Rows[0]["TKH_DSCNT"] != "")
                {
                    dr["param32"] = Convert.ToDouble(DTTemptbl.Rows[0]["TKH_DSCNT"].ToString()).ToString("n2"); //Discount
                }
                else
                {
                    dr["param32"] = "";
                }

                if (DTTemptbl.Rows[0]["TKH_NTAMT"] != null && DTTemptbl.Rows[0]["TKH_NTAMT"] != "")
                {
                    dr["param33"] = Convert.ToDouble(DTTemptbl.Rows[0]["TKH_NTAMT"].ToString()).ToString("n2");// net amount
                }
                else
                {
                    dr["param33"] = "0.00";
                }


                if (DTTemptbl.Rows[0]["TKH_VTAMT"] != null && DTTemptbl.Rows[0]["TKH_VTAMT"] != "")
                {
                    dr["param34"] = Convert.ToDouble(DTTemptbl.Rows[0]["TKH_VTAMT"].ToString()).ToString("n2"); // VAT Amount
                }
                else
                {
                    dr["param34"] = "";
                }

                if (DTTemptbl.Rows[0]["TKH_AMT"] != null && DTTemptbl.Rows[0]["TKH_AMT"] != "")
                {
                    dr["param35"] = Convert.ToDouble(DTTemptbl.Rows[0]["TKH_AMT"].ToString()).ToString("n2");// Total Amount
                }
                else
                {
                    dr["param35"] = "0.00";
                }
                if (DTTemptbl.Rows[0]["TKH_AMTPD"] != null && DTTemptbl.Rows[0]["TKH_AMTPD"] != "")
                {
                    dr["param36"] = Convert.ToDouble(DTTemptbl.Rows[0]["TKH_AMTPD"].ToString()).ToString("n2");// Amount Paid
                }
                else
                {
                    dr["param36"] = "0.00";
                }
                if (DTTemptbl.Rows[0]["TKH_CHNGE"] != null && DTTemptbl.Rows[0]["TKH_CHNGE"] != "")
                {
                    dr["param37"] = Convert.ToDouble(DTTemptbl.Rows[0]["TKH_CHNGE"].ToString()).ToString("n2");// Change Due
                }
                else
                {
                    dr["param37"] = "0.00";
                }//TKH_BLANC
                if (DTTemptbl.Rows[0]["TKH_BLANC"] != null && DTTemptbl.Rows[0]["TKH_BLANC"] != "")
                {
                    dr["param39"] = Convert.ToDouble(DTTemptbl.Rows[0]["TKH_BLANC"].ToString()).ToString("n2");// Balance Due
                }
                else
                {
                    dr["param39"] = "0.00";
                }

                if (DTTemptbl.Rows[0]["TKH_VATPR"] != null && DTTemptbl.Rows[0]["TKH_VATPR"] != "")
                {
                    dr["param28"] = DTTemptbl.Rows[0]["TKH_VATPR"].ToString();// Balance Due
                }
                else
                {
                    dr["param28"] = "-";
                }

                if (DTTemptbl.Rows[0]["TKH_TTYPE"] != null && DTTemptbl.Rows[0]["TKH_TTYPE"] != "")
                {
                    dr["param08"] = DTTemptbl.Rows[0]["TKH_TTYPE"].ToString();// TYPE
                }
                else
                {
                    dr["param08"] = "0.00";
                }

                if (DTTemptbl.Rows[0]["TKH_PAYCD"] != null && DTTemptbl.Rows[0]["TKH_PAYCD"] != "")
                {
                    if (DTTemptbl.Rows[0]["TKH_PAYCD"] == "A/R")
                    {
                        dr["param10"] = "Credit";
                    }
                    else
                    {
                        dr["param10"] = DTTemptbl.Rows[0]["TKH_PAYCD"].ToString();
                    }
                }
                else
                {
                    dr["param10"] = "-";
                }

                if (DTTemptbl.Rows[0]["TKH_PAYCD"] != null && !DTTemptbl.Rows[0]["TKH_PAYCD"].ToString().Equals("A/R"))
                {
                    dr["param21"] = "Amount Paid";
                }
                else
                {
                    dr["param21"] = "";
                }

                if (DTTemptbl.Rows[0]["TKH_PAYCD"] != null && !DTTemptbl.Rows[0]["TKH_PAYCD"].ToString().Equals("A/R"))
                {
                    dr["param22"] = "Change Due";
                }
                else
                {
                    dr["param22"] = "";
                    dr["param36"] = DBNull.Value;
                    dr["param37"] = DBNull.Value;
                }

                ReportsDataSet.PARAM_TABLE.Rows.Add(dr);
            }
        }
    }

    public void ReceiptReportGen()
    {
        try
        {
            ReportDocument RptDoc = new ReportDocument();

            if (this.SalesType == "Sales")
            {
                RptDoc.Load(Server.MapPath("~/RptFiles/Ticket/CR_SLS_RECPT_RPT.rpt"));
            }
            else if (this.SalesType == "Return")
            {
                RptDoc.Load(Server.MapPath("~/RptFiles/Ticket/CR_RTN_RECPT_RPT.rpt"));
            }
            else
            {
                throw new Exception("Forsan Vansales Exception: Ticket Type is void.");
            }

            RptDoc.SetDataSource(ReportsDataSet);
            RptVwr.ReportSource = RptDoc;
            Session["Rpt"] = RptDoc;
            //CrystalDecisions.CrystalReports.Engine.TextObject TXTCOMNTS_BOX = RptDoc.ReportDefinition.ReportObjects["TXT_CMMNT"] as CrystalDecisions.CrystalReports.Engine.TextObject;

            //TXTCOMNTS_BOX.ObjectFormat.EnableSuppress = txtComments.Text.Trim().Length <= 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void A4ReportGen()
    {
        try
        {
            ReportDocument RptDoc = new ReportDocument();

            RptDoc.Load(Server.MapPath("~/RptFiles/Ticket/CR_INV_BILAN1.rpt"));

            CrystalDecisions.CrystalReports.Engine.Section SEC_RTN = RptDoc.ReportDefinition.Sections["SEC_RTN"] as CrystalDecisions.CrystalReports.Engine.Section;
            SEC_RTN.SectionFormat.EnableSuppress = SalesType == "Sales";

            RptDoc.SetDataSource(ReportsDataSet);
            RptVwr.ReportSource = RptDoc;
            Session["Rpt"] = RptDoc;
            //CrystalDecisions.CrystalReports.Engine.TextObject TXTCOMNTS_BOX = RptDoc.ReportDefinition.ReportObjects["TXT_CMMNT"] as CrystalDecisions.CrystalReports.Engine.TextObject;

            //TXTCOMNTS_BOX.ObjectFormat.EnableSuppress = txtComments.Text.Trim().Length <= 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void TktRprntRptStrNo_Init(object sender, EventArgs e)
    {
        DataTable dtStores = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE WHERE STM_CODE IN (" + Session["userBranchCode"].ToString() + ") ");

        TktRprntRptStrNo.DataSource = dtStores;
        TktRprntRptStrNo.ValueField = "STM_CODE";
        TktRprntRptStrNo.TextField = "STM_NAME";
        TktRprntRptStrNo.DataBind();
    }
    protected void TktRprntRptRegNo_Init(object sender, EventArgs e)
    {
        string strNo = TktRprntRptStrNo.Value == null ? "''" : TktRprntRptStrNo.Value.ToString();
        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + strNo + ") ");

        TktRprntRptRegNo.DataSource = dtRegisters;
        TktRprntRptRegNo.ValueField = "REG_CODE";
        TktRprntRptRegNo.TextField = "REG_NAME";
        TktRprntRptRegNo.DataBind();
    }

    protected void TktRprntRptTicketNo_Init(object sender, EventArgs e)
    {
        string regNo = TktRprntRptRegNo.Value == null ? "''" : TktRprntRptRegNo.Value.ToString();
        DataTable dtRegisters = dbHlpr.FetchData("SELECT DISTINCT TKH_HKEY, TKH_TKTNO FROM SLS_TKH_HIST WHERE TKH_REGNO IN (" + regNo + ") ");

        TktRprntRptTicketNo.DataSource = dtRegisters;
        TktRprntRptTicketNo.ValueField = "TKH_HKEY";
        TktRprntRptTicketNo.TextField = "TKH_TKTNO";
        TktRprntRptTicketNo.DataBind();
    }

    protected void TktRprntRptStrNo_ValueChanged(object sender, EventArgs e)
    {
        ASPxComboBox value = (ASPxComboBox)sender;
        string strNo = value.Value == null ? "''" : value.Value.ToString();
        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + strNo + ") ");

        TktRprntRptRegNo.DataSource = dtRegisters;
        TktRprntRptRegNo.ValueField = "REG_CODE";
        TktRprntRptRegNo.TextField = "REG_NAME";
        TktRprntRptRegNo.DataBind();
    }

    protected void TktRprntRptRegNo_ValueChanged(object sender, EventArgs e)
    {
        string strNo = TktRprntRptStrNo.Value == null ? "''" : TktRprntRptStrNo.Value.ToString();
        string regNo = TktRprntRptRegNo.Value == null ? "''" : TktRprntRptRegNo.Value.ToString();
        DataTable dtRegisters = dbHlpr.FetchData("SELECT DISTINCT TKH_HKEY, TKH_TKTNO "
            + " FROM SLS_TKH_HIST "
            + " WHERE TKH_STRNO IN (" + strNo + ") "
            + " AND TKH_REGNO IN (" + regNo + ") "
            + " ORDER BY TKH_HKEY ");

        TktRprntRptTicketNo.DataSource = dtRegisters;
        TktRprntRptTicketNo.ValueField = "TKH_HKEY";
        TktRprntRptTicketNo.TextField = "TKH_TKTNO";
        TktRprntRptTicketNo.DataBind();
    }
}