﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Tickets_TicketsHistory : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
    
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (IsPostBack && Session["HdrQry"] != null && Session["LinQry"] != null)
        {
            DxGridSqlDataSource1.SelectCommand = Session["HdrQry"].ToString();
            DxGridSqlDataSource2.SelectCommand = Session["LinQry"].ToString();
        }
        /*DxGridSqlDataSource1.SelectCommand = "SELECT "
            + " TKH_HKEY, TKH_TKTNO, TKH_STRNO, TKH_REGNO, TKH_DRWNO, TKH_CUSNO, TKH_CUSNM, "
            + " TKH_CUSVT, TKH_SREP, TKH_DATE, TKH_TIME, TKH_CRTDT, TKH_CRTIM, "
            + " TKH_CRTBY, TKH_SLAMT, TKH_DSCNT, TKH_NTAMT, TKH_VTAMT, TKH_AMT, "
            + " TKH_AMTPD, TKH_PAYCD, TKH_TTYPE, TKH_CHNGE, TKH_BLANC, TKH_GEOLA, TKH_GEOLN, "
            + " TKH_PSTBY, TKH_PSTDT, TKH_PSTIM "
            + " FROM SLS_TKH_HIST "
            //+ " WHERE TKH_TTYPE = 'Sales' "
            + " ORDER BY TKH_TKTNO DESC ";

        DxGridSqlDataSource2.SelectCommand = "SELECT "
            + " TKL_HKEY, TKL_LKEY, TKL_SEQNO, TKL_TKTNO, TKL_STRNO, TKL_REGNO, TKL_DRWNO, TKL_ITMNO, TKL_DESC, "
            + " TKL_UOM, TKL_QTY, TKL_ITMPR, TKL_PRICE, TKL_EXPRC, TKL_TAXCD, "
            + " TKL_DSCNT, TKL_TXFAC, TKL_VATPRC, TKL_CUSNO, TKL_CUSNM, TKL_SREP, "
            + " TKL_DATE, TKL_TIME, TKL_CRTDT, TKL_CRTIM, TKL_CRTBY, TKL_TTYPE, "
            + " TKL_TEXMT, TKL_EXPDT, TKL_RSNCD, TKL_PAYCD, TKL_GEOLA, TKL_GEOLN, "
            + " TKL_PSTBY, TKL_PSTDT, TKL_PSTIM "
            + " FROM SLS_TKL_HIST ";*/
    }

    protected void btnViewTicket_Click(object sender, EventArgs e)
    {
        string STR_NO = TktInqryStrNo.Value != null ? TktInqryStrNo.Value.ToString().Trim() : "";
        string REG_NO = TktInqryRegNo.Value != null ? TktInqryRegNo.Value.ToString().Trim() : "";
        string CUS_NO = TktInqryCusNo.Value != null ? TktInqryCusNo.Value.ToString().Trim() : "";
        string TKT_NO = TktInqryTicketNo.Text.ToString().Trim();

        string START_DATE = TktInqryDateFrom.Value != null ? ((DateTime)TktInqryDateFrom.Value).ToString("yyyy-MM-dd") : new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");
        string END_DATE = TktInqryDateTo.Value != null ? ((DateTime)TktInqryDateTo.Value).ToString("yyyy-MM-dd") : new DateTime(9999, 12, 31).ToString("yyyy-MM-dd");
        END_DATE = END_DATE + " 23:59:59.998";

        string HdrQry = "SELECT "
            + " TKH_HKEY, TKH_TKTNO, TKH_STRNO, TKH_REGNO, TKH_DRWNO, TKH_CUSNO, TKH_CUSNM, "
            + " TKH_CUSVT, TKH_SREP, TKH_DATE, TKH_TIME, TKH_CRTDT, TKH_CRTIM, "
            + " TKH_CRTBY, TKH_SLAMT, TKH_DSCNT, TKH_NTAMT, TKH_VTAMT, TKH_AMT, "
            + " TKH_AMTPD, TKH_PAYCD, TKH_TTYPE, TKH_CHNGE, TKH_BLANC, TKH_GEOLA, TKH_GEOLN, "
            + " TKH_PSTBY, TKH_PSTDT, TKH_PSTIM "
            + " FROM SLS_TKH_HIST "
            + " WHERE 1 = 1 ";

        if (TKT_NO.Length > 0)
            HdrQry += " AND TKH_TKTNO = '" + TKT_NO + "' ";
        if (STR_NO.Length > 0)
            HdrQry += " AND TKH_STRNO = '" + STR_NO + "' ";
        if (REG_NO.Length > 0)
            HdrQry += " AND TKH_REGNO = '" + REG_NO + "' ";
        if (CUS_NO.Length > 0)
            HdrQry += " AND TKH_CUSNO = '" + CUS_NO + "' ";
        if (START_DATE.Length > 0 || END_DATE.Length > 0)
            HdrQry += " AND TKH_DATE BETWEEN '" + START_DATE + "' AND '" + END_DATE + "' ";

        HdrQry += " ORDER BY TKH_TKTNO DESC ";

        DxGridSqlDataSource1.SelectCommand = HdrQry;

        string LinQry = "SELECT "
            + " TKL_HKEY, TKL_LKEY, TKL_SEQNO, TKL_TKTNO, TKL_STRNO, TKL_REGNO, TKL_DRWNO, TKL_ITMNO, TKL_DESC, "
            + " TKL_UOM, TKL_QTY, TKL_ITMPR, TKL_PRICE, TKL_EXPRC, TKL_TAXCD, "
            + " TKL_DSCNT, TKL_TXFAC, TKL_VATPRC, TKL_CUSNO, TKL_CUSNM, TKL_SREP, "
            + " TKL_DATE, TKL_TIME, TKL_CRTDT, TKL_CRTIM, TKL_CRTBY, TKL_TTYPE, "
            + " TKL_TEXMT, TKL_EXPDT, TKL_RSNCD, TKL_PAYCD, TKL_GEOLA, TKL_GEOLN, "
            + " TKL_PSTBY, TKL_PSTDT, TKL_PSTIM, "
            + " TKL_ISFREE, TKL_PRCOF, TKL_PCRSN "
            + " FROM SLS_TKL_HIST "
            + " WHERE 1 = 1 ";

        if (TKT_NO.Length > 0)
            LinQry += " AND TKL_TKTNO = '" + TKT_NO + "' ";
        if (STR_NO.Length > 0)
            LinQry += " AND TKL_STRNO = '" + STR_NO + "' ";
        if (REG_NO.Length > 0)
            LinQry += " AND TKL_REGNO = '" + REG_NO + "' ";
        if (CUS_NO.Length > 0)
            LinQry += " AND TKL_CUSNO = '" + CUS_NO + "' ";
        if (START_DATE.Length > 0 || END_DATE.Length > 0)
            LinQry += " AND TKL_DATE BETWEEN '" + START_DATE + "' AND '" + END_DATE + "' ";

        LinQry += " ORDER BY TKL_SEQNO ";

        DxGridSqlDataSource2.SelectCommand = LinQry;

        Session["HdrQry"] = HdrQry;
        Session["LinQry"] = LinQry;
    }

    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        //gvInquery.SettingsPager.PageSize = Convert.ToInt32(ddlNoOfRecords.SelectedItem.Value);
        //gvInquery.DataBind();
    }

    protected void ACB_CodeCheck_Callback(object source, CallbackEventArgs e)
    {

    }

    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {
    }

    // Common Methods
    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }

    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void gvInquery_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["gvSalesLines"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    protected void gvInquery_CustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        //  if(e.Column.FieldName == "")
    }

    protected void gvInquery_DetailRowExpandedChanged(object sender, ASPxGridViewDetailRowEventArgs e)
    {
        int vIndex = e.VisibleIndex;
        string ABC = gvInquery.GetRowValues(vIndex, "TKH_HKEY").ToString();

        DxGridSqlDataSource2.SelectCommand = "SELECT "
            + " TKL_HKEY, TKL_LKEY, TKL_SEQNO, TKL_TKTNO, TKL_STRNO, TKL_REGNO, TKL_DRWNO, TKL_ITMNO, TKL_DESC, "
            + " TKL_UOM, TKL_QTY, TKL_ITMPR, TKL_PRICE, TKL_EXPRC, TKL_TAXCD, "
            + " TKL_DSCNT, TKL_TXFAC, TKL_VATPRC, TKL_CUSNO, TKL_CUSNM, TKL_SREP, "
            + " TKL_DATE, TKL_TIME, TKL_CRTDT, TKL_CRTIM, TKL_CRTBY, TKL_TTYPE, "
            + " TKL_TEXMT, TKL_EXPDT, TKL_RSNCD, TKL_PAYCD, TKL_GEOLA, TKL_GEOLN "
            + " FROM SLS_TKL_HIST "
            + " WHERE TKL_HKEY = '" + ABC + "' ";
    }

    protected void TktInqryStrNo_Init(object sender, EventArgs e)
    {
        DataTable dtStores = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE WHERE STM_CODE IN (" + Session["userBranchCode"].ToString() + ") ");

        TktInqryStrNo.DataSource = dtStores;
        TktInqryStrNo.ValueField = "STM_CODE";
        TktInqryStrNo.TextField = "STM_NAME";
        TktInqryStrNo.DataBind();
    }

    protected void TktInqryRegNo_Init(object sender, EventArgs e)
    {
        string strNo = TktInqryStrNo.Value == null ? "''" : TktInqryStrNo.Value.ToString();
        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + strNo + ") ");

        TktInqryRegNo.DataSource = dtRegisters;
        TktInqryRegNo.ValueField = "REG_CODE";
        TktInqryRegNo.TextField = "REG_NAME";
        TktInqryRegNo.DataBind();
    }

    protected void TktInqryCusNo_Init(object sender, EventArgs e)
    {
        DataTable dtCustomers = dbHlpr.FetchData("SELECT CUS_NO, CUS_NO + ' ' + CUS_NAME AS DSPLY_FLD FROM STP_MST_CSTMR ");

        TktInqryCusNo.DataSource = dtCustomers;
        TktInqryCusNo.ValueField = "CUS_NO";
        TktInqryCusNo.TextField = "DSPLY_FLD";
        TktInqryCusNo.DataBind();
    }

    protected void TktInqryStrNo_ValueChanged(object sender, EventArgs e)
    {
        ASPxComboBox value = (ASPxComboBox)sender;
        string strNo = value.Value == null ? "''" : value.Value.ToString();
        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + strNo + ") ");

        TktInqryRegNo.DataSource = dtRegisters;
        TktInqryRegNo.ValueField = "REG_CODE";
        TktInqryRegNo.TextField = "REG_NAME";
        TktInqryRegNo.DataBind();
    }

}//END CLASS