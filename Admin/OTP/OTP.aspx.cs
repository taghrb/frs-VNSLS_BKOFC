﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;

public partial class Admin_OTP_OTP : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["userMenuCode"] == "PIC")
        {
            DataTable dtUsr = (DataTable)Session["userObject"];
            if (dtUsr.Rows[0]["USR_MGACS"] == DBNull.Value || dtUsr.Rows[0]["USR_MGACS"].ToString() == string.Empty || dtUsr.Rows[0]["USR_MGACS"].ToString() != "1")
            {
                if (Page.IsCallback)
                {
                    ASPxWebControl.RedirectOnCallback("~/");
                }
                else
                {
                    Response.Redirect("~/");
                }
            }
        }

        dtTmTicketDate.Date = DateTime.Now;
    }

    protected void btnGenerateOtp_Click(object sender, EventArgs e)
    {
        DateTime tktDate = dtTmTicketDate.Date;
        DateTime expiryDate = dtTmExpiryDate.Date;
        string dateSelected = tktDate.ToString("yyyyMMdd");
        string expiryDateSelected = expiryDate.ToString("yyyyMMdd");
        string StringToHash = 
                      cmbxRouteID.Value.ToString().Trim() + "|"
                    + txtTicketNo.Text.Trim() + "|"
                    + dateSelected.Trim() + "|"
                    + lkUpCustNo.Value.ToString().Trim() + "|"
                    + lkUpItemNo.Value.ToString().Trim() + "|"
                    + cmbxUom.Value.ToString().Trim() + "|"
                    + txtQty.Text.Trim().Replace(",", "") + "|"
                    + txtOldPrice.Text.Trim().Replace(",", "") + "|"
                    + txtNewPrice.Text.Trim().Replace(",", "") + "|"
                    + lkUpPrChngRsnCd.Value.ToString().Trim() + "|"
                    + expiryDateSelected.Trim();
        lblOtp.Text = dbHlpr.GetStableHash(StringToHash).ToString();
    }
}