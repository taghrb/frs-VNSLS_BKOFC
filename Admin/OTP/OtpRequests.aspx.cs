﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;

public partial class Admin_OtpRequests_OtpRequests : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["userMenuCode"] != null && Session["userMenuCode"].ToString() == "PIC")
        {
            DataTable dtUsr = (DataTable)Session["userObject"];
            if (dtUsr.Rows[0]["USR_MGACS"] == DBNull.Value || dtUsr.Rows[0]["USR_MGACS"].ToString() == string.Empty || dtUsr.Rows[0]["USR_MGACS"].ToString() != "1")
            {
                if (Page.IsCallback)
                {
                    ASPxWebControl.RedirectOnCallback("~/");
                }
                else
                {
                    Response.Redirect("~/");
                }
            }
        }

        string StrNos = Session["userBranchCode"].ToString();

        DateTime dtNow = DateTime.Now;
        string dateToday = dtNow.ToString("yyyy-MM-dd");
        string timeOneHourAgo = dtNow.AddHours(-1).ToString("HH:mm");

        string qry = "SELECT ";
        qry += " OTR_REQID, OTR_USRID, OTR_STRNG, OTR_CODE, OTR_RCVDT, ";
        qry += " OTR_RCVTM, OTR_ROUTE, OTR_TKTNO, OTR_TKTDT, ";
        qry += " OTR_CUSNO, OTR_ITMNO, OTR_UOM, OTR_QTY, ";
        qry += " OTR_OLDPR, OTR_NEWPR, ";
        qry += " CUS_NAME, PRO_DESC1 ";
        qry += " FROM SLS_OTP_RQUST ";
        qry += " LEFT JOIN STP_MSTR_SLREP ON OTR_ROUTE = SRP_CODE ";
        qry += " LEFT JOIN STP_MST_CSTMR ON OTR_CUSNO = CUS_NO ";
        qry += " LEFT JOIN INV_MSTR_PRODT ON OTR_ITMNO = PRO_CODE ";
        qry += " WHERE ";
        qry += " OTR_RCVDT = '" + dateToday + "' ";
        qry += " AND OTR_RCVTM > '" + timeOneHourAgo + "' ";
        qry += " AND SRP_STRNO IN (" + StrNos + ") ";
        qry += " ORDER BY OTR_REQID DESC ";

        DxGridSqlDataSource1.SelectCommand = qry;
    }

    protected void categoryListingGridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvOtpRqustInquery_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvOtpRqustInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string otpRqustId = e.Keys["OTR_REQID"].ToString().Trim();

        dbHlpr.ExecuteNonQuery("DELETE FROM SLS_OTP_RQUST WHERE OTR_REQID = '" + otpRqustId + "'");

        e.Cancel = true;
        gvOtpRqustInquery.CancelEdit();
    }
    protected void ACB_CodeCheck_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (e.Parameter.Length > 0)
        {
            DataTable myDT = new DataTable();
            myDT = dbHlpr.FetchData("SELECT * FROM SLS_OTP_RQUST WHERE OTR_REQID = '" + e.Parameter.Trim() + "'");
            JavaScriptSerializer myserializer = new JavaScriptSerializer();
            if (myDT.Rows.Count > 0)
            {
                e.Result = myserializer.Serialize(myDT.Rows[0].ItemArray);
            }
            else
            {
                e.Result = myserializer.Serialize("");
            }
        }
    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxTextBox txtBxCode = ((ASPxTextBox)gvOtpRqustInquery.FindEditFormTemplateControl("OtpRqustEditCode"));
        string otpRqustId = "0";

        if (txtBxCode.Text.Length > 0)
        {
            otpRqustId = txtBxCode.Text.Trim();
        }

        string OtpRqustName = ((ASPxTextBox)gvOtpRqustInquery.FindEditFormTemplateControl("OtpRqustEditName")).Text;

        DataTable myDT = new DataTable();

        myDT = myDT = dbHlpr.FetchData("SELECT * FROM SLS_OTP_RQUST WHERE OTR_REQID = '" + otpRqustId + "'");
        if (myDT.Rows.Count <= 0)
        {

        }
        else
        {

        }

        gvOtpRqustInquery.CancelEdit();
        gvOtpRqustInquery.DataBind();
    }
    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvOtpRqustInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvOtpRqustInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

    }
    protected void gvOtpRqustInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            string OtpRqust = e.CommandArgs.CommandArgument.ToString();

            if (OtpRqust != "") // Code is sent via request
            {
                dbHlpr.ExecuteNonQuery("DELETE FROM SLS_OTP_RQUST WHERE OTR_REQID = '" + OtpRqust + "'");

                gvOtpRqustInquery.CancelEdit();
                gvOtpRqustInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvOtpRqustInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid OTP Request. Unable to Delete OTP Request.";
            }
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvOtpRqustInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvOtpRqustInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvOtpRqustInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvOtpRqustInquery.SearchPanelFilter = txtFilter.Text;
        gvOtpRqustInquery.DataBind();
    }
    protected void gvOtpRqustInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvOtpRqustInquery, CBX_filter);
    }
    protected void gvOtpRqustInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvOtpRqustInquery, CBX_filter);
    }


    protected void OtpRqustEditCode_Init(object sender, EventArgs e)
    {
        if (!gvOtpRqustInquery.IsNewRowEditing)
        {
            ASPxTextBox txtCode = (ASPxTextBox)sender;
            txtCode.ReadOnly = true;
        }
        else
        {
            ASPxTextBox txtCode1 = (ASPxTextBox)sender;
            txtCode1.ReadOnly = false;
        }
    }
}