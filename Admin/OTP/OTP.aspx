﻿<%@ Page Title="Generate OTP" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="OTP.aspx.cs" Inherits="Admin_OTP_OTP" %>


<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <script type="text/javascript">

        function grid_customizationWindowCloseUp(s, e) {
            gvUOMClientInquery.ShowCustomizationWindow();
        }
    </script>
    <style type="text/css">
        .fieldsetWrapper {
            padding: 10px;
            width: 50%;
            margin: 0 auto;
            background-color: #F7F5F1;
            margin-top: 5px;
            border-radius: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="contentTopHere1" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
            <li>&nbsp;
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">

    <div class="formDiv" style="background-color: transparent;">
        <div class="formHeaderDiv">
            <h3>OTP Generator</h3>
        </div>
        <div class="fieldsetWrapper">
            <fieldset>
                <legend></legend>
                <table>
                    <tr>
                        <td>Van #:&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxComboBox ID="cmbxRouteID" ClientInstanceName="cmbxClientRouteID" DataSourceID="cmbxRouteSqlDataSource1" ValueField="SRP_CODE" TextField="SRP_CODE" runat="server" Width="170px" ValueType="System.String" TabIndex="1">
                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="true" ErrorText="Route is Required." />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                                <asp:SqlDataSource ID="cmbxRouteSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT SRP_CODE FROM STP_MSTR_SLREP ORDER BY SRP_CODE"></asp:SqlDataSource>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Ticket #:&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxTextBox ID="txtTicketNo" runat="server" Width="170px" TabIndex="2">
                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RegularExpression ValidationExpression="\d+" ErrorText="Invalid Ticket Number" />
                                        <RequiredField IsRequired="true" ErrorText="Ticket Number is Required" />
                                    </ValidationSettings>
                                    <MaskSettings Mask="<999999999>" />
                                </dx:ASPxTextBox>
                            </div>
                        </td>

                        <td>Ticket Date:&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxDateEdit ID="dtTmTicketDate" runat="server" Width="170px" TabIndex="3" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy">
                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="true" ErrorText="Ticket Number is Required" />
                                    </ValidationSettings>
                                </dx:ASPxDateEdit>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Cust No.:&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxGridLookup ID="lkUpCustNo" runat="server" Width="170px" TabIndex="4" TextFormatString="{0}" SelectionMode="Single" DataSourceID="CustNoSqlDataSource1" KeyFieldName="CUS_NO">
                                    <GridViewProperties>
                                        <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                                        <SettingsPager PageSize="10" />
                                    </GridViewProperties>
                                    <Columns>
                                        <dx:GridViewDataColumn Caption="Cust #" FieldName="CUS_NO" Width="20%" />
                                        <dx:GridViewDataColumn Caption="Name" FieldName="CUS_NAME" Width="30%" />
                                        <dx:GridViewDataColumn Caption="Address" FieldName="CUS_ADDR1" Visible="false" Width="30%" />
                                        <dx:GridViewDataColumn Caption="Catg" FieldName="CUS_CATG" Width="10%" />
                                        <dx:GridViewDataColumn Caption="S. Rep" FieldName="CUS_SLSRP" Width="10%" />
                                    </Columns>
                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="true" ErrorText="Cust number is Required" />
                                    </ValidationSettings>
                                </dx:ASPxGridLookup>
                                <asp:SqlDataSource ID="CustNoSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT CUS_NO, CUS_NAME, CUS_ADDR1, CUS_CATG, CUS_SLSRP FROM STP_MST_CSTMR ORDER BY CUS_NO"></asp:SqlDataSource>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Item No.:&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxGridLookup ID="lkUpItemNo" runat="server" Width="170px" TabIndex="5" TextFormatString="{0}" SelectionMode="Single" DataSourceID="ItemNoSqlDataSource1" KeyFieldName="PRO_CODE">
                                    <GridViewProperties>
                                        <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                                        <SettingsPager PageSize="10" />
                                    </GridViewProperties>
                                    <Columns>
                                        <dx:GridViewDataColumn Caption="Item #" FieldName="PRO_CODE" Width="20%" />
                                        <dx:GridViewDataColumn Caption="Desc" FieldName="PRO_DESC1" Width="30%" />
                                        <dx:GridViewDataColumn Caption="Catg" FieldName="PRO_CATG" Visible="false" Width="30%" />
                                        <dx:GridViewDataColumn Caption="Price" FieldName="PRO_PRICE" Width="10%" />
                                        <dx:GridViewDataColumn Caption="Weight" FieldName="PRO_WT" Width="10%" />
                                    </Columns>
                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="true" ErrorText="Item number is Required" />
                                    </ValidationSettings>
                                </dx:ASPxGridLookup>
                                <asp:SqlDataSource ID="ItemNoSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT PRO_CODE, PRO_DESC1, PRO_CATG, PRO_PRICE, PRO_WT FROM INV_MSTR_PRODT ORDER BY PRO_CODE"></asp:SqlDataSource>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>UoM : &nbsp;&nbsp;</td>
                        <td>
                            <div class="=DivReq">
                                <asp:SqlDataSource ID="CmBxUomSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT DISTINCT UOM_CODE FROM STP_MSTR_UOM"></asp:SqlDataSource>
                                <dx:ASPxComboBox ID="cmbxUom" runat="server" DataSourceID="CmBxUomSqlDataSource1" TextField="UOM_CODE" ValueField="UOM_CODE" Width="170px">
                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="Text" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <RequiredField IsRequired="true" />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                            </div>
                        </td>
                        <td>Quantity :&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxTextBox ID="txtQty" runat="server" Width="170px" TabIndex="8">
                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="true" ErrorText="Quantity is Required" />
                                    </ValidationSettings>
                                    <MaskSettings Mask="<0..99999>.<00..999>" />
                                </dx:ASPxTextBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Price Change Reason:&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxGridLookup ID="lkUpPrChngRsnCd" runat="server" Width="170px" TabIndex="5" TextFormatString="{0}" SelectionMode="Single" DataSourceID="PrChngRsnCdSqlDataSource1" KeyFieldName="PRS_CODE">
                                    <GridViewProperties>
                                        <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                                        <SettingsPager PageSize="10" />
                                    </GridViewProperties>
                                    <Columns>
                                        <dx:GridViewDataColumn Caption="Code" FieldName="PRS_CODE" Width="20%" />
                                        <dx:GridViewDataColumn Caption="Desc" FieldName="PRS_TITLE" Width="80%" />
                                    </Columns>
                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="true" ErrorText="Price Change Reason is Required" />
                                    </ValidationSettings>
                                </dx:ASPxGridLookup>
                                <asp:SqlDataSource ID="PrChngRsnCdSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT PRS_CODE, PRS_TITLE FROM STP_MST_PRSNS ORDER BY PRS_CODE"></asp:SqlDataSource>
                            </div>
                        </td>

                        <td>Expiry Date:&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxDateEdit ID="dtTmExpiryDate" runat="server" Width="170px" TabIndex="3" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy">
                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="true" ErrorText="Expiry date is Required" />
                                    </ValidationSettings>
                                </dx:ASPxDateEdit>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Old Price:&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxTextBox ID="txtOldPrice" runat="server" Width="170px" TabIndex="8">
                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="true" ErrorText="Old Price is Required" />
                                    </ValidationSettings>
                                    <MaskSettings Mask="<0..99999>.<00..99>" />
                                </dx:ASPxTextBox>
                            </div>
                        </td>

                        <td>New Price:&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxTextBox ID="txtNewPrice" runat="server" Width="170px" TabIndex="9">
                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="true" ErrorText="New Price is Required" />
                                    </ValidationSettings>
                                    <MaskSettings Mask="<0..99999>.<00..99>" />
                                </dx:ASPxTextBox>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Generated OTP:&nbsp;&nbsp;</td>
                        <td>
                            <dx:ASPxLabel ID="lblOtp" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
                        </td>
                        <td></td>
                    </tr>

                </table>
            </fieldset>
            <hr style="color: skyblue" />
            <dx:ASPxLabel ID="lblErrorMessage" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
            <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                <dx:ASPxButton ID="btnGenerateOtp" runat="server" Text="Generate OTP" CssClass="btnPopup btn btn-success" CausesValidation="true" OnClick="btnGenerateOtp_Click" />
                <dx:ASPxButton ID="btnPopUpClear" runat="server" Text="Clear" CssClass="btnPopup btn btn-warning" CausesValidation="false" AutoPostBack="false">
                    <ClientSideEvents Click="function(s, e) { CallBackCode.PerformCallback(CatClientEditCode.GetText()); }" />
                </dx:ASPxButton>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        window.onload = function () {
            $('form').on('click', '.btnPopupClose', function () {
                $('.overlay').hide();
            });
        };

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //if (prm != null) {
        prm.add_endRequest(function (sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.overlay').show();
            }
        });
        prm.add_endRequest(GridOnEndCallBack);
        //};

    </script>
</asp:Content>

