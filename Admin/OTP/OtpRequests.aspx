﻿<%@ Page Title="Manage OTP Requests" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="OtpRequests.aspx.cs" Inherits="Admin_OtpRequests_OtpRequests" %>


<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <script type="text/javascript">

        function grid_customizationWindowCloseUp(s, e) {
            gvOtpRqustClientInquery.ShowCustomizationWindow();
        }
    </script>

</asp:Content>
<asp:Content ID="contentTopHere1" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
            <li>
                <asp:LinkButton ID="LinkBtnPDF" OnClick="LinkBtnPDF_Click" runat="server">
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonXLX" OnClick="LinkButtonXLX_Click" runat="server">
                <img src="~/img/FilesIcons/excel.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonXxls" OnClick="LinkButtonXxls_Click" runat="server">
                <img src="~/img/FilesIcons/xlsx.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonRtf" OnClick="LinkButtonRtf_Click" runat="server">
                <img src="~/img/FilesIcons/rtf.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonCvs" OnClick="LinkButtonCvs_Click" runat="server">
                <img src="~/img/FilesIcons/cvs.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonWord" OnClick="LinkButtonRtf_Click" runat="server">
                <img src="~/img/FilesIcons/word.png" runat="server" />
                </asp:LinkButton>
            </li>

            <li>
                <asp:LinkButton ID="LinkButtonHtml" OnClick="LinkButtonHtml_Click" runat="server">
                <img src="~/img/icons/searchtop.jpg" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton6" OnClick="LinkButton3_Click" runat="server">               
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton7" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/word.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton8" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/excel.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton9" OnClick="LinkButton3_Click" OnClientClick="window.print();" runat="server">
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton10" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/torrent.png" runat="server" />
                </asp:LinkButton>
            </li>
        </ul>
        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvOtpRqustInquery" ExportedRowType="All"></dx:ASPxGridViewExporter>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">

    <div class="formDiv">
        <div class="formHeaderDiv">
            <h3>OTP Requests Master</h3>
        </div>
        <asp:SqlDataSource runat="server" ID="DxGridSqlDataSource1"
            ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
        <dx:ASPxGridView ID="gvOtpRqustInquery" CssClass="FromGridView" ClientInstanceName="gvOtpRqustClientInquery" Theme="Office2010Black" runat="server" AutoGenerateColumns="False" KeyFieldName="OTR_REQID"
            DataSourceID="DxGridSqlDataSource1" OnCustomButtonCallback="gvOtpRqustInquery_CustomButtonCallback"
            OnRowCommand="gvOtpRqustInquery_RowCommand"
            OnDataBound="gvOtpRqustInquery_DataBound"
            OnBeforeGetCallbackResult="gvOtpRqustInquery_BeforeGetCallbackResult"
            OnRowDeleting="gvOtpRqustInquery_RowDeleting">

            <SettingsContextMenu Enabled="True" EnableColumnMenu="True" EnableRowMenu="True"></SettingsContextMenu>
            <SettingsSearchPanel Visible="False" CustomEditorID="CustomSearchPanelTxtBx" />
            <Columns>
                
                <dx:GridViewDataTextColumn FieldName="OTR_REQID" Caption="Code" VisibleIndex="4">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                    <EditFormSettings VisibleIndex="0" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="OTR_USRID" Caption="User" VisibleIndex="5">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="OTR_STRNG" Caption="String" VisibleIndex="6" Visible="false">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="OTR_CODE" Caption="OTP Code" VisibleIndex="7">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataDateColumn FieldName="OTR_RCVDT" Caption="Received On" VisibleIndex="8" PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataDateColumn>
                <dx:GridViewDataTextColumn FieldName="OTR_RCVTM" Caption="Received At" VisibleIndex="9">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="OTR_ROUTE" Caption="Route" VisibleIndex="10">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="OTR_TKTNO" Caption="Ticket #" VisibleIndex="11">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="OTR_TKTDT" Caption="Ticket Date" VisibleIndex="12">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="OTR_CUSNO" Caption="Cust #" VisibleIndex="13">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="OTR_ITMNO" Caption="Item #" VisibleIndex="14">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="OTR_UOM" Caption="UoM" VisibleIndex="15">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="OTR_QTY" Caption="Qty" VisibleIndex="16">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="OTR_OLDPR" Caption="Old Price" VisibleIndex="17">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="OTR_NEWPR" Caption="New Price" VisibleIndex="18">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
            </Columns>
            <Settings HorizontalScrollBarMode="Auto" />

            <SettingsCommandButton>
                <NewButton ButtonType="Button" Text="New" Styles-Style-CssClass="btna btn btn-primary  btn-sm">
                    <Image runat="server" Url="~/img/icons/white-icn/Add.png" Width="15px"></Image>
                </NewButton>

                <SelectButton ButtonType="Button" Text="View" Styles-Style-CssClass="btna btn btn-success  btn-sm">
                    <Image ToolTip="View button here" Url="~/img/icons/white-icn/eyee1.png" Width="15px" />
                </SelectButton>

                <EditButton ButtonType="Button" Text="Edit" Styles-Style-CssClass="btna btn btn-info  btn-sm">
                    <Image runat="server" Url="~/img/icons/white-icn/Pen Tool.png" Width="15px"></Image>
                </EditButton>

                <DeleteButton ButtonType="Button" Text="Delete" Styles-Style-CssClass="btna btn btn-danger btn-sm btn-danger-variant">
                    <Image runat="server" Url="~/img/icons/white-icn/Trash.png" Width="15px"></Image>
                </DeleteButton>

                <CancelButton ButtonType="Button" Text="Close" Styles-Style-CssClass="btnPopup btn btn-primary">
                </CancelButton>
                <UpdateButton ButtonType="Button" Text="Update">
                </UpdateButton>
            </SettingsCommandButton>
            <SettingsEditing EditFormColumnCount="2" />
            <SettingsPopup>
                <EditForm VerticalAlign="WindowCenter" AllowResize="True" Modal="True" HorizontalAlign="WindowCenter"></EditForm>
            </SettingsPopup>

            <SettingsPager Position="Bottom">
                <PageSizeItemSettings Caption="Records per Page" Items="10, 20, 50, 100" ShowAllItem="true" Position="Right" Visible="true">
                </PageSizeItemSettings>
            </SettingsPager>
            <Styles>
                <CommandColumn Spacing="0px" Wrap="False" />
                <EditFormDisplayRow HorizontalAlign="Center" VerticalAlign="Middle"></EditFormDisplayRow>
                <AlternatingRow CssClass="alterRowGV" Enabled="True"></AlternatingRow>
                <Row CssClass="rowHeight"></Row>
            </Styles>
            <Settings ShowGroupPanel="true" VerticalScrollBarMode="Auto" />
            <SettingsLoadingPanel Mode="ShowOnStatusBar" />
            <SettingsBehavior EnableCustomizationWindow="true" AllowDragDrop="true" ConfirmDelete="True" ColumnResizeMode="Control" />
            <ClientSideEvents ColumnStartDragging="grid_customizationWindowCloseUp" EndCallback="GridOnEndCallBack" />

            <Templates>
            </Templates>

            <Templates>
                <StatusBar>
                    <table>
                        <tr>
                            <td>
                                <%--<dx:ASPxButton ID="btnNewRecord" runat="server" Text="Add New Item" AutoPostBack="false">
                                    <ClientSideEvents Click="function (s, e){ FromGridView.AddNewRow();  }" />
                                </dx:ASPxButton>--%>
                            </td>
                            <td>Search: </td>
                            <td>
                                <dx:ASPxButtonEdit runat="server" ID="CustomSearchPanelTxtBx" />
                            </td>
                            <td>&nbsp;</td>
                            <td>Search In:</td>
                            <td>
                                <dx:ASPxComboBox AutoPostBack="true" ID="DDLFilterByColumn" runat="server" OnSelectedIndexChanged="DDLFilterByColumn_SelectedIndexChanged">
                                    <Items>
                                        <dx:ListEditItem Text="All" Value="*" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>

                        </tr>
                    </table>
                </StatusBar>
            </Templates>

        </dx:ASPxGridView>
    </div>

    <script type="text/javascript">
        window.onload = function () {
            $('form').on('click', '.btnPopupClose', function () {
                $('.overlay').hide();
            });
        };

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //if (prm != null) {
        prm.add_endRequest(function (sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.overlay').show();
            }
        });
        prm.add_endRequest(GridOnEndCallBack);
        //};


        function OnTestValidation(s, e) {
            //var testData = e.value;
            //if (!testData) {
            lblError.SetVisible(!e.isValid);
            //  return;
        }

        function CB_categoryCodeLostFocus(s, e) {
            //var RowVals = JSON.parse(e.result);
            //if (RowVals.length > 0) {
            //    CatClientEditName.SetText(RowVals[1]);
            //    CatClientEditDivision.SetValue(RowVals[4]);
            //    CatClientEditSeqNo.SetText(RowVals[5]);
            //    $(CatClientEditImageIMAGE).attr('src', MyBaseUrl_JS + RowVals[3]);
            //    CatClientEditImageOldPath.SetText(RowVals[3]);
            //} else {
            //    CatClientEditName.SetText("");
            //    CatClientEditDivision.SetValue("");
            //    CatClientEditSeqNo.SetText("");
            //    $(CatClientEditImageIMAGE).removeAttr('src');
            //    CatClientEditImageOldPath.SetText("");
            //}
        }
    </script>
</asp:Content>

