﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;

public partial class Admin_CustomersCatg_Default : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT [CUS_CATCD]      ,[CUS_CATNM]       FROM [CUS_MST_CATG] ";
    }

    protected void categoryListingGridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvInquery_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string uomCode = e.Keys["CUS_CATCD"].ToString().Trim();

        dbHlpr.ExecuteNonQuery("DELETE FROM [CUS_MST_CATG] WHERE CUS_CATCD = '" + uomCode + "'");

        DataTable dt = dbHlpr.FetchData("SELECT '" + uomCode + "' AS TEXT01 ");
        dbHlpr.CreateDownloadRecord(dt, "ALL", "CUS_MST_CATG", "DLT");


        e.Cancel = true;
        gvInquery.CancelEdit();
    }
    protected void ACB_CodeCheck_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (e.Parameter.Length > 0)
        {
            DataTable myDT = new DataTable();
            myDT = dbHlpr.FetchData("SELECT * FROM [CUS_MST_CATG] WHERE CUS_CATCD = '" + e.Parameter.Trim() + "'");
            JavaScriptSerializer myserializer = new JavaScriptSerializer();
            if (myDT.Rows.Count > 0)
            {
                e.Result = myserializer.Serialize(myDT.Rows[0].ItemArray);
            }
            else
            {
                e.Result = myserializer.Serialize("");
            }
        }
    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxTextBox txtBxCode = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("CatEditCode"));
        string uomCode = "0";
        //  string CatTyp = "0";
        if (txtBxCode.Text.Length > 0)
        {
            uomCode = txtBxCode.Text.Trim();
        }

        string UomName = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("CatEditName")).Text;
        //       string CatType = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("CatEditType")).SelectedItem.Value.ToString();

        DataTable myDT = new DataTable();

        myDT = myDT = dbHlpr.FetchData("SELECT * FROM [CUS_MST_CATG] WHERE CUS_CATCD = '" + uomCode + "'");
        if (myDT.Rows.Count <= 0)
        {

            dbHlpr.ExecuteNonQuery("INSERT INTO [CUS_MST_CATG] (CUS_CATCD,  CUS_CATNM   ) "
            + " VALUES ('" + uomCode + "',  '" + UomName + "')");

            DataTable dt = dbHlpr.FetchData("SELECT "
                + " CUS_CATCD AS TEXT01, CUS_CATNM AS TEXT02 "
                + " FROM CUS_MST_CATG "
                + " WHERE CUS_CATCD = '" + uomCode + "' ");
            dbHlpr.CreateDownloadRecord(dt, "ALL", "CUS_MST_CATG", "CRT");
        }
        else
        {
            dbHlpr.ExecuteNonQuery("UPDATE [CUS_MST_CATG] SET "
            + " CUS_CATNM = '" + UomName + "' "
            + " WHERE CUS_CATCD = '" + uomCode + "'");

            DataTable dt = dbHlpr.FetchData("SELECT "
                + " CUS_CATCD AS TEXT01, CUS_CATNM AS TEXT02 "
                + " FROM CUS_MST_CATG "
                + " WHERE CUS_CATCD = '" + uomCode + "' ");
            dbHlpr.CreateDownloadRecord(dt, "ALL", "CUS_MST_CATG", "UPD");
        }

        gvInquery.CancelEdit();
        gvInquery.DataBind();
    }
    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

        ////html export here
        //gridExport.ExportHtmlToResponse();
        //string str;
        //MemoryStream ms = new MemoryStream();
        //try
        //{
        //    gridExport.ExportToHtml(ms);
        //    ms.Seek(0, SeekOrigin.Begin);
        //    StreamReader sr = new StreamReader(ms);
        //    str = sr.ReadToEnd();
        //}
        //finally
        //{
        //    ms.Close();
        //}

    }
    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            string uomId = e.CommandArgs.CommandArgument.ToString();

            if (uomId != "") // Code is sent via request
            {
                dbHlpr.ExecuteNonQuery("DELETE FROM [CUS_MST_CATG] WHERE CUS_CATCD = '" + uomId + "'");

                DataTable dt = dbHlpr.FetchData("SELECT '" + uomId + "' AS TEXT01 ");
                dbHlpr.CreateDownloadRecord(dt, "ALL", "CUS_MST_CATG", "DLT");

                gvInquery.CancelEdit();
                gvInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid Category ID. Unable to delete Category.";
            }
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }
    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }
    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }


    protected void CatEditCode_Init(object sender, EventArgs e)
    {
        if (!gvInquery.IsNewRowEditing)
        {
            ASPxTextBox txtCode = (ASPxTextBox)sender;
            txtCode.ReadOnly = true;
        }
        else
        {
            ASPxTextBox txtCode1 = (ASPxTextBox)sender;
            txtCode1.ReadOnly = false;
        }     
    }
}