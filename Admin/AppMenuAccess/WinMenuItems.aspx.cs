﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AppMenuAccess_WinMenuItems : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
    private string PageParent_SLCTD = string.Empty;
    private string PageType_SLCTD = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT * FROM STP_WMN_FORMS";
    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvMenuPagesInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvMenuPagesInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void gvMenuPagesInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvMenuPagesInquery, CBX_filter);
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvMenuPagesInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvMenuPagesInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvMenuPagesInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvMenuPagesInquery.SearchPanelFilter = txtFilter.Text;
        gvMenuPagesInquery.DataBind();
    }

    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxComboBox ddlType = (((ASPxComboBox)gvMenuPagesInquery.FindEditFormTemplateControl("ddlType")));
        ASPxTextBox txtSeqNo = (((ASPxTextBox)gvMenuPagesInquery.FindEditFormTemplateControl("txtSeqNo")));
        ASPxTextBox txtCode1 = (((ASPxTextBox)gvMenuPagesInquery.FindEditFormTemplateControl("txtCode")));
        ASPxTextBox txtName1 = (((ASPxTextBox)gvMenuPagesInquery.FindEditFormTemplateControl("txtName")));
        ASPxTextBox txtUrl = (((ASPxTextBox)gvMenuPagesInquery.FindEditFormTemplateControl("txtUrl")));
        ASPxComboBox ddlParent = (((ASPxComboBox)gvMenuPagesInquery.FindEditFormTemplateControl("ddlParentID")));

        if (txtCode1 == null || txtName1 == null || txtCode1.Text.Length <= 0 || txtName1.Text.Length <= 0)
        {
            ((ASPxLabel)gvMenuPagesInquery.FindEditFormTemplateControl("lblErrorMessage")).Text = "Error: Empty Fields. Please enter Data.";
            return;
        }

        string code = txtCode1.Text.Trim().ToUpper();
        string seqNo = txtSeqNo.Text.Trim();
        string name = txtName1.Text.Trim();
        string type = ddlType.Value.ToString();
        string url = txtUrl.Text.Trim();
        string parent = "0";
        if (ddlParent != null && ddlParent.Value != null)
        {
            parent = ddlParent.Value.ToString();
        }

        //check for unique code here
        DataTable myDT = dbHlpr.FetchData("SELECT * FROM STP_WMN_FORMS WHERE WMF_CODE = '" + code + "'");
        if (myDT.Rows.Count > 1)
        {
            ASPxLabel lblError = (((ASPxLabel)gvMenuPagesInquery.FindControl("lblErrorMessage")));
            ((ASPxLabel)gvMenuPagesInquery.FindEditFormTemplateControl("lblErrorMessage")).Text = "Alert: Multiple Items will be updated.";
            return;
        }
        else if (myDT.Rows.Count == 1)
        {
            dbHlpr.ExecuteNonQuery("UPDATE STP_WMN_FORMS SET "
                + " WMF_TYPE = '" + type + "', "
                + " WMF_SEQNO = '" + seqNo + "', "
                + " WMF_NAME = '" + name + "', "
                + " WMF_URL = '" + url + "', "
                + " WMF_PARNT = '" + parent + "' "
                + " WHERE WMF_CODE = '" + code + "'");

            DataTable dt = dbHlpr.FetchData("SELECT "
                + " WMF_ID AS TEXT01, WMF_CODE AS TEXT02, WMF_URL AS TEXT03, WMF_NAME AS TEXT04, "
                + " WMF_ICON AS TEXT05, WMF_PARNT AS TEXT06, WMF_TYPE AS TEXT07, WMF_SEQNO AS TEXT08 "
                + " FROM STP_WMN_FORMS "
                + " WHERE WMF_CODE = '" + code + "'");
            dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_WMN_FORMS", "UPD");
        }
        else if (myDT.Rows.Count <= 0)
        {

            if (parent.Length > 0 && type.Equals("G"))
            {
                parent = "0";
            }

            dbHlpr.ExecuteNonQuery("INSERT INTO STP_WMN_FORMS (WMF_CODE, WMF_SEQNO, WMF_NAME, WMF_PARNT, WMF_TYPE, WMF_URL) "
            + " VALUES ( "
            + " '" + code + "', "
            + " '" + seqNo + "', "
            + " '" + name + "', "
            + " '" + parent + "', "
            + " '" + type + "', "
            + " '" + url + "' "
            + " )");

            DataTable dt = dbHlpr.FetchData("SELECT "
                + " WMF_ID AS TEXT01, WMF_CODE AS TEXT02, WMF_URL AS TEXT03, WMF_NAME AS TEXT04, "
                + " WMF_ICON AS TEXT05, WMF_PARNT AS TEXT06, WMF_TYPE AS TEXT07, WMF_SEQNO AS TEXT08 "
                + " FROM STP_WMN_FORMS "
                + " WHERE WMF_CODE = '" + code + "'");
            dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_WMN_FORMS", "CRT");
        }
        else
        {
            ASPxLabel lblError = (((ASPxLabel)gvMenuPagesInquery.FindControl("lblErrorMessage")));
            ((ASPxLabel)gvMenuPagesInquery.FindEditFormTemplateControl("lblErrorMessage")).Text = "Alert: Something is not right, Please try again.";
            return;
        }

        gvMenuPagesInquery.CancelEdit();
        gvMenuPagesInquery.DataBind();
    }

    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ASPxComboBox ddlType = (ASPxComboBox)sender;
        ASPxComboBox ddlParent = (ASPxComboBox)gvMenuPagesInquery.FindEditFormTemplateControl("ddlParentID");

        DataTable dt = new DataTable();

        switch (ddlType.Value.ToString())
        {
            case "P":
                dt = dbHlpr.FetchData("SELECT * FROM [STP_WMN_FORMS] WHERE WMF_TYPE ='G' OR WMF_TYPE ='T' ");
                ddlParent.Visible = true;
                break;
            case "T":
                ddlParent.Visible = false;
                break;
            case "G":
                ddlParent.Visible = false;
                break;
            default:
                ddlParent.Visible = false;
                break;
        }

        ddlParent.DataSource = dt;
        ddlParent.DataBind();
    }//end method

    protected void gvMenuPagesInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        //codfe for delete here

        string pageCode = (e.Keys["WMF_CODE"].ToString().ToUpper());
        dbHlpr.ExecuteNonQuery("DELETE FROM STP_WMN_FORMS WHERE WMF_CODE = '" + pageCode + "'");

        DataTable dt = dbHlpr.FetchData("SELECT '" + pageCode + "' AS TEXT01 ");
        dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_WMN_FORMS", "DLT");

        e.Cancel = true;
        gvMenuPagesInquery.CancelEdit();
        gvMenuPagesInquery.DataBind();
    }

    protected void ddlParentID_Init(object sender, EventArgs e)
    {
        ASPxComboBox CBX_Parent = (ASPxComboBox)sender;

        string strWhere = "";
        if (PageType_SLCTD.Equals("P"))
        {
            strWhere = " WHERE WMF_TYPE = 'G' OR WMF_TYPE = 'T' ";
        }

        if (strWhere.Length > 0)
        {
            CBX_Parent.DataSource = dbHlpr.FetchData("SELECT * FROM [STP_WMN_FORMS] " + strWhere);
            //CBX_Parent.ValueField = "BRN_CODE";
            //CBX_Parent.TextField = "BRN_NAME";
            CBX_Parent.DataBind();

            CBX_Parent.Value = PageParent_SLCTD;

            CBX_Parent.Visible = true;
        }
        else
        {
            CBX_Parent.Visible = false;
        }
    }

    protected void gvMenuPagesInquery_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        PageParent_SLCTD = ((ASPxGridView)sender).GetRowValues(((ASPxGridView)sender).EditingRowVisibleIndex, "WMF_PARNT").ToString();
        PageType_SLCTD = ((ASPxGridView)sender).GetRowValues(((ASPxGridView)sender).EditingRowVisibleIndex, "WMF_TYPE").ToString();

        ASPxComboBox ddlParent = (((ASPxComboBox)gvMenuPagesInquery.FindEditFormTemplateControl("ddlParentID")));

        DataTable dt = new DataTable();
        switch (PageType_SLCTD)
        {
            case "P":
                dt = dbHlpr.FetchData("SELECT * FROM [STP_WMN_FORMS] WHERE WMF_TYPE ='G' OR WMF_TYPE ='T' ");
                ddlParent.Visible = true;
                break;
            case "T":
                ddlParent.Visible = false;
                break;
            case "G":
                ddlParent.Visible = false;
                break;
            default:
                ddlParent.Visible = false;
                break;
        }
        ddlParent.DataSource = dt;
        ddlParent.DataBind();
    }
}//end class