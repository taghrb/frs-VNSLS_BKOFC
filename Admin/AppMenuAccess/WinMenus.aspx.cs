﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress;
using System.Data;
using DevExpress.Web.ASPxTreeList;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

public partial class AppMenuAccess_WinMenus : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        gridExport.WriteXlsToResponse();
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        gridExport.WriteXlsxToResponse();
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }//end method

    protected void ACB_CodeCheck_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string menuCode = txtCode.Text.Trim().ToUpper();
        DataTable dtMenu = dbHlpr.FetchData("SELECT * FROM STP_USR_WNMNU WHERE MNU_CODE = '" + menuCode + "'");
        Dictionary<string, object> returnObj = new Dictionary<string, object>();

        if (dtMenu.Rows.Count > 0)
        {
            DataTable dtTree = dbHlpr.FetchData("SELECT * FROM STP_LNK_WMACS WHERE WMA_MNCOD = '" + menuCode + "'");
            DataTable dtPageIds = dbHlpr.FetchData("SELECT WMF_ID FROM STP_WMN_FORMS");

            returnObj.Add("MenuInfo", dtMenu.Rows);
            returnObj.Add("TreeInfo", dtTree.Rows);
            returnObj.Add("PageIds", dtPageIds.Rows);
        }
        else
        {
            returnObj.Add("MenuInfo", null);
            returnObj.Add("TreeInfo", null);
            returnObj.Add("PageIds", null);
        }

        e.Result = JsonConvert.SerializeObject(returnObj);
    }//end method

    protected void btnClear_Click(object sender, EventArgs e)
    {

    }//end method

    protected void btnSave_Click(object sender, EventArgs e)
    {

    }//end method

    protected void btnPuAddMenuCode_Click(object sender, EventArgs e)
    {

    }//end method

    protected void treeList_DataBound(object sender, EventArgs e)
    {

    }//end method

    protected void treeList_CustomDataCallback(object sender, DevExpress.Web.ASPxTreeList.TreeListCustomDataCallbackEventArgs e)
    {

    }//end method

    protected void cb_Init(object sender, EventArgs e)
    {
        //     gridUserMenu.BeginUpdate();
        //     gridUserMenu.ClearSort();
        ////     gridUserMenu.GroupBy(gridUserMenu.Columns["GROUPCODE"]);
        //     gridUserMenu.EndUpdate();
        //     gridUserMenu.ExpandAll();

        // (gridUserMenu.Columns[0] as GridViewDataColumn).GroupBy();
        //// gridUserMenu.Columns[0] as GridViewDataColumn).GroupBy();
        // //ASPXGridView  view = gridUserMenu;
        // ////(GridViewDataColumn)gridUserMenu.Columns["GROUPCODE"].GroupBy();
        // ////gridUserMenu.Columns[0].Grid.AllColumns.GroupBy();
        // //gridUserMenu.GroupBy(gridUserMenu.Columns["GROUPCODE"], 0);
        // gridUserMenu.Settings.ShowGroupedColumns = true;
        // gridUserMenu.Settings.ShowGroupPanel = false;
    }

    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ///////////////////////////////////////////////////////
        ////// add menu code fire in MNU Database Table ///////
        ///////////////////////////////////////////////////////
        if (txtCode.Text == "" || txtName.Text == "")
        {
            //lblMessage.ForeColor = System.Drawing.Color.Red;
            //lblMessage.Text = "Error: Empty Fields. Please enter Data.";
            return;
        }
        string MnuCode = txtCode.Text.ToUpper();
        string MnuName = txtName.Text;

        DataTable dt = dbHlpr.FetchData("SELECT * FROM STP_USR_WNMNU WHERE MNU_CODE = '" + MnuCode + "'");
        if (dt.Rows.Count > 0)
        {
            dbHlpr.ExecuteNonQuery("UPDATE STP_USR_WNMNU SET MNU_NAME = '" + MnuName + "' WHERE MNU_CODE = '" + MnuCode + "'");

            DataTable dtMnuMstr = dbHlpr.FetchData("SELECT "
                + " MNU_KEY AS TEXT01, MNU_CODE AS TEXT02, MNU_NAME AS TEXT03 "
                + " FROM STP_USR_WNMNU "
                + " WHERE MNU_CODE = '" + MnuCode + "'");
            dbHlpr.CreateDownloadRecord(dtMnuMstr, "ALL", "STP_USR_WNMNU", "UPD");

            dbHlpr.ExecuteNonQuery("DELETE FROM STP_LNK_WMACS WHERE WMA_MNCOD = '" + MnuCode + "'");

            DataTable dtMnuAccs = dbHlpr.FetchData("SELECT '" + MnuCode + "' AS TEXT02 ");
            dbHlpr.CreateDownloadRecord(dtMnuAccs, "ALL", "STP_LNK_WMACS", "DLT");

            lblMessage.ForeColor = System.Drawing.Color.Green;
            lblMessage.Text = "Menu updated successfully";
        }
        else
        {
            int insertId = int.Parse(dbHlpr.ExecuteScalarQuery("INSERT INTO STP_USR_WNMNU (MNU_CODE, MNU_NAME) OUTPUT INSERTED.MNU_KEY VALUES ('" + MnuCode + "', '" + MnuName + "')").ToString());
            if (insertId == 1 && insertId != 0)
            {
                lblMessage.ForeColor = System.Drawing.Color.Green;
                lblMessage.Text = "Successfully Inserted";
            }
            else
            {
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblMessage.Text = "Alert: Something went wrong. Please try again";
                return;
            }
        }

        //////////////////////////////////////////////////////

        ASPxTreeList2.ExpandAll();

        //////////////////////////////////////////////////////
        ///////////////// Handle Tree here now ///////////////
        //////////////////////////////////////////////////////

        List<TreeListNode> selectedNodes = ASPxTreeList2.GetSelectedNodes();
        foreach (DevExpress.Web.ASPxTreeList.TreeListNode tempNode in selectedNodes)
        {
            //get data of node
            string WMF_ID = tempNode.GetValue("WMF_ID").ToString();
            string WMF_NAME = tempNode.GetValue("WMF_NAME").ToString();
            string WMF_CODE = tempNode.GetValue("WMF_CODE").ToString();
            string WMF_PARNT = tempNode.GetValue("WMF_PARNT").ToString();
            string WMF_TYPE = tempNode.GetValue("WMF_TYPE").ToString();

            InsertTreeData(tempNode, MnuCode);
        }
    }//end method

    private void InsertTreeData(DevExpress.Web.ASPxTreeList.TreeListNode currentNode, string MnuCode)
    {
        if (currentNode.ParentNode != null)
        {
            InsertTreeData(currentNode.ParentNode, MnuCode);
        }

        InsertNodeInDbIfNotExists(currentNode, MnuCode);
    }

    private void InsertNodeInDbIfNotExists(DevExpress.Web.ASPxTreeList.TreeListNode nodeToInsert, string MnuCode)
    {
        string menuCode = MnuCode;
        //if (nodeToInsert.GetValue("WMF_ID") != null)
        if (nodeToInsert.GetValue("WMF_CODE") != null)
        {
            string pageId = nodeToInsert.GetValue("WMF_ID").ToString();
            string pageName = nodeToInsert.GetValue("WMF_NAME").ToString();
            string pageCode = nodeToInsert.GetValue("WMF_CODE").ToString();
            string pageType = nodeToInsert.GetValue("WMF_TYPE").ToString();

            //DataTable dt = dbHlpr.FetchData("SELECT MPG_KEY FROM STP_LNK_WMACS WHERE WMA_MNCOD = '" + menuCode + "' AND MPG_PGID = '" + pageId + "' AND WMA_PGCOD  = '" + pageCode + "'");
            DataTable dt = dbHlpr.FetchData("SELECT WMA_KEY FROM STP_LNK_WMACS WHERE WMA_MNCOD = '" + menuCode + "' AND WMA_PGCOD  = '" + pageCode + "'");
            if (dt.Rows.Count == 0)
            {
                dbHlpr.ExecuteNonQuery("INSERT INTO STP_LNK_WMACS (WMA_KEY, WMA_MNCOD, WMA_PGCOD, WMA_PGID, WMA_PGTYP, WMA_PGNAM) VALUES ("
                    + "'" + (menuCode + pageCode) + "', "
                    + "'" + menuCode + "', "
                    + "'" + pageCode + "', "
                    + "'" + pageId + "', "
                    + "'" + pageType + "', "
                    + "'" + pageName + "' )"
                    );

                DataTable dt1 = dbHlpr.FetchData("SELECT "
                    + " WMA_KEY AS TEXT01, WMA_MNCOD AS TEXT02, WMA_PGCOD AS TEXT03, WMA_PGID AS TEXT04, "
                    + " WMA_PGNAM AS TEXT05, WMA_PGTYP AS TEXT06, WMA_PGURL AS TEXT07 "
                    + " FROM STP_LNK_WMACS "
                    + " WHERE WMA_KEY = '" + (menuCode + pageCode) + "'");
                dbHlpr.CreateDownloadRecord(dt1, "ALL", "STP_LNK_WMACS", "CRT");
            }
        }
    }
}//end class