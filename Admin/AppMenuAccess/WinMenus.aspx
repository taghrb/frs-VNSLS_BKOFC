﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="WinMenus.aspx.cs" Inherits="AppMenuAccess_WinMenus" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dx" %>


<asp:Content ID="Content3" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <script type="text/javascript">
        function grid_customizationWindowCloseUp(s, e) {
            FromGridView.ShowCustomizationWindow();
        }

        $(function () {
            $('.testing123').dialog();
        });

    </script>
</asp:Content>

<asp:Content ID="contentTopHere1" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
            <li>
                <asp:ImageButton ID="LinkBtnPDF" OnClick="LinkBtnPDF_Click" runat="server" ImageUrl="~/img/FilesIcons/acrobat.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonXLX" OnClick="LinkButtonXLX_Click" runat="server" ImageUrl="~/img/FilesIcons/excel.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonXxls" OnClick="LinkButtonXxls_Click" runat="server" ImageUrl="~/img/FilesIcons/xlsx.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonRtf" OnClick="LinkButtonRtf_Click" runat="server" ImageUrl="~/img/FilesIcons/rtf.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonCvs" OnClick="LinkButtonCvs_Click" runat="server" ImageUrl="~/img/FilesIcons/cvs.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonWord" OnClick="LinkButtonRtf_Click" runat="server" ImageUrl="~/img/FilesIcons/word.png" Width="50" Height="40"></asp:ImageButton>
            </li>

            <li>
                <asp:ImageButton ID="LinkButtonHtml" OnClick="LinkButtonHtml_Click" runat="server" ImageUrl="~/img/icons/searchtop.jpg" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton6" OnClick="LinkButton3_Click" runat="server" ImageUrl="~/img/FilesIcons/acrobat.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton7" OnClick="LinkButton3_Click" runat="server" ImageUrl="~/img/FilesIcons/word.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton8" OnClick="LinkButton3_Click" runat="server" ImageUrl="~/img/FilesIcons/excel.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton9" OnClick="LinkButton3_Click" OnClientClick="window.print();" runat="server" ImageUrl="~/img/FilesIcons/acrobat.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton10" OnClick="LinkButton3_Click" runat="server" ImageUrl="~/img/FilesIcons/torrent.png" Width="50" Height="40"></asp:ImageButton>
            </li>
        </ul>
        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvMenuPagesInquery" ExportedRowType="All"></dx:ASPxGridViewExporter>
    </div>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">
    <div class="formDiv">
        <div class="formHeaderDiv">
            <h3>Create Menu</h3>
        </div>
        <fieldset class="FieldSetTop">
            <table>
                <tr>
                    <td>Code : </td>
                    <td>
                        <div class="DivReq">
                            <dx:ASPxComboBox DataSourceID="DS_Menus" ID="txtCode" TextField="MNU_CODE" ValueField="MNU_CODE" ClientInstanceName="txtClientCode" runat="server" Width="170px" TabIndex="0" MaxLength="5">
                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="Text" ErrorTextPosition="Right" SetFocusOnError="true">
                                    <ErrorFrameStyle Font-Size="Smaller" />
                                    <RequiredField IsRequired="true" />
                                </ValidationSettings>
                                <NullTextStyle Font-Size="Small" />
                                <ClientSideEvents ValueChanged="function(s, e) { CallBackCode.PerformCallback(s.GetText()); }"></ClientSideEvents>
                            </dx:ASPxComboBox>
                            <dx:ASPxCallback ID="ACB_CodeCheck" runat="server" ClientInstanceName="CallBackCode" OnCallback="ACB_CodeCheck_Callback">
                                <ClientSideEvents CallbackComplete="function(s,e) {
                                                        callBackFunction(s, e);
                                                        }" />
                            </dx:ASPxCallback>
                            <asp:SqlDataSource runat="server" ID="DS_Menus" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>' SelectCommand="SELECT MNU_CODE FROM STP_USR_WNMNU " />
                        </div>
                    </td>
                    <td>Name : </td>
                    <td>
                        <div class="DivReq">
                            <dx:ASPxTextBox ID="txtName" ClientInstanceName="txtClientName" MaxLength="50" runat="server" Width="170px" Text="" TabIndex="1">
                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="Text" ErrorTextPosition="Right" SetFocusOnError="true">
                                    <ErrorFrameStyle Font-Size="Smaller" />
                                    <RequiredField IsRequired="true" />
                                </ValidationSettings>
                                <NullTextStyle Font-Size="Small" />
                            </dx:ASPxTextBox>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <dx:ASPxLabel ID="lblMessage" Text="" runat="server"></dx:ASPxLabel>
                    </td>
                </tr>
            </table>
        </fieldset>
        <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%; background-color: #A6E3E3;">
            <dx:ASPxButton ID="btnAdd1" runat="server" Text="Save" CssClass="btnPopup btn btn-success" CausesValidation="true" OnClick="btnAdd1_Click" />
            <%--<dx:ASPxButton ID="btnUpdate1" runat="server" Text="Update" CssClass="btnPopup btn btn-primary" OnClick="btnUpdate1_Click" />--%>
            <dx:ASPxButton ID="btnClear1" runat="server" Text="Clear" CssClass="btnPopup btn btn-warning"></dx:ASPxButton>
        </div>

        <dx:ASPxTreeList ID="ASPxTreeList2" ClientInstanceName="treeList" runat="server" ParentFieldName="WMF_PARNT" KeyFieldName="WMF_ID" DataSourceID="SqlDataSource1" Theme="Office2010Blue">
            <SettingsBehavior AllowFocusedNode="True" AutoExpandAllNodes="true"></SettingsBehavior>
            <SettingsSelection Enabled="True" Recursive="true"></SettingsSelection>
            <Settings GridLines="Horizontal" />
            <Styles>
                <AlternatingNode Enabled="True" />
            </Styles>
        </dx:ASPxTreeList>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM [STP_WMN_FORMS]"></asp:SqlDataSource>
    </div>
    <script type="text/javascript">
        function callBackFunction(s, e) {
            var jsonData = JSON.parse(e.result);
            if (jsonData.MenuInfo == null) {
                txtClientName.SetText("");
                return;
            }
            // Set Menu Title
            txtClientName.SetText(jsonData.MenuInfo[0].ItemArray[2]);
            // Set all checkboxes to unchecked
            for (i = 0; i < jsonData.PageIds.length; i++) {
                treeList.SelectNode(jsonData.PageIds[i].ItemArray[0], false);
            }

            // Set Checkboxes for the selected menu only
            for (i = 0; i < jsonData.TreeInfo.length; i++) {
                treeList.SelectNode(jsonData.TreeInfo[i].ItemArray[3]);
            }
        }
    </script>
</asp:Content>
