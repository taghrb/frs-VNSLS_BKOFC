﻿<%@ Page Title="Price Deviation Report" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="PriceDeviation.aspx.cs" Inherits="Admin_Reports_PriceDeviation" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>


<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <script type="text/javascript">

        function grid_customizationWindowCloseUp(s, e) {
            gvUOMClientInquery.ShowCustomizationWindow();
        }

        // Code to create a custom multiselect ComboBox, Dropdown
        var textSeparator = ","; // Between each checked item in ComboBox
        function OnListBoxSelectionChanged(listBox, args) {
            if (args.index == 0)
                args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
            if (listBox.name.indexOf("lstBxStrItems") != -1) {
                IsAllSelected(checkStrListBox) ? checkStrListBox.SelectIndices([0]) : checkStrListBox.UnselectIndices([0]);
                UpdateText(ClientStrNo, checkStrListBox);
            } else if (listBox.name.indexOf("lstBxRegItems") != -1) {
                IsAllSelected(checkRegListBox) ? checkRegListBox.SelectIndices([0]) : checkRegListBox.UnselectIndices([0]);
                UpdateText(ClientRegNo, checkRegListBox);
            }
        }
        function IsAllSelected(chkLstBx) {
            var selectedDataItemCount = chkLstBx.GetItemCount() - (chkLstBx.GetItem(0).selected ? 0 : 1);
            return chkLstBx.GetSelectedItems().length == selectedDataItemCount;
        }
        function UpdateText(ddObj, lstBxObj) {
            var selectedItems = lstBxObj.GetSelectedItems();
            ddObj.SetText(GetSelectedItemsText(selectedItems));
        }
        function SynchronizeListBoxValues(dropDown, args) {
            var texts = dropDown.GetText().split(textSeparator);
            if (dropDown.name.indexOf("StrNo") != -1) {
                checkStrListBox.UnselectAll();
                checkStrListBox.SelectValues(texts);
                IsAllSelected(checkStrListBox) ? checkStrListBox.SelectIndices([0]) : checkStrListBox.UnselectIndices([0]);
                UpdateText(ClientStrNo, checkStrListBox);
            } else if (dropDown.name.indexOf("RegNo") != -1) {
                checkRegListBox.UnselectAll();
                checkRegListBox.SelectValues(texts);
                IsAllSelected(checkRegListBox) ? checkRegListBox.SelectIndices([0]) : checkRegListBox.UnselectIndices([0]);
                UpdateText(ClientRegNo, checkRegListBox);
            }
        }
        function GetSelectedItemsText(items) {
            var texts = [];
            for (var i = 0; i < items.length; i++)
                if (items[i].index != 0)
                    texts.push(items[i].value);
            return texts.join(textSeparator);
        }
        function GetValuesByTexts(texts, chkLstBx) {
            var actualValues = [];
            var item;
            for (var i = 0; i < texts.length; i++) {
                item = chkLstBx.FindItemByText(texts[i]);
                if (item != null)
                    actualValues.push(item.value);
            }
            return actualValues;
        }

    </script>
    <style type="text/css">
        .fieldsetWrapper {
            padding: 10px;
            width: 70%;
            margin: 0 auto;
            background-color: #F7F5F1;
            margin-top: 5px;
            border-radius: 10px;
        }

        .dxeDropDownWindow.dxpc-content .dxgvControl {
            display: inline-block !important;
            min-width: 300px;
        }
    </style>
</asp:Content>
<asp:Content ID="contentTopHere1" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
            <li>&nbsp;
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">

    <div class="formDiv" style="background-color: transparent;">
        <div class="formHeaderDiv">
            <h3>Price Deviation Report</h3>
        </div>
        <div class="fieldsetWrapper">
            <fieldset>
                <legend></legend>
                <table style="margin: 0 auto;">
                    <tr>
                        <td>Format : &nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxComboBox ID="RptFormat" runat="server" AnimationType="Auto" OnSelectedIndexChanged="RptFormat_SelectedIndexChanged" AutoPostBack="true">
                                    <Items>
                                        <dx:ListEditItem Text="Flash" Value="Flash" Selected="true" />
                                        <dx:ListEditItem Text="Brief" Value="Brief" />
                                        <dx:ListEditItem Text="Detail" Value="Detail" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </div>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>Recap By : &nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxComboBox ID="RptRecapBy" runat="server" AnimationType="Auto" ClientEnabled="false">
                                    <Items>
                                        <dx:ListEditItem Text="Select..." Value="" Selected="true" />
                                        <dx:ListEditItem Text="By Store By Van" Value="StrByVan" />
                                        <dx:ListEditItem Text="By Item By Store" Value="ItmByStr" />
                                    </Items>
                                    <ValidationSettings ErrorTextPosition="Bottom" RequiredField-IsRequired="true" RequiredField-ErrorText="Recap by is required" />
                                </dx:ASPxComboBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Ticket Type :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxComboBox ID="TicketType" runat="server" AnimationType="Auto" OnSelectedIndexChanged="RptFormat_SelectedIndexChanged">
                                    <Items>
                                        <dx:ListEditItem Text="All" Value="" Selected="true" />
                                        <dx:ListEditItem Text="Sales" Value="Sales" />
                                        <dx:ListEditItem Text="Return" Value="Return" />
                                        <dx:ListEditItem Text="Void" Value="Void" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </div>
                        </td>
                        <td></td>

                        <td>Pay Code :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxComboBox ID="PayCode" runat="server" AnimationType="Auto" OnSelectedIndexChanged="RptFormat_SelectedIndexChanged">
                                    <Items>
                                        <dx:ListEditItem Text="All" Value="" Selected="true" />
                                        <dx:ListEditItem Text="Cash" Value="CASH" />
                                        <dx:ListEditItem Text="A/R" Value="A/R" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </div>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>Reason Code :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxGridLookup
                                    ID="ReasonCode" runat="server" TextFormatString="{0}" KeyFieldName="PRS_CODE"
                                    DataSourceID="SqlDS_ReasonCode" SelectionMode="Multiple" MultiTextSeparator=","
                                    GridViewProperties-SettingsBehavior-AllowSelectByRowClick="false"
                                    NullText="All" NullTextStyle-ForeColor="Black">
                                    <GridViewProperties>
                                        <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                                        <SettingsPager PageSizeItemSettings-ShowAllItem="true" PageSize="10" />
                                    </GridViewProperties>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="true" Width="10%" SelectAllCheckboxMode="AllPages" />
                                        <dx:GridViewDataColumn Caption="Code" FieldName="PRS_CODE" Width="20%" />
                                        <dx:GridViewDataColumn Caption="Title" FieldName="PRS_TITLE" Width="70%" />
                                    </Columns>
                                </dx:ASPxGridLookup>
                                <asp:SqlDataSource ID="SqlDS_ReasonCode" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                    SelectCommand="SELECT PRS_CODE, PRS_TITLE FROM STP_MST_PRSNS"></asp:SqlDataSource>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Date From :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxDateEdit runat="server" ID="DateFrom" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy" NullText="Earliest" NullTextStyle-ForeColor="Black"></dx:ASPxDateEdit>
                            </div>
                        </td>
                        <td></td>

                        <td>Date To :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxDateEdit runat="server" ID="DateTo" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy" NullText="Latest" NullTextStyle-ForeColor="Black"></dx:ASPxDateEdit>
                            </div>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>Store # :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxDropDownEdit ID="StrNo" OnInit="StrNo_Init" ClientInstanceName="ClientStrNo" CssClass="multiselect_combobox" runat="server" AnimationType="Auto">
                                    <DropDownWindowTemplate>
                                        <dx:ASPxListBox Width="170px" ID="lstBxStrItems" ClientInstanceName="checkStrListBox" SelectionMode="CheckColumn" runat="server" OnValueChanged="StoreChanged" AutoPostBack="true">
                                            <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged" />
                                        </dx:ASPxListBox>
                                    </DropDownWindowTemplate>
                                    <ClientSideEvents TextChanged="SynchronizeListBoxValues" DropDown="SynchronizeListBoxValues" />
                                </dx:ASPxDropDownEdit>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Item # :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxGridLookup
                                    ID="Items" runat="server" TextFormatString="{0}" KeyFieldName="PRO_CODE"
                                    DataSourceID="SqlDS_Items" SelectionMode="Multiple" MultiTextSeparator=","
                                    GridViewProperties-SettingsBehavior-AllowSelectByRowClick="false"
                                    NullText="All" NullTextStyle-ForeColor="Black">
                                    <GridViewProperties>
                                        <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                                        <SettingsPager PageSizeItemSettings-ShowAllItem="true" PageSize="10" />
                                    </GridViewProperties>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="true" Width="10%" SelectAllCheckboxMode="AllPages" />
                                        <dx:GridViewDataColumn Caption="Item #" FieldName="PRO_CODE" Width="20%" />
                                        <dx:GridViewDataColumn Caption="Desc" FieldName="PRO_DESC1" Width="70%" />
                                    </Columns>
                                </dx:ASPxGridLookup>
                                <asp:SqlDataSource ID="SqlDS_Items" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                    SelectCommand="SELECT PRO_CODE, PRO_DESC1 FROM INV_MSTR_PRODT ORDER BY PRO_CODE"></asp:SqlDataSource>
                            </div>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td></td>
                        <td></td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>Register # :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxDropDownEdit ID="RegNo" OnInit="RegNo_Init" ClientInstanceName="ClientRegNo" CssClass="multiselect_combobox" runat="server" AnimationType="Auto">
                                    <DropDownWindowTemplate>
                                        <dx:ASPxListBox Width="170px" ID="lstBxRegItems" ClientInstanceName="checkRegListBox" SelectionMode="CheckColumn" runat="server">
                                            <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged" />
                                        </dx:ASPxListBox>
                                    </DropDownWindowTemplate>
                                    <ClientSideEvents TextChanged="SynchronizeListBoxValues" DropDown="SynchronizeListBoxValues" />
                                </dx:ASPxDropDownEdit>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Catg :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxGridLookup
                                    ID="Catg" runat="server" TextFormatString="{0}" KeyFieldName="INV_CATCD"
                                    DataSourceID="SqlDS_Catgs" SelectionMode="Multiple" MultiTextSeparator=","
                                    GridViewProperties-SettingsBehavior-AllowSelectByRowClick="false"
                                    NullText="All" NullTextStyle-ForeColor="Black">
                                    <GridViewProperties>
                                        <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                                        <SettingsPager PageSizeItemSettings-ShowAllItem="true" PageSize="10" />
                                    </GridViewProperties>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="true" Width="10%" SelectAllCheckboxMode="AllPages" />
                                        <dx:GridViewDataColumn Caption="Item #" FieldName="INV_CATCD" Width="20%" />
                                        <dx:GridViewDataColumn Caption="Desc" FieldName="INV_CATNM" Width="70%" />
                                    </Columns>
                                </dx:ASPxGridLookup>
                                <asp:SqlDataSource ID="SqlDS_Catgs" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                    SelectCommand="SELECT INV_CATCD, INV_CATNM FROM STP_MSTR_ICATG WHERE INV_CATYP = 'C' ORDER BY INV_CATCD "></asp:SqlDataSource>
                            </div>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>Class :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxGridLookup
                                    ID="Udf1" runat="server" TextFormatString="{0}" KeyFieldName="INV_CATCD"
                                    DataSourceID="SqlDS_Udf1" SelectionMode="Multiple" MultiTextSeparator=","
                                    GridViewProperties-SettingsBehavior-AllowSelectByRowClick="false"
                                    NullText="All" NullTextStyle-ForeColor="Black">
                                    <GridViewProperties>
                                        <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                                        <SettingsPager PageSizeItemSettings-ShowAllItem="true" PageSize="10" />
                                    </GridViewProperties>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="true" Width="10%" SelectAllCheckboxMode="AllPages" />
                                        <dx:GridViewDataColumn Caption="Item #" FieldName="INV_CATCD" Width="20%" />
                                        <dx:GridViewDataColumn Caption="Desc" FieldName="INV_CATNM" Width="70%" />
                                    </Columns>
                                </dx:ASPxGridLookup>
                                <asp:SqlDataSource ID="SqlDS_Udf1" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                    SelectCommand="SELECT INV_CATCD, INV_CATNM FROM STP_MSTR_ICATG WHERE INV_CATYP = '1' ORDER BY INV_CATCD "></asp:SqlDataSource>
                            </div>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>Type :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxGridLookup
                                    ID="Udf3" runat="server" TextFormatString="{0}" KeyFieldName="INV_CATCD"
                                    DataSourceID="SqlDS_Udf3" SelectionMode="Multiple" MultiTextSeparator=","
                                    GridViewProperties-SettingsBehavior-AllowSelectByRowClick="false"
                                    NullText="All" NullTextStyle-ForeColor="Black">
                                    <GridViewProperties>
                                        <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                                        <SettingsPager PageSizeItemSettings-ShowAllItem="true" PageSize="10" />
                                    </GridViewProperties>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="true" Width="10%" SelectAllCheckboxMode="AllPages" />
                                        <dx:GridViewDataColumn Caption="Item #" FieldName="INV_CATCD" Width="20%" />
                                        <dx:GridViewDataColumn Caption="Desc" FieldName="INV_CATNM" Width="70%" />
                                    </Columns>
                                </dx:ASPxGridLookup>
                                <asp:SqlDataSource ID="SqlDS_Udf3" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                    SelectCommand="SELECT INV_CATCD, INV_CATNM FROM STP_MSTR_ICATG WHERE INV_CATYP = '3' ORDER BY INV_CATCD "></asp:SqlDataSource>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Sub Catg :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxGridLookup
                                    ID="SubCatg" runat="server" TextFormatString="{0}" KeyFieldName="INV_CATCD"
                                    DataSourceID="SqlDS_SubCatg" SelectionMode="Multiple" MultiTextSeparator=","
                                    GridViewProperties-SettingsBehavior-AllowSelectByRowClick="false"
                                    NullText="All" NullTextStyle-ForeColor="Black">
                                    <GridViewProperties>
                                        <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                                        <SettingsPager PageSizeItemSettings-ShowAllItem="true" PageSize="10" />
                                    </GridViewProperties>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="true" Width="10%" SelectAllCheckboxMode="AllPages" />
                                        <dx:GridViewDataColumn Caption="Item #" FieldName="INV_CATCD" Width="20%" />
                                        <dx:GridViewDataColumn Caption="Desc" FieldName="INV_CATNM" Width="70%" />
                                    </Columns>
                                </dx:ASPxGridLookup>
                                <asp:SqlDataSource ID="SqlDS_SubCatg" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                    SelectCommand="SELECT INV_CATCD, INV_CATNM FROM STP_MSTR_ICATG WHERE INV_CATYP = 'S' ORDER BY INV_CATCD "></asp:SqlDataSource>
                            </div>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>Sub Class :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxGridLookup
                                    ID="Udf2" runat="server" TextFormatString="{0}" KeyFieldName="INV_CATCD"
                                    DataSourceID="SqlDS_Udf2" SelectionMode="Multiple" MultiTextSeparator=","
                                    GridViewProperties-SettingsBehavior-AllowSelectByRowClick="false"
                                    NullText="All" NullTextStyle-ForeColor="Black">
                                    <GridViewProperties>
                                        <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                                        <SettingsPager PageSizeItemSettings-ShowAllItem="true" PageSize="10" />
                                    </GridViewProperties>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="true" Width="10%" SelectAllCheckboxMode="AllPages" />
                                        <dx:GridViewDataColumn Caption="Item #" FieldName="INV_CATCD" Width="20%" />
                                        <dx:GridViewDataColumn Caption="Desc" FieldName="INV_CATNM" Width="70%" />
                                    </Columns>
                                </dx:ASPxGridLookup>
                                <asp:SqlDataSource ID="SqlDS_Udf2" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                    SelectCommand="SELECT INV_CATCD, INV_CATNM FROM STP_MSTR_ICATG WHERE INV_CATYP = '2' ORDER BY INV_CATCD "></asp:SqlDataSource>
                            </div>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>Group :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxGridLookup
                                    ID="Udf4" runat="server" TextFormatString="{0}" KeyFieldName="INV_CATCD"
                                    DataSourceID="SqlDS_Udf4" SelectionMode="Multiple" MultiTextSeparator=","
                                    GridViewProperties-SettingsBehavior-AllowSelectByRowClick="false"
                                    NullText="All" NullTextStyle-ForeColor="Black">
                                    <GridViewProperties>
                                        <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                                        <SettingsPager PageSizeItemSettings-ShowAllItem="true" PageSize="10" />
                                    </GridViewProperties>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="true" Width="10%" SelectAllCheckboxMode="AllPages" />
                                        <dx:GridViewDataColumn Caption="Item #" FieldName="INV_CATCD" Width="20%" />
                                        <dx:GridViewDataColumn Caption="Desc" FieldName="INV_CATNM" Width="70%" />
                                    </Columns>
                                </dx:ASPxGridLookup>
                                <asp:SqlDataSource ID="SqlDS_Udf4" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                    SelectCommand="SELECT INV_CATCD, INV_CATNM FROM STP_MSTR_ICATG WHERE INV_CATYP = '4' ORDER BY INV_CATCD "></asp:SqlDataSource>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Cust # :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxGridLookup
                                    ID="CustNo" runat="server" TextFormatString="{0}" KeyFieldName="CUS_NO"
                                    DataSourceID="SqlDS_CustNo" SelectionMode="Multiple" MultiTextSeparator=","
                                    GridViewProperties-SettingsBehavior-AllowSelectByRowClick="false"
                                    NullText="All" NullTextStyle-ForeColor="Black">
                                    <GridViewProperties>
                                        <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                                        <SettingsPager PageSizeItemSettings-ShowAllItem="true" PageSize="10" />
                                    </GridViewProperties>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="true" Width="10%" SelectAllCheckboxMode="AllPages" />
                                        <dx:GridViewDataColumn Caption="Cust #" FieldName="CUS_NO" Width="20%" />
                                        <dx:GridViewDataColumn Caption="Name" FieldName="CUS_NAME" Width="70%" Settings-AutoFilterCondition="Contains" />
                                    </Columns>
                                </dx:ASPxGridLookup>
                                <asp:SqlDataSource ID="SqlDS_CustNo" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                    SelectCommand="SELECT CUS_NO, CUS_NAME FROM STP_MST_CSTMR ORDER BY CUS_NO "></asp:SqlDataSource>
                            </div>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>Cust Catg :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxGridLookup
                                    ID="CustCatg" runat="server" TextFormatString="{0}" KeyFieldName="CUS_CATCD"
                                    DataSourceID="SqlDS_CustCatg" SelectionMode="Multiple" MultiTextSeparator=","
                                    GridViewProperties-SettingsBehavior-AllowSelectByRowClick="false"
                                    NullText="All" NullTextStyle-ForeColor="Black">
                                    <GridViewProperties>
                                        <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                                        <SettingsPager PageSizeItemSettings-ShowAllItem="true" PageSize="10" />
                                    </GridViewProperties>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="true" Width="10%" SelectAllCheckboxMode="AllPages" />
                                        <dx:GridViewDataColumn Caption="Code" FieldName="CUS_CATCD" Width="20%" />
                                        <dx:GridViewDataColumn Caption="Title" FieldName="CUS_CATNM" Width="70%" />
                                    </Columns>
                                </dx:ASPxGridLookup>
                                <asp:SqlDataSource ID="SqlDS_CustCatg" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                    SelectCommand="SELECT CUS_CATCD, CUS_CATNM FROM CUS_MST_CATG ORDER BY CUS_CATCD "></asp:SqlDataSource>
                            </div>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>Cust Group :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxGridLookup
                                    ID="CustGroup" runat="server" TextFormatString="{0}" KeyFieldName="CGP_CODE"
                                    DataSourceID="SqlDS_CustGroup" SelectionMode="Multiple" MultiTextSeparator=","
                                    GridViewProperties-SettingsBehavior-AllowSelectByRowClick="false"
                                    NullText="All" NullTextStyle-ForeColor="Black">
                                    <GridViewProperties>
                                        <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                                        <SettingsPager PageSizeItemSettings-ShowAllItem="true" PageSize="10" />
                                    </GridViewProperties>
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="true" Width="10%" SelectAllCheckboxMode="AllPages" />
                                        <dx:GridViewDataColumn Caption="Code" FieldName="CGP_CODE" Width="20%" />
                                        <dx:GridViewDataColumn Caption="Title" FieldName="CGP_NAME" Width="70%" />
                                    </Columns>
                                </dx:ASPxGridLookup>
                                <asp:SqlDataSource ID="SqlDS_CustGroup" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                    SelectCommand="SELECT CGP_CODE, CGP_NAME FROM STP_MSTR_CUSGP ORDER BY CGP_CODE "></asp:SqlDataSource>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Comments :&nbsp;&nbsp;&nbsp;</td>
                        <td colspan="7">
                            <div class="DivReq">
                                <dx:ASPxTextBox runat="server" ID="txtComments" Width="100%"></dx:ASPxTextBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </fieldset>
            <hr style="color: skyblue" />
            <dx:ASPxLabel ID="lblErrorMessage" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
            <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                <dx:ASPxButton ID="btnGenReport" runat="server" Text="Load Report" CssClass="btnPopup btn btn-success" CausesValidation="true" OnClick="btnGenReport_Click" />
                <dx:ASPxButton ID="btnPopUpClear" runat="server" Text="Clear" CssClass="btnPopup btn btn-warning" CausesValidation="false" AutoPostBack="false">
                    <ClientSideEvents Click="function(s, e) { CallBackCode.PerformCallback(CatClientEditCode.GetText()); }" />
                </dx:ASPxButton>
            </div>

            <CR:CrystalReportViewer ViewStateMode="Enabled" HasCrystalLogo="False" ToolPanelView="None" Style="margin: 0 auto;" ID="RptVwr" runat="server" AutoDataBind="false" />
        </div>
    </div>
    <script type="text/javascript">
        window.onload = function () {
            $('form').on('click', '.btnPopupClose', function () {
                $('.overlay').hide();
            });
        };

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //if (prm != null) {
        prm.add_endRequest(function (sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.overlay').show();
            }
        });
        prm.add_endRequest(GridOnEndCallBack);
        //};

    </script>
</asp:Content>

