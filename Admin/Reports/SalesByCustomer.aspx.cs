﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using System.Drawing;
using DevExpress.XtraPrinting;
using DevExpress.Export.Xl;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class Admin_Reports_SalesByCustomer : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
    ReportsDS ReportsDataSet = new ReportsDS();
    string START_DATE = "Earliest";
    string END_DATE = "Latest";

    protected void Page_Init(object sender, EventArgs e)
    {
        //DateFrom.Value = new DateTime(1900, 1, 1);
        //DateTo.Value = new DateTime(9999, 12, 31);

        if (Session["Rpt"] != null && IsPostBack)
        {
            RptVwr.ReportSource = Session["Rpt"];
        }
    }

    protected void btnExportSalesByCustomer_Click(object sender, EventArgs e)
    {
        START_DATE = DateFrom.Value != null ? ((DateTime)DateFrom.Value).ToString("yyyy-MM-dd") : new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");
        END_DATE = DateTo.Value != null ? ((DateTime)DateTo.Value).ToString("yyyy-MM-dd") : new DateTime(9999, 12, 31).ToString("yyyy-MM-dd");

        string oneStore = StrNo.Value.ToString();
        string oneRegister = RegNo.Value.ToString();

        if (CmBxReportType.Value == "RCPT")
        {
            DataTable SLSHdrDT = new DataTable();
            DataTable SLSLinDT = new DataTable();

            SLSHdrDT = dbHlpr.FetchData("SELECT "
                    + " TKH_TKTNO, TKH_CUSNO, TKH_CUSNM, TKH_CUSVT, TKH_SREP,TKH_DATE, TKH_TIME, TKH_CRTDT, TKH_CRTIM, TKH_CRTBY, "
                    + " TKH_SLAMT,TKH_DSCNT, TKH_NTAMT, TKH_VTAMT, TKH_AMT, TKH_AMTPD, TKH_PAYCD, TKH_CHNGE, TKH_BLANC, TKH_TTYPE "
                    + " FROM SLS_TRX_THDR "
                    + " WHERE TKH_STRNO = '" + oneStore + "' "
                    + " AND TKH_REGNO = '" + oneRegister + "' "
                    + " AND TKH_DATE BETWEEN '" + START_DATE + "' AND CONVERT(datetime, '" + END_DATE + " 23:59:59.998') ");
            SLSLinDT = dbHlpr.FetchData("SELECT "
                            + " TKL_CUSNO, TKL_CUSNM, MAX(TKL_SREP) AS TKL_SREP, MAX(TKL_DATE) AS TKL_DATE, SUM(TKL_EXPRC) AS TKL_EXPRC, "
                            + " SUM(TKL_VATPRC) AS TKL_VATPRC, COUNT(TKL_TKTNO) AS TKL_TKTNO, MAX(TKL_TTYPE) AS TKL_TTYPE	"
                            + " FROM SLS_TRX_TLIN "
                            + " WHERE TKL_TTYPE <> 'Void' AND TKL_STRNO = '" + oneStore + "' AND TKL_REGNO = '" + oneRegister + "' "
                            + " AND TKL_DATE BETWEEN '" + START_DATE + "' AND CONVERT(datetime, '" + END_DATE + " 23:59:59.998') "
                            + " GROUP BY TKL_CUSNO, TKL_CUSNM ");
            if (SLSHdrDT.Rows.Count <= 0 || SLSLinDT.Rows.Count <= 0)
            {
                lblErrorMessage.Text = "No Record Found.";
                RptVwr.ReportSource = null;
                Session["Rpt"] = null;
            }
            else
            {
                lblErrorMessage.Text = "";

                ReportTempData(SLSHdrDT, 1);
                ReportTempData(SLSLinDT, 2);

                ReportGen();
            }
        }
        else if (CmBxReportType.Value == "A4")
        {
            string q = "SELECT "
                + " TKH_TKTNO, TKH_CUSNO, TKH_CUSNM, TKH_SREP, "
                + " TKH_DATE, TKH_TIME, TKH_AMT, TKH_PAYCD, TKH_CRTBY, "
                + " TKH_STRNO, TKH_REGNO, TKH_TTYPE AS LINE_TYPE, '' AS APLY2, '' AS STATS, "
                + " '0' AS INVCAMT, '' AS DISCFCT, '0' AS DISCVAL, "
                + " '' AS APLY2INVC, '' AS APLY2DAT, '0' AS APLY2AMT "
                + " FROM SLS_TRX_THDR "
                + " WHERE TKH_DATE BETWEEN '" + START_DATE + "' AND CONVERT(datetime, '" + END_DATE + " 23:59:59.998')  "
                + " AND TKH_STRNO IN (" + oneStore + ") "
                + " AND TKH_REGNO IN (" + oneRegister + ") "

                + " ORDER BY TKH_TKTNO ";
            DataTable dtRecords = dbHlpr.FetchData(q);

            if (dtRecords.Rows.Count <= 0)
            {
                lblErrorMessage.Text = "No Record Found.";
                RptVwr.ReportSource = null;
                Session["Rpt"] = null;
            }
            else
            {
                lblErrorMessage.Text = "";

                ReportA4TempData(dtRecords, oneStore, oneRegister);
                ReportGen();
            }
        }
    }


    private bool ReportA4TempData(DataTable dtDsr, string STRS, string REGS)
    {
        if (dtDsr.Rows.Count <= 0)
        {
            return false;
        }

        double cashSales = 0;
        double cashReturns = 0;
        double cashNetSales = 0;
        double creditSales = 0;
        double creditReturns = 0;
        double creditNetSales = 0;
        double totalSales = 0;
        double totalReturns = 0;
        double netSales = 0;

        for (int i = 0; i < dtDsr.Rows.Count; i++)
        {
            DataRow tmpRow = ReportsDataSet.TEMP_TABLE.NewRow();

            string strGrpName = "";
            switch (dtDsr.Rows[i]["LINE_TYPE"].ToString())
            {
                case "Sales":
                    strGrpName += "SALES";
                    if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("CASH"))
                    {
                        strGrpName += " - CASH";
                        cashSales += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                    }
                    else if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("A/R"))
                    {
                        strGrpName += " - CREDIT";
                        creditSales += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                    }
                    break;
                case "Return":
                    strGrpName += "SALES RETURN";
                    if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("CASH"))
                    {
                        strGrpName += " - CASH";
                        cashReturns += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                    }
                    else if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("A/R"))
                    {
                        strGrpName += " - CREDIT";
                        creditReturns += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                    }
                    break;
            }

            tmpRow["TEXT01"] = strGrpName;

            tmpRow["TEXT10"] = dtDsr.Rows[i]["TKH_TKTNO"].ToString();
            tmpRow["TEXT11"] = Convert.ToDateTime(dtDsr.Rows[i]["TKH_DATE"].ToString()).ToString("dd-MMM-yyyy");
            tmpRow["TEXT12"] = dtDsr.Rows[i]["TKH_CUSNO"].ToString();
            tmpRow["TEXT13"] = dtDsr.Rows[i]["TKH_CUSNM"].ToString();
            tmpRow["TEXT14"] = dtDsr.Rows[i]["APLY2"].ToString();
            tmpRow["TEXT15"] = dtDsr.Rows[i]["STATS"].ToString();
            tmpRow["TEXT17"] = dtDsr.Rows[i]["DISCVAL"].ToString() + (dtDsr.Rows[i]["DISCFCT"].ToString().Equals("P") ? "%" : "");
            tmpRow["TEXT19"] = dtDsr.Rows[i]["TKH_PAYCD"].ToString();

            if (dtDsr.Rows[i]["INVCAMT"].ToString().Trim().Length > 0)
            {
                tmpRow["NUM11"] = Convert.ToDouble(dtDsr.Rows[i]["INVCAMT"].ToString()).ToString("n2");
            }
            else
            {
                tmpRow["NUM11"] = Convert.ToDouble("0.00").ToString("n2");
            }

            tmpRow["NUM10"] = Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString()).ToString("n2");

            tmpRow["TEXT21"] = dtDsr.Rows[i]["APLY2INVC"].ToString();
            try
            {
                tmpRow["TEXT22"] = Convert.ToDateTime(dtDsr.Rows[i]["APLY2DAT"].ToString()).ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                tmpRow["TEXT22"] = "";
            }
            if (dtDsr.Rows[i]["APLY2AMT"].ToString().Trim().Length > 0)
            {
                tmpRow["NUM20"] = Convert.ToDouble(dtDsr.Rows[i]["APLY2AMT"].ToString()).ToString("n2");
            }
            else
            {
                tmpRow["NUM20"] = Convert.ToDouble("0.00").ToString("n2");
            }

            ReportsDataSet.TEMP_TABLE.Rows.Add(tmpRow);
        }

        DataRow prmRow = ReportsDataSet.PARAM_TABLE.NewRow();

        prmRow["param01"] = "Sales by Customer - Detail";
        prmRow["param02"] = START_DATE.Equals("1900-01-01") ? "Earliest" : Convert.ToDateTime(START_DATE).ToString("dd-MMM-yyyy");
        prmRow["param03"] = END_DATE.Equals("9999-12-31") ? "Latest" : Convert.ToDateTime(END_DATE).ToString("dd-MMM-yyyy");

        string Routes = string.Join(",", dtDsr.AsEnumerable().Select(r => r.Field<string>("TKH_SREP")).ToArray().Distinct(StringComparer.OrdinalIgnoreCase)).ToUpper();
        string VsUsers = string.Join(",", dtDsr.AsEnumerable().Select(r => r.Field<string>("TKH_CRTBY")).ToArray().Distinct(StringComparer.OrdinalIgnoreCase)).ToUpper();
        prmRow["param04"] = Routes.IndexOf(',') >= 0 ? "Multiple" : Routes;
        prmRow["param05"] = VsUsers.IndexOf(',') >= 0 ? VsUsers : VsUsers;
        prmRow["param06"] = STRS.IndexOf(',') >= 0 ? "Multiple" : dtDsr.Rows[0]["TKH_STRNO"].ToString();
        prmRow["param07"] = REGS.IndexOf(',') >= 0 ? "Multiple" : dtDsr.Rows[0]["TKH_REGNO"].ToString();
        prmRow["param08"] = Session["UserId"].ToString().ToUpper();
        prmRow["param09"] = dbHlpr.GetRptSeqNo();

        prmRow["param10"] = txtComments.Text.Trim();

        prmRow["param12"] = STRS.IndexOf(',') >= 0 ? "Multiple" : dtDsr.Rows[0]["TKH_STRNO"].ToString();

        prmRow["param18"] = CmBxRecapBy.Value;

        cashNetSales = cashSales + (cashReturns);
        creditNetSales = creditSales + (creditReturns);
        totalSales = cashSales + creditSales;
        totalReturns = cashReturns + (creditReturns);

        netSales = cashSales + creditSales + (cashReturns) + (creditReturns);
        prmRow["param21"] = cashSales.ToString("n2");
        prmRow["param22"] = creditSales.ToString("n2");
        prmRow["param23"] = cashReturns.ToString("n2");
        prmRow["param24"] = creditReturns.ToString("n2");
        prmRow["param25"] = netSales.ToString("n2");
        prmRow["param26"] = 0.00.ToString("n2");
        prmRow["param27"] = 0.00.ToString("n2");
        prmRow["param28"] = cashNetSales.ToString("n2");
        prmRow["param29"] = creditNetSales.ToString("n2");
        prmRow["param30"] = totalSales.ToString("n2");
        prmRow["param31"] = totalReturns.ToString("n2");

        prmRow["param32"] = 0.00.ToString("n2");
        prmRow["param33"] = 0.00.ToString("n2");
        prmRow["param34"] = 0.00.ToString("n2");

        ReportsDataSet.PARAM_TABLE.Rows.Add(prmRow);

        return true;
    }

    public void ReportTempData(DataTable DTTemptbl, int A)
    {
        if (A == 2)
        {
            for (int i = 0; i < DTTemptbl.Rows.Count; i++)
            {
                DataRow dr = ReportsDataSet.TEMP_TABLE.NewRow();   //Tables["TEMP_TABLE"]. 

                if (DTTemptbl.Rows[i]["TKL_CUSNO"] != null && DTTemptbl.Rows[i]["TKL_CUSNO"] != "")
                {
                    dr["Text05"] = DTTemptbl.Rows[i]["TKL_CUSNO"].ToString(); // Catg No
                }
                else
                {
                    dr["Text05"] = "";
                }
                if (DTTemptbl.Rows[i]["TKL_CUSNM"] != null && DTTemptbl.Rows[i]["TKL_CUSNM"] != "")
                {
                    dr["Text06"] = DTTemptbl.Rows[i]["TKL_CUSNM"].ToString(); // cust name
                }
                else
                {
                    dr["Text06"] = "";
                }


                if (DTTemptbl.Rows[i]["TKL_SREP"] != null && DTTemptbl.Rows[i]["TKL_SREP"] != "")
                {
                    dr["Text07"] = DTTemptbl.Rows[i]["TKL_SREP"].ToString().ToUpper(); // SLS rep
                }
                else
                {
                    dr["Text07"] = "";
                }
                if (DTTemptbl.Rows[i]["TKL_DATE"] != null && DTTemptbl.Rows[i]["TKL_DATE"] != "")
                {
                    dr["Text08"] = DTTemptbl.Rows[i]["TKL_DATE"].ToString(); // Date
                }
                else
                {
                    dr["Text08"] = "";
                }
                if (DTTemptbl.Rows[i]["TKL_TTYPE"] != null && DTTemptbl.Rows[i]["TKL_TTYPE"] != "")
                {
                    dr["Text10"] = DTTemptbl.Rows[i]["TKL_TTYPE"].ToString(); // TRX Type
                }
                else
                {
                    dr["Text10"] = "";
                }

                if (DTTemptbl.Rows[i]["TKL_EXPRC"] != null && DTTemptbl.Rows[i]["TKL_EXPRC"] != "")
                {
                    dr["Num03"] = DTTemptbl.Rows[i]["TKL_EXPRC"].ToString();// Ex Price
                }
                else
                {
                    dr["Num03"] = "0.00";
                }
                if (DTTemptbl.Rows[i]["TKL_VATPRC"] != null && DTTemptbl.Rows[i]["TKL_VATPRC"] != "")
                {
                    dr["Num04"] = DTTemptbl.Rows[i]["TKL_VATPRC"].ToString();// VAT Price
                }
                else
                {
                    dr["Num04"] = "0.00";
                }

                if (DTTemptbl.Rows[i]["TKL_TKTNO"] != null && DTTemptbl.Rows[i]["TKL_TKTNO"] != "")
                {
                    dr["Num05"] = DTTemptbl.Rows[i]["TKL_TKTNO"].ToString();// VAT Price
                }
                else
                {
                    dr["Num05"] = "0.00";
                }
                ReportsDataSet.TEMP_TABLE.Rows.Add(dr);
            }
        }
        else
        {
            /////////////////////////////////////////////////////
            ////////// Header Param table start here ////////////
            /////////////////////////////////////////////////////
            DataRow dr = ReportsDataSet.PARAM_TABLE.NewRow();   //Tables["TEMP_TABLE"]. 
            //Company Details starts

            dr["param11"] = "Forsan Foods & Consumer Products Co. Ltd";
            dr["param12"] = "الغذائىة الفرسان - مبيعات فان";
            dr["param13"] = "VAT # 300056053500003 ";
            dr["param14"] = "Pri. Sultan Abdul Aziz St ,Riyadh";
            dr["param15"] = "Tel # (011) 416-4422";
            dr["param16"] = "Mobile # 055-816-1122";

            if (DTTemptbl.Rows[0]["TKH_TKTNO"] != null && DTTemptbl.Rows[0]["TKH_TKTNO"] != "")
            {
                dr["param01"] = DTTemptbl.Rows[0]["TKH_TKTNO"].ToString(); // Ticket Code
            }
            else
            {
                dr["param01"] = "";
            }
            if (DTTemptbl.Rows[0]["TKH_SREP"] != null && DTTemptbl.Rows[0]["TKH_SREP"] != "")
            {
                dr["param05"] = DTTemptbl.Rows[0]["TKH_SREP"].ToString().ToUpper(); // Sales Rep
            }
            else
            {
                dr["param05"] = "";
            }
            if (DTTemptbl.Rows[0]["TKH_DATE"] != null && DTTemptbl.Rows[0]["TKH_DATE"] != "")
            {
                DateTime DTtmp = Convert.ToDateTime(DTTemptbl.Rows[0]["TKH_DATE"].ToString());
                dr["param06"] = DTtmp.ToString("dd-MMM-yyyy, HH:mm"); // Date                   
            }
            else
            {
                dr["param06"] = "";
            }
            if (DTTemptbl.Rows[0]["TKH_TIME"] != null && DTTemptbl.Rows[0]["TKH_TIME"] != "")
            {
                dr["param07"] = DTTemptbl.Rows[0]["TKH_TIME"].ToString(); // Time
            }
            else
            {
                dr["param07"] = "";
            }
            if (DTTemptbl.Rows[0]["TKH_SLAMT"] != null && DTTemptbl.Rows[0]["TKH_SLAMT"] != "")
            {
                dr["param31"] = DTTemptbl.Rows[0]["TKH_SLAMT"].ToString(); // sales amount
            }
            else
            {
                dr["param31"] = "";
            }

            if (DTTemptbl.Rows[0]["TKH_DSCNT"] != null && DTTemptbl.Rows[0]["TKH_DSCNT"] != "")
            {
                dr["param32"] = DTTemptbl.Rows[0]["TKH_DSCNT"].ToString(); //Discount
            }
            else
            {
                dr["param32"] = "";
            }

            if (DTTemptbl.Rows[0]["TKH_NTAMT"] != null && DTTemptbl.Rows[0]["TKH_NTAMT"] != "")
            {
                dr["param33"] = DTTemptbl.Rows[0]["TKH_NTAMT"].ToString();// net amount
            }
            else
            {
                dr["param33"] = "0.00";
            }


            if (DTTemptbl.Rows[0]["TKH_VTAMT"] != null && DTTemptbl.Rows[0]["TKH_VTAMT"] != "")
            {
                dr["param34"] = DTTemptbl.Rows[0]["TKH_VTAMT"].ToString(); // VAT Amount
            }
            else
            {
                dr["param34"] = "";
            }

            if (DTTemptbl.Rows[0]["TKH_AMT"] != null && DTTemptbl.Rows[0]["TKH_AMT"] != "")
            {
                dr["param35"] = DTTemptbl.Rows[0]["TKH_AMT"].ToString();// Total Amount
            }
            else
            {
                dr["param35"] = "0.00";
            }
            if (DTTemptbl.Rows[0]["TKH_AMTPD"] != null && DTTemptbl.Rows[0]["TKH_AMTPD"] != "")
            {
                dr["param36"] = DTTemptbl.Rows[0]["TKH_AMTPD"].ToString();// Amount Paid
            }
            else
            {
                dr["param36"] = "0.00";
            }
            if (DTTemptbl.Rows[0]["TKH_CHNGE"] != null && DTTemptbl.Rows[0]["TKH_CHNGE"] != "")
            {
                dr["param37"] = DTTemptbl.Rows[0]["TKH_CHNGE"].ToString();// Change Due
            }
            else
            {
                dr["param37"] = "0.00";
            }//TKH_BLANC
            if (DTTemptbl.Rows[0]["TKH_BLANC"] != null && DTTemptbl.Rows[0]["TKH_BLANC"] != "")
            {
                dr["param38"] = DTTemptbl.Rows[0]["TKH_BLANC"].ToString();// Balance Due
            }
            else
            {
                dr["param38"] = "0.00";
            }//TKH_BLANC
            //if (DTTemptbl.Rows[0]["TKL_TXFCT"] != null && DTTemptbl.Rows[0]["TKL_TXFCT"] != "")
            //{
            //    dr["param39"] = DTTemptbl.Rows[0]["TKL_TXFCT"].ToString();// Balance Due
            //}
            //else
            //{
            dr["param39"] = "5.00";
            //}//TKH_Tax Factor

            if (DTTemptbl.Rows[0]["TKH_TTYPE"] != null && DTTemptbl.Rows[0]["TKH_TTYPE"] != "")
            {
                dr["param08"] = DTTemptbl.Rows[0]["TKH_TTYPE"].ToString();// TYPE
            }
            else
            {
                dr["param08"] = "";
            }
            dr["param20"] = DTTemptbl.Rows.Count;
            ReportsDataSet.PARAM_TABLE.Rows.Add(dr);

        }

    }//end method

    public void ReportGen()
    {
        try
        {
            // One Way
            ReportDocument RptDoc = new ReportDocument();
            if (CmBxReportType.Value == "RCPT")
            {
                RptDoc.Load(Server.MapPath("~/RptFiles/AppReports/RPT_SLS_BYCUS.rpt"));
            }
            else if (CmBxReportType.Value == "A4")
            {
                RptDoc.Load(Server.MapPath("~/RptFiles/AppReports/RPT_SLS_BYCUS_A4.rpt"));
            }
            RptDoc.SetDataSource(ReportsDataSet);
            RptVwr.ReportSource = RptDoc;
            Session["Rpt"] = RptDoc;
            //CrystalDecisions.CrystalReports.Engine.TextObject TXTCOMNTS_BOX = RptDoc.ReportDefinition.ReportObjects["TXT_CMMNT"] as CrystalDecisions.CrystalReports.Engine.TextObject;

            //TXTCOMNTS_BOX.ObjectFormat.EnableSuppress = txtComments.Text.Trim().Length <= 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void StrNo_Init(object sender, EventArgs e)
    {
        DataTable dtStores = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE WHERE STM_CODE IN (" + Session["userBranchCode"].ToString() + ") ");

        StrNo.DataSource = dtStores;
        StrNo.ValueField = "STM_CODE";
        StrNo.TextField = "STM_NAME";
        StrNo.DataBind();
    }
    protected void RegNo_Init(object sender, EventArgs e)
    {
        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + Session["userBranchCode"].ToString() + ") ");

        RegNo.DataSource = dtRegisters;
        RegNo.ValueField = "REG_CODE";
        RegNo.TextField = "REG_NAME";
        RegNo.DataBind();
    }

    protected void DsrStrNo_ValueChanged(object sender, EventArgs e)
    {
        ASPxComboBox value = (ASPxComboBox)sender;
        string strNo = value.Value == null ? "" : value.Value.ToString();
        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + strNo + ") ");

        RegNo.DataSource = dtRegisters;
        RegNo.ValueField = "REG_CODE";
        RegNo.TextField = "REG_NAME";
        RegNo.DataBind();
    }
}