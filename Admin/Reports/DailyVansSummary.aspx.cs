﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using System.Drawing;
using DevExpress.XtraPrinting;
using DevExpress.Export.Xl;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class Admin_Reports_DailyVansSummary : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
    ReportsDS ReportsDataSet = new ReportsDS();
    string START_DATE = "Earliest";
    string END_DATE = "Latest";

    bool HistoryReport = false;
    string TKTS_HDR_TABLE = "SLS_TRX_THDR";
    string TKTS_LIN_TABLE = "SLS_TRX_TLIN";
    string RCPTS_TABLE = "CUS_PAY_RCPTS";
    string RCPTS_APLY2_TABLE = "CUS_PAY_APLY2";

    protected void Page_Init(object sender, EventArgs e)
    {
        //DsrDateFrom.Value = new DateTime(1900, 1, 1);
        //DsrDateTo.Value = new DateTime(9999, 12, 31);

        if (Session["Rpt"] != null && IsPostBack)
        {
            RptVwr.ReportSource = Session["Rpt"];
        }

        if (Request.QueryString["Trx"] != null && Request.QueryString["Trx"].ToString().ToLower() == "history")
        {
            this.HistoryReport = true;
            this.TKTS_HDR_TABLE = "SLS_TKH_HIST";
            this.TKTS_LIN_TABLE = "SLS_TKL_HIST";
            this.RCPTS_TABLE = "CUS_HST_PYMNT";
            this.RCPTS_APLY2_TABLE = "CUS_HST_APLY2";
        }
    }

    protected void btnExportDailyVansSummary_Click(object sender, EventArgs e)
    {
        START_DATE = DsrDateFrom.Value != null ? ((DateTime)DsrDateFrom.Value).ToString("yyyy-MM-dd") : new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");
        END_DATE = DsrDateTo.Value != null ? ((DateTime)DsrDateTo.Value).ToString("yyyy-MM-dd") : new DateTime(9999, 12, 31).ToString("yyyy-MM-dd");

        string oneStore = DsrStrNo.Value.ToString();

        DataTable dtExport = new DataTable();
        string qry = "SELECT TKH_STRNO, TKH_SREP AS VAN, LINE_TYPE AS TYP, TKH_PAYCD AS PAYCD, SUM(TKH_AMT) AS AMT FROM ("
            + " SELECT "
            + " TKH_TKTNO, TKH_CUSNO, TKH_CUSNM, TKH_SREP, "
            + " TKH_DATE, TKH_TIME, TKH_AMT, TKH_PAYCD, TKH_CRTBY, "
            + " TKH_STRNO, TKH_REGNO, TKH_TTYPE AS LINE_TYPE, '' AS APLY2, '' AS STATS, "
            + " '0' AS INVCAMT, '' AS DISCFCT, '0' AS DISCVAL "
            + " FROM " + this.TKTS_HDR_TABLE + " "
            + " WHERE TKH_DATE BETWEEN '" + START_DATE + "' AND CONVERT(datetime, '" + END_DATE + " 23:59:59.998')  ";
        if (oneStore != "All")
        {
            qry += " AND TKH_STRNO IN (" + oneStore + ") ";
        }

        qry += " UNION ALL "
            + " SELECT "
            + " PR_NO, PR_CUSNO, PR_CUSNM, PR_SREP, "
            + " PR_RCPDT, PR_RCPTM, PR_AMTPD, PR_PAYCD, PR_CRTBY, "
            + " PR_STRNO, PR_REGNO, 'RCPT' AS LINE_TYPE, PR_APLY2 AS APLY2, PR_STATS AS STATS, "
            + " PR_TOTAL AS INVCAMT, PR_DSTYP AS DISCFCT, PR_DSFCT AS DISCVAL "
            + " FROM " + this.RCPTS_TABLE + " "
            + " WHERE PR_RCPDT BETWEEN '" + START_DATE + "' AND '" + END_DATE + "' ";
        if (oneStore != "All")
        {
            qry += " AND PR_STRNO IN (" + oneStore + ") ";
        }

        qry += " ) tbl "
            + " GROUP BY TKH_STRNO, TKH_SREP, LINE_TYPE, TKH_PAYCD "
            + " ORDER BY TKH_SREP ";

        dtExport = dbHlpr.FetchData(qry);

        if (PrepareTempData(dtExport, oneStore))
        {
            lblErrorMessage.Text = "";
            ReportGen();
        }
        else
        {
            lblErrorMessage.Text = "No Record Found.";
            RptVwr.ReportSource = null;
            Session["Rpt"] = null;
        }
    }

    private bool PrepareTempData(DataTable dtDsr, string STRS)
    {
        if (dtDsr.Rows.Count <= 0)
        {
            return false;
        }

        // VAN, TYP, PAYCD, AMT

        //DataRow[] vans = dtDsr.Select("DISTINCT VAN");
        List<object> vans = (from r in dtDsr.AsEnumerable()
                             select r["VAN"]).Distinct().ToList();
        for (int i = 0; i < vans.Count; i++)
        {
            double cashSales = 0;
            double cashReturns = 0;
            double cashNetSales = 0;
            double creditSales = 0;
            double creditReturns = 0;
            double creditNetSales = 0;
            double totalSales = 0;
            double totalReturns = 0;
            double netSales = 0;
            double cshCollections = 0;
            double chqCollections = 0;
            double bnkCollections = 0;
            double netCollections = 0;
            double totalCash = 0;

            DataRow tmpRow = ReportsDataSet.TEMP_TABLE.NewRow();

            string VanNo = vans[i].ToString();

            tmpRow["TEXT01"] = VanNo;
            tmpRow["TEXT02"] = "";
            tmpRow["TEXT03"] = "";

            DataRow[] dtRws = dtDsr.Select("VAN='" + VanNo + "'");
            if (dtRws.Length > 0)
            {
                tmpRow["TEXT02"] = dtRws[0]["TKH_STRNO"].ToString();
                tmpRow["TEXT03"] = GetStoreName(dtRws[0]["TKH_STRNO"].ToString());
            }

            cashSales = Convert.ToDouble(ComputeMyDataTable(dtDsr, "SUM(AMT)", "VAN = '" + VanNo + "' AND TYP = 'Sales' AND PAYCD = 'CASH'"));
            cashReturns = Convert.ToDouble(ComputeMyDataTable(dtDsr, "SUM(AMT)", "VAN = '" + VanNo + "' AND TYP = 'Return' AND PAYCD = 'CASH'"));

            creditSales = Convert.ToDouble(ComputeMyDataTable(dtDsr, "SUM(AMT)", "VAN = '" + VanNo + "' AND TYP = 'Sales' AND PAYCD = 'A/R'"));
            creditReturns = Convert.ToDouble(ComputeMyDataTable(dtDsr, "SUM(AMT)", "VAN = '" + VanNo + "' AND TYP = 'Return' AND PAYCD = 'A/R'"));

            cshCollections = Convert.ToDouble(ComputeMyDataTable(dtDsr, "SUM(AMT)", "VAN = '" + VanNo + "' AND TYP = 'RCPT' AND PAYCD = 'CASH'"));
            chqCollections = Convert.ToDouble(ComputeMyDataTable(dtDsr, "SUM(AMT)", "VAN = '" + VanNo + "' AND TYP = 'RCPT' AND PAYCD = 'CHQ'"));
            bnkCollections = Convert.ToDouble(ComputeMyDataTable(dtDsr, "SUM(AMT)", "VAN = '" + VanNo + "' AND TYP = 'RCPT' AND PAYCD = 'BANK'"));

            cashNetSales = cashSales + (cashReturns);
            creditNetSales = creditSales + (creditReturns);
            totalSales = cashSales + creditSales;
            totalReturns = cashReturns + (creditReturns);

            netSales = cashSales + creditSales + (cashReturns) + (creditReturns);

            netCollections = cshCollections + chqCollections + bnkCollections;

            totalCash = cashSales + (cashReturns) + cshCollections;

            tmpRow["NUM01"] = cashSales.ToString("n2");
            tmpRow["NUM02"] = creditSales.ToString("n2");
            tmpRow["NUM03"] = cashReturns.ToString("n2");
            tmpRow["NUM04"] = creditReturns.ToString("n2");
            tmpRow["NUM05"] = netSales.ToString("n2");
            tmpRow["NUM06"] = cshCollections.ToString("n2");
            tmpRow["NUM07"] = totalCash.ToString("n2");
            tmpRow["NUM08"] = cashNetSales.ToString("n2");
            tmpRow["NUM09"] = creditNetSales.ToString("n2");
            tmpRow["NUM10"] = totalSales.ToString("n2");
            tmpRow["NUM11"] = totalReturns.ToString("n2");

            tmpRow["NUM12"] = chqCollections.ToString("n2");
            tmpRow["NUM13"] = bnkCollections.ToString("n2");
            tmpRow["NUM14"] = netCollections.ToString("n2");

            ReportsDataSet.TEMP_TABLE.Rows.Add(tmpRow);
        }

        DataRow prmRow = ReportsDataSet.PARAM_TABLE.NewRow();

        prmRow["param01"] = "Daily Vansales Summary";
        prmRow["param02"] = START_DATE.Equals("1900-01-01") ? "Earliest" : Convert.ToDateTime(START_DATE).ToString("dd-MMM-yyyy");
        prmRow["param03"] = END_DATE.Equals("9999-12-31") ? "Latest" : Convert.ToDateTime(END_DATE).ToString("dd-MMM-yyyy");

        string Routes = string.Join(",", dtDsr.AsEnumerable().Select(r => r.Field<string>("VAN")).ToArray().Distinct(StringComparer.OrdinalIgnoreCase)).ToUpper();
        prmRow["param06"] = DsrStrNo.Value != null ? DsrStrNo.Value.ToString() : "";
        prmRow["param08"] = Session["UserId"].ToString().ToUpper();
        prmRow["param09"] = dbHlpr.GetRptSeqNo();

        prmRow["param10"] = txtComments.Text.Trim();

        prmRow["param12"] = STRS.IndexOf(',') >= 0 ? "Multiple" : GetStoreName(STRS);

        ReportsDataSet.PARAM_TABLE.Rows.Add(prmRow);

        return true;
    }

    public void ReportGen()
    {
        try
        {
            // One Way
            ReportDocument RptDoc = new ReportDocument();
            RptDoc.Load(Server.MapPath("~/RptFiles/CR_VAN_SUMRY.rpt"));
            RptDoc.SetDataSource(ReportsDataSet);
            RptVwr.ReportSource = RptDoc;
            Session["Rpt"] = RptDoc;
            CrystalDecisions.CrystalReports.Engine.TextObject TXTCOMNTS_BOX = RptDoc.ReportDefinition.ReportObjects["TXT_CMMNT"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            CrystalDecisions.CrystalReports.Engine.TextObject TXT_GRP_BRANCH = RptDoc.ReportDefinition.ReportObjects["TXT_GRP_BRANCH"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            if (TXT_GRP_BRANCH != null)
            {
                if (DsrStrNo.Value != null && DsrStrNo.Value.ToString() != "All")
                {
                    TXT_GRP_BRANCH.ObjectFormat.EnableSuppress = true;
                }
            }

            CrystalDecisions.CrystalReports.Engine.Section SEC_BRANCH_FOOTER = RptDoc.ReportDefinition.Sections["SEC_BRANCH_FOOTER"] as CrystalDecisions.CrystalReports.Engine.Section;
            CrystalDecisions.CrystalReports.Engine.Section SEC_REPORT_FOOTER = RptDoc.ReportDefinition.Sections["SEC_REPORT_FOOTER"] as CrystalDecisions.CrystalReports.Engine.Section;
            
                if (SEC_BRANCH_FOOTER != null)
                {
                    if (this.HistoryReport)
                    {
                        SEC_BRANCH_FOOTER.SectionFormat.EnableSuppress = true;
                        SEC_REPORT_FOOTER.SectionFormat.EnableSuppress = true;
                    }
                    else if (DsrStrNo.Value != null && DsrStrNo.Value.ToString() != "All")
                    {
                        SEC_BRANCH_FOOTER.SectionFormat.EnableSuppress = false;
                        SEC_REPORT_FOOTER.SectionFormat.EnableSuppress = true;
                    }
                    else
                    {
                        SEC_BRANCH_FOOTER.SectionFormat.EnableSuppress = true;
                        SEC_REPORT_FOOTER.SectionFormat.EnableSuppress = false;
                    }
                }
            
            TXTCOMNTS_BOX.ObjectFormat.EnableSuppress = txtComments.Text.Trim().Length <= 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void DsrStrNo_Init(object sender, EventArgs e)
    {
        DataTable dtStores = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE WHERE STM_CODE IN (" + Session["userBranchCode"].ToString() + ") ");

        DataRow dr = dtStores.NewRow();
        dr["STM_CODE"] = "All";
        dr["STM_NAME"] = "All";
        dtStores.Rows.InsertAt(dr, 0);

        DsrStrNo.DataSource = dtStores;
        DsrStrNo.ValueField = "STM_CODE";
        DsrStrNo.TextField = "STM_NAME";
        DsrStrNo.DataBind();
    }

    private string GetStoreName(string StrNo)
    {
        if (StrNo == "All")
        {
            return "All Branches";
        }

        DataTable dtStores = dbHlpr.FetchData("SELECT STM_NAME FROM STP_MSTR_STORE WHERE STM_CODE = '" + StrNo + "' ");
        if (dtStores.Rows.Count > 0)
        {
            return dtStores.Rows[0]["STM_NAME"].ToString();
        }
        return "";
    }

    private string ComputeMyDataTable(DataTable dt, string computeExpr, string filterExpr)
    {
        var result = dt.Compute(computeExpr, filterExpr);
        if (result != DBNull.Value)
        {
            return result.ToString();
        }

        return "0.00";
    }
}