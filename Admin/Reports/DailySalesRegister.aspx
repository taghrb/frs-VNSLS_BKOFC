﻿<%@ Page Title="Daily Sales Register" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="DailySalesRegister.aspx.cs" Inherits="Admin_Reports_DailySalesRegister" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>


<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <script type="text/javascript">

        function grid_customizationWindowCloseUp(s, e) {
            gvUOMClientInquery.ShowCustomizationWindow();
        }

    </script>
    <style type="text/css">
        .fieldsetWrapper {
            padding: 10px;
            width: 70%;
            margin: 0 auto;
            background-color: #F7F5F1;
            margin-top: 5px;
            border-radius: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="contentTopHere1" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
            <li>&nbsp;
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">

    <div class="formDiv" style="background-color: transparent;">
        <div class="formHeaderDiv">
            <h3>Daily Sales Register</h3>
        </div>
        <div class="fieldsetWrapper">
            <fieldset>
                <legend></legend>
                <table style="margin: 0 auto;">
                    <tr>
                        <td>Report Type : </td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxComboBox ID="DsrReportType" ClientInstanceName="DsrClientReportType" runat="server" AnimationType="Auto">
                                    <Items>
                                        <dx:ListEditItem Text="Flash" Value="Flash" />
                                        <dx:ListEditItem Text="Brief" Value="Brief" Selected="true" />
                                        <dx:ListEditItem Text="Detail" Value="Detail" />
                                    </Items>
                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="true" ErrorText="Report Type is Required." />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Store # :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxComboBox ID="DsrStrNo" OnInit="DsrStrNo_Init" ClientInstanceName="DsrClientStrNo" runat="server" AnimationType="Auto" OnValueChanged="DsrStrNo_ValueChanged" AutoPostBack="true">
                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="true" ErrorText="Store No. is Required." />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                            </div>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>Register # :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxComboBox ID="DsrRegNo" OnInit="DsrRegNo_Init" ClientInstanceName="DsrClientRegNo" runat="server" AnimationType="Auto">
                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="true" ErrorText="Register No. is Required." />
                                    </ValidationSettings>
                                </dx:ASPxComboBox>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Date From :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxDateEdit runat="server" ID="DsrDateFrom" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy" NullText="Earliest" NullTextStyle-ForeColor="Black"></dx:ASPxDateEdit>
                            </div>
                        </td>
                        <td></td>

                        <td>Date To :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxDateEdit runat="server" ID="DsrDateTo" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy" NullText="Latest" NullTextStyle-ForeColor="Black"></dx:ASPxDateEdit>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Comments :&nbsp;&nbsp;&nbsp;</td>
                        <td colspan="4">
                            <div class="DivReq">
                                <dx:ASPxTextBox Width="100%" runat="server" ID="txtComments"></dx:ASPxTextBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </fieldset>
            <hr style="color: skyblue" />
            <dx:ASPxLabel ID="lblErrorMessage" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
            <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                <dx:ASPxButton ID="btnExportDailySalesRegister" runat="server" Text="Load Report" CssClass="btnPopup btn btn-success" CausesValidation="true" OnClick="btnExportDailySalesRegister_Click" />
                <dx:ASPxButton ID="btnPopUpClear" runat="server" Text="Clear" CssClass="btnPopup btn btn-warning" CausesValidation="false" AutoPostBack="false">
                    <ClientSideEvents Click="function(s, e) { CallBackCode.PerformCallback(CatClientEditCode.GetText()); }" />
                </dx:ASPxButton>
            </div>

            <CR:CrystalReportViewer ViewStateMode="Enabled" HasCrystalLogo="False" ToolPanelView="None" Style="margin: 0 auto;" ID="RptVwr" runat="server" AutoDataBind="false" />
        </div>
    </div>
    <script type="text/javascript">
        window.onload = function () {
            $('form').on('click', '.btnPopupClose', function () {
                $('.overlay').hide();
            });
        };

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //if (prm != null) {
        prm.add_endRequest(function (sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.overlay').show();
            }
        });
        prm.add_endRequest(GridOnEndCallBack);
        //};

    </script>
</asp:Content>

