﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Inventory_TicketGps : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DxGridSqlDataSource1.SelectCommand = "SELECT " 
                + " '' AS TKH_STRNO, '' AS TKH_SREP, "
                + " '0.00' AS Total, '0.00' AS Captured, '0.00' AS Missing, '' AS 'GPS Status', '0.00' AS '%age', '0.00' AS 'Loss %age' " 
                + " WHERE 1 = 0 ";
        }
        else
        {
            try
            {
                string DateSelected = Convert.ToDateTime(ASPxDateEdit1.Value).ToString("yyyy-MM-dd");
                if (DateSelected == "0001-01-01")
                {
                    if (Session.Contents["dateSelected"] != null)
                    {
                        DateSelected = Session.Contents["dateSelected"].ToString();
                    }
                    else
                    {
                        DateSelected = "1900-01-01";
                    }
                }
                string qry = "";
                qry += " SELECT ";
                qry += " TKH_STRNO, TKH_SREP, SUM(Total) AS Total, SUM(Captured) AS Captured, SUM(Missing) AS Missing, ";
                qry += " CASE ";
                qry += " WHEN SUM(Total) = 0 THEN 'N/A' ";
                qry += " WHEN SUM(Captured) = 0 AND SUM(Total) > 0 THEN 'Missing' ";

                qry += " WHEN SUM(Captured) > 0 AND SUM(Captured) < SUM(Total) THEN 'Partially Available' ";
                qry += " WHEN SUM(Captured) > 0 AND SUM(Captured) = SUM(Total) THEN 'Available' ";
                qry += " END AS 'GPS Status', ";
                qry += " (CAST(SUM(Captured) AS FLOAT) / CAST(SUM(Total) AS FLOAT)) * 100 AS '%age', ";
                qry += " (CAST(SUM(Missing) AS FLOAT) / CAST(SUM(Total) AS FLOAT)) * 100 AS 'Loss %age' ";
                qry += " FROM ( ";
                qry += " SELECT TKH_STRNO, TKH_SREP, COUNT(TKH_TKTNO) AS Total, 0 AS Captured, 0 AS Missing ";
                qry += " FROM SLS_TRX_THDR ";
                qry += " WHERE CONVERT(DATE, TKH_DATE) = '" + DateSelected + "' ";
                qry += " GROUP BY TKH_STRNO, TKH_SREP ";

                qry += " UNION ALL ";
                
                qry += " SELECT TKH_STRNO, TKH_SREP, 0, COUNT(TKH_TKTNO), 0 ";
                qry += " FROM SLS_TRX_THDR ";
                qry += " WHERE CONVERT(DATE, TKH_DATE) = '" + DateSelected + "' AND TKH_GEOLA <> '0.0' AND TKH_GEOLA <> 'TIMEOUT' ";
                qry += " GROUP BY TKH_STRNO, TKH_SREP ";
                
                qry += " UNION ALL ";
                
                qry += " SELECT TKH_STRNO, TKH_SREP, 0, 0, COUNT(TKH_TKTNO) ";
                qry += " FROM SLS_TRX_THDR ";
                qry += " WHERE CONVERT(DATE, TKH_DATE) = '" + DateSelected + "' AND (TKH_GEOLA = '0.0' OR TKH_GEOLA = 'TIMEOUT') ";
                qry += " GROUP BY TKH_STRNO, TKH_SREP ";
                qry += " ) tbl ";
                qry += " GROUP BY tbl.TKH_STRNO, tbl.TKH_SREP ";
                qry += " ORDER BY TKH_STRNO, TKH_SREP ";
                DxGridSqlDataSource1.SelectCommand = qry;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        //gvInquery.SettingsPager.PageSize = Convert.ToInt32(ddlNoOfRecords.SelectedItem.Value);
        //gvInquery.DataBind();
    }

    protected void ACB_CodeCheck_Callback(object source, CallbackEventArgs e)
    {

    }

    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {
    }

    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            int RcptNo;
            if (int.TryParse(e.CommandArgs.CommandArgument.ToString(), out RcptNo)) // Code is sent via request
            {
                
                gvInquery.CancelEdit();
                gvInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid Order ID. Unable to delete Order.";
            }
        }
        else if (e.CommandArgs.CommandName.Equals("VoidReceipt"))
        {
            Int64 ReceiptNo;
            if (Int64.TryParse(e.CommandArgs.CommandArgument.ToString(), out ReceiptNo)) // Code is sent via request
            {
                
                gvInquery.CancelEdit();
                gvInquery.DataBind();
            }
        }
    }

    // Common Methods

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }

    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void gvInquery_CustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        //  if(e.Column.FieldName == "")
    }

    protected void ASPxDateEdit1_DateChanged(object sender, EventArgs e)
    {
        try
        {
            string DateSelected = Convert.ToDateTime(ASPxDateEdit1.Value).ToString("yyyy-MM-dd");
            string qry = "";
            qry += " SELECT ";
            qry += " TKH_STRNO, TKH_SREP, SUM(Total) AS Total, SUM(Captured) AS Captured, SUM(Missing) AS Missing, ";
            qry += " CASE ";
            qry += " WHEN SUM(Total) = 0 THEN 'N/A' ";
            qry += " WHEN SUM(Captured) = 0 AND SUM(Total) > 0 THEN 'Missing' ";

            qry += " WHEN SUM(Captured) > 0 AND SUM(Captured) < SUM(Total) THEN 'Partially Available' ";
            qry += " WHEN SUM(Captured) > 0 AND SUM(Captured) = SUM(Total) THEN 'Available' ";
            qry += " END AS 'GPS Status', ";
            qry += " (CAST(SUM(Captured) AS FLOAT) / CAST(SUM(Total) AS FLOAT)) * 100 AS '%age', ";
            qry += " (CAST(SUM(Missing) AS FLOAT) / CAST(SUM(Total) AS FLOAT)) * 100 AS 'Loss %age' ";
            qry += " FROM ( ";
            qry += " SELECT TKH_STRNO, TKH_SREP, COUNT(TKH_TKTNO) AS Total, 0 AS Captured, 0 AS Missing ";
            qry += " FROM SLS_TRX_THDR ";
            qry += " WHERE CONVERT(DATE, TKH_DATE) = '" + DateSelected + "' ";
            qry += " GROUP BY TKH_STRNO, TKH_SREP ";

            qry += " UNION ALL ";

            qry += " SELECT TKH_STRNO, TKH_SREP, 0, COUNT(TKH_TKTNO), 0 ";
            qry += " FROM SLS_TRX_THDR ";
            qry += " WHERE CONVERT(DATE, TKH_DATE) = '" + DateSelected + "' AND TKH_GEOLA <> '0.0' AND TKH_GEOLA <> 'TIMEOUT' ";
            qry += " GROUP BY TKH_STRNO, TKH_SREP ";

            qry += " UNION ALL ";

            qry += " SELECT TKH_STRNO, TKH_SREP, 0, 0, COUNT(TKH_TKTNO) ";
            qry += " FROM SLS_TRX_THDR ";
            qry += " WHERE CONVERT(DATE, TKH_DATE) = '" + DateSelected + "' AND (TKH_GEOLA = '0.0' OR TKH_GEOLA = 'TIMEOUT') ";
            qry += " GROUP BY TKH_STRNO, TKH_SREP ";
            qry += " ) tbl ";
            qry += " GROUP BY tbl.TKH_STRNO, tbl.TKH_SREP ";
            qry += " ORDER BY TKH_STRNO, TKH_SREP ";
            DxGridSqlDataSource1.SelectCommand = qry;
            Session.Contents.Add("dateSelected", DateSelected);
        }
        catch (Exception)
        {
            throw;
        }
    }
}//END CLASS