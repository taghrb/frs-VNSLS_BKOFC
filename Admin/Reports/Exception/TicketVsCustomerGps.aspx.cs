﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Reports_Exception_TicketVsCustomerGps : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
    
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (IsPostBack && Session["TktVsGpsHdrQry"] != null)
        {
            DxGridSqlDataSource1.SelectCommand = Session["TktVsGpsHdrQry"].ToString();
        }
    }

    protected void btnViewTicket_Click(object sender, EventArgs e)
    {
        string STR_NO = TktInqryStrNo.Value != null ? TktInqryStrNo.Value.ToString().Trim() : "";
        string REG_NO = TktInqryRegNo.Value != null ? TktInqryRegNo.Value.ToString().Trim() : "";
        string CUS_NO = TktInqryCusNo.Value != null ? TktInqryCusNo.Value.ToString().Trim() : "";
        string TKT_NO = TktInqryTicketNo.Text.ToString().Trim();

        string START_DATE = TktInqryDateFrom.Value != null ? ((DateTime)TktInqryDateFrom.Value).ToString("yyyy-MM-dd") : new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");
        string END_DATE = TktInqryDateTo.Value != null ? ((DateTime)TktInqryDateTo.Value).ToString("yyyy-MM-dd") : new DateTime(9999, 12, 31).ToString("yyyy-MM-dd");
        END_DATE = END_DATE + " 23:59:59.998";

        string TrxHdrQry = "SELECT "
            + " TKH_HKEY, TKH_TKTNO, TKH_STRNO, TKH_REGNO, " 
            + " TKH_CUSNO, TKH_CUSNM, TKH_SREP, TKH_DATE, TKH_TIME, " 
            + " TKH_CRTDT, TKH_CRTIM, TKH_CRTBY, TKH_AMT, TKH_VTAMT, " 
            + " TKH_PAYCD, TKH_TTYPE, TKH_GEOLA, TKH_GEOLN, "
            + " CUS_GEOLA, CUS_GEOLN, "
            + " dbo.fnCalcDistanceKM(TKH_GEOLA, CUS_GEOLA, TKH_GEOLN, CUS_GEOLN) AS DistanceBetween, "
            + " 'https://www.google.com/maps/dir/?api=1&origin=' + TKH_GEOLA + ',' + TKH_GEOLN + '&destination=' + CUS_GEOLA + ',' + CUS_GEOLN AS Directions "
            + " FROM SLS_TRX_THDR "
            + " JOIN STP_MST_CSTMR ON CUS_GEOLA <> '' AND CUS_GEOLA <> '0.0' AND CUS_GEOLA <> 'TIMEOUT' AND CUS_NAME NOT LIKE '%cash%' AND CUS_NO = TKH_CUSNO "
            + " WHERE TKH_GEOLA <> '0.0' AND TKH_GEOLN <> 'TIMEOUT' ";

        string HstHdrQry = "SELECT "
            + " TKH_HKEY, TKH_TKTNO, TKH_STRNO, TKH_REGNO, "
            + " TKH_CUSNO, TKH_CUSNM, TKH_SREP, TKH_DATE, TKH_TIME, "
            + " TKH_CRTDT, TKH_CRTIM, TKH_CRTBY, TKH_AMT, TKH_VTAMT, "
            + " TKH_PAYCD, TKH_TTYPE, TKH_GEOLA, TKH_GEOLN, "
            + " CUS_GEOLA, CUS_GEOLN, "
            + " dbo.fnCalcDistanceKM(TKH_GEOLA, CUS_GEOLA, TKH_GEOLN, CUS_GEOLN) AS DistanceBetween, "
            + " 'https://www.google.com/maps/dir/?api=1&origin=' + TKH_GEOLA + ',' + TKH_GEOLN + '&destination=' + CUS_GEOLA + ',' + CUS_GEOLN AS Directions "
            + " FROM SLS_TKH_HIST "
            + " JOIN STP_MST_CSTMR ON CUS_GEOLA <> '' AND CUS_GEOLA <> '0.0' AND CUS_GEOLA <> 'TIMEOUT' AND CUS_NAME NOT LIKE '%cash%' AND CUS_NO = TKH_CUSNO "
            + " WHERE TKH_GEOLA <> '0.0' AND TKH_GEOLN <> 'TIMEOUT' ";


        if (TKT_NO.Length > 0)
        {
            TrxHdrQry += " AND TKH_TKTNO = '" + TKT_NO + "' ";
            HstHdrQry += " AND TKH_TKTNO = '" + TKT_NO + "' ";
        }
        if (STR_NO.Length > 0)
        {
            TrxHdrQry += " AND TKH_STRNO = '" + STR_NO + "' ";
            HstHdrQry += " AND TKH_STRNO = '" + STR_NO + "' ";
        }
        if (REG_NO.Length > 0)
        {
            TrxHdrQry += " AND TKH_REGNO = '" + REG_NO + "' ";
            HstHdrQry += " AND TKH_REGNO = '" + REG_NO + "' ";
        }
        if (CUS_NO.Length > 0)
        {
            TrxHdrQry += " AND TKH_CUSNO = '" + CUS_NO + "' ";
            HstHdrQry += " AND TKH_CUSNO = '" + CUS_NO + "' ";
        }
        if (START_DATE.Length > 0 || END_DATE.Length > 0)
        {
            TrxHdrQry += " AND TKH_DATE BETWEEN '" + START_DATE + "' AND '" + END_DATE + "' ";
            HstHdrQry += " AND TKH_DATE BETWEEN '" + START_DATE + "' AND '" + END_DATE + "' ";
        }

        string MainHdrQry = "SELECT * FROM ( " + TrxHdrQry + " UNION ALL " + HstHdrQry + ") tbl ORDER BY tbl.DistanceBetween DESC ";

        DxGridSqlDataSource1.SelectCommand = MainHdrQry;

        Session["TktVsGpsHdrQry"] = MainHdrQry;

        gvInquery.DataBind();
    }

    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        //gvInquery.SettingsPager.PageSize = Convert.ToInt32(ddlNoOfRecords.SelectedItem.Value);
        //gvInquery.DataBind();
    }

    protected void ACB_CodeCheck_Callback(object source, CallbackEventArgs e)
    {

    }

    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {
    }

    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        {
        }
        else if (e.CommandArgs.CommandName.Equals("VoidTicket"))
        {
            decimal TicketNo;
            if (e.KeyValue.ToString().Trim().Length > 0 && decimal.TryParse(e.KeyValue.ToString(), out TicketNo))
            {
            }
        }
    }

    // Common Methods

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }

    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void gvInquery_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["gvSalesLines"] = (sender as ASPxGridView).GetMasterRowKeyValue();
    }

    protected void gvInquery_CustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        //  if(e.Column.FieldName == "")
    }

    protected void TktInqryStrNo_Init(object sender, EventArgs e)
    {
        DataTable dtStores = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE WHERE STM_CODE IN (" + Session["userBranchCode"].ToString() + ") ");

        TktInqryStrNo.DataSource = dtStores;
        TktInqryStrNo.ValueField = "STM_CODE";
        TktInqryStrNo.TextField = "STM_NAME";
        TktInqryStrNo.DataBind();
    }

    protected void TktInqryRegNo_Init(object sender, EventArgs e)
    {
        string strNo = TktInqryStrNo.Value == null ? "''" : TktInqryStrNo.Value.ToString();
        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + strNo + ") ");

        TktInqryRegNo.DataSource = dtRegisters;
        TktInqryRegNo.ValueField = "REG_CODE";
        TktInqryRegNo.TextField = "REG_NAME";
        TktInqryRegNo.DataBind();
    }

    protected void TktInqryCusNo_Init(object sender, EventArgs e)
    {
        DataTable dtCustomers = dbHlpr.FetchData("SELECT CUS_NO, CUS_NO + ' ' + CUS_NAME AS DSPLY_FLD FROM STP_MST_CSTMR ");

        TktInqryCusNo.DataSource = dtCustomers;
        TktInqryCusNo.ValueField = "CUS_NO";
        TktInqryCusNo.TextField = "DSPLY_FLD";
        TktInqryCusNo.DataBind();
    }

    protected void TktInqryStrNo_ValueChanged(object sender, EventArgs e)
    {
        ASPxComboBox value = (ASPxComboBox)sender;
        string strNo = value.Value == null ? "''" : value.Value.ToString();
        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + strNo + ") ");

        TktInqryRegNo.DataSource = dtRegisters;
        TktInqryRegNo.ValueField = "REG_CODE";
        TktInqryRegNo.TextField = "REG_NAME";
        TktInqryRegNo.DataBind();
    }


}//END CLASS