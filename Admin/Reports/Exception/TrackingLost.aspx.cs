﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Reports_Exception_TrackingLost : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DxGridSqlDataSource1.SelectCommand = "SELECT "
                + " '' AS StoreNo, '' AS Store, '' AS VanNo, '' AS LostSince "
                + " WHERE 1 = 0 ";
        }
        else
        {
            if (cmbxTrackingLost.Value == null)
            {
                return;
            }

            try
            {
                string qry = GetDataQuery();
                DxGridSqlDataSource1.SelectCommand = qry;
                gvInquery.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void ACB_CodeCheck_Callback(object source, CallbackEventArgs e)
    {

    }

    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {
    }

    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        
    }

    // Common Methods

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
    }

    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void gvInquery_CustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        //  if(e.Column.FieldName == "")
    }

    protected void cmbxTrackingLost_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cmbxTrackingLost.Value == null)
        {
            return;
        }

        try
        {
            string qry = GetDataQuery();
            DxGridSqlDataSource1.SelectCommand = qry;
            gvInquery.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private string GetDataQuery()
    {
        string q = "";
        string filterBy = "";

        switch (cmbxTrackingLost.Value.ToString())
        {
            case "1 Month":
                filterBy = " WHERE LostForHours / 24 >= '30' ";
                break;
            case "1 Week":
                filterBy = " WHERE LostForHours / 24 >= '7' ";
                break;
            case "1 Day":
                filterBy = " WHERE LostForHours / 24 >= '1' ";
                break;
            case "12 Hours":
                filterBy = " WHERE LostForHours >= '12' ";
                break;
            case "8 Hours":
                filterBy = " WHERE LostForHours >= '8' ";
                break;
            case "6 Hours":
                filterBy = " WHERE LostForHours >= '6' ";
                break;
            case "4 Hours":
                filterBy = " WHERE LostForHours >= '4' ";
                break;
            case "2 Hours":
                filterBy = " WHERE LostForHours >= '2' ";
                break;
            case "Beginning":
                break;
            default:
                break;
        }

        q = " SELECT *, LostForHours / 24 AS LostForDays FROM ( SELECT "
            + " SRP_STRNO AS StoreNo, STM_NAME AS Store, SRP_CODE AS VanNo, "
            + " MAX(CRD_CAPAT) AS SyncdOn, "
            + " DATEDIFF(HOUR, MAX(CRD_CAPAT), GETDATE()) AS LostForHours "
            + " FROM STP_MSTR_SLREP  "
            + " LEFT JOIN STP_MSTR_STORE ON SRP_STRNO = STM_CODE "
            + " LEFT JOIN GPS_LIV_CORDS ON SRP_CODE = CRD_VAN "
            + " GROUP BY SRP_STRNO, STM_NAME, SRP_CODE "
            + " ) tbl "
            + filterBy
            + " ORDER BY tbl.StoreNo, tbl.VanNo ";
        return q;
    }
}//END CLASS