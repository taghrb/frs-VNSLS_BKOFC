﻿<%@ Page Title="Tracking Lost" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="TrackingLost.aspx.cs" Inherits="Admin_Reports_Exception_TrackingLost" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <style>
        #map-wrapper {
            width: 100%;
            height: 200px;
        }
    </style>
    <script type="text/javascript">
        jQuery = jQuery.noConflict();
        $ = jQuery.noConflict();
        function grid_customizationWindowCloseUp(s, e) {
            //if(FromGridView.IsCustomizationWindowVisible())
            FromGridView.ShowCustomizationWindow();
        }

        $(function () {
            $('.testing123').dialog();
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topBarContent1" runat="Server">
    <div class="topContentBar">
        <ul>
            <li>
                <asp:ImageButton ID="LinkBtnPDF" OnClick="LinkBtnPDF_Click" runat="server" ImageUrl="~/img/FilesIcons/acrobat.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonXLX" OnClick="LinkButtonXLX_Click" runat="server" ImageUrl="~/img/FilesIcons/excel.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonXxls" OnClick="LinkButtonXxls_Click" runat="server" ImageUrl="~/img/FilesIcons/xlsx.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonRtf" OnClick="LinkButtonRtf_Click" runat="server" ImageUrl="~/img/FilesIcons/rtf.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonCvs" OnClick="LinkButtonCvs_Click" runat="server" ImageUrl="~/img/FilesIcons/cvs.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonWord" OnClick="LinkButtonRtf_Click" runat="server" ImageUrl="~/img/FilesIcons/word.png" Width="50" Height="40"></asp:ImageButton>
            </li>

            <li>
                <asp:ImageButton ID="LinkButtonHtml" Visible="false" OnClick="LinkButtonHtml_Click" runat="server" ImageUrl="~/img/icons/searchtop.jpg" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton6" Visible="false" OnClick="LinkButton3_Click" runat="server" ImageUrl="~/img/FilesIcons/acrobat.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton7" Visible="false" OnClick="LinkButton3_Click" runat="server" ImageUrl="~/img/FilesIcons/word.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton8" Visible="false" OnClick="LinkButton3_Click" runat="server" ImageUrl="~/img/FilesIcons/excel.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton9" Visible="false" OnClick="LinkButton3_Click" OnClientClick="window.print();" runat="server" ImageUrl="~/img/FilesIcons/acrobat.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton10" Visible="false" OnClick="LinkButton3_Click" runat="server" ImageUrl="~/img/FilesIcons/torrent.png" Width="50" Height="40"></asp:ImageButton>
            </li>
        </ul>

        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvInquery" ExportedRowType="All"></dx:ASPxGridViewExporter>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">

    <div class="formDiv">
        <div class="formHeaderDiv">
            <h3>Tracking Lost</h3>
        </div>
        <%--        <asp:UpdateProgress runat="server" ID="UpdateProgressOrdersPage" AssociatedUpdatePanelID="UpdatePanelOrdersPage">
            <ProgressTemplate>
                <div style="position: fixed; z-index: 999; height: 12000px; width: 100%; top: 0; background-color: black; filter: alpha(opacity=60); opacity: 0.6; -moz-opacity: 0.8;">
                        <img style="display: block; z-index: 1000; margin: 150px auto;" src="../../img/loading-image.gif" alt="Loading... Please wait" />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>--%>
        <%--<asp:UpdatePanel runat="server" ID="UpdatePanelOrdersPage" ChildrenAsTriggers="true">
            <ContentTemplate>--%>
                <fieldset style="padding: 10px; background: gray;">
                    <div style="margin: 0 auto; width: 50%;">
                        <label style="display: inline; float: left; margin: 0; line-height: 32px;">Lost Since : </label>
                        <dx:ASPxComboBox ID="cmbxTrackingLost" runat="server" AnimationType="Auto" OnSelectedIndexChanged="cmbxTrackingLost_SelectedIndexChanged" AutoPostBack="true">
                            <Items>
                                <dx:ListEditItem Text="Select..." Value="" Selected="true" />
                                <dx:ListEditItem Text="Beginning" Value="Beginning" />
                                <dx:ListEditItem Text="1 Month" Value="1 Month" />
                                <dx:ListEditItem Text="1 Week" Value="1 Week" />
                                <dx:ListEditItem Text="1 Day" Value="1 Day" />
                                <dx:ListEditItem Text="12 Hours" Value="12 Hours" />
                                <dx:ListEditItem Text="8 Hours" Value="8 Hours" />
                                <dx:ListEditItem Text="6 Hours" Value="6 Hours" />
                                <dx:ListEditItem Text="4 Hours" Value="4 Hours" />
                                <dx:ListEditItem Text="2 Hours" Value="2 Hours" />
                            </Items>
                            <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                <ErrorFrameStyle Font-Size="Smaller" />
                                <RequiredField IsRequired="true" ErrorText="Filter Type is Required." />
                            </ValidationSettings>
                        </dx:ASPxComboBox>
                    </div>
                </fieldset>
                <asp:SqlDataSource runat="server" ID="DxGridSqlDataSource1"
                    ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
                <dx:ASPxGridView ID="gvInquery" CssClass="FromGridView" ClientInstanceName="FromGridView" Theme="Office2010Black" runat="server"
                    DataSourceID="DxGridSqlDataSource1"
                    AutoGenerateColumns="false"
                    KeyFieldName="VLD_LKEY"
                    OnRowCommand="gvInquery_RowCommand"
                    OnDataBound="gvInquery_DataBound"
                    OnBeforeGetCallbackResult="gvInquery_BeforeGetCallbackResult"
                    Settings-ShowTitlePanel="false"
                    SettingsText-Title="Tracking Lost">


                    <Settings UseFixedTableLayout="false" ShowGroupPanel="true" VerticalScrollBarMode="Auto" HorizontalScrollBarMode="Visible" />
                    <SettingsContextMenu Enabled="True" EnableColumnMenu="True" EnableRowMenu="True" />
                    <SettingsSearchPanel Visible="False" CustomEditorID="CustomSearchPanelTxtBx" />
                    <Columns>
                        <dx:GridViewCommandColumn Caption="Select All " VisibleIndex="0" Visible="false"
                            ShowNewButtonInHeader="false"
                            SelectAllCheckboxMode="AllPages"
                            ShowSelectButton="true"
                            AllowDragDrop="False">
                        </dx:GridViewCommandColumn>

                        <dx:GridViewDataTextColumn FieldName="StoreNo" Caption="Br. Code" VisibleIndex="0">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Store" Caption="Branch" VisibleIndex="1">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="VanNo" Caption="Van #" VisibleIndex="2">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="SyncdOn" Caption="Last Synced On" VisibleIndex="3">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="LostForHours" Caption="Lost Since (Hrs)" VisibleIndex="4">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="LostForDays" Caption="Lost Since (Days)" VisibleIndex="4">
                            <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <SettingsCommandButton>
                        <NewButton ButtonType="Button" Text="New" Styles-Style-CssClass="btna btn btn-primary  btn-sm">
                            <Image runat="server" Url="~/img/icons/white-icn/Add.png" Width="15px"></Image>
                        </NewButton>
                        <SelectButton ButtonType="Button" Text="Select" Styles-Style-CssClass="btna btn btn-success  btn-sm">
                            <Image ToolTip="Select button here" Url="~/img/icons/white-icn/eyee1.png" Width="15px" />
                        </SelectButton>
                        <EditButton ButtonType="Button" Text="Edit" Styles-Style-CssClass="btna btn btn-info btn-sm">
                            <Image runat="server" Url="~/img/icons/white-icn/Pen Tool.png" Width="15px"></Image>
                        </EditButton>
                        <DeleteButton ButtonType="Button" Text="Delete" Styles-Style-CssClass="btna btn btn-danger btn-sm btn-danger-variant">
                            <Image runat="server" Url="~/img/icons/white-icn/Trash.png" Width="15px"></Image>
                        </DeleteButton>
                        <CancelButton ButtonType="Button" Text="Close" Styles-Style-CssClass="btnPopup btn btn-primary">
                            <%--<Image ToolTip="Cancel button here" Url="~/img/icons/close.png" />--%>
                        </CancelButton>
                        <UpdateButton ButtonType="Button" Text="Update">
                        </UpdateButton>
                    </SettingsCommandButton>
                    <SettingsEditing EditFormColumnCount="2" />
                    <SettingsPopup>
                        <EditForm VerticalAlign="WindowCenter" AllowResize="True" Modal="True" HorizontalAlign="WindowCenter"></EditForm>
                    </SettingsPopup>
                    <SettingsPager Mode="ShowAllRecords"></SettingsPager>
                    <Styles>
                        <CommandColumn Spacing="0px" Wrap="False" />
                        <EditFormDisplayRow HorizontalAlign="Center" VerticalAlign="Middle"></EditFormDisplayRow>
                        <AlternatingRow CssClass="alterRowGV" Enabled="True"></AlternatingRow>
                        <Row CssClass="rowHeight"></Row>
                        <Cell Wrap="False" />
                    </Styles>

                    <SettingsLoadingPanel Mode="ShowOnStatusBar" />
                    <SettingsBehavior SortMode="DisplayText" AllowSelectSingleRowOnly="false" AllowSelectByRowClick="true" EnableCustomizationWindow="true" AllowDragDrop="true" ConfirmDelete="true" ColumnResizeMode="Control" />
                    <ClientSideEvents ColumnStartDragging="grid_customizationWindowCloseUp" EndCallback="GridOnEndCallBack" />

                    <Templates>
                        <StatusBar>
                            <table>
                                <tr>
                                    <td>Search: </td>
                                    <td>
                                        <dx:ASPxButtonEdit runat="server" ID="CustomSearchPanelTxtBx" />
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>Search In:</td>
                                    <td>
                                        <dx:ASPxComboBox AutoPostBack="true" ID="DDLFilterByColumn" runat="server" OnSelectedIndexChanged="DDLFilterByColumn_SelectedIndexChanged">
                                            <Items>
                                                <dx:ListEditItem Text="All" Value="*" />
                                            </Items>
                                        </dx:ASPxComboBox>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td></td>
                                </tr>
                            </table>
                        </StatusBar>
                    </Templates>
                </dx:ASPxGridView>

            <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
    </div>
</asp:Content>

