﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using System.Drawing;
using DevExpress.XtraPrinting;
using DevExpress.Export.Xl;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class Admin_Reports_PaymentsByCustomer : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
    ReportsDS ReportsDataSet = new ReportsDS();
    string START_DATE = "Earliest";
    string END_DATE = "Latest";

    protected void Page_Init(object sender, EventArgs e)
    {
        //PayByCustDateFrom.Value = new DateTime(1900, 1, 1);
        //PayByCustDateTo.Value = new DateTime(9999, 12, 31);

        if (Session["Rpt"] != null && IsPostBack)
        {
            RptVwr.ReportSource = Session["Rpt"];
        }
    }

    protected void btnExportPaymentsByCustomer_Click(object sender, EventArgs e)
    {
        START_DATE = PayByCustDateFrom.Value != null ? ((DateTime)PayByCustDateFrom.Value).ToString("yyyy-MM-dd") : new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");
        END_DATE = PayByCustDateTo.Value != null ? ((DateTime)PayByCustDateTo.Value).ToString("yyyy-MM-dd") : new DateTime(9999, 12, 31).ToString("yyyy-MM-dd");

        string oneStore = PayByCustStrNo.Value.ToString();
        string oneRegister = PayByCustRegNo.Value.ToString();

        DataTable dtExport = new DataTable();

        if (CmBxReportType.Value == "RCPT")
        {
            string qryLin = "SELECT "
                + " PR_NO, PR_RCPDT, PR_RCPTM, PR_CUSNO, PR_CUSNM, "
                + " PR_STRNO, PR_REGNO, PR_DRWNO, PR_AMTPD, PR_PAYCD, "
                + " PR_CHQNO, PR_APLY2, PR_CMNTS, PR_CRTBY, PR_CRTDT, "
                + " PR_CRTTM, PR_SREP, PR_GEOLA, PR_GEOLN, PR_STATS, "
                + " PR_TOTAL, PR_DSTYP, PR_DSFCT, "
                + " '' AS APLY2INVC, '' AS APLY2DAT, '' AS APLY2AMT "
                + " FROM CUS_PAY_RCPTS "
                + " WHERE PR_STRNO = '" + oneStore + "' "
                + " AND PR_REGNO = '" + oneRegister + "' "
                + " AND PR_RCPDT BETWEEN '" + START_DATE + "' AND CONVERT(datetime, '" + END_DATE + " 23:59:59.998') ";
            dtExport = dbHlpr.FetchData(qryLin);
        }
        else if (CmBxReportType.Value == "A4")
        {
            string qryLin = "SELECT "
                + " PR_NO, PR_RCPDT, PR_RCPTM, PR_CUSNO, PR_CUSNM, PR_SREP, "
                + " PR_STRNO, PR_REGNO, PR_AMTPD, PR_PAYCD, PR_CRTBY, PR_CRTDT, PR_CRTTM, "
                + " PR_APLY2, PR_STATS, "
                + " PR_TOTAL, PR_DSTYP, PR_DSFCT, "
                + " PA_INVNO AS APLY2INVC, PA_INVDT AS APLY2DAT, PA_AMT AS APLY2AMT "
                + " FROM CUS_PAY_RCPTS "
                + " LEFT JOIN CUS_PAY_APLY2 ON PR_NO = PA_PRNO "
                + " WHERE PR_STRNO = '" + oneStore + "' "
                + " AND PR_REGNO = '" + oneRegister + "' "
                + " AND PR_RCPDT BETWEEN '" + START_DATE + "' AND CONVERT(datetime, '" + END_DATE + " 23:59:59.998') ";
            dtExport = dbHlpr.FetchData(qryLin);
        }

        lblErrorMessage.Text = "";
        RptVwr.ReportSource = null;
        Session["Rpt"] = null;

        if (CmBxReportType.Value == "RCPT")
        {
            if (PaymentsReportTempData(dtExport, oneStore, oneRegister))
            {
                ReportGen();
            }
            else
            {
                lblErrorMessage.Text = "No Record Found.";
            }
        }
        else if (CmBxReportType.Value == "A4")
        {
            if (PaymentsReportTempData(dtExport, oneStore, oneRegister))
            {
                ReportGen();
            }
            else
            {
                lblErrorMessage.Text = "No Record Found.";
            }
        }
        else
        {
            lblErrorMessage.Text = "Invalid Report Type.";
        }
    }

    public bool PaymentsReportTempData(DataTable DTBL, string STRS, string REGS)
    {
        if (DTBL.Rows.Count <= 0)
        {
            return false;
        }

        DataRow dr = ReportsDataSet.PARAM_TABLE.NewRow();

        //Company Details starts
        dr["param02"] = START_DATE;
        dr["param03"] = END_DATE;
        dr["param04"] = PayByCustRegNo.Value.ToString();
        dr["param11"] = PayByCustStrNo.Value.ToString();
        dr["param12"] = "الغذائىة الفرسان - مبيعات فان";
        dr["param13"] = "VAT # 300056053500003 ";
        dr["param14"] = "Pri. Sultan Abdul Aziz St ,Riyadh";
        dr["param15"] = "Tel # (011) 416-4422";
        dr["param16"] = "Mobile # 055-816-1122";

        if (DTBL.Rows[0]["PR_NO"] != null && DTBL.Rows[0]["PR_NO"] != "")
        {
            dr["param01"] = DTBL.Rows[0]["PR_NO"].ToString(); // Ticket Code
        }
        else
        {
            dr["param01"] = "";
        }
        if (DTBL.Rows[0]["PR_SREP"] != null && DTBL.Rows[0]["PR_SREP"] != "")
        {
            dr["param05"] = DTBL.Rows[0]["PR_SREP"].ToString().ToUpper(); // Sales Rep
        }
        else
        {
            dr["param05"] = "";
        }
        if (DTBL.Rows[0]["PR_CRTDT"] != null && DTBL.Rows[0]["PR_CRTDT"] != "")
        {
            DateTime DTtmp = Convert.ToDateTime(DTBL.Rows[0]["PR_CRTDT"].ToString());
            dr["param06"] = DTtmp.ToString("dd-MMM-yyyy, HH:mm"); // Date                   
        }
        else
        {
            dr["param06"] = "";
        }
        dr["param06"] = DateTime.Now.ToString("dd-MMM-yyyy, HH:mm"); // Date 

        if (DTBL.Rows[0]["PR_CRTTM"] != null && DTBL.Rows[0]["PR_CRTTM"] != "")
        {
            dr["param07"] = DTBL.Rows[0]["PR_CRTTM"].ToString(); // Time
        }
        else
        {
            dr["param07"] = "";
        }

        if (DTBL.Rows[0]["PR_AMTPD"] != null && DTBL.Rows[0]["PR_AMTPD"] != "")
        {
            dr["param35"] = DTBL.Rows[0]["PR_AMTPD"].ToString();// Total Amount
        }
        else
        {
            dr["param35"] = "0.00";
        }

        dr["param20"] = DTBL.Rows.Count;

        string cashAmt = DTBL.Compute("SUM(PR_AMTPD)", "PR_PAYCD = 'CASH'").ToString();
        string chqAmt = DTBL.Compute("SUM(PR_AMTPD)", "PR_PAYCD = 'CHQ'").ToString();
        string spanAmt = DTBL.Compute("SUM(PR_AMTPD)", "PR_PAYCD = 'SPAN'").ToString();
        string voidAmt = DTBL.Compute("SUM(PR_AMTPD)", "PR_STATS = 'Void'").ToString();
        dr["param21"] = cashAmt.Trim().Equals("") ? "0.00" : String.Format("{0:n}", Convert.ToDouble(cashAmt));
        dr["param22"] = chqAmt.Trim().Equals("") ? "0.00" : String.Format("{0:n}", Convert.ToDouble(chqAmt));
        dr["param23"] = spanAmt.Trim().Equals("") ? "0.00" : String.Format("{0:n}", Convert.ToDouble(spanAmt));
        dr["param24"] = voidAmt.Trim().Equals("") ? "0.00" : String.Format("{0:n}", Convert.ToDouble(voidAmt));
        ReportsDataSet.PARAM_TABLE.Rows.Add(dr);

        for (int i = 0; i < DTBL.Rows.Count; i++)
        {
            DataRow drLin = ReportsDataSet.TEMP_TABLE.NewRow();

            if (DTBL.Rows[i]["PR_NO"] != null && DTBL.Rows[i]["PR_NO"] != "")
            {
                drLin["Text01"] = DTBL.Rows[i]["PR_NO"].ToString(); // Ticket No / PR No
            }
            else
            {
                drLin["Text01"] = "";
            }

            if (DTBL.Rows[i]["PR_CUSNO"] != null && DTBL.Rows[i]["PR_CUSNO"] != "")
            {
                drLin["Text05"] = DTBL.Rows[i]["PR_CUSNO"].ToString(); // Catg No
            }
            else
            {
                drLin["Text05"] = "";
            }

            if (DTBL.Rows[i]["PR_CUSNM"] != null && DTBL.Rows[i]["PR_CUSNM"] != "")
            {
                drLin["Text06"] = DTBL.Rows[i]["PR_CUSNM"].ToString(); // cust name
            }
            else
            {
                drLin["Text06"] = "";
            }

            if (DTBL.Rows[i]["PR_PAYCD"] != null && DTBL.Rows[i]["PR_PAYCD"] != "")
            {
                drLin["Text15"] = DTBL.Rows[i]["PR_PAYCD"].ToString(); // Pay Code
            }
            else
            {
                drLin["Text15"] = "";
            }


            if (DTBL.Rows[i]["PR_SREP"] != null && DTBL.Rows[i]["PR_SREP"] != "")
            {
                drLin["Text07"] = DTBL.Rows[i]["PR_SREP"].ToString().ToUpper(); // SLS rep
            }
            else
            {
                drLin["Text07"] = "";
            }
            if (DTBL.Rows[i]["PR_CRTDT"] != null && DTBL.Rows[i]["PR_CRTDT"] != "")
            {
                drLin["Text08"] = DTBL.Rows[i]["PR_CRTDT"].ToString(); // Date
            }
            else
            {
                drLin["Text08"] = "";
            }

            if (CmBxReportType.Value == "A4")
            {
                try
                {
                    drLin["Text08"] = Convert.ToDateTime(drLin["Text08"].ToString()).ToString("dd-MMM-yyyy");
                }
                catch
                {
                }
            }

            if (DTBL.Rows[i]["PR_PAYCD"] != null && DTBL.Rows[i]["PR_PAYCD"] != "")
            {
                if (DTBL.Rows[i]["PR_PAYCD"].ToString().Equals("CASH"))
                {
                    drLin["Text17"] = "Cash";
                }
                else if (DTBL.Rows[i]["PR_PAYCD"].ToString().Equals("CHQ"))
                {
                    drLin["Text17"] = "Cheque";
                }
                else if (DTBL.Rows[i]["PR_PAYCD"].ToString().Equals("SPAN"))
                {
                    drLin["Text17"] = "Span";
                }
                else
                {
                    drLin["Text17"] = DTBL.Rows[i]["PR_PAYCD"].ToString();
                }
            }
            else
            {
                drLin["Text17"] = "";
            }

            if (DTBL.Rows[i]["PR_APLY2"] != null && DTBL.Rows[i]["PR_APLY2"] != "")
            {
                drLin["Text18"] = DTBL.Rows[i]["PR_APLY2"].ToString();
            }
            else
            {
                drLin["Text18"] = "";
            }

            if (DTBL.Rows[i]["PR_STATS"] != null && DTBL.Rows[i]["PR_STATS"] != "")
            {
                drLin["Text19"] = DTBL.Rows[i]["PR_STATS"].ToString();
            }
            else
            {
                drLin["Text19"] = "-";
            }
            /////////////// num starts here /////////////////
            if (DTBL.Rows[i]["PR_AMTPD"] != null && DTBL.Rows[i]["PR_AMTPD"] != "")
            {
                drLin["Num03"] = DTBL.Rows[i]["PR_AMTPD"].ToString();// Ex Price
            }
            else
            {
                drLin["Num03"] = "0.00";
            }


            drLin["TEXT21"] = DTBL.Rows[i]["APLY2INVC"].ToString();
            try
            {
                drLin["TEXT22"] = Convert.ToDateTime(DTBL.Rows[i]["APLY2DAT"].ToString()).ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                drLin["TEXT22"] = "";
            }
            if (DTBL.Rows[i]["APLY2AMT"].ToString().Trim().Length > 0)
            {
                drLin["NUM20"] = Convert.ToDouble(DTBL.Rows[i]["APLY2AMT"].ToString()).ToString("n2");
            }
            else
            {
                drLin["NUM20"] = Convert.ToDouble("0.00").ToString("n2");
            }

            ReportsDataSet.TEMP_TABLE.Rows.Add(drLin);
        }


        return true;
    }

    public void ReportGen()
    {
        try
        {
            // One Way
            ReportDocument RptDoc = new ReportDocument();
            if (CmBxReportType.Value == "RCPT")
            {
                RptDoc.Load(Server.MapPath("~/RptFiles/AppReports/RPT_PAY_BYCUS.rpt"));
            }
            else if (CmBxReportType.Value == "A4")
            {
                RptDoc.Load(Server.MapPath("~/RptFiles/AppReports/RPT_PAY_BYCUS_A4.rpt"));
            }
            RptDoc.SetDataSource(ReportsDataSet);
            RptVwr.ReportSource = RptDoc;
            Session["Rpt"] = RptDoc;
            //CrystalDecisions.CrystalReports.Engine.TextObject TXTCOMNTS_BOX = RptDoc.ReportDefinition.ReportObjects["TXT_CMMNT"] as CrystalDecisions.CrystalReports.Engine.TextObject;

            //TXTCOMNTS_BOX.ObjectFormat.EnableSuppress = txtComments.Text.Trim().Length <= 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void PayByCustStrNo_Init(object sender, EventArgs e)
    {
        DataTable dtStores = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE WHERE STM_CODE IN (" + Session["userBranchCode"].ToString() + ") ");

        PayByCustStrNo.DataSource = dtStores;
        PayByCustStrNo.ValueField = "STM_CODE";
        PayByCustStrNo.TextField = "STM_NAME";
        PayByCustStrNo.DataBind();
    }
    protected void PayByCustRegNo_Init(object sender, EventArgs e)
    {
        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + Session["userBranchCode"].ToString() + ") ");

        PayByCustRegNo.DataSource = dtRegisters;
        PayByCustRegNo.ValueField = "REG_CODE";
        PayByCustRegNo.TextField = "REG_NAME";
        PayByCustRegNo.DataBind();
    }

    protected void DsrStrNo_ValueChanged(object sender, EventArgs e)
    {
        ASPxComboBox value = (ASPxComboBox)sender;
        string strNo = value.Value == null ? "" : value.Value.ToString();
        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + strNo + ") ");

        PayByCustRegNo.DataSource = dtRegisters;
        PayByCustRegNo.ValueField = "REG_CODE";
        PayByCustRegNo.TextField = "REG_NAME";
        PayByCustRegNo.DataBind();
    }
}