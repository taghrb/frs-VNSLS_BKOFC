﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using System.Drawing;
using DevExpress.XtraPrinting;
using DevExpress.Export.Xl;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class Admin_Reports_Excp_HdrLinCustMisMatch : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
    string START_DATE = "Earliest";
    string END_DATE = "Latest";

    protected void Page_Init(object sender, EventArgs e)
    {

    }

    protected void btnExportExcp_HdrLinCustMisMatch_Click(object sender, EventArgs e)
    {
        START_DATE = DateFrom.Value != null ? ((DateTime)DateFrom.Value).ToString("yyyy-MM-dd") : new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");
        END_DATE = DateTo.Value != null ? ((DateTime)DateTo.Value).ToString("yyyy-MM-dd") : new DateTime(9999, 12, 31).ToString("yyyy-MM-dd");

        string stores = CommaSeperatedToQuery(StrNo.Value.ToString());
        string registers = CommaSeperatedToQuery(RegNo.Value.ToString());

        DataTable dtExport = new DataTable();

        string qry = "SELECT * FROM ( "
                + " SELECT "
                + " TKH_TKTNO AS 'Ticket #', TKH_STRNO AS 'Store #', TKH_REGNO AS 'Reg. #', TKH_SREP AS 'S. Rep.', "
                + " TKH_TTYPE AS 'TRX Type', TKH_CUSNO AS 'Cust. #', TKH_CUSNM AS 'Cust. Name', "
                + " TKL_CUSNO AS 'Ln Cust. #', TKL_CUSNM AS 'Ln Cust. Name', "
                + " TKH_DATE AS 'Date', TKH_TIME AS 'Time', "
                + " TKH_SLAMT AS 'Sales Amt.', TKH_NTAMT AS 'Net Amt.', TKH_VTAMT AS 'VAT Amt.', TKH_AMT AS 'Total', "
                + " TKH_PAYCD AS 'Pay Cd', TKH_CRTBY AS 'User', TKH_HKEY AS 'Hdr Key', TKL_HKEY AS 'Lin Key' "

                + " FROM SLS_TRX_TLIN "

                + " JOIN SLS_TRX_THDR ON TKL_HKEY = TKH_HKEY "

                + " WHERE TKH_DATE BETWEEN '" + START_DATE + "' AND CONVERT(datetime, '" + END_DATE + " 23:59:59.998') "
                + " AND TKH_STRNO IN (" + stores + ") "
                + " AND TKH_REGNO IN (" + registers + ") "

                + " UNION ALL "

                + " SELECT "
                + " TKH_TKTNO AS 'Ticket #', TKH_STRNO AS 'Store #', TKH_REGNO AS 'Reg. #', TKH_SREP AS 'S. Rep.', "
                + " TKH_TTYPE AS 'TRX Type', TKH_CUSNO AS 'Cust. #', TKH_CUSNM AS 'Cust. Name', "
                + " TKL_CUSNO AS 'Ln Cust. #', TKL_CUSNM AS 'Ln Cust. Name', "
                + " TKH_DATE AS 'Date', TKH_TIME AS 'Time', "
                + " TKH_SLAMT AS 'Sales Amt.', TKH_NTAMT AS 'Net Amt.', TKH_VTAMT AS 'VAT Amt.', TKH_AMT AS 'Total', "
                + " TKH_PAYCD AS 'Pay Cd', TKH_CRTBY AS 'User', TKH_HKEY AS 'Hdr Key', TKL_HKEY AS 'Lin Key' "

                + " FROM SLS_TKL_HIST "

                + " JOIN SLS_TKH_HIST ON TKL_HKEY = TKH_HKEY "

                + " WHERE TKH_DATE BETWEEN '" + START_DATE + "' AND CONVERT(datetime, '" + END_DATE + " 23:59:59.998') "
                + " AND TKH_STRNO IN (" + stores + ") "
                + " AND TKH_REGNO IN (" + registers + ") "

                + " ) tbl "
                + " WHERE tbl.[Cust. #] <> tbl.[Ln Cust. #] "
                + " ORDER BY tbl.[Date], tbl.[Ticket #] ";

        dtExport = dbHlpr.FetchData(qry);

        if (dtExport.Rows.Count > 0)
        {
            lblErrorMessage.Text = "";
            gvExportData.AutoGenerateColumns = true;

            DxGridSqlDataSource1.SelectCommand = qry;

            string filename = "VS_EXCP_HDRLIN_" + DateTime.Now.ToString("ddMMMyyyy$HH:mm:ss") + ".xls";

            var exportOptions = new XlsxExportOptionsEx();
            exportOptions.ShowGridLines = false;
            exportOptions.ExportType = DevExpress.Export.ExportType.WYSIWYG;
            //exportOptions.CustomizeCell += exportOptions_CustomizeCell;
            gvExportDataExporter.Styles.Cell.Wrap = DevExpress.Utils.DefaultBoolean.False;
            gvExportDataExporter.Styles.Default.Wrap = DevExpress.Utils.DefaultBoolean.False;
            gvExportDataExporter.Styles.Header.Wrap = DevExpress.Utils.DefaultBoolean.False;
            gvExportDataExporter.WriteXlsxToResponse(filename, exportOptions);
            //ExportToExcel(dtExport, filename);
            //ExportToExcelNormal(dtExport, filename);
            //ExportToExcelDevExpress(dtExport, filename);
        }
        else
        {
            lblErrorMessage.Text = "No Record Found.";
        }
    }

    protected void gvExportDataExporter_RenderBrick(object sender, ASPxGridViewExportRenderingEventArgs e)
    {
        e.BrickStyle.BorderWidth = 1;
        e.BrickStyle.BorderColor = Color.Black;

        if (e.RowType == GridViewRowType.Header)
        {
            e.BrickStyle.BackColor = Color.FromArgb(198, 89, 17);
            e.BrickStyle.ForeColor = Color.White;
            e.BrickStyle.Font = new Font("Segoe UI", 12, FontStyle.Bold);
        }
        else if (e.RowType == GridViewRowType.Data)
        {
            e.BrickStyle.Font = new Font("Segoe UI", 12, FontStyle.Regular);
            if (e.VisibleIndex % 2 == 0)
            {
                e.BrickStyle.BackColor = Color.FromArgb(221, 235, 247);
            }
            else
            {
                e.BrickStyle.BackColor = Color.White;
            }
        }
        else
        {
            e.BrickStyle.BorderWidth = 0;
            e.BrickStyle.BorderColor = Color.Transparent;
        }
    }

    protected void gvExportData_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        e.Cell.Height = Unit.Point(25);
    }

    private void ExportToExcelDevExpress(DataTable dtExport, string filename)
    {
        System.IO.StringWriter tw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
        ASPxGridView dgGrid = new ASPxGridView();
        dgGrid.Theme = "Office2010Black";
        dgGrid.DataSource = dtExport;
        dgGrid.DataBind();

        ASPxGridViewExporter exporter = new ASPxGridViewExporter();
        exporter.GridViewID = dgGrid.ID;
        exporter.WriteXlsToResponse(filename);
    }

    private string CommaSeperatedToQuery(string str)
    {
        string[] strArr = str.Split(',');
        string newStr = string.Empty;

        if (strArr.Length > 1)
        {
            for (int i = 0; i < strArr.Length; i++)
            {
                newStr += "'" + strArr[i] + "',";
            }
            newStr = newStr.TrimEnd(',');
            return newStr;
        }
        else
        {
            newStr = "'" + strArr[0] + "'";
            return newStr;
        }
    }

    protected void StrNo_Init(object sender, EventArgs e)
    {
        ASPxDropDownEdit ddStores = (ASPxDropDownEdit)sender;
        ASPxListBox lstBxStores = (ASPxListBox)ddStores.FindControl("lstBxStrItems");

        DataTable dtStores = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE WHERE STM_CODE IN (" + Session["userBranchCode"].ToString() + ") ");

        string tempStr = "";
        for (int i = 0; i < dtStores.Rows.Count; i++)
        {
            tempStr += dtStores.Rows[i]["STM_CODE"].ToString() + ",";
        }
        tempStr = tempStr.Trim(',');

        DataRow dr = dtStores.NewRow();
        dr["STM_NAME"] = "All";
        dr["STM_CODE"] = "0";
        dtStores.Rows.InsertAt(dr, 0);

        lstBxStores.DataSource = dtStores;
        lstBxStores.ValueField = "STM_CODE";
        lstBxStores.TextField = "STM_NAME";
        lstBxStores.DataBind();

        ddStores.Value = tempStr;
    }
    protected void RegNo_Init(object sender, EventArgs e)
    {
        ASPxDropDownEdit ddRegisters = (ASPxDropDownEdit)sender;
        ASPxListBox lstBxRegisters = (ASPxListBox)ddRegisters.FindControl("lstBxRegItems");

        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + Session["userBranchCode"].ToString() + ") ");

        string tempStr = "";
        for (int i = 0; i < dtRegisters.Rows.Count; i++)
        {
            tempStr += dtRegisters.Rows[i]["REG_CODE"].ToString() + ",";
        }
        tempStr = tempStr.Trim(',');

        DataRow dr = dtRegisters.NewRow();
        dr["REG_NAME"] = "All";
        dr["REG_CODE"] = "0";
        dtRegisters.Rows.InsertAt(dr, 0);

        lstBxRegisters.DataSource = dtRegisters;
        lstBxRegisters.ValueField = "REG_CODE";
        lstBxRegisters.TextField = "REG_NAME";
        lstBxRegisters.DataBind();

        ddRegisters.Value = tempStr;
    }
    protected void StoreChanged(object sender, EventArgs e)
    {
        ASPxListBox lstBxRegisters = (ASPxListBox)RegNo.FindControl("lstBxRegItems");

        string stores = "''";
        if (StrNo.Value != null)
        {
            stores = StrNo.Value.ToString();
        }

        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + stores + ") ");

        string tempStr = "";
        for (int i = 0; i < dtRegisters.Rows.Count; i++)
        {
            tempStr += dtRegisters.Rows[i]["REG_CODE"].ToString() + ",";
        }
        tempStr = tempStr.Trim(',');

        DataRow dr = dtRegisters.NewRow();
        dr["REG_NAME"] = "All";
        dr["REG_CODE"] = "0";
        dtRegisters.Rows.InsertAt(dr, 0);

        lstBxRegisters.DataSource = dtRegisters;
        lstBxRegisters.ValueField = "REG_CODE";
        lstBxRegisters.TextField = "REG_NAME";
        lstBxRegisters.DataBind();

        RegNo.Value = tempStr;
    }

}