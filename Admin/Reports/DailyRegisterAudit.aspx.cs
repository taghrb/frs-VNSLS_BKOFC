﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using System.Drawing;
using DevExpress.XtraPrinting;
using DevExpress.Export.Xl;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class Admin_Reports_DailyRegisterAudit : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
    ReportsDS ReportsDataSet = new ReportsDS();
    string START_DATE = "Earliest";
    string END_DATE = "Latest";

    protected void Page_Init(object sender, EventArgs e)
    {
        //DraDateFrom.Value = new DateTime(1900, 1, 1);
        //DraDateTo.Value = new DateTime(9999, 12, 31);

        if (Session["Rpt"] != null && IsPostBack)
        {
            RptVwr.ReportSource = Session["Rpt"];
        }
    }

    protected void btnExportDailyRegisterAudit_Click(object sender, EventArgs e)
    {
        START_DATE = DraDateFrom.Value != null ? ((DateTime)DraDateFrom.Value).ToString("yyyy-MM-dd") : new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");
        END_DATE = DraDateTo.Value != null ? ((DateTime)DraDateTo.Value).ToString("yyyy-MM-dd") : new DateTime(9999, 12, 31).ToString("yyyy-MM-dd");

        string stores = CommaSeperatedToQuery(DraStrNo.Value.ToString());
        string registers = CommaSeperatedToQuery(DraRegNo.Value.ToString());

        DataTable dtExport = new DataTable();
        //string qry = "SELECT "
        //    + " TKL_DATE, TKL_TTYPE, TKL_DRWNO, TKL_REGNO, "
        //    + " TKL_CUSNO, TKL_CUSNM, TKL_SREP, "
        //    + " '' AS 'SHPTO', '' AS 'PONUM', '' AS 'SHPVIA', "
        //    + " TKL_TKTNO, TKL_ITMNO, TKL_DESC, "
        //    + " TKL_UOM , TKL_QTY , TKL_RSNCD, TKL_PRICE, "
        //    + " '' AS 'REFNUM', "
        //    + " TKL_EXPDT, TKL_VATPRC, TKL_PAYCD, TKH_AMT "
        //    + " FROM SLS_TRX_TLIN "
        //    + " JOIN SLS_TRX_THDR ON TKH_HKEY = TKL_HKEY "
        //    + " WHERE "
        //    + " TKL_DATE BETWEEN '" + dateFrom + "' AND '" + dateTo + "' "
        //    + " AND TKL_STRNO IN (" + stores + ") "
        //    + " AND TKL_REGNO IN (" + registers + ") ";

        string qry = "SELECT "
                + " TKH_TKTNO, TKH_CUSNO, TKH_CUSNM, "
                + " TKH_DATE, TKH_TIME, TKH_SREP, "
                + " TKH_SLAMT, TKH_NTAMT, TKH_VTAMT, TKH_AMT, "
                + " TKH_PAYCD, TKH_CRTBY, "
                + " TKH_STRNO, TKH_REGNO, TKH_TTYPE,"

                + " TKL_ITMNO, TKL_DESC, TKL_EXPDT, TKL_QTY, TKL_UOM, TKL_PRICE, TKL_EXPRC, TKL_VATPRC "

                + " FROM SLS_TRX_THDR "

                + " JOIN SLS_TRX_TLIN ON TKH_TKTNO = TKL_TKTNO "

                + " WHERE TKH_DATE BETWEEN '" + START_DATE + "' AND CONVERT(datetime, '" + END_DATE + " 23:59:59.998') "
                + " AND TKL_STRNO IN (" + stores + ") "
                + " AND TKL_REGNO IN (" + registers + ") "

                + " ORDER BY TKH_TKTNO ";

        dtExport = dbHlpr.FetchData(qry);

        if (PrepareTempData(dtExport, stores, registers))
        {
            lblErrorMessage.Text = "";
            ReportGen();
        }
        else
        {
            lblErrorMessage.Text = "No Record Found.";
            RptVwr.ReportSource = null;
            Session["Rpt"] = null;
        }
    }

    private bool PrepareTempData(DataTable dtDsr, string STRS, string REGS)
    {
        if (dtDsr.Rows.Count <= 0)
        {
            return false;
        }

        double cashSales = 0;
        double cashReturns = 0;
        double cashNetSales = 0;
        double creditSales = 0;
        double creditReturns = 0;
        double creditNetSales = 0;
        double totalSales = 0;
        double totalReturns = 0;
        double netSales = 0;

        double voidedSales = 0;

        int noOfVoidedTickets = 0;
        int noOfSalesTickets = 0;
        int noOfReturnsTickets = 0;
        int noOfTickets = 0;

        string previousTktNo = "";
        bool countThisTicketAsNew = false;
        for (int i = 0; i < dtDsr.Rows.Count; i++)
        {
            DataRow tmpRow = ReportsDataSet.TEMP_TABLE.NewRow();

            // This below logic is for Tickets Counter
            countThisTicketAsNew = false;
            if (!dtDsr.Rows[i]["TKH_TKTNO"].ToString().Equals(previousTktNo))
            {
                previousTktNo = dtDsr.Rows[i]["TKH_TKTNO"].ToString();
                countThisTicketAsNew = true;
            }
            
            switch (dtDsr.Rows[i]["TKH_TTYPE"].ToString())
            {
                case "Sales":
                    if (countThisTicketAsNew)
                    {
                        if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("CASH"))
                        {
                            cashSales += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                        }
                        else if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("A/R"))
                        {
                            creditSales += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                        }
                        noOfSalesTickets++;
                        noOfTickets++;
                    }
                    break;
                case "Return":
                    if (countThisTicketAsNew)
                    {
                        if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("CASH"))
                        {
                            cashReturns += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                        }
                        else if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("A/R"))
                        {
                            creditReturns += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                        }
                        noOfReturnsTickets++;
                        noOfTickets++;
                    }
                    break;
                case "Void":
                    if (countThisTicketAsNew)
                    {
                        voidedSales += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                        noOfVoidedTickets++;
                    }
                    break;
            }

            tmpRow["TEXT01"] = dtDsr.Rows[i]["TKH_TKTNO"].ToString();
            tmpRow["TEXT02"] = dtDsr.Rows[i]["TKH_CUSNO"].ToString();
            tmpRow["TEXT03"] = dtDsr.Rows[i]["TKH_CUSNM"].ToString();
            tmpRow["TEXT04"] = Convert.ToDateTime(dtDsr.Rows[i]["TKH_DATE"].ToString()).ToString("dd-MMM-yyyy");
            tmpRow["TEXT05"] = Convert.ToDateTime(dtDsr.Rows[i]["TKH_TIME"].ToString()).ToString("HH:mm");
            tmpRow["TEXT06"] = dtDsr.Rows[i]["TKH_SREP"].ToString();
            tmpRow["TEXT07"] = dtDsr.Rows[i]["TKH_PAYCD"].ToString();
            tmpRow["TEXT08"] = dtDsr.Rows[i]["TKH_CRTBY"].ToString();
            tmpRow["TEXT09"] = dtDsr.Rows[i]["TKH_TTYPE"].ToString();

            tmpRow["TEXT11"] = dtDsr.Rows[i]["TKL_ITMNO"].ToString();
            tmpRow["TEXT12"] = dtDsr.Rows[i]["TKL_DESC"].ToString();
            tmpRow["TEXT13"] = Convert.ToDateTime(dtDsr.Rows[i]["TKL_EXPDT"].ToString()).ToString("dd-MMM-yyyy");
            tmpRow["TEXT14"] = dtDsr.Rows[i]["TKL_UOM"].ToString();


            tmpRow["NUM01"] = Convert.ToDouble(dtDsr.Rows[i]["TKH_NTAMT"].ToString()).ToString("n2");
            tmpRow["NUM02"] = Convert.ToDouble(dtDsr.Rows[i]["TKH_VTAMT"].ToString()).ToString("n2");
            tmpRow["NUM03"] = Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString()).ToString("n2");

            tmpRow["NUM04"] = Convert.ToDouble(dtDsr.Rows[i]["TKL_QTY"].ToString()).ToString("n3");
            tmpRow["NUM05"] = Convert.ToDouble(dtDsr.Rows[i]["TKL_PRICE"].ToString()).ToString("n2");
            tmpRow["NUM06"] = Convert.ToDouble(dtDsr.Rows[i]["TKL_EXPRC"].ToString()).ToString("n2");
            tmpRow["NUM07"] = (Convert.ToDouble(dtDsr.Rows[i]["TKL_VATPRC"].ToString()) - Convert.ToDouble(dtDsr.Rows[i]["TKL_EXPRC"].ToString())).ToString("n2");
            tmpRow["NUM08"] = Convert.ToDouble(dtDsr.Rows[i]["TKL_VATPRC"].ToString()).ToString("n2");

            ReportsDataSet.TEMP_TABLE.Rows.Add(tmpRow);
        }

        DataRow prmRow = ReportsDataSet.PARAM_TABLE.NewRow();

        prmRow["param02"] = START_DATE.Equals("1900-01-01") ? "Earliest" : Convert.ToDateTime(START_DATE).ToString("dd-MMM-yyyy");
        prmRow["param03"] = END_DATE.Equals("9999-12-31") ? "Latest" : Convert.ToDateTime(END_DATE).ToString("dd-MMM-yyyy");

        //prmRow["param04"] = dtDsr.Rows[0]["TKH_SREP"].ToString();
        //prmRow["param05"] = dtDsr.Rows[0]["TKH_CRTBY"].ToString().ToUpper();
        string Routes = string.Join(",", dtDsr.AsEnumerable().Select(r => r.Field<string>("TKH_SREP")).ToArray().Distinct(StringComparer.OrdinalIgnoreCase)).ToUpper();
        string VsUsers = string.Join(",", dtDsr.AsEnumerable().Select(r => r.Field<string>("TKH_CRTBY")).ToArray().Distinct(StringComparer.OrdinalIgnoreCase)).ToUpper();
        prmRow["param04"] = Routes.IndexOf(',') >= 0 ? "Multiple" : Routes;
        prmRow["param05"] = VsUsers.IndexOf(',') >= 0 ? "Multiple" : VsUsers;
        prmRow["param06"] = STRS.IndexOf(',') >= 0 ? "Multiple" : dtDsr.Rows[0]["TKH_STRNO"].ToString();
        prmRow["param07"] = REGS.IndexOf(',') >= 0 ? "Multiple" : dtDsr.Rows[0]["TKH_REGNO"].ToString();
        prmRow["param08"] = Session["UserId"].ToString().ToUpper();
        prmRow["param09"] = dbHlpr.GetRptSeqNo();

        prmRow["param10"] = txtComments.Text.Trim();

        cashNetSales = cashSales + (cashReturns);
        creditNetSales = creditSales + (creditReturns);
        totalSales = cashSales + creditSales;
        totalReturns = cashReturns + (creditReturns);

        //double netSales = cashSales + creditSales + (cashReturns * -1) + (creditReturns * -1);
        //double totalCash = cashSales + (cashReturns * -1) + collections;
        netSales = cashSales + creditSales + (cashReturns) + (creditReturns);

        prmRow["param21"] = cashSales.ToString("n2");
        prmRow["param22"] = creditSales.ToString("n2");
        prmRow["param23"] = cashReturns.ToString("n2");
        prmRow["param24"] = creditReturns.ToString("n2");
        prmRow["param25"] = netSales.ToString("n2");

        prmRow["param28"] = cashNetSales.ToString("n2");
        prmRow["param29"] = creditNetSales.ToString("n2");
        prmRow["param30"] = totalSales.ToString("n2");
        prmRow["param31"] = totalReturns.ToString("n2");

        prmRow["param32"] = voidedSales.ToString("n2");

        prmRow["param41"] = noOfSalesTickets;
        prmRow["param42"] = noOfReturnsTickets;
        prmRow["param43"] = noOfTickets;
        prmRow["param44"] = noOfVoidedTickets;

        ReportsDataSet.PARAM_TABLE.Rows.Add(prmRow);
        ReportsDataSet.RPT_HDR.Rows.Add(ReportsDataSet.RPT_HDR.NewRow());
        return true;
    }

    public void ReportGen()
    {
        try
        {
            // One Way
            ReportDocument RptDoc = new ReportDocument();
            RptDoc.Load(Server.MapPath("~/RptFiles/CR_DRA_MAIN.rpt"));
            RptDoc.SetDataSource(ReportsDataSet);
            RptVwr.ReportSource = RptDoc;
            Session["Rpt"] = RptDoc;
            CrystalDecisions.CrystalReports.Engine.TextObject TXTCOMNTS_BOX = RptDoc.ReportDefinition.ReportObjects["TXT_CMMNT"] as CrystalDecisions.CrystalReports.Engine.TextObject;

            // Second Way
            //ReportClass RptObj = new CR_DRA_MAIN();
            //RptObj.SetDatabaseLogon("sa", "123");
            //RptObj.SetDataSource(ReportsDataSet);
            //RptVwr.ReportSource = RptObj;
            //Session["Rpt"] = RptObj;
            //CrystalDecisions.CrystalReports.Engine.TextObject TXTCOMNTS_BOX = RptObj.ReportDefinition.ReportObjects["TXT_CMMNT"] as CrystalDecisions.CrystalReports.Engine.TextObject;

            TXTCOMNTS_BOX.ObjectFormat.EnableSuppress = txtComments.Text.Trim().Length <= 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void ExportToExcelDevExpress(DataTable dtExport, string filename)
    {
        System.IO.StringWriter tw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
        ASPxGridView dgGrid = new ASPxGridView();
        dgGrid.Theme = "Office2010Black";
        dgGrid.DataSource = dtExport;
        dgGrid.DataBind();

        ASPxGridViewExporter exporter = new ASPxGridViewExporter();
        exporter.GridViewID = dgGrid.ID;
        exporter.WriteXlsToResponse(filename);
    }

    private void ExportToExcelNormal(DataTable dtExport, string filename)
    {
        System.IO.StringWriter tw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
        DataGrid dgGrid = new DataGrid();
        dgGrid.DataSource = dtExport;
        dgGrid.DataBind();

        //Get the HTML for the control.
        dgGrid.RenderControl(hw);
        //Write the HTML back to the browser.
        //Response.ContentType = application/vnd.ms-excel;
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "");
        this.EnableViewState = false;
        Response.Write(tw.ToString());
        Response.End();
    }

    protected void ExportToExcel(DataTable dtExport, string filename)
    {
        GridView GridView1 = new GridView();

        GridView1.RowStyle.BackColor = Color.White;
        GridView1.AlternatingRowStyle.BackColor = Color.White;

        GridView1.DataSource = dtExport;
        GridView1.DataBind();

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=" + filename);
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            //To Export all pages
            GridView1.AllowPaging = false;

            GridView1.HeaderRow.BackColor = Color.White;
            GridView1.HeaderRow.ForeColor = Color.White;

            foreach (TableCell cell in GridView1.HeaderRow.Cells)
            {
                cell.BackColor = Color.FromArgb(198, 89, 17);
                cell.ForeColor = Color.White;

                cell.CssClass = "header-cell";
            }

            foreach (GridViewRow row in GridView1.Rows)
            {
                row.BackColor = Color.White;
                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = Color.FromArgb(221, 235, 247);
                    }
                    else
                    {
                        cell.BackColor = GridView1.RowStyle.BackColor;
                    }
                    cell.CssClass = "body-cell";
                }
            }

            GridView1.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .header-cell { font-family: 'Segoe UI'; font-size: 12pt; font-style: bold; } .body-cell { font-family: 'Segoe UI'; font-size: 12pt; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    private string CommaSeperatedToQuery(string str)
    {
        string[] strArr = str.Split(',');
        string newStr = string.Empty;

        if (strArr.Length > 1)
        {
            for (int i = 0; i < strArr.Length; i++)
            {
                newStr += "'" + strArr[i] + "',";
            }
            newStr = newStr.TrimEnd(',');
            return newStr;
        }
        else
        {
            newStr = "'" + strArr[0] + "'";
            return newStr;
        }
    }

    protected void DraStrNo_Init(object sender, EventArgs e)
    {
        ASPxDropDownEdit ddStores = (ASPxDropDownEdit)sender;
        ASPxListBox lstBxStores = (ASPxListBox)ddStores.FindControl("lstBxStrItems");

        DataTable dtStores = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE WHERE STM_CODE IN (" + Session["userBranchCode"].ToString() + ") ");

        string tempStr = "";
        for (int i = 0; i < dtStores.Rows.Count; i++)
        {
            tempStr += dtStores.Rows[i]["STM_CODE"].ToString() + ",";
        }
        tempStr = tempStr.Trim(',');

        DataRow dr = dtStores.NewRow();
        dr["STM_NAME"] = "All";
        dr["STM_CODE"] = "0";
        dtStores.Rows.InsertAt(dr, 0);

        lstBxStores.DataSource = dtStores;
        lstBxStores.ValueField = "STM_CODE";
        lstBxStores.TextField = "STM_NAME";
        lstBxStores.DataBind();

        ddStores.Value = tempStr;
    }
    protected void DraRegNo_Init(object sender, EventArgs e)
    {
        ASPxDropDownEdit ddRegisters = (ASPxDropDownEdit)sender;
        ASPxListBox lstBxRegisters = (ASPxListBox)ddRegisters.FindControl("lstBxRegItems");

        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + Session["userBranchCode"].ToString() + ") ");

        string tempStr = "";
        for (int i = 0; i < dtRegisters.Rows.Count; i++)
        {
            tempStr += dtRegisters.Rows[i]["REG_CODE"].ToString() + ",";
        }
        tempStr = tempStr.Trim(',');

        DataRow dr = dtRegisters.NewRow();
        dr["REG_NAME"] = "All";
        dr["REG_CODE"] = "0";
        dtRegisters.Rows.InsertAt(dr, 0);

        lstBxRegisters.DataSource = dtRegisters;
        lstBxRegisters.ValueField = "REG_CODE";
        lstBxRegisters.TextField = "REG_NAME";
        lstBxRegisters.DataBind();

        ddRegisters.Value = tempStr;
    }
    protected void StoreChanged(object sender, EventArgs e)
    {
        ASPxListBox lstBxRegisters = (ASPxListBox)DraRegNo.FindControl("lstBxRegItems");

        string stores = "''";
        if (DraStrNo.Value != null)
        {
            stores = DraStrNo.Value.ToString();
        }

        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + stores + ") ");

        string tempStr = "";
        for (int i = 0; i < dtRegisters.Rows.Count; i++)
        {
            tempStr += dtRegisters.Rows[i]["REG_CODE"].ToString() + ",";
        }
        tempStr = tempStr.Trim(',');

        DataRow dr = dtRegisters.NewRow();
        dr["REG_NAME"] = "All";
        dr["REG_CODE"] = "0";
        dtRegisters.Rows.InsertAt(dr, 0);

        lstBxRegisters.DataSource = dtRegisters;
        lstBxRegisters.ValueField = "REG_CODE";
        lstBxRegisters.TextField = "REG_NAME";
        lstBxRegisters.DataBind();

        DraRegNo.Value = tempStr;
    }

}