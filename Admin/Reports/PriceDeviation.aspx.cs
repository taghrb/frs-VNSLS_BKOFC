﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using System.Drawing;
using DevExpress.XtraPrinting;
using DevExpress.Export.Xl;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class Admin_Reports_PriceDeviation : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
    ReportsDS ReportsDataSet = new ReportsDS();
    string START_DATE = "Earliest";
    string END_DATE = "Latest";

    protected void Page_Init(object sender, EventArgs e)
    {
        //DateFrom.Value = new DateTime(1900, 1, 1);
        //DateTo.Value = new DateTime(9999, 12, 31);

        if (Session["Rpt"] != null && IsPostBack)
        {
            RptVwr.ReportSource = Session["Rpt"];
        }
    }

    protected void btnGenReport_Click(object sender, EventArgs e)
    {
        START_DATE = DateFrom.Value != null ? ((DateTime)DateFrom.Value).ToString("yyyy-MM-dd") : new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");
        END_DATE = DateTo.Value != null ? ((DateTime)DateTo.Value).ToString("yyyy-MM-dd") : new DateTime(9999, 12, 31).ToString("yyyy-MM-dd");

        string reasonCodes = ReasonCode.Value != null ? CommaSeperatedToQuery(ReasonCode.Text.ToString()) : "";
        string items = Items.Value != null ? CommaSeperatedToQuery(Items.Text.ToString()) : "";
        string catgs = Catg.Value != null ? CommaSeperatedToQuery(Catg.Text.ToString()) : "";
        string subcatgs = SubCatg.Value != null ? CommaSeperatedToQuery(SubCatg.Text.ToString()) : "";
        string udf1s = Udf1.Value != null ? CommaSeperatedToQuery(Udf1.Text.ToString()) : "";
        string udf2s = Udf2.Value != null ? CommaSeperatedToQuery(Udf2.Text.ToString()) : "";
        string udf3s = Udf3.Value != null ? CommaSeperatedToQuery(Udf3.Text.ToString()) : "";
        string udf4s = Udf4.Value != null ? CommaSeperatedToQuery(Udf4.Text.ToString()) : "";

        string customers = CustNo.Value != null ? CommaSeperatedToQuery(CustNo.Text.ToString()) : "";
        string cuscatgs = CustCatg.Value != null ? CommaSeperatedToQuery(CustCatg.Text.ToString()) : "";
        string cusgroups = CustGroup.Value != null ? CommaSeperatedToQuery(CustGroup.Text.ToString()) : "";

        string stores = CommaSeperatedToQuery(StrNo.Value.ToString());
        string registers = CommaSeperatedToQuery(RegNo.Value.ToString());

        string WHERE_FILTER = "";
        if (TicketType.Value != null && TicketType.Value.ToString().Length > 0)
        {
            WHERE_FILTER += " AND TKL_TTYPE = '" + TicketType.Value.ToString() + "' ";
        }

        if (PayCode.Value != null && PayCode.Value.ToString().Length > 0)
        {
            WHERE_FILTER += " AND TKL_PAYCD = '" + PayCode.Value.ToString() + "' ";
        }

        if (stores.Length > 0)
        {
            WHERE_FILTER += " AND TKL_STRNO IN (" + stores + ") ";
        }

        if (registers.Length > 0)
        {
            WHERE_FILTER += " AND TKL_REGNO IN (" + registers + ") ";
        }

        if (items.Length > 0)
        {
            WHERE_FILTER += " AND TKL_ITMNO IN (" + items + ") ";
        }

        if (catgs.Length > 0)
        {
            WHERE_FILTER += " AND PRO_CATG IN (" + catgs + ") ";
        }

        if (subcatgs.Length > 0)
        {
            WHERE_FILTER += " AND PRO_SCATG IN (" + subcatgs + ") ";
        }

        if (udf1s.Length > 0)
        {
            WHERE_FILTER += " AND PRO_UDF1 IN (" + udf1s + ") ";
        }

        if (udf2s.Length > 0)
        {
            WHERE_FILTER += " AND PRO_UDF2 IN (" + udf2s + ") ";
        }

        if (udf3s.Length > 0)
        {
            WHERE_FILTER += " AND PRO_UDF3 IN (" + udf3s + ") ";
        }

        if (udf4s.Length > 0)
        {
            WHERE_FILTER += " AND PRO_UDF4 IN (" + udf4s + ") ";
        }

        if (reasonCodes.Length > 0)
        {
            WHERE_FILTER += " AND TKL_PCRSN IN (" + reasonCodes + ") ";
        }

        if (customers.Length > 0)
        {
            WHERE_FILTER += " AND TKL_CUSNO IN (" + customers + ") ";
        }

        if (cuscatgs.Length > 0)
        {
            WHERE_FILTER += " AND CUS_CATG IN (" + cuscatgs + ") ";
        }

        if (cusgroups.Length > 0)
        {
            WHERE_FILTER += " AND CUS_GRPCD IN (" + cusgroups + ") ";
        }

        string qry = "";

        if (RptFormat.Value == "Flash")
        {
            qry = GetFlashQuery(WHERE_FILTER);
        }
        else if (RptFormat.Value == "Brief")
        {
            if (RptRecapBy.Value == "StrByVan")
            {
                qry = GetBriefQuery(WHERE_FILTER, " TKL_STRNO, STM_NAME, TKL_REGNO ");
            }
            else if (RptRecapBy.Value == "ItmByStr")
            {
                qry = GetBriefQuery(WHERE_FILTER, " TKL_ITMNO, TKL_DESC, TKL_STRNO, STM_NAME ");
            }
        }
        else if (RptFormat.Value == "Detail")
        {
            qry = GetDetailQuery(WHERE_FILTER);
        }

        DataTable dtExport = dbHlpr.FetchData(qry);

        if (dtExport.Rows.Count > 0)
        {
            lblErrorMessage.Text = "";
            if (RptFormat.Value == "Flash")
            {
                KeyValuePair<int, string>[] colTitles = new KeyValuePair<int, string>[6];
                colTitles[0] = new KeyValuePair<int, string>(0, "Store#");
                colTitles[1] = new KeyValuePair<int, string>(1, "Name");
                colTitles[2] = new KeyValuePair<int, string>(2, "L.Price Amount");
                colTitles[3] = new KeyValuePair<int, string>(3, "Sales Amount");
                colTitles[4] = new KeyValuePair<int, string>(4, "Difference");
                colTitles[5] = new KeyValuePair<int, string>(5, "Diff %");

                KeyValuePair<int, string>[] numericColFormats = new KeyValuePair<int, string>[4];
                numericColFormats[0] = new KeyValuePair<int, string>(2, "n2");
                numericColFormats[1] = new KeyValuePair<int, string>(3, "n2");
                numericColFormats[2] = new KeyValuePair<int, string>(4, "n2");
                numericColFormats[3] = new KeyValuePair<int, string>(5, "n2");

                ExportFormattedExcel(dtExport,
                    "Price Deviation Report - Flash",
                    "Period : " + START_DATE + " - " + END_DATE,
                    colTitles,
                    numericColFormats);
            }
            else if (RptFormat.Value == "Brief")
            {
                if (RptRecapBy.Value == "StrByVan")
                {
                    KeyValuePair<int, string>[] colTitles = new KeyValuePair<int, string>[7];
                    colTitles[0] = new KeyValuePair<int, string>(0, "Store#");
                    colTitles[1] = new KeyValuePair<int, string>(1, "Name");
                    colTitles[2] = new KeyValuePair<int, string>(2, "VAN#");
                    colTitles[3] = new KeyValuePair<int, string>(3, "L.Price Amount");
                    colTitles[4] = new KeyValuePair<int, string>(4, "Sales Amount");
                    colTitles[5] = new KeyValuePair<int, string>(5, "Difference");
                    colTitles[6] = new KeyValuePair<int, string>(6, "Diff %");


                    KeyValuePair<int, string>[] numericColFormats = new KeyValuePair<int, string>[4];
                    numericColFormats[0] = new KeyValuePair<int, string>(3, "n2");
                    numericColFormats[1] = new KeyValuePair<int, string>(4, "n2");
                    numericColFormats[2] = new KeyValuePair<int, string>(5, "n2");
                    numericColFormats[3] = new KeyValuePair<int, string>(6, "n2");

                    ExportFormattedExcel(dtExport,
                        "Price Deviation Report - Brief - By Store by VAN",
                        "Period : " + START_DATE + " - " + END_DATE,
                        colTitles,
                        numericColFormats);
                }
                else if (RptRecapBy.Value == "ItmByStr")
                {
                    KeyValuePair<int, string>[] colTitles = new KeyValuePair<int, string>[8];
                    colTitles[0] = new KeyValuePair<int, string>(0, "Item#");
                    colTitles[1] = new KeyValuePair<int, string>(1, "Description");
                    colTitles[2] = new KeyValuePair<int, string>(2, "Store#");
                    colTitles[3] = new KeyValuePair<int, string>(3, "Name");
                    colTitles[4] = new KeyValuePair<int, string>(4, "L.Price Amount");
                    colTitles[5] = new KeyValuePair<int, string>(5, "Sales Amount");
                    colTitles[6] = new KeyValuePair<int, string>(6, "Difference");
                    colTitles[7] = new KeyValuePair<int, string>(7, "Diff %");


                    KeyValuePair<int, string>[] numericColFormats = new KeyValuePair<int, string>[4];
                    numericColFormats[0] = new KeyValuePair<int, string>(4, "n2");
                    numericColFormats[1] = new KeyValuePair<int, string>(5, "n2");
                    numericColFormats[2] = new KeyValuePair<int, string>(6, "n2");
                    numericColFormats[3] = new KeyValuePair<int, string>(7, "n2");

                    ExportFormattedExcel(dtExport,
                        "Price Deviation Report - Brief - By Item By Store",
                        "Period : " + START_DATE + " - " + END_DATE,
                        colTitles,
                        numericColFormats);
                }
            }
            else if (RptFormat.Value == "Detail")
            {
                KeyValuePair<int, string>[] colTitles = new KeyValuePair<int, string>[20];
                colTitles[0] = new KeyValuePair<int, string>(0, "Ticket#");
                colTitles[1] = new KeyValuePair<int, string>(1, "Store");
                colTitles[2] = new KeyValuePair<int, string>(2, "Branch Name");
                colTitles[3] = new KeyValuePair<int, string>(3, "Reg");
                colTitles[4] = new KeyValuePair<int, string>(4, "Item");
                colTitles[5] = new KeyValuePair<int, string>(5, "Description");
                colTitles[6] = new KeyValuePair<int, string>(6, "UoM");
                colTitles[7] = new KeyValuePair<int, string>(7, "QTY");
                colTitles[8] = new KeyValuePair<int, string>(8, "Item Price");
                colTitles[9] = new KeyValuePair<int, string>(9, "Sales Price");
                colTitles[10] = new KeyValuePair<int, string>(10, "List Price");
                colTitles[11] = new KeyValuePair<int, string>(11, "Ext. Sales Price");
                colTitles[12] = new KeyValuePair<int, string>(12, "Diff");
                colTitles[13] = new KeyValuePair<int, string>(13, "%");
                colTitles[14] = new KeyValuePair<int, string>(14, "Price w/ VAT");
                colTitles[15] = new KeyValuePair<int, string>(15, "Cust#");
                colTitles[16] = new KeyValuePair<int, string>(16, "Customer Name");
                colTitles[17] = new KeyValuePair<int, string>(17, "Type");
                colTitles[18] = new KeyValuePair<int, string>(18, "Reason");
                colTitles[19] = new KeyValuePair<int, string>(19, "Paycode");

                KeyValuePair<int, string>[] numericColFormats = new KeyValuePair<int, string>[8];
                numericColFormats[0] = new KeyValuePair<int, string>(7, "n3");
                numericColFormats[1] = new KeyValuePair<int, string>(8, "n2");
                numericColFormats[2] = new KeyValuePair<int, string>(9, "n2");
                numericColFormats[3] = new KeyValuePair<int, string>(10, "n2");
                numericColFormats[4] = new KeyValuePair<int, string>(11, "n2");
                numericColFormats[5] = new KeyValuePair<int, string>(12, "n2");
                numericColFormats[6] = new KeyValuePair<int, string>(13, "n2");
                numericColFormats[7] = new KeyValuePair<int, string>(14, "n2");

                ExportFormattedExcel(dtExport,
                    "Price Deviation Report - Detail",
                    "Period : " + START_DATE + " - " + END_DATE,
                    colTitles,
                    numericColFormats);
            }
        }
        else
        {
            lblErrorMessage.Text = "No Record Found.";
            RptVwr.ReportSource = null;
            Session["Rpt"] = null;
        }
    }

    private string GetDetailQuery(string WHERE_FILTER)
    {
        string q = GetMainQuery(WHERE_FILTER);

        return q;
    }

    private string GetBriefQuery(string WHERE_FILTER, string GroupBy)
    {
        string mainQry = GetMainQuery(WHERE_FILTER);

        string q = "SELECT "
            + GroupBy + " , "
            + " SUM(LISTPRICE) AS LISTPRICE, SUM(TKL_EXPRC) AS TKL_EXPRC, SUM(Diff) AS Diff, "
            + " CASE WHEN SUM(LISTPRICE) <> 0 THEN (SUM(Diff) / SUM(LISTPRICE)) * 100 ELSE 0 END AS '%' "
            + " FROM ( "
            + mainQry
            + " ) tbl "
            + " GROUP BY " + GroupBy
            + " ORDER BY " + GroupBy;

        return q;
    }
    
    private string GetFlashQuery(string WHERE_FILTER)
    {
        string mainQry = GetMainQuery(WHERE_FILTER);

        string q = "SELECT "
            + " TKL_STRNO, STM_NAME, "
            + " SUM(LISTPRICE) AS LISTPRICE, SUM(TKL_EXPRC) AS TKL_EXPRC, SUM(Diff) AS Diff, "
            + " CASE WHEN SUM(LISTPRICE) <> 0 THEN (SUM(Diff) / SUM(LISTPRICE)) * 100 ELSE 0 END AS '%' "
            + " FROM ( "
            + mainQry
            + " ) tbl "
            + " GROUP BY TKL_STRNO, STM_NAME "
            + " ORDER BY TKL_STRNO ";

        return q;
    }

    private string GetMainQuery(string WHERE_FILTER)
    {
        string q = "SELECT "
                + " TKL_TKTNO, TKL_STRNO, STM_NAME, TKL_REGNO, "
                + " TKL_ITMNO, TKL_DESC, TKL_UOM, TKL_QTY, TKL_ITMPR, TKL_PRICE, "
                + " TKL_QTY * TKL_ITMPR AS LISTPRICE, TKL_EXPRC, (TKL_QTY * TKL_ITMPR) - TKL_EXPRC AS Diff, "
                + " CASE WHEN (TKL_QTY * TKL_ITMPR) <> 0 THEN (((TKL_QTY * TKL_ITMPR) - TKL_EXPRC) / (TKL_QTY * TKL_ITMPR)) * 100 ELSE 0 END AS '%', "
                + " TKL_VATPRC, TKL_CUSNO, TKL_CUSNM, TKL_TTYPE, TKL_PCRSN, TKL_PAYCD "
                + " FROM SLS_TRX_TLIN "
                + " LEFT JOIN STP_MSTR_STORE ON TKL_STRNO = STM_CODE "
                + " LEFT JOIN INV_MSTR_PRODT ON TKL_ITMNO = PRO_CODE "
                + " LEFT JOIN STP_MST_CSTMR ON TKL_CUSNO = CUS_NO "
                + " WHERE TKL_ITMPR <> TKL_PRICE "
                + " AND TKL_DATE BETWEEN '" + START_DATE + "' AND CONVERT(datetime, '" + END_DATE + " 23:59:59.998') ";
        q += WHERE_FILTER;

        return q;
    }

    public void ExportFormattedExcel(DataTable dt, string title, string subTitle, KeyValuePair<int, string>[] colCaptions, KeyValuePair<int, string>[] colFormats)
    {
        string FileName = Guid.NewGuid().ToString();

        ASPxGridView gvTemplateToExport = new ASPxGridView();
        gvTemplateToExport.ID = FileName;
        gvTemplateToExport.AutoGenerateColumns = true;

        //gvTemplateToExport.SettingsText.Title = title;
        //gvTemplateToExport.Settings.ShowTitlePanel = true;

        this.Controls.Add(gvTemplateToExport);

        gvTemplateToExport.DataSource = dt;
        gvTemplateToExport.DataBind();

        for (int i = 0; i < colCaptions.Length; i++)
        {
            ((GridViewDataTextColumn)gvTemplateToExport.Columns[colCaptions[i].Key]).Caption = colCaptions[i].Value;
        }

        for (int i = 0; i < colFormats.Length; i++)
        {
            ((GridViewDataTextColumn)gvTemplateToExport.Columns[colFormats[i].Key]).PropertiesTextEdit.DisplayFormatString = colFormats[i].Value;
        }

        dbHlpr.ExportToFormattedXls1997(this, gvTemplateToExport);
        /*string filePath = Server.MapPath(string.Format("{0}/{1}.{2}", "~/App_Data/", FileName, "xls"));
        dbHlpr.ExportToFormattedXls1997(this, gvTemplateToExport, filePath, title, subTitle);
        string downloadWithName = "iDR-Web-EoD-Stock-Status.xls";
        ForceDownloadXlsFile(filePath, downloadWithName);*/

        this.Controls.Remove(gvTemplateToExport);
    }

    private void ExportToExcelDevExpress(DataTable dtExport, string filename)
    {
        System.IO.StringWriter tw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
        ASPxGridView dgGrid = new ASPxGridView();
        dgGrid.Theme = "Office2010Black";
        dgGrid.DataSource = dtExport;
        dgGrid.DataBind();

        ASPxGridViewExporter exporter = new ASPxGridViewExporter();
        exporter.GridViewID = dgGrid.ID;
        exporter.WriteXlsToResponse(filename);
    }

    private void ExportToExcelNormal(DataTable dtExport, string filename)
    {
        System.IO.StringWriter tw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
        DataGrid dgGrid = new DataGrid();
        dgGrid.DataSource = dtExport;
        dgGrid.DataBind();

        //Get the HTML for the control.
        dgGrid.RenderControl(hw);
        //Write the HTML back to the browser.
        //Response.ContentType = application/vnd.ms-excel;
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "");
        this.EnableViewState = false;
        Response.Write(tw.ToString());
        Response.End();
    }

    protected void ExportToExcel(DataTable dtExport, string filename)
    {
        GridView GridView1 = new GridView();

        GridView1.RowStyle.BackColor = Color.White;
        GridView1.AlternatingRowStyle.BackColor = Color.White;

        GridView1.DataSource = dtExport;
        GridView1.DataBind();

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=" + filename);
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            //To Export all pages
            GridView1.AllowPaging = false;

            GridView1.HeaderRow.BackColor = Color.White;
            GridView1.HeaderRow.ForeColor = Color.White;

            foreach (TableCell cell in GridView1.HeaderRow.Cells)
            {
                cell.BackColor = Color.FromArgb(198, 89, 17);
                cell.ForeColor = Color.White;

                cell.CssClass = "header-cell";
            }

            foreach (GridViewRow row in GridView1.Rows)
            {
                row.BackColor = Color.White;
                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = Color.FromArgb(221, 235, 247);
                    }
                    else
                    {
                        cell.BackColor = GridView1.RowStyle.BackColor;
                    }
                    cell.CssClass = "body-cell";
                }
            }

            GridView1.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .header-cell { font-family: 'Segoe UI'; font-size: 12pt; font-style: bold; } .body-cell { font-family: 'Segoe UI'; font-size: 12pt; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    private string CommaSeperatedToQuery(string str)
    {
        string[] strArr = str.Split(',');
        string newStr = string.Empty;

        if (strArr.Length > 1)
        {
            for (int i = 0; i < strArr.Length; i++)
            {
                newStr += "'" + strArr[i] + "',";
            }
            newStr = newStr.TrimEnd(',');
            return newStr;
        }
        else
        {
            newStr = "'" + strArr[0] + "'";
            return newStr;
        }
    }

    protected void StrNo_Init(object sender, EventArgs e)
    {
        ASPxDropDownEdit ddStores = (ASPxDropDownEdit)sender;
        ASPxListBox lstBxStores = (ASPxListBox)ddStores.FindControl("lstBxStrItems");

        DataTable dtStores = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE WHERE STM_CODE IN (" + Session["userBranchCode"].ToString() + ") ");

        string tempStr = "";
        for (int i = 0; i < dtStores.Rows.Count; i++)
        {
            tempStr += dtStores.Rows[i]["STM_CODE"].ToString() + ",";
        }
        tempStr = tempStr.Trim(',');

        DataRow dr = dtStores.NewRow();
        dr["STM_NAME"] = "All";
        dr["STM_CODE"] = "0";
        dtStores.Rows.InsertAt(dr, 0);

        lstBxStores.DataSource = dtStores;
        lstBxStores.ValueField = "STM_CODE";
        lstBxStores.TextField = "STM_NAME";
        lstBxStores.DataBind();

        ddStores.Value = tempStr;
    }
    protected void RegNo_Init(object sender, EventArgs e)
    {
        ASPxDropDownEdit ddRegisters = (ASPxDropDownEdit)sender;
        ASPxListBox lstBxRegisters = (ASPxListBox)ddRegisters.FindControl("lstBxRegItems");

        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + Session["userBranchCode"].ToString() + ") ");

        string tempStr = "";
        for (int i = 0; i < dtRegisters.Rows.Count; i++)
        {
            tempStr += dtRegisters.Rows[i]["REG_CODE"].ToString() + ",";
        }
        tempStr = tempStr.Trim(',');

        DataRow dr = dtRegisters.NewRow();
        dr["REG_NAME"] = "All";
        dr["REG_CODE"] = "0";
        dtRegisters.Rows.InsertAt(dr, 0);

        lstBxRegisters.DataSource = dtRegisters;
        lstBxRegisters.ValueField = "REG_CODE";
        lstBxRegisters.TextField = "REG_NAME";
        lstBxRegisters.DataBind();

        ddRegisters.Value = tempStr;
    }
    protected void StoreChanged(object sender, EventArgs e)
    {
        ASPxListBox lstBxRegisters = (ASPxListBox)RegNo.FindControl("lstBxRegItems");

        string stores = "''";
        if (StrNo.Value != null)
        {
            stores = StrNo.Value.ToString();
        }

        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + stores + ") ");

        string tempStr = "";
        for (int i = 0; i < dtRegisters.Rows.Count; i++)
        {
            tempStr += dtRegisters.Rows[i]["REG_CODE"].ToString() + ",";
        }
        tempStr = tempStr.Trim(',');

        DataRow dr = dtRegisters.NewRow();
        dr["REG_NAME"] = "All";
        dr["REG_CODE"] = "0";
        dtRegisters.Rows.InsertAt(dr, 0);

        lstBxRegisters.DataSource = dtRegisters;
        lstBxRegisters.ValueField = "REG_CODE";
        lstBxRegisters.TextField = "REG_NAME";
        lstBxRegisters.DataBind();

        RegNo.Value = tempStr;
    }

    protected void RptFormat_SelectedIndexChanged(object sender, EventArgs e)
    {
        switch (RptFormat.SelectedIndex)
        {
            case 1:
                RptRecapBy.ClientEnabled = true;
                break;
            default:
                RptRecapBy.ClientEnabled = false;
                break;
        }
    }

    protected void Items_Init(object sender, EventArgs e)
    {
        ASPxGridLookup ddItems = (ASPxGridLookup)sender;

        DataTable dtItems = dbHlpr.FetchData("SELECT PRO_CODE, PRO_DESC1  FROM INV_MSTR_PRODT ORDER BY PRO_CODE ");

        ddItems.DataSource = dtItems;
        ddItems.DataBind();
    }
}