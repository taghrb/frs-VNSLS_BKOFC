﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using System.Drawing;
using DevExpress.XtraPrinting;
using DevExpress.Export.Xl;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class Admin_Reports_SalesByProduct : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
    ReportsDS ReportsDataSet = new ReportsDS();
    string START_DATE = "Earliest";
    string END_DATE = "Latest";

    protected void Page_Init(object sender, EventArgs e)
    {
        //DateFrom.Value = new DateTime(1900, 1, 1);
        //DateTo.Value = new DateTime(9999, 12, 31);

        if (Session["Rpt"] != null && IsPostBack)
        {
            RptVwr.ReportSource = Session["Rpt"];
        }
    }

    protected void btnExportSalesByProduct_Click(object sender, EventArgs e)
    {
        START_DATE = DateFrom.Value != null ? ((DateTime)DateFrom.Value).ToString("yyyy-MM-dd") : new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");
        END_DATE = DateTo.Value != null ? ((DateTime)DateTo.Value).ToString("yyyy-MM-dd") : new DateTime(9999, 12, 31).ToString("yyyy-MM-dd");

        string oneStore = StrNo.Value.ToString();
        string oneRegister = RegNo.Value.ToString();

        DataTable SLSHdrDT = new DataTable();
        DataTable SLSLinDT = new DataTable();
        DataTable PayRcptsLinDT1 = new DataTable();


        SLSHdrDT = dbHlpr.FetchData("SELECT "
            + " TKH_TKTNO, TKH_CUSNO, TKH_CUSNM, TKH_CUSVT, TKH_SREP,TKH_DATE, TKH_TIME, TKH_CRTDT, TKH_CRTIM, TKH_CRTBY, "
            + " TKH_SLAMT,TKH_DSCNT, TKH_NTAMT, TKH_VTAMT, TKH_AMT, TKH_AMTPD, TKH_PAYCD, TKH_CHNGE, TKH_BLANC, TKH_TTYPE "
            + " FROM SLS_TRX_THDR "
            + " WHERE TKH_STRNO = '" + oneStore + "' "
            + " AND TKH_REGNO = '" + oneRegister + "' "
            + " AND TKH_DATE BETWEEN '" + START_DATE + "' AND CONVERT(datetime, '" + END_DATE + " 23:59:59.998') ");
        SLSLinDT = dbHlpr.FetchData("SELECT "
                        + " COUNT(TKL_TKTNO) AS TKL_TKTNO, TKL_ITMNO, TKL_DESC, TKL_UOM, "
                        + " SUM(TKL_QTY) AS TKL_QTY, SUM(TKL_DSCNT) AS TKL_DSCNT, SUM(TKL_EXPRC) AS TKL_EXPRC, "
                        + " SUM(TKL_VATPRC) AS TKL_VATPRC, MAX(TKL_SREP) AS TKL_SREP, MAX(TKL_DATE) AS TKL_DATE, MAX(TKL_TTYPE) AS TKL_TTYPE "
                        + " FROM SLS_TRX_TLIN  "
                        + " WHERE TKL_TTYPE <> 'Void' AND TKL_STRNO = '" + oneStore + "' AND TKL_REGNO = '" + oneRegister + "' "
                        + " AND TKL_DATE BETWEEN '" + START_DATE + "' AND CONVERT(datetime, '" + END_DATE + " 23:59:59.998') "
                        + " GROUP BY TKL_ITMNO, TKL_DESC, TKL_UOM ");
        if (SLSHdrDT.Rows.Count <= 0 || SLSLinDT.Rows.Count <= 0)
        {
            lblErrorMessage.Text = "No Record Found.";
            RptVwr.ReportSource = null;
            Session["Rpt"] = null;
        }
        else
        {
            lblErrorMessage.Text = "";
            
            ReportTempData(SLSHdrDT, 1);
            ReportTempData(SLSLinDT, 2);

            ReportGen();
        }
    }

    public void ReportTempData(DataTable DTTemptbl, int A)
    {
        if (A == 2)
        {
            for (int i = 0; i < DTTemptbl.Rows.Count; i++)
            {
                DataRow dr = ReportsDataSet.TEMP_TABLE.NewRow();   //Tables["TEMP_TABLE"]. 

                if (DTTemptbl.Rows[i]["TKL_ITMNO"] != null && DTTemptbl.Rows[i]["TKL_ITMNO"] != "")
                {
                    dr["Text02"] = DTTemptbl.Rows[i]["TKL_ITMNO"].ToString(); // Item No
                    string ProdDesc2 = ProdDescDetails(DTTemptbl.Rows[i]["TKL_ITMNO"].ToString());
                    dr["Text11"] = ProdDesc2;
                    dr["Text11"] = "";
                    string ProdCatg = ProdCatgDetails(DTTemptbl.Rows[i]["TKL_ITMNO"].ToString());
                    dr["Text09"] = ProdCatg; // Product Category
                }
                else
                {
                    dr["Text02"] = "";
                    dr["Text11"] = "";
                }


                if (DTTemptbl.Rows[i]["TKL_DESC"] != null && DTTemptbl.Rows[i]["TKL_DESC"] != "")
                {
                    dr["Text03"] = DTTemptbl.Rows[i]["TKL_DESC"].ToString(); // PROD Catg
                }
                else
                {
                    dr["Text03"] = "";
                }
                if (DTTemptbl.Rows[i]["TKL_UOM"] != null && DTTemptbl.Rows[i]["TKL_UOM"] != "")
                {
                    dr["Text04"] = DTTemptbl.Rows[i]["TKL_UOM"].ToString(); // UOM
                }
                else
                {
                    dr["Text04"] = "";
                }


                if (DTTemptbl.Rows[i]["TKL_SREP"] != null && DTTemptbl.Rows[i]["TKL_SREP"] != "")
                {
                    dr["Text07"] = DTTemptbl.Rows[i]["TKL_SREP"].ToString().ToUpper(); // SLS rep
                }
                else
                {
                    dr["Text07"] = "";
                }
                if (DTTemptbl.Rows[i]["TKL_DATE"] != null && DTTemptbl.Rows[i]["TKL_DATE"] != "")
                {
                    dr["Text08"] = DTTemptbl.Rows[i]["TKL_DATE"].ToString(); // Date
                }
                else
                {
                    dr["Text08"] = "";
                }
                if (DTTemptbl.Rows[i]["TKL_TTYPE"] != null && DTTemptbl.Rows[i]["TKL_TTYPE"] != "")
                {
                    dr["Text10"] = DTTemptbl.Rows[i]["TKL_TTYPE"].ToString(); // TRX Type
                }
                else
                {
                    dr["Text10"] = "";
                }

                /////////////// num starts here /////////////////

                if (DTTemptbl.Rows[i]["TKL_QTY"] != null && DTTemptbl.Rows[i]["TKL_QTY"] != "")
                {
                    dr["Num01"] = DTTemptbl.Rows[i]["TKL_QTY"].ToString();// QTY
                }
                else
                {
                    dr["Num01"] = "0.00";
                }

                if (DTTemptbl.Rows[i]["TKL_EXPRC"] != null && DTTemptbl.Rows[i]["TKL_EXPRC"] != "")
                {
                    dr["Num03"] = DTTemptbl.Rows[i]["TKL_EXPRC"].ToString();// Ex Price
                }
                else
                {
                    dr["Num03"] = "0.00";
                }
                if (DTTemptbl.Rows[i]["TKL_VATPRC"] != null && DTTemptbl.Rows[i]["TKL_VATPRC"] != "")
                {
                    dr["Num04"] = DTTemptbl.Rows[i]["TKL_VATPRC"].ToString();// VAT Price
                }
                else
                {
                    dr["Num04"] = "0.00";
                }

                if (DTTemptbl.Rows[i]["TKL_TKTNO"] != null && DTTemptbl.Rows[i]["TKL_TKTNO"] != "")
                {
                    dr["Num05"] = DTTemptbl.Rows[i]["TKL_TKTNO"].ToString();// VAT Price
                }
                else
                {
                    dr["Num05"] = "0.00";
                }
                ReportsDataSet.TEMP_TABLE.Rows.Add(dr);
            }
        }
        else
        {
            /////////////////////////////////////////////////////
            ////////// Header Param table start here ////////////
            /////////////////////////////////////////////////////
            DataRow dr = ReportsDataSet.PARAM_TABLE.NewRow();   //Tables["TEMP_TABLE"]. 
            //Company Details starts
            dr["param11"] = "Forsan Foods & Consumer Products Co. Ltd";
            dr["param12"] = "الغذائىة الفرسان - مبيعات فان";
            dr["param13"] = "VAT # 300056053500003 ";
            dr["param14"] = "Pri. Sultan Abdul Aziz St ,Riyadh";
            dr["param15"] = "Tel # (011) 416-4422";
            dr["param16"] = "Mobile # 055-816-1122";

            if (DTTemptbl.Rows[0]["TKH_TKTNO"] != null && DTTemptbl.Rows[0]["TKH_TKTNO"] != "")
            {
                dr["param01"] = DTTemptbl.Rows[0]["TKH_TKTNO"].ToString(); // Ticket Code
            }
            else
            {
                dr["param01"] = "";
            }
            if (DTTemptbl.Rows[0]["TKH_SREP"] != null && DTTemptbl.Rows[0]["TKH_SREP"] != "")
            {
                dr["param05"] = DTTemptbl.Rows[0]["TKH_SREP"].ToString().ToUpper(); // Sales Rep
            }
            else
            {
                dr["param05"] = "";
            }
            if (DTTemptbl.Rows[0]["TKH_DATE"] != null && DTTemptbl.Rows[0]["TKH_DATE"] != "")
            {
                DateTime DTtmp = Convert.ToDateTime(DTTemptbl.Rows[0]["TKH_DATE"].ToString());
                dr["param06"] = DTtmp.ToString("dd-MMM-yyyy, HH:mm"); // Date                   
            }
            else
            {
                dr["param06"] = "";
            }
            if (DTTemptbl.Rows[0]["TKH_TIME"] != null && DTTemptbl.Rows[0]["TKH_TIME"] != "")
            {
                dr["param07"] = DTTemptbl.Rows[0]["TKH_TIME"].ToString(); // Time
            }
            else
            {
                dr["param07"] = "";
            }
            if (DTTemptbl.Rows[0]["TKH_SLAMT"] != null && DTTemptbl.Rows[0]["TKH_SLAMT"] != "")
            {
                dr["param31"] = DTTemptbl.Rows[0]["TKH_SLAMT"].ToString(); // sales amount
            }
            else
            {
                dr["param31"] = "";
            }

            if (DTTemptbl.Rows[0]["TKH_DSCNT"] != null && DTTemptbl.Rows[0]["TKH_DSCNT"] != "")
            {
                dr["param32"] = DTTemptbl.Rows[0]["TKH_DSCNT"].ToString(); //Discount
            }
            else
            {
                dr["param32"] = "";
            }

            if (DTTemptbl.Rows[0]["TKH_NTAMT"] != null && DTTemptbl.Rows[0]["TKH_NTAMT"] != "")
            {
                dr["param33"] = DTTemptbl.Rows[0]["TKH_NTAMT"].ToString();// net amount
            }
            else
            {
                dr["param33"] = "0.00";
            }


            if (DTTemptbl.Rows[0]["TKH_VTAMT"] != null && DTTemptbl.Rows[0]["TKH_VTAMT"] != "")
            {
                dr["param34"] = DTTemptbl.Rows[0]["TKH_VTAMT"].ToString(); // VAT Amount
            }
            else
            {
                dr["param34"] = "";
            }

            if (DTTemptbl.Rows[0]["TKH_AMT"] != null && DTTemptbl.Rows[0]["TKH_AMT"] != "")
            {
                dr["param35"] = DTTemptbl.Rows[0]["TKH_AMT"].ToString();// Total Amount
            }
            else
            {
                dr["param35"] = "0.00";
            }
            if (DTTemptbl.Rows[0]["TKH_AMTPD"] != null && DTTemptbl.Rows[0]["TKH_AMTPD"] != "")
            {
                dr["param36"] = DTTemptbl.Rows[0]["TKH_AMTPD"].ToString();// Amount Paid
            }
            else
            {
                dr["param36"] = "0.00";
            }
            if (DTTemptbl.Rows[0]["TKH_CHNGE"] != null && DTTemptbl.Rows[0]["TKH_CHNGE"] != "")
            {
                dr["param37"] = DTTemptbl.Rows[0]["TKH_CHNGE"].ToString();// Change Due
            }
            else
            {
                dr["param37"] = "0.00";
            }//TKH_BLANC
            if (DTTemptbl.Rows[0]["TKH_BLANC"] != null && DTTemptbl.Rows[0]["TKH_BLANC"] != "")
            {
                dr["param38"] = DTTemptbl.Rows[0]["TKH_BLANC"].ToString();// Balance Due
            }
            else
            {
                dr["param38"] = "0.00";
            }//TKH_BLANC
            //if (DTTemptbl.Rows[0]["TKL_TXFCT"] != null && DTTemptbl.Rows[0]["TKL_TXFCT"] != "")
            //{
            //    dr["param39"] = DTTemptbl.Rows[0]["TKL_TXFCT"].ToString();// Balance Due
            //}
            //else
            //{
            dr["param39"] = "5.00";
            //}//TKH_Tax Factor

            if (DTTemptbl.Rows[0]["TKH_TTYPE"] != null && DTTemptbl.Rows[0]["TKH_TTYPE"] != "")
            {
                dr["param08"] = DTTemptbl.Rows[0]["TKH_TTYPE"].ToString();// TYPE
            }
            else
            {
                dr["param08"] = "";
            }
            dr["param20"] = DTTemptbl.Rows.Count;
            ReportsDataSet.PARAM_TABLE.Rows.Add(dr);

        }

    }//end method

    public string ProdDescDetails(string ProdCode)
    {
        DataTable dtProdDesc2 = dbHlpr.FetchData("SELECT PRO_CODE, PRO_DESC1,PRO_DESC2, PRO_UOM, PRO_PRICE FROM INV_MSTR_PRODT "
           + "  WHERE PRO_CODE='" + ProdCode + "'");
        if (dtProdDesc2.Rows.Count < 1)
            return "";
        else
            return dtProdDesc2.Rows[0]["PRO_DESC2"].ToString();
    }

    public string ProdCatgDetails(string ProdCode)
    {
        DataTable dtProdCatg = dbHlpr.FetchData("SELECT PRO_CATG , PRO_SCATG FROM INV_MSTR_PRODT "
           + "  WHERE PRO_CODE='" + ProdCode + "'");
        if (dtProdCatg.Rows.Count < 1)
            return "";
        else
            return dtProdCatg.Rows[0]["PRO_CATG"].ToString(); // +" / " + dtProdCatg.Rows[0]["PRO_SCATG"].ToString();
    }

    public void ReportGen()
    {
        try
        {
            // One Way
            ReportDocument RptDoc = new ReportDocument();
            RptDoc.Load(Server.MapPath("~/RptFiles/AppReports/RPT_SLS_BYPRO.rpt"));
            RptDoc.SetDataSource(ReportsDataSet);
            RptVwr.ReportSource = RptDoc;
            Session["Rpt"] = RptDoc;
            //CrystalDecisions.CrystalReports.Engine.TextObject TXTCOMNTS_BOX = RptDoc.ReportDefinition.ReportObjects["TXT_CMMNT"] as CrystalDecisions.CrystalReports.Engine.TextObject;

            //TXTCOMNTS_BOX.ObjectFormat.EnableSuppress = txtComments.Text.Trim().Length <= 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void StrNo_Init(object sender, EventArgs e)
    {
        DataTable dtStores = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE WHERE STM_CODE IN (" + Session["userBranchCode"].ToString() + ") ");

        StrNo.DataSource = dtStores;
        StrNo.ValueField = "STM_CODE";
        StrNo.TextField = "STM_NAME";
        StrNo.DataBind();
    }
    protected void RegNo_Init(object sender, EventArgs e)
    {
        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + Session["userBranchCode"].ToString() + ") ");

        RegNo.DataSource = dtRegisters;
        RegNo.ValueField = "REG_CODE";
        RegNo.TextField = "REG_NAME";
        RegNo.DataBind();
    }

    protected void DsrStrNo_ValueChanged(object sender, EventArgs e)
    {
        ASPxComboBox value = (ASPxComboBox)sender;
        string strNo = value.Value == null ? "" : value.Value.ToString();
        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + strNo + ") ");

        RegNo.DataSource = dtRegisters;
        RegNo.ValueField = "REG_CODE";
        RegNo.TextField = "REG_NAME";
        RegNo.DataBind();
    }
}