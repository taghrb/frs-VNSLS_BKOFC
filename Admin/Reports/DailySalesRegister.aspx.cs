﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using System.Drawing;
using DevExpress.XtraPrinting;
using DevExpress.Export.Xl;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class Admin_Reports_DailySalesRegister : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
    ReportsDS ReportsDataSet = new ReportsDS();
    string START_DATE = "Earliest";
    string END_DATE = "Latest";

    bool HistoryReport = false;
    string TKTS_HDR_TABLE = "SLS_TRX_THDR";
    string TKTS_LIN_TABLE = "SLS_TRX_TLIN";
    string RCPTS_TABLE = "CUS_PAY_RCPTS";
    string RCPTS_APLY2_TABLE = "CUS_PAY_APLY2";

    protected void Page_Init(object sender, EventArgs e)
    {
        //DsrDateFrom.Value = new DateTime(1900, 1, 1);
        //DsrDateTo.Value = new DateTime(9999, 12, 31);

        if (Session["Rpt"] != null && IsPostBack)
        {
            RptVwr.ReportSource = Session["Rpt"];
        }

        if (Request.QueryString["Trx"] != null && Request.QueryString["Trx"].ToString().ToLower() == "history")
        {
            this.HistoryReport = true;
            this.TKTS_HDR_TABLE = "SLS_TKH_HIST";
            this.TKTS_LIN_TABLE = "SLS_TKL_HIST";
            this.RCPTS_TABLE = "CUS_HST_PYMNT";
            this.RCPTS_APLY2_TABLE = "CUS_HST_APLY2";
        }
    }

    protected void btnExportDailySalesRegister_Click(object sender, EventArgs e)
    {
        START_DATE = DsrDateFrom.Value != null ? ((DateTime)DsrDateFrom.Value).ToString("yyyy-MM-dd") : new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");
        END_DATE = DsrDateTo.Value != null ? ((DateTime)DsrDateTo.Value).ToString("yyyy-MM-dd") : new DateTime(9999, 12, 31).ToString("yyyy-MM-dd");

        string oneStore = DsrStrNo.Value.ToString();
        string oneRegister = DsrRegNo.Value.ToString();
        string reportType = DsrReportType.Value.ToString();

        DataTable dtExport = new DataTable();
        string qry = "";
        if (reportType == "Flash")
        {
            qry = GetBriefQuery(oneStore, oneRegister, START_DATE, END_DATE);
        }
        else if (reportType == "Brief")
        {
            qry = GetBriefQuery(oneStore, oneRegister, START_DATE, END_DATE);
        }
        else if (reportType == "Detail")
        {
            qry = GetDetailQuery(oneStore, oneRegister, START_DATE, END_DATE);
        }

        dtExport = dbHlpr.FetchData(qry);

        if (dtExport.Rows.Count <= 0)
        {
            lblErrorMessage.Text = "No Record Found.";
            RptVwr.ReportSource = null;
            Session["Rpt"] = null;

            return;
        }

        lblErrorMessage.Text = "";
        if (reportType == "Flash")
        {
            PrepareTempDataBrief(dtExport, oneStore, oneRegister);
        }
        else if (reportType == "Brief")
        {
            PrepareTempDataBrief(dtExport, oneStore, oneRegister);
        }
        else if (reportType == "Detail")
        {
            PrepareTempDataDetail(dtExport, oneStore, oneRegister);
        }

        ReportGen();
    }

    private bool PrepareTempDataBrief(DataTable dtDsr, string STRS, string REGS)
    {
        if (dtDsr.Rows.Count <= 0)
        {
            return false;
        }

        double cashSales = 0;
        double cashReturns = 0;
        double cashNetSales = 0;
        double creditSales = 0;
        double creditReturns = 0;
        double creditNetSales = 0;
        double totalSales = 0;
        double totalReturns = 0;
        double netSales = 0;
        double cshCollections = 0;
        double chqCollections = 0;
        double spnCollections = 0;
        double netCollections = 0;
        double totalCash = 0;

        List<string> calculatedCashReceipts = new List<string>();
        for (int i = 0; i < dtDsr.Rows.Count; i++)
        {
            DataRow tmpRow = ReportsDataSet.TEMP_TABLE.NewRow();

            string strGrpName = "";
            if (dtDsr.Rows[i]["LINE_TYPE"].ToString().Equals("RCPT"))
            {
                strGrpName += "COLLECTIONS";
                if (!dtDsr.Rows[i]["STATS"].ToString().Equals("Void"))
                {
                    if (!calculatedCashReceipts.Contains(dtDsr.Rows[i]["TKH_TKTNO"].ToString()))
                    {
                        if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("CASH"))
                        {
                            cshCollections += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                        }
                        else if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("CHQ"))
                        {
                            chqCollections += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                        }
                        else if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("BANK"))
                        {
                            spnCollections += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                        }
                        netCollections += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                    }
                }
                else
                {
                    strGrpName += " - VOID";
                }
                calculatedCashReceipts.Add(dtDsr.Rows[i]["TKH_TKTNO"].ToString());
            }
            else
            {
                switch (dtDsr.Rows[i]["LINE_TYPE"].ToString())
                {
                    case "Sales":
                        strGrpName += "SALES";
                        if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("CASH"))
                        {
                            strGrpName += " - CASH";
                            cashSales += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                        }
                        else if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("A/R"))
                        {
                            strGrpName += " - CREDIT";
                            creditSales += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                        }
                        break;
                    case "Return":
                        strGrpName += "SALES RETURN";
                        if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("CASH"))
                        {
                            strGrpName += " - CASH";
                            cashReturns += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                        }
                        else if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("A/R"))
                        {
                            strGrpName += " - CREDIT";
                            creditReturns += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                        }
                        break;
                }
            }
            tmpRow["TEXT01"] = strGrpName;

            tmpRow["TEXT10"] = dtDsr.Rows[i]["TKH_TKTNO"].ToString();
            tmpRow["TEXT11"] = Convert.ToDateTime(dtDsr.Rows[i]["TKH_DATE"].ToString()).ToString("dd-MMM-yyyy");
            tmpRow["TEXT12"] = dtDsr.Rows[i]["TKH_CUSNO"].ToString();
            tmpRow["TEXT13"] = dtDsr.Rows[i]["TKH_CUSNM"].ToString();
            tmpRow["TEXT14"] = dtDsr.Rows[i]["APLY2"].ToString();
            tmpRow["TEXT15"] = dtDsr.Rows[i]["STATS"].ToString();
            tmpRow["TEXT17"] = dtDsr.Rows[i]["DISCVAL"].ToString() + (dtDsr.Rows[i]["DISCFCT"].ToString().Equals("P") ? "%" : "");
            tmpRow["TEXT19"] = dtDsr.Rows[i]["TKH_PAYCD"].ToString();

            if (dtDsr.Rows[i]["INVCAMT"].ToString().Trim().Length > 0)
            {
                tmpRow["NUM11"] = Convert.ToDouble(dtDsr.Rows[i]["INVCAMT"].ToString()).ToString("n2");
            }
            else
            {
                tmpRow["NUM11"] = Convert.ToDouble("0.00").ToString("n2");
            }

            tmpRow["NUM10"] = Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString()).ToString("n2");

            tmpRow["TEXT21"] = dtDsr.Rows[i]["APLY2INVC"].ToString();
            try
            {
                tmpRow["TEXT22"] = Convert.ToDateTime(dtDsr.Rows[i]["APLY2DAT"].ToString()).ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                tmpRow["TEXT22"] = "";
            }
            if (dtDsr.Rows[i]["APLY2AMT"].ToString().Trim().Length > 0)
            {
                tmpRow["NUM20"] = Convert.ToDouble(dtDsr.Rows[i]["APLY2AMT"].ToString()).ToString("n2");
            }
            else
            {
                tmpRow["NUM20"] = Convert.ToDouble("0.00").ToString("n2");
            }

            ReportsDataSet.TEMP_TABLE.Rows.Add(tmpRow);
        }

        InsertDummyGroupsIfNotExists("SALES - CASH");
        InsertDummyGroupsIfNotExists("SALES - CREDIT");
        InsertDummyGroupsIfNotExists("SALES RETURN - CASH");
        InsertDummyGroupsIfNotExists("SALES RETURN - CREDIT");

        DataRow prmRow = ReportsDataSet.PARAM_TABLE.NewRow();

        prmRow["param01"] = "Daily Sales Report - " + DsrReportType.Value.ToString();
        prmRow["param02"] = START_DATE.Equals("1900-01-01") ? "Earliest" : Convert.ToDateTime(START_DATE).ToString("dd-MMM-yyyy");
        prmRow["param03"] = END_DATE.Equals("9999-12-31") ? "Latest" : Convert.ToDateTime(END_DATE).ToString("dd-MMM-yyyy");

        //prmRow["param04"] = dtDsr.Rows[0]["TKH_SREP"].ToString();
        //prmRow["param05"] = dtDsr.Rows[0]["TKH_CRTBY"].ToString().ToUpper();
        string Routes = string.Join(",", dtDsr.AsEnumerable().Select(r => r.Field<string>("TKH_SREP")).ToArray().Distinct(StringComparer.OrdinalIgnoreCase)).ToUpper();
        string VsUsers = string.Join(",", dtDsr.AsEnumerable().Select(r => r.Field<string>("TKH_CRTBY")).ToArray().Distinct(StringComparer.OrdinalIgnoreCase)).ToUpper();
        prmRow["param04"] = Routes.IndexOf(',') >= 0 ? "Multiple" : Routes;
        prmRow["param05"] = VsUsers.IndexOf(',') >= 0 ? VsUsers : VsUsers;
        prmRow["param06"] = STRS.IndexOf(',') >= 0 ? "Multiple" : dtDsr.Rows[0]["TKH_STRNO"].ToString();
        prmRow["param07"] = REGS.IndexOf(',') >= 0 ? "Multiple" : dtDsr.Rows[0]["TKH_REGNO"].ToString();
        prmRow["param08"] = Session["UserId"].ToString().ToUpper();
        prmRow["param09"] = dbHlpr.GetRptSeqNo();

        prmRow["param10"] = txtComments.Text.Trim();

        prmRow["param12"] = STRS.IndexOf(',') >= 0 ? "Multiple" : GetStoreName(dtDsr.Rows[0]["TKH_STRNO"].ToString());

        cashNetSales = cashSales + (cashReturns);
        creditNetSales = creditSales + (creditReturns);
        totalSales = cashSales + creditSales;
        totalReturns = cashReturns + (creditReturns);

        //double netSales = cashSales + creditSales + (cashReturns * -1) + (creditReturns * -1);
        //double totalCash = cashSales + (cashReturns * -1) + collections;
        netSales = cashSales + creditSales + (cashReturns) + (creditReturns);
        totalCash = cashSales + (cashReturns) + cshCollections;
        prmRow["param21"] = cashSales.ToString("n2");
        prmRow["param22"] = creditSales.ToString("n2");
        prmRow["param23"] = cashReturns.ToString("n2");
        prmRow["param24"] = creditReturns.ToString("n2");
        prmRow["param25"] = netSales.ToString("n2");
        prmRow["param26"] = cshCollections.ToString("n2");
        prmRow["param27"] = totalCash.ToString("n2");
        prmRow["param28"] = cashNetSales.ToString("n2");
        prmRow["param29"] = creditNetSales.ToString("n2");
        prmRow["param30"] = totalSales.ToString("n2");
        prmRow["param31"] = totalReturns.ToString("n2");

        prmRow["param32"] = chqCollections.ToString("n2");
        prmRow["param33"] = spnCollections.ToString("n2");
        prmRow["param34"] = netCollections.ToString("n2");

        ReportsDataSet.PARAM_TABLE.Rows.Add(prmRow);

        return true;
    }

    private bool PrepareTempDataDetail(DataTable dtDsr, string STRS, string REGS)
    {
        if (dtDsr.Rows.Count <= 0)
        {
            return false;
        }

        double cashSales = 0;
        double cashReturns = 0;
        double cashNetSales = 0;
        double creditSales = 0;
        double creditReturns = 0;
        double creditNetSales = 0;
        double totalSales = 0;
        double totalReturns = 0;
        double netSales = 0;
        double cshCollections = 0;
        double chqCollections = 0;
        double spnCollections = 0;
        double netCollections = 0;
        double totalCash = 0;

        List<string> calculatedCashReceipts = new List<string>();
        List<string> calculatedTickets = new List<string>();
        for (int i = 0; i < dtDsr.Rows.Count; i++)
        {
            DataRow tmpRow = ReportsDataSet.TEMP_TABLE.NewRow();

            string strGrpName = "";
            if (dtDsr.Rows[i]["LINE_TYPE"].ToString().Equals("RCPT"))
            {
                strGrpName += "COLLECTIONS";
                if (!dtDsr.Rows[i]["STATS"].ToString().Equals("Void"))
                {
                    if (!calculatedCashReceipts.Contains(dtDsr.Rows[i]["TKH_TKTNO"].ToString()))
                    {
                        if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("CASH"))
                        {
                            cshCollections += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                        }
                        else if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("CHQ"))
                        {
                            chqCollections += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                        }
                        else if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("BANK"))
                        {
                            spnCollections += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                        }
                        netCollections += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                    }
                }
                else
                {
                    strGrpName += " - VOID";
                }
                calculatedCashReceipts.Add(dtDsr.Rows[i]["TKH_TKTNO"].ToString());
            }
            else
            {
                switch (dtDsr.Rows[i]["LINE_TYPE"].ToString())
                {
                    case "Sales":
                        strGrpName += "SALES";
                        if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("CASH"))
                        {
                            strGrpName += " - CASH";
                            if (!calculatedTickets.Contains(dtDsr.Rows[i]["TKH_TKTNO"].ToString()))
                            {
                                cashSales += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                            }
                        }
                        else if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("A/R"))
                        {
                            strGrpName += " - CREDIT";
                            if (!calculatedTickets.Contains(dtDsr.Rows[i]["TKH_TKTNO"].ToString()))
                            {
                                creditSales += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                            }
                        }
                        break;
                    case "Return":
                        strGrpName += "SALES RETURN";
                        if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("CASH"))
                        {
                            strGrpName += " - CASH";
                            if (!calculatedTickets.Contains(dtDsr.Rows[i]["TKH_TKTNO"].ToString()))
                            {
                                cashReturns += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                            }
                        }
                        else if (dtDsr.Rows[i]["TKH_PAYCD"].ToString().Equals("A/R"))
                        {
                            strGrpName += " - CREDIT";
                            if (!calculatedTickets.Contains(dtDsr.Rows[i]["TKH_TKTNO"].ToString()))
                            {
                                creditReturns += Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString());
                            }
                        }
                        break;
                }
                calculatedTickets.Add(dtDsr.Rows[i]["TKH_TKTNO"].ToString());
            }
            tmpRow["TEXT01"] = strGrpName;

            tmpRow["TEXT10"] = dtDsr.Rows[i]["TKH_TKTNO"].ToString();
            tmpRow["TEXT11"] = Convert.ToDateTime(dtDsr.Rows[i]["TKH_DATE"].ToString()).ToString("dd-MMM-yyyy");
            tmpRow["TEXT12"] = dtDsr.Rows[i]["TKH_CUSNO"].ToString();
            tmpRow["TEXT13"] = dtDsr.Rows[i]["TKH_CUSNM"].ToString();
            tmpRow["TEXT14"] = dtDsr.Rows[i]["APLY2"].ToString();
            tmpRow["TEXT15"] = dtDsr.Rows[i]["STATS"].ToString();
            tmpRow["TEXT17"] = dtDsr.Rows[i]["DISCVAL"].ToString() + (dtDsr.Rows[i]["DISCFCT"].ToString().Equals("P") ? "%" : "");
            tmpRow["TEXT19"] = dtDsr.Rows[i]["TKH_PAYCD"].ToString();

            if (dtDsr.Rows[i]["INVCAMT"].ToString().Trim().Length > 0)
            {
                tmpRow["NUM11"] = Convert.ToDouble(dtDsr.Rows[i]["INVCAMT"].ToString()).ToString("n2");
            }
            else
            {
                tmpRow["NUM11"] = Convert.ToDouble("0.00").ToString("n2");
            }

            tmpRow["NUM10"] = Convert.ToDouble(dtDsr.Rows[i]["TKH_AMT"].ToString()).ToString("n2");

            tmpRow["TEXT21"] = dtDsr.Rows[i]["APLY2INVC"].ToString();
            try
            {
                tmpRow["TEXT22"] = Convert.ToDateTime(dtDsr.Rows[i]["APLY2DAT"].ToString()).ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                tmpRow["TEXT22"] = "";
            }
            if (dtDsr.Rows[i]["APLY2AMT"].ToString().Trim().Length > 0)
            {
                tmpRow["NUM20"] = Convert.ToDouble(dtDsr.Rows[i]["APLY2AMT"].ToString()).ToString("n2");
            }
            else
            {
                tmpRow["NUM20"] = Convert.ToDouble("0.00").ToString("n2");
            }

            tmpRow["TEXT05"] = dtDsr.Rows[i]["TKL_ITMNO"].ToString();
            tmpRow["TEXT06"] = dtDsr.Rows[i]["TKL_DESC"].ToString();
            tmpRow["TEXT07"] = Convert.ToDateTime(dtDsr.Rows[i]["TKL_EXPDT"].ToString()).ToString("dd-MMM-yyyy");
            tmpRow["TEXT08"] = dtDsr.Rows[i]["TKL_UOM"].ToString();

            tmpRow["NUM04"] = Convert.ToDouble(dtDsr.Rows[i]["TKL_QTY"].ToString()).ToString("n3");
            tmpRow["NUM05"] = Convert.ToDouble(dtDsr.Rows[i]["TKL_PRICE"].ToString()).ToString("n2");
            tmpRow["NUM06"] = Convert.ToDouble(dtDsr.Rows[i]["TKL_EXPRC"].ToString()).ToString("n2");
            tmpRow["NUM07"] = (Convert.ToDouble(dtDsr.Rows[i]["TKL_VATPRC"].ToString()) - Convert.ToDouble(dtDsr.Rows[i]["TKL_EXPRC"].ToString())).ToString("n2");
            tmpRow["NUM08"] = Convert.ToDouble(dtDsr.Rows[i]["TKL_VATPRC"].ToString()).ToString("n2");


            ReportsDataSet.TEMP_TABLE.Rows.Add(tmpRow);
        }

        InsertDummyGroupsIfNotExists("SALES - CASH");
        InsertDummyGroupsIfNotExists("SALES - CREDIT");
        InsertDummyGroupsIfNotExists("SALES RETURN - CASH");
        InsertDummyGroupsIfNotExists("SALES RETURN - CREDIT");

        DataRow prmRow = ReportsDataSet.PARAM_TABLE.NewRow();

        prmRow["param01"] = "Daily Sales Report - " + DsrReportType.Value.ToString();
        prmRow["param02"] = START_DATE.Equals("1900-01-01") ? "Earliest" : Convert.ToDateTime(START_DATE).ToString("dd-MMM-yyyy");
        prmRow["param03"] = END_DATE.Equals("9999-12-31") ? "Latest" : Convert.ToDateTime(END_DATE).ToString("dd-MMM-yyyy");

        //prmRow["param04"] = dtDsr.Rows[0]["TKH_SREP"].ToString();
        //prmRow["param05"] = dtDsr.Rows[0]["TKH_CRTBY"].ToString().ToUpper();
        string Routes = string.Join(",", dtDsr.AsEnumerable().Select(r => r.Field<string>("TKH_SREP")).ToArray().Distinct(StringComparer.OrdinalIgnoreCase)).ToUpper();
        string VsUsers = string.Join(",", dtDsr.AsEnumerable().Select(r => r.Field<string>("TKH_CRTBY")).ToArray().Distinct(StringComparer.OrdinalIgnoreCase)).ToUpper();
        prmRow["param04"] = Routes.IndexOf(',') >= 0 ? "Multiple" : Routes;
        prmRow["param05"] = VsUsers.IndexOf(',') >= 0 ? VsUsers : VsUsers;
        prmRow["param06"] = STRS.IndexOf(',') >= 0 ? "Multiple" : dtDsr.Rows[0]["TKH_STRNO"].ToString();
        prmRow["param07"] = REGS.IndexOf(',') >= 0 ? "Multiple" : dtDsr.Rows[0]["TKH_REGNO"].ToString();
        prmRow["param08"] = Session["UserId"].ToString().ToUpper();
        prmRow["param09"] = dbHlpr.GetRptSeqNo();

        prmRow["param10"] = txtComments.Text.Trim();

        prmRow["param12"] = STRS.IndexOf(',') >= 0 ? "Multiple" : GetStoreName(dtDsr.Rows[0]["TKH_STRNO"].ToString());

        cashNetSales = cashSales + (cashReturns);
        creditNetSales = creditSales + (creditReturns);
        totalSales = cashSales + creditSales;
        totalReturns = cashReturns + (creditReturns);

        //double netSales = cashSales + creditSales + (cashReturns * -1) + (creditReturns * -1);
        //double totalCash = cashSales + (cashReturns * -1) + collections;
        netSales = cashSales + creditSales + (cashReturns) + (creditReturns);
        totalCash = cashSales + (cashReturns) + cshCollections;
        prmRow["param21"] = cashSales.ToString("n2");
        prmRow["param22"] = creditSales.ToString("n2");
        prmRow["param23"] = cashReturns.ToString("n2");
        prmRow["param24"] = creditReturns.ToString("n2");
        prmRow["param25"] = netSales.ToString("n2");
        prmRow["param26"] = cshCollections.ToString("n2");
        prmRow["param27"] = totalCash.ToString("n2");
        prmRow["param28"] = cashNetSales.ToString("n2");
        prmRow["param29"] = creditNetSales.ToString("n2");
        prmRow["param30"] = totalSales.ToString("n2");
        prmRow["param31"] = totalReturns.ToString("n2");

        prmRow["param32"] = chqCollections.ToString("n2");
        prmRow["param33"] = spnCollections.ToString("n2");
        prmRow["param34"] = netCollections.ToString("n2");

        ReportsDataSet.PARAM_TABLE.Rows.Add(prmRow);

        return true;
    }

    private string GetBriefQuery(string Store, string Register, string StartDate, string EndDate)
    {
        string q = "SELECT "
                + " TKH_TKTNO, TKH_CUSNO, TKH_CUSNM, TKH_SREP, "
                + " TKH_DATE, TKH_TIME, TKH_AMT, TKH_PAYCD, TKH_CRTBY, "
                + " TKH_STRNO, TKH_REGNO, TKH_TTYPE AS LINE_TYPE, '' AS APLY2, '' AS STATS, "
                + " '0' AS INVCAMT, '' AS DISCFCT, '0' AS DISCVAL, "
                + " '' AS APLY2INVC, '' AS APLY2DAT, '0' AS APLY2AMT "
                + " FROM " + this.TKTS_HDR_TABLE + " "
                + " WHERE TKH_DATE BETWEEN '" + StartDate + "' AND CONVERT(datetime, '" + EndDate + " 23:59:59.998')  "
                + " AND TKH_STRNO IN (" + Store + ") "
                + " AND TKH_REGNO IN (" + Register + ") "

                + " UNION ALL "

                + " SELECT "
                + " PR_NO, PR_CUSNO, PR_CUSNM, PR_SREP, "
                + " PR_RCPDT, PR_RCPTM, PR_AMTPD, PR_PAYCD, PR_CRTBY, "
                + " PR_STRNO, PR_REGNO, 'RCPT' AS LINE_TYPE, PR_APLY2 AS APLY2, PR_STATS AS STATS, "
                + " PR_TOTAL AS INVCAMT, PR_DSTYP AS DISCFCT, PR_DSFCT AS DISCVAL, "
                + " PA_INVNO AS APLY2INVC, PA_INVDT AS APLY2DAT, PA_AMT AS APLY2AMT "
                + " FROM " + this.RCPTS_TABLE + " "
                + " LEFT JOIN " + this.RCPTS_APLY2_TABLE + " ON PR_NO = PA_PRNO "
                + " WHERE PR_RCPDT BETWEEN '" + StartDate + "' AND CONVERT(datetime, '" + EndDate + " 23:59:59.998') "
                + " AND PR_STRNO IN (" + Store + ") "
                + " AND PR_REGNO IN (" + Register + ") "

                + " ORDER BY TKH_TKTNO ";

        return q;
    }

    private string GetDetailQuery(string Store, string Register, string StartDate, string EndDate)
    {
        string q = "SELECT "
                + " TKH_TKTNO, TKH_CUSNO, TKH_CUSNM, TKH_SREP, "
                + " TKH_DATE, TKH_TIME, TKH_AMT, TKH_PAYCD, TKH_CRTBY, "
                + " TKH_STRNO, TKH_REGNO, TKH_TTYPE AS LINE_TYPE, '' AS APLY2, '' AS STATS, "
                + " '0' AS INVCAMT, '' AS DISCFCT, '0' AS DISCVAL, "
                + " '' AS APLY2INVC, '' AS APLY2DAT, '0' AS APLY2AMT, "
                + " TKL_ITMNO, TKL_DESC, TKL_EXPDT, TKL_QTY, TKL_UOM, TKL_PRICE, TKL_EXPRC, TKL_VATPRC "

                + " FROM " + this.TKTS_HDR_TABLE + " "
                + " JOIN " + this.TKTS_LIN_TABLE + " ON TKH_TKTNO = TKL_TKTNO "

                + " WHERE TKH_DATE BETWEEN '" + StartDate + "' AND CONVERT(datetime, '" + EndDate + " 23:59:59.998')  "
                + " AND TKH_STRNO IN (" + Store + ") "
                + " AND TKH_REGNO IN (" + Register + ") "

                + " UNION ALL "

                + " SELECT "
                + " PR_NO, PR_CUSNO, PR_CUSNM, PR_SREP, "
                + " PR_RCPDT, PR_RCPTM, PR_AMTPD, PR_PAYCD, PR_CRTBY, "
                + " PR_STRNO, PR_REGNO, 'RCPT' AS LINE_TYPE, PR_APLY2 AS APLY2, PR_STATS AS STATS, "
                + " PR_TOTAL AS INVCAMT, PR_DSTYP AS DISCFCT, PR_DSFCT AS DISCVAL, "
                + " PA_INVNO AS APLY2INVC, PA_INVDT AS APLY2DAT, PA_AMT AS APLY2AMT, "
                + " '', '', '', '', '', '', '', '' "

                + " FROM " + this.RCPTS_TABLE + " "
                + " LEFT JOIN " + this.RCPTS_APLY2_TABLE + " ON PR_NO = PA_PRNO "

                + " WHERE PR_RCPDT BETWEEN '" + StartDate + "' AND CONVERT(datetime, '" + EndDate + " 23:59:59.998') "
                + " AND PR_STRNO IN ('" + Store + "') "
                + " AND PR_REGNO IN ('" + Register + "') "

                + " ORDER BY TKH_TKTNO ";

        return q;
    }

    public void ReportGen()
    {
        try
        {
            // One Way
            ReportDocument RptDoc = new ReportDocument();

            if (DsrReportType.Value.ToString().Equals("Flash"))
            {
                RptDoc.Load(Server.MapPath("~/RptFiles/DSR_FLS/CR_DSR_MAIN_FLS.rpt"));
            }
            else if (DsrReportType.Value.ToString().Equals("Brief"))
            {
                RptDoc.Load(Server.MapPath("~/RptFiles/CR_DSR_MAIN.rpt"));
            }
            else if (DsrReportType.Value.ToString().Equals("Detail"))
            {
                RptDoc.Load(Server.MapPath("~/RptFiles/DSR_DTL/CR_DSR_MAIN_DTL.rpt"));
            }

            RptDoc.SetDataSource(ReportsDataSet);
            RptVwr.ReportSource = RptDoc;
            CrystalDecisions.CrystalReports.Engine.TextObject TXTCOMNTS_BOX = RptDoc.ReportDefinition.ReportObjects["TXT_CMMNT"] as CrystalDecisions.CrystalReports.Engine.TextObject;

            CrystalDecisions.CrystalReports.Engine.Section RptFtrSign = RptDoc.ReportDefinition.Sections["RptFtrSign"] as CrystalDecisions.CrystalReports.Engine.Section;
            RptFtrSign.SectionFormat.EnableSuppress = HistoryReport;

            // Second Way
            //ReportClass RptObj = new CR_DSR_MAIN();
            //RptObj.SetDatabaseLogon("sa", "123");
            //RptObj.SetDataSource(ReportsDataSet);
            //RptVwr.ReportSource = RptObj;
            //Session["Rpt"] = RptObj;
            //CrystalDecisions.CrystalReports.Engine.TextObject TXTCOMNTS_BOX = RptObj.ReportDefinition.ReportObjects["TXT_CMMNT"] as CrystalDecisions.CrystalReports.Engine.TextObject;

            TXTCOMNTS_BOX.ObjectFormat.EnableSuppress = txtComments.Text.Trim().Length <= 0;

            Session["Rpt"] = RptDoc;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void DsrStrNo_Init(object sender, EventArgs e)
    {
        DataTable dtStores = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE WHERE STM_CODE IN (" + Session["userBranchCode"].ToString() + ") ");

        DsrStrNo.DataSource = dtStores;
        DsrStrNo.ValueField = "STM_CODE";
        DsrStrNo.TextField = "STM_NAME";
        DsrStrNo.DataBind();
    }
    protected void DsrRegNo_Init(object sender, EventArgs e)
    {
        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + Session["userBranchCode"].ToString() + ") ");

        DsrRegNo.DataSource = dtRegisters;
        DsrRegNo.ValueField = "REG_CODE";
        DsrRegNo.TextField = "REG_NAME";
        DsrRegNo.DataBind();
    }

    protected void DsrStrNo_ValueChanged(object sender, EventArgs e)
    {
        ASPxComboBox value = (ASPxComboBox)sender;
        string strNo = value.Value == null ? "" : value.Value.ToString();
        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + strNo + ") ");

        DsrRegNo.DataSource = dtRegisters;
        DsrRegNo.ValueField = "REG_CODE";
        DsrRegNo.TextField = "REG_NAME";
        DsrRegNo.DataBind();
    }

    private string GetStoreName(string StrNo)
    {
        DataTable dtStores = dbHlpr.FetchData("SELECT STM_NAME FROM STP_MSTR_STORE WHERE STM_CODE = '" + StrNo + "' ");
        if (dtStores.Rows.Count > 0)
        {
            return dtStores.Rows[0]["STM_NAME"].ToString();
        }
        return "";
    }

    private void InsertDummyGroupsIfNotExists(string grpName)
    {
        DataRow[] drFound = ReportsDataSet.TEMP_TABLE.Select("TEXT01='" + grpName + "'");
        if (drFound.Length <= 0)
        {
            DataRow tmpRow = ReportsDataSet.TEMP_TABLE.NewRow();

            tmpRow["TEXT01"] = grpName;

            ReportsDataSet.TEMP_TABLE.Rows.Add(tmpRow);
        }
    }
}