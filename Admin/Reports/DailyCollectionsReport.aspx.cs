﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using System.Drawing;
using DevExpress.XtraPrinting;
using DevExpress.Export.Xl;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class Admin_Reports_DailyCollectionsReport : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
    ReportsDS ReportsDataSet = new ReportsDS();
    string START_DATE = "Earliest";
    string END_DATE = "Latest";

    bool HistoryReport = false;
    string TKTS_HDR_TABLE = "SLS_TRX_THDR";
    string TKTS_LIN_TABLE = "SLS_TRX_TLIN";
    string RCPTS_TABLE = "CUS_PAY_RCPTS";
    string RCPTS_APLY2_TABLE = "CUS_PAY_APLY2";

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["Rpt"] != null && IsPostBack)
        {
            RptVwr.ReportSource = Session["Rpt"];
        }

        if (Request.QueryString["Trx"] != null && Request.QueryString["Trx"].ToString().ToLower() == "history")
        {
            this.HistoryReport = true;
            this.TKTS_HDR_TABLE = "SLS_TKH_HIST";
            this.TKTS_LIN_TABLE = "SLS_TKL_HIST";
            this.RCPTS_TABLE = "CUS_HST_PYMNT";
            this.RCPTS_APLY2_TABLE = "CUS_HST_APLY2";
        }
    }

    protected void btnExportDailyCollectionsReport_Click(object sender, EventArgs e)
    {
        START_DATE = DcrDateFrom.Value != null ? ((DateTime)DcrDateFrom.Value).ToString("yyyy-MM-dd") : new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");
        END_DATE = DcrDateTo.Value != null ? ((DateTime)DcrDateTo.Value).ToString("yyyy-MM-dd") : new DateTime(9999, 12, 31).ToString("yyyy-MM-dd");

        string oneStore = DcrStrNo.Value.ToString();
        string reportType = DcrReportType.Value.ToString();

        DataTable dtExport = new DataTable();

        string qry = "";
        if (reportType == "Flash" || reportType == "Brief")
        {
            qry = " SELECT "
                + " PR_NO, PR_CUSNO, PR_CUSNM, PR_SREP, "
                + " PR_RCPDT, PR_RCPTM, PR_AMTPD, PR_PAYCD, PR_CRTBY, "
                + " PR_STRNO, PR_REGNO, PR_APLY2 AS APLY2, PR_STATS AS STATS, "
                + " PR_TOTAL AS INVCAMT, PR_DSTYP AS DISCFCT, PR_DSFCT AS DISCVAL "
                + " FROM " + this.RCPTS_TABLE + " "
                + " WHERE PR_RCPDT BETWEEN '" + START_DATE + "' AND '" + END_DATE + "' "
                + " AND PR_STATS <> 'Void' ";
            if (oneStore != "All")
            {
                qry += " AND PR_STRNO IN (" + oneStore + ") ";
            }
        }
        else
        {
            qry = " SELECT "
                + " PR_NO, PR_CUSNO, PR_CUSNM, PR_SREP, "
                + " PR_RCPDT, PR_RCPTM, PR_AMTPD, PR_PAYCD, PR_CRTBY, "
                + " PR_STRNO, PR_REGNO, PR_APLY2 AS APLY2, PR_STATS AS STATS, "
                + " PR_TOTAL AS INVCAMT, PR_DSTYP AS DISCFCT, PR_DSFCT AS DISCVAL, "
                + " PA_INVNO AS APLY2INVC, PA_INVDT AS APLY2DAT, PA_AMT AS APLY2AMT "
                + " FROM " + this.RCPTS_TABLE + " "
                + " LEFT JOIN " + this.RCPTS_APLY2_TABLE + " ON PR_NO = PA_PRNO "
                + " WHERE PR_RCPDT BETWEEN '" + START_DATE + "' AND '" + END_DATE + "' "
                + " AND PR_STATS <> 'Void' ";
            if (oneStore != "All")
            {
                qry += " AND PR_STRNO IN (" + oneStore + ") ";
            }
        }

        dtExport = dbHlpr.FetchData(qry);

        lblErrorMessage.Text = "";

        if (reportType == "Flash" || reportType == "Brief")
        {
            PrepareTempDataFlashBrief(dtExport, oneStore);
            ReportGen();
        }
        else if (reportType == "Detail")
        {
            PrepareTempDataDetail(dtExport, oneStore);
            ReportGen();
        }
        else
        {
            lblErrorMessage.Text = "No Record Found.";
            RptVwr.ReportSource = null;
            Session["Rpt"] = null;
        }
    }

    private bool PrepareTempDataDetail(DataTable dtDcr, string STRS)
    {
        if (dtDcr.Rows.Count <= 0)
        {
            return false;
        }

        for (int i = 0; i < dtDcr.Rows.Count; i++)
        {
            DataRow tmpRow = ReportsDataSet.TEMP_TABLE.NewRow();

            tmpRow["TEXT07"] = dtDcr.Rows[i]["PR_STRNO"].ToString();
            tmpRow["TEXT16"] = GetStoreName(dtDcr.Rows[i]["PR_STRNO"].ToString());
            tmpRow["TEXT08"] = dtDcr.Rows[i]["PR_REGNO"].ToString();
            tmpRow["TEXT09"] = dtDcr.Rows[i]["PR_SREP"].ToString();
            tmpRow["TEXT10"] = dtDcr.Rows[i]["PR_NO"].ToString();
            tmpRow["TEXT11"] = Convert.ToDateTime(dtDcr.Rows[i]["PR_RCPDT"].ToString()).ToString("dd-MMM-yyyy");
            tmpRow["TEXT12"] = dtDcr.Rows[i]["PR_CUSNO"].ToString();
            tmpRow["TEXT13"] = dtDcr.Rows[i]["PR_CUSNM"].ToString();
            tmpRow["TEXT14"] = dtDcr.Rows[i]["APLY2"].ToString();
            tmpRow["TEXT15"] = dtDcr.Rows[i]["STATS"].ToString();
            tmpRow["TEXT17"] = dtDcr.Rows[i]["DISCVAL"].ToString() + (dtDcr.Rows[i]["DISCFCT"].ToString().Equals("P") ? "%" : "");
            tmpRow["TEXT19"] = dtDcr.Rows[i]["PR_PAYCD"].ToString();
            tmpRow["TEXT21"] = dtDcr.Rows[i]["APLY2INVC"].ToString();
            try
            {
                tmpRow["TEXT22"] = Convert.ToDateTime(dtDcr.Rows[i]["APLY2DAT"].ToString()).ToString("dd-MMM-yyyy");
            }
            catch (Exception ex)
            {
                tmpRow["TEXT22"] = "";
            }

            tmpRow["NUM10"] = Convert.ToDouble(dtDcr.Rows[i]["PR_AMTPD"].ToString()).ToString("n2");
            if (dtDcr.Rows[i]["INVCAMT"].ToString().Trim().Length > 0)
            {
                tmpRow["NUM11"] = Convert.ToDouble(dtDcr.Rows[i]["INVCAMT"].ToString()).ToString("n2");
            }
            else
            {
                tmpRow["NUM11"] = Convert.ToDouble("0.00").ToString("n2");
            }

            if (dtDcr.Rows[i]["APLY2AMT"].ToString().Trim().Length > 0)
            {
                tmpRow["NUM20"] = Convert.ToDouble(dtDcr.Rows[i]["APLY2AMT"].ToString()).ToString("n2");
            }
            else
            {
                tmpRow["NUM20"] = Convert.ToDouble("0.00").ToString("n2");
            }

            ReportsDataSet.TEMP_TABLE.Rows.Add(tmpRow);
        }

        DataRow prmRow = ReportsDataSet.PARAM_TABLE.NewRow();

        prmRow["param01"] = "Daily Collections Report - " + DcrReportType.Value.ToString();
        prmRow["param02"] = START_DATE.Equals("1900-01-01") ? "Earliest" : Convert.ToDateTime(START_DATE).ToString("dd-MMM-yyyy");
        prmRow["param03"] = END_DATE.Equals("9999-12-31") ? "Latest" : Convert.ToDateTime(END_DATE).ToString("dd-MMM-yyyy");

        string Routes = string.Join(",", dtDcr.AsEnumerable().Select(r => r.Field<string>("PR_SREP")).ToArray().Distinct(StringComparer.OrdinalIgnoreCase)).ToUpper();
        string VsUsers = string.Join(",", dtDcr.AsEnumerable().Select(r => r.Field<string>("PR_CRTBY")).ToArray().Distinct(StringComparer.OrdinalIgnoreCase)).ToUpper();
        prmRow["param04"] = Routes.IndexOf(',') >= 0 ? "Multiple" : Routes;
        prmRow["param05"] = VsUsers.IndexOf(',') >= 0 ? VsUsers : VsUsers;
        prmRow["param06"] = DcrStrNo.Value != null ? DcrStrNo.Value.ToString() : "";
        prmRow["param08"] = Session["UserId"].ToString().ToUpper();
        prmRow["param09"] = dbHlpr.GetRptSeqNo();

        prmRow["param10"] = txtComments.Text.Trim();

        prmRow["param12"] = STRS.IndexOf(',') >= 0 ? "Multiple" : GetStoreName(STRS);

        prmRow["param20"] = Convert.ToDouble(ComputeMyDataTable(ReportsDataSet.TEMP_TABLE, "SUM(NUM10)", "")).ToString("n2");
        prmRow["param21"] = Convert.ToDouble(ComputeMyDataTable(ReportsDataSet.TEMP_TABLE, "SUM(NUM11)", "")).ToString("n2");

        ReportsDataSet.PARAM_TABLE.Rows.Add(prmRow);

        return true;
    }


    private bool PrepareTempDataFlashBrief(DataTable dtDcr, string STRS)
    {
        if (dtDcr.Rows.Count <= 0)
        {
            return false;
        }

        for (int i = 0; i < dtDcr.Rows.Count; i++)
        {
            DataRow tmpRow = ReportsDataSet.TEMP_TABLE.NewRow();

            tmpRow["TEXT07"] = dtDcr.Rows[i]["PR_STRNO"].ToString();
            tmpRow["TEXT16"] = GetStoreName(dtDcr.Rows[i]["PR_STRNO"].ToString());
            tmpRow["TEXT08"] = dtDcr.Rows[i]["PR_REGNO"].ToString();
            tmpRow["TEXT09"] = dtDcr.Rows[i]["PR_SREP"].ToString();
            tmpRow["TEXT10"] = dtDcr.Rows[i]["PR_NO"].ToString();
            tmpRow["TEXT11"] = Convert.ToDateTime(dtDcr.Rows[i]["PR_RCPDT"].ToString()).ToString("dd-MMM-yyyy");
            tmpRow["TEXT12"] = dtDcr.Rows[i]["PR_CUSNO"].ToString();
            tmpRow["TEXT13"] = dtDcr.Rows[i]["PR_CUSNM"].ToString();
            tmpRow["TEXT14"] = dtDcr.Rows[i]["APLY2"].ToString();
            tmpRow["TEXT15"] = dtDcr.Rows[i]["STATS"].ToString();
            tmpRow["TEXT17"] = dtDcr.Rows[i]["DISCVAL"].ToString() + (dtDcr.Rows[i]["DISCFCT"].ToString().Equals("P") ? "%" : "");
            tmpRow["TEXT19"] = dtDcr.Rows[i]["PR_PAYCD"].ToString();

            tmpRow["NUM10"] = Convert.ToDouble(dtDcr.Rows[i]["PR_AMTPD"].ToString()).ToString("n2");
            if (dtDcr.Rows[i]["INVCAMT"].ToString().Trim().Length > 0)
            {
                tmpRow["NUM11"] = Convert.ToDouble(dtDcr.Rows[i]["INVCAMT"].ToString()).ToString("n2");
            }
            else
            {
                tmpRow["NUM11"] = Convert.ToDouble("0.00").ToString("n2");
            }

            ReportsDataSet.TEMP_TABLE.Rows.Add(tmpRow);
        }

        DataRow prmRow = ReportsDataSet.PARAM_TABLE.NewRow();

        prmRow["param01"] = "Daily Collections Report - " + DcrReportType.Value.ToString();
        prmRow["param02"] = START_DATE.Equals("1900-01-01") ? "Earliest" : Convert.ToDateTime(START_DATE).ToString("dd-MMM-yyyy");
        prmRow["param03"] = END_DATE.Equals("9999-12-31") ? "Latest" : Convert.ToDateTime(END_DATE).ToString("dd-MMM-yyyy");

        string Routes = string.Join(",", dtDcr.AsEnumerable().Select(r => r.Field<string>("PR_SREP")).ToArray().Distinct(StringComparer.OrdinalIgnoreCase)).ToUpper();
        string VsUsers = string.Join(",", dtDcr.AsEnumerable().Select(r => r.Field<string>("PR_CRTBY")).ToArray().Distinct(StringComparer.OrdinalIgnoreCase)).ToUpper();
        prmRow["param04"] = Routes.IndexOf(',') >= 0 ? "Multiple" : Routes;
        prmRow["param05"] = VsUsers.IndexOf(',') >= 0 ? VsUsers : VsUsers;
        prmRow["param06"] = DcrStrNo.Value != null ? DcrStrNo.Value.ToString() : "";
        prmRow["param08"] = Session["UserId"].ToString().ToUpper();
        prmRow["param09"] = dbHlpr.GetRptSeqNo();

        prmRow["param10"] = txtComments.Text.Trim();

        prmRow["param12"] = STRS.IndexOf(',') >= 0 ? "Multiple" : GetStoreName(STRS);

        prmRow["param20"] = Convert.ToDouble(ComputeMyDataTable(ReportsDataSet.TEMP_TABLE, "SUM(NUM10)", "")).ToString("n2");
        prmRow["param21"] = Convert.ToDouble(ComputeMyDataTable(ReportsDataSet.TEMP_TABLE, "SUM(NUM11)", "")).ToString("n2");

        ReportsDataSet.PARAM_TABLE.Rows.Add(prmRow);

        return true;
    }

    public void ReportGen()
    {
        try
        {
            ReportDocument RptDoc = new ReportDocument();

            if (DcrReportType.Value.ToString().Equals("Flash"))
            {
                RptDoc.Load(Server.MapPath("~/RptFiles/CR_DCR_FLS.rpt"));
            }
            else if (DcrReportType.Value.ToString().Equals("Brief"))
            {
                RptDoc.Load(Server.MapPath("~/RptFiles/CR_DCR_BRF.rpt"));
            }
            else if (DcrReportType.Value.ToString().Equals("Detail"))
            {
                RptDoc.Load(Server.MapPath("~/RptFiles/CR_DCR_DTL.rpt"));
            }

            RptDoc.SetDataSource(ReportsDataSet);
            RptVwr.ReportSource = RptDoc;

            Session["Rpt"] = RptDoc;

            CrystalDecisions.CrystalReports.Engine.Section RptFtrSign = RptDoc.ReportDefinition.Sections["RptFtrSign"] as CrystalDecisions.CrystalReports.Engine.Section;
            RptFtrSign.SectionFormat.EnableSuppress = HistoryReport;

            CrystalDecisions.CrystalReports.Engine.TextObject TXTCOMNTS_BOX = RptDoc.ReportDefinition.ReportObjects["TXT_CMMNT"] as CrystalDecisions.CrystalReports.Engine.TextObject;
            TXTCOMNTS_BOX.ObjectFormat.EnableSuppress = txtComments.Text.Trim().Length <= 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void DcrStrNo_Init(object sender, EventArgs e)
    {
        DataTable dtStores = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE WHERE STM_CODE IN (" + Session["userBranchCode"].ToString() + ") ");

        DataRow dr = dtStores.NewRow();
        dr["STM_CODE"] = "All";
        dr["STM_NAME"] = "All";
        dtStores.Rows.InsertAt(dr, 0);

        DcrStrNo.DataSource = dtStores;
        DcrStrNo.ValueField = "STM_CODE";
        DcrStrNo.TextField = "STM_NAME";
        DcrStrNo.DataBind();
    }

    private string GetStoreName(string StrNo)
    {
        if (StrNo == "All")
        {
            return "All Branches";
        }

        DataTable dtStores = dbHlpr.FetchData("SELECT STM_NAME FROM STP_MSTR_STORE WHERE STM_CODE = '" + StrNo + "' ");
        if (dtStores.Rows.Count > 0)
        {
            return dtStores.Rows[0]["STM_NAME"].ToString();
        }
        return "";
    }

    private string ComputeMyDataTable(DataTable dt, string computeExpr, string filterExpr)
    {
        var result = dt.Compute(computeExpr, filterExpr);
        if (result != DBNull.Value)
        {
            return result.ToString();
        }

        return "0.00";
    }
}