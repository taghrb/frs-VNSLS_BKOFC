﻿<%@ Page Title="Exceptional Reports - Header Line Customer Mismatch" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="Excp_HdrLinCustMisMatch.aspx.cs" Inherits="Admin_Reports_Excp_HdrLinCustMisMatch" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>


<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <script type="text/javascript">

        function grid_customizationWindowCloseUp(s, e) {
            gvUOMClientInquery.ShowCustomizationWindow();
        }

        // Code to create a custom multiselect ComboBox, Dropdown
        var textSeparator = ","; // Between each checked item in ComboBox
        function OnListBoxSelectionChanged(listBox, args) {
            if (args.index == 0)
                args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
            if (listBox.name.indexOf("lstBxStrItems") != -1) {
                IsAllSelected(checkStrListBox) ? checkStrListBox.SelectIndices([0]) : checkStrListBox.UnselectIndices([0]);
                UpdateText(ClientStrNo, checkStrListBox);
            } else if (listBox.name.indexOf("lstBxRegItems") != -1) {
                IsAllSelected(checkRegListBox) ? checkRegListBox.SelectIndices([0]) : checkRegListBox.UnselectIndices([0]);
                UpdateText(ClientRegNo, checkRegListBox);
            }
        }
        function IsAllSelected(chkLstBx) {
            var selectedDataItemCount = chkLstBx.GetItemCount() - (chkLstBx.GetItem(0).selected ? 0 : 1);
            return chkLstBx.GetSelectedItems().length == selectedDataItemCount;
        }
        function UpdateText(ddObj, lstBxObj) {
            var selectedItems = lstBxObj.GetSelectedItems();
            ddObj.SetText(GetSelectedItemsText(selectedItems));
        }
        function SynchronizeListBoxValues(dropDown, args) {
            var texts = dropDown.GetText().split(textSeparator);
            if (dropDown.name.indexOf("StrNo") != -1) {
                checkStrListBox.UnselectAll();
                checkStrListBox.SelectValues(texts);
                IsAllSelected(checkStrListBox) ? checkStrListBox.SelectIndices([0]) : checkStrListBox.UnselectIndices([0]);
                UpdateText(ClientStrNo, checkStrListBox);
            } else if (dropDown.name.indexOf("RegNo") != -1) {
                checkRegListBox.UnselectAll();
                checkRegListBox.SelectValues(texts);
                IsAllSelected(checkRegListBox) ? checkRegListBox.SelectIndices([0]) : checkRegListBox.UnselectIndices([0]);
                UpdateText(ClientRegNo, checkRegListBox);
            }
        }
        function GetSelectedItemsText(items) {
            var texts = [];
            for (var i = 0; i < items.length; i++)
                if (items[i].index != 0)
                    texts.push(items[i].value);
            return texts.join(textSeparator);
        }
        function GetValuesByTexts(texts, chkLstBx) {
            var actualValues = [];
            var item;
            for (var i = 0; i < texts.length; i++) {
                item = chkLstBx.FindItemByText(texts[i]);
                if (item != null)
                    actualValues.push(item.value);
            }
            return actualValues;
        }

    </script>
    <style type="text/css">
        .fieldsetWrapper {
            padding: 10px;
            width: 70%;
            margin: 0 auto;
            background-color: #F7F5F1;
            margin-top: 5px;
            border-radius: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="contentTopHere1" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
            <li>&nbsp;
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">

    <div class="formDiv" style="background-color: transparent;">
        <div class="formHeaderDiv">
            <h3>Header Line Customer Mismatch</h3>
        </div>
        <div class="fieldsetWrapper">
            <fieldset>
                <legend></legend>
                <table style="margin: 0 auto;">
                    <tr>
                        <td>Store # :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxDropDownEdit ID="StrNo" OnInit="StrNo_Init" ClientInstanceName="ClientStrNo" CssClass="multiselect_combobox" runat="server" AnimationType="Auto">
                                    <DropDownWindowTemplate>
                                        <dx:ASPxListBox Width="170px" ID="lstBxStrItems" ClientInstanceName="checkStrListBox" SelectionMode="CheckColumn" runat="server" OnValueChanged="StoreChanged" AutoPostBack="true">
                                            <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged" />
                                        </dx:ASPxListBox>
                                    </DropDownWindowTemplate>
                                    <ClientSideEvents TextChanged="SynchronizeListBoxValues" DropDown="SynchronizeListBoxValues" />
                                </dx:ASPxDropDownEdit>
                            </div>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>Register # :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxDropDownEdit ID="RegNo" OnInit="RegNo_Init" ClientInstanceName="ClientRegNo" CssClass="multiselect_combobox" runat="server" AnimationType="Auto">
                                    <DropDownWindowTemplate>
                                        <dx:ASPxListBox Width="170px" ID="lstBxRegItems" ClientInstanceName="checkRegListBox" SelectionMode="CheckColumn" runat="server">
                                            <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged" />
                                        </dx:ASPxListBox>
                                    </DropDownWindowTemplate>
                                    <ClientSideEvents TextChanged="SynchronizeListBoxValues" DropDown="SynchronizeListBoxValues" />
                                </dx:ASPxDropDownEdit>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Date From :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxDateEdit runat="server" ID="DateFrom" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy" NullText="Earliest" NullTextStyle-ForeColor="Black"></dx:ASPxDateEdit>
                            </div>
                        </td>
                        <td></td>

                        <td>Date To :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxDateEdit runat="server" ID="DateTo" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy" NullText="Latest" NullTextStyle-ForeColor="Black"></dx:ASPxDateEdit>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Comments :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxTextBox runat="server" ID="txtComments"></dx:ASPxTextBox>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </fieldset>
            <hr style="color: skyblue" />
            <dx:ASPxLabel ID="lblErrorMessage" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
            <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                <dx:ASPxButton ID="btnExportExcp_HdrLinCustMisMatch" runat="server" Text="Load Report" CssClass="btnPopup btn btn-success" CausesValidation="true" OnClick="btnExportExcp_HdrLinCustMisMatch_Click" />
                <dx:ASPxButton ID="btnPopUpClear" runat="server" Text="Clear" CssClass="btnPopup btn btn-warning" CausesValidation="false" AutoPostBack="false">
                    <ClientSideEvents Click="function(s, e) { CallBackCode.PerformCallback(CatClientEditCode.GetText()); }" />
                </dx:ASPxButton>
            </div>

            <asp:SqlDataSource runat="server" ID="DxGridSqlDataSource1"
                ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
            <dx:ASPxGridView ClientVisible="false" ID="gvExportData" ClientInstanceName="gvClientExportData" Theme="Office2010Black" runat="server"
                OnHtmlDataCellPrepared="gvExportData_HtmlDataCellPrepared"
                AutoGenerateColumns="false" DataSourceID="DxGridSqlDataSource1">
            </dx:ASPxGridView>
            <dx:ASPxGridViewExporter OnRenderBrick="gvExportDataExporter_RenderBrick" ID="gvExportDataExporter" GridViewID="gvExportData" runat="server"></dx:ASPxGridViewExporter>

        </div>
    </div>
    <script type="text/javascript">
        window.onload = function () {
            $('form').on('click', '.btnPopupClose', function () {
                $('.overlay').hide();
            });
        };

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //if (prm != null) {
        prm.add_endRequest(function (sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.overlay').show();
            }
        });
        prm.add_endRequest(GridOnEndCallBack);
        //};

    </script>
</asp:Content>

