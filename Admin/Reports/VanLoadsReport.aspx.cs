﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using System.Drawing;
using DevExpress.XtraPrinting;
using DevExpress.Export.Xl;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class Admin_Reports_VanLoadsReport : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
    ReportsDS ReportsDataSet = new ReportsDS();

    protected void Page_Init(object sender, EventArgs e)
    {
        //VldRptDateFrom.Value = new DateTime(1900, 1, 1);
        //VldRptDateTo.Value = new DateTime(9999, 12, 31);

        if (Session["Rpt"] != null && IsPostBack)
        {
            RptVwr.ReportSource = Session["Rpt"];
        }
    }

    protected void btnExportVanLoadsReport_Click(object sender, EventArgs e)
    {
        string oneStore = VldRptStrNo.Value.ToString();
        string oneRegister = VldRptRegNo.Value.ToString();
        string loadNo = VldRptLoadNo.Value.ToString();
        string trxType = VldRptLoadType.Value.ToString();

        DataTable dtExport = new DataTable();
        string qry = "";
        switch (trxType)
        {
            case "REQUEST":
                qry = "SELECT "
                    + " VLD_LDNUM, VLD_ITMNO, VLD_DESC, "
                    + " VLD_EXPDT, VLD_TXUOM AS VLD_STUOM, VLD_TXQTY AS VLD_STQTY, "
                    + " VLD_SREP, VLD_STRNO, VLD_REGNO, VLD_DRWNO, "
                    + " VLD_TRXDT, VLD_TRXTM, VLD_CRTDT, VLD_CRTIM, VLD_CRTBY, "
                    + " VLD_BCHNO "
                    + " FROM INV_VAN_LDTRX "
                    + " WHERE VLD_LDNUM = '" + loadNo + "' "
                    + " AND VLD_TTYPE = '" + trxType + "' ";
                break;
            case "LOAD":
                qry = "SELECT "
                    + " VLD_LDNUM, VLD_ITMNO, VLD_DESC, "
                    + " VLD_EXPDT, VLD_STUOM, VLD_STQTY, "
                    + " VLD_SREP, VLD_STRNO, VLD_REGNO, VLD_DRWNO, "
                    + " VLD_TRXDT, VLD_TRXTM, VLD_CRTDT, VLD_CRTIM, VLD_CRTBY, "
                    + " VLD_BCHNO "
                    + " FROM INV_VAN_LDTRX "
                    + " WHERE VLD_LDNUM = '" + loadNo + "' "
                    + " AND VLD_TTYPE = '" + trxType + "' "
                    + " AND VLD_STQTY <> 0 ";
                break;
            case "UNLOAD":
                qry = "SELECT "
                    + " VLD_LDNUM, VLD_ITMNO, VLD_DESC, "
                    + " VLD_EXPDT, VLD_STUOM, VLD_STQTY, "
                    + " VLD_SREP, VLD_STRNO, VLD_REGNO, VLD_DRWNO, "
                    + " VLD_TRXDT, VLD_TRXTM, VLD_CRTDT, VLD_CRTIM, VLD_CRTBY, "
                    + " VLD_BCHNO "
                    + " FROM INV_VAN_LDTRX "
                    + " WHERE VLD_LDNUM = '" + loadNo + "' "
                    + " AND VLD_TTYPE <> '" + "REQUEST" + "' ";
                break;
            default:
                break;
        }

        dtExport = dbHlpr.FetchData(qry);

        string MinimumDate = dbHlpr.FetchData("SELECT "
            + " COALESCE(MIN(VLD_CRTDT), CONVERT(DATETIME, '1900-01-01')) "
            + " FROM INV_VAN_LDTRX "
            + " WHERE VLD_LDNUM = '" + loadNo + "' "
            + " AND VLD_TTYPE <> 'REQUEST' ").Rows[0][0].ToString();
        string MaximumDate = dbHlpr.FetchData("SELECT "
            + " COALESCE(MAX(VLD_CRTDT), CONVERT(DATETIME, '2099-12-31')) "
            + " FROM INV_VAN_LDTRX "
            + " WHERE VLD_LDNUM = '" + loadNo + "' "
            + " AND VLD_TTYPE <> 'REQUEST' ").Rows[0][0].ToString();

        A4ReportTempData(dtExport, loadNo, MinimumDate, MaximumDate);
        if (ReportsDataSet.TEMP_TABLE.Rows.Count > 0 && ReportsDataSet.PARAM_TABLE.Rows.Count > 0)
        {
            lblErrorMessage.Text = "";
            ReportGen();
        }
        else
        {
            lblErrorMessage.Text = "No Record Found.";
            RptVwr.ReportSource = null;
            Session["Rpt"] = null;
        }
    }


    public void A4ReportTempData(DataTable DT_LINES, string TRX_NO, string MinimumDate, string MaximumDate)
    {
        string UnloadDate = "";

        for (int i = 0; i < DT_LINES.Rows.Count; i++)
        {
            DataRow dr = ReportsDataSet.TEMP_TABLE.NewRow();   //Tables["TEMP_TABLE"]. 

            if (DT_LINES.Rows[i]["VLD_LDNUM"] != null && DT_LINES.Rows[i]["VLD_LDNUM"] != "")
            {
                dr["Text01"] = DatabaseHelperClass.ConvertToArabic(DT_LINES.Rows[i]["VLD_LDNUM"].ToString()); // Trsansaction No
            }
            else
            {
                dr["Text01"] = "";
            }

            if (DT_LINES.Rows[i]["VLD_BCHNO"] != null && DT_LINES.Rows[i]["VLD_BCHNO"] != "")
            {
                dr["Text10"] = DT_LINES.Rows[i]["VLD_BCHNO"].ToString(); // Item No
            }
            else
            {
                dr["Text10"] = "";
            }

            if (DT_LINES.Rows[i]["VLD_ITMNO"] != null && DT_LINES.Rows[i]["VLD_ITMNO"] != "")
            {
                dr["Text11"] = DT_LINES.Rows[i]["VLD_ITMNO"].ToString(); // Item No
            }
            else
            {
                dr["Text11"] = "";
            }

            if (DT_LINES.Rows[i]["VLD_DESC"] != null && DT_LINES.Rows[i]["VLD_DESC"] != "")
            {
                dr["Text12"] = DT_LINES.Rows[i]["VLD_DESC"].ToString(); // +" " + dtgv.Rows[i]["DESC_LIN_2"].ToString(); //  Desc
            }
            else
            {
                dr["Text12"] = "";
            }

            if (DT_LINES.Rows[i]["VLD_STUOM"] != null && DT_LINES.Rows[i]["VLD_STUOM"] != "")
            {
                dr["Text14"] = DT_LINES.Rows[i]["VLD_STUOM"].ToString();
            }
            else
            {
                dr["Text14"] = "";
            }

            ////////////////////////// numbers starts here /////////////////////////////

            if (DT_LINES.Rows[i]["VLD_STQTY"] != null && DT_LINES.Rows[i]["VLD_STQTY"] != "")
            {
                double qty = Convert.ToDouble(DT_LINES.Rows[i]["VLD_STQTY"].ToString());
                if (qty > 0)
                {
                    dr["NUM04"] = qty.ToString("###,###.000");// QTY
                    dr["NUM05"] = 0.ToString("###,###.000");// QTY
                }
                else
                {
                    dr["NUM04"] = 0.ToString("###,###.000");// QTY
                    dr["NUM05"] = qty.ToString("###,###.000");// QTY
                    try
                    {
                        UnloadDate = Convert.ToDateTime(DT_LINES.Rows[i]["VLD_TRXDT"].ToString()).ToString("dd-MMM-yyyy");
                    }
                    catch (Exception xx)
                    {
                        UnloadDate = DT_LINES.Rows[i]["VLD_TRXDT"].ToString();
                    }
                }
            }
            else
            {
                dr["NUM04"] = "0.000";
                dr["NUM05"] = "0.000";
            }

            dr["NUM06"] = "0.000";
            DataTable dtSlsLines = dbHlpr.FetchData("SELECT SUM(TKL_MNQTY) "
                + " FROM SLS_TRX_TLIN "
                + " WHERE TKL_ITMNO = '" + dr["Text11"].ToString() + "' "
                + " AND TKL_DATE BETWEEN '" + MinimumDate + "' AND '" + MaximumDate + "' "
                + " AND TKL_STRNO = '" + DT_LINES.Rows[0]["VLD_STRNO"] + "' "
                + " AND TKL_REGNO = '" + DT_LINES.Rows[0]["VLD_REGNO"] + "' "
                + " AND TKL_TTYPE <> 'Void' ");
            if (dtSlsLines.Rows[0][0] != DBNull.Value)
            {
                dr["NUM06"] = dtSlsLines.Rows[0][0].ToString();
            }

            ReportsDataSet.TEMP_TABLE.Rows.Add(dr);
        }

        if (ReportsDataSet.TEMP_TABLE.Rows.Count > 0)
        {
            DataRow dr = ReportsDataSet.PARAM_TABLE.NewRow();

            dr["param01"] = Session["UserId"].ToString().ToUpper();
            dr["param02"] = dbHlpr.GetRptSeqNo();

            if (TRX_NO != "")
            {
                dr["param03"] = TRX_NO;
            }
            else
            {
                dr["param03"] = "";
            }

            dr["param04"] = "";
            if (TRX_NO != "")
            {
                DataTable dtRequestInfo = dbHlpr.FetchData("SELECT "
                    + " VLD_LDNUM, VLD_ITMNO, VLD_DESC, "
                    + " VLD_EXPDT, VLD_TXUOM AS VLD_STUOM, VLD_TXQTY AS VLD_STQTY, "
                    + " VLD_SREP, VLD_STRNO, VLD_REGNO, VLD_DRWNO, "
                    + " VLD_TRXDT, VLD_TRXTM, VLD_CRTDT, VLD_CRTIM, VLD_CRTBY, "
                    + " VLD_BCHNO "
                    + " FROM INV_VAN_LDTRX "
                    + " WHERE VLD_LDNUM = '" + TRX_NO + "' "
                    + " AND VLD_TTYPE = 'REQUEST' "
                    + " ORDER BY VLD_SEQNO ");
                if (dtRequestInfo.Rows.Count > 0)
                {
                    if (dtRequestInfo.Rows[0]["VLD_CRTBY"] != null && dtRequestInfo.Rows[0]["VLD_CRTBY"] != "")
                    {
                        dr["param04"] = dtRequestInfo.Rows[0]["VLD_CRTBY"].ToString().ToUpper();
                    }
                }
            }

            if (DT_LINES.Rows[0]["VLD_TRXDT"] != null && DT_LINES.Rows[0]["VLD_TRXDT"] != "")
            {
                try
                {
                    dr["param05"] = Convert.ToDateTime(MinimumDate).ToString("dd-MMM-yyyy");
                }
                catch (Exception xx)
                {
                    dr["param05"] = DT_LINES.Rows[0]["VLD_TRXDT"];
                }
            }
            else
            {
                dr["param05"] = "";
            }

            if (DT_LINES.Rows[0]["VLD_TRXTM"] != null && DT_LINES.Rows[0]["VLD_TRXTM"] != "")
            {
                try
                {
                    dr["param09"] = Convert.ToDateTime(DT_LINES.Rows[0]["VLD_TRXTM"].ToString()).ToString("HH:mm");
                }
                catch (Exception xx)
                {
                    dr["param09"] = DT_LINES.Rows[0]["VLD_TRXTM"];
                }
            }
            else
            {
                dr["param09"] = "";
            }

            if (DT_LINES.Rows[0]["VLD_STRNO"] != null && DT_LINES.Rows[0]["VLD_STRNO"] != "")
            {
                dr["param06"] = DT_LINES.Rows[0]["VLD_STRNO"];
            }
            else
            {
                dr["param06"] = "";
            }

            if (DT_LINES.Rows[0]["VLD_REGNO"] != null && DT_LINES.Rows[0]["VLD_REGNO"] != "")
            {
                dr["param07"] = DT_LINES.Rows[0]["VLD_REGNO"];
            }
            else
            {
                dr["param07"] = "";
            }

            if (DT_LINES.Rows[0]["VLD_DRWNO"] != null && DT_LINES.Rows[0]["VLD_DRWNO"] != "")
            {
                dr["param08"] = DT_LINES.Rows[0]["VLD_DRWNO"];
            }
            else
            {
                dr["param08"] = "";
            }

            if (DT_LINES.Rows[0]["VLD_CRTDT"] != null && DT_LINES.Rows[0]["VLD_CRTDT"] != "")
            {
                try
                {
                    dr["param10"] = Convert.ToDateTime(DT_LINES.Rows[0]["VLD_CRTDT"].ToString()).ToString("dd-MMM-yyyy");
                }
                catch (Exception xx)
                {
                    dr["param10"] = DT_LINES.Rows[0]["VLD_CRTDT"];
                }
            }
            else
            {
                dr["param10"] = "";
            }

            dr["param11"] = UnloadDate;

            ReportsDataSet.PARAM_TABLE.Rows.Add(dr);
        }
    }

    public void ReportGen()
    {
        try
        {
            ReportDocument RptDoc = new ReportDocument();
            if (VldRptLoadType.Value == "REQUEST")
            {
                RptDoc.Load(Server.MapPath("~/RptFiles/CR_VAN_LDREQ_A4.rpt"));
            }
            else if (VldRptLoadType.Value == "LOAD")
            {
                RptDoc.Load(Server.MapPath("~/RptFiles/CR_VAN_LOAD_A4.rpt"));
            }
            else if (VldRptLoadType.Value == "UNLOAD")
            {
                RptDoc.Load(Server.MapPath("~/RptFiles/CR_VAN_ULOAD_A4.rpt"));
            }
            else
            {
                throw new Exception("Forsan Vansales Exception: Invalid Vanload Trx Type.");
            }
            RptDoc.SetDataSource(ReportsDataSet);
            RptVwr.ReportSource = RptDoc;
            Session["Rpt"] = RptDoc;
            //CrystalDecisions.CrystalReports.Engine.TextObject TXTCOMNTS_BOX = RptDoc.ReportDefinition.ReportObjects["TXT_CMMNT"] as CrystalDecisions.CrystalReports.Engine.TextObject;

            //TXTCOMNTS_BOX.ObjectFormat.EnableSuppress = txtComments.Text.Trim().Length <= 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void VldRptStrNo_Init(object sender, EventArgs e)
    {
        DataTable dtStores = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE WHERE STM_CODE IN (" + Session["userBranchCode"].ToString() + ") ");

        VldRptStrNo.DataSource = dtStores;
        VldRptStrNo.ValueField = "STM_CODE";
        VldRptStrNo.TextField = "STM_NAME";
        VldRptStrNo.DataBind();
    }
    protected void VldRptRegNo_Init(object sender, EventArgs e)
    {
        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + Session["userBranchCode"].ToString() + ") ");

        VldRptRegNo.DataSource = dtRegisters;
        VldRptRegNo.ValueField = "REG_CODE";
        VldRptRegNo.TextField = "REG_NAME";
        VldRptRegNo.DataBind();
    }

    protected void VldRptLoadNo_Init(object sender, EventArgs e)
    {
        DataTable dtRegisters = dbHlpr.FetchData("SELECT DISTINCT VLD_LDNUM FROM INV_VAN_LDTRX WHERE VLD_STRNO IN (" + Session["userBranchCode"].ToString() + ") ");

        VldRptLoadNo.DataSource = dtRegisters;
        VldRptLoadNo.ValueField = "VLD_LDNUM";
        VldRptLoadNo.TextField = "VLD_LDNUM";
        VldRptLoadNo.DataBind();
    }

    protected void VldRptStrNo_ValueChanged(object sender, EventArgs e)
    {
        ASPxComboBox value = (ASPxComboBox)sender;
        string strNo = value.Value == null ? "" : value.Value.ToString();
        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + strNo + ") ");

        VldRptRegNo.DataSource = dtRegisters;
        VldRptRegNo.ValueField = "REG_CODE";
        VldRptRegNo.TextField = "REG_NAME";
        VldRptRegNo.DataBind();
    }

    protected void VldRptRegNo_ValueChanged(object sender, EventArgs e)
    {
        string strNo = VldRptStrNo.Value == null ? "" : VldRptStrNo.Value.ToString();
        string regNo = VldRptRegNo.Value == null ? "" : VldRptRegNo.Value.ToString();
        DataTable dtRegisters = dbHlpr.FetchData("SELECT DISTINCT VLD_LDNUM "
            + " FROM INV_VAN_LDTRX "
            + " WHERE VLD_STRNO IN (" + strNo + ") "
            + " AND VLD_REGNO IN (" + regNo + ") ");

        VldRptLoadNo.DataSource = dtRegisters;
        VldRptLoadNo.ValueField = "VLD_LDNUM";
        VldRptLoadNo.TextField = "VLD_LDNUM";
        VldRptLoadNo.DataBind();
    }
}