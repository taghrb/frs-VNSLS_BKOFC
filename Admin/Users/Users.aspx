﻿<%@ Page Title="Manage Users" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="Users.aspx.cs" Inherits="Admin_Users_Default" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content4" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <script type="text/javascript">

        function grid_customizationWindowCloseUp(s, e) {
            //if(FromGridView.IsCustomizationWindowVisible())
            FromGridView.ShowCustomizationWindow();
        }

        $(function () {
            $('.testing123').dialog();
        });


        // Code to create a custom multiselect ComboBox, Dropdown
        var textSeparator = ","; // Between each checked item in ComboBox
        function OnListBoxSelectionChanged(listBox, args) {
            if (listBox.name.indexOf("lstBxAssgndStoresItems") != -1) {
                UpdateText(DraClientAssgndStoresNo, checkAssgndStoresListBox);
            }
        }
        function UpdateText(ddObj, lstBxObj) {
            var selectedItems = lstBxObj.GetSelectedItems();
            ddObj.SetText(GetSelectedItemsText(selectedItems));
        }
        function SynchronizeListBoxValues(dropDown, args) {
            var texts = dropDown.GetText().split(textSeparator);
            if (dropDown.name.indexOf("AssgndStoresNo") != -1) {
                checkAssgndStoresListBox.UnselectAll();
                checkAssgndStoresListBox.SelectValues(texts);
                UpdateText(DraClientAssgndStoresNo, checkAssgndStoresListBox);
            }
        }
        function GetSelectedItemsText(items) {
            var texts = [];
            for (var i = 0; i < items.length; i++)
                texts.push(items[i].value);
            return texts.join(textSeparator);
        }
        function GetValuesByTexts(texts, chkLstBx) {
            var actualValues = [];
            var item;
            for (var i = 0; i < texts.length; i++) {
                item = chkLstBx.FindItemByText(texts[i]);
                if (item != null)
                    actualValues.push(item.value);
            }
            return actualValues;
        }


    </script>

</asp:Content>
<asp:Content ID="contentTopHere1" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
            <li>
                <asp:LinkButton ID="LinkBtnPDF" OnClick="LinkBtnPDF_Click" runat="server">
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonXLX" OnClick="LinkButtonXLX_Click" runat="server">
                <img src="~/img/FilesIcons/excel.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonXxls" OnClick="LinkButtonXxls_Click" runat="server">
                <img src="~/img/FilesIcons/xlsx.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonRtf" OnClick="LinkButtonRtf_Click" runat="server">
                <img src="~/img/FilesIcons/rtf.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonCvs" OnClick="LinkButtonCvs_Click" runat="server">
                <img src="~/img/FilesIcons/cvs.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonWord" OnClick="LinkButtonRtf_Click" runat="server">
                <img src="~/img/FilesIcons/word.png" runat="server" />
                </asp:LinkButton>
            </li>

            <li>
                <asp:LinkButton ID="LinkButtonHtml" OnClick="LinkButtonHtml_Click" runat="server">
                <img src="~/img/icons/searchtop.jpg" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton6" OnClick="LinkButton3_Click" runat="server">               
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton7" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/word.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton8" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/excel.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton9" OnClick="LinkButton3_Click" OnClientClick="window.print();" runat="server">
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton10" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/torrent.png" runat="server" />
                </asp:LinkButton>
            </li>
        </ul>
        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvUserInquery" ExportedRowType="All" FileName="sdsdd">
        </dx:ASPxGridViewExporter>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">

    <div class="formDiv">
        <div class="formHeaderDiv">
            <h3>Users </h3>
        </div>
        <asp:SqlDataSource runat="server" ID="DxGridSqlDataSource1"
            ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
        <dx:ASPxGridView ID="gvUserInquery" CssClass="FromGridView" ClientInstanceName="FromGridView" Theme="Office2010Black" runat="server" AutoGenerateColumns="False" KeyFieldName="USR_ID"
            DataSourceID="DxGridSqlDataSource1" OnCustomButtonCallback="gvUserInquery_CustomButtonCallback"
            OnRowCommand="gvUserInquery_RowCommand"
            OnStartRowEditing="gvUserInquery_StartRowEditing"
            OnDataBound="gvUserInquery_DataBound"
            OnBeforeGetCallbackResult="gvUserInquery_BeforeGetCallbackResult"
            OnRowDeleting="gvUserInquery_RowDeleting">

            <SettingsContextMenu Enabled="True" EnableColumnMenu="True" EnableRowMenu="True"></SettingsContextMenu>
            <SettingsSearchPanel Visible="False" CustomEditorID="CustomSearchPanelTxtBx" />
            <Columns>
                <dx:GridViewCommandColumn ShowNewButtonInHeader="false" ShowSelectButton="true" Width="110" Caption=" " AllowDragDrop="False" VisibleIndex="1"></dx:GridViewCommandColumn>
                <dx:GridViewCommandColumn ShowNewButtonInHeader="true" ShowEditButton="true" Width="110" VisibleIndex="2" AllowDragDrop="False"></dx:GridViewCommandColumn>
                <dx:GridViewCommandColumn ShowNewButtonInHeader="false" ShowDeleteButton="true" Width="130" Caption=" " AllowDragDrop="False" VisibleIndex="3"></dx:GridViewCommandColumn>

                <dx:GridViewDataTextColumn FieldName="USR_ID" Caption="User ID" VisibleIndex="4">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="USR_NAME" Caption="Full Name" VisibleIndex="5">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="USR_INITL" Caption="Initial" VisibleIndex="6">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="USR_EMPNO" Caption="Employee No." VisibleIndex="8">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="STM_NAME" Caption="Store" VisibleIndex="9">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="USR_DTGOV" Caption="Editing Masters" VisibleIndex="10">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                    <DataItemTemplate>
                        <dx:ASPxLabel runat="server" Text='<%# Eval("USR_DTGOV").ToString().Equals("1") ? "Allowed" : "Not Allowed" %>'>
                        </dx:ASPxLabel>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="USR_SYACS" Caption="System Access" VisibleIndex="11">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                    <DataItemTemplate>
                        <dx:ASPxLabel runat="server" Text='<%# Eval("USR_SYACS").ToString().Equals("1") ? "Allowed" : "Not Allowed" %>'>
                        </dx:ASPxLabel>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="USR_VSACS" Caption="Vansales App Access" VisibleIndex="12">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                    <DataItemTemplate>
                        <dx:ASPxLabel runat="server" Text='<%# Eval("USR_VSACS").ToString().Equals("1") ? "Allowed" : "Not Allowed" %>'>
                        </dx:ASPxLabel>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="USR_MGACS" Caption="Manager App Access" VisibleIndex="13">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                    <DataItemTemplate>
                        <dx:ASPxLabel runat="server" Text='<%# Eval("USR_MGACS").ToString().Equals("1") ? "Allowed" : "Not Allowed" %>'>
                        </dx:ASPxLabel>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="USR_BOACS" Caption="Backoffice Access" VisibleIndex="14">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                    <DataItemTemplate>
                        <dx:ASPxLabel runat="server" Text='<%# Eval("USR_BOACS").ToString().Equals("1") ? "Allowed" : "Not Allowed" %>'>
                        </dx:ASPxLabel>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="MNU_NAME" Caption="Menu" VisibleIndex="15">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="DPT_NAME" Caption="Department" VisibleIndex="16">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="USR_GCMID" Caption="GCM Id" VisibleIndex="17" Visible="false">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>

            </Columns>

            <Settings HorizontalScrollBarMode="Auto" />

            <SettingsCommandButton>
                <NewButton ButtonType="Button" Text="New" Styles-Style-CssClass="btna btn btn-primary  btn-sm">
                    <Image runat="server" Url="~/img/icons/white-icn/Add.png" Width="15px"></Image>
                </NewButton>

                <SelectButton ButtonType="Button" Text="View" Styles-Style-CssClass="btna btn btn-success  btn-sm">
                    <Image ToolTip="View button here" Url="~/img/icons/white-icn/eyee1.png" Width="15px" />
                </SelectButton>

                <EditButton ButtonType="Button" Text="Edit" Styles-Style-CssClass="btna btn btn-info  btn-sm">
                    <Image runat="server" Url="~/img/icons/white-icn/Pen Tool.png" Width="15px"></Image>
                </EditButton>

                <DeleteButton ButtonType="Button" Text="Delete" Styles-Style-CssClass="btna btn btn-danger btn-sm btn-danger-variant">
                    <Image runat="server" Url="~/img/icons/white-icn/Trash.png" Width="15px"></Image>
                </DeleteButton>

                <CancelButton ButtonType="Button" Text="Close" Styles-Style-CssClass="btnPopup btn btn-primary">
                </CancelButton>
                <UpdateButton ButtonType="Button" Text="Update">
                </UpdateButton>
            </SettingsCommandButton>
            <SettingsEditing EditFormColumnCount="1" />

            <SettingsPopup>
                <EditForm VerticalAlign="WindowCenter" AllowResize="True" Modal="True" HorizontalAlign="WindowCenter"></EditForm>
            </SettingsPopup>

            <SettingsPager Position="Bottom">
                <PageSizeItemSettings Caption="Records per Page" Items="10, 20, 50, 100" ShowAllItem="true" Position="Right" Visible="true">
                </PageSizeItemSettings>
            </SettingsPager>
            <Styles>
                <CommandColumn Spacing="0px" Wrap="False" />
                <EditFormDisplayRow HorizontalAlign="Center" VerticalAlign="Middle"></EditFormDisplayRow>
                <AlternatingRow CssClass="alterRowGV" Enabled="True"></AlternatingRow>
                <Row CssClass="rowHeight"></Row>
            </Styles>
            <Settings ShowGroupPanel="true" VerticalScrollBarMode="Auto" />
            <SettingsLoadingPanel Mode="ShowOnStatusBar" />
            <SettingsBehavior EnableCustomizationWindow="true" AllowDragDrop="true" ConfirmDelete="True" ColumnResizeMode="Control" />
            <ClientSideEvents ColumnStartDragging="grid_customizationWindowCloseUp" EndCallback="GridOnEndCallBack" />

            <Templates>
                <EditForm>
                    <div class="overlay">
                        <div class="FormPopup">
                            <div class="formHeaderDiv">
                                <h3>User</h3>
                            </div>
                            <fieldset>
                                <legend></legend>
                                <table>
                                    <tr>
                                        <td>User Id : </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtCode" ClientInstanceName="txtClientCode" runat="server" Width="170px" Text='<%# Bind("USR_ID") %>' TabIndex="1" MaxLength="10">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="User Id is Required" />
                                                    </ValidationSettings>
                                                    <ClientSideEvents LostFocus="function(s, e) { ClBk_CodeLostFocus.PerformCallback(txtClientCode.GetText()); }" />
                                                </dx:ASPxTextBox>
                                                <dx:ASPxCallback ID="ClBk_CodeLostFocus" runat="server" ClientInstanceName="ClBk_CodeLostFocus" OnCallback="ClBk_CodeLostFocus_Callback">
                                                    <ClientSideEvents CallbackComplete="function(s,e) { OnCallbackComplete(s, e); }" />
                                                </dx:ASPxCallback>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Emp. No. : </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtEmpNo" ClientInstanceName="txtClientEmpNo" runat="server" Width="170px" Text='<%# Bind("USR_EMPNO") %>' TabIndex="2" MaxLength="15">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RegularExpression ValidationExpression="[A-Z0-9\-]{0,15}" ErrorText="Invalid Employee Number." />
                                                        <RequiredField IsRequired="true" ErrorText="Employee # is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                        <td>Initials : </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtInitials" ClientInstanceName="txtClientInitials" runat="server" Width="170px" Text='<%# Bind("USR_INITL") %>' TabIndex="3" MaxLength="5">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RegularExpression ValidationExpression="[A-Z\-]{0,5}" ErrorText="Invalid Initials." />
                                                        <RequiredField IsRequired="true" ErrorText="User Initials is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Name: </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtUserName" ClientInstanceName="txtClientUserName" runat="server" Width="170px" Text='<%# Bind("USR_NAME") %>' TabIndex="4" MaxLength="30">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Name is Required" />
                                                    </ValidationSettings>
                                                    <NullTextStyle Font-Size="Small" />
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                        <td>Password: </td>
                                        <td>
                                            <dx:ASPxTextBox ID="txtPass" Password="true" ClientInstanceName="txtClientPass" runat="server" Text='<%# Bind("USR_PSWRD") %>' MinLength="6" Width="170px" TabIndex="5" MaxLength="8">
                                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RequiredField IsRequired="false" ErrorText="Password is Required" />
                                                </ValidationSettings>
                                                <NullTextStyle Font-Size="Small" />
                                            </dx:ASPxTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Department :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmBxDept" ClientInstanceName="cmBxClientDept" DataSourceID="CmBxDeptSqlDataSource1" ValueField="DPT_CODE" TextField="DPT_NAME" runat="server" Width="170px" ValueType="System.String" Value='<%# Bind("USR_DEPT") %>' TabIndex="6">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Department is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="CmBxDeptSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM STP_USR_DEPTS"></asp:SqlDataSource>
                                            </div>
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Branch/Store :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmBxBrnch" ClientInstanceName="cmBxClientBrnch" DataSourceID="CmBxBrnchSqlDataSource1" ValueField="STM_CODE" TextField="STM_NAME" runat="server" Width="170px" ValueType="System.String" Value='<%# Bind("USR_PRMBR") %>' TabIndex="6">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Store is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="CmBxBrnchSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM STP_MSTR_STORE"></asp:SqlDataSource>
                                            </div>
                                        </td>
                                        <td>Assigned Stores :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxDropDownEdit ID="AssgndStoresNo" OnInit="AssgndStoresNo_Init" ClientInstanceName="DraClientAssgndStoresNo" CssClass="multiselect_combobox" runat="server" AnimationType="Auto">
                                                    <DropDownWindowTemplate>
                                                        <dx:ASPxListBox Width="170px" ID="lstBxAssgndStoresItems" ClientInstanceName="checkAssgndStoresListBox" SelectionMode="CheckColumn" runat="server">
                                                            <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged" />
                                                        </dx:ASPxListBox>
                                                    </DropDownWindowTemplate>
                                                    <ClientSideEvents TextChanged="SynchronizeListBoxValues" DropDown="SynchronizeListBoxValues" />
                                                </dx:ASPxDropDownEdit>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Menu :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmBxMenu" ClientInstanceName="cmBxClientMenu" DataSourceID="CmBxMenuSqlDataSource1" ValueField="MNU_CODE" TextField="MNU_NAME" runat="server" MaxLength="15" Width="170px" ValueType="System.String" Value='<%# Bind("USR_MNCOD") %>' TabIndex="7">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Menu is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="CmBxMenuSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM STP_USR_MENUS"></asp:SqlDataSource>
                                            </div>
                                        </td>
                                        <td>Allow Edit Masters :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmBxEditMasters" ClientInstanceName="cmBxClientEditMasters" runat="server" SelectedIndex='<%# Eval("USR_DTGOV") != null ? Eval("USR_DTGOV").ToString().Trim().Equals("1") ? 0 : 1 : 1 %>' Width="170px" ValueType="System.Char" TabIndex="8">
                                                    <Items>
                                                        <dx:ListEditItem Text="Yes" Value="1" />
                                                        <dx:ListEditItem Text="No" Value="0" />
                                                    </Items>
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Edit Masters value is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>System Access: </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmBxSystemAccess" ClientInstanceName="cmBxClientSystemAccess" runat="server" SelectedIndex='<%# Eval("USR_SYACS") != null ? Eval("USR_SYACS").ToString().Trim().Equals("1") ? 0 : 1 : 1 %>' Width="170px" ValueType="System.Char" TabIndex="9">
                                                    <Items>
                                                        <dx:ListEditItem Text="Yes" Value="1" />
                                                        <dx:ListEditItem Text="No" Value="0" />
                                                    </Items>
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="System Access is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>
                                        <td>VS App Access: </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmBxVansalesAccess" ClientInstanceName="cmBxClientVansalesAccess" runat="server" SelectedIndex='<%# Eval("USR_VSACS") != null ? Eval("USR_VSACS").ToString().Trim().Equals("1") ? 0 : 1 : 1 %>' Width="170px" ValueType="System.Char" TabIndex="10">
                                                    <Items>
                                                        <dx:ListEditItem Text="Yes" Value="1" />
                                                        <dx:ListEditItem Text="No" Value="0" />
                                                    </Items>
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="VS App Access is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Manager App Access: </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmBxMgrAppAccess" ClientInstanceName="cmBxClientMgrAppAccess" runat="server" SelectedIndex='<%# Eval("USR_MGACS") != null ? Eval("USR_MGACS").ToString().Trim().Equals("1") ? 0 : 1 : 1 %>' Width="170px" ValueType="System.Char" TabIndex="11">
                                                    <Items>
                                                        <dx:ListEditItem Text="Yes" Value="1" />
                                                        <dx:ListEditItem Text="No" Value="0" />
                                                    </Items>
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Manager App Access is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>
                                        <td>Backoffice Access: </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmBxBkOfcAccess" ClientInstanceName="cmBxClientBkOfcAccess" runat="server" SelectedIndex='<%# Eval("USR_BOACS") != null ? Eval("USR_BOACS").ToString().Trim().Equals("1") ? 0 : 1 : 1 %>' Width="170px" ValueType="System.Char" TabIndex="12">
                                                    <Items>
                                                        <dx:ListEditItem Text="Yes" Value="1" />
                                                        <dx:ListEditItem Text="No" Value="0" />
                                                    </Items>
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Backoffice Access is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                            <hr style="color: skyblue" />
                            <div style="padding: 5px 10px;">
                                <dx:ASPxLabel ID="lblErrorMessage" runat="server" ForeColor="Red" />
                            </div>
                            <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                                <dx:ASPxButton ID="btnAdd1" runat="server" Text="Save" CssClass="btnPopup btn btn-success" CausesValidation="true" OnClick="btnAdd1_Click" TabIndex="13" />
                                <dx:ASPxButton ID="btnPopUpClear" runat="server" Text="Cancel" CssClass="btnPopup btn btn-warning" CausesValidation="false" AutoPostBack="false">
                                    <ClientSideEvents Click="function(s, e) { CallBackCode.PerformCallback(txtClientCode.GetText()); }" />
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="btnPopUpDelete" runat="server" CommandArgument='<%# Eval("USR_ID") %>' CommandName="PopUpDelete" Text="Delete" CssClass="btnPopup btn btn-danger btn-danger-variant" CausesValidation="false">
                                    <ClientSideEvents Click="function(s, e) { 
                                                if (!confirm('Confirm Delete?')) { 
                                                    e.processOnServer = false;  
                                                } 
                                        }" />
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="btnPopUpPrint" runat="server" Text="Print" CssClass="btnPopup btn btn-info btn-info-variant" AutoPostBack="false" CausesValidation="false">
                                    <ClientSideEvents Click="function(s, e) { printDoc(s, e); }" />
                                </dx:ASPxButton>
                                <dx:ASPxGridViewTemplateReplacement ID="btnPopUpClose" ReplacementType="EditFormCancelButton" runat="server" />
                            </div>
                        </div>
                    </div>
                </EditForm>
            </Templates>

            <Templates>
                <StatusBar>
                    <table>
                        <tr>
                            <td>
                                <%--<dx:ASPxButton ID="btnNewRecord" runat="server" Text="Add New Item" AutoPostBack="false">
                                    <ClientSideEvents Click="function (s, e){ FromGridView.AddNewRow();  }" />
                                </dx:ASPxButton>--%>
                            </td>
                            <td>Search: </td>
                            <td>
                                <dx:ASPxButtonEdit runat="server" ID="CustomSearchPanelTxtBx" />
                            </td>
                            <td>&nbsp;</td>
                            <td>Search In:</td>
                            <td>
                                <dx:ASPxComboBox AutoPostBack="true" ID="DDLFilterByColumn" runat="server" OnSelectedIndexChanged="DDLFilterByColumn_SelectedIndexChanged">
                                    <Items>
                                        <dx:ListEditItem Text="All" Value="*" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>

                        </tr>
                    </table>
                </StatusBar>
            </Templates>

        </dx:ASPxGridView>

        </section>       
           <%-- <asp:SqlDataSource runat="server" ID="ItemSQLDS" ConnectionString='<%$ ConnectionStrings:MyConn %>' SelectCommand="SELECT [ITEM_NO], [DESCRP], [DESCRP2], [TRACK], [BARCODE], [CATG], [SUB_CATG], [USR_DEF_1], [USR_DEF_2], [USR_DEF_3], [USR_DEF_4], [STK_UNIT],[STK_UNIT2], [REG_PRC], [ALIAS] FROM [IM_STP_ITEM]" ProviderName='<%$ ConnectionStrings:MyConn.ProviderName %>'></asp:SqlDataSource>--%>
    </div>

    <script type="text/javascript">
        window.onload = function () {
            $('form').on('click', '.btnPopupClose', function () {
                //$('.overlay').hide();
            });
        };

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //if (prm != null) {
        prm.add_endRequest(function (sender, e) {
            //if (sender._postBackSettings.panelsToUpdate != null) {            
            //$('.overlay').show();
            //}
        });
        //};


        function OnTestValidation(s, e) {
            //var testData = e.value;
            //if (!testData) {
            lblError.SetVisible(!e.isValid);
            //  return;
        }

        function OnCallbackComplete(s, e) {
            txtClientEmpNo.SetText("");
            txtClientInitials.SetText("");
            txtClientUserName.SetText("");
            txtClientPass.SetText("");
            cmBxClientDept.SetValue("");
            cmBxClientBrnch.SetValue("");
            cmBxClientMenu.SetValue("");
            cmBxClientEditMasters.SetValue("0");
            cmBxClientSystemAccess.SetValue("0");
            cmBxClientVansalesAccess.SetValue("0");
            cmBxClientMgrAppAccess.SetValue("0");
            cmBxClientBkOfcAccess.SetValue("0");

            var jsonData = JSON.parse(e.result);
            console.log(jsonData);
            if (jsonData.length > 0) {
                txtClientEmpNo.SetText(jsonData[0]["USR_EMPNO"]);
                txtClientInitials.SetText(jsonData[0]["USR_INITL"]);
                txtClientUserName.SetText(jsonData[0]["USR_NAME"]);

                cmBxClientDept.SetValue(jsonData[0]["USR_DEPT"]);
                cmBxClientBrnch.SetValue(jsonData[0]["USR_PRMBR"]);
                cmBxClientMenu.SetValue(jsonData[0]["USR_MNCOD"]);
                cmBxClientEditMasters.SetValue(jsonData[0]["USR_DTGOV"]);
                cmBxClientSystemAccess.SetValue(jsonData[0]["USR_SYACS"]);
                cmBxClientVansalesAccess.SetValue(jsonData[0]["USR_VSACS"]);
                cmBxClientMgrAppAccess.SetValue(jsonData[0]["USR_MGACS"]);
                cmBxClientBkOfcAccess.SetValue(jsonData[0]["USR_BOACS"]);
            }
        }
    </script>
</asp:Content>
