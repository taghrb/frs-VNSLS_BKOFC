﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress;
using System.Data;
using DevExpress.Web.ASPxTreeList;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

public partial class UserAccess : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        gridExport.WriteXlsToResponse();
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        gridExport.WriteXlsxToResponse();
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }//end method

    protected void ACB_CodeCheck_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        string menuCode = txtCode.Text.Trim().ToUpper();
        DataTable dtMenu = dbHlpr.FetchData("SELECT * FROM STP_USR_MENUS WHERE MNU_CODE = '" + menuCode + "'");
        Dictionary<string, object> returnObj = new Dictionary<string, object>();

        if (dtMenu.Rows.Count > 0)
        {
            DataTable dtTree = dbHlpr.FetchData("SELECT * FROM STP_LNK_MNACS WHERE MNA_MNCOD = '" + menuCode + "'");
            DataTable dtPageIds = dbHlpr.FetchData("SELECT PAG_ID FROM STP_MNU_PAGES");

            returnObj.Add("MenuInfo", dtMenu.Rows);
            returnObj.Add("TreeInfo", dtTree.Rows);
            returnObj.Add("PageIds", dtPageIds.Rows);
        }
        else
        {
            returnObj.Add("MenuInfo", null);
            returnObj.Add("TreeInfo", null);
            returnObj.Add("PageIds", null);
        }

        e.Result = JsonConvert.SerializeObject(returnObj);
    }//end method

    protected void btnClear_Click(object sender, EventArgs e)
    {

    }//end method

    protected void btnSave_Click(object sender, EventArgs e)
    {

    }//end method

    protected void btnPuAddMenuCode_Click(object sender, EventArgs e)
    {

    }//end method

    protected void treeList_DataBound(object sender, EventArgs e)
    {

    }//end method

    protected void treeList_CustomDataCallback(object sender, DevExpress.Web.ASPxTreeList.TreeListCustomDataCallbackEventArgs e)
    {

    }//end method

    protected void cb_Init(object sender, EventArgs e)
    {
        //     gridUserMenu.BeginUpdate();
        //     gridUserMenu.ClearSort();
        ////     gridUserMenu.GroupBy(gridUserMenu.Columns["GROUPCODE"]);
        //     gridUserMenu.EndUpdate();
        //     gridUserMenu.ExpandAll();

        // (gridUserMenu.Columns[0] as GridViewDataColumn).GroupBy();
        //// gridUserMenu.Columns[0] as GridViewDataColumn).GroupBy();
        // //ASPXGridView  view = gridUserMenu;
        // ////(GridViewDataColumn)gridUserMenu.Columns["GROUPCODE"].GroupBy();
        // ////gridUserMenu.Columns[0].Grid.AllColumns.GroupBy();
        // //gridUserMenu.GroupBy(gridUserMenu.Columns["GROUPCODE"], 0);
        // gridUserMenu.Settings.ShowGroupedColumns = true;
        // gridUserMenu.Settings.ShowGroupPanel = false;
    }

    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ///////////////////////////////////////////////////////
        ////// add menu code fire in MNU Database Table ///////
        ///////////////////////////////////////////////////////
        if (txtCode.Text == "" || txtName.Text == "")
        {
            //lblMessage.ForeColor = System.Drawing.Color.Red;
            //lblMessage.Text = "Error: Empty Fields. Please enter Data.";
            return;
        }
        string MnuCode = txtCode.Text.ToUpper();
        string MnuName = txtName.Text;

        DataTable dt = dbHlpr.FetchData("SELECT * FROM STP_USR_MENUS WHERE MNU_CODE = '" + MnuCode + "'");
        if (dt.Rows.Count > 0)
        {
            dbHlpr.ExecuteNonQuery("UPDATE STP_USR_MENUS SET MNU_NAME = '" + MnuName + "' WHERE MNU_CODE = '" + MnuCode + "'");
            dbHlpr.ExecuteNonQuery("DELETE FROM STP_LNK_MNACS WHERE MNA_MNCOD = '" + MnuCode + "'");

            lblMessage.ForeColor = System.Drawing.Color.Green;
            lblMessage.Text = "Menu updated successfully";
        }
        else
        {
            int insertId = int.Parse(dbHlpr.ExecuteScalarQuery("INSERT INTO STP_USR_MENUS (MNU_CODE, MNU_NAME) OUTPUT INSERTED.MNU_KEY VALUES ('" + MnuCode + "', '" + MnuName + "')").ToString());
            if (insertId == 1 && insertId != 0)
            {
                lblMessage.ForeColor = System.Drawing.Color.Green;
                lblMessage.Text = "Successfully Inserted";
            }
            else
            {
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblMessage.Text = "Alert: Something went wrong. Please try again";
                return;
            }
        }

        //////////////////////////////////////////////////////

        ASPxTreeList2.ExpandAll();

        //////////////////////////////////////////////////////
        ///////////////// Handle Tree here now ///////////////
        //////////////////////////////////////////////////////

        List<TreeListNode> selectedNodes = ASPxTreeList2.GetSelectedNodes();
        foreach (DevExpress.Web.ASPxTreeList.TreeListNode tempNode in selectedNodes)
        {
            //get data of node
            string PAG_ID = tempNode.GetValue("PAG_ID").ToString();
            string PAG_NAME = tempNode.GetValue("PAG_NAME").ToString();
            string PAG_CODE = tempNode.GetValue("PAG_CODE").ToString();
            string PAG_PARNT = tempNode.GetValue("PAG_PARNT").ToString();
            string PAG_TYPE = tempNode.GetValue("PAG_TYPE").ToString();

            InsertTreeData(tempNode, MnuCode);
        }
    }//end method

    private void InsertTreeData(DevExpress.Web.ASPxTreeList.TreeListNode currentNode, string MnuCode)
    {
        if (currentNode.ParentNode != null)
        {
            InsertTreeData(currentNode.ParentNode, MnuCode);
        }

        InsertNodeInDbIfNotExists(currentNode, MnuCode);
    }

    private void InsertNodeInDbIfNotExists(DevExpress.Web.ASPxTreeList.TreeListNode nodeToInsert, string MnuCode)
    {
        string menuCode = MnuCode;
        //if (nodeToInsert.GetValue("PAG_ID") != null)
        if (nodeToInsert.GetValue("PAG_CODE") != null)
        {
            string pageId = nodeToInsert.GetValue("PAG_ID").ToString();
            string pageName = nodeToInsert.GetValue("PAG_NAME").ToString();
            string pageCode = nodeToInsert.GetValue("PAG_CODE").ToString();
            string pageType = nodeToInsert.GetValue("PAG_TYPE").ToString();

            //DataTable dt = dbHlpr.FetchData("SELECT MPG_KEY FROM STP_LNK_MNACS WHERE MNA_MNCOD = '" + menuCode + "' AND MPG_PGID = '" + pageId + "' AND MNA_PGCOD  = '" + pageCode + "'");
            DataTable dt = dbHlpr.FetchData("SELECT MNA_KEY FROM STP_LNK_MNACS WHERE MNA_MNCOD = '" + menuCode + "' AND MNA_PGCOD  = '" + pageCode + "'");
            if (dt.Rows.Count == 0)
            {
                dbHlpr.ExecuteNonQuery("INSERT INTO STP_LNK_MNACS (MNA_KEY, MNA_MNCOD, MNA_PGCOD, MNA_PGID, MNA_PGTYP, MNA_PGNAM) VALUES ("
                    + "'" + (menuCode + pageCode) + "', "
                    + "'" + menuCode + "', "
                    + "'" + pageCode + "', "
                    + "'" + pageId + "', "
                    + "'" + pageType + "', "
                    + "'" + pageName + "' )"
                    );
            }
        }
    }
}//end class