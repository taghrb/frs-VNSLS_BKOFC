﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="MenuItems.aspx.cs" Inherits="MenuTypes" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <script type="text/javascript">
        jQuery = jQuery.noConflict();
        $ = jQuery.noConflict();
        function grid_customizationWindowCloseUp(s, e) {
            FromGridView.ShowCustomizationWindow();
        }

        function Grid_SelectedIndexesChangedClient(s, e) {
            var currentValues = FromGridView.GetEditor('PAG_CODE').GetValue();
            CallBackCode.PerformCallBack(s.GetSelectedItem().Value + "," + ddlClientLOC.GetValue());
            if (currentValues != "" && currentValues != null) {

            }
        }

        function OnChangedClient(cbItm) {

        }

        $(function () {
            $('.testing123').dialog();
        });
    </script>
</asp:Content>

<asp:Content ID="contentTopHere1" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
            <li>
                <asp:ImageButton ID="LinkBtnPDF" OnClick="LinkBtnPDF_Click" runat="server" ImageUrl="~/img/FilesIcons/acrobat.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonXLX" OnClick="LinkButtonXLX_Click" runat="server" ImageUrl="~/img/FilesIcons/excel.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonXxls" OnClick="LinkButtonXxls_Click" runat="server" ImageUrl="~/img/FilesIcons/xlsx.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonRtf" OnClick="LinkButtonRtf_Click" runat="server" ImageUrl="~/img/FilesIcons/rtf.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonCvs" OnClick="LinkButtonCvs_Click" runat="server" ImageUrl="~/img/FilesIcons/cvs.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButtonWord" OnClick="LinkButtonRtf_Click" runat="server" ImageUrl="~/img/FilesIcons/word.png" Width="50" Height="40"></asp:ImageButton>
            </li>

            <li>
                <asp:ImageButton ID="LinkButtonHtml" OnClick="LinkButtonHtml_Click" runat="server" ImageUrl="~/img/icons/searchtop.jpg" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton6" OnClick="LinkButton3_Click" runat="server" ImageUrl="~/img/FilesIcons/acrobat.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton7" OnClick="LinkButton3_Click" runat="server" ImageUrl="~/img/FilesIcons/word.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton8" OnClick="LinkButton3_Click" runat="server" ImageUrl="~/img/FilesIcons/excel.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton9" OnClick="LinkButton3_Click" OnClientClick="window.print();" runat="server" ImageUrl="~/img/FilesIcons/acrobat.png" Width="50" Height="40"></asp:ImageButton>
            </li>
            <li>
                <asp:ImageButton ID="LinkButton10" OnClick="LinkButton3_Click" runat="server" ImageUrl="~/img/FilesIcons/torrent.png" Width="50" Height="40"></asp:ImageButton>
            </li>
        </ul>
        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvMenuPagesInquery" ExportedRowType="All"></dx:ASPxGridViewExporter>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">
    <div class="formDiv">
        <div class="formHeaderDiv">
            <h3>Create Menu Items</h3>
        </div>

        <asp:SqlDataSource ID="DxGridSqlDataSource1" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>' runat="server"></asp:SqlDataSource>
        <dx:ASPxGridView ID="gvMenuPagesInquery" runat="server" CssClass="FromGridView" DataSourceID="DxGridSqlDataSource1" Theme="Office2010Black" ClientInstanceName="FromGridView" Width="100%" KeyFieldName="PAG_CODE"
            AutoGenerateColumns="False" OnRowDeleting="gvMenuPagesInquery_RowDeleting"
            OnDataBound="gvMenuPagesInquery_DataBound"
            OnStartRowEditing="gvMenuPagesInquery_StartRowEditing"
            Settings-ShowTitlePanel="false"
            SettingsText-Title="Oder Master">


            <Settings ColumnMinWidth="150" UseFixedTableLayout="false" ShowGroupPanel="true" VerticalScrollBarMode="Auto" HorizontalScrollBarMode="Visible" />

            <SettingsContextMenu Enabled="True" EnableColumnMenu="True" EnableRowMenu="True" />
            <SettingsSearchPanel Visible="False" CustomEditorID="CustomSearchPanelTxtBx" />

            <Columns>
                <dx:GridViewDataColumn>
                    <DataItemTemplate>
                        <dx:ASPxButton ID="btnOrderDetailView" runat="server" CommandArgument='<%# Eval("PAG_CODE") %>' CommandName="OrderDetailView" Text="View" CssClass="btna btn btn-success btn-sm" CausesValidation="false">
                            <Image ToolTip="View button here" Url="/img/icons/white-icn/eyee1.png" Width="15px" />
                        </dx:ASPxButton>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
                <%-- <dx:GridViewCommandColumn ShowSelectButton="true" Width="110" Caption=" " VisibleIndex="1"></dx:GridViewCommandColumn>--%>
                <dx:GridViewCommandColumn ShowNewButtonInHeader="true" ShowEditButton="true" Width="110" VisibleIndex="2"></dx:GridViewCommandColumn>
                <dx:GridViewCommandColumn ShowDeleteButton="true" Width="130" Caption=" " VisibleIndex="3"></dx:GridViewCommandColumn>
                <dx:GridViewDataComboBoxColumn FieldName="PAG_CODE" Caption="Page Code" VisibleIndex="4">
                    <PropertiesComboBox ClientInstanceName="ddlItmNo">
                        <ClientSideEvents LostFocus="function(s, e) { CallBackCode.PerformCallback(s.GetText()); }"></ClientSideEvents>
                    </PropertiesComboBox>
                </dx:GridViewDataComboBoxColumn>

                <dx:GridViewDataTextColumn FieldName="PAG_SEQNO" Caption="Sequence No" VisibleIndex="5">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PAG_NAME" Caption="Name" VisibleIndex="6">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PAG_URL" Caption="Url" VisibleIndex="7">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PAG_PARNT" Caption="Parent Id" VisibleIndex="8">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PAG_TYPE" Caption="Type" VisibleIndex="9">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>

            </Columns>
            <SettingsCommandButton>
                <NewButton ButtonType="Button" Text="New" Styles-Style-CssClass="btna btn btn-primary  btn-sm">
                    <Image runat="server" Url="~/img/icons/white-icn/Add.png" Width="15px"></Image>
                </NewButton>

                <SelectButton ButtonType="Button" Text="View" Styles-Style-CssClass="btna btn btn-success btn-sm">
                    <Image ToolTip="View button here" Url="~/img/icons/white-icn/eyee1.png" Width="15px" />
                    <%--<Image ToolTip="View button here" Url="~/img/icons/white-icn/eyee1.png" Width="18px" />--%>
                </SelectButton>

                <EditButton ButtonType="Button" Text="Edit" Styles-Style-CssClass="btna btn btn-info btn-sm">
                    <Image runat="server" Url="~/img/icons/white-icn/Pen Tool.png" Width="15px"></Image>
                </EditButton>

                <DeleteButton ButtonType="Button" Text="Delete" Styles-Style-CssClass="btna btn btn-danger btn-sm btn-danger-variant">
                    <Image runat="server" Url="~/img/icons/white-icn/Trash.png" Width="15px"></Image>
                </DeleteButton>

                <CancelButton ButtonType="Button" Text="Close" Styles-Style-CssClass="btnPopup btn btn-primary">
                    <%--<Image ToolTip="Cancel button here" Url="~/img/icons/close.png" />--%>
                </CancelButton>
                <UpdateButton ButtonType="Button" Text="Update">
                </UpdateButton>
            </SettingsCommandButton>
            <SettingsEditing EditFormColumnCount="2" />
            <SettingsPopup>
                <EditForm VerticalAlign="WindowCenter" AllowResize="True" Modal="True" HorizontalAlign="WindowCenter"></EditForm>
            </SettingsPopup>

            <SettingsPager Position="Bottom">
                <PageSizeItemSettings Caption="Records per Page" Items="10, 20, 50, 100" ShowAllItem="true" Position="Right" Visible="true">
                </PageSizeItemSettings>
            </SettingsPager>
            <Styles>
                <CommandColumn Spacing="0px" Wrap="False" />
                <EditFormDisplayRow HorizontalAlign="Center" VerticalAlign="Middle"></EditFormDisplayRow>
                <AlternatingRow CssClass="alterRowGV" Enabled="True"></AlternatingRow>
                <Row CssClass="rowHeight"></Row>
                <Cell Wrap="False" />
            </Styles>

            <SettingsLoadingPanel Mode="ShowOnStatusBar" />
            <SettingsBehavior SortMode="DisplayText" EnableCustomizationWindow="true" AllowDragDrop="true" ConfirmDelete="true" ColumnResizeMode="Control" />
            <ClientSideEvents ColumnStartDragging="grid_customizationWindowCloseUp" EndCallback="GridOnEndCallBack" />

            <Templates>
                <EditForm>

                    <div class="overlay">
                        <div class="FormPopup">
                            <div class="formHeaderDiv">
                                <h3>Menu Type</h3>
                            </div>
                            <fieldset>
                                <table>
                                    <tr>
                                        <td>Type : </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="ddlType" ClientInstanceName="ddlClientType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" Width="170px" Value='<%# Bind("PAG_TYPE") %>' TabIndex="0">
                                                    <Items>
                                                        <dx:ListEditItem Text="Top" Value="T" />
                                                        <dx:ListEditItem Text="Group" Value="G" />
                                                        <dx:ListEditItem Text="Page" Value="P" />
                                                        <dx:ListEditItem Text="Role" Value="R" />
                                                    </Items>
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="Text" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>
                                        <td>Sequence No : </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtSeqNo" ClientInstanceName="txtClientSeqNo" runat="server" Width="170px" TabIndex="2" Text='<%# Bind("PAG_SEQNO") %>' MaxLength="2">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="Text" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RegularExpression ValidationExpression="[0-9]{0,2}" ErrorText="Invalid Sequence Number." />
                                                        <RequiredField IsRequired="true" />
                                                    </ValidationSettings>
                                                    <NullTextStyle Font-Size="Small" />
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Code : </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtCode" ClientInstanceName="txtClientCode" MaxLength="8" runat="server" Width="170px" TabIndex="3" Text='<%# Bind("PAG_CODE") %>'>
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="Text" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" />
                                                    </ValidationSettings>
                                                    <NullTextStyle Font-Size="Small" />
                                                    <MaskSettings Mask=">cccccccc" />
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                        <td>Name : </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtName" ClientInstanceName="txtClientName" MaxLength="50" runat="server" Width="170px" Text='<%# Bind("PAG_NAME") %>' TabIndex="4">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="Text" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" />
                                                    </ValidationSettings>
                                                    <NullTextStyle Font-Size="Small" />
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>URL : </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtUrl" ClientInstanceName="ddlClientUrl" runat="server" Width="170px" TabIndex="5" Text='<%# Bind("PAG_URL") %>'>
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="Text" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <RequiredField IsRequired="false" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                        <td>Parent : </td>
                                        <td>
                                            <dx:ASPxComboBox ID="ddlParentID" OnInit="ddlParentID_Init" ClientInstanceName="ddlClientType" runat="server" Visible="false" TextField="PAG_NAME" ValueField="PAG_ID" Width="170px" TabIndex="6">
                                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="Text" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <RequiredField IsRequired="true" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="3">
                                            <dx:ASPxLabel ID="lblErrorMessage2" ClientInstanceName="lblErrorMessage2" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>

                            <hr style="color: skyblue" />
                            <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                                <dx:ASPxLabel ID="ASPxLabel1" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
                                <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                                    <dx:ASPxButton ID="btnAdd1" runat="server" Text="Save" CssClass="btnPopup btn btn-success" CausesValidation="true" OnClick="btnAdd1_Click" />
                                    <dx:ASPxButton ID="btnPopUpClear" runat="server" Text="Cancel" CssClass="btnPopup btn btn-warning" CausesValidation="false" AutoPostBack="false">
                                        <ClientSideEvents Click="function(s, e) { CallBackCode.PerformCallback(CatClientEditCode.GetText()); }" />
                                    </dx:ASPxButton>
                                    <dx:ASPxButton ID="btnPopUpDelete" runat="server" CommandArgument='<%# Eval("PAG_CODE") %>' CommandName="PopUpDelete" Text="Delete" CssClass="btnPopup btn btn-danger btn-danger-variant" CausesValidation="false">
                                        <ClientSideEvents Click="function(s, e) { 
                                                if (!confirm('Confirm Delete?')) { 
                                                    e.processOnServer = false;  
                                                }
                                        }" />
                                    </dx:ASPxButton>
                                    <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Print" CssClass="btnPopup btn btn-info btn-info-variant" AutoPostBack="false" CausesValidation="false">
                                        <ClientSideEvents Click="function(s, e) { printDoc(s, e); }" />
                                    </dx:ASPxButton>
                                    <dx:ASPxGridViewTemplateReplacement ID="btnPopUpClose" ReplacementType="EditFormCancelButton" runat="server" />
                                </div>
                            </div>
                        </div>
                </EditForm>
            </Templates>
            <Templates>
                <StatusBar>
                    <table>
                        <tr>
                            <td>Search: </td>
                            <td>
                                <dx:ASPxButtonEdit runat="server" ID="CustomSearchPanelTxtBx" />
                            </td>
                            <td>&nbsp;</td>
                            <td>Search By: </td>
                            <td>
                                <dx:ASPxComboBox ID="DDLFilterByColumn" runat="server" OnSelectedIndexChanged="DDLFilterByColumn_SelectedIndexChanged">
                                    <Items>
                                        <dx:ListEditItem Text="All" Value="*" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>
                        </tr>
                    </table>
                </StatusBar>
            </Templates>
            
            <SettingsEditing Mode="EditForm"></SettingsEditing>
        </dx:ASPxGridView>
    </div>
    <script type="text/javascript">
    </script>
</asp:Content>
