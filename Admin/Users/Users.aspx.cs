﻿using DevExpress.Web;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Users_Default : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        //DxGridSqlDataSource1.SelectCommand = "SELECT * FROM [STP_MSTR_USERS]";
        DxGridSqlDataSource1.SelectCommand = "SELECT [STP_MSTR_USERS].*, MNU_NAME, DPT_NAME, STM_NAME "
            + " FROM [STP_MSTR_USERS] "
            + " LEFT JOIN STP_USR_MENUS ON USR_MNCOD = MNU_CODE "
            + " LEFT JOIN STP_USR_DEPTS ON USR_DEPT = DPT_CODE "
            + " LEFT JOIN STP_MSTR_STORE ON USR_PRMBR = STM_CODE ";
    }

    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvUserInquery_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvUserInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string userCode = e.Keys["USR_ID"].ToString();

        dbHlpr.ExecuteNonQuery("DELETE FROM [STP_MSTR_USERS] WHERE USR_ID = '" + userCode + "'");

        e.Cancel = true;
        gvUserInquery.CancelEdit();
    }
    protected void ClBk_CodeLostFocus_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (e.Parameter.Length > 0)
        {
            DataTable myDT = new DataTable();
            myDT = dbHlpr.FetchData("SELECT * FROM [STP_MSTR_USERS] WHERE USR_ID = '" + e.Parameter.ToString() + "'");
            e.Result = JsonConvert.SerializeObject(myDT);
        }
    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxTextBox txtEmpNo = (((ASPxTextBox)gvUserInquery.FindEditFormTemplateControl("txtEmpNo")));
        ASPxTextBox txtId = (((ASPxTextBox)gvUserInquery.FindEditFormTemplateControl("txtCode")));
        ASPxTextBox txtInitials = (((ASPxTextBox)gvUserInquery.FindEditFormTemplateControl("txtInitials")));
        ASPxTextBox txtUserName = (((ASPxTextBox)gvUserInquery.FindEditFormTemplateControl("txtUserName")));
        ASPxTextBox txtPass = (((ASPxTextBox)gvUserInquery.FindEditFormTemplateControl("txtPass")));
        ASPxComboBox cmBxDept = (((ASPxComboBox)gvUserInquery.FindEditFormTemplateControl("cmBxDept")));
        ASPxComboBox cmBxBrnch = (((ASPxComboBox)gvUserInquery.FindEditFormTemplateControl("cmBxBrnch")));
        ASPxDropDownEdit ddAssngdStores = ((ASPxDropDownEdit)gvUserInquery.FindEditFormTemplateControl("AssgndStoresNo"));
        ASPxComboBox cmBxSystemAccess = (((ASPxComboBox)gvUserInquery.FindEditFormTemplateControl("cmBxSystemAccess")));
        ASPxComboBox cmBxVansalesAccess = (((ASPxComboBox)gvUserInquery.FindEditFormTemplateControl("cmBxVansalesAccess")));
        ASPxComboBox cmBxMgrAppAccess = (((ASPxComboBox)gvUserInquery.FindEditFormTemplateControl("cmBxMgrAppAccess")));
        ASPxComboBox cmBxBkOfcAccess = (((ASPxComboBox)gvUserInquery.FindEditFormTemplateControl("cmBxBkOfcAccess")));
        ASPxComboBox cmBxMenu = (((ASPxComboBox)gvUserInquery.FindEditFormTemplateControl("cmBxMenu")));
        ASPxComboBox cmBxEditMasters = (((ASPxComboBox)gvUserInquery.FindEditFormTemplateControl("cmBxEditMasters")));


        string userEmpNo = txtEmpNo.Text;
        string userId = txtId.Text;
        string userInitials = txtInitials.Text;
        string userName = txtUserName.Text;
        string userPassword = txtPass.Text;
        string userSystemAccess = cmBxSystemAccess.Value.ToString();
        string userVansalesAccess = cmBxVansalesAccess.Value.ToString();
        string userMgrAppAccess = cmBxMgrAppAccess.Value.ToString();
        string userBkOfcAccess = cmBxBkOfcAccess.Value.ToString();
        string userDept = cmBxDept.Value.ToString();
        string userBranch = cmBxBrnch.Value.ToString();
        string userAssgndStores = ddAssngdStores.Value != null ? ddAssngdStores.Value.ToString() : "";
        string userMenuCode = cmBxMenu.Value.ToString();
        string userEditMasters = cmBxEditMasters.Value.ToString();

        DataTable dtUserOfEmpNo = dbHlpr.FetchData("SELECT * FROM STP_MSTR_USERS WHERE USR_EMPNO = '" + userEmpNo + "' AND USR_ID <> '" + userId + "' ");
        if (dtUserOfEmpNo.Rows.Count > 0)
        {
            ASPxLabel lblErrMsg = (ASPxLabel)gvUserInquery.FindEditFormTemplateControl("lblErrorMessage");
            lblErrMsg.Text = "Employee Code already exists.";
        }
        else
        {

            DataTable myDT = myDT = dbHlpr.FetchData("SELECT * FROM [STP_MSTR_USERS] WHERE USR_ID = '" + userId + "'");
            if (myDT.Rows.Count <= 0)
            {
                dbHlpr.ExecuteNonQuery("INSERT INTO STP_MSTR_USERS ( "
                    + " USR_ID, USR_INITL, USR_NAME, USR_PSWRD, USR_EMPNO, USR_DEPT,  "
                    + " USR_MNCOD, USR_PRMBR, USR_SYACS, USR_VSACS, USR_MGACS, USR_BOACS, USR_DTGOV "
                    + " ) VALUES ( "
                    + " '" + userId + "', '" + userInitials + "', '" + userName + "', '" + userPassword + "', '" + userEmpNo + "', '" + userDept + "', "
                    + " '" + userMenuCode + "', '" + userBranch + "', '" + userSystemAccess + "', '" + userVansalesAccess + "', "
                    + " '" + userMgrAppAccess + "', '" + userBkOfcAccess + "', '" + userEditMasters + "')");

                // Assign Stores
                this.AssignStores(userId, userBranch, userAssgndStores);
            }
            else
            {
                dbHlpr.ExecuteNonQuery("UPDATE [STP_MSTR_USERS] SET "
                    + " USR_INITL = '" + userInitials + "', "
                    + " USR_NAME = '" + userName + "', "
                    //+ " USR_PSWRD = '" + userPassword + "', "
                    + " USR_EMPNO = '" + userEmpNo + "', "
                    + " USR_DEPT = '" + userDept + "', "
                    + " USR_PRMBR = '" + userBranch + "', "
                    + " USR_MNCOD = '" + userMenuCode + "', "
                    + " USR_SYACS = '" + userSystemAccess + "', "
                    + " USR_VSACS = '" + userVansalesAccess + "', "
                    + " USR_MGACS = '" + userMgrAppAccess + "', "
                    + " USR_BOACS = '" + userBkOfcAccess + "', "
                    + " USR_DTGOV = '" + userEditMasters + "' "
                    + " WHERE USR_ID = '" + userId + "'");

                // Assign Routes
                this.AssignStores(userId, userBranch, userAssgndStores);
            }
        }
        gvUserInquery.CancelEdit();
        gvUserInquery.DataBind();
    }

    private void AssignStores(string UserId, string MainStore, string AssignedStores)
    {
        dbHlpr.ExecuteNonQuery("DELETE FROM STP_LNK_USRBR WHERE UBR_USRID = '" + UserId + "' ");

        string mainKey = MainStore + UserId;
        dbHlpr.ExecuteNonQuery("INSERT INTO STP_LNK_USRBR (UBR_KEY, UBR_STRNO, UBR_USRID "
            + " ) VALUES ( "
            + " '" + mainKey + "','" + MainStore + "','" + UserId + "') ");
        if (AssignedStores.Trim().Length > 0)
        {
            string[] assgndStoresArr = AssignedStores.Trim().Split(',');
            foreach (string storeNo in assgndStoresArr)
            {
                string key = (storeNo + UserId);
                dbHlpr.ExecuteNonQuery("DELETE FROM STP_LNK_USRBR WHERE UBR_KEY = '" + key + "' ");
                dbHlpr.ExecuteNonQuery("INSERT INTO STP_LNK_USRBR (UBR_KEY, UBR_STRNO, UBR_USRID "
                    + " ) VALUES ( "
                    + " '" + key + "','" + storeNo + "','" + UserId + "') ");
            }
        }
    }

    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvUserInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvUserInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

        ////html export here
        //gridExport.ExportHtmlToResponse();
        //string str;
        //MemoryStream ms = new MemoryStream();
        //try
        //{
        //    gridExport.ExportToHtml(ms);
        //    ms.Seek(0, SeekOrigin.Begin);
        //    StreamReader sr = new StreamReader(ms);
        //    str = sr.ReadToEnd();
        //}
        //finally
        //{
        //    ms.Close();
        //}

    }

    protected void gvUserInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            string userId = e.CommandArgs.CommandArgument.ToString().Trim();
            if (userId.Length > 0) // Code is sent via request
            {
                dbHlpr.ExecuteNonQuery("DELETE FROM [STP_MSTR_USERS] WHERE USR_ID = '" + userId + "'");

                gvUserInquery.CancelEdit();
                gvUserInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvUserInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid User ID. Unable to delete User.";
            }
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvUserInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvUserInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvUserInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvUserInquery.SearchPanelFilter = txtFilter.Text;
        gvUserInquery.DataBind();
    }
    protected void gvUserInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvUserInquery, CBX_filter);
    }
    protected void gvUserInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvUserInquery, CBX_filter);
    }

    private string EditUserBranch_SLCTD = string.Empty;
    private string EditUserSysAcs_SLCTD = string.Empty;

    protected void gvUserInquery_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        ASPxGridView gv = (ASPxGridView)sender;
        EditUserBranch_SLCTD = gv.GetRowValues(gv.EditingRowVisibleIndex, "USR_PRMBR").ToString();
        EditUserSysAcs_SLCTD = gv.GetRowValues(gv.EditingRowVisibleIndex, "USR_SYACS").ToString();

    }
    protected void cmBxBrnch_Init(object sender, EventArgs e)
    {
        // Get Categories for current active division
        ASPxComboBox cbBrnch = (ASPxComboBox)sender;

        cbBrnch.DataSource = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE");
        cbBrnch.ValueField = "STM_CODE";
        cbBrnch.TextField = "STM_NAME";
        cbBrnch.DataBind();
        cbBrnch.Value = EditUserBranch_SLCTD;
    }

    //protected void cmBxSecBrnch_Init(object sender, EventArgs e)
    //{
    //    ASPxDropDownEdit cbSecBrnch = (ASPxDropDownEdit)sender;
    //    ASPxListBox lstBxSecBrnch = (ASPxListBox)cbSecBrnch.FindControl("lstBxSecBrnch");


    //    DataTable dtBrnchs = dbHlpr.FetchData("SELECT * FROM " + Session["ADC"] + "STP_BRNCH");

    //    DataRow dr = dtBrnchs.NewRow();
    //    dr["BRN_NAME"] = "All";
    //    dr["BRN_CODE"] = "0";
    //    dtBrnchs.Rows.InsertAt(dr, 0);

    //    lstBxSecBrnch.DataSource = dtBrnchs;
    //    lstBxSecBrnch.ValueField = "BRN_CODE";
    //    lstBxSecBrnch.TextField = "BRN_NAME";
    //    lstBxSecBrnch.DataBind();

    //    cbSecBrnch.Value = EditUserSecBranch_SLCTD;
    //}
    protected void AssgndStoresNo_Init(object sender, EventArgs e)
    {
        ASPxDropDownEdit ddAssgndStores = (ASPxDropDownEdit)sender;
        ASPxListBox lstBxAssgndStores = (ASPxListBox)ddAssgndStores.FindControl("lstBxAssgndStoresItems");

        DataTable dtAssgndStores = dbHlpr.FetchData("SELECT STM_CODE, STM_NAME FROM STP_MSTR_STORE ORDER BY STM_NAME ");

        string UserId = "";
        if (!gvUserInquery.IsNewRowEditing)
        {
            UserId = gvUserInquery.GetRowValues(gvUserInquery.EditingRowVisibleIndex, "USR_ID").ToString();
        }
        DataTable assignedAssgndStores = dbHlpr.FetchData("SELECT UBR_STRNO FROM STP_LNK_USRBR WHERE UBR_USRID = '" + UserId + "' ");
        string tempStr = "";
        for (int i = 0; i < assignedAssgndStores.Rows.Count; i++)
        {
            tempStr += assignedAssgndStores.Rows[i]["UBR_STRNO"].ToString() + ",";
        }
        tempStr = tempStr.Trim(',');

        lstBxAssgndStores.DataSource = dtAssgndStores;
        lstBxAssgndStores.ValueField = "STM_CODE";
        lstBxAssgndStores.TextField = "STM_NAME";
        lstBxAssgndStores.DataBind();

        ddAssgndStores.Value = tempStr;
    }
}