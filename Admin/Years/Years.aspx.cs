﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Years_Years : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT YR_CODE, YR_NAME, YR_CMNTS FROM STP_MST_YEAR ";
    }

    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxTextBox txtBxCode = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtCode"));
        string RCode = "0";
        //  string CatTyp = "0";
        if (txtBxCode.Text.Length > 0)
        {
            RCode = txtBxCode.Text.Trim();
        }

        string RName = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtName")).Text;
        string RComents = ((ASPxMemo)gvInquery.FindEditFormTemplateControl("txtComments")).Text;

        DataTable myDT = new DataTable();

        myDT = myDT = dbHlpr.FetchData("SELECT * FROM STP_MST_YEAR WHERE YR_CODE = '" + RCode + "' ");
        if (myDT.Rows.Count <= 0)
        {

            dbHlpr.ExecuteNonQuery("INSERT INTO STP_MST_YEAR (YR_CODE, YR_NAME, YR_CMNTS)"
                + " VALUES ('" + RCode + "',  '" + RName + "' , '" + RComents + "' )");

            DataTable dt = dbHlpr.FetchData("SELECT "
                + " YR_CODE AS TEXT01, YR_NAME AS TEXT02, YR_CMNTS AS TEXT03 "
                + " FROM STP_MST_YEAR "
                + " WHERE YR_CODE = '" + RCode + "' ");
            dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MST_YEAR", "CRT");
        }
        else
        {
            dbHlpr.ExecuteNonQuery("UPDATE STP_MST_YEAR SET "
                + " YR_NAME = '" + RName + "', "
                + " YR_CMNTS = '" + RComents + "' "
                + " WHERE YR_CODE = '" + RCode + "'");

            DataTable dt = dbHlpr.FetchData("SELECT "
                + " YR_CODE AS TEXT01, YR_NAME AS TEXT02, YR_CMNTS AS TEXT03 "
                + " FROM STP_MST_YEAR "
                + " WHERE YR_CODE = '" + RCode + "' ");
            dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MST_YEAR", "UPD");
        }

        gvInquery.CancelEdit();
        gvInquery.DataBind();
    }//end method

    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }//end method

    protected void gvInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string RCode = e.Keys["YR_CODE"].ToString().Trim();

        dbHlpr.ExecuteNonQuery(" DELETE FROM STP_MST_YEAR WHERE YR_CODE =  '" + RCode + "' ");

        DataTable dt = dbHlpr.FetchData("SELECT '" + RCode + "' AS TEXT01 ");
        dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MST_YEAR", "DLT");

        e.Cancel = true;
        gvInquery.CancelEdit();
    }//end method

    protected void gvInquery_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }//end method

    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }//end method

    protected void ACB_CodeCheck_Callback(object source, CallbackEventArgs e)
    {
        if (e.Parameter.Length > 0)
        {
            DataTable myDT = new DataTable();
            myDT = dbHlpr.FetchData(" SELECT * FROM STP_MST_YEAR WHERE YR_CODE = '" + int.Parse(e.Parameter.ToString()) + "' ");
            JavaScriptSerializer myserializer = new JavaScriptSerializer();
            if (myDT.Rows.Count > 0)
            {
                e.Result = myserializer.Serialize(myDT.Rows[0].ItemArray);
            }
            else
            {
                e.Result = myserializer.Serialize("");
            }
        }
    }//end method

    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked

            int CHK = dbHlpr.ExecuteNonQuery(" DELETE FROM STP_MST_YEAR WHERE YR_CODE = '" + e.CommandArgs.CommandArgument.ToString().Trim() + "' ");

            DataTable dt = dbHlpr.FetchData("SELECT '" + e.CommandArgs.CommandArgument.ToString().Trim() + "' AS TEXT01 ");
            dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MST_YEAR", "DLT");

            if (CHK == 1) // Code is sent via request
            {
                gvInquery.CancelEdit();
                gvInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid ID. Unable to delete.";
            }
        }
    }


    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }

    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }
    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

    }
    protected void txtCode_Init(object sender, EventArgs e)
    {
        if (!gvInquery.IsNewRowEditing)
        {
            ASPxTextBox txtCode = (ASPxTextBox)sender;
            txtCode.ReadOnly = true;
        }
        else
        {
            ASPxTextBox txtCode1 = (ASPxTextBox)sender;
            txtCode1.ReadOnly = false;
        }
    }
}//end class