﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using DevExpress.XtraPrinting;
using DevExpress.Export;
using System.Drawing;

public partial class Admin_Categories_Default : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT INV_CATCD, INV_CATNM, INV_CATYP FROM STP_MSTR_ICATG ";
    }

    protected void categoryListingGridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvCategoriesInquery_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvCategoriesInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string catCode = e.Keys["INV_CATCD"].ToString().Trim();
        string catType = e.Keys["INV_CATYP"].ToString().Trim();

        dbHlpr.ExecuteNonQuery("DELETE FROM STP_MSTR_ICATG WHERE INV_CATCD = '" + catCode + "' AND INV_CATYP = '" + catType + "' ");

        DataTable dt = dbHlpr.FetchData("SELECT '" + catCode + "' AS TEXT01, '" + catType + "' AS TEXT03 ");
        dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MSTR_ICATG", "DLT");

        e.Cancel = true;
        gvCategoriesInquery.CancelEdit();
    }
    protected void ACB_CodeCheck_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (e.Parameter.Length > 0)
        {
            DataTable myDT = new DataTable();
            myDT = dbHlpr.FetchData("SELECT * FROM [STP_MSTR_ICATG] WHERE INV_CATCD = '" + e.Parameter.Trim() + "'");
            JavaScriptSerializer myserializer = new JavaScriptSerializer();
            if (myDT.Rows.Count > 0)
            {
                e.Result = myserializer.Serialize(myDT.Rows[0].ItemArray);
            }
            else
            {
                e.Result = myserializer.Serialize("");
            }
        }
    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxTextBox txtBxCode = ((ASPxTextBox)gvCategoriesInquery.FindEditFormTemplateControl("CatEditCode"));
        string CatCode = "0";
        //  string CatTyp = "0";
        if (txtBxCode.Text.Length > 0)
        {
            CatCode = txtBxCode.Text.Trim();
        }

        string CatName = ((ASPxTextBox)gvCategoriesInquery.FindEditFormTemplateControl("CatEditName")).Text;
        string CatType = ((ASPxComboBox)gvCategoriesInquery.FindEditFormTemplateControl("CatEditType")).SelectedItem.Value.ToString();

        DataTable myDT = new DataTable();

        myDT = myDT = dbHlpr.FetchData("SELECT * FROM STP_MSTR_ICATG WHERE INV_CATCD = '" + CatCode + "'  AND INV_CATYP = '" + CatType + "'");
        if (myDT.Rows.Count <= 0)
        {

            dbHlpr.ExecuteNonQuery("INSERT INTO STP_MSTR_ICATG (INV_CATCD, INV_CATNM, INV_CATYP) "
            + " VALUES ('" + CatCode + "', '" + CatName + "', '" + CatType + "')");

            DataTable dt = dbHlpr.FetchData("SELECT "
                + " INV_CATCD AS TEXT01, INV_CATNM AS TEXT02, INV_CATYP AS TEXT03 "
                + " FROM STP_MSTR_ICATG "
                + " WHERE INV_CATCD = '" + CatCode + "' AND INV_CATYP = '" + CatType + "'");
            dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MSTR_ICATG", "CRT");
        }
        else
        {
            dbHlpr.ExecuteNonQuery("UPDATE STP_MSTR_ICATG SET "
            + " INV_CATNM = '" + CatName + "', "
            + " INV_CATYP = '" + CatType + "' "
            + " WHERE INV_CATCD = '" + CatCode + "' AND INV_CATYP = '" + CatType + "'");

            DataTable dt = dbHlpr.FetchData("SELECT "
                + " INV_CATCD AS TEXT01, INV_CATNM AS TEXT02, INV_CATYP AS TEXT03 "
                + " FROM STP_MSTR_ICATG "
                + " WHERE INV_CATCD = '" + CatCode + "' AND INV_CATYP = '" + CatType + "'");
            dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MSTR_ICATG", "UPD");
        }

        gvCategoriesInquery.CancelEdit();
        gvCategoriesInquery.DataBind();
    }

    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvCategoriesInquery);
    }

    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvCategoriesInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

        ////html export here
        //gridExport.ExportHtmlToResponse();
        //string str;
        //MemoryStream ms = new MemoryStream();
        //try
        //{
        //    gridExport.ExportToHtml(ms);
        //    ms.Seek(0, SeekOrigin.Begin);
        //    StreamReader sr = new StreamReader(ms);
        //    str = sr.ReadToEnd();
        //}
        //finally
        //{
        //    ms.Close();
        //}

    }
    protected void gvCategoriesInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            string[] CmndArgs = e.CommandArgs.CommandArgument.ToString().Split(new char[] { ',' });
            string catId = CmndArgs[0];
            string CatType = CmndArgs[1];
            if (catId != "" && CatType != "") // Code is sent via request
            {
                dbHlpr.ExecuteNonQuery("DELETE FROM STP_MSTR_ICATG WHERE INV_CATCD = '" + catId + "' AND INV_CATYP = '" + CatType + "'");

                DataTable dt = dbHlpr.FetchData("SELECT '" + catId + "' AS TEXT01, '" + CatType + "' AS TEXT03 ");
                dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MSTR_ICATG", "DLT");

                gvCategoriesInquery.CancelEdit();
                gvCategoriesInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvCategoriesInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid Category ID. Unable to delete Category.";
            }
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvCategoriesInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvCategoriesInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvCategoriesInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvCategoriesInquery.SearchPanelFilter = txtFilter.Text;
        gvCategoriesInquery.DataBind();
    }

    protected void gvCategoriesInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvCategoriesInquery, CBX_filter);
    }

    protected void gvCategoriesInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvCategoriesInquery, CBX_filter);
    }

    protected void CatEditCode_Init(object sender, EventArgs e)
    {
        if (!gvCategoriesInquery.IsNewRowEditing)
        {
            ASPxTextBox txtCode = (ASPxTextBox)sender;
            txtCode.ReadOnly = true;
        }
        else
        {
            ASPxTextBox txtCode1 = (ASPxTextBox)sender;
            txtCode1.ReadOnly = false;
        }
    }
}