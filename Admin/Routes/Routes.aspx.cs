﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Customers_Default : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT [RUT_CODE] ,[RUT_NAME] ,[RUT_CMNTS]    FROM [STP_MSTR_ROUTE]";
    }

    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxTextBox txtBxCode = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtCode"));
        string RCode = "0";
        //  string CatTyp = "0";
        if (txtBxCode.Text.Length > 0)
        {
            RCode = txtBxCode.Text.Trim();
        }

        string RName = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtName")).Text;
        string RComments = ((ASPxMemo)gvInquery.FindEditFormTemplateControl("txtComments")).Text;

        DataTable myDT = new DataTable();

        myDT = myDT = dbHlpr.FetchData("SELECT * FROM [STP_MSTR_ROUTE] WHERE RUT_CODE = '" + RCode + "'");
        if (myDT.Rows.Count <= 0)
        {

            dbHlpr.ExecuteNonQuery("INSERT INTO [STP_MSTR_ROUTE] ([RUT_CODE] ,[RUT_NAME],[RUT_CMNTS]  )"
            + " VALUES ('" + RCode + "',  '" + RName + "',  '" + RComments + "'  )");
        }
        else
        {
            dbHlpr.ExecuteNonQuery("UPDATE [STP_MSTR_ROUTE] SET "
            + "  RUT_NAME = '" + RName + "' "
             + " ,RUT_CMNTS = '" + RComments + "' "
            + " WHERE RUT_CODE = '" + RCode + "'");
        }

        gvInquery.CancelEdit();
        gvInquery.DataBind();
    }//end method

    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }//end method

    protected void gvInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string RCode = e.Keys["RUT_CODE"].ToString().Trim();

        dbHlpr.ExecuteNonQuery("DELETE FROM [STP_MSTR_ROUTE] WHERE RUT_CODE =  '" + RCode + "' ");

        e.Cancel = true;
        gvInquery.CancelEdit();
    }//end method

    protected void gvInquery_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }//end method

    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }//end method

    protected void ACB_CodeCheck_Callback(object source, CallbackEventArgs e)
    {
        if (e.Parameter.Length > 0)
        {
            DataTable myDT = new DataTable();
            myDT = dbHlpr.FetchData("SELECT * FROM STP_MSTR_ROUTE WHERE RUT_CODE = '" + int.Parse(e.Parameter.ToString()) + "'");
            JavaScriptSerializer myserializer = new JavaScriptSerializer();
            if (myDT.Rows.Count > 0)
            {
                e.Result = myserializer.Serialize(myDT.Rows[0].ItemArray);
            }
            else
            {
                e.Result = myserializer.Serialize("");
            }
        }
    }//end method

    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            int cstId;
            // if ( e.CommandArgs.CommandArgument.ToString()) // Code is sent via request
            //  {
            int chk = dbHlpr.ExecuteNonQuery("DELETE FROM STP_MSTR_ROUTE WHERE RUT_CODE = '" + e.CommandArgs.CommandArgument.ToString().Trim() + "'");
            if (chk == 1)
            {
                gvInquery.CancelEdit();
                gvInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid ID. Unable to delete.";
            }
        }
    }


    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }

    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }
    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }
    protected void gvInquery_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
    {
        if (e.Column.Caption == "Status")
        {
            e.DisplayText = GetCustomerStatus(e.GetFieldValue("CUS_STATS"), false);
        }
    }

    protected string GetCustomerStatus(object status, bool forClient = true)
    {
        string className = "";
        string statusString = "";

        switch (status.ToString())
        {
            case "-1":
                className = "label-rejected white-clr";
                statusString = "Blacklisted";
                break;
            case "0":
                className = "label-pending";
                statusString = "Inactive";
                break;
            case "1":
                className = "label-approved";
                statusString = "Active";
                break;
            default:
                className = "";
                statusString = "";
                break;
        }

        if (!forClient)
        {
            return statusString;
        }

        return "<span class='" + className + "'>" + statusString + "</span>";
    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

        ////html export here
        //gridExport.ExportHtmlToResponse();
        //string str;
        //MemoryStream ms = new MemoryStream();
        //try
        //{
        //    gridExport.ExportToHtml(ms);
        //    ms.Seek(0, SeekOrigin.Begin);
        //    StreamReader sr = new StreamReader(ms);
        //    str = sr.ReadToEnd();
        //}
        //finally
        //{
        //    ms.Close();
        //}

    }
    protected void txtCode_Init(object sender, EventArgs e)
    {
        if (!gvInquery.IsNewRowEditing)
        {
            ASPxTextBox txtCode = (ASPxTextBox)sender;
            txtCode.ReadOnly = true;
        }
        else
        {
            ASPxTextBox txtCode1 = (ASPxTextBox)sender;
            txtCode1.ReadOnly = false;
        }     
    }
}//end class