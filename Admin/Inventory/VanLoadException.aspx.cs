﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Inventory_VanLoadException : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DxGridSqlDataSource1.SelectCommand = "SELECT '' SRP_CODE, '' TRXDATE, '' VLD_LDNUM, '' RQUST, '' VNLOAD, '' VNUNLOAD WHERE 1 = 0 ";
        }
        else
        {
            try
            {
                string DateSelected = Convert.ToDateTime(ASPxDateEdit1.Value).ToString("yyyy-MM-dd");
                if (DateSelected == "0001-01-01")
                {
                    if (Session.Contents["dateSelected"] != null)
                    {
                        DateSelected = Session.Contents["dateSelected"].ToString();
                    }
                    else
                    {
                        DateSelected = "1900-01-01";
                    }
                }
                DxGridSqlDataSource1.SelectCommand = "SELECT "
                    + " SRP_TYPE, SRP_CODE, '" + DateSelected + "' AS TRXDATE, VLD_LDNUM, "
                    + " MAX(RQUST) AS RQUST, MAX(VNLOAD) AS VNLOAD, MAX(VNUNLOAD) AS VNUNLOAD "
                    + " FROM ( "
                    + " SELECT "
                    + " SRP_TYPE, SRP_CODE, VLD_LDNUM, "
                    + " CASE WHEN VLD_TTYPE = 'REQUEST' THEN 'Yes' ELSE 'No' END AS RQUST, "
                    + " CASE WHEN VLD_TTYPE = 'LOAD' THEN 'Yes'  ELSE 'No' END AS VNLOAD, "
                    + " CASE WHEN VLD_TTYPE = 'UNLOAD' THEN 'Yes'  ELSE 'No' END AS VNUNLOAD "
                    + " FROM STP_MSTR_SLREP "
                    + " LEFT JOIN INV_VAN_LDTRX ON SRP_CODE = VLD_SREP AND VLD_TRXDT = '" + DateSelected + " 00:00:00.000'  "
                    + " GROUP BY SRP_TYPE, SRP_CODE, SRP_NAME, VLD_LDNUM, VLD_TTYPE "
                    + " ) tbl "
                    + " GROUP BY SRP_TYPE, SRP_CODE, VLD_LDNUM "
                    + " ORDER BY SRP_CODE, SRP_TYPE, VLD_LDNUM ";
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        //gvInquery.SettingsPager.PageSize = Convert.ToInt32(ddlNoOfRecords.SelectedItem.Value);
        //gvInquery.DataBind();
    }

    protected void ACB_CodeCheck_Callback(object source, CallbackEventArgs e)
    {

    }

    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {
    }

    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            int RcptNo;
            if (int.TryParse(e.CommandArgs.CommandArgument.ToString(), out RcptNo)) // Code is sent via request
            {
                dbHlpr.ExecuteNonQuery("DELETE FROM [CUS_PAY_RCPTS] WHERE PR_NO = '" + RcptNo + "'");

                gvInquery.CancelEdit();
                gvInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid Order ID. Unable to delete Order.";
            }
        }
        else if (e.CommandArgs.CommandName.Equals("VoidReceipt"))
        {
            Int64 ReceiptNo;
            if (Int64.TryParse(e.CommandArgs.CommandArgument.ToString(), out ReceiptNo)) // Code is sent via request
            {
                dbHlpr.ExecuteNonQuery("UPDATE CUS_PAY_RCPTS SET PR_STATS = 'Voided' WHERE PR_NO = '" + ReceiptNo + "' ");

                gvInquery.CancelEdit();
                gvInquery.DataBind();
            }
        }
    }

    // Common Methods

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }

    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void gvInquery_CustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        //  if(e.Column.FieldName == "")
    }

    protected void ASPxDateEdit1_DateChanged(object sender, EventArgs e)
    {
        string DateSelected = Convert.ToDateTime(ASPxDateEdit1.Value).ToString("yyyy-MM-dd");
        DxGridSqlDataSource1.SelectCommand = "SELECT "
            + " SRP_TYPE, SRP_CODE, '" + DateSelected + "' AS TRXDATE, VLD_LDNUM, "
            + " MAX(RQUST) AS RQUST, MAX(VNLOAD) AS VNLOAD, MAX(VNUNLOAD) AS VNUNLOAD "
            + " FROM ( "
            + " SELECT "
            + " SRP_TYPE, SRP_CODE, VLD_LDNUM, "
            + " CASE WHEN VLD_TTYPE = 'REQUEST' THEN 'Yes' ELSE 'No' END AS RQUST, "
            + " CASE WHEN VLD_TTYPE = 'LOAD' THEN 'Yes'  ELSE 'No' END AS VNLOAD, "
            + " CASE WHEN VLD_TTYPE = 'UNLOAD' THEN 'Yes'  ELSE 'No' END AS VNUNLOAD "
            + " FROM STP_MSTR_SLREP "
            + " LEFT JOIN INV_VAN_LDTRX ON SRP_CODE = VLD_SREP AND VLD_TRXDT = '" + DateSelected + " 00:00:00.000'  "
            + " GROUP BY SRP_TYPE, SRP_CODE, SRP_NAME, VLD_LDNUM, VLD_TTYPE "
            + " ) tbl "
            + " GROUP BY SRP_TYPE, SRP_CODE, VLD_LDNUM "
            + " ORDER BY SRP_CODE, SRP_TYPE, VLD_LDNUM ";
        Session.Contents.Add("dateSelected", DateSelected);
    }
}//END CLASS