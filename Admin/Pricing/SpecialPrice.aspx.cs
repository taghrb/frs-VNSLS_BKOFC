﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using System.Data.OleDb;

public partial class Admin_Pricing_SpecialPrice : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT "
            + " SPCPR_KEY, "
            + " SPCPR_TYPE, SPCPR_BRNCD, SPCPR_CUSCD, SPCPR_ITMCD, SPCPR_UOM, "
            + " SPCPR_BQTL1, SPCPR_FQTL1, SPCPR_PRCL1, SPCPR_MINL1, SPCPR_MAXL1, "
            + " SPCPR_BQTL2, SPCPR_FQTL2, SPCPR_PRCL2, SPCPR_MINL2, SPCPR_MAXL2, "
            + " SPCPR_BQTL3, SPCPR_FQTL3, SPCPR_PRCL3, SPCPR_MINL3, SPCPR_MAXL3, "
            + " SPCPR_BQTL4, SPCPR_FQTL4, SPCPR_PRCL4, SPCPR_MINL4, SPCPR_MAXL4 "
            + " FROM INV_PRC_SPECL ";
    }

    protected void btnDownloadTemplate_Click(object sender, EventArgs e)
    {
        ASPxGridView gvTemplateToExport = new ASPxGridView();
        gvTemplateToExport.ID = "Template_SpecialPrice_Upload";
        gvTemplateToExport.AutoGenerateColumns = true;

        this.Controls.Add(gvTemplateToExport);

        gvTemplateToExport.DataSource = dbHlpr.FetchData(
            "SELECT "
            + " '' AS [Type (CT / NB)], '' AS [CustNo/CustCatg], '' AS [StrNo],'' AS [ItemNo], "
            + " '0.00' AS [BuyQty1], '0.00' AS [FreeQty1], '0.00' AS [Price1], '0.00' [MinPrice1], '0.00' [MaxPrice1], "
            + " '0.00' AS [BuyQty2], '0.00' AS [FreeQty2], '0.00' AS [Price2], '0.00' [MinPrice2], '0.00' [MaxPrice2], "
            + " '0.00' AS [BuyQty3], '0.00' AS [FreeQty3], '0.00' AS [Price3], '0.00' [MinPrice3], '0.00' [MaxPrice3], "
            + " '0.00' AS [BuyQty4], '0.00' AS [FreeQty4], '0.00' AS [Price4], '0.00' [MinPrice4], '0.00' [MaxPrice4] "
            + " WHERE 1 = 0 "
            );
        gvTemplateToExport.DataBind();

        dbHlpr.ExportToFormattedXls1997(this, gvTemplateToExport);
        this.Controls.Remove(gvTemplateToExport);
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        lblErrorMessage.Text = "";
        lblResult.Text = "";

        if (UploadedXlsFile.HasFile)
        {
            try
            {
                string fileNameWithExt = Path.GetFileName(UploadedXlsFile.FileName);
                string fileNameWithoutExt = Path.GetFileNameWithoutExtension(UploadedXlsFile.FileName);
                string fileExtension = fileNameWithExt.Replace(fileNameWithoutExt, "");
                if (fileExtension.Equals(".xls"))
                {
                    DateTime nw = DateTime.Now;
                    string timeStamp = nw.Year + nw.Month + nw.Day + "$" + nw.Hour + nw.Minute;
                    string saveAt = Server.MapPath("~/Uploads/ProductPrices/") + timeStamp + "_" + fileNameWithExt;
                    UploadedXlsFile.SaveAs(saveAt);

                    string resultMessage = "File Uploaded Successfully";


                    OleDbConnection oleDbConn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + saveAt + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"");

                    oleDbConn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    DataSet ds = new DataSet();

                    DataTable dtSheets;
                    dtSheets = oleDbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string sheetName = dtSheets.Rows[0]["TABLE_NAME"].ToString();

                    cmd.Connection = oleDbConn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                    oleda = new OleDbDataAdapter(cmd);
                    oleda.Fill(ds, "dsS1no");
                    DataTable dt = ds.Tables["dsS1no"];

                    DatabaseHelperClass dbhlpr = new DatabaseHelperClass();
                    int rowsInserted = 0;
                    int duplicatesFound = 0;
                    int rowsUpdated = 0;
                    int rowsSkipped = 0;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string PriceType = dt.Rows[i][0].ToString().Trim().Equals("") ? "CT" : dt.Rows[i][0].ToString().Trim(); // CT / NB
                        string Customer = dt.Rows[i][1].ToString().Trim().Equals("") ? "*NULL" : dt.Rows[i][1].ToString().Trim();
                        string StrNo = dt.Rows[i][2].ToString().Trim().Equals("") ? "*NULL" : dt.Rows[i][2].ToString().Trim();
                        string ItemNo = dt.Rows[i][3].ToString().Trim().Equals("") ? "*NULL" : dt.Rows[i][3].ToString().Trim();
                        string BuyQty1 = dt.Rows[i][4].ToString().Trim().Equals("") ? "0" : dt.Rows[i][4].ToString().Trim();
                        string FreeQty1 = dt.Rows[i][5].ToString().Trim().Equals("") ? "0" : dt.Rows[i][5].ToString().Trim();
                        string Price1 = dt.Rows[i][6].ToString().Trim().Equals("") ? "0" : dt.Rows[i][6].ToString().Trim();
                        string MinPrice1 = dt.Rows[i][7].ToString().Trim().Equals("") ? "0" : dt.Rows[i][7].ToString().Trim();
                        string MaxPrice1 = dt.Rows[i][8].ToString().Trim().Equals("") ? "0" : dt.Rows[i][8].ToString().Trim();
                        string BuyQty2 = dt.Rows[i][9].ToString().Trim().Equals("") ? "0" : dt.Rows[i][9].ToString().Trim();
                        string FreeQty2 = dt.Rows[i][10].ToString().Trim().Equals("") ? "0" : dt.Rows[i][10].ToString().Trim();
                        string Price2 = dt.Rows[i][11].ToString().Trim().Equals("") ? "0" : dt.Rows[i][11].ToString().Trim();
                        string MinPrice2 = dt.Rows[i][12].ToString().Trim().Equals("") ? "0" : dt.Rows[i][12].ToString().Trim();
                        string MaxPrice2 = dt.Rows[i][13].ToString().Trim().Equals("") ? "0" : dt.Rows[i][13].ToString().Trim();
                        string BuyQty3 = dt.Rows[i][14].ToString().Trim().Equals("") ? "0" : dt.Rows[i][14].ToString().Trim();
                        string FreeQty3 = dt.Rows[i][15].ToString().Trim().Equals("") ? "0" : dt.Rows[i][15].ToString().Trim();
                        string Price3 = dt.Rows[i][16].ToString().Trim().Equals("") ? "0" : dt.Rows[i][16].ToString().Trim();
                        string MinPrice3 = dt.Rows[i][17].ToString().Trim().Equals("") ? "0" : dt.Rows[i][17].ToString().Trim();
                        string MaxPrice3 = dt.Rows[i][18].ToString().Trim().Equals("") ? "0" : dt.Rows[i][18].ToString().Trim();
                        string BuyQty4 = dt.Rows[i][19].ToString().Trim().Equals("") ? "0" : dt.Rows[i][19].ToString().Trim();
                        string FreeQty4 = dt.Rows[i][20].ToString().Trim().Equals("") ? "0" : dt.Rows[i][20].ToString().Trim();
                        string Price4 = dt.Rows[i][21].ToString().Trim().Equals("") ? "0" : dt.Rows[i][21].ToString().Trim();
                        string MinPrice4 = dt.Rows[i][22].ToString().Trim().Equals("") ? "0" : dt.Rows[i][22].ToString().Trim();
                        string MaxPrice4 = dt.Rows[i][23].ToString().Trim().Equals("") ? "0" : dt.Rows[i][23].ToString().Trim();

                        DataTable dtRoutes = dbHlpr.FetchData("SELECT SRP_CODE FROM STP_MSTR_SLREP WHERE SRP_STRNO = '" + StrNo + "'");

                        if (Convert.ToDouble(MaxPrice1) >= Convert.ToDouble(MinPrice1)
                            || Convert.ToDouble(MaxPrice2) >= Convert.ToDouble(MinPrice2)
                            || Convert.ToDouble(MaxPrice3) >= Convert.ToDouble(MinPrice3)
                            || Convert.ToDouble(MaxPrice4) >= Convert.ToDouble(MinPrice4)
                            )
                        {
                            string mainUom = "";
                            DataTable dtItemMainUoms = dbHlpr.FetchData("SELECT ITU_UOMCD FROM INV_ITM_UOMS WHERE ITU_ITMCD = '" + ItemNo + "' AND ITU_MAIN = '1' ");
                            if (dtItemMainUoms.Rows.Count > 0)
                            {
                                mainUom = dtItemMainUoms.Rows[0]["ITU_UOMCD"].ToString().Trim();
                            }

                            string key = PriceType + StrNo + Customer + ItemNo + mainUom;
                            DataTable dtExisting = dbhlpr.FetchData("SELECT * "
                                + " FROM INV_PRC_SPECL "
                                + " WHERE "
                                + " SPCPR_KEY = '" + key + "' ");

                            if (dtExisting.Rows.Count > 0)
                            {
                                duplicatesFound++;
                            }

                            if (dtExisting.Rows.Count == 0)
                            {
                                dbHlpr.ExecuteNonQuery("INSERT INTO INV_PRC_SPECL ( "
                                    + " SPCPR_KEY, "
                                    + " SPCPR_TYPE, SPCPR_BRNCD, SPCPR_CUSCD, SPCPR_ITMCD, SPCPR_UOM, "
                                    + " SPCPR_BQTL1, SPCPR_FQTL1, SPCPR_PRCL1, SPCPR_MINL1, SPCPR_MAXL1, "
                                    + " SPCPR_BQTL2, SPCPR_FQTL2, SPCPR_PRCL2, SPCPR_MINL2, SPCPR_MAXL2, "
                                    + " SPCPR_BQTL3, SPCPR_FQTL3, SPCPR_PRCL3, SPCPR_MINL3, SPCPR_MAXL3, "
                                    + " SPCPR_BQTL4, SPCPR_FQTL4, SPCPR_PRCL4, SPCPR_MINL4, SPCPR_MAXL4 "
                                    + " ) VALUES ( "
                                    + " '" + key + "', "
                                    + " '" + PriceType + "', '" + StrNo + "', '" + Customer + "', '" + ItemNo + "', '" + mainUom + "', "
                                    + " '" + BuyQty1 + "', '" + FreeQty1 + "', '" + Price1 + "', '" + MinPrice1 + "', '" + MaxPrice1 + "', "
                                    + " '" + BuyQty2 + "', '" + FreeQty2 + "', '" + Price2 + "', '" + MinPrice2 + "', '" + MaxPrice2 + "', "
                                    + " '" + BuyQty3 + "', '" + FreeQty3 + "', '" + Price3 + "', '" + MinPrice3 + "', '" + MaxPrice3 + "', "
                                    + " '" + BuyQty4 + "', '" + FreeQty4 + "', '" + Price4 + "', '" + MinPrice4 + "', '" + MaxPrice4 + "' "
                                    + " )");
                                rowsInserted++;

                                DataTable dt1 = dbHlpr.FetchData("SELECT "
                                    + " SPCPR_KEY AS TEXT01, SPCPR_TYPE AS TEXT02, SPCPR_BRNCD AS TEXT03, SPCPR_CUSCD AS TEXT04, "
                                    + " SPCPR_ITMCD AS TEXT05, SPCPR_UOM AS TEXT06, "
                                    + " SPCPR_BQTL1 AS NUM01, SPCPR_FQTL1 AS NUM02, SPCPR_PRCL1 AS NUM03, SPCPR_MINL1 AS NUM04, SPCPR_MAXL1 AS NUM05, "
                                    + " SPCPR_BQTL2 AS NUM06, SPCPR_FQTL2 AS NUM07, SPCPR_PRCL2 AS NUM08, SPCPR_MINL2 AS NUM09, SPCPR_MAXL2 AS NUM10, "
                                    + " SPCPR_BQTL3 AS NUM11, SPCPR_FQTL3 AS NUM12, SPCPR_PRCL3 AS NUM13, SPCPR_MINL3 AS NUM14, SPCPR_MAXL3 AS NUM15, "
                                    + " SPCPR_BQTL4 AS NUM16, SPCPR_FQTL4 AS NUM17, SPCPR_PRCL4 AS NUM18, SPCPR_MINL4 AS NUM19, SPCPR_MAXL4 AS NUM20 "
                                    + " FROM INV_PRC_SPECL "
                                    + " WHERE SPCPR_KEY = '" + key + "' ");
                                for (int j = 0; j < dtRoutes.Rows.Count; j++)
                                {
                                    dbHlpr.CreateDownloadRecord(dt1, dtRoutes.Rows[j]["SRP_CODE"].ToString(), "INV_PRC_SPECL", "CRT");
                                }
                            }
                            else if (dtExisting.Rows.Count > 0) // && chkBxUpdateDuplicates.Checked)
                            {
                                dbhlpr.ExecuteNonQuery(" DELETE FROM INV_PRC_SPECL WHERE SPCPR_KEY = '" + key + "' ");

                                DataTable dtDlt = dbHlpr.FetchData("SELECT '" + key + "' AS TEXT01 ");                                
                                for (int j = 0; j < dtRoutes.Rows.Count; j++)
                                {
                                    dbHlpr.CreateDownloadRecord(dtDlt, dtRoutes.Rows[j]["SRP_CODE"].ToString(), "INV_PRC_SPECL", "DLT");
                                }

                                string updateQry = "INSERT INTO INV_PRC_SPECL ( "
                                    + " SPCPR_KEY, "
                                    + " SPCPR_TYPE, SPCPR_BRNCD, SPCPR_CUSCD, SPCPR_ITMCD, SPCPR_UOM, "
                                    + " SPCPR_BQTL1, SPCPR_FQTL1, SPCPR_PRCL1, SPCPR_MINL1, SPCPR_MAXL1, "
                                    + " SPCPR_BQTL2, SPCPR_FQTL2, SPCPR_PRCL2, SPCPR_MINL2, SPCPR_MAXL2, "
                                    + " SPCPR_BQTL3, SPCPR_FQTL3, SPCPR_PRCL3, SPCPR_MINL3, SPCPR_MAXL3, "
                                    + " SPCPR_BQTL4, SPCPR_FQTL4, SPCPR_PRCL4, SPCPR_MINL4, SPCPR_MAXL4 "
                                    + " ) VALUES ( "
                                    + " '" + key + "', "
                                    + " '" + PriceType + "', '" + StrNo + "', '" + Customer + "', '" + ItemNo + "', '" + mainUom + "', "
                                    + " '" + BuyQty1 + "', '" + FreeQty1 + "', '" + Price1 + "', '" + MinPrice1 + "', '" + MaxPrice1 + "', "
                                    + " '" + BuyQty2 + "', '" + FreeQty2 + "', '" + Price2 + "', '" + MinPrice2 + "', '" + MaxPrice2 + "', "
                                    + " '" + BuyQty3 + "', '" + FreeQty3 + "', '" + Price3 + "', '" + MinPrice3 + "', '" + MaxPrice3 + "', "
                                    + " '" + BuyQty4 + "', '" + FreeQty4 + "', '" + Price4 + "', '" + MinPrice4 + "', '" + MaxPrice4 + "' "
                                    + " )";
                                dbhlpr.ExecuteNonQuery(updateQry);
                                rowsUpdated++;

                                DataTable dt1 = dbHlpr.FetchData("SELECT "
                                    + " SPCPR_KEY AS TEXT01, SPCPR_TYPE AS TEXT02, SPCPR_BRNCD AS TEXT03, SPCPR_CUSCD AS TEXT04, "
                                    + " SPCPR_ITMCD AS TEXT05, SPCPR_UOM AS TEXT06, "
                                    + " SPCPR_BQTL1 AS NUM01, SPCPR_FQTL1 AS NUM02, SPCPR_PRCL1 AS NUM03, SPCPR_MINL1 AS NUM04, SPCPR_MAXL1 AS NUM05, "
                                    + " SPCPR_BQTL2 AS NUM06, SPCPR_FQTL2 AS NUM07, SPCPR_PRCL2 AS NUM08, SPCPR_MINL2 AS NUM09, SPCPR_MAXL2 AS NUM10, "
                                    + " SPCPR_BQTL3 AS NUM11, SPCPR_FQTL3 AS NUM12, SPCPR_PRCL3 AS NUM13, SPCPR_MINL3 AS NUM14, SPCPR_MAXL3 AS NUM15, "
                                    + " SPCPR_BQTL4 AS NUM16, SPCPR_FQTL4 AS NUM17, SPCPR_PRCL4 AS NUM18, SPCPR_MINL4 AS NUM19, SPCPR_MAXL4 AS NUM20 "
                                    + " FROM INV_PRC_SPECL "
                                    + " WHERE SPCPR_KEY = '" + key + "' ");
                                for (int j = 0; j < dtRoutes.Rows.Count; j++)
                                {
                                    dbHlpr.CreateDownloadRecord(dt1, dtRoutes.Rows[j]["SRP_CODE"].ToString(), "INV_PRC_SPECL", "CRT");
                                }
                            }
                        }
                        else
                        {
                            rowsSkipped++;
                        }
                    }

                    lblResult.Text = resultMessage;
                    lblResult.Text += "\n" + rowsInserted + " records inserted successfully.";
                    lblResult.Text += "\n" + duplicatesFound + " duplicate records found.";
                    lblResult.Text += "\n" + rowsUpdated + " records updated successfully.";
                    lblResult.Text += "\n" + rowsSkipped + " records skipped. Max Price cannot be less than Min Price.";
                }
                else
                {
                    lblErrorMessage.Text = "Please Select a valid .xls file";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        else
        {
            lblErrorMessage.Text = "Please Select a file to Upload.";
        }
    }

    protected void gvInquery_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string SCode = e.Keys["SPCPR_KEY"].ToString().Trim();

        DataTable dtStore = dbHlpr.FetchData("SELECT SPCPR_BRNCD FROM INV_PRC_SPECL WHERE SPCPR_KEY = '" + SCode + "' ");

        dbHlpr.ExecuteNonQuery("DELETE FROM INV_PRC_SPECL WHERE SPCPR_KEY = '" + SCode + "' ");

        if (dtStore.Rows.Count > 0)
        {
            DataTable dt = dbHlpr.FetchData("SELECT '" + SCode + "' AS TEXT01 ");
            DataTable dtRoutes = dbHlpr.FetchData("SELECT SRP_CODE FROM STP_MSTR_SLREP WHERE SRP_STRNO = '" + dtStore.Rows[0]["SPCPR_BRNCD"].ToString() + "'");
            for (int i = 0; i < dtRoutes.Rows.Count; i++)
            {
                dbHlpr.CreateDownloadRecord(dt, dtRoutes.Rows[i]["SRP_CODE"].ToString(), "INV_PRC_SPECL", "DLT");
            }
        }

        e.Cancel = true;
        gvInquery.CancelEdit();
    }

    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        RadioButtonList rbtnPriceType = ((RadioButtonList)gvInquery.FindEditFormTemplateControl("rbtnPriceType"));
        ASPxComboBox cmbxBranchCode = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxBranchCode"));
        ASPxComboBox cmbxCustomerCode = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxCustomerCode"));
        ASPxComboBox cmbxItemCode = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxItemCode"));

        string priceType = rbtnPriceType.SelectedValue == null ? "" : rbtnPriceType.SelectedValue.ToString().Trim();
        string brnCode = cmbxBranchCode.Value == null ? "" : cmbxBranchCode.Value.ToString().Trim();
        string cusCode = cmbxCustomerCode.Value == null ? "" : cmbxCustomerCode.Value.ToString().Trim();
        string itmCode = cmbxItemCode.Value == null ? "" : cmbxItemCode.Value.ToString().Trim();

        if (priceType.Length <= 0 || cusCode.Length <= 0 || brnCode.Length <= 0 || itmCode.Length <= 0)
        {
            gvInquery.CancelEdit();
            gvInquery.DataBind();
            return;
        }

        string mainUom = "";
        DataTable dtItemMainUoms = dbHlpr.FetchData("SELECT ITU_UOMCD FROM INV_ITM_UOMS WHERE ITU_ITMCD = '" + itmCode + "' AND ITU_MAIN = '1' ");
        if (dtItemMainUoms.Rows.Count > 0)
        {
            mainUom = dtItemMainUoms.Rows[0]["ITU_UOMCD"].ToString().Trim();
        }

        string key = priceType + brnCode + cusCode + itmCode + mainUom;
        dbHlpr.ExecuteNonQuery("DELETE FROM INV_PRC_SPECL WHERE SPCPR_KEY = '" + key + "' ");

        DataTable dt = dbHlpr.FetchData("SELECT '" + key + "' AS TEXT01 ");
        DataTable dtRoutes = dbHlpr.FetchData("SELECT SRP_CODE FROM STP_MSTR_SLREP WHERE SRP_STRNO = '" + brnCode + "'");
        for (int i = 0; i < dtRoutes.Rows.Count; i++)
        {
            dbHlpr.CreateDownloadRecord(dt, dtRoutes.Rows[i]["SRP_CODE"].ToString(), "INV_PRC_SPECL", "DLT");
        }

        ASPxTextBox txtBuyQty1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtBuyQty1"));
        ASPxTextBox txtFreeQty1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtFreeQty1"));
        ASPxTextBox txtPrice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice1"));
        ASPxTextBox txtMinPrice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice1"));
        ASPxTextBox txtMaxPrice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice1"));

        ASPxTextBox txtBuyQty2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtBuyQty2"));
        ASPxTextBox txtFreeQty2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtFreeQty2"));
        ASPxTextBox txtPrice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice2"));
        ASPxTextBox txtMinPrice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice2"));
        ASPxTextBox txtMaxPrice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice2"));

        ASPxTextBox txtBuyQty3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtBuyQty3"));
        ASPxTextBox txtFreeQty3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtFreeQty3"));
        ASPxTextBox txtPrice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice3"));
        ASPxTextBox txtMinPrice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice3"));
        ASPxTextBox txtMaxPrice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice3"));

        ASPxTextBox txtBuyQty4 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtBuyQty4"));
        ASPxTextBox txtFreeQty4 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtFreeQty4"));
        ASPxTextBox txtPrice4 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice4"));
        ASPxTextBox txtMinPrice4 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice4"));
        ASPxTextBox txtMaxPrice4 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice4"));

        string buyQty1 = txtBuyQty1.Text;
        string freeQty1 = txtFreeQty1.Text;
        string price1 = txtPrice1.Text;
        string minPrice1 = txtMinPrice1.Text;
        string maxPrice1 = txtMaxPrice1.Text;

        string buyQty2 = txtBuyQty2.Text;
        string freeQty2 = txtFreeQty2.Text;
        string price2 = txtPrice2.Text;
        string minPrice2 = txtMinPrice2.Text;
        string maxPrice2 = txtMaxPrice2.Text;

        string buyQty3 = txtBuyQty3.Text;
        string freeQty3 = txtFreeQty3.Text;
        string price3 = txtPrice3.Text;
        string minPrice3 = txtMinPrice3.Text;
        string maxPrice3 = txtMaxPrice3.Text;

        string buyQty4 = txtBuyQty4.Text;
        string freeQty4 = txtFreeQty4.Text;
        string price4 = txtPrice4.Text;
        string minPrice4 = txtMinPrice4.Text;
        string maxPrice4 = txtMaxPrice4.Text;

        dbHlpr.ExecuteNonQuery("INSERT INTO INV_PRC_SPECL ( "
                        + " SPCPR_KEY, "
                        + " SPCPR_TYPE, SPCPR_BRNCD, SPCPR_CUSCD, SPCPR_ITMCD, SPCPR_UOM, "
                        + " SPCPR_BQTL1, SPCPR_FQTL1, SPCPR_PRCL1, SPCPR_MINL1, SPCPR_MAXL1, "
                        + " SPCPR_BQTL2, SPCPR_FQTL2, SPCPR_PRCL2, SPCPR_MINL2, SPCPR_MAXL2, "
                        + " SPCPR_BQTL3, SPCPR_FQTL3, SPCPR_PRCL3, SPCPR_MINL3, SPCPR_MAXL3, "
                        + " SPCPR_BQTL4, SPCPR_FQTL4, SPCPR_PRCL4, SPCPR_MINL4, SPCPR_MAXL4 "
                        + " ) VALUES ( "
                        + " '" + key + "', "
                        + " '" + priceType + "', '" + brnCode + "', '" + cusCode + "', '" + itmCode + "', '" + mainUom + "', "
                        + " '" + buyQty1 + "', '" + freeQty1 + "', '" + price1 + "', '" + minPrice1 + "', '" + maxPrice1 + "', "
                        + " '" + buyQty2 + "', '" + freeQty2 + "', '" + price2 + "', '" + minPrice2 + "', '" + maxPrice2 + "', "
                        + " '" + buyQty3 + "', '" + freeQty3 + "', '" + price3 + "', '" + minPrice3 + "', '" + maxPrice3 + "', "
                        + " '" + buyQty4 + "', '" + freeQty4 + "', '" + price4 + "', '" + minPrice4 + "', '" + maxPrice4 + "' "
                        + " )");

        DataTable dt1 = dbHlpr.FetchData("SELECT "
            + " SPCPR_KEY AS TEXT01, SPCPR_TYPE AS TEXT02, SPCPR_BRNCD AS TEXT03, SPCPR_CUSCD AS TEXT04, " 
            + " SPCPR_ITMCD AS TEXT05, SPCPR_UOM AS TEXT06, "
            + " SPCPR_BQTL1 AS NUM01, SPCPR_FQTL1 AS NUM02, SPCPR_PRCL1 AS NUM03, SPCPR_MINL1 AS NUM04, SPCPR_MAXL1 AS NUM05, "
            + " SPCPR_BQTL2 AS NUM06, SPCPR_FQTL2 AS NUM07, SPCPR_PRCL2 AS NUM08, SPCPR_MINL2 AS NUM09, SPCPR_MAXL2 AS NUM10, "
            + " SPCPR_BQTL3 AS NUM11, SPCPR_FQTL3 AS NUM12, SPCPR_PRCL3 AS NUM13, SPCPR_MINL3 AS NUM14, SPCPR_MAXL3 AS NUM15, "
            + " SPCPR_BQTL4 AS NUM16, SPCPR_FQTL4 AS NUM17, SPCPR_PRCL4 AS NUM18, SPCPR_MINL4 AS NUM19, SPCPR_MAXL4 AS NUM20 "
            + " FROM INV_PRC_SPECL "
            + " WHERE SPCPR_KEY = '" + key + "' ");
        for (int i = 0; i < dtRoutes.Rows.Count; i++)
        {
            dbHlpr.CreateDownloadRecord(dt1, dtRoutes.Rows[i]["SRP_CODE"].ToString(), "INV_PRC_SPECL", "CRT");
        }

        gvInquery.CancelEdit();
        gvInquery.DataBind();
    }
    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

    }
    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        {
            string SId = e.CommandArgs.CommandArgument.ToString();

            if (SId != "") // Code is sent via request
            {
                DataTable dtStore = dbHlpr.FetchData("SELECT SPCPR_BRNCD FROM INV_PRC_SPECL WHERE SPCPR_KEY = '" + SId + "' ");

                dbHlpr.ExecuteNonQuery("DELETE FROM INV_PRC_SPECL WHERE SPCPR_KEY = '" + SId + "' ");

                if (dtStore.Rows.Count > 0)
                {
                    DataTable dt = dbHlpr.FetchData("SELECT '" + SId + "' AS TEXT01 ");
                    DataTable dtRoutes = dbHlpr.FetchData("SELECT SRP_CODE FROM STP_MSTR_SLREP WHERE SRP_STRNO = '" + dtStore.Rows[0]["SPCPR_BRNCD"].ToString() + "'");
                    for (int i = 0; i < dtRoutes.Rows.Count; i++)
                    {
                        dbHlpr.CreateDownloadRecord(dt, dtRoutes.Rows[i]["SRP_CODE"].ToString(), "INV_PRC_SPECL", "DLT");
                    }
                }

                gvInquery.CancelEdit();
                gvInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid Category ID. Unable to delete Category.";
            }
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }
    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }
    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void txtCode_Init(object sender, EventArgs e)
    {
        if (!gvInquery.IsNewRowEditing)
        {
            ASPxTextBox txtCode = (ASPxTextBox)sender;
            txtCode.ReadOnly = true;
        }
        else
        {
            ASPxTextBox txtCode1 = (ASPxTextBox)sender;
            txtCode1.ReadOnly = false;
        }
    }
    protected void cmbxItemCode_ValueChanged(object sender, EventArgs e)
    {
        SelectedValueChanged(sender, e);
    }
    protected void cmbxBranchCode_ValueChanged(object sender, EventArgs e)
    {
        SelectedValueChanged(sender, e);
    }
    protected void cmbxCustomerCode_ValueChanged(object sender, EventArgs e)
    {
        SelectedValueChanged(sender, e);
    }
    protected void SelectedValueChanged(object sender, EventArgs e)
    {
        ASPxTextBox txtBuyQty1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtBuyQty1"));
        ASPxTextBox txtFreeQty1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtFreeQty1"));
        ASPxTextBox txtPrice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice1"));
        ASPxTextBox txtMinPrice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice1"));
        ASPxTextBox txtMaxPrice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice1"));

        ASPxTextBox txtBuyQty2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtBuyQty2"));
        ASPxTextBox txtFreeQty2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtFreeQty2"));
        ASPxTextBox txtPrice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice2"));
        ASPxTextBox txtMinPrice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice2"));
        ASPxTextBox txtMaxPrice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice2"));

        ASPxTextBox txtBuyQty3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtBuyQty3"));
        ASPxTextBox txtFreeQty3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtFreeQty3"));
        ASPxTextBox txtPrice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice3"));
        ASPxTextBox txtMinPrice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice3"));
        ASPxTextBox txtMaxPrice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice3"));

        ASPxTextBox txtBuyQty4 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtBuyQty4"));
        ASPxTextBox txtFreeQty4 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtFreeQty4"));
        ASPxTextBox txtPrice4 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice4"));
        ASPxTextBox txtMinPrice4 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice4"));
        ASPxTextBox txtMaxPrice4 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice4"));

        txtBuyQty1.Text = "";
        txtFreeQty1.Text = "";
        txtPrice1.Text = "";
        txtMinPrice1.Text = "";
        txtMaxPrice1.Text = "";

        txtBuyQty2.Text = "";
        txtFreeQty2.Text = "";
        txtPrice2.Text = "";
        txtMinPrice2.Text = "";
        txtMaxPrice2.Text = "";

        txtBuyQty3.Text = "";
        txtFreeQty3.Text = "";
        txtPrice3.Text = "";
        txtMinPrice3.Text = "";
        txtMaxPrice3.Text = "";

        txtBuyQty4.Text = "";
        txtFreeQty4.Text = "";
        txtPrice4.Text = "";
        txtMinPrice4.Text = "";
        txtMaxPrice4.Text = "";

        RadioButtonList rbtnPriceType = ((RadioButtonList)gvInquery.FindEditFormTemplateControl("rbtnPriceType"));
        ASPxComboBox cmbxBranchCode = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxBranchCode"));
        ASPxComboBox cmbxCustomerCode = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxCustomerCode"));
        ASPxComboBox cmbxItemCode = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxItemCode"));

        string priceType = rbtnPriceType.SelectedValue == null ? "" : rbtnPriceType.SelectedValue.ToString().Trim();
        string brnCode = cmbxBranchCode.Value == null ? "" : cmbxBranchCode.Value.ToString().Trim();
        string cusCode = cmbxCustomerCode.Value == null ? "" : cmbxCustomerCode.Value.ToString().Trim();
        string itmCode = cmbxItemCode.Value == null ? "" : cmbxItemCode.Value.ToString().Trim();
        string mainUom = "";

        if (cmbxItemCode.Value != null)
        {
            DataTable dtbl = dbHlpr.FetchData("SELECT PRO_DESC1 FROM INV_MSTR_PRODT WHERE PRO_CODE = '" + itmCode + "' ");
            ASPxLabel lblItemDesc = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblItemDesc");
            if (dtbl.Rows.Count > 0)
            {
                lblItemDesc.Text = dtbl.Rows[0]["PRO_DESC1"].ToString();
            }
            else
            {
                lblItemDesc.Text = "";
            }

            DataTable dtItemMainUoms = dbHlpr.FetchData("SELECT ITU_UOMCD FROM INV_ITM_UOMS WHERE ITU_ITMCD = '" + itmCode + "' AND ITU_MAIN = '1' ");
            if (dtItemMainUoms.Rows.Count > 0)
            {
                mainUom = dtItemMainUoms.Rows[0]["ITU_UOMCD"].ToString().Trim();
            }
        }

        string key = priceType + brnCode + cusCode + itmCode + mainUom;
        DataTable exstng = dbHlpr.FetchData("SELECT "
                + " SPCPR_KEY, "
                + " SPCPR_TYPE, SPCPR_BRNCD, SPCPR_CUSCD, SPCPR_ITMCD, SPCPR_UOM, "
                + " SPCPR_BQTL1, SPCPR_FQTL1, SPCPR_PRCL1, SPCPR_MINL1, SPCPR_MAXL1, "
                + " SPCPR_BQTL2, SPCPR_FQTL2, SPCPR_PRCL2, SPCPR_MINL2, SPCPR_MAXL2, "
                + " SPCPR_BQTL3, SPCPR_FQTL3, SPCPR_PRCL3, SPCPR_MINL3, SPCPR_MAXL3, "
                + " SPCPR_BQTL4, SPCPR_FQTL4, SPCPR_PRCL4, SPCPR_MINL4, SPCPR_MAXL4 "
                + " FROM INV_PRC_SPECL "
                + " WHERE SPCPR_KEY = '" + key + "'; ");

        if (exstng.Rows.Count > 0)
        {
            txtBuyQty1.Text = exstng.Rows[0]["SPCPR_BQTL1"].ToString();
            txtFreeQty1.Text = exstng.Rows[0]["SPCPR_FQTL1"].ToString();
            txtPrice1.Text = exstng.Rows[0]["SPCPR_PRCL1"].ToString();
            txtMinPrice1.Text = exstng.Rows[0]["SPCPR_MINL1"].ToString();
            txtMaxPrice1.Text = exstng.Rows[0]["SPCPR_MAXL1"].ToString();

            txtBuyQty2.Text = exstng.Rows[0]["SPCPR_BQTL2"].ToString();
            txtFreeQty2.Text = exstng.Rows[0]["SPCPR_FQTL2"].ToString();
            txtPrice2.Text = exstng.Rows[0]["SPCPR_PRCL2"].ToString();
            txtMinPrice2.Text = exstng.Rows[0]["SPCPR_MINL2"].ToString();
            txtMaxPrice2.Text = exstng.Rows[0]["SPCPR_MAXL2"].ToString();

            txtBuyQty3.Text = exstng.Rows[0]["SPCPR_BQTL3"].ToString();
            txtFreeQty3.Text = exstng.Rows[0]["SPCPR_FQTL3"].ToString();
            txtPrice3.Text = exstng.Rows[0]["SPCPR_PRCL3"].ToString();
            txtMinPrice3.Text = exstng.Rows[0]["SPCPR_MINL3"].ToString();
            txtMaxPrice3.Text = exstng.Rows[0]["SPCPR_MAXL3"].ToString();

            txtBuyQty4.Text = exstng.Rows[0]["SPCPR_BQTL4"].ToString();
            txtFreeQty4.Text = exstng.Rows[0]["SPCPR_FQTL4"].ToString();
            txtPrice4.Text = exstng.Rows[0]["SPCPR_PRCL4"].ToString();
            txtMinPrice4.Text = exstng.Rows[0]["SPCPR_MINL4"].ToString();
            txtMaxPrice4.Text = exstng.Rows[0]["SPCPR_MAXL4"].ToString();
        }
    }
}