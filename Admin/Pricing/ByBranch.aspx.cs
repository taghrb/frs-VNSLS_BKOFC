﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using System.Data.OleDb;

public partial class Admin_Pricing_ByBranch : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT "
            + " BRPRC_KEY, BRPRC_BRNCD, BRPRC_ITMCD, BRPRC_UOM, BRPRC_PRICE, BRPRC_MIN, BRPRC_MAX "
            + " FROM INV_PRC_BRNCH ";
    }

    protected void btnDownloadTemplate_Click(object sender, EventArgs e)
    {
        ASPxGridView gvTemplateToExport = new ASPxGridView();
        gvTemplateToExport.ID = "Template_BranchPrice_Upload";
        gvTemplateToExport.AutoGenerateColumns = true;

        this.Controls.Add(gvTemplateToExport);

        gvTemplateToExport.DataSource = dbHlpr.FetchData(
            "SELECT " +
            " '' AS [Item No], '' AS [UoM], '0.00' AS [Price], '0.00' AS [Min Price], '0.00' AS [Max Price], '' [Store No] " +
            " WHERE 1 = 0 "
            );
        gvTemplateToExport.DataBind();

        dbHlpr.ExportToFormattedXls1997(this, gvTemplateToExport);
        this.Controls.Remove(gvTemplateToExport);
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        lblErrorMessage.Text = "";
        lblResult.Text = "";

        if (UploadedXlsFile.HasFile)
        {
            try
            {
                string fileNameWithExt = Path.GetFileName(UploadedXlsFile.FileName);
                string fileNameWithoutExt = Path.GetFileNameWithoutExtension(UploadedXlsFile.FileName);
                string fileExtension = fileNameWithExt.Replace(fileNameWithoutExt, "");
                if (fileExtension.Equals(".xls"))
                {
                    DateTime nw = DateTime.Now;
                    string timeStamp = nw.Year + nw.Month + nw.Day + "$" + nw.Hour + nw.Minute;
                    string saveAt = Server.MapPath("~/Uploads/ProductPrices/") + timeStamp + "_" + fileNameWithExt;
                    UploadedXlsFile.SaveAs(saveAt);

                    string resultMessage = "File Uploaded Successfully";


                    OleDbConnection oleDbConn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + saveAt + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"");

                    oleDbConn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    DataSet ds = new DataSet();

                    DataTable dtSheets;
                    dtSheets = oleDbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string sheetName = dtSheets.Rows[0]["TABLE_NAME"].ToString();

                    cmd.Connection = oleDbConn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                    oleda = new OleDbDataAdapter(cmd);
                    oleda.Fill(ds, "dsS1no");
                    DataTable dt = ds.Tables["dsS1no"];

                    DatabaseHelperClass dbhlpr = new DatabaseHelperClass();
                    int rowsInserted = 0;
                    int duplicatesFound = 0;
                    int rowsUpdated = 0;
                    int rowsSkipped = 0;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string ItemNo = dt.Rows[i][0].ToString().Trim().Equals("") ? "*NULL" : dt.Rows[i][0].ToString().Trim();
                        string UoM = dt.Rows[i][1].ToString().Trim().Equals("") ? "*NULL" : dt.Rows[i][1].ToString().Trim();
                        string Price = dt.Rows[i][2].ToString().Trim().Equals("") ? "0" : dt.Rows[i][2].ToString().Trim();
                        string MinPrice = dt.Rows[i][3].ToString().Trim().Equals("") ? "0" : dt.Rows[i][3].ToString().Trim();
                        string MaxPrice = dt.Rows[i][4].ToString().Trim().Equals("") ? "0" : dt.Rows[i][4].ToString().Trim();
                        string StrNo = dt.Rows[i][5].ToString().Trim().Equals("") ? "*NULL" : dt.Rows[i][5].ToString().Trim();

                        DataTable dtRoutes = dbHlpr.FetchData("SELECT SRP_CODE FROM STP_MSTR_SLREP WHERE SRP_STRNO = '" + StrNo + "'");

                        if (Convert.ToDouble(MaxPrice) >= Convert.ToDouble(MinPrice))
                        {
                            DataTable dtExisting = dbhlpr.FetchData("SELECT * "
                                + " FROM INV_PRC_BRNCH "
                                + " WHERE "
                                + " BRPRC_ITMCD = '" + ItemNo + "' "
                                + " AND BRPRC_UOM = '" + UoM + "' "
                                + " AND BRPRC_BRNCD = '" + StrNo + "' ");

                            if (dtExisting.Rows.Count > 0)
                            {
                                duplicatesFound++;
                            }

                            string key = StrNo + ItemNo + UoM;
                            if (dtExisting.Rows.Count == 0)
                            {
                                dbHlpr.ExecuteNonQuery("INSERT INTO INV_PRC_BRNCH ( "
                                    + " BRPRC_KEY, BRPRC_BRNCD, BRPRC_ITMCD, BRPRC_UOM, BRPRC_PRICE, BRPRC_MIN, BRPRC_MAX "
                                    + " ) VALUES ( "
                                    + " '" + key + "', '" + StrNo + "', '" + ItemNo + "', '" + UoM + "', '" + Price + "', '" + MinPrice + "', '" + MaxPrice + "' "
                                    + " )");
                                rowsInserted++;

                                DataTable dt1 = dbHlpr.FetchData("SELECT "
                                    + " BRPRC_KEY AS TEXT01, BRPRC_BRNCD AS TEXT02, BRPRC_ITMCD AS TEXT03, BRPRC_UOM AS TEXT04, "
                                    + " BRPRC_PRICE AS NUM01, BRPRC_MIN AS NUM02, BRPRC_MAX AS NUM03 "
                                    + " FROM INV_PRC_BRNCH "
                                    + " WHERE BRPRC_KEY = '" + key + "' ");
                                for (int j = 0; j < dtRoutes.Rows.Count; j++)
                                {
                                    dbHlpr.CreateDownloadRecord(dt1, dtRoutes.Rows[j]["SRP_CODE"].ToString(), "INV_PRC_BRNCH", "CRT");
                                }
                            }
                            else if (dtExisting.Rows.Count > 0) // && chkBxUpdateDuplicates.Checked)
                            {
                                string updateQry = "UPDATE INV_PRC_BRNCH SET "
                                    + " BRPRC_KEY = '" + key + "', BRPRC_BRNCD = '" + StrNo + "', BRPRC_ITMCD = '" + ItemNo + "', BRPRC_UOM = '" + UoM + "', "
                                    + " BRPRC_PRICE = '" + Price + "', BRPRC_MIN = '" + MinPrice + "', BRPRC_MAX = '" + MaxPrice + "' "
                                    + " WHERE "
                                    + " BRPRC_ITMCD = '" + ItemNo + "' "
                                    + " AND BRPRC_UOM = '" + UoM + "' "
                                    + " AND BRPRC_BRNCD = '" + StrNo + "' ";
                                rowsUpdated += dbhlpr.ExecuteNonQuery(updateQry);

                                DataTable dt1 = dbHlpr.FetchData("SELECT "
                                    + " BRPRC_KEY AS TEXT01, BRPRC_BRNCD AS TEXT02, BRPRC_ITMCD AS TEXT03, BRPRC_UOM AS TEXT04, "
                                    + " BRPRC_PRICE AS NUM01, BRPRC_MIN AS NUM02, BRPRC_MAX AS NUM03 "
                                    + " FROM INV_PRC_BRNCH "
                                    + " WHERE BRPRC_KEY = '" + key + "' ");
                                for (int j = 0; j < dtRoutes.Rows.Count; j++)
                                {
                                    dbHlpr.CreateDownloadRecord(dt1, dtRoutes.Rows[j]["SRP_CODE"].ToString(), "INV_PRC_BRNCH", "UPD");
                                }
                            }
                        }
                        else
                        {
                            rowsSkipped++;
                        }
                    }

                    lblResult.Text = resultMessage;
                    lblResult.Text += "\n" + rowsInserted + " records inserted successfully.";
                    lblResult.Text += "\n" + duplicatesFound + " duplicate records found.";
                    lblResult.Text += "\n" + rowsUpdated + " records updated successfully.";
                    lblResult.Text += "\n" + rowsSkipped + " records skipped. Max Price cannot be less than Min Price.";
                }
                else
                {
                    lblErrorMessage.Text = "Please Select a valid .xls file";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        else
        {
            lblErrorMessage.Text = "Please Select a file to Upload.";
        }
    }

    protected void gvInquery_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string SCode = e.Keys["BRPRC_KEY"].ToString().Trim();

        DataTable dtStore = dbHlpr.FetchData("SELECT BRPRC_BRNCD FROM INV_PRC_BRNCH WHERE BRPRC_KEY = '" + SCode + "' ");

        dbHlpr.ExecuteNonQuery("DELETE FROM INV_PRC_BRNCH WHERE BRPRC_KEY = '" + SCode + "' ");

        if (dtStore.Rows.Count > 0)
        {
            DataTable dt = dbHlpr.FetchData("SELECT '" + SCode + "' AS TEXT01 ");
            DataTable dtRoutes = dbHlpr.FetchData("SELECT SRP_CODE FROM STP_MSTR_SLREP WHERE SRP_STRNO = '" + dtStore.Rows[0]["BRPRC_BRNCD"].ToString() + "'");
            for (int i = 0; i < dtRoutes.Rows.Count; i++)
			{
                dbHlpr.CreateDownloadRecord(dt, dtRoutes.Rows[i]["SRP_CODE"].ToString(), "INV_PRC_BRNCH", "DLT");
			}
        }

        e.Cancel = true;
        gvInquery.CancelEdit();
    }

    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxComboBox lkUpBranch = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxBranchCode"));
        ASPxComboBox lkUpItem = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxItemCode"));

        string brnCode = lkUpBranch.Value == null ? "" : lkUpBranch.Value.ToString().Trim();
        string itmCode = lkUpItem.Value == null ? "" : lkUpItem.Value.ToString().Trim();

        if (brnCode.Length <= 0 || itmCode.Length <= 0)
        {
            gvInquery.CancelEdit();
            gvInquery.DataBind();
            return;
        }

        System.Web.UI.HtmlControls.HtmlTableRow row1 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw1"));
        System.Web.UI.HtmlControls.HtmlTableRow row2 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw2"));
        System.Web.UI.HtmlControls.HtmlTableRow row3 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw3"));

        DataTable dtRoutes = dbHlpr.FetchData("SELECT SRP_CODE FROM STP_MSTR_SLREP WHERE SRP_STRNO = '" + brnCode + "'");

        dbHlpr.ExecuteNonQuery("DELETE FROM INV_PRC_BRNCH WHERE BRPRC_BRNCD = '" + brnCode + "' AND BRPRC_ITMCD = '" + itmCode + "' ");

        DataTable dtDlt = dbHlpr.FetchData("SELECT "
            + " '" + brnCode + "' AS TEXT02, '" + itmCode + "' AS TEXT03 ");
        for (int i = 0; i < dtRoutes.Rows.Count; i++)
        {
            dbHlpr.CreateDownloadRecord(dtDlt, dtRoutes.Rows[i]["SRP_CODE"].ToString(), "INV_PRC_BRNCH", "DLT");
        }
        if (row1 != null)
        {
            Label lblUom1 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom1"));
            ASPxTextBox txtPrice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice1"));
            ASPxTextBox txtMinPrice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice1"));
            ASPxTextBox txtMaxPrice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice1"));

            string uom1 = lblUom1.Text;
            string price1 = txtPrice1.Text;
            string minPrice1 = txtMinPrice1.Text;
            string maxPrice1 = txtMaxPrice1.Text;

            if (uom1.Trim().Length > 0 && price1.Trim().Length > 0)
            {
                string key = brnCode + itmCode + uom1;
                dbHlpr.ExecuteNonQuery("INSERT INTO INV_PRC_BRNCH ( "
                    + " BRPRC_KEY, BRPRC_BRNCD, BRPRC_ITMCD, BRPRC_UOM, BRPRC_PRICE, BRPRC_MIN, BRPRC_MAX "
                    + " ) VALUES ( "
                    + " '" + key + "', '" + brnCode + "', '" + itmCode + "', '" + uom1 + "', '" + price1 + "', '" + minPrice1 + "', '" + maxPrice1 + "' "
                    + " )");

                DataTable dt1 = dbHlpr.FetchData("SELECT "
                    + " BRPRC_KEY AS TEXT01, BRPRC_BRNCD AS TEXT02, BRPRC_ITMCD AS TEXT03, BRPRC_UOM AS TEXT04, "
                    + " BRPRC_PRICE AS NUM01, BRPRC_MIN AS NUM02, BRPRC_MAX AS NUM03 "
                    + " FROM INV_PRC_BRNCH "
                    + " WHERE BRPRC_KEY = '" + key + "' ");
                for (int i = 0; i < dtRoutes.Rows.Count; i++)
                {
                    dbHlpr.CreateDownloadRecord(dt1, dtRoutes.Rows[i]["SRP_CODE"].ToString(), "INV_PRC_BRNCH", "CRT");
                }
            }
        }

        if (row2 != null)
        {
            Label lblUom2 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom2"));
            ASPxTextBox txtPrice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice2"));
            ASPxTextBox txtMinPrice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice2"));
            ASPxTextBox txtMaxPrice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice2"));

            string uom2 = lblUom2.Text;
            string price2 = txtPrice2.Text;
            string minPrice2 = txtMinPrice2.Text;
            string maxPrice2 = txtMaxPrice2.Text;

            if (uom2.Trim().Length > 0 && price2.Trim().Length > 0)
            {
                string key = brnCode + itmCode + uom2;
                dbHlpr.ExecuteNonQuery("INSERT INTO INV_PRC_BRNCH ( "
                    + " BRPRC_KEY, BRPRC_BRNCD, BRPRC_ITMCD, BRPRC_UOM, BRPRC_PRICE, BRPRC_MIN, BRPRC_MAX "
                    + " ) VALUES ( "
                    + " '" + key + "', '" + brnCode + "', '" + itmCode + "', '" + uom2 + "', '" + price2 + "', '" + minPrice2 + "', '" + maxPrice2 + "' "
                    + " )");

                DataTable dt1 = dbHlpr.FetchData("SELECT "
                    + " BRPRC_KEY AS TEXT01, BRPRC_BRNCD AS TEXT02, BRPRC_ITMCD AS TEXT03, BRPRC_UOM AS TEXT04, "
                    + " BRPRC_PRICE AS NUM01, BRPRC_MIN AS NUM02, BRPRC_MAX AS NUM03 "
                    + " FROM INV_PRC_BRNCH "
                    + " WHERE BRPRC_KEY = '" + key + "' ");
                for (int i = 0; i < dtRoutes.Rows.Count; i++)
                {
                    dbHlpr.CreateDownloadRecord(dt1, dtRoutes.Rows[i]["SRP_CODE"].ToString(), "INV_PRC_BRNCH", "CRT");
                }
            }
        }

        if (row3 != null)
        {
            Label lblUom3 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom3"));
            ASPxTextBox txtPrice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice3"));
            ASPxTextBox txtMinPrice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice3"));
            ASPxTextBox txtMaxPrice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice3"));

            string uom3 = lblUom3.Text;
            string price3 = txtPrice3.Text;
            string minPrice3 = txtMinPrice3.Text;
            string maxPrice3 = txtMaxPrice3.Text;

            if (uom3.Trim().Length > 0 && price3.Trim().Length > 0)
            {
                string key = brnCode + itmCode + uom3;
                dbHlpr.ExecuteNonQuery("INSERT INTO INV_PRC_BRNCH ( "
                    + " BRPRC_KEY, BRPRC_BRNCD, BRPRC_ITMCD, BRPRC_UOM, BRPRC_PRICE, BRPRC_MIN, BRPRC_MAX "
                    + " ) VALUES ( "
                    + " '" + key + "', '" + brnCode + "', '" + itmCode + "', '" + uom3 + "', '" + price3 + "', '" + minPrice3 + "', '" + maxPrice3 + "' "
                    + " )");

                DataTable dt1 = dbHlpr.FetchData("SELECT "
                    + " BRPRC_KEY AS TEXT01, BRPRC_BRNCD AS TEXT02, BRPRC_ITMCD AS TEXT03, BRPRC_UOM AS TEXT04, "
                    + " BRPRC_PRICE AS NUM01, BRPRC_MIN AS NUM02, BRPRC_MAX AS NUM03 "
                    + " FROM INV_PRC_BRNCH "
                    + " WHERE BRPRC_KEY = '" + key + "' ");
                for (int i = 0; i < dtRoutes.Rows.Count; i++)
                {
                    dbHlpr.CreateDownloadRecord(dt1, dtRoutes.Rows[i]["SRP_CODE"].ToString(), "INV_PRC_BRNCH", "CRT");
                }
            }
        }

        gvInquery.CancelEdit();
        gvInquery.DataBind();
    }
    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

    }
    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        {
            string SId = e.CommandArgs.CommandArgument.ToString();

            if (SId != "") // Code is sent via request
            {
                DataTable dtStore = dbHlpr.FetchData("SELECT BRPRC_BRNCD FROM INV_PRC_BRNCH WHERE BRPRC_KEY = '" + SId + "' ");

                dbHlpr.ExecuteNonQuery("DELETE FROM INV_PRC_BRNCH WHERE BRPRC_KEY = '" + SId + "' ");

                if (dtStore.Rows.Count > 0)
                {
                    DataTable dt = dbHlpr.FetchData("SELECT '" + SId + "' AS TEXT01 ");
                    DataTable dtRoutes = dbHlpr.FetchData("SELECT SRP_CODE FROM STP_MSTR_SLREP WHERE SRP_STRNO = '" + dtStore.Rows[0]["BRPRC_BRNCD"].ToString() + "'");
                    for (int i = 0; i < dtRoutes.Rows.Count; i++)
                    {
                        dbHlpr.CreateDownloadRecord(dt, dtRoutes.Rows[i]["SRP_CODE"].ToString(), "INV_PRC_BRNCH", "DLT");
                    }
                }

                gvInquery.CancelEdit();
                gvInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid Category ID. Unable to delete Category.";
            }
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }
    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }
    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void txtCode_Init(object sender, EventArgs e)
    {
        if (!gvInquery.IsNewRowEditing)
        {
            ASPxTextBox txtCode = (ASPxTextBox)sender;
            txtCode.ReadOnly = true;
        }
        else
        {
            ASPxTextBox txtCode1 = (ASPxTextBox)sender;
            txtCode1.ReadOnly = false;
        }
    }
    protected void cmbxItemCode_ValueChanged(object sender, EventArgs e)
    {
        SelectedValueChanged(sender, e);
    }
    protected void cmbxBranchCode_ValueChanged(object sender, EventArgs e)
    {
        SelectedValueChanged(sender, e);
    }
    protected void SelectedValueChanged(object sender, EventArgs e)
    {
        ASPxComboBox lkUpBranch = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxBranchCode"));
        ASPxComboBox lkUpItem = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxItemCode"));

        System.Web.UI.HtmlControls.HtmlTableRow row1 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw1"));
        System.Web.UI.HtmlControls.HtmlTableRow row2 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw2"));
        System.Web.UI.HtmlControls.HtmlTableRow row3 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw3"));

        row1.Visible = false;
        row2.Visible = false;
        row3.Visible = false;

        DataTable dtItemUoms = new DataTable();
        if (lkUpItem.Value != null)
        {
            DataTable dtbl = dbHlpr.FetchData("SELECT PRO_DESC1 FROM INV_MSTR_PRODT WHERE PRO_CODE = '" + lkUpItem.Value + "' ");
            ASPxLabel lblItemDesc = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblItemDesc");
            if (dtbl.Rows.Count > 0)
            {
                lblItemDesc.Text = dtbl.Rows[0]["PRO_DESC1"].ToString();
            }
            else
            {
                lblItemDesc.Text = "";
            }

            dtItemUoms = dbHlpr.FetchData("SELECT "
                + " ITU_ITMCD, ITU_UOMCD, ITU_MAIN, '' AS PRICE, '' AS MINPRICE, '' AS MAXPRICE "
                + " FROM INV_ITM_UOMS "
                + " WHERE ITU_ITMCD = '" + lkUpItem.Value + "' "
                + " ORDER BY ITU_MAIN DESC ");
        }

        if (lkUpBranch.Value != null && lkUpItem.Value != null)
        {
            DataTable dtBrnchPrices = dbHlpr.FetchData(" SELECT "
                + " BRPRC_KEY, BRPRC_BRNCD, BRPRC_ITMCD, BRPRC_UOM, BRPRC_PRICE, BRPRC_MIN, BRPRC_MAX "
                + " FROM INV_PRC_BRNCH "
                + " WHERE BRPRC_BRNCD = '" + lkUpBranch.Value.ToString().Trim() + "' "
                + " AND BRPRC_ITMCD = '" + lkUpItem.Value.ToString().Trim() + "'; ");

            for (int i = 0; i < dtBrnchPrices.Rows.Count; i++)
            {
                for (int j = 0; j < dtItemUoms.Rows.Count; j++)
                {
                    if (dtBrnchPrices.Rows[i]["BRPRC_UOM"].ToString().Trim().ToLower() == dtItemUoms.Rows[j]["ITU_UOMCD"].ToString().Trim().ToLower())
                    {
                        dtItemUoms.Rows[j]["PRICE"] = dtBrnchPrices.Rows[i]["BRPRC_PRICE"];
                        dtItemUoms.Rows[j]["MINPRICE"] = dtBrnchPrices.Rows[i]["BRPRC_MIN"];
                        dtItemUoms.Rows[j]["MAXPRICE"] = dtBrnchPrices.Rows[i]["BRPRC_MAX"];
                    }
                }
            }
        }

        if (dtItemUoms.Rows.Count > 0)
        {
            Label uom1 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom1"));
            ASPxTextBox price1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice1"));
            ASPxTextBox minprice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice1"));
            ASPxTextBox maxprice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice1"));

            uom1.Text = dtItemUoms.Rows[0]["ITU_UOMCD"].ToString();
            price1.Text = dtItemUoms.Rows[0]["PRICE"].ToString();
            minprice1.Text = dtItemUoms.Rows[0]["MINPRICE"].ToString();
            maxprice1.Text = dtItemUoms.Rows[0]["MAXPRICE"].ToString();

            row1.Visible = true;
        }

        if (dtItemUoms.Rows.Count > 1)
        {
            Label uom2 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom2"));
            ASPxTextBox price2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice2"));
            ASPxTextBox minprice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice2"));
            ASPxTextBox maxprice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice2"));

            uom2.Text = dtItemUoms.Rows[1]["ITU_UOMCD"].ToString();
            price2.Text = dtItemUoms.Rows[1]["PRICE"].ToString();
            minprice2.Text = dtItemUoms.Rows[1]["MINPRICE"].ToString();
            maxprice2.Text = dtItemUoms.Rows[1]["MAXPRICE"].ToString();

            row2.Visible = true;
        }

        if (dtItemUoms.Rows.Count > 2)
        {
            Label uom3 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom3"));
            ASPxTextBox price3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice3"));
            ASPxTextBox minprice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice3"));
            ASPxTextBox maxprice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice3"));

            uom3.Text = dtItemUoms.Rows[2]["ITU_UOMCD"].ToString();
            price3.Text = dtItemUoms.Rows[2]["PRICE"].ToString();
            minprice3.Text = dtItemUoms.Rows[2]["MINPRICE"].ToString();
            maxprice3.Text = dtItemUoms.Rows[2]["MAXPRICE"].ToString();

            row3.Visible = true;
        }
    }
}