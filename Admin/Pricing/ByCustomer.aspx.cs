﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;

public partial class Admin_Pricing_ByCustomer : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT CSPRC_KEY, CSPRC_CUSCD, CSPRC_ITMCD, CSPRC_UOM, CSPRC_PRICE, CSPRC_MIN, CSPRC_MAX, CUS_NAME, PRO_DESC1 "
            + " FROM INV_PRC_CSTMR "
            + " JOIN INV_MSTR_PRODT ON CSPRC_ITMCD = PRO_CODE "
            + " JOIN STP_MST_CSTMR ON CSPRC_CUSCD = CUS_NO ";
    }

    protected void categoryListingGridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvInquery_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string key = e.Keys["CSPRC_KEY"].ToString().Trim();

        DataTable dtCustomers = dbHlpr.FetchData("SELECT DISTINCT CSPRC_CUSCD FROM INV_PRC_CSTMR WHERE CSPRC_KEY = '" + key + "' ");

        dbHlpr.ExecuteNonQuery("DELETE FROM INV_PRC_CSTMR WHERE CSPRC_KEY = '" + key + "'");

        if (dtCustomers.Rows.Count > 0)
        {
            for (int i = 0; i < dtCustomers.Rows.Count; i++)
            {
                string custNo = dtCustomers.Rows[i]["CSPRC_CUSCD"].ToString();
                DataTable dt = dbHlpr.FetchData("SELECT '" + key + "' AS TEXT01 ");
                DataTable dtCustomerVans = dbHlpr.FetchData("SELECT DISTINCT CUS_SLSRP FROM ( "
                    + " SELECT DISTINCT CUS_SLSRP FROM STP_MST_CSTMR WHERE CUS_NO = '" + custNo + "' "
                    + " UNION ALL "
                    + " SELECT DISTINCT RTC_ROUTE FROM STP_LNK_RTCUS WHERE RTC_CUSNO = '" + custNo + "' "
                    + " ) tbl ");
                foreach (DataRow drw in dtCustomerVans.Rows)
                {
                    dbHlpr.CreateDownloadRecord(dt, drw["CUS_SLSRP"].ToString(), "INV_PRC_CSTMR", "DLT");
                }
            }
        }

        e.Cancel = true;
        gvInquery.CancelEdit();
    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxComboBox lkUpCustomer = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("CustomerCode"));
        ASPxComboBox lkUpItem = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("ItemNo"));

        string custNo = lkUpCustomer.Value == null ? "" : lkUpCustomer.Value.ToString().Trim();
        string itemNo = lkUpItem.Value == null ? "" : lkUpItem.Value.ToString().Trim();

        if (custNo.Length <= 0 || itemNo.Length <= 0)
        {
            gvInquery.CancelEdit();
            gvInquery.DataBind();
            return;
        }

        System.Web.UI.HtmlControls.HtmlTableRow row1 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw1"));
        System.Web.UI.HtmlControls.HtmlTableRow row2 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw2"));
        System.Web.UI.HtmlControls.HtmlTableRow row3 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw3"));

        dbHlpr.ExecuteNonQuery("DELETE FROM INV_PRC_CSTMR WHERE CSPRC_CUSCD = '" + custNo + "' AND CSPRC_ITMCD = '" + itemNo + "' ");

        DataTable dt = dbHlpr.FetchData("SELECT '" + custNo + "' AS TEXT02, '" + itemNo + "' AS TEXT03 ");
        DataTable dtCustomerVans = dbHlpr.FetchData("SELECT DISTINCT CUS_SLSRP FROM ( "
            + " SELECT DISTINCT CUS_SLSRP FROM STP_MST_CSTMR WHERE CUS_NO = '" + custNo + "' " 
            + " UNION ALL " 
            + " SELECT DISTINCT RTC_ROUTE FROM STP_LNK_RTCUS WHERE RTC_CUSNO = '" + custNo + "' " 
            + " ) tbl ");
        foreach (DataRow drw in dtCustomerVans.Rows)
        {
            dbHlpr.CreateDownloadRecord(dt, drw["CUS_SLSRP"].ToString(), "INV_PRC_CSTMR", "DLT");
        }
        if (row1 != null)
        {
            Label lblUom1 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom1"));
            ASPxTextBox txtPrice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice1"));
            ASPxTextBox txtMinPrice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice1"));
            ASPxTextBox txtMaxPrice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice1"));

            string uom1 = lblUom1.Text;
            string price1 = txtPrice1.Text;
            string minPrice1 = txtMinPrice1.Text;
            string maxPrice1 = txtMaxPrice1.Text;

            if (uom1.Trim().Length > 0 && price1.Trim().Length > 0)
            {
                string key = custNo + itemNo + uom1;
                dbHlpr.ExecuteNonQuery("INSERT INTO INV_PRC_CSTMR ( "
                    + " CSPRC_KEY, CSPRC_CUSCD, CSPRC_ITMCD, CSPRC_UOM, CSPRC_PRICE, CSPRC_MIN, CSPRC_MAX "
                    + " ) VALUES ( "
                    + " '" + key + "', '" + custNo + "', '" + itemNo + "', '" + uom1 + "', '" + price1 + "', '" + minPrice1 + "', '" + maxPrice1 + "' "
                    + " )");

                DataTable dt1 = dbHlpr.FetchData("SELECT "
                    + " CSPRC_KEY AS TEXT01, CSPRC_CUSCD AS TEXT02, CSPRC_ITMCD AS TEXT03, CSPRC_UOM AS TEXT04, "
                    + " CSPRC_PRICE AS NUM01, CSPRC_MIN AS NUM02, CSPRC_MAX AS NUM03 "
                    + " FROM INV_PRC_CSTMR "
                    + " WHERE CSPRC_KEY = '" + key + "' ");
                foreach (DataRow drw in dtCustomerVans.Rows)
                {
                    dbHlpr.CreateDownloadRecord(dt1, drw["CUS_SLSRP"].ToString(), "INV_PRC_CSTMR", "CRT");
                }
            }
        }

        if (row2 != null)
        {
            Label lblUom2 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom2"));
            ASPxTextBox txtPrice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice2"));
            ASPxTextBox txtMinPrice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice2"));
            ASPxTextBox txtMaxPrice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice2"));

            string uom2 = lblUom2.Text;
            string price2 = txtPrice2.Text;
            string minPrice2 = txtMinPrice2.Text;
            string maxPrice2 = txtMaxPrice2.Text;

            if (uom2.Trim().Length > 0 && price2.Trim().Length > 0)
            {
                string key = custNo + itemNo + uom2;
                dbHlpr.ExecuteNonQuery("INSERT INTO INV_PRC_CSTMR ( "
                    + " CSPRC_KEY, CSPRC_CUSCD, CSPRC_ITMCD, CSPRC_UOM, CSPRC_PRICE, CSPRC_MIN, CSPRC_MAX "
                    + " ) VALUES ( "
                    + " '" + key + "', '" + custNo + "', '" + itemNo + "', '" + uom2 + "', '" + price2 + "', '" + minPrice2 + "', '" + maxPrice2 + "' "
                    + " )");

                DataTable dt1 = dbHlpr.FetchData("SELECT "
                    + " CSPRC_KEY AS TEXT01, CSPRC_CUSCD AS TEXT02, CSPRC_ITMCD AS TEXT03, CSPRC_UOM AS TEXT04, "
                    + " CSPRC_PRICE AS NUM01, CSPRC_MIN AS NUM02, CSPRC_MAX AS NUM03 "
                    + " FROM INV_PRC_CSTMR "
                    + " WHERE CSPRC_KEY = '" + key + "' ");
                foreach (DataRow drw in dtCustomerVans.Rows)
                {
                    dbHlpr.CreateDownloadRecord(dt1, drw["CUS_SLSRP"].ToString(), "INV_PRC_CSTMR", "CRT");
                }
            }
        }

        if (row3 != null)
        {
            Label lblUom3 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom3"));
            ASPxTextBox txtPrice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice3"));
            ASPxTextBox txtMinPrice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice3"));
            ASPxTextBox txtMaxPrice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice3"));

            string uom3 = lblUom3.Text;
            string price3 = txtPrice3.Text;
            string minPrice3 = txtMinPrice3.Text;
            string maxPrice3 = txtMaxPrice3.Text;

            string key = custNo + itemNo + uom3;
            if (uom3.Trim().Length > 0 && price3.Trim().Length > 0)
            {
                dbHlpr.ExecuteNonQuery("INSERT INTO INV_PRC_CSTMR ( "
                    + " CSPRC_KEY, CSPRC_CUSCD, CSPRC_ITMCD, CSPRC_UOM, CSPRC_PRICE, CSPRC_MIN, CSPRC_MAX "
                    + " ) VALUES ( "
                    + " '" + key + "', '" + custNo + "', '" + itemNo + "', '" + uom3 + "', '" + price3 + "', '" + minPrice3 + "', '" + maxPrice3 + "' "
                    + " )");

                DataTable dt1 = dbHlpr.FetchData("SELECT "
                    + " CSPRC_KEY AS TEXT01, CSPRC_CUSCD AS TEXT02, CSPRC_ITMCD AS TEXT03, CSPRC_UOM AS TEXT04, "
                    + " CSPRC_PRICE AS NUM01, CSPRC_MIN AS NUM02, CSPRC_MAX AS NUM03 "
                    + " FROM INV_PRC_CSTMR "
                    + " WHERE CSPRC_KEY = '" + key + "' ");
                foreach (DataRow drw in dtCustomerVans.Rows)
                {
                    dbHlpr.CreateDownloadRecord(dt1, drw["CUS_SLSRP"].ToString(), "INV_PRC_CSTMR", "CRT");
                }
            }
        }

        gvInquery.CancelEdit();
        gvInquery.DataBind();
    }
    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

    }
    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        {
            DataTable dtCustomers = dbHlpr.FetchData("SELECT DISTINCT CSPRC_CUSCD FROM INV_PRC_CSTMR WHERE CSPRC_KEY = '" + e.CommandArgs.CommandArgument.ToString() + "' ");

            dbHlpr.ExecuteNonQuery("DELETE FROM INV_PRC_CSTMR WHERE CSPRC_KEY = '" + e.CommandArgs.CommandArgument.ToString() + "'");

            if (dtCustomers.Rows.Count > 0)
            {
                for (int i = 0; i < dtCustomers.Rows.Count; i++)
                {
                    string custNo = dtCustomers.Rows[i]["CSPRC_CUSCD"].ToString();
                    DataTable dt = dbHlpr.FetchData("SELECT '" + e.CommandArgs.CommandArgument.ToString() + "' AS TEXT01 ");
                    DataTable dtCustomerVans = dbHlpr.FetchData("SELECT DISTINCT CUS_SLSRP FROM ( "
                        + " SELECT DISTINCT CUS_SLSRP FROM STP_MST_CSTMR WHERE CUS_NO = '" + custNo + "' "
                        + " UNION ALL "
                        + " SELECT DISTINCT RTC_ROUTE FROM STP_LNK_RTCUS WHERE RTC_CUSNO = '" + custNo + "' "
                        + " ) tbl ");
                    foreach (DataRow drw in dtCustomerVans.Rows)
                    {
                        dbHlpr.CreateDownloadRecord(dt, drw["CUS_SLSRP"].ToString(), "INV_PRC_CSTMR", "DLT");
                    }
                }
            }

            gvInquery.CancelEdit();
            gvInquery.DataBind();
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }

    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }
    protected void SelectedValueChanged(object sender, EventArgs e)
    {
        ASPxComboBox lkUpCustomer = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("CustomerCode"));
        ASPxComboBox lkUpItem = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("ItemNo"));


        ASPxLabel lblCustName = ((ASPxLabel)gvInquery.FindEditFormTemplateControl("lblCustName"));
        ASPxLabel lblItemDesc = ((ASPxLabel)gvInquery.FindEditFormTemplateControl("lblItemDesc"));
        lblCustName.Text = "";
        lblItemDesc.Text = "";

        System.Web.UI.HtmlControls.HtmlTableRow row1 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw1"));
        System.Web.UI.HtmlControls.HtmlTableRow row2 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw2"));
        System.Web.UI.HtmlControls.HtmlTableRow row3 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw3"));

        row1.Visible = false;
        row2.Visible = false;
        row3.Visible = false;

        DataTable dtItemUoms = new DataTable();
        if (lkUpItem.Value != null)
        {
            DataTable dtItemInfo = dbHlpr.FetchData("SELECT PRO_DESC1 FROM INV_MSTR_PRODT WHERE PRO_CODE = '" + lkUpItem.Value.ToString().Trim() + "' ");
            if (dtItemInfo.Rows.Count > 0)
            {
                lblItemDesc.Text = dtItemInfo.Rows[0]["PRO_DESC1"].ToString();
            }

            dtItemUoms = dbHlpr.FetchData("SELECT "
                + " ITU_ITMCD, ITU_UOMCD, '' AS PRICE, '' AS MINPRICE, '' AS MAXPRICE, ITU_MAIN "
                + " FROM INV_ITM_UOMS "
                + " WHERE ITU_ITMCD = '" + lkUpItem.Value.ToString().Trim() + "' "
                + " ORDER BY ITU_MAIN DESC, ITU_UOMCD ");
        }

        if (lkUpCustomer.Value != null)
        {
            DataTable dtCustInfo = dbHlpr.FetchData("SELECT CUS_NAME FROM STP_MST_CSTMR WHERE CUS_NO = '" + lkUpCustomer.Value.ToString().Trim() + "' ");
            if (dtCustInfo.Rows.Count > 0)
            {
                lblCustName.Text = dtCustInfo.Rows[0]["CUS_NAME"].ToString();
            }
        }

        if (lkUpCustomer.Value != null && lkUpItem.Value != null)
        {
            DataTable dtCntctPrices = dbHlpr.FetchData(" SELECT "
                + " CSPRC_KEY, CSPRC_CUSCD, CSPRC_ITMCD, CSPRC_UOM, CSPRC_PRICE, CSPRC_MIN, CSPRC_MAX "
                + " FROM INV_PRC_CSTMR "
                + " WHERE CSPRC_CUSCD = '" + lkUpCustomer.Value.ToString().Trim() + "' "
                + " AND CSPRC_ITMCD = '" + lkUpItem.Value.ToString().Trim() + "'; ");

            for (int i = 0; i < dtCntctPrices.Rows.Count; i++)
            {
                for (int j = 0; j < dtItemUoms.Rows.Count; j++)
                {
                    if (dtCntctPrices.Rows[i]["CSPRC_UOM"].ToString().Trim().ToLower() == dtItemUoms.Rows[j]["ITU_UOMCD"].ToString().Trim().ToLower())
                    {
                        dtItemUoms.Rows[j]["PRICE"] = dtCntctPrices.Rows[i]["CSPRC_PRICE"];
                        dtItemUoms.Rows[j]["MINPRICE"] = dtCntctPrices.Rows[i]["CSPRC_MIN"];
                        dtItemUoms.Rows[j]["MAXPRICE"] = dtCntctPrices.Rows[i]["CSPRC_MAX"];
                    }
                }
            }
        }

        if (dtItemUoms.Rows.Count > 0)
        {
            Label uom1 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom1"));
            ASPxTextBox price1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice1"));
            ASPxTextBox minprice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice1"));
            ASPxTextBox maxprice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice1"));

            uom1.Text = dtItemUoms.Rows[0]["ITU_UOMCD"].ToString();
            price1.Text = dtItemUoms.Rows[0]["PRICE"].ToString();
            minprice1.Text = dtItemUoms.Rows[0]["MINPRICE"].ToString();
            maxprice1.Text = dtItemUoms.Rows[0]["MAXPRICE"].ToString();

            row1.Visible = true;
        }

        if (dtItemUoms.Rows.Count > 1)
        {
            Label uom2 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom2"));
            ASPxTextBox price2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice2"));
            ASPxTextBox minprice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice2"));
            ASPxTextBox maxprice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice2"));

            uom2.Text = dtItemUoms.Rows[1]["ITU_UOMCD"].ToString();
            price2.Text = dtItemUoms.Rows[1]["PRICE"].ToString();
            minprice2.Text = dtItemUoms.Rows[1]["MINPRICE"].ToString();
            maxprice2.Text = dtItemUoms.Rows[1]["MAXPRICE"].ToString();

            row2.Visible = true;
        }

        if (dtItemUoms.Rows.Count > 2)
        {
            Label uom3 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom3"));
            ASPxTextBox price3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice3"));
            ASPxTextBox minprice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice3"));
            ASPxTextBox maxprice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice3"));

            uom3.Text = dtItemUoms.Rows[2]["ITU_UOMCD"].ToString();
            price3.Text = dtItemUoms.Rows[2]["PRICE"].ToString();
            minprice3.Text = dtItemUoms.Rows[2]["MINPRICE"].ToString();
            maxprice3.Text = dtItemUoms.Rows[2]["MAXPRICE"].ToString();

            row3.Visible = true;
        }
    }
}