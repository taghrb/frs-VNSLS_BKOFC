﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using System.Data.OleDb;

public partial class Admin_Pricing_ByCustCatg : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT "
            + " CCPRC_KEY, "
            + " CCPRC_CTGCD, CCPRC_BRNCD, CCPRC_RTCD, CCPRC_ITMCD, CCPRC_UOM, "
            + " CCPRC_PRICE, CCPRC_MIN, CCPRC_MAX, CUS_CATNM, PRO_DESC1 "
            + " FROM INV_PRC_CUCTG "
            + " JOIN INV_MSTR_PRODT ON CCPRC_ITMCD = PRO_CODE "
            + " JOIN CUS_MST_CATG ON CCPRC_CTGCD = CUS_CATCD ";
    }
    protected void btnDownloadTemplate_Click(object sender, EventArgs e)
    {
        ASPxGridView gvTemplateToExport = new ASPxGridView();
        gvTemplateToExport.ID = "Template_CustomerCatgPrice_Upload";
        gvTemplateToExport.AutoGenerateColumns = true;

        this.Controls.Add(gvTemplateToExport);

        gvTemplateToExport.DataSource = dbHlpr.FetchData(
            "SELECT " +
            " '' [Customer Catg], '' [Store No], '' [Route No], '' AS [Item No], '' AS [UoM], '0.00' AS [Price], '0.00' AS [Min Price], '0.00' AS [Max Price] " +
            " WHERE 1 = 0 "
            );
        gvTemplateToExport.DataBind();

        dbHlpr.ExportToFormattedXls1997(this, gvTemplateToExport);
        this.Controls.Remove(gvTemplateToExport);
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        lblErrorMessage.Text = "";
        lblResult.Text = "";

        if (UploadedXlsFile.HasFile)
        {
            try
            {
                string fileNameWithExt = Path.GetFileName(UploadedXlsFile.FileName);
                string fileNameWithoutExt = Path.GetFileNameWithoutExtension(UploadedXlsFile.FileName);
                string fileExtension = fileNameWithExt.Replace(fileNameWithoutExt, "");
                if (fileExtension.Equals(".xls"))
                {
                    DateTime nw = DateTime.Now;
                    string timeStamp = nw.Year + nw.Month + nw.Day + "$" + nw.Hour + nw.Minute;
                    string saveAt = Server.MapPath("~/Uploads/ProductPrices/") + timeStamp + "_" + fileNameWithExt;
                    UploadedXlsFile.SaveAs(saveAt);

                    string resultMessage = "File Uploaded Successfully";


                    OleDbConnection oleDbConn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + saveAt + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\"");

                    oleDbConn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    DataSet ds = new DataSet();

                    DataTable dtSheets;
                    dtSheets = oleDbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string sheetName = dtSheets.Rows[0]["TABLE_NAME"].ToString();

                    cmd.Connection = oleDbConn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                    oleda = new OleDbDataAdapter(cmd);
                    oleda.Fill(ds, "dsS1no");
                    DataTable dt = ds.Tables["dsS1no"];

                    DatabaseHelperClass dbhlpr = new DatabaseHelperClass();
                    int rowsInserted = 0;
                    int duplicatesFound = 0;
                    int rowsUpdated = 0;
                    int rowsSkipped = 0;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string CustomerCatg = dt.Rows[i][0].ToString().Trim().Equals("") ? "*NULL" : dt.Rows[i][0].ToString().Trim();
                        string StrNo = dt.Rows[i][1].ToString().Trim().Equals("") ? "*NULL" : dt.Rows[i][1].ToString().Trim();
                        string RouteNo = dt.Rows[i][2].ToString().Trim().Equals("") ? "*NULL" : dt.Rows[i][2].ToString().Trim();
                        string ItemNo = dt.Rows[i][3].ToString().Trim().Equals("") ? "*NULL" : dt.Rows[i][3].ToString().Trim();
                        string UoM = dt.Rows[i][4].ToString().Trim().Equals("") ? "*NULL" : dt.Rows[i][4].ToString().Trim();
                        string Price = dt.Rows[i][5].ToString().Trim().Equals("") ? "0" : dt.Rows[i][5].ToString().Trim();
                        string MinPrice = dt.Rows[i][6].ToString().Trim().Equals("") ? "0" : dt.Rows[i][6].ToString().Trim();
                        string MaxPrice = dt.Rows[i][7].ToString().Trim().Equals("") ? "0" : dt.Rows[i][7].ToString().Trim();

                        DataTable dtCustomerVans = dbHlpr.FetchData("SELECT DISTINCT CUS_SLSRP FROM ( "
                            + " SELECT DISTINCT CUS_SLSRP FROM STP_MST_CSTMR WHERE CUS_CATG = '" + CustomerCatg + "' "
                            + " UNION ALL "
                            + " SELECT DISTINCT RTC_ROUTE FROM STP_LNK_RTCUS WHERE RTC_CUSNO IN (SELECT DISTINCT CUS_NO FROM STP_MST_CSTMR WHERE CUS_CATG = '" + CustomerCatg + "') "
                            + " ) tbl ");

                        if (Convert.ToDouble(MaxPrice) >= Convert.ToDouble(MinPrice))
                        {
                            DataTable dtExisting = dbhlpr.FetchData("SELECT * "
                               + " FROM INV_PRC_CUCTG "
                               + " WHERE CCPRC_CTGCD = '" + CustomerCatg + "' "
                               + " AND CCPRC_BRNCD = '" + StrNo + "' "
                               + " AND CCPRC_RTCD = '" + RouteNo + "' "
                               + " AND CCPRC_ITMCD = '" + ItemNo + "' "
                               + " AND CCPRC_UOM = '" + UoM + "' ");

                            if (dtExisting.Rows.Count > 0)
                            {
                                duplicatesFound++;
                            }

                            string key = CustomerCatg + StrNo + RouteNo + ItemNo + UoM;
                            if (dtExisting.Rows.Count == 0)
                            {
                                dbHlpr.ExecuteNonQuery("INSERT INTO INV_PRC_CUCTG ( "
                                    + " CCPRC_KEY, CCPRC_CTGCD, CCPRC_BRNCD, CCPRC_RTCD, "
                                    + " CCPRC_ITMCD, CCPRC_UOM, CCPRC_PRICE, CCPRC_MIN, CCPRC_MAX "
                                    + " ) VALUES ( "
                                    + " '" + key + "', '" + CustomerCatg + "', '" + StrNo + "', '" + RouteNo + "', "
                                    + " '" + ItemNo + "', '" + UoM + "', '" + Price + "', '" + MinPrice + "', '" + MaxPrice + "' "
                                    + " )");
                                rowsInserted++;

                                DataTable dt1 = dbHlpr.FetchData("SELECT "
                                    + " CCPRC_KEY AS TEXT01, CCPRC_CTGCD AS TEXT02, CCPRC_BRNCD AS TEXT03, CCPRC_RTCD AS TEXT04, "
                                    + " CCPRC_ITMCD AS TEXT05, CCPRC_UOM AS TEXT06, CCPRC_PRICE AS NUM01, CCPRC_MIN AS NUM02, CCPRC_MAX AS NUM03 "
                                    + " FROM INV_PRC_CUCTG "
                                    + " WHERE CCPRC_KEY = '" + key + "' ");
                                foreach (DataRow drw in dtCustomerVans.Rows)
                                {
                                    dbHlpr.CreateDownloadRecord(dt1, drw["CUS_SLSRP"].ToString(), "INV_PRC_CUCTG", "CRT");
                                }
                            }
                            else if (dtExisting.Rows.Count > 0) // && chkBxUpdateDuplicates.Checked)
                            {
                                string updateQry = "UPDATE INV_PRC_CUCTG SET "
                                    + " CCPRC_KEY = '" + key + "', " 
                                    + " CCPRC_CTGCD = '" + CustomerCatg + "', CCPRC_BRNCD = '" + StrNo + "', CCPRC_RTCD = '" + RouteNo + "', "
                                    + " CCPRC_ITMCD = '" + ItemNo + "', CCPRC_UOM = '" + UoM + "', "
                                    + " CCPRC_PRICE = '" + Price + "', CCPRC_MIN = '" + MinPrice + "', CCPRC_MAX = '" + MaxPrice + "' "
                                    + " WHERE "
                                    + " CCPRC_CTGCD = '" + CustomerCatg + "' "
                                    + " AND CCPRC_BRNCD = '" + StrNo + "' "
                                    + " AND CCPRC_RTCD = '" + RouteNo + "' "
                                    + " AND CCPRC_ITMCD = '" + ItemNo + "' "
                                    + " AND CCPRC_UOM = '" + UoM + "' ";
                                rowsUpdated += dbhlpr.ExecuteNonQuery(updateQry);

                                DataTable dt1 = dbHlpr.FetchData("SELECT "
                                    + " CCPRC_KEY AS TEXT01, CCPRC_CTGCD AS TEXT02, CCPRC_BRNCD AS TEXT03, CCPRC_RTCD AS TEXT04, "
                                    + " CCPRC_ITMCD AS TEXT05, CCPRC_UOM AS TEXT06, CCPRC_PRICE AS NUM01, CCPRC_MIN AS NUM02, CCPRC_MAX AS NUM03 "
                                    + " FROM INV_PRC_CUCTG "
                                    + " WHERE CCPRC_KEY = '" + key + "' ");
                                foreach (DataRow drw in dtCustomerVans.Rows)
                                {
                                    dbHlpr.CreateDownloadRecord(dt1, drw["CUS_SLSRP"].ToString(), "INV_PRC_CUCTG", "UPD");
                                }
                            }
                        }
                        else
                        {
                            rowsSkipped++;
                        }
                    }

                    lblResult.Text = resultMessage;
                    lblResult.Text += "\n" + rowsInserted + " records inserted successfully.";
                    lblResult.Text += "\n" + duplicatesFound + " duplicate records found.";
                    lblResult.Text += "\n" + rowsUpdated + " records updated successfully.";
                    lblResult.Text += "\n" + rowsSkipped + " records skipped. Max Price cannot be less than Min Price.";
                }
                else
                {
                    lblErrorMessage.Text = "Please Select a valid .xls file";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        else
        {
            lblErrorMessage.Text = "Please Select a file to Upload.";
        }
    }

    protected void categoryListingGridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvInquery_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string key = e.Keys["CCPRC_KEY"].ToString().Trim();

        DataTable dtCustCatgs = dbHlpr.FetchData("SELECT DISTINCT CCPRC_CTGCD FROM INV_PRC_CUCTG WHERE CCPRC_KEY = '" + key + "' ");

        dbHlpr.ExecuteNonQuery("DELETE FROM INV_PRC_CUCTG WHERE CCPRC_KEY = '" + key + "'");

        if (dtCustCatgs.Rows.Count > 0)
        {
            for (int i = 0; i < dtCustCatgs.Rows.Count; i++)
            {
                string custCatg = dtCustCatgs.Rows[i]["CCPRC_CTGCD"].ToString();
                DataTable dt = dbHlpr.FetchData("SELECT '" + key + "' AS TEXT01 ");
                DataTable dtCustomerVans = dbHlpr.FetchData("SELECT DISTINCT CUS_SLSRP FROM ( "
                    + " SELECT DISTINCT CUS_SLSRP FROM STP_MST_CSTMR WHERE CUS_CATG = '" + custCatg + "' "
                    + " UNION ALL "
                    + " SELECT DISTINCT RTC_ROUTE FROM STP_LNK_RTCUS WHERE RTC_CUSNO IN (SELECT DISTINCT CUS_NO FROM STP_MST_CSTMR WHERE CUS_CATG = '" + custCatg + "') "
                    + " ) tbl ");
                foreach (DataRow drw in dtCustomerVans.Rows)
                {
                    dbHlpr.CreateDownloadRecord(dt, drw["CUS_SLSRP"].ToString(), "INV_PRC_CUCTG", "DLT");
                }
            }
        }

        e.Cancel = true;
        gvInquery.CancelEdit();
    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxComboBox lkUpCustomer = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("CustomerCode"));
        ASPxComboBox lkUpBranch = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("BranchCode"));
        ASPxComboBox lkUpRoute = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("RouteCode"));
        ASPxComboBox lkUpItem = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("ItemNo"));

        string custNo = lkUpCustomer.Value == null ? "" : lkUpCustomer.Value.ToString().Trim();
        string brnNo = lkUpBranch.Value == null ? "" : lkUpBranch.Value.ToString().Trim();
        string routeNo = lkUpRoute.Value == null ? "" : lkUpRoute.Value.ToString().Trim();
        string itemNo = lkUpItem.Value == null ? "" : lkUpItem.Value.ToString().Trim();

        if (custNo.Length <= 0 || brnNo.Length <= 0 || routeNo.Length <= 0 || itemNo.Length <= 0)
        {
            gvInquery.CancelEdit();
            gvInquery.DataBind();
            return;
        }

        System.Web.UI.HtmlControls.HtmlTableRow row1 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw1"));
        System.Web.UI.HtmlControls.HtmlTableRow row2 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw2"));
        System.Web.UI.HtmlControls.HtmlTableRow row3 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw3"));

        dbHlpr.ExecuteNonQuery("DELETE FROM INV_PRC_CUCTG "
            + " WHERE CCPRC_CTGCD = '" + custNo + "' "
            + " AND CCPRC_BRNCD = '" + brnNo + "' "
            + " AND CCPRC_RTCD = '" + routeNo + "' "
            + " AND CCPRC_ITMCD = '" + itemNo + "' ");

        DataTable dt = dbHlpr.FetchData("SELECT '" + custNo + "' AS TEXT02, '" + brnNo + "' AS TEXT03, '" + routeNo + "' AS TEXT04, '" + itemNo + "' AS TEXT05 ");
        DataTable dtCustomerVans = dbHlpr.FetchData("SELECT DISTINCT CUS_SLSRP FROM ( "
            + " SELECT DISTINCT CUS_SLSRP FROM STP_MST_CSTMR WHERE CUS_CATG = '" + custNo + "' "
            + " UNION ALL "
            + " SELECT DISTINCT RTC_ROUTE FROM STP_LNK_RTCUS WHERE RTC_CUSNO IN (SELECT DISTINCT CUS_NO FROM STP_MST_CSTMR WHERE CUS_CATG = '" + custNo + "') "
            + " ) tbl ");
        foreach (DataRow drw in dtCustomerVans.Rows)
        {
            dbHlpr.CreateDownloadRecord(dt, drw["CUS_SLSRP"].ToString(), "INV_PRC_CUCTG", "DLT");
        }
        if (row1 != null)
        {
            Label lblUom1 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom1"));
            ASPxTextBox txtPrice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice1"));
            ASPxTextBox txtMinPrice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice1"));
            ASPxTextBox txtMaxPrice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice1"));

            string uom1 = lblUom1.Text;
            string price1 = txtPrice1.Text;
            string minPrice1 = txtMinPrice1.Text;
            string maxPrice1 = txtMaxPrice1.Text;

            if (uom1.Trim().Length > 0 && price1.Trim().Length > 0)
            {
                string key = custNo + brnNo + routeNo + itemNo + uom1;
                dbHlpr.ExecuteNonQuery("INSERT INTO INV_PRC_CUCTG ( "
                    + " CCPRC_KEY, CCPRC_CTGCD, CCPRC_BRNCD, CCPRC_RTCD, CCPRC_ITMCD, CCPRC_UOM, CCPRC_PRICE, CCPRC_MIN, CCPRC_MAX "
                    + " ) VALUES ( "
                    + " '" + key + "', '" + custNo + "', '" + brnNo + "', '" + routeNo + "', '" + itemNo + "', '" + uom1 + "', '" + price1 + "', '" + minPrice1 + "', '" + maxPrice1 + "' "
                    + " )");

                DataTable dt1 = dbHlpr.FetchData("SELECT "
                    + " CCPRC_KEY AS TEXT01, CCPRC_CTGCD AS TEXT02, CCPRC_BRNCD AS TEXT03, CCPRC_RTCD AS TEXT04, "
                    + " CCPRC_ITMCD AS TEXT05, CCPRC_UOM AS TEXT06, CCPRC_PRICE AS NUM01, CCPRC_MIN AS NUM02, CCPRC_MAX AS NUM03 "
                    + " FROM INV_PRC_CUCTG "
                    + " WHERE CCPRC_KEY = '" + key + "' ");
                foreach (DataRow drw in dtCustomerVans.Rows)
                {
                    dbHlpr.CreateDownloadRecord(dt1, drw["CUS_SLSRP"].ToString(), "INV_PRC_CUCTG", "CRT");
                }
            }
        }

        if (row2 != null)
        {
            Label lblUom2 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom2"));
            ASPxTextBox txtPrice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice2"));
            ASPxTextBox txtMinPrice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice2"));
            ASPxTextBox txtMaxPrice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice2"));

            string uom2 = lblUom2.Text;
            string price2 = txtPrice2.Text;
            string minPrice2 = txtMinPrice2.Text;
            string maxPrice2 = txtMaxPrice2.Text;

            if (uom2.Trim().Length > 0 && price2.Trim().Length > 0)
            {
                string key = custNo + brnNo + routeNo + itemNo + uom2;
                dbHlpr.ExecuteNonQuery("INSERT INTO INV_PRC_CUCTG ( "
                    + " CCPRC_KEY, CCPRC_CTGCD, CCPRC_BRNCD, CCPRC_RTCD, CCPRC_ITMCD, CCPRC_UOM, CCPRC_PRICE, CCPRC_MIN, CCPRC_MAX "
                    + " ) VALUES ( "
                    + " '" + key + "', '" + custNo + "', '" + brnNo + "', '" + routeNo + "', '" + itemNo + "', '" + uom2 + "', '" + price2 + "', '" + minPrice2 + "', '" + maxPrice2 + "' "
                    + " )");

                DataTable dt1 = dbHlpr.FetchData("SELECT "
                    + " CCPRC_KEY AS TEXT01, CCPRC_CTGCD AS TEXT02, CCPRC_BRNCD AS TEXT03, CCPRC_RTCD AS TEXT04, "
                    + " CCPRC_ITMCD AS TEXT05, CCPRC_UOM AS TEXT06, CCPRC_PRICE AS NUM01, CCPRC_MIN AS NUM02, CCPRC_MAX AS NUM03 "
                    + " FROM INV_PRC_CUCTG "
                    + " WHERE CCPRC_KEY = '" + key + "' ");
                foreach (DataRow drw in dtCustomerVans.Rows)
                {
                    dbHlpr.CreateDownloadRecord(dt1, drw["CUS_SLSRP"].ToString(), "INV_PRC_CUCTG", "CRT");
                }
            }
        }

        if (row3 != null)
        {
            Label lblUom3 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom3"));
            ASPxTextBox txtPrice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice3"));
            ASPxTextBox txtMinPrice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice3"));
            ASPxTextBox txtMaxPrice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice3"));

            string uom3 = lblUom3.Text;
            string price3 = txtPrice3.Text;
            string minPrice3 = txtMinPrice3.Text;
            string maxPrice3 = txtMaxPrice3.Text;

            string key = custNo + brnNo + routeNo + itemNo + uom3;
            if (uom3.Trim().Length > 0 && price3.Trim().Length > 0)
            {
                dbHlpr.ExecuteNonQuery("INSERT INTO INV_PRC_CUCTG ( "
                    + " CCPRC_KEY, CCPRC_CTGCD, CCPRC_BRNCD, CCPRC_RTCD, CCPRC_ITMCD, CCPRC_UOM, CCPRC_PRICE, CCPRC_MIN, CCPRC_MAX "
                    + " ) VALUES ( "
                    + " '" + key + "', '" + custNo + "', '" + brnNo + "', '" + routeNo + "', '" + itemNo + "', '" + uom3 + "', '" + price3 + "', '" + minPrice3 + "', '" + maxPrice3 + "' "
                    + " )");

                DataTable dt1 = dbHlpr.FetchData("SELECT "
                    + " CCPRC_KEY AS TEXT01, CCPRC_CTGCD AS TEXT02, CCPRC_BRNCD AS TEXT03, CCPRC_RTCD AS TEXT04, "
                    + " CCPRC_ITMCD AS TEXT05, CCPRC_UOM AS TEXT06, CCPRC_PRICE AS NUM01, CCPRC_MIN AS NUM02, CCPRC_MAX AS NUM03 "
                    + " FROM INV_PRC_CUCTG "
                    + " WHERE CCPRC_KEY = '" + key + "' ");
                foreach (DataRow drw in dtCustomerVans.Rows)
                {
                    dbHlpr.CreateDownloadRecord(dt1, drw["CUS_SLSRP"].ToString(), "INV_PRC_CUCTG", "CRT");
                }
            }
        }

        gvInquery.CancelEdit();
        gvInquery.DataBind();
    }
    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

    }
    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        {
            DataTable dtCustCatgs = dbHlpr.FetchData("SELECT DISTINCT CCPRC_CTGCD FROM INV_PRC_CUCTG WHERE CCPRC_KEY = '" + e.CommandArgs.CommandArgument.ToString() + "' ");

            dbHlpr.ExecuteNonQuery("DELETE FROM INV_PRC_CUCTG WHERE CCPRC_KEY = '" + e.CommandArgs.CommandArgument.ToString() + "'");

            if (dtCustCatgs.Rows.Count > 0)
            {
                for (int i = 0; i < dtCustCatgs.Rows.Count; i++)
                {
                    string custCatg = dtCustCatgs.Rows[i]["CCPRC_CTGCD"].ToString();
                    DataTable dt = dbHlpr.FetchData("SELECT '" + e.CommandArgs.CommandArgument.ToString() + "' AS TEXT01 ");
                    DataTable dtCustomerVans = dbHlpr.FetchData("SELECT DISTINCT CUS_SLSRP FROM ( "
                        + " SELECT DISTINCT CUS_SLSRP FROM STP_MST_CSTMR WHERE CUS_CATG = '" + custCatg + "' "
                        + " UNION ALL "
                        + " SELECT DISTINCT RTC_ROUTE FROM STP_LNK_RTCUS WHERE RTC_CUSNO IN (SELECT DISTINCT CUS_NO FROM STP_MST_CSTMR WHERE CUS_CATG = '" + custCatg + "') "
                        + " ) tbl ");
                    foreach (DataRow drw in dtCustomerVans.Rows)
                    {
                        dbHlpr.CreateDownloadRecord(dt, drw["CUS_SLSRP"].ToString(), "INV_PRC_CUCTG", "DLT");
                    }
                }
            }

            gvInquery.CancelEdit();
            gvInquery.DataBind();
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }

    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }
    protected void SelectedValueChanged(object sender, EventArgs e)
    {
        ASPxComboBox lkUpCustomer = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("CustomerCode"));
        ASPxComboBox lkUpBranch = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("BranchCode"));
        ASPxComboBox lkUpRoute = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("RouteCode"));
        ASPxComboBox lkUpItem = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("ItemNo"));


        ASPxLabel lblCustCatgTitle = ((ASPxLabel)gvInquery.FindEditFormTemplateControl("lblCustCatgTitle"));
        ASPxLabel lblItemDesc = ((ASPxLabel)gvInquery.FindEditFormTemplateControl("lblItemDesc"));
        lblCustCatgTitle.Text = "";
        lblItemDesc.Text = "";

        System.Web.UI.HtmlControls.HtmlTableRow row1 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw1"));
        System.Web.UI.HtmlControls.HtmlTableRow row2 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw2"));
        System.Web.UI.HtmlControls.HtmlTableRow row3 = ((System.Web.UI.HtmlControls.HtmlTableRow)gvInquery.FindEditFormTemplateControl("trw3"));

        row1.Visible = false;
        row2.Visible = false;
        row3.Visible = false;

        DataTable dtItemUoms = new DataTable();
        if (lkUpItem.Value != null)
        {
            DataTable dtItemInfo = dbHlpr.FetchData("SELECT PRO_DESC1 FROM INV_MSTR_PRODT WHERE PRO_CODE = '" + lkUpItem.Value.ToString().Trim() + "' ");
            if (dtItemInfo.Rows.Count > 0)
            {
                lblItemDesc.Text = dtItemInfo.Rows[0]["PRO_DESC1"].ToString();
            }

            dtItemUoms = dbHlpr.FetchData("SELECT "
                + " ITU_ITMCD, ITU_UOMCD, '' AS PRICE, '' AS MINPRICE, '' AS MAXPRICE, ITU_MAIN "
                + " FROM INV_ITM_UOMS "
                + " WHERE ITU_ITMCD = '" + lkUpItem.Value.ToString().Trim() + "' "
                + " ORDER BY ITU_MAIN DESC, ITU_UOMCD ");
        }

        if (lkUpCustomer.Value != null)
        {
            DataTable dtCustInfo = dbHlpr.FetchData("SELECT CUS_CATNM FROM CUS_MST_CATG WHERE CUS_CATCD = '" + lkUpCustomer.Value.ToString().Trim() + "' ");
            if (dtCustInfo.Rows.Count > 0)
            {
                lblCustCatgTitle.Text = dtCustInfo.Rows[0]["CUS_CATNM"].ToString();
            }
        }

        if (lkUpRoute.Value != null)
        {
            DataTable dtbl = dbHlpr.FetchData("SELECT "
                + " STM_CODE, STM_NAME "
                + " FROM STP_MSTR_SLREP JOIN STP_MSTR_STORE ON SRP_STRNO = STM_CODE "
                + " WHERE SRP_CODE = '" + lkUpRoute.Value + "' ");
            ASPxLabel lblBranch = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblBranch");
            if (dtbl.Rows.Count > 0)
            {
                lblBranch.Text = dtbl.Rows[0]["STM_CODE"].ToString() + " - " + dtbl.Rows[0]["STM_NAME"].ToString();
            }
            else
            {
                lblBranch.Text = "";
            }
        }

        if (lkUpCustomer.Value != null && lkUpBranch.Value != null && lkUpRoute.Value != null && lkUpItem.Value != null)
        {
            DataTable dtCntctPrices = dbHlpr.FetchData(" SELECT "
                + " CCPRC_KEY, CCPRC_CTGCD, CCPRC_BRNCD, CCPRC_RTCD, CCPRC_ITMCD, CCPRC_UOM, CCPRC_PRICE, CCPRC_MIN, CCPRC_MAX "
                + " FROM INV_PRC_CUCTG "
                + " WHERE CCPRC_CTGCD = '" + lkUpCustomer.Value.ToString().Trim() + "' "
                + " AND CCPRC_BRNCD = '" + lkUpBranch.Value.ToString().Trim() + "' "
                + " AND CCPRC_RTCD = '" + lkUpRoute.Value.ToString().Trim() + "' "
                + " AND CCPRC_ITMCD = '" + lkUpItem.Value.ToString().Trim() + "'; ");

            for (int i = 0; i < dtCntctPrices.Rows.Count; i++)
            {
                for (int j = 0; j < dtItemUoms.Rows.Count; j++)
                {
                    if (dtCntctPrices.Rows[i]["CCPRC_UOM"].ToString().Trim().ToLower() == dtItemUoms.Rows[j]["ITU_UOMCD"].ToString().Trim().ToLower())
                    {
                        dtItemUoms.Rows[j]["PRICE"] = dtCntctPrices.Rows[i]["CCPRC_PRICE"];
                        dtItemUoms.Rows[j]["MINPRICE"] = dtCntctPrices.Rows[i]["CCPRC_MIN"];
                        dtItemUoms.Rows[j]["MAXPRICE"] = dtCntctPrices.Rows[i]["CCPRC_MAX"];
                    }
                }
            }
        }

        if (dtItemUoms.Rows.Count > 0)
        {
            Label uom1 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom1"));
            ASPxTextBox price1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice1"));
            ASPxTextBox minprice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice1"));
            ASPxTextBox maxprice1 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice1"));

            uom1.Text = dtItemUoms.Rows[0]["ITU_UOMCD"].ToString();
            price1.Text = dtItemUoms.Rows[0]["PRICE"].ToString();
            minprice1.Text = dtItemUoms.Rows[0]["MINPRICE"].ToString();
            maxprice1.Text = dtItemUoms.Rows[0]["MAXPRICE"].ToString();

            row1.Visible = true;
        }

        if (dtItemUoms.Rows.Count > 1)
        {
            Label uom2 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom2"));
            ASPxTextBox price2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice2"));
            ASPxTextBox minprice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice2"));
            ASPxTextBox maxprice2 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice2"));

            uom2.Text = dtItemUoms.Rows[1]["ITU_UOMCD"].ToString();
            price2.Text = dtItemUoms.Rows[1]["PRICE"].ToString();
            minprice2.Text = dtItemUoms.Rows[1]["MINPRICE"].ToString();
            maxprice2.Text = dtItemUoms.Rows[1]["MAXPRICE"].ToString();

            row2.Visible = true;
        }

        if (dtItemUoms.Rows.Count > 2)
        {
            Label uom3 = ((Label)gvInquery.FindEditFormTemplateControl("lblUom3"));
            ASPxTextBox price3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPrice3"));
            ASPxTextBox minprice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice3"));
            ASPxTextBox maxprice3 = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice3"));

            uom3.Text = dtItemUoms.Rows[2]["ITU_UOMCD"].ToString();
            price3.Text = dtItemUoms.Rows[2]["PRICE"].ToString();
            minprice3.Text = dtItemUoms.Rows[2]["MINPRICE"].ToString();
            maxprice3.Text = dtItemUoms.Rows[2]["MAXPRICE"].ToString();

            row3.Visible = true;
        }
    }
}