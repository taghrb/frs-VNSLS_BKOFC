﻿<%@ Page Title="Special Price" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="SpecialPrice.aspx.cs" Inherits="Admin_Pricing_SpecialPrice" %>


<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <script type="text/javascript">

        function grid_customizationWindowCloseUp(s, e) {
            gvClientInquery.ShowCustomizationWindow();
        }
    </script>
    <style type="text/css">
        .fieldsetWrapper {
            padding: 10px;
            width: 50%;
            margin: 0 auto;
            background-color: #F7F5F1;
            margin-top: 5px;
            border-radius: 10px;
            border: 1px solid #3e3e3e;
            margin-bottom: 10px;
        }
    </style>
    <style type="text/css">
        .fieldsetWrapper legend {
            font-size: 16px;
            text-decoration: underline;
            text-align: center;
        }
    </style>
    <style type="text/css">
        .FormPopup fieldset {
            max-height: initial;
        }
    </style>

</asp:Content>
<asp:Content ID="contentTopHere1" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
            <li>
                <asp:LinkButton ID="LinkBtnPDF" OnClick="LinkBtnPDF_Click" runat="server">
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonXLX" OnClick="LinkButtonXLX_Click" runat="server">
                <img src="~/img/FilesIcons/excel.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonXxls" OnClick="LinkButtonXxls_Click" runat="server">
                <img src="~/img/FilesIcons/xlsx.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonRtf" OnClick="LinkButtonRtf_Click" runat="server">
                <img src="~/img/FilesIcons/rtf.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonCvs" OnClick="LinkButtonCvs_Click" runat="server">
                <img src="~/img/FilesIcons/cvs.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonWord" OnClick="LinkButtonRtf_Click" runat="server">
                <img src="~/img/FilesIcons/word.png" runat="server" />
                </asp:LinkButton>
            </li>

            <li>
                <asp:LinkButton ID="LinkButtonHtml" OnClick="LinkButtonHtml_Click" runat="server">
                <img src="~/img/icons/searchtop.jpg" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton6" OnClick="LinkButton3_Click" runat="server">               
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton7" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/word.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton8" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/excel.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton9" OnClick="LinkButton3_Click" OnClientClick="window.print();" runat="server">
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton10" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/torrent.png" runat="server" />
                </asp:LinkButton>
            </li>
        </ul>
        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvInquery" ExportedRowType="All"></dx:ASPxGridViewExporter>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">

    <div class="formDiv">
        <div class="formHeaderDiv">
            <h3>Special Price</h3>
        </div>

        <div class="fieldsetWrapper">
            <fieldset>
                <legend>Upload Special Price</legend>
                <table>
                    <tr>
                        <td>Choose File :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <asp:FileUpload ID="UploadedXlsFile" runat="server" TabIndex="1" />
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Result :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <dx:ASPxLabel ID="lblResult" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
                        </td>
                        <td></td>
                    </tr>

                </table>
            </fieldset>
            <hr style="color: skyblue" />
            <dx:ASPxLabel ID="lblErrorMessage" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
            <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                <dx:ASPxButton ID="btnUpload" runat="server" Text="Upload Prices" CssClass="btnPopup btn btn-success" CausesValidation="true" OnClick="btnUpload_Click" />
                <dx:ASPxButton ID="btnPopUpClear" runat="server" Text="Clear" CssClass="btnPopup btn btn-warning" CausesValidation="false" AutoPostBack="false">
                    <ClientSideEvents Click="function(s, e) { CallBackCode.PerformCallback(CatClientEditCode.GetText()); }" />
                </dx:ASPxButton>
                <dx:ASPxButton ID="btnDownloadTemplate" runat="server" Text="Download Xls Template" CssClass="btnPopup btn btn-info" CausesValidation="false" OnClick="btnDownloadTemplate_Click" />
            </div>
        </div>

        <asp:SqlDataSource runat="server" ID="DxGridSqlDataSource1"
            ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
        <dx:ASPxGridView ID="gvInquery" CssClass="FromGridView" ClientInstanceName="gvClientInquery" Theme="Office2010Black" runat="server" AutoGenerateColumns="False" KeyFieldName="SPCPR_KEY"
            DataSourceID="DxGridSqlDataSource1" OnCustomButtonCallback="gvInquery_CustomButtonCallback"
            OnRowCommand="gvInquery_RowCommand"
            OnDataBound="gvInquery_DataBound"
            OnBeforeGetCallbackResult="gvInquery_BeforeGetCallbackResult"
            OnRowDeleting="gvInquery_RowDeleting">

            <SettingsContextMenu Enabled="True" EnableColumnMenu="True" EnableRowMenu="True"></SettingsContextMenu>
            <SettingsSearchPanel Visible="False" CustomEditorID="CustomSearchPanelTxtBx" />
            <Columns>
                <dx:GridViewCommandColumn ShowNewButtonInHeader="false" ShowSelectButton="true" Width="110" Caption=" " AllowDragDrop="False" VisibleIndex="0"></dx:GridViewCommandColumn>
                <dx:GridViewCommandColumn ShowNewButtonInHeader="true" ShowEditButton="true" Width="110" AllowDragDrop="False" VisibleIndex="1"></dx:GridViewCommandColumn>
                <dx:GridViewCommandColumn ShowNewButtonInHeader="false" ShowDeleteButton="true" Width="130" Caption=" " AllowDragDrop="False" VisibleIndex="2"></dx:GridViewCommandColumn>

                <dx:GridViewDataTextColumn FieldName="SPCPR_TYPE" Caption="Type">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_BRNCD" Caption="Branch Code">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                    <EditFormSettings VisibleIndex="0" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_CUSCD" Caption="Customer">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_ITMCD" Caption="Item #">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_UOM" Caption="UoM">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_BQTL1" Caption="Buy Qty L1">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_FQTL1" Caption="Free Qty L1">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_PRCL1" Caption="Price L1">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_MINL1" Caption="Min Price L1">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_MAXL1" Caption="Max Price L1">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_BQTL2" Caption="Buy Qty L2">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_FQTL2" Caption="Free Qty L2">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_PRCL2" Caption="Price L2">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_MINL2" Caption="Min Price L2">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_MAXL2" Caption="Max Price L2">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_BQTL3" Caption="Buy Qty L3">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_FQTL3" Caption="Free Qty L3">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_PRCL3" Caption="Price L3">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_MINL3" Caption="Min Price L3">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_MAXL3" Caption="Max Price L3">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_BQTL4" Caption="Buy Qty L4">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_FQTL4" Caption="Free Qty L4">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_PRCL4" Caption="Price L4">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_MINL4" Caption="Min Price L4">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="SPCPR_MAXL4" Caption="Max Price L4">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
            </Columns>
            <Settings HorizontalScrollBarMode="Auto" />

            <SettingsCommandButton>
                <NewButton ButtonType="Button" Text="New" Styles-Style-CssClass="btna btn btn-primary  btn-sm">
                    <Image runat="server" Url="~/img/icons/white-icn/Add.png" Width="15px"></Image>
                </NewButton>

                <SelectButton ButtonType="Button" Text="View" Styles-Style-CssClass="btna btn btn-success  btn-sm">
                    <Image ToolTip="View button here" Url="~/img/icons/white-icn/eyee1.png" Width="15px" />
                </SelectButton>

                <EditButton ButtonType="Button" Text="Edit" Styles-Style-CssClass="btna btn btn-info  btn-sm">
                    <Image runat="server" Url="~/img/icons/white-icn/Pen Tool.png" Width="15px"></Image>
                </EditButton>

                <DeleteButton ButtonType="Button" Text="Delete" Styles-Style-CssClass="btna btn btn-danger btn-sm btn-danger-variant">
                    <Image runat="server" Url="~/img/icons/white-icn/Trash.png" Width="15px"></Image>
                </DeleteButton>

                <CancelButton ButtonType="Button" Text="Close" Styles-Style-CssClass="btnPopup btn btn-primary">
                </CancelButton>
                <UpdateButton ButtonType="Button" Text="Update">
                </UpdateButton>
            </SettingsCommandButton>
            <SettingsEditing EditFormColumnCount="2" />
            <SettingsPopup>
                <EditForm VerticalAlign="WindowCenter" AllowResize="True" Modal="True" HorizontalAlign="WindowCenter"></EditForm>
            </SettingsPopup>

            <SettingsPager Position="Bottom">
                <PageSizeItemSettings Caption="Records per Page" Items="10, 20, 50, 100" ShowAllItem="true" Position="Right" Visible="true">
                </PageSizeItemSettings>
            </SettingsPager>
            <Styles>
                <CommandColumn Spacing="0px" Wrap="False" />
                <EditFormDisplayRow HorizontalAlign="Center" VerticalAlign="Middle"></EditFormDisplayRow>
                <AlternatingRow CssClass="alterRowGV" Enabled="True"></AlternatingRow>
                <Row CssClass="rowHeight"></Row>
            </Styles>
            <Settings ShowGroupPanel="true" VerticalScrollBarMode="Auto" />
            <SettingsLoadingPanel Mode="ShowOnStatusBar" />
            <SettingsBehavior EnableCustomizationWindow="true" AllowDragDrop="true" ConfirmDelete="True" ColumnResizeMode="Control" />
            <ClientSideEvents ColumnStartDragging="grid_customizationWindowCloseUp" EndCallback="GridOnEndCallBack" />

            <Templates>
                <EditForm>
                    <div class="overlay">
                        <div class="FormPopup">
                            <div class="formHeaderDiv">
                                <h3>Special Price</h3>
                            </div>
                            <fieldset>
                                <legend></legend>
                                <table>
                                    <tr>
                                        <td>Price by : </td>
                                        <td>
                                            <asp:RadioButtonList runat="server" ID="rbtnPriceType" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="CT" Text="Customer Category" Selected="True" />
                                                <asp:ListItem Value="NB" Text="Customer Number" Enabled="false" />
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Branch: </td>
                                        <td colspan="7">
                                            <div class="DivReqDepc">
                                                <asp:SqlDataSource ID="CmBxBranchCodeSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT 'ALL' AS STM_CODE, 'All' AS STM_NAME, 'All' AS STMTXTFLD UNION ALL SELECT STM_CODE, STM_NAME, (STM_CODE + ' - ' + STM_NAME) AS STMTXTFLD FROM STP_MSTR_STORE"></asp:SqlDataSource>
                                                <dx:ASPxComboBox ID="cmbxBranchCode" runat="server" Value='<%# Bind("SPCPR_BRNCD") %>' DataSourceID="CmBxBranchCodeSqlDataSource1" TextField="STMTXTFLD" ValueField="STM_CODE" Width="170px" TabIndex="6" OnDataBound="SelectedValueChanged" OnValueChanged="cmbxBranchCode_ValueChanged" AutoPostBack="true">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="Text" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Customer : </td>
                                        <td>
                                            <div class="DivReq">
                                                <asp:SqlDataSource runat="server" ID="CmBxCustCodeSqlDataSource1" SelectCommand="SELECT CUS_CATCD AS VAL, (CUS_CATCD + ' - ' + CUS_CATNM) AS TXT FROM CUS_MST_CATG" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
                                                <dx:ASPxComboBox ID="cmbxCustomerCode" runat="server" Value='<%# Bind("SPCPR_CUSCD") %>' DataSourceID="CmBxCustCodeSqlDataSource1" TextField="TXT" ValueField="VAL" Width="170px" TabIndex="6" OnDataBound="SelectedValueChanged" OnValueChanged="cmbxCustomerCode_ValueChanged" AutoPostBack="true">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="Text" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Item: </td>
                                        <td colspan="2">
                                            <asp:SqlDataSource ID="CmBxItemCodeSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT PRO_CODE, PRO_DESC1, (PRO_CODE + ' - ' + PRO_DESC1) AS PROTXTFLD FROM INV_MSTR_PRODT"></asp:SqlDataSource>
                                            <dx:ASPxComboBox ID="cmbxItemCode" runat="server" Value='<%# Bind("SPCPR_ITMCD") %>' DataSourceID="CmBxItemCodeSqlDataSource1" TextField="PROTXTFLD" ValueField="PRO_CODE" Width="170px" TabIndex="6" OnDataBound="SelectedValueChanged" OnValueChanged="cmbxItemCode_ValueChanged" AutoPostBack="true">
                                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="Text" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <RequiredField IsRequired="true" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td colspan="5">
                                            <dx:ASPxLabel ID="lblItemDesc" runat="server" Text="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8">
                                            <h3><u>UoMs and Prices : </u></h3>
                                        </td>
                                    </tr>
                                </table>
                                <!-- Level 1 -->
                                <div class="container-fluid">
                                    <div class="row-fluid">
                                        <h4 class="col-md-12" style="border-bottom: 1px solid; margin: 0px; margin-bottom: 3px;">Level 1 : </h4>
                                        <div class="col-md-6" style="padding-top: 23px;">
                                            <label class="pull-left" style="width: 10%; text-align: left; padding-top: 8px;">Buy</label>

                                            <div class="pull-left" style="width: 35%;">
                                                <dx:ASPxTextBox ID="txtBuyQty1" ClientInstanceName="txtBuyQty1" runat="server" TabIndex="3" MaxLength="255" Width="100%">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="false" ErrorText="Buy Qty is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>

                                            <label class="pull-left" style="width: 10%; text-align: center; padding-top: 8px;">Get</label>

                                            <div class="pull-left" style="width: 35%;">
                                                <dx:ASPxTextBox ID="txtFreeQty1" ClientInstanceName="txtFreeQty1" runat="server" TabIndex="3" MaxLength="255" Width="100%">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="false" ErrorText="Free Qty is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>

                                            <label class="pull-left" style="width: 10%; text-align: right; padding-top: 8px;">Free</label>
                                        </div>

                                        <div class="col-md-2">
                                            <label>Price : </label>
                                            <dx:ASPxTextBox ID="txtPrice1" ClientInstanceName="txtPrice1" runat="server" TabIndex="3" MaxLength="255" Width="100%">
                                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RequiredField IsRequired="false" ErrorText="Price is Required" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </div>


                                        <div class="col-md-2">
                                            <label>Min Price : </label>
                                            <dx:ASPxTextBox ID="txtMinPrice1" ClientInstanceName="txtMinPrice1" runat="server" TabIndex="4" MaxLength="15" Width="100%">
                                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RequiredField IsRequired="false" ErrorText="Min. Price is Required" />
                                                </ValidationSettings>
                                                <NullTextStyle Font-Size="Small" />
                                            </dx:ASPxTextBox>
                                        </div>

                                        <div class="col-md-2">
                                            <label>Max Price : </label>
                                            <dx:ASPxTextBox ID="txtMaxPrice1" ClientInstanceName="txtMaxPrice1" runat="server" TabIndex="4" MaxLength="15" Width="100%">
                                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RequiredField IsRequired="false" ErrorText="Max. Price is Required" />
                                                </ValidationSettings>
                                                <NullTextStyle Font-Size="Small" />
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>
                                </div>
                                <!-- Level 2 -->
                                <div class="container-fluid">
                                    <div class="row-fluid">
                                        <h4 class="col-md-12" style="border-bottom: 1px solid; margin: 0px; margin-bottom: 3px;">Level 2 : </h4>
                                        <div class="col-md-6" style="padding-top: 23px;">
                                            <label class="pull-left" style="width: 10%; text-align: left; padding-top: 8px;">Buy</label>

                                            <div class="pull-left" style="width: 35%;">
                                                <dx:ASPxTextBox ID="txtBuyQty2" ClientInstanceName="txtBuyQty2" runat="server" TabIndex="3" MaxLength="255" Width="100%">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="false" ErrorText="Buy Qty is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>

                                            <label class="pull-left" style="width: 10%; text-align: center; padding-top: 8px;">Get</label>

                                            <div class="pull-left" style="width: 35%;">
                                                <dx:ASPxTextBox ID="txtFreeQty2" ClientInstanceName="txtFreeQty2" runat="server" TabIndex="3" MaxLength="255" Width="100%">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="false" ErrorText="Free Qty is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>

                                            <label class="pull-left" style="width: 10%; text-align: right; padding-top: 8px;">Free</label>
                                        </div>

                                        <div class="col-md-2">
                                            <label>Price : </label>
                                            <dx:ASPxTextBox ID="txtPrice2" ClientInstanceName="txtPrice2" runat="server" TabIndex="3" MaxLength="255" Width="100%">
                                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RequiredField IsRequired="false" ErrorText="Price is Required" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </div>


                                        <div class="col-md-2">
                                            <label>Min Price : </label>
                                            <dx:ASPxTextBox ID="txtMinPrice2" ClientInstanceName="txtMinPrice2" runat="server" TabIndex="4" MaxLength="15" Width="100%">
                                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RequiredField IsRequired="false" ErrorText="Min. Price is Required" />
                                                </ValidationSettings>
                                                <NullTextStyle Font-Size="Small" />
                                            </dx:ASPxTextBox>
                                        </div>

                                        <div class="col-md-2">
                                            <label>Max Price : </label>
                                            <dx:ASPxTextBox ID="txtMaxPrice2" ClientInstanceName="txtMaxPrice2" runat="server" TabIndex="4" MaxLength="15" Width="100%">
                                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RequiredField IsRequired="false" ErrorText="Max. Price is Required" />
                                                </ValidationSettings>
                                                <NullTextStyle Font-Size="Small" />
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>
                                </div>
                                <!-- Level 3 -->
                                <div class="container-fluid">
                                    <div class="row-fluid">
                                        <h4 class="col-md-12" style="border-bottom: 1px solid; margin: 0px; margin-bottom: 3px;">Level 3 : </h4>
                                        <div class="col-md-6" style="padding-top: 23px;">
                                            <label class="pull-left" style="width: 10%; text-align: left; padding-top: 8px;">Buy</label>

                                            <div class="pull-left" style="width: 35%;">
                                                <dx:ASPxTextBox ID="txtBuyQty3" ClientInstanceName="txtBuyQty3" runat="server" TabIndex="3" MaxLength="255" Width="100%">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="false" ErrorText="Buy Qty is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>

                                            <label class="pull-left" style="width: 10%; text-align: center; padding-top: 8px;">Get</label>

                                            <div class="pull-left" style="width: 35%;">
                                                <dx:ASPxTextBox ID="txtFreeQty3" ClientInstanceName="txtFreeQty3" runat="server" TabIndex="3" MaxLength="255" Width="100%">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="false" ErrorText="Free Qty is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>

                                            <label class="pull-left" style="width: 10%; text-align: right; padding-top: 8px;">Free</label>
                                        </div>

                                        <div class="col-md-2">
                                            <label>Price : </label>
                                            <dx:ASPxTextBox ID="txtPrice3" ClientInstanceName="txtPrice3" runat="server" TabIndex="3" MaxLength="255" Width="100%">
                                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RequiredField IsRequired="false" ErrorText="Price is Required" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </div>


                                        <div class="col-md-2">
                                            <label>Min Price : </label>
                                            <dx:ASPxTextBox ID="txtMinPrice3" ClientInstanceName="txtMinPrice3" runat="server" TabIndex="4" MaxLength="15" Width="100%">
                                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RequiredField IsRequired="false" ErrorText="Min. Price is Required" />
                                                </ValidationSettings>
                                                <NullTextStyle Font-Size="Small" />
                                            </dx:ASPxTextBox>
                                        </div>

                                        <div class="col-md-2">
                                            <label>Max Price : </label>
                                            <dx:ASPxTextBox ID="txtMaxPrice3" ClientInstanceName="txtMaxPrice3" runat="server" TabIndex="4" MaxLength="15" Width="100%">
                                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RequiredField IsRequired="false" ErrorText="Max. Price is Required" />
                                                </ValidationSettings>
                                                <NullTextStyle Font-Size="Small" />
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>
                                </div>
                                <!-- Level 4 -->
                                <div class="container-fluid">
                                    <div class="row-fluid">
                                        <h4 class="col-md-12" style="border-bottom: 1px solid; margin: 0px; margin-bottom: 3px;">Level 4 : </h4>
                                        <div class="col-md-6" style="padding-top: 23px;">
                                            <label class="pull-left" style="width: 10%; text-align: left; padding-top: 8px;">Buy</label>

                                            <div class="pull-left" style="width: 35%;">
                                                <dx:ASPxTextBox ID="txtBuyQty4" ClientInstanceName="txtBuyQty4" runat="server" TabIndex="3" MaxLength="255" Width="100%">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="false" ErrorText="Buy Qty is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>

                                            <label class="pull-left" style="width: 10%; text-align: center; padding-top: 8px;">Get</label>

                                            <div class="pull-left" style="width: 35%;">
                                                <dx:ASPxTextBox ID="txtFreeQty4" ClientInstanceName="txtFreeQty4" runat="server" TabIndex="3" MaxLength="255" Width="100%">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="false" ErrorText="Free Qty is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>

                                            <label class="pull-left" style="width: 10%; text-align: right; padding-top: 8px;">Free</label>
                                        </div>

                                        <div class="col-md-2">
                                            <label>Price : </label>
                                            <dx:ASPxTextBox ID="txtPrice4" ClientInstanceName="txtPrice4" runat="server" TabIndex="3" MaxLength="255" Width="100%">
                                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RequiredField IsRequired="false" ErrorText="Price is Required" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </div>


                                        <div class="col-md-2">
                                            <label>Min Price : </label>
                                            <dx:ASPxTextBox ID="txtMinPrice4" ClientInstanceName="txtMinPrice4" runat="server" TabIndex="4" MaxLength="15" Width="100%">
                                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RequiredField IsRequired="false" ErrorText="Min. Price is Required" />
                                                </ValidationSettings>
                                                <NullTextStyle Font-Size="Small" />
                                            </dx:ASPxTextBox>
                                        </div>

                                        <div class="col-md-2">
                                            <label>Max Price : </label>
                                            <dx:ASPxTextBox ID="txtMaxPrice4" ClientInstanceName="txtMaxPrice4" runat="server" TabIndex="4" MaxLength="15" Width="100%">
                                                <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RequiredField IsRequired="false" ErrorText="Max. Price is Required" />
                                                </ValidationSettings>
                                                <NullTextStyle Font-Size="Small" />
                                            </dx:ASPxTextBox>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                            <hr style="color: skyblue" />
                            <dx:ASPxLabel ID="lblErrorMessage" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
                            <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                                <dx:ASPxButton ID="btnAdd1" runat="server" Text="Save" CssClass="btnPopup btn btn-success" CausesValidation="true" OnClick="btnAdd1_Click" />
                                <dx:ASPxButton ID="btnPopUpClear" runat="server" Text="Cancel" CssClass="btnPopup btn btn-warning" CausesValidation="false" AutoPostBack="false">
                                    <ClientSideEvents Click="function(s, e) { CallBackCode.PerformCallback(CatClientEditCode.GetText()); }" />
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="btnPopUpDelete" runat="server" CommandArgument='<%# Eval("SPCPR_KEY") %>' CommandName="PopUpDelete" Text="Delete" CssClass="btnPopup btn btn-danger btn-danger-variant" CausesValidation="false">
                                    <ClientSideEvents Click="function(s, e) { 
                                                if (!confirm('Confirm Delete?')) { 
                                                    e.processOnServer = false;  
                                                } 
                                        }" />
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="btnPopUpPrint" runat="server" Text="Print" CssClass="btnPopup btn btn-info btn-info-variant" AutoPostBack="false" CausesValidation="false">
                                    <ClientSideEvents Click="function(s, e) { printDoc(s, e); }" />
                                </dx:ASPxButton>
                                <dx:ASPxGridViewTemplateReplacement ID="btnPopUpClose" ReplacementType="EditFormCancelButton" runat="server" />
                            </div>
                        </div>
                    </div>
                </EditForm>
            </Templates>

            <Templates>
                <StatusBar>
                    <table>
                        <tr>
                            <td>
                                <%--<dx:ASPxButton ID="btnNewRecord" runat="server" Text="Add New Item" AutoPostBack="false">
                                    <ClientSideEvents Click="function (s, e){ FromGridView.AddNewRow();  }" />
                                </dx:ASPxButton>--%>
                            </td>
                            <td>Search: </td>
                            <td>
                                <dx:ASPxButtonEdit runat="server" ID="CustomSearchPanelTxtBx" />
                            </td>
                            <td>&nbsp;</td>
                            <td>Search In:</td>
                            <td>
                                <dx:ASPxComboBox AutoPostBack="true" ID="DDLFilterByColumn" runat="server" OnSelectedIndexChanged="DDLFilterByColumn_SelectedIndexChanged">
                                    <Items>
                                        <dx:ListEditItem Text="All" Value="*" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>

                        </tr>
                    </table>
                </StatusBar>
            </Templates>

        </dx:ASPxGridView>
    </div>

    <script type="text/javascript">
        window.onload = function () {
            $('form').on('click', '.btnPopupClose', function () {
                $('.overlay').hide();
            });
        };

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //if (prm != null) {
        prm.add_endRequest(function (sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.overlay').show();
            }
        });
        prm.add_endRequest(GridOnEndCallBack);
        //};


        function OnTestValidation(s, e) {
            //var testData = e.value;
            //if (!testData) {
            lblError.SetVisible(!e.isValid);
            //  return;
        }

        function CB_categoryCodeLostFocus(s, e) {
            //var RowVals = JSON.parse(e.result);
            //if (RowVals.length > 0) {
            //    CatClientEditName.SetText(RowVals[1]);
            //    CatClientEditDivision.SetValue(RowVals[4]);
            //    CatClientEditSeqNo.SetText(RowVals[5]);
            //    $(CatClientEditImageIMAGE).attr('src', MyBaseUrl_JS + RowVals[3]);
            //    CatClientEditImageOldPath.SetText(RowVals[3]);
            //} else {
            //    CatClientEditName.SetText("");
            //    CatClientEditDivision.SetValue("");
            //    CatClientEditSeqNo.SetText("");
            //    $(CatClientEditImageIMAGE).removeAttr('src');
            //    CatClientEditImageOldPath.SetText("");
            //}
        }
    </script>
</asp:Content>

