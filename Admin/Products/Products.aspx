﻿<%@ Page Title="Manage Products" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="Products.aspx.cs" Inherits="Admin_Products_DxDefault" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content4" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <script type="text/javascript">

        function grid_customizationWindowCloseUp(s, e) {
            //if(FromGridView.IsCustomizationWindowVisible())
            FromGridView.ShowCustomizationWindow();
        }

        $(function () {
            $('.testing123').dialog();
        });

        // Code to create a custom multiselect ComboBox, Dropdown
        var textSeparator = ","; // Between each checked item in ComboBox
        function OnListBoxSelectionChanged(listBox, args) {
            if (args.index == 0)
                args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
            UpdateSelectAllItemState();
            UpdateText();
        }
        function UpdateSelectAllItemState() {
            IsAllSelected() ? checkListBox.SelectIndices([0]) : checkListBox.UnselectIndices([0]);
        }
        function IsAllSelected() {
            var selectedDataItemCount = checkListBox.GetItemCount() - (checkListBox.GetItem(0).selected ? 0 : 1);
            return checkListBox.GetSelectedItems().length == selectedDataItemCount;
        }
        function UpdateText() {
            var selectedItems = checkListBox.GetSelectedItems();
            ProdClientEditExtras.SetText(GetSelectedItemsText(selectedItems));
        }
        function SynchronizeListBoxValues(dropDown, args) {
            checkListBox.UnselectAll();
            var texts = dropDown.GetText().split(textSeparator);
            //var values = GetValuesByTexts(texts);
            checkListBox.SelectValues(texts);
            UpdateSelectAllItemState();
            UpdateText(); // for remove non-existing texts
        }
        function GetSelectedItemsText(items) {
            var texts = [];
            for (var i = 0; i < items.length; i++)
                if (items[i].index != 0)
                    texts.push(items[i].value);
            return texts.join(textSeparator);
        }
        function GetValuesByTexts(texts) {
            var actualValues = [];
            var item;
            for (var i = 0; i < texts.length; i++) {
                item = checkListBox.FindItemByText(texts[i]);
                if (item != null)
                    actualValues.push(item.value);
            }
            return actualValues;
        }

    </script>

</asp:Content>
<asp:Content ID="contentTopHere1" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
            <li>
                <asp:LinkButton ID="LinkBtnPDF" OnClick="LinkBtnPDF_Click" runat="server">
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonXLX" OnClick="LinkButtonXLX_Click" runat="server">
                <img src="~/img/FilesIcons/excel.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonXxls" OnClick="LinkButtonXxls_Click" runat="server">
                <img src="~/img/FilesIcons/xlsx.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonRtf" OnClick="LinkButtonRtf_Click" runat="server">
                <img src="~/img/FilesIcons/rtf.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonCvs" OnClick="LinkButtonCvs_Click" runat="server">
                <img src="~/img/FilesIcons/cvs.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonWord" OnClick="LinkButtonRtf_Click" runat="server">
                <img src="~/img/FilesIcons/word.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonHtml" OnClick="LinkButtonHtml_Click" runat="server">
                <img src="~/img/icons/searchtop.jpg" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton6" OnClick="LinkButton3_Click" runat="server">               
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton7" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/word.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton8" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/excel.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton9" OnClick="LinkButton3_Click" OnClientClick="window.print();" runat="server">
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton10" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/torrent.png" runat="server" />
                </asp:LinkButton>
            </li>
        </ul>
        <%--<span>Search:
                 <asp:TextBox ID="txtSearchContent" runat="server" placeholder="Search.." ToolTip="Enter Text to search at Content Page"></asp:TextBox>
            </span>--%>

        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvProductsInquery" ExportedRowType="All"></dx:ASPxGridViewExporter>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">

    <div class="formDiv">
        <div class="formHeaderDiv">
            <h3>Products </h3>
        </div>
        <%--<asp:UpdatePanel runat="server" ID="UpdatePanelOrdersPage" ChildrenAsTriggers="true">
            <ContentTemplate>--%>
        <dx:ASPxGridView ID="gvProductsInquery" CssClass="FromGridView" ClientInstanceName="FromGridView" Theme="Office2010Black" runat="server" AutoGenerateColumns="False" KeyFieldName="PRO_CODE"
            OnCustomButtonCallback="gvProductsInquery_CustomButtonCallback"
            OnStartRowEditing="gvProductsInquery_StartRowEditing"
            OnRowCommand="gvProductsInquery_RowCommand"
            OnDataBound="gvProductsInquery_DataBound"
            OnBeforeGetCallbackResult="gvProductsInquery_BeforeGetCallbackResult"
            OnRowDeleting="gvProductsInquery_RowDeleting"
            OnSelectionChanged="gvProductsInquery_SelectionChanged">

            <SettingsContextMenu Enabled="True" EnableColumnMenu="True" EnableRowMenu="True"></SettingsContextMenu>
            <SettingsSearchPanel Visible="False" CustomEditorID="CustomSearchPanelTxtBx" />
            <Columns>
                <dx:GridViewCommandColumn ShowNewButtonInHeader="false" ShowSelectButton="true" Width="110" Caption=" " AllowDragDrop="False" VisibleIndex="1"></dx:GridViewCommandColumn>
                <dx:GridViewCommandColumn ShowNewButtonInHeader="true" ShowEditButton="true" Width="110" Caption=" " AllowDragDrop="False" VisibleIndex="2"></dx:GridViewCommandColumn>
                <dx:GridViewCommandColumn ShowNewButtonInHeader="false" ShowDeleteButton="true" Width="130" Caption=" " AllowDragDrop="False" VisibleIndex="3"></dx:GridViewCommandColumn>

                <dx:GridViewDataTextColumn FieldName="PRO_CODE" Caption="Code" VisibleIndex="4">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                    <EditFormSettings VisibleIndex="0" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PRO_DESC1" Caption="Name" VisibleIndex="5">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PRO_DESC2" Caption="الاسم" VisibleIndex="6" Visible="false">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PRO_CATG" Caption=" Category" VisibleIndex="7">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PRO_SCATG" Caption="Sub Category" VisibleIndex="8">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PRO_UDF1" Caption="UDF1" VisibleIndex="9" Visible="false">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PRO_UDF2" Caption="UDF2" VisibleIndex="10" Visible="false">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PRO_UDF3" Caption="UDF3" VisibleIndex="11" Visible="false">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PRO_UDF4" Caption="UDF4" VisibleIndex="12" Visible="false">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PRO_UOM" Caption="UOM" VisibleIndex="13">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PRO_PRICE" Caption="Price" VisibleIndex="14" UnboundType="Decimal" PropertiesTextEdit-DisplayFormatString="n2">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ALT_UOM1" Caption="Alt. UoM1" VisibleIndex="15">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ALT_PRC1" Caption="Alt Price1" VisibleIndex="16" UnboundType="Decimal" PropertiesTextEdit-DisplayFormatString="n2">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ALT_CNVFC1" Caption="Alt Conv. Factor1" VisibleIndex="17" UnboundType="Decimal" PropertiesTextEdit-DisplayFormatString="n5">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ALT_UOM2" Caption="Alt. UoM2" VisibleIndex="18" Visible="false">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ALT_PRC2" Caption="Alt Price2" VisibleIndex="19" Visible="false" UnboundType="Decimal" PropertiesTextEdit-DisplayFormatString="n2">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ALT_CNVFC2" Caption="Alt Conv. Factor2" VisibleIndex="20" Visible="false" UnboundType="Decimal" PropertiesTextEdit-DisplayFormatString="n5">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ALT_UOM3" Caption="Alt. UoM3" VisibleIndex="21" Visible="false">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ALT_PRC3" Caption="Alt Price3" VisibleIndex="22" Visible="false" UnboundType="Decimal" PropertiesTextEdit-DisplayFormatString="n2">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ALT_CNVFC3" Caption="Alt Conv. Factor3" VisibleIndex="23" Visible="false" UnboundType="Decimal" PropertiesTextEdit-DisplayFormatString="n5">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PRO_TRKMD" Caption="Track Method" VisibleIndex="24" Visible="false">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PRO_WT" Caption="Weight" VisibleIndex="25" Visible="false" UnboundType="Decimal" PropertiesTextEdit-DisplayFormatString="n3">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PRO_ALCOD" Caption="Alt. Code" VisibleIndex="26" Visible="false">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PRO_STATS" Caption="Status" VisibleIndex="27" Visible="false">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PRO_TAXCD" Caption="Tax Code" VisibleIndex="28" Visible="false">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
            </Columns>

            <Settings HorizontalScrollBarMode="Auto" />

            <SettingsCommandButton>
                <NewButton ButtonType="Button" Text="New" Styles-Style-CssClass="btna btn btn-primary  btn-sm">
                    <Image runat="server" Url="~/img/icons/white-icn/Add.png" Width="15px"></Image>
                </NewButton>

                <SelectButton ButtonType="Button" Text="View" Styles-Style-CssClass="btna btn btn-success  btn-sm">
                    <Image ToolTip="View button here" Url="~/img/icons/white-icn/eyee1.png" Width="15px" />
                </SelectButton>

                <EditButton ButtonType="Button" Text="Edit" Styles-Style-CssClass="btna btn btn-info  btn-sm">
                    <Image runat="server" Url="~/img/icons/white-icn/Pen Tool.png" Width="15px"></Image>
                </EditButton>

                <DeleteButton ButtonType="Button" Text="Delete" Styles-Style-CssClass="btna btn btn-danger btn-sm btn-danger-variant">
                    <Image runat="server" Url="~/img/icons/white-icn/Trash.png" Width="15px"></Image>
                </DeleteButton>

                <CancelButton ButtonType="Button" Text="Close" Styles-Style-CssClass="btnPopup btn btn-primary">
                </CancelButton>
                <UpdateButton ButtonType="Button" Text="Update">
                </UpdateButton>
            </SettingsCommandButton>
            <SettingsEditing EditFormColumnCount="2" />
            <SettingsPopup>
                <EditForm VerticalAlign="WindowCenter" AllowResize="True" Modal="True" HorizontalAlign="WindowCenter"></EditForm>
            </SettingsPopup>

            <SettingsPager Position="Bottom">
                <PageSizeItemSettings Caption="Records per Page" Items="10, 20, 50, 100" ShowAllItem="true" Position="Right" Visible="true">
                </PageSizeItemSettings>
            </SettingsPager>
            <Styles>
                <CommandColumn Spacing="0px" Wrap="False" />
                <EditFormDisplayRow HorizontalAlign="Center" VerticalAlign="Middle"></EditFormDisplayRow>
                <AlternatingRow CssClass="alterRowGV" Enabled="True"></AlternatingRow>
                <Row CssClass="rowHeight"></Row>
            </Styles>
            <Settings ShowGroupPanel="true" VerticalScrollBarMode="Auto" />
            <SettingsLoadingPanel Mode="ShowOnStatusBar" />
            <SettingsBehavior EnableCustomizationWindow="true" AllowDragDrop="true" ConfirmDelete="True" ColumnResizeMode="Control" AllowSelectSingleRowOnly="true" ProcessSelectionChangedOnServer="true" />
            <ClientSideEvents ColumnStartDragging="grid_customizationWindowCloseUp" EndCallback="GridOnEndCallBack" />

            <Templates>
                <EditForm>
                    <div class="overlay">
                        <div class="FormPopup">
                            <div class="formHeaderDiv">
                                <h3>Product </h3>
                            </div>
                            <fieldset>
                                <legend></legend>
                                <table style="display: inline-block;">
                                    <tr>
                                        <td>Code : </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtCode" ClientInstanceName="txtClientCode" OnInit="txtCode_Init" runat="server" Width="170px" Text='<%# Bind("PRO_CODE") %>' TabIndex="1" MaxLength="15">
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RegularExpression ValidationExpression="[A-Z0-9\-]{0,15}" ErrorText="Invalid Code." />
                                                        <RequiredField IsRequired="true" ErrorText="Code is Required" />
                                                    </ValidationSettings>
                                                    <MaskSettings Mask=">ccccccccccccccc" />
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                        <td>Alternative Code : </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="ProdAltCode" ClientInstanceName="ProdClientAltCode" runat="server" Width="170px" Text='<%# Bind("PRO_ALCOD") %>' TabIndex="2" MaxLength="15">
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RegularExpression ValidationExpression="[A-Z0-9\-]{0,15}" ErrorText="Invalid Code." />
                                                        <RequiredField IsRequired="true" ErrorText="Alt. Code is Required" />
                                                    </ValidationSettings>
                                                    <MaskSettings Mask=">ccccccccccccccc" />
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Name: </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="ProdEditName" ClientInstanceName="ProdClientEditName" runat="server" Width="170px" Text='<%# Bind("PRO_DESC1") %>' TabIndex="3" MaxLength="50">
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Name is Required" />
                                                    </ValidationSettings>
                                                    <NullTextStyle Font-Size="Small" />
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                        <td>Arabic Name: </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="ProdEditArbName" ClientInstanceName="ProdClientEditArbName" runat="server" Width="170px" Text='<%# Bind("PRO_DESC2") %>' TabIndex="4" MaxLength="50">
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Arabic Name is Required" />
                                                    </ValidationSettings>
                                                    <NullTextStyle Font-Size="Small" />
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Track Method :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="ProdEditTrackMethod" ClientInstanceName="ProdClientEditTrackMethod" runat="server" Width="170px" Value='<%# Bind("PRO_TRKMD") %>' TabIndex="5">
                                                    <Items>
                                                        <dx:ListEditItem Text="Normal" Value="N" />
                                                        <dx:ListEditItem Text="LOT Detail" Value="D" />
                                                    </Items>
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Track Method is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Category : </td>
                                        <td>
                                            <dx:ASPxComboBox ID="ProdEditCategory" ClientInstanceName="ProdClientEditCategory" runat="server" Width="170px" TabIndex="6" OnInit="ProdEditCategory_Init" SelectionMode="Single">
                                                <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <RequiredField IsRequired="false" ErrorText="Category is Required" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td>Sub-Category : </td>
                                        <td>
                                            <dx:ASPxComboBox ID="ProdEditSubCategory" ClientInstanceName="ProdClientEditSubCategory" runat="server" Width="170px" TabIndex="7" OnInit="ProdEditSubCategory_Init" SelectionMode="Single">
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Class : </td>
                                        <td>
                                            <dx:ASPxComboBox ID="ProdEditUDF1" ClientInstanceName="ProdClientEditUDF1" runat="server" Width="170px" TabIndex="8" OnInit="ProdEditUDF1_Init" SelectionMode="Single">
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td>Sub Class : </td>
                                        <td>
                                            <dx:ASPxComboBox ID="ProdEditUDF2" ClientInstanceName="ProdClientEditUDF2" runat="server" Width="170px" TabIndex="9" OnInit="ProdEditUDF2_Init" SelectionMode="Single">
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Type : </td>
                                        <td>
                                            <dx:ASPxComboBox ID="ProdEditUDF3" ClientInstanceName="ProdClientEditUDF3" runat="server" Width="170px" TabIndex="10" OnInit="ProdEditUDF3_Init" SelectionMode="Single">
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td>Brand : </td>
                                        <td>
                                            <dx:ASPxComboBox ID="ProdEditUDF4" ClientInstanceName="ProdClientEditUDF4" runat="server" Width="170px" TabIndex="11" OnInit="ProdEditUDF4_Init" SelectionMode="Single">
                                            </dx:ASPxComboBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>UOM : </td>
                                        <td>
                                            <asp:SqlDataSource runat="server" ID="DS_UoMs" SelectCommand="SELECT UOM_CODE, UOM_NAME FROM STP_MSTR_UOM" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
                                            <dx:ASPxGridLookup ID="ProdEditUOM" ClientInstanceName="ProdClientEditUOM" runat="server" Width="170px" TabIndex="12" DataSourceID="DS_UoMs" TextFormatString="{1}" Value='<%# Bind("PRO_UOM") %>' KeyFieldName="UOM_CODE" SelectionMode="Single">
                                                <Columns>
                                                    <dx:GridViewDataColumn Caption="Code" FieldName="UOM_CODE"></dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Caption="Name" FieldName="UOM_NAME"></dx:GridViewDataColumn>
                                                </Columns>
                                                <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <RequiredField IsRequired="true" ErrorText="UOM is Required" />
                                                </ValidationSettings>
                                            </dx:ASPxGridLookup>
                                        </td>
                                        <td>Price :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="ProdEditPrice" ClientInstanceName="ProdClientEditPrice" runat="server" MaxLength="13" Width="170px" Text='<%# Bind("PRO_PRICE") %>' TabIndex="3">
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RegularExpression ValidationExpression="^[0-9]((\d+)?(.\d+)?)?$" ErrorText="Please enter a valid number." />

                                                        <RequiredField IsRequired="true" ErrorText="Price is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Weight : </td>
                                        <td>
                                            <dx:ASPxTextBox ID="ProdEditWeight" ClientInstanceName="ProdClientEditWeight" runat="server" Width="170px" TabIndex="14" MaxLength="5" Text='<%# Bind("PRO_WT") %>'>
                                                <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <RegularExpression ValidationExpression="^[0-9]((\d+)?(.\d+)?)?$" ErrorText="Invalid Weight, Please enter a valid number." />
                                                    <RequiredField IsRequired="true" ErrorText="Weight is Required" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td>Alternative UoMs? :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="ProdEditHasAltUOM" OnInit="ProdEditHasAltUOM_Init" ClientInstanceName="ProdClientEditHasAltUOM" runat="server" MaxLength="15" Width="170px" TabIndex="3">
                                                    <Items>
                                                        <dx:ListEditItem Text="Yes" Value="Y" />
                                                        <dx:ListEditItem Text="No" Value="N" />
                                                    </Items>
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Has Alt. UoM is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Status : </td>
                                        <td>
                                            <dx:ASPxComboBox ID="ProdEditStatus" Value='<%# Bind("PRO_STATS") %>' ClientInstanceName="ProdClientEditStatus" runat="server" Width="170px" TabIndex="11" MaxLength="5">
                                                <Items>
                                                    <dx:ListEditItem Text="Active" Value="A" />
                                                    <dx:ListEditItem Text="In-Active" Value="C" />
                                                </Items>
                                                <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <RequiredField IsRequired="true" ErrorText="Status is Required" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td>Tax Code :</td>
                                        <td>
                                            <div class="DivReq">
                                                <asp:SqlDataSource runat="server" ID="SqlDataSource4" SelectCommand="SELECT TAX_CODE, TAX_TITLE, TAX_FACTR FROM STP_MSTR_TAXES" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
                                                <dx:ASPxComboBox ID="ProdEditTaxCode" DataSourceID="SqlDataSource4" ClientInstanceName="ProdClientEditTaxCode" runat="server" MaxLength="15" ValueField="TAX_CODE" TextField="TAX_TITLE" Width="170px" Value='<%# Bind("PRO_TAXCD") %>' TabIndex="3">
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Tax Code is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table style="display: inline-block; border: 1px solid black; border-radius: 5px; padding: 0px 5px 0px 0px; margin-left: 0px;">
                                    <tr>
                                        <td colspan="2"><span style="font-size: 22px; font-weight: bold;">Alternate UoMs</span></td>
                                    </tr>
                                    <tr>
                                        <td>Alt. Unit 1 : </td>
                                        <td>
                                            <asp:SqlDataSource runat="server" ID="DS_AltUoM1" SelectCommand="SELECT UOM_CODE, UOM_NAME FROM STP_MSTR_UOM" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
                                            <dx:ASPxGridLookup ID="ProdEditAltUOM1" ClientInstanceName="ProdClientEditAltUOM1" runat="server" Width="170px" TabIndex="12" DataSourceID="DS_AltUoM1" TextFormatString="{1}" Value='<%# Bind("ALT_UOM1") %>' KeyFieldName="UOM_CODE" SelectionMode="Single">
                                                <Columns>
                                                    <dx:GridViewDataColumn Caption="Code" FieldName="UOM_CODE"></dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Caption="Name" FieldName="UOM_NAME"></dx:GridViewDataColumn>
                                                </Columns>
                                                <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                    <RequiredField IsRequired="false" ErrorText="UOM is Required" />
                                                </ValidationSettings>
                                            </dx:ASPxGridLookup>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Alt Unit 1 Price :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="ProdEditAltUOMPrc1" ClientInstanceName="ProdClientEditAltPrc1" runat="server" MaxLength="13" Width="170px" Text='<%# Bind("ALT_PRC1") %>' TabIndex="3">
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RegularExpression ValidationExpression="^[0-9]((\d+)?(.\d+)?)?$" ErrorText="Please enter a valid number." />

                                                        <RequiredField IsRequired="false" ErrorText="Price is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Alt Conv Factor 1 :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="ProdEditAltUOMCnvFc1" ClientInstanceName="ProdClientEditAltCnvFc1" runat="server" MaxLength="13" Width="170px" Text='<%# Bind("ALT_CNVFC1") %>' TabIndex="3">
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RegularExpression ValidationExpression="^[0-9]((\d+)?(.\d+)?)?$" ErrorText="Please enter a valid number." />

                                                        <RequiredField IsRequired="false" ErrorText="Conversion Factor Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Alt. Unit 2 : </td>
                                        <td>
                                            <asp:SqlDataSource runat="server" ID="DS_AltUoM2" SelectCommand="SELECT UOM_CODE, UOM_NAME FROM STP_MSTR_UOM" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
                                            <dx:ASPxGridLookup ID="ProdEditAltUOM2" ClientInstanceName="ProdClientEditAltUOM2" runat="server" Width="170px" TabIndex="12" DataSourceID="DS_AltUoM2" TextFormatString="{1}" Value='<%# Bind("ALT_UOM2") %>' KeyFieldName="UOM_CODE" SelectionMode="Single">
                                                <Columns>
                                                    <dx:GridViewDataColumn Caption="Code" FieldName="UOM_CODE"></dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Caption="Name" FieldName="UOM_NAME"></dx:GridViewDataColumn>
                                                </Columns>
                                                <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                    <RequiredField IsRequired="false" ErrorText="UOM is Required" />
                                                </ValidationSettings>
                                            </dx:ASPxGridLookup>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Alt Unit 2 Price :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="ProdEditAltUOMPrc2" ClientInstanceName="ProdClientEditAltUOMPrc2" runat="server" MaxLength="13" Width="170px" Text='<%# Bind("ALT_PRC2") %>' TabIndex="3">
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RegularExpression ValidationExpression="^[0-9]((\d+)?(.\d+)?)?$" ErrorText="Please enter a valid number." />

                                                        <RequiredField IsRequired="false" ErrorText="Price is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Alt Conv Factor 2 :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="ProdEditAltUOMCnvFc2" ClientInstanceName="ProdClientEditAltCnvFc2" runat="server" MaxLength="13" Width="170px" Text='<%# Bind("ALT_CNVFC2") %>' TabIndex="3">
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RegularExpression ValidationExpression="^[0-9]((\d+)?(.\d+)?)?$" ErrorText="Please enter a valid number." />

                                                        <RequiredField IsRequired="false" ErrorText="Conversion Factor Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Alt. Unit 3 : </td>
                                        <td>
                                            <asp:SqlDataSource runat="server" ID="DS_AltUoM3" SelectCommand="SELECT UOM_CODE, UOM_NAME FROM STP_MSTR_UOM" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
                                            <dx:ASPxGridLookup ID="ProdEditAltUOM3" ClientInstanceName="ProdClientEditAltUOM3" runat="server" Width="170px" TabIndex="12" DataSourceID="DS_AltUoM3" TextFormatString="{1}" Value='<%# Bind("ALT_UOM3") %>' KeyFieldName="UOM_CODE" SelectionMode="Single">
                                                <Columns>
                                                    <dx:GridViewDataColumn Caption="Code" FieldName="UOM_CODE"></dx:GridViewDataColumn>
                                                    <dx:GridViewDataColumn Caption="Name" FieldName="UOM_NAME"></dx:GridViewDataColumn>
                                                </Columns>
                                                <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                    <RequiredField IsRequired="false" ErrorText="UOM is Required" />
                                                </ValidationSettings>
                                            </dx:ASPxGridLookup>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Alt Unit 3 Price :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="ProdEditAltUOMPrc3" ClientInstanceName="ProdClientEditAltUOMPrc3" runat="server" MaxLength="13" Width="170px" Text='<%# Bind("ALT_PRC3") %>' TabIndex="3">
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RegularExpression ValidationExpression="^[0-9]((\d+)?(.\d+)?)?$" ErrorText="Please enter a valid number." />

                                                        <RequiredField IsRequired="false" ErrorText="Price is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Alt Conv Factor 3 :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="ProdEditAltUOMCnvFc3" ClientInstanceName="ProdClientEditAltCnvFc3" runat="server" MaxLength="13" Width="170px" Text='<%# Bind("ALT_CNVFC3") %>' TabIndex="3">
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RegularExpression ValidationExpression="^[0-9]((\d+)?(.\d+)?)?$" ErrorText="Please enter a valid number." />

                                                        <RequiredField IsRequired="false" ErrorText="Conversion Factor Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                            </fieldset>
                                <hr style="color: skyblue" />
                            <dx:ASPxLabel ID="lblErrorMessage" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
                            <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                                <dx:ASPxButton ID="btnAdd1" runat="server" Text="Save" CssClass="btnPopup btn btn-success" CausesValidation="true" OnClick="btnAdd1_Click" />
                                <dx:ASPxButton ID="btnPopUpClear" runat="server" Text="Cancel" CssClass="btnPopup btn btn-warning" CausesValidation="false" AutoPostBack="false">
                                    <ClientSideEvents Click="function(s, e) { CallBackCode.PerformCallback(txtClientCode.GetText()); }" />
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="btnPopUpDelete" runat="server" CommandArgument='<%# Eval("PRO_CODE") %>' CommandName="PopUpDelete" Text="Delete" CssClass="btnPopup btn btn-danger btn-danger-variant" CausesValidation="false">
                                    <ClientSideEvents Click="function(s, e) { 
                                                if (!confirm('Confirm Delete?')) { 
                                                    e.processOnServer = false;  
                                                } 
                                        }" />
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="btnPopUpPrint" runat="server" Text="Print" CssClass="btnPopup btn btn-info btn-info-variant" AutoPostBack="false" CausesValidation="false">
                                    <ClientSideEvents Click="function(s, e) { printDoc(s, e); }" />
                                </dx:ASPxButton>
                                <dx:ASPxGridViewTemplateReplacement ID="btnPopUpClose" ReplacementType="EditFormCancelButton" runat="server" />
                            </div>
                        </div>
                    </div>
                </EditForm>
            </Templates>

            <Templates>
                <StatusBar>
                    <table>
                        <tr>
                            <td>
                                <%--<dx:ASPxButton ID="btnNewRecord" runat="server" Text="Add New Item" AutoPostBack="false">
                                    <ClientSideEvents Click="function (s, e){ FromGridView.AddNewRow();  }" />
                                </dx:ASPxButton>--%>
                            </td>
                            <td>Search: </td>
                            <td>
                                <dx:ASPxButtonEdit runat="server" ID="CustomSearchPanelTxtBx" />
                            </td>
                            <td>&nbsp;</td>
                            <td>Search In:</td>
                            <td>
                                <dx:ASPxComboBox AutoPostBack="true" ID="DDLFilterByColumn" runat="server" OnSelectedIndexChanged="DDLFilterByColumn_SelectedIndexChanged">
                                    <Items>
                                        <dx:ListEditItem Text="All" Value="*" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>

                        </tr>
                    </table>
                </StatusBar>
            </Templates>

        </dx:ASPxGridView>
        <%--</ContentTemplate>
        </asp:UpdatePanel>--%>



        <asp:Literal ID="ShowHideDetail" runat="server"></asp:Literal>
        <div class="overlay" id="detail_overlay" runat="server" visible="false">
            <div class="FormPopup" id="detail_popup" runat="server" visible="false">
                <div class="formHeaderDiv">
                    <h3>Product Details</h3>
                </div>
                <fieldset>
                    <legend></legend>
                    <table style="display: inline-block;">
                        <tr>
                            <td>Code : </td>
                            <td>
                                <div class="DivReq">
                                    <dx:ASPxLabel ID="lblVwCode" runat="server"></dx:ASPxLabel>
                                </div>
                            </td>
                            <td>Alternative Code : </td>
                            <td>
                                <div class="DivReq">
                                    <dx:ASPxLabel ID="lblVwAltCode" runat="server"></dx:ASPxLabel>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Name: </td>
                            <td>
                                <div class="DivReq">
                                    <dx:ASPxLabel ID="lblVwName" runat="server"></dx:ASPxLabel>
                                </div>
                            </td>
                            <td>Arabic Name: </td>
                            <td>
                                <div class="DivReq">
                                    <dx:ASPxLabel ID="lblVwArName" runat="server"></dx:ASPxLabel>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Track Method :</td>
                            <td>
                                <div class="DivReq">
                                    <dx:ASPxLabel ID="lblVwTrackMethod" runat="server"></dx:ASPxLabel>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Category : </td>
                            <td>
                                <dx:ASPxLabel ID="lblVwCatg" runat="server"></dx:ASPxLabel>
                            </td>
                            <td>Sub-Category : </td>
                            <td>
                                <dx:ASPxLabel ID="lblVwSubCatg" runat="server"></dx:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>Class : </td>
                            <td>
                                <dx:ASPxLabel ID="lblVwClass" runat="server"></dx:ASPxLabel>
                            </td>
                            <td>Sub Class : </td>
                            <td>
                                <dx:ASPxLabel ID="lblVwSubClass" runat="server"></dx:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>Type : </td>
                            <td>
                                <dx:ASPxLabel ID="lblVwType" runat="server"></dx:ASPxLabel>
                            </td>
                            <td>Brand : </td>
                            <td>
                                <dx:ASPxLabel ID="lblVwBrand" runat="server"></dx:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>UOM : </td>
                            <td>
                                <dx:ASPxLabel ID="lblVwUom" runat="server"></dx:ASPxLabel>
                            </td>
                            <td>Price :</td>
                            <td>
                                <div class="DivReq">
                                    <dx:ASPxLabel ID="lblVwPrice" runat="server"></dx:ASPxLabel>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Weight : </td>
                            <td>
                                <dx:ASPxLabel ID="lblVwWeight" runat="server"></dx:ASPxLabel>
                            </td>
                            <td>Alternative UoMs? :</td>
                            <td>
                                <div class="DivReq">
                                    <dx:ASPxLabel ID="lblVwHasAltUom" runat="server"></dx:ASPxLabel>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Status : </td>
                            <td>
                                <dx:ASPxLabel ID="lblVwStatus" runat="server"></dx:ASPxLabel>
                            </td>
                            <td>Tax Code :</td>
                            <td>
                                <div class="DivReq">
                                    <dx:ASPxLabel ID="lblVwTaxCode" runat="server"></dx:ASPxLabel>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table style="display: inline-block; border: 1px solid black; border-radius: 5px; padding: 0px 5px 0px 0px; margin-left: 0px;">
                        <tr>
                            <td colspan="2"><span style="font-size: 22px; font-weight: bold;">Alternate UoMs</span></td>
                        </tr>
                        <tr>
                            <td>Alt. Unit 1 : </td>
                            <td>
                                <dx:ASPxLabel ID="lblVwAltUom1" runat="server"></dx:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>Alt Unit 1 Price :</td>
                            <td>
                                <div class="DivReq">
                                    <dx:ASPxLabel ID="lblVwAltPrice1" runat="server"></dx:ASPxLabel>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Alt Conv Factor 1 :</td>
                            <td>
                                <div class="DivReq">
                                    <dx:ASPxLabel ID="lblVwAltConv1" runat="server"></dx:ASPxLabel>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Alt. Unit 2 : </td>
                            <td>
                                <dx:ASPxLabel ID="lblVwAltUom2" runat="server"></dx:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>Alt Unit 2 Price :</td>
                            <td>
                                <div class="DivReq">
                                    <dx:ASPxLabel ID="lblVwAltPrice2" runat="server"></dx:ASPxLabel>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Alt Conv Factor 2 :</td>
                            <td>
                                <div class="DivReq">
                                    <dx:ASPxLabel ID="lblVwAltConv2" runat="server"></dx:ASPxLabel>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Alt. Unit 3 : </td>
                            <td>
                                <dx:ASPxLabel ID="lblVwAltUom3" runat="server"></dx:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td>Alt Unit 3 Price :</td>
                            <td>
                                <div class="DivReq">
                                    <dx:ASPxLabel ID="lblVwAltPrice3" runat="server"></dx:ASPxLabel>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Alt Conv Factor 3 :</td>
                            <td>
                                <div class="DivReq">
                                    <dx:ASPxLabel ID="lblVwAltConv3" runat="server"></dx:ASPxLabel>
                                </div>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <hr style="color: skyblue" />
                <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                    <dx:ASPxButton ID="btnPopUpVwClose" runat="server" Text="Close" CssClass="btnPopup btn btn-info btn-info-variant" AutoPostBack="false" CausesValidation="false">
                        <ClientSideEvents Click="function(s, e) { jQuery('#MainContent_MainContentPlaceholder_detail_overlay').hide(); jQuery('#MainContent_MainContentPlaceholder_detail_popup').hide(); }" />
                    </dx:ASPxButton>
                </div>
            </div>
        </div>


    </div>

    <script type="text/javascript">
        window.onload = function () {
            $('form').on('click', '.btnPopupClose', function () {
                //$('.overlay').hide();
            });
        };

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //if (prm != null) {
        prm.add_endRequest(function (sender, e) {
            //if (sender._postBackSettings.panelsToUpdate != null) {            
            //$('.overlay').show();
            //}
        });
        prm.add_endRequest(GridOnEndCallBack);
        //};


        function OnTestValidation(s, e) {
            //var testData = e.value;
            //if (!testData) {
            lblError.SetVisible(!e.isValid);
            //  return;
        }
    </script>
</asp:Content>

