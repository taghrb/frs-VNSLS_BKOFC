﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;

public partial class Admin_System_PriceList : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
    private string Country_SLCTD = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT * FROM SYS_STP_PRLST";
    }

    protected void categoryListingGridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvInquery_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string PrclstKey = e.Keys["PRLST_KEY"].ToString().Trim();

        dbHlpr.ExecuteNonQuery("DELETE FROM SYS_STP_PRLST WHERE PRLST_KEY = '" + PrclstKey + "'");

        e.Cancel = true;
        gvInquery.CancelEdit();
    }

    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxTextBox txtBxCode = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("EditCode"));
        string PrclstKey = "0";

        if (txtBxCode.Text.Length > 0)
        {
            PrclstKey = txtBxCode.Text.Trim();
        }

        string PrclstName = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("EditName")).Text;
        string PrclstAlias = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("EditAlias")).Text;
        string PrclstCntry = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("ddlCountry")).SelectedItem.Value.ToString();
       
        DataTable myDT = new DataTable();

        myDT = myDT = dbHlpr.FetchData("SELECT * FROM SYS_STP_PRLST WHERE PRLST_KEY = '" + PrclstKey + "'");
        if (myDT.Rows.Count <= 0)
        {

            dbHlpr.ExecuteNonQuery("INSERT INTO SYS_STP_PRLST (PRLST_KEY, PRLST_NAME, PRLST_ALIAS, PRLST_CNTRY) "
            + " VALUES ('" + PrclstKey + "', '" + PrclstName + "', '" + PrclstAlias + "', '" + PrclstCntry+ "')");
        }
        else
        {
            dbHlpr.ExecuteNonQuery("UPDATE SYS_STP_PRLST SET "
            + " PRLST_NAME = '" + PrclstName + "', "
            + " PRLST_ALIAS = '" + PrclstAlias + "', "
            + " PRLST_CNTRY = '" + PrclstCntry + "' "
            + " WHERE PRLST_KEY = '" + PrclstKey + "'");
        }

        gvInquery.CancelEdit();
        gvInquery.DataBind();
    }
    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

    }
    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            string PrclstId = e.CommandArgs.CommandArgument.ToString();
           
            if (PrclstId != "") // Code is sent via request
            {
                dbHlpr.ExecuteNonQuery("DELETE FROM SYS_STP_PRLST WHERE PRLST_KEY = '" + PrclstId + "'");

                gvInquery.CancelEdit();
                gvInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid Code. Unable to delete Price List.";
            }
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }
    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }
    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }


    protected void EditCode_Init(object sender, EventArgs e)
    {
        if (!gvInquery.IsNewRowEditing)
        {
            ASPxTextBox txtCode = (ASPxTextBox)sender;
            txtCode.ReadOnly = true;
        }
        else
        {
            ASPxTextBox txtCode1 = (ASPxTextBox)sender;
            txtCode1.ReadOnly = false;
        }     
    }
    protected void ddlCountry_Init(object sender, EventArgs e)
    {
        ASPxComboBox CBX_Country = (ASPxComboBox)sender;
        CBX_Country.DataSource = dbHlpr.FetchData("SELECT * FROM STP_MST_CNTRY");
        CBX_Country.DataBind();

        CBX_Country.Value = Country_SLCTD;
    }

    protected void gvInquery_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        Country_SLCTD = ((ASPxGridView)sender).GetRowValues(((ASPxGridView)sender).EditingRowVisibleIndex, "PRLST_CNTRY").ToString();
    }
}