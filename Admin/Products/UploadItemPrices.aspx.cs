﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using System.Data.OleDb;

public partial class Admin_Products_UploadItemPrices : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {

    }

    protected void btnDownloadTemplate_Click(object sender, EventArgs e)
    {
        ASPxGridView gvTemplateToExport = new ASPxGridView();
        gvTemplateToExport.ID = "Template_PriceList_Upload";
        gvTemplateToExport.AutoGenerateColumns = true;

        this.Controls.Add(gvTemplateToExport);

        gvTemplateToExport.DataSource = dbHlpr.FetchData(
            "SELECT " +
            " '' AS [Item No], '' AS [UOM], '0.00' AS [Price], '0' AS [Is Main?], " +
            " '0.00000' AS [Conversion Factor], '' AS [Price List Code], '0.00' AS [MinPrice], '0.00' AS [MaxPrice] " +
            " WHERE 1 = 0 "
            );
        gvTemplateToExport.DataBind();

        dbHlpr.ExportToFormattedXls1997(this, gvTemplateToExport);
        this.Controls.Remove(gvTemplateToExport);
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        lblErrorMessage.Text = "";
        lblResult.Text = "";

        if (UploadedXlsFile.HasFile)
        {
            try
            {
                string fileNameWithExt = Path.GetFileName(UploadedXlsFile.FileName);
                string fileNameWithoutExt = Path.GetFileNameWithoutExtension(UploadedXlsFile.FileName);
                string fileExtension = fileNameWithExt.Replace(fileNameWithoutExt, "");
                if (fileExtension.Equals(".xls"))
                {
                    DateTime nw = DateTime.Now;
                    string timeStamp = nw.Year + nw.Month + nw.Day + "$" + nw.Hour + nw.Minute;
                    string saveAt = Server.MapPath("~/Uploads/ProductPrices/") + timeStamp + "_" + fileNameWithExt;
                    UploadedXlsFile.SaveAs(saveAt);

                    string resultMessage = "File Uploaded Successfully";


                    OleDbConnection oleDbConn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + saveAt + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"");

                    oleDbConn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    DataSet ds = new DataSet();

                    DataTable dtSheets;
                    dtSheets = oleDbConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string sheetName = dtSheets.Rows[0]["TABLE_NAME"].ToString();

                    cmd.Connection = oleDbConn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                    oleda = new OleDbDataAdapter(cmd);
                    oleda.Fill(ds, "dsS1no");
                    DataTable dt = ds.Tables["dsS1no"];

                    DatabaseHelperClass dbhlpr = new DatabaseHelperClass();
                    int rowsInserted = 0;
                    int duplicatesFound = 0;
                    int rowsUpdated = 0;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string ItemNo = dt.Rows[i][0].ToString().Trim().Equals("") ? "*NULL" : dt.Rows[i][0].ToString().Trim();
                        string UoM = dt.Rows[i][1].ToString().Trim().Equals("") ? "*NULL" : dt.Rows[i][1].ToString().Trim();
                        string Price = dt.Rows[i][2].ToString().Trim().Equals("") ? "0" : dt.Rows[i][2].ToString().Trim();
                        string IsMain = dt.Rows[i][3].ToString().Trim().Equals("") ? "0" : dt.Rows[i][3].ToString().Trim();
                        string ConvFact = dt.Rows[i][4].ToString().Trim().Equals("") ? "0" : dt.Rows[i][4].ToString().Trim();
                        string PriceListCode = dt.Rows[i][5].ToString().Trim().Equals("") ? "*NULL" : dt.Rows[i][5].ToString().Trim();
                        string MinPrice = dt.Rows[i][6].ToString().Trim().Equals("") ? "0" : dt.Rows[i][6].ToString().Trim();
                        string MaxPrice = dt.Rows[i][7].ToString().Trim().Equals("") ? "0" : dt.Rows[i][7].ToString().Trim();

                        string TaxCode = "";
                        DataTable dtItemInfo = dbhlpr.FetchData("SELECT * "
                            + " FROM INV_MSTR_PRODT "
                            + " WHERE "
                            + " PRO_CODE = '" + ItemNo + "' ");
                        if (dtItemInfo.Rows.Count > 0)
                        {
                            TaxCode = dtItemInfo.Rows[0]["PRO_TAXCD"].ToString();
                        }

                        DataTable dtExisting = dbhlpr.FetchData("SELECT * "
                            + " FROM INV_ITM_ALUOM "
                            + " WHERE "
                            + " ALU_ITMCD = '" + ItemNo + "' "
                            + " AND ALU_UOMCD = '" + UoM + "' "
                            + " AND ALU_PRLST = '" + PriceListCode + "' ");

                        if (dtExisting.Rows.Count > 0)
                        {
                            duplicatesFound++;
                        }

                        if (dtExisting.Rows.Count == 0)
                        {
                            string qry = "INSERT INTO INV_ITM_ALUOM ( "
                                + " ALU_ITMCD, ALU_UOMCD, ALU_PRICE, ALU_MIN, ALU_MAX, ALU_MAIN, ALU_CNVFC, ALU_PRLST, ALU_TAXCD "
                                + " ) VALUES ( "
                                + " '" + ItemNo + "', " + " '" + UoM + "', '" + Price + "', "
                                + " '" + MinPrice + "', '" + MaxPrice + "', '" + IsMain + "', "
                                + " '" + ConvFact + "', '" + PriceListCode + "', '" + TaxCode + "' "
                                + " ) ";
                            dbhlpr.ExecuteNonQuery(qry);
                            rowsInserted++;
                        }
                        else if (dtExisting.Rows.Count > 0) // && chkBxUpdateDuplicates.Checked)
                        {
                            string updateQry = "UPDATE INV_ITM_ALUOM SET "
                                + " ALU_PRICE = '" + Price + "', "
                                + " ALU_MIN = '" + MinPrice + "', "
                                + " ALU_MAX = '" + MaxPrice + "', "
                                + " ALU_MAIN = '" + IsMain + "', "
                                + " ALU_CNVFC = '" + ConvFact + "', "
                                + " ALU_TAXCD = '" + TaxCode + "' "
                                + " WHERE "
                                + " ALU_ITMCD = '" + ItemNo + "' "
                                + " AND ALU_UOMCD = '" + UoM + "' "
                                + " AND ALU_PRLST = '" + PriceListCode + "' ";
                            rowsUpdated += dbhlpr.ExecuteNonQuery(updateQry);
                        }

                        if (IsMain == "1")
                        {
                            dbhlpr.ExecuteNonQuery("UPDATE INV_MSTR_PRODT SET PRO_PRICE = '" + Price + "' WHERE PRO_CODE = '" + ItemNo + "' ");
                        }
                    }

                    lblResult.Text = resultMessage;
                    lblResult.Text += "\n" + rowsInserted + " records inserted successfully.";
                    lblResult.Text += "\n" + duplicatesFound + " duplicate records found.";
                    lblResult.Text += "\n" + rowsUpdated + " records updated successfully.";
                }
                else
                {
                    lblErrorMessage.Text = "Please Select a valid .xls file";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        else
        {
            lblErrorMessage.Text = "Please Select a file to Upload.";
        }
    }

    protected void txtPriceCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["needBind"] = true;
        gvInquery.DataSource = dbHlpr.FetchData(GetPriceListData());
        gvInquery.DataBind();
    }

    private string GetPriceListData()
    {
        if (txtPriceCode.SelectedItem != null && txtPriceCode.SelectedItem.Value != null)
        {
            return "SELECT *, PRO_DESC1 FROM INV_ITM_ALUOM LEFT JOIN INV_MSTR_PRODT ON ALU_ITMCD = PRO_CODE WHERE ALU_PRLST = '" + txtPriceCode.SelectedItem.Value + "' ";
        }
        else
        {
            return "SELECT *, '' AS PRO_DESC1 FROM INV_ITM_ALUOM WHERE 1 = 0";
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }
    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }//end method

    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void gvInquery_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string PricelistCode = e.Keys["ALU_PRLST"].ToString().Trim();
        string ItemCode = e.Keys["ALU_ITMCD"].ToString().Trim();
        string UomCode = e.Keys["ALU_UOMCD"].ToString().Trim();

        dbHlpr.ExecuteNonQuery("DELETE FROM INV_ITM_ALUOM WHERE ALU_PRLST = '" + PricelistCode + "' AND ALU_ITMCD = '" + ItemCode + "' AND ALU_UOMCD = '" + UomCode + "' ");

        DataTable dt = dbHlpr.FetchData("SELECT '" + ItemCode + "' AS TEXT01, '" + UomCode + "' AS TEXT02, '" + PricelistCode + "' AS TEXT05 ");
        DataTable dtRoutes = dbHlpr.FetchData("SELECT DFG_SLSRP FROM STP_MSTR_DCONFG WHERE DFG_PRLST = '" + PricelistCode + "'");
        for (int i = 0; i < dtRoutes.Rows.Count; i++)
        {
            dbHlpr.CreateDownloadRecord(dt, dtRoutes.Rows[i]["DFG_SLSRP"].ToString(), "INV_ITM_ALUOM", "DLT");
        }

        e.Cancel = true;
        gvInquery.CancelEdit();
    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxComboBox cmbxTaxCode = (ASPxComboBox)gvInquery.FindEditFormTemplateControl("EditTax");

        ASPxLabel lblUom1 = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblUom1");
        ASPxLabel lblUom2 = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblUom2");
        ASPxLabel lblUom3 = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblUom3");
        ASPxLabel lblUom4 = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblUom4");

        ASPxTextBox txtPriceUoM1 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPriceUoM1");
        ASPxTextBox txtPriceUoM2 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPriceUoM2");
        ASPxTextBox txtPriceUoM3 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPriceUoM3");
        ASPxTextBox txtPriceUoM4 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPriceUoM4");

        ASPxTextBox txtMinPriceUoM1 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice1");
        ASPxTextBox txtMinPriceUoM2 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice2");
        ASPxTextBox txtMinPriceUoM3 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice3");
        ASPxTextBox txtMinPriceUoM4 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice4");

        ASPxTextBox txtMaxPriceUoM1 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice1");
        ASPxTextBox txtMaxPriceUoM2 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice2");
        ASPxTextBox txtMaxPriceUoM3 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice3");
        ASPxTextBox txtMaxPriceUoM4 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice4");

        string PriceListCode = txtPriceCode.SelectedItem.Value.ToString();
        string ItemNo = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("EditItem")).Value.ToString();
        string TaxCode = cmbxTaxCode.Value.ToString();

        if (lblUom1.Text.Trim().Length > 0)
        {
            string unit = lblUom1.Text.Split(' ')[2];
            string price = txtPriceUoM1.Text.Trim();
            string minPrice = txtMinPriceUoM1.Text.Trim();
            string maxPrice = txtMaxPriceUoM1.Text.Trim();
            SavePriceList(PriceListCode, ItemNo, TaxCode, unit, price, minPrice, maxPrice);
        }

        if (lblUom2.Text.Trim().Length > 0)
        {
            string unit = lblUom2.Text.Split(' ')[2];
            string price = txtPriceUoM2.Text.Trim();
            string minPrice = txtMinPriceUoM2.Text.Trim();
            string maxPrice = txtMaxPriceUoM2.Text.Trim();
            SavePriceList(PriceListCode, ItemNo, TaxCode, unit, price, minPrice, maxPrice);
        }

        if (lblUom3.Text.Trim().Length > 0)
        {
            string unit = lblUom3.Text.Split(' ')[2];
            string price = txtPriceUoM3.Text.Trim();
            string minPrice = txtMinPriceUoM3.Text.Trim();
            string maxPrice = txtMaxPriceUoM3.Text.Trim();
            SavePriceList(PriceListCode, ItemNo, TaxCode, unit, price, minPrice, maxPrice);
        }

        if (lblUom4.Text.Trim().Length > 0)
        {
            string unit = lblUom4.Text.Split(' ')[2];
            string price = txtPriceUoM4.Text.Trim();
            string minPrice = txtMinPriceUoM4.Text.Trim();
            string maxPrice = txtMaxPriceUoM4.Text.Trim();
            SavePriceList(PriceListCode, ItemNo, TaxCode, unit, price, minPrice, maxPrice);
        }


        gvInquery.CancelEdit();
        gvInquery.DataBind();
    }

    void SavePriceList(string PriceListCode, string ItemNo, string TaxCode, string UoM, string Price, string MinPrice, string MaxPrice)
    {
        DataTable dtblUoms = dbHlpr.FetchData("SELECT ITU_ITMCD, ITU_UOMCD, ITU_CNVFC, ITU_PRICE, ITU_MAIN FROM INV_ITM_UOMS WHERE ITU_ITMCD = '" + ItemNo + "' AND ITU_UOMCD = '" + UoM + "' ");
        string IsMain = dtblUoms.Rows[0]["ITU_MAIN"].ToString();
        string ConvFact = dtblUoms.Rows[0]["ITU_CNVFC"].ToString();

        DataTable dtRoutes = dbHlpr.FetchData("SELECT DFG_SLSRP FROM STP_MSTR_DCONFG WHERE DFG_PRLST = '" + PriceListCode + "' ");

        DataTable myDT = new DataTable();

        myDT = myDT = dbHlpr.FetchData("SELECT * FROM INV_ITM_ALUOM WHERE ALU_PRLST = '" + PriceListCode + "' AND ALU_ITMCD = '" + ItemNo + "' AND ALU_UOMCD = '" + UoM + "' ");
        if (myDT.Rows.Count <= 0)
        {
            dbHlpr.ExecuteNonQuery("INSERT INTO INV_ITM_ALUOM (ALU_ITMCD, ALU_UOMCD, ALU_PRICE, ALU_MIN, ALU_MAX, ALU_MAIN, ALU_CNVFC, ALU_PRLST, ALU_TAXCD "
                                + " ) VALUES ( "
                                + " '" + ItemNo + "', " + " '" + UoM + "', '" + Price + "', "
                                + " '" + MinPrice + "', '" + MaxPrice + "', '" + IsMain + "', "
                                + " '" + ConvFact + "', '" + PriceListCode + "', '" + TaxCode + "' "
                                + " ) ");

            DataTable dt1 = dbHlpr.FetchData("SELECT "
                + " ALU_ITMCD AS TEXT01, ALU_UOMCD AS TEXT02, ALU_PRICE AS NUM01, ALU_MAIN AS TEXT03, "
                + " ALU_CNVFC AS NUM02, ALU_TAXCD AS TEXT04, ALU_PRLST AS TEXT05, ALU_MIN AS NUM03, "
                + " ALU_MAX AS NUM04 "
                + " FROM INV_ITM_ALUOM "
                + " WHERE ALU_PRLST = '" + PriceListCode + "' AND ALU_ITMCD = '" + ItemNo + "' AND ALU_UOMCD = '" + UoM + "' ");
            for (int i = 0; i < dtRoutes.Rows.Count; i++)
            {
                dbHlpr.CreateDownloadRecord(dt1, dtRoutes.Rows[i]["DFG_SLSRP"].ToString(), "INV_ITM_ALUOM", "CRT");
            }
        }
        else
        {
            dbHlpr.ExecuteNonQuery("UPDATE INV_ITM_ALUOM SET "
                                + " ALU_PRICE = '" + Price + "', "
                                + " ALU_MIN = '" + MinPrice + "', "
                                + " ALU_MAX = '" + MaxPrice + "', "
                                + " ALU_MAIN = '" + IsMain + "', "
                                + " ALU_CNVFC = '" + ConvFact + "', "
                                + " ALU_TAXCD = '" + TaxCode + "' "
                                + " WHERE "
                                + " ALU_ITMCD = '" + ItemNo + "' "
                                + " AND ALU_UOMCD = '" + UoM + "' "
                                + " AND ALU_PRLST = '" + PriceListCode + "' ");

            DataTable dt1 = dbHlpr.FetchData("SELECT "
                + " ALU_ITMCD AS TEXT01, ALU_UOMCD AS TEXT02, ALU_PRICE AS NUM01, ALU_MAIN AS TEXT03, "
                + " ALU_CNVFC AS NUM02, ALU_TAXCD AS TEXT04, ALU_PRLST AS TEXT05, ALU_MIN AS NUM03, "
                + " ALU_MAX AS NUM04 "
                + " FROM INV_ITM_ALUOM "
                + " WHERE ALU_PRLST = '" + PriceListCode + "' AND ALU_ITMCD = '" + ItemNo + "' AND ALU_UOMCD = '" + UoM + "' ");
            for (int i = 0; i < dtRoutes.Rows.Count; i++)
            {
                dbHlpr.CreateDownloadRecord(dt1, dtRoutes.Rows[i]["DFG_SLSRP"].ToString(), "INV_ITM_ALUOM", "UPD");
            }
        }
    }

    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            string PriceListItemCodeUom = e.CommandArgs.CommandArgument.ToString();
            string[] args = PriceListItemCodeUom.Split('|');
            if (args.Length == 3) // Code is sent via request
            {
                dbHlpr.ExecuteNonQuery("DELETE FROM INV_ITM_ALUOM WHERE "
                                + " ALU_ITMCD = '" + args[1] + "' "
                                + " AND ALU_UOMCD = '" + args[2] + "' "
                                + " AND ALU_PRLST = '" + args[0] + "' ");

                DataTable dt = dbHlpr.FetchData("SELECT '" + args[1] + "' AS TEXT01, '" + args[2] + "' AS TEXT02, '" + args[0] + "' AS TEXT05 ");
                DataTable dtRoutes = dbHlpr.FetchData("SELECT DFG_SLSRP FROM STP_MSTR_DCONFG WHERE DFG_PRLST = '" + args[0] + "' ");
                for (int i = 0; i < dtRoutes.Rows.Count; i++)
                {
                    dbHlpr.CreateDownloadRecord(dt, dtRoutes.Rows[i]["DFG_SLSRP"].ToString(), "INV_ITM_ALUOM", "DLT");
                }

                gvInquery.CancelEdit();
                gvInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid Country Code. Unable to delete Country.";
            }
        }
    }
    protected void gvInquery_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
    }
    protected void gvInquery_DataBinding(object sender, EventArgs e)
    {
        if (ViewState["needBind"] != null && (bool)ViewState["needBind"])
        {
            gvInquery.DataSource = dbHlpr.FetchData(GetPriceListData());
        }
    }
    protected void EditItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (((ASPxComboBox)gvInquery.FindEditFormTemplateControl("EditItem")).SelectedItem == null)
        {
            return;
        }

        string SelectedItemCode = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("EditItem")).SelectedItem.Value.ToString();

        ASPxComboBox cmbxTaxCode = (ASPxComboBox)gvInquery.FindEditFormTemplateControl("EditTax");

        ASPxLabel lblUom1 = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblUom1");
        ASPxLabel lblUom2 = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblUom2");
        ASPxLabel lblUom3 = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblUom3");
        ASPxLabel lblUom4 = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblUom4");

        ASPxTextBox txtPriceUoM1 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPriceUoM1");
        ASPxTextBox txtPriceUoM2 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPriceUoM2");
        ASPxTextBox txtPriceUoM3 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPriceUoM3");
        ASPxTextBox txtPriceUoM4 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPriceUoM4");

        ASPxLabel lblMinPrice1 = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblMinPrice1");
        ASPxLabel lblMinPrice2 = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblMinPrice2");
        ASPxLabel lblMinPrice3 = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblMinPrice3");
        ASPxLabel lblMinPrice4 = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblMinPrice4");

        ASPxTextBox txtMinPriceUoM1 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice1");
        ASPxTextBox txtMinPriceUoM2 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice2");
        ASPxTextBox txtMinPriceUoM3 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice3");
        ASPxTextBox txtMinPriceUoM4 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMinPrice4");

        ASPxLabel lblMaxPrice1 = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblMaxPrice1");
        ASPxLabel lblMaxPrice2 = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblMaxPrice2");
        ASPxLabel lblMaxPrice3 = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblMaxPrice3");
        ASPxLabel lblMaxPrice4 = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblMaxPrice4");

        ASPxTextBox txtMaxPriceUoM1 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice1");
        ASPxTextBox txtMaxPriceUoM2 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice2");
        ASPxTextBox txtMaxPriceUoM3 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice3");
        ASPxTextBox txtMaxPriceUoM4 = (ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMaxPrice4");

        cmbxTaxCode.Value = null;

        lblUom1.Text = "";
        lblUom2.Text = "";
        lblUom3.Text = "";
        lblUom4.Text = "";
        lblUom1.ClientVisible = false;
        lblUom2.ClientVisible = false;
        lblUom3.ClientVisible = false;
        lblUom4.ClientVisible = false;

        txtPriceUoM1.Text = "";
        txtPriceUoM2.Text = "";
        txtPriceUoM3.Text = "";
        txtPriceUoM4.Text = "";
        txtPriceUoM1.ClientVisible = false;
        txtPriceUoM2.ClientVisible = false;
        txtPriceUoM3.ClientVisible = false;
        txtPriceUoM4.ClientVisible = false;

        lblMinPrice1.Text = "";
        lblMinPrice2.Text = "";
        lblMinPrice3.Text = "";
        lblMinPrice4.Text = "";
        lblMinPrice1.ClientVisible = false;
        lblMinPrice2.ClientVisible = false;
        lblMinPrice3.ClientVisible = false;
        lblMinPrice4.ClientVisible = false;

        txtMinPriceUoM1.Text = "";
        txtMinPriceUoM2.Text = "";
        txtMinPriceUoM3.Text = "";
        txtMinPriceUoM4.Text = "";
        txtMinPriceUoM1.ClientVisible = false;
        txtMinPriceUoM2.ClientVisible = false;
        txtMinPriceUoM3.ClientVisible = false;
        txtMinPriceUoM4.ClientVisible = false;

        lblMaxPrice1.Text = "";
        lblMaxPrice2.Text = "";
        lblMaxPrice3.Text = "";
        lblMaxPrice4.Text = "";
        lblMaxPrice1.ClientVisible = false;
        lblMaxPrice2.ClientVisible = false;
        lblMaxPrice3.ClientVisible = false;
        lblMaxPrice4.ClientVisible = false;

        txtMaxPriceUoM1.Text = "";
        txtMaxPriceUoM2.Text = "";
        txtMaxPriceUoM3.Text = "";
        txtMaxPriceUoM4.Text = "";
        txtMaxPriceUoM1.ClientVisible = false;
        txtMaxPriceUoM2.ClientVisible = false;
        txtMaxPriceUoM3.ClientVisible = false;
        txtMaxPriceUoM4.ClientVisible = false;

        DataTable dtUoms = new DataTable();
        if (txtPriceCode.Value != null)
        {
            dtUoms = dbHlpr.FetchData("SELECT "
                + " ITU_ITMCD, ITU_UOMCD, ITU_CNVFC, ITU_PRICE, ITU_MAIN, ALU_PRICE, ALU_MIN, ALU_MAX, ALU_TAXCD "
                + " FROM INV_ITM_UOMS "
                + " LEFT JOIN INV_ITM_ALUOM ON ITU_ITMCD = ALU_ITMCD AND ITU_UOMCD = ALU_UOMCD AND ALU_PRLST = '" + txtPriceCode.Value.ToString() + "' "
                + " WHERE ITU_ITMCD = '" + SelectedItemCode + "' "
                + " ORDER BY ITU_MAIN DESC ");
        }
        else
        {
            dtUoms = dbHlpr.FetchData("SELECT "
                + " ITU_ITMCD, ITU_UOMCD, ITU_CNVFC, ITU_PRICE, ITU_MAIN, ALU_PRICE, ALU_MIN, ALU_MAX, ALU_TAXCD "
                + " FROM INV_ITM_UOMS "
                + " LEFT JOIN INV_ITM_ALUOM ON ITU_ITMCD = ALU_ITMCD AND ITU_UOMCD = ALU_UOMCD AND 1 = 0 "
                + " WHERE ITU_ITMCD = '" + SelectedItemCode + "' "
                + " ORDER BY ITU_MAIN DESC ");
        }

        for (int i = 0; i < dtUoms.Rows.Count; i++)
        {
            string TaxCode = dtUoms.Rows[i]["ALU_TAXCD"].ToString();
            string TextForPriceTextBox = dtUoms.Rows[i]["ALU_PRICE"].ToString();
            string TextForMinPriceTextBox = dtUoms.Rows[i]["ALU_MIN"].ToString();
            string TextForMaxPriceTextBox = dtUoms.Rows[i]["ALU_MAX"].ToString();

            string TextForLabel = "Price per " + dtUoms.Rows[i]["ITU_UOMCD"].ToString();
            if (dtUoms.Rows[i]["ITU_MAIN"].ToString() == "1")
            {
                TextForLabel += " *";
            }

            if (TaxCode.Trim().Length > 0 && cmbxTaxCode.Value == null)
            {
                cmbxTaxCode.Value = TaxCode;
            }

            switch (i)
            {
                case 0:
                    lblUom1.Text = TextForLabel;
                    lblUom1.ClientVisible = true;

                    txtPriceUoM1.Text = TextForPriceTextBox;
                    txtPriceUoM1.ClientVisible = true;
                    txtMinPriceUoM1.Text = TextForMinPriceTextBox;
                    txtMinPriceUoM1.ClientVisible = true;
                    txtMaxPriceUoM1.Text = TextForMaxPriceTextBox;
                    txtMaxPriceUoM1.ClientVisible = true;
                    break;
                case 1:
                    lblUom2.Text = TextForLabel;
                    lblUom2.ClientVisible = true;

                    txtPriceUoM2.Text = TextForPriceTextBox;
                    txtPriceUoM2.ClientVisible = true;
                    txtMinPriceUoM2.Text = TextForMinPriceTextBox;
                    txtMinPriceUoM2.ClientVisible = true;
                    txtMaxPriceUoM2.Text = TextForMaxPriceTextBox;
                    txtMaxPriceUoM2.ClientVisible = true;
                    break;
                case 2:
                    lblUom3.Text = TextForLabel;
                    lblUom3.ClientVisible = true;

                    txtPriceUoM3.Text = TextForPriceTextBox;
                    txtPriceUoM3.ClientVisible = true;
                    txtMinPriceUoM3.Text = TextForMinPriceTextBox;
                    txtMinPriceUoM3.ClientVisible = true;
                    txtMaxPriceUoM3.Text = TextForMaxPriceTextBox;
                    txtMaxPriceUoM3.ClientVisible = true;
                    break;
                case 3:
                    lblUom4.Text = TextForLabel;
                    lblUom4.ClientVisible = true;

                    txtPriceUoM4.Text = TextForPriceTextBox;
                    txtPriceUoM4.ClientVisible = true;
                    txtMinPriceUoM4.Text = TextForMinPriceTextBox;
                    txtMinPriceUoM4.ClientVisible = true;
                    txtMaxPriceUoM4.Text = TextForMaxPriceTextBox;
                    txtMaxPriceUoM4.ClientVisible = true;
                    break;
                default:
                    break;
            }
        }
    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

    }
}