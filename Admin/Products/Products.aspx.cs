﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Products_DxDefault : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    //public List<string> CommentIds = new List<string>();
    //public List<string> DefaultCommentIds = new List<string>();

    private string EditProdCategory_SLCTD = string.Empty;
    private string EditProdSubCategory_SLCTD = string.Empty;
    private string EditProdUdf1_SLCTD = string.Empty;
    private string EditProdUdf2_SLCTD = string.Empty;
    private string EditProdUdf3_SLCTD = string.Empty;
    private string EditProdUdf4_SLCTD = string.Empty;
    //   private string EditProdXtras_SLCTD = string.Empty;
    private string EditProdHasAltUOM_SLCTD = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString.HasKeys() && Request.QueryString["ItemCode"] != null)
        {
            DataTable dtItems = dbHlpr.FetchData("SELECT "
                + " PRO_CODE, PRO_DESC1, PRO_DESC2, PRO_TRKMD, PRO_CATG, PRO_SCATG, "
                + " PRO_UDF1, PRO_UDF2, PRO_UDF3, PRO_UDF4, PRO_WT, "
                + " PRO_UOM, PRO_PRICE, PRO_ALCOD, PRO_STATS, PRO_TAXCD, "
                + " '' AS ALT_UOM1, 0.00 AS ALT_PRC1, 0.00 AS ALT_CNVFC1, "
                + " '' AS ALT_UOM2, 0.00 AS ALT_PRC2, 0.00 AS ALT_CNVFC2, "
                + " '' AS ALT_UOM3, 0.00 AS ALT_PRC3, 0.00 AS ALT_CNVFC3 "
                + " FROM INV_MSTR_PRODT "
                + " WHERE PRO_CODE = '" + Request.QueryString["ItemCode"].Trim() + "' ");


            DataTable dtAltUoMs = dbHlpr.FetchData("SELECT "
                + " ITU_ITMCD, ITU_UOMCD, ITU_PRICE, ITU_MAIN, ITU_CNVFC "
                + " FROM INV_ITM_UOMS "
                + " WHERE ITU_ITMCD = '" + dtItems.Rows[0]["PRO_CODE"].ToString() + "' "
                + " AND ITU_MAIN <> 1 ");
            for (int j = 0; j < dtAltUoMs.Rows.Count; j++)
            {
                string UoMCol = "ALT_UOM" + (j + 1);
                string PrcCol = "ALT_PRC" + (j + 1);
                string CnvfcCol = "ALT_CNVFC" + (j + 1);
                dtItems.Rows[0][UoMCol] = dtAltUoMs.Rows[j]["ITU_UOMCD"].ToString();
                dtItems.Rows[0][PrcCol] = dtAltUoMs.Rows[j]["ITU_PRICE"].ToString();
                dtItems.Rows[0][CnvfcCol] = dtAltUoMs.Rows[j]["ITU_CNVFC"].ToString().Trim().Length > 0 ? dtAltUoMs.Rows[j]["ITU_CNVFC"].ToString() : "0";
            }

            if (dtItems.Rows.Count > 0)
            {
                detail_overlay.Visible = true;
                detail_popup.Visible = true;

                DataRow dr = dtItems.Rows[0];

                lblVwCode.Text = dr["PRO_CODE"].ToString();
                lblVwAltCode.Text = dr["PRO_ALCOD"].ToString();
                lblVwName.Text = dr["PRO_DESC1"].ToString();
                lblVwArName.Text = dr["PRO_DESC2"].ToString();
                if (dr["PRO_TRKMD"].ToString() == "D")
                {
                    lblVwTrackMethod.Text = "LOT Detail";
                }
                else if (dr["PRO_TRKMD"].ToString() == "N")
                {
                    lblVwTrackMethod.Text = "Normal";
                }
                else
                {
                    lblVwTrackMethod.Text = dr["PRO_TRKMD"].ToString();
                }
                lblVwCatg.Text = dr["PRO_CATG"].ToString();
                lblVwSubCatg.Text = dr["PRO_SCATG"].ToString();
                lblVwClass.Text = dr["PRO_UDF1"].ToString();
                lblVwSubClass.Text = dr["PRO_UDF2"].ToString();
                lblVwType.Text = dr["PRO_UDF3"].ToString();
                lblVwBrand.Text = dr["PRO_UDF4"].ToString();
                lblVwUom.Text = dr["PRO_UOM"].ToString();
                lblVwPrice.Text = dr["PRO_PRICE"].ToString();
                lblVwWeight.Text = dr["PRO_WT"].ToString();
                lblVwHasAltUom.Text = dtAltUoMs.Rows.Count > 0 ? "Yes" : "No";
                lblVwStatus.Text = dr["PRO_STATS"].ToString();
                lblVwTaxCode.Text = dr["PRO_TAXCD"].ToString();
                lblVwAltUom1.Text = dr["ALT_UOM1"].ToString();
                lblVwAltPrice1.Text = dr["ALT_PRC1"].ToString();
                lblVwAltConv1.Text = dr["ALT_CNVFC1"].ToString();
                lblVwAltUom2.Text = dr["ALT_UOM2"].ToString();
                lblVwAltPrice2.Text = dr["ALT_PRC2"].ToString();
                lblVwAltConv2.Text = dr["ALT_CNVFC2"].ToString();
                lblVwAltUom3.Text = dr["ALT_UOM3"].ToString();
                lblVwAltPrice3.Text = dr["ALT_PRC3"].ToString();
                lblVwAltConv3.Text = dr["ALT_CNVFC3"].ToString();
            }
            else
            {
                Response.Write("<script>alert('Item not found.'); window.location.href = '" + Page.ResolveUrl("~/Admin/Products/Products.aspx") + "';</script>");
            }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        DataTable dtProds = dbHlpr.FetchData("SELECT "
                + " PRO_CODE, PRO_DESC1, PRO_DESC2, PRO_TRKMD, PRO_CATG, PRO_SCATG, "
                + " PRO_UDF1, PRO_UDF2, PRO_UDF3, PRO_UDF4, PRO_WT, "
                + " PRO_UOM, PRO_PRICE, PRO_ALCOD, PRO_STATS, PRO_TAXCD, "
                + " '' AS ALT_UOM1, 0.00 AS ALT_PRC1, 0.00 AS ALT_CNVFC1, "
                + " '' AS ALT_UOM2, 0.00 AS ALT_PRC2, 0.00 AS ALT_CNVFC2, "
                + " '' AS ALT_UOM3, 0.00 AS ALT_PRC3, 0.00 AS ALT_CNVFC3 "
                + " FROM INV_MSTR_PRODT ");

        for (int i = 0; i < dtProds.Rows.Count; i++)
        {
            DataTable dtAltUoMs = dbHlpr.FetchData("SELECT "
                + " ITU_ITMCD, ITU_UOMCD, ITU_PRICE, ITU_MAIN, ITU_CNVFC "
                + " FROM INV_ITM_UOMS "
                + " WHERE ITU_ITMCD = '" + dtProds.Rows[i]["PRO_CODE"].ToString() + "' "
                + " AND ITU_MAIN <> 1 ");
            for (int j = 0; j < dtAltUoMs.Rows.Count; j++)
            {
                string UoMCol = "ALT_UOM" + (j + 1);
                string PrcCol = "ALT_PRC" + (j + 1);
                string CnvfcCol = "ALT_CNVFC" + (j + 1);
                dtProds.Rows[i][UoMCol] = dtAltUoMs.Rows[j]["ITU_UOMCD"].ToString();
                dtProds.Rows[i][PrcCol] = dtAltUoMs.Rows[j]["ITU_PRICE"].ToString();
                dtProds.Rows[i][CnvfcCol] = dtAltUoMs.Rows[j]["ITU_CNVFC"].ToString().Trim().Length > 0 ? dtAltUoMs.Rows[j]["ITU_CNVFC"].ToString() : "0";
            }
        }

        gvProductsInquery.DataSource = dtProds;
        gvProductsInquery.DataBind();

        if (!Convert.ToBoolean(Session["userEditMastersAccess"]))
        {
            gvProductsInquery.Columns[0].Visible = false;
            gvProductsInquery.Columns[1].Visible = false;
            gvProductsInquery.Columns[2].Visible = false;
        }
    }

    protected void gvProductsInquery_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        // Set selected category
        ASPxGridView gv = (ASPxGridView)sender;
        EditProdCategory_SLCTD = gv.GetRowValues(gv.EditingRowVisibleIndex, "PRO_CATG").ToString();
        EditProdSubCategory_SLCTD = gv.GetRowValues(gv.EditingRowVisibleIndex, "PRO_SCATG").ToString();
        EditProdUdf1_SLCTD = gv.GetRowValues(gv.EditingRowVisibleIndex, "PRO_UDF1").ToString();
        EditProdUdf2_SLCTD = gv.GetRowValues(gv.EditingRowVisibleIndex, "PRO_UDF2").ToString();
        EditProdUdf3_SLCTD = gv.GetRowValues(gv.EditingRowVisibleIndex, "PRO_UDF3").ToString();
        EditProdUdf4_SLCTD = gv.GetRowValues(gv.EditingRowVisibleIndex, "PRO_UDF4").ToString();

        string prodCode = gv.GetRowValues(gv.EditingRowVisibleIndex, "PRO_CODE").ToString().Trim();
        DataTable altUoMs = dbHlpr.FetchData("SELECT [ITU_ITMCD], [ITU_UOMCD], [ITU_PRICE], ITU_MAIN FROM [INV_ITM_UOMS] WHERE ITU_MAIN <> 1 AND ITU_ITMCD = '" + prodCode + "' ");

        if (altUoMs.Rows.Count > 0)
        {
            EditProdHasAltUOM_SLCTD = "Y";
        }
        else
        {
            EditProdHasAltUOM_SLCTD = "N";
        }
    }

    protected void gvProductsInquery_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvProductsInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string prdCode = e.Keys["PRO_CODE"].ToString().Trim();
        dbHlpr.ExecuteNonQuery("DELETE FROM INV_ITM_UOMS WHERE ITU_ITMCD = '" + prdCode + "' ");
        dbHlpr.ExecuteNonQuery("DELETE FROM INV_MSTR_PRODT WHERE PRO_CODE = '" + prdCode + "'");

        DataTable dt = dbHlpr.FetchData("SELECT '" + prdCode + "' AS TEXT01 ");
        dbHlpr.CreateDownloadRecord(dt, "ALL", "INV_MSTR_PRODT", "DLT");

        e.Cancel = true;
        gvProductsInquery.CancelEdit();
    }

    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxTextBox txtProdCode = ((ASPxTextBox)gvProductsInquery.FindEditFormTemplateControl("txtCode"));
        string productCode = "0";
        if (txtProdCode.Text.Length > 0)
        {
            productCode = txtProdCode.Text.Trim();
        }


        DataTable myDt = dbHlpr.FetchData("SELECT * FROM INV_MSTR_PRODT WHERE PRO_CODE = '" + productCode + "'");

        // Create Product object, with or without code
        string txtProdAltCode = ((ASPxTextBox)gvProductsInquery.FindEditFormTemplateControl("ProdAltCode")).Text;
        string txtDesc1 = ((ASPxTextBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditName")).Text;
        string txtDesc2 = ((ASPxTextBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditArbName")).Text;
        double txtPrice = double.Parse(((ASPxTextBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditPrice")).Text);

        string prodBTrxMethod = ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditTrackMethod")).Value == null ? "" : ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditTrackMethod")).Value.ToString().Trim();

        string prodBCategoryCode = ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditCategory")).Value == null ? "" : ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditCategory")).Value.ToString().Trim();
        string prodBSubCategoryCode = ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditSubCategory")).Value == null ? "" : ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditSubCategory")).Value.ToString().Trim();
        string prodBUdf1Code = ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditUDF1")).Value == null ? "" : ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditUDF1")).Value.ToString().Trim();
        string prodBUdf2Code = ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditUDF2")).Value == null ? "" : ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditUDF2")).Value.ToString().Trim();
        string prodBUdf3Code = ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditUDF3")).Value == null ? "" : ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditUDF3")).Value.ToString().Trim();
        string prodBUdf4Code = ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditUDF4")).Value == null ? "" : ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditUDF4")).Value.ToString().Trim();


        string prodBUom = ((ASPxGridLookup)gvProductsInquery.FindEditFormTemplateControl("ProdEditUOM")).Value == null ? "" : ((ASPxGridLookup)gvProductsInquery.FindEditFormTemplateControl("ProdEditUOM")).Value.ToString().Trim();

        double txtWeight = double.Parse(((ASPxTextBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditWeight")).Text);

        string prodBTaxCode = ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditTaxCode")).Value == null ? "" : ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditTaxCode")).Value.ToString().Trim();
        string prodBStatus = ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditStatus")).Value == null ? "" : ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditStatus")).Value.ToString().Trim();

        if (myDt.Rows.Count == 0)
        {
            string insertQry = "INSERT INTO [INV_MSTR_PRODT] ( "
                                                    + " PRO_CODE, PRO_DESC1, PRO_DESC2, PRO_TRKMD, PRO_UOM, PRO_CATG,  PRO_SCATG, "
                                                    + " PRO_UDF1, PRO_UDF2, PRO_UDF3, PRO_UDF4, PRO_WT, PRO_PRICE, PRO_STATS, PRO_ALCOD, PRO_TAXCD ) "
                //+ " OUTPUT INSERTED.PRO_CODE "
                                                    + " VALUES ( "
                                                    + " '" + productCode + "', '" + txtDesc1 + "', N'" + txtDesc2 + "', '" + prodBTrxMethod + "', "
                                                    + " '" + prodBUom + "', '" + prodBCategoryCode + "', '" + prodBSubCategoryCode + "', "
                                                    + " '" + prodBUdf1Code + "', '" + prodBUdf2Code + "', '" + prodBUdf3Code + "', '" + prodBUdf4Code + "',"
                                                    + " '" + txtWeight + "', '" + txtPrice + "', '" + prodBStatus + "', '" + txtProdAltCode + "', '" + prodBTaxCode + "')";
            //productCode = dbHlpr.ExecuteScalarQuery(insertQry).ToString();
            dbHlpr.ExecuteNonQuery(insertQry);

            UpdateAlternateUoMs(productCode, prodBUom, txtPrice.ToString());

            //////////////////////////////////////// User Log Code ///////////////////////////////////////////
            //string Comments = "User id \"" + SessionDetails.UserId + "\" inserted " + productCode + " into " + this.Name + " Page at " + System.DateTime.Now.ToString();
            //dbHlpr.InsertUserLog(SessionDetails.UserId, SessionDetails.UserName, DateTime.Now, Comments, insertQry);
            /////////////////////////////////////////////////////////////////////////////////////////////////
        }
        else
        {
            string updateQry = "UPDATE [INV_MSTR_PRODT] SET "
                                + " PRO_DESC1  = '" + txtDesc1 + "', "
                                + " PRO_DESC2  = N'" + txtDesc2 + "', "
                                + " PRO_TRKMD  = '" + prodBTrxMethod + "', "
                                + " PRO_UOM  = '" + prodBUom + "', "
                                + " PRO_CATG   = '" + prodBCategoryCode + "', "
                                + " PRO_SCATG  = '" + prodBSubCategoryCode + "', "
                                + " PRO_UDF1   = '" + prodBUdf1Code + "', "
                                + " PRO_UDF2   = '" + prodBUdf2Code + "', "
                                + " PRO_UDF3   = '" + prodBUdf3Code + "', "
                                + " PRO_UDF4   = '" + prodBUdf4Code + "', "
                                + " PRO_WT  = '" + txtWeight + "', "
                                + " PRO_PRICE = '" + txtPrice + "', "
                                + " PRO_ALCOD  = '" + txtProdAltCode + "', "
                                + " PRO_STATS   = '" + prodBStatus + "', "
                                + " PRO_TAXCD = '" + prodBTaxCode + "' "
                                + " WHERE PRO_CODE = '" + productCode + "'";
            dbHlpr.ExecuteNonQuery(updateQry);

            UpdateAlternateUoMs(productCode, prodBUom, txtPrice.ToString());

            DataTable dtVansToSync = dbHlpr.FetchData("SELECT "
                + " DISTINCT DFG_SLSRP FROM STP_MSTR_DCONFG WHERE DFG_PRLST IN ( "
                + " SELECT DISTINCT ALU_PRLST FROM INV_ITM_ALUOM WHERE ALU_ITMCD = '" + productCode + "' "
                + ") ORDER BY DFG_SLSRP ");
            DataTable dt = dbHlpr.FetchData("SELECT "
                + " PRO_CODE AS TEXT01, PRO_DESC1 AS TEXT02, PRO_DESC2 AS TEXT03, PRO_TRKMD AS TEXT04, "
                + " PRO_CATG AS TEXT05, PRO_SCATG AS TEXT06, PRO_UDF1 AS TEXT07, PRO_UDF2 AS TEXT08, "
                + " PRO_UDF3 AS TEXT09, PRO_UDF4 AS TEXT10, PRO_UOM AS TEXT11, PRO_ALCOD AS TEXT12, " 
                + " PRO_STATS AS TEXT13, PRO_TAXCD AS TEXT14, PRO_WT AS NUM01, PRO_PRICE AS NUM02 "
                + " FROM INV_MSTR_PRODT "
                + " WHERE PRO_CODE = '" + productCode + "'");
            foreach (DataRow drw in dtVansToSync.Rows)
            {
                dbHlpr.CreateDownloadRecord(dt, drw["DFG_SLSRP"].ToString(), "INV_MSTR_PRODT", "UPD");
            }

            //////////////////////////////////////// User Log Code ///////////////////////////////////////////
            //string Comments = "User id \"" + SessionDetails.UserId + "\" updated " + productCode + " into " + this.Name + " Page at " + System.DateTime.Now.ToString();
            //dbHlpr.InsertUserLog(SessionDetails.UserId, SessionDetails.UserName, DateTime.Now, Comments, updateQry);
            /////////////////////////////////////////////////////////////////////////////////////////////////
        }

        gvProductsInquery.CancelEdit();
        gvProductsInquery.DataBind();
    }

    private void UpdateAlternateUoMs(string productCode, string prodBUom, string price)
    {
        dbHlpr.ExecuteNonQuery("DELETE FROM INV_ITM_UOMS WHERE ITU_ITMCD = '" + productCode + "' ");

        // Insert Main UoM
        string insertMainUoMQry = "INSERT INTO [INV_ITM_UOMS] ( "
                                                        + " ITU_ITMCD, ITU_UOMCD, ITU_PRICE, ITU_MAIN, ITU_CNVFC "
                                                        + " ) VALUES ( "
                                                        + "'" + productCode + "', '" + prodBUom + "', '" + price + "', '1', '1' "
                                                        + " )";
        dbHlpr.ExecuteNonQuery(insertMainUoMQry);

        string cmBxAltUnit = ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditHasAltUOM")).Value == null ? "" : ((ASPxComboBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditHasAltUOM")).Value.ToString().Trim();
        // Aletrnate UoM
        if (cmBxAltUnit != null && cmBxAltUnit.Equals("Y"))
        {
            string lkUpEdtAltUom1 = ((ASPxGridLookup)gvProductsInquery.FindEditFormTemplateControl("ProdEditAltUOM1")).Value == null ? "" : ((ASPxGridLookup)gvProductsInquery.FindEditFormTemplateControl("ProdEditAltUOM1")).Value.ToString().Trim();
            if (lkUpEdtAltUom1 != null && !lkUpEdtAltUom1.Trim().Equals(""))
            {
                string txtAltUomPrice1 = ((ASPxTextBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditAltUOMPrc1")).Text;
                string txtAltUomCnvFc1 = ((ASPxTextBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditAltUOMCnvFc1")).Text;
                string insertAltUoMQry = "INSERT INTO [INV_ITM_UOMS] ( "
                                                        + " ITU_ITMCD, ITU_UOMCD, ITU_PRICE, ITU_MAIN, ITU_CNVFC "
                                                        + " ) VALUES ( "
                                                        + "'" + productCode + "', '" + lkUpEdtAltUom1 + "', '" + txtAltUomPrice1 + "', '0', '" + txtAltUomCnvFc1 + "' "
                                                        + " )";
                dbHlpr.ExecuteNonQuery(insertAltUoMQry);
            }

            string lkUpEdtAltUom2 = ((ASPxGridLookup)gvProductsInquery.FindEditFormTemplateControl("ProdEditAltUOM2")).Value == null ? "" : ((ASPxGridLookup)gvProductsInquery.FindEditFormTemplateControl("ProdEditAltUOM2")).Value.ToString().Trim();
            if (lkUpEdtAltUom2 != null && !lkUpEdtAltUom2.Trim().Equals(""))
            {
                string txtAltUomPrice2 = ((ASPxTextBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditAltUOMPrc2")).Text;
                string txtAltUomCnvFc2 = ((ASPxTextBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditAltUOMCnvFc2")).Text;
                string insertAltUoMQry = "INSERT INTO [INV_ITM_UOMS] ( "
                                                        + " ITU_ITMCD, ITU_UOMCD, ITU_PRICE, ITU_MAIN, ITU_CNVFC "
                                                        + " ) VALUES ( "
                                                        + "'" + productCode + "', '" + lkUpEdtAltUom2 + "', '" + txtAltUomPrice2 + "', '0', '" + txtAltUomCnvFc2 + "' "
                                                        + " )";
                dbHlpr.ExecuteNonQuery(insertAltUoMQry);
            }

            string lkUpEdtAltUom3 = ((ASPxGridLookup)gvProductsInquery.FindEditFormTemplateControl("ProdEditAltUOM3")).Value == null ? "" : ((ASPxGridLookup)gvProductsInquery.FindEditFormTemplateControl("ProdEditAltUOM3")).Value.ToString().Trim();
            if (lkUpEdtAltUom3 != null && !lkUpEdtAltUom3.Trim().Equals(""))
            {
                string txtAltUomPrice3 = ((ASPxTextBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditAltUOMPrc3")).Text;
                string txtAltUomCnvFc3 = ((ASPxTextBox)gvProductsInquery.FindEditFormTemplateControl("ProdEditAltUOMCnvFc3")).Text;
                string insertAltUoMQry = "INSERT INTO [INV_ITM_UOMS] ( "
                                                        + " ITU_ITMCD, ITU_UOMCD, ITU_PRICE, ITU_MAIN, ITU_CNVFC "
                                                        + " ) VALUES ( "
                                                        + "'" + productCode + "', '" + lkUpEdtAltUom3 + "', '" + txtAltUomPrice3 + "', '0', '" + txtAltUomCnvFc3 + "' "
                                                        + " )";
                dbHlpr.ExecuteNonQuery(insertAltUoMQry);
            }
        }
    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvProductsInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvProductsInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

    }
    protected void gvProductsInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            string prdId = e.CommandArgs.CommandArgument.ToString().Trim();
            if (prdId.Length > 0) // Code is sent via request
            {
                dbHlpr.ExecuteNonQuery("DELETE FROM INV_ITM_UOMS WHERE ITU_ITMCD = '" + prdId + "' ");
                dbHlpr.ExecuteNonQuery("DELETE FROM INV_MSTR_PRODT WHERE PRO_CODE = '" + prdId + "'");

                DataTable dt = dbHlpr.FetchData("SELECT '" + prdId + "' AS TEXT01 ");
                dbHlpr.CreateDownloadRecord(dt, "ALL", "INV_MSTR_PRODT", "DLT");

                gvProductsInquery.CancelEdit();
                gvProductsInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvProductsInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid Product ID. Unable to delete Item.";
            }
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvProductsInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvProductsInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvProductsInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvProductsInquery.SearchPanelFilter = txtFilter.Text;
        gvProductsInquery.DataBind();
    }
    protected void gvProductsInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvProductsInquery, CBX_filter);
    }
    protected void gvProductsInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvProductsInquery, CBX_filter);
    }
    protected void ProdEditCategory_Init(object sender, EventArgs e)
    {
        // Get Categories for current active division
        ASPxComboBox cbCatg = (ASPxComboBox)sender;

        cbCatg.DataSource = dbHlpr.FetchData("SELECT[INV_CATCD] ,[INV_CATNM] ,[INV_CATYP]  FROM  [STP_MSTR_ICATG] WHERE INV_CATYP = 'C'");
        cbCatg.ValueField = "INV_CATCD";
        cbCatg.TextField = "INV_CATNM";
        cbCatg.DataBind();
        cbCatg.Value = EditProdCategory_SLCTD;
    }

    protected void ProdEditSubCategory_Init(object sender, EventArgs e)
    {
        // Get Categories for current active division
        ASPxComboBox cbSubCatg = (ASPxComboBox)sender;

        cbSubCatg.DataSource = dbHlpr.FetchData("SELECT[INV_CATCD] ,[INV_CATNM] ,[INV_CATYP]  FROM  [STP_MSTR_ICATG] WHERE INV_CATYP = 'S'");
        cbSubCatg.ValueField = "INV_CATCD";
        cbSubCatg.TextField = "INV_CATNM";
        cbSubCatg.DataBind();
        cbSubCatg.Value = EditProdSubCategory_SLCTD;
    }

    protected void ProdEditUDF1_Init(object sender, EventArgs e)
    {
        // Get Categories for current active division
        ASPxComboBox cbUdf1Catg = (ASPxComboBox)sender;

        cbUdf1Catg.DataSource = dbHlpr.FetchData(" SELECT[INV_CATCD] ,[INV_CATNM] ,[INV_CATYP]  FROM  [STP_MSTR_ICATG]  WHERE INV_CATYP = '1'");
        cbUdf1Catg.ValueField = "INV_CATCD";
        cbUdf1Catg.TextField = "INV_CATNM";
        cbUdf1Catg.DataBind();
        cbUdf1Catg.Value = EditProdUdf1_SLCTD;
    }

    protected void ProdEditUDF2_Init(object sender, EventArgs e)
    {
        // Get Categories for current active division
        ASPxComboBox cbUdf2Catg = (ASPxComboBox)sender;

        cbUdf2Catg.DataSource = dbHlpr.FetchData("SELECT[INV_CATCD] ,[INV_CATNM] ,[INV_CATYP]  FROM  [STP_MSTR_ICATG] WHERE INV_CATYP = '2'");
        cbUdf2Catg.ValueField = "INV_CATCD";
        cbUdf2Catg.TextField = "INV_CATNM";
        cbUdf2Catg.DataBind();
        cbUdf2Catg.Value = EditProdUdf2_SLCTD;
    }

    protected void ProdEditUDF3_Init(object sender, EventArgs e)
    {
        // Get Categories for current active division
        ASPxComboBox cbUdf3Catg = (ASPxComboBox)sender;

        cbUdf3Catg.DataSource = dbHlpr.FetchData("SELECT[INV_CATCD] ,[INV_CATNM] ,[INV_CATYP]  FROM  [STP_MSTR_ICATG] WHERE INV_CATYP = '3'");
        cbUdf3Catg.ValueField = "INV_CATCD";
        cbUdf3Catg.TextField = "INV_CATNM";
        cbUdf3Catg.DataBind();
        cbUdf3Catg.Value = EditProdUdf3_SLCTD;
    }

    protected void ProdEditUDF4_Init(object sender, EventArgs e)
    {
        // Get Categories for current active division
        ASPxComboBox cbUdf4Catg = (ASPxComboBox)sender;

        cbUdf4Catg.DataSource = dbHlpr.FetchData("SELECT[INV_CATCD] ,[INV_CATNM] ,[INV_CATYP]  FROM  [STP_MSTR_ICATG] WHERE INV_CATYP = '4'");
        cbUdf4Catg.ValueField = "INV_CATCD";
        cbUdf4Catg.TextField = "INV_CATNM";
        cbUdf4Catg.DataBind();
        cbUdf4Catg.Value = EditProdUdf4_SLCTD;
    }

    protected void ProdEditHasAltUOM_Init(object sender, EventArgs e)
    {
        // Get Categories for current active division
        ASPxComboBox cbCatg = (ASPxComboBox)sender;
        cbCatg.Value = EditProdHasAltUOM_SLCTD;
    }

    protected void txtCode_Init(object sender, EventArgs e)
    {
        if (!gvProductsInquery.IsNewRowEditing)
        {
            ASPxTextBox txtCode = (ASPxTextBox)sender;
            txtCode.ReadOnly = true;
        }
        else
        {
            ASPxTextBox txtCode1 = (ASPxTextBox)sender;
            txtCode1.ReadOnly = false;
        }
    }
    protected void gvProductsInquery_SelectionChanged(object sender, EventArgs e)
    {
        List<Object> itemCodes = gvProductsInquery.GetSelectedFieldValues("PRO_CODE");

        if (itemCodes.Count > 0)
        {
            string TARGET_URL = "~/Admin/Products/Products.aspx?ItemCode=" + itemCodes[0];
            try
            {
                Response.Redirect(TARGET_URL);
            }
            catch (Exception)
            {
                try
                {
                    DevExpress.Web.ASPxWebControl.RedirectOnCallback(TARGET_URL);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}