﻿<%@ Page Title="Upload Product Prices" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="UploadItemPrices.aspx.cs" Inherits="Admin_Products_UploadItemPrices" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <script type="text/javascript">

        function grid_customizationWindowCloseUp(s, e) {
            //if(FromGridView.IsCustomizationWindowVisible())
            gvClientInquery.ShowCustomizationWindow();
        }

    </script>
    <style type="text/css">
        .fieldsetWrapper {
            padding: 10px;
            width: 50%;
            margin: 0 auto;
            background-color: #F7F5F1;
            margin-top: 5px;
            border-radius: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="contentTopHere1" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
            <li>
                <asp:LinkButton ID="LinkBtnPDF" OnClick="LinkBtnPDF_Click" runat="server">
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonXLX" OnClick="LinkButtonXLX_Click" runat="server">
                <img src="~/img/FilesIcons/excel.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonXxls" OnClick="LinkButtonXxls_Click" runat="server">
                <img src="~/img/FilesIcons/xlsx.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonRtf" OnClick="LinkButtonRtf_Click" runat="server">
                <img src="~/img/FilesIcons/rtf.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonCvs" OnClick="LinkButtonCvs_Click" runat="server">
                <img src="~/img/FilesIcons/cvs.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonWord" OnClick="LinkButtonRtf_Click" runat="server">
                <img src="~/img/FilesIcons/word.png" runat="server" />
                </asp:LinkButton>
            </li>

            <li>
                <asp:LinkButton ID="LinkButtonHtml" OnClick="LinkButtonHtml_Click" runat="server">
                <img src="~/img/icons/searchtop.jpg" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton6" OnClick="LinkButton3_Click" runat="server">               
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton7" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/word.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton8" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/excel.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton9" OnClick="LinkButton3_Click" OnClientClick="window.print();" runat="server">
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton10" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/torrent.png" runat="server" />
                </asp:LinkButton>
            </li>
        </ul>
        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvInquery" ExportedRowType="All"></dx:ASPxGridViewExporter>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">

    <div class="formDiv" style="background-color: transparent;">
        <div class="formHeaderDiv">
            <h3>Upload Product Prices</h3>
        </div>
        <div class="fieldsetWrapper">
            <fieldset>
                <legend></legend>
                <table>
                    <tr>
                        <td>Price List</td>
                        <td>
                            <div class="DivReq">
                                <dx:ASPxComboBox OnSelectedIndexChanged="txtPriceCode_SelectedIndexChanged" AutoPostBack="true" DataSourceID="DS_PriceLists" ID="txtPriceCode" TextField="PRLST_NAME" ValueField="PRLST_KEY" runat="server" Width="170px" TabIndex="0" MaxLength="5">
                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="Text" ErrorTextPosition="Right" SetFocusOnError="true">
                                        <ErrorFrameStyle Font-Size="Smaller" />
                                        <RequiredField IsRequired="true" ErrorText="Please select Price List" />
                                    </ValidationSettings>
                                    <NullTextStyle Font-Size="Small" />
                                </dx:ASPxComboBox>
                                <asp:SqlDataSource runat="server" ID="DS_PriceLists" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>' SelectCommand="SELECT * FROM SYS_STP_PRLST " />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Choose File :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <div class="DivReq">
                                <asp:FileUpload ID="UploadedXlsFile" runat="server" TabIndex="1" />
                            </div>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Result :&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <dx:ASPxLabel ID="lblResult" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
                        </td>
                        <td></td>
                    </tr>

                </table>
            </fieldset>
            <hr style="color: skyblue" />
            <dx:ASPxLabel ID="lblErrorMessage" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
            <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                <dx:ASPxButton ID="btnUpload" runat="server" Text="Upload Item Prices" CssClass="btnPopup btn btn-success" CausesValidation="true" OnClick="btnUpload_Click" />
                <dx:ASPxButton ID="btnPopUpClear" runat="server" Text="Clear" CssClass="btnPopup btn btn-warning" CausesValidation="false" AutoPostBack="false">
                    <ClientSideEvents Click="function(s, e) { CallBackCode.PerformCallback(CatClientEditCode.GetText()); }" />
                </dx:ASPxButton>
                <dx:ASPxButton ID="btnDownloadTemplate" runat="server" Text="Download Xls Template" CssClass="btnPopup btn btn-info" CausesValidation="false" OnClick="btnDownloadTemplate_Click" />
            </div>
        </div>

        <dx:ASPxGridView ID="gvInquery" CssClass="FromGridView" ClientInstanceName="gvClientInquery" Theme="Office2010Black" runat="server" AutoGenerateColumns="False" KeyFieldName="ALU_PRLST;ALU_ITMCD;ALU_UOMCD"
            OnCustomButtonCallback="gvInquery_CustomButtonCallback"
            OnDataBinding="gvInquery_DataBinding"
            OnStartRowEditing="gvInquery_StartRowEditing"
            OnRowCommand="gvInquery_RowCommand"
            OnDataBound="gvInquery_DataBound"
            OnBeforeGetCallbackResult="gvInquery_BeforeGetCallbackResult"
            OnRowDeleting="gvInquery_RowDeleting">

            <SettingsContextMenu Enabled="True" EnableColumnMenu="True" EnableRowMenu="True"></SettingsContextMenu>
            <SettingsSearchPanel Visible="False" CustomEditorID="CustomSearchPanelTxtBx" />
            <Columns>
                <dx:GridViewCommandColumn ShowNewButtonInHeader="true" ShowDeleteButton="true" Width="130" Caption=" " AllowDragDrop="False" VisibleIndex="1"></dx:GridViewCommandColumn>

                <dx:GridViewDataTextColumn FieldName="ALU_PRLST" Caption="Price List" VisibleIndex="4">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ALU_ITMCD" Caption="Item Code" VisibleIndex="5">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="PRO_DESC1" Caption="Item Desc" VisibleIndex="6">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ALU_UOMCD" Caption="UoM Code" VisibleIndex="7" Visible="true">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ALU_TAXCD" Caption="Tax Code" VisibleIndex="8" Visible="true">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ALU_PRICE" Caption="Price" VisibleIndex="9" Visible="true">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ALU_MIN" Caption="Min. Price" VisibleIndex="10" Visible="true">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ALU_MAX" Caption="Max Price" VisibleIndex="11" Visible="true">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ALU_MAIN" Caption="Is Main UoM?" VisibleIndex="12" Visible="true">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="ALU_CNVFC" Caption="Conversion Factor" VisibleIndex="13" Visible="true">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>

            </Columns>

            <Settings HorizontalScrollBarMode="Auto" />

            <SettingsCommandButton>
                <NewButton ButtonType="Button" Text="New" Styles-Style-CssClass="btna btn btn-primary  btn-sm">
                    <Image runat="server" Url="~/img/icons/white-icn/Add.png" Width="15px"></Image>
                </NewButton>

                <SelectButton ButtonType="Button" Text="View" Styles-Style-CssClass="btna btn btn-success  btn-sm">
                    <Image ToolTip="View button here" Url="~/img/icons/white-icn/eyee1.png" Width="15px" />
                </SelectButton>

                <EditButton ButtonType="Button" Text="Edit" Styles-Style-CssClass="btna btn btn-info  btn-sm">
                    <Image runat="server" Url="~/img/icons/white-icn/Pen Tool.png" Width="15px"></Image>
                </EditButton>

                <DeleteButton ButtonType="Button" Text="Delete" Styles-Style-CssClass="btna btn btn-danger btn-sm btn-danger-variant">
                    <Image runat="server" Url="~/img/icons/white-icn/Trash.png" Width="15px"></Image>
                </DeleteButton>

                <CancelButton ButtonType="Button" Text="Close" Styles-Style-CssClass="btnPopup btn btn-primary">
                </CancelButton>
                <UpdateButton ButtonType="Button" Text="Update">
                </UpdateButton>
            </SettingsCommandButton>
            <SettingsEditing EditFormColumnCount="2" />
            <SettingsPopup>
                <EditForm VerticalAlign="WindowCenter" AllowResize="True" Modal="True" HorizontalAlign="WindowCenter"></EditForm>
            </SettingsPopup>

            <SettingsPager Position="Bottom">
                <PageSizeItemSettings Caption="Records per Page" Items="10, 20, 50, 100" ShowAllItem="true" Position="Right" Visible="true">
                </PageSizeItemSettings>
            </SettingsPager>
            <Styles>
                <CommandColumn Spacing="0px" Wrap="False" />
                <EditFormDisplayRow HorizontalAlign="Center" VerticalAlign="Middle"></EditFormDisplayRow>
                <AlternatingRow CssClass="alterRowGV" Enabled="True"></AlternatingRow>
                <Row CssClass="rowHeight"></Row>
            </Styles>
            <Settings ShowGroupPanel="true" VerticalScrollBarMode="Auto" />
            <SettingsLoadingPanel Mode="ShowOnStatusBar" />
            <SettingsBehavior EnableCustomizationWindow="true" AllowDragDrop="true" ConfirmDelete="True" ColumnResizeMode="Control" />
            <ClientSideEvents ColumnStartDragging="grid_customizationWindowCloseUp" EndCallback="GridOnEndCallBack" />


            <Templates>
                <EditForm>
                    <div class="overlay">
                        <div class="FormPopup">
                            <div class="formHeaderDiv">
                                <h3>Item Price Detail</h3>
                            </div>
                            <fieldset>
                                <legend></legend>
                                <table>
                                    <tr>
                                        <td>Item Code : </td>
                                        <td>
                                            <div class="DivReq">
                                                <asp:SqlDataSource runat="server" ID="DS_Items" SelectCommand="SELECT PRO_CODE, PRO_DESC1 FROM INV_MSTR_PRODT" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
                                                <dx:ASPxComboBox OnSelectedIndexChanged="EditItem_SelectedIndexChanged" AutoPostBack="true" ID="EditItem" ClientInstanceName="ClientEditItem" runat="server" Width="170px" TabIndex="2" DataSourceID="DS_Items" Value='<%# Bind("ALU_ITMCD") %>' TextField="PRO_CODE" ValueField="PRO_CODE" DisplayFormatString="{0}">
                                                    <Columns>
                                                        <dx:ListBoxColumn FieldName="PRO_CODE" Width="100px" />
                                                        <dx:ListBoxColumn FieldName="PRO_DESC1" Width="250px" />
                                                    </Columns>
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <RequiredField IsRequired="true" ErrorText="Item Code is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>

                                        <td>&nbsp; </td>
                                        <td>Tax Code : </td>
                                        <td>
                                            <div class="DivReq">
                                                <asp:SqlDataSource runat="server" ID="DS_Taxes" SelectCommand="SELECT TAX_CODE, TAX_TITLE, TAX_FACTR FROM STP_MSTR_TAXES" ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
                                                <dx:ASPxComboBox ID="EditTax" DataSourceID="DS_Taxes" ClientInstanceName="ClientEditTax" runat="server" ValueField="TAX_CODE" TextField="TAX_TITLE" Width="170px" Value='<%# Bind("ALU_TAXCD") %>' TabIndex="4">
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Tax Code is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ClientVisible="false" runat="server" ID="lblUoM1"></dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <dx:ASPxTextBox ClientVisible="false" ID="txtPriceUoM1" runat="server" Width="170px" TabIndex="3">
                                                <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RegularExpression ValidationExpression="^[0-9]((\d+)?(.\d+)?)?$" ErrorText="Please enter a valid number." />
                                                    <RequiredField IsRequired="true" ErrorText="Price is Required." />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td></td>
                                        <td>
                                            <dx:ASPxLabel ClientVisible="false" runat="server" ID="lblMinPrice1" Text="Min Price : "></dx:ASPxLabel></td>
                                        <td>
                                            <div class="DivReqDepc">
                                                <dx:ASPxTextBox ClientVisible="false" ID="txtMinPrice1" ClientInstanceName="txtMinPrice1" runat="server" Width="170px" TabIndex="5" MaxLength="15">
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Min. Price is Required" />
                                                    </ValidationSettings>
                                                    <NullTextStyle Font-Size="Small" />
                                                    <MaskSettings Mask="<0..99999>.<00..99>" />
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                        <td></td>
                                        <td>
                                            <dx:ASPxLabel ClientVisible="false" runat="server" ID="lblMaxPrice1" Text="Max Price : "></dx:ASPxLabel>
                                        <td>
                                            <div class="DivReqDepc">
                                                <dx:ASPxTextBox ClientVisible="false" ID="txtMaxPrice1" ClientInstanceName="txtMaxPrice1" runat="server" Width="170px" TabIndex="6" MaxLength="15">
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Max. Price is Required" />
                                                    </ValidationSettings>
                                                    <NullTextStyle Font-Size="Small" />
                                                    <MaskSettings Mask="<0..99999>.<00..99>" />
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ClientVisible="false" runat="server" ID="lblUoM2"></dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <dx:ASPxTextBox ClientVisible="false" ID="txtPriceUoM2" runat="server" Width="170px" TabIndex="3">
                                                <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RegularExpression ValidationExpression="^[0-9]((\d+)?(.\d+)?)?$" ErrorText="Please enter a valid number." />
                                                    <RequiredField IsRequired="true" ErrorText="Price is Required." />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td></td>
                                        <td>
                                            <dx:ASPxLabel ClientVisible="false" runat="server" ID="lblMinPrice2" Text="Min Price : "></dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <div class="DivReqDepc">
                                                <dx:ASPxTextBox ClientVisible="false" ID="txtMinPrice2" ClientInstanceName="txtMinPrice2" runat="server" Width="170px" TabIndex="8" MaxLength="15">
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Min. Price is Required" />
                                                    </ValidationSettings>
                                                    <NullTextStyle Font-Size="Small" />
                                                    <MaskSettings Mask="<0..99999>.<00..99>" />
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                        <td></td>
                                        <td>
                                            <dx:ASPxLabel ClientVisible="false" runat="server" ID="lblMaxPrice2" Text="Max Price : "></dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <div class="DivReqDepc">
                                                <dx:ASPxTextBox ClientVisible="false" ID="txtMaxPrice2" ClientInstanceName="txtMaxPrice2" runat="server" Width="170px" TabIndex="9" MaxLength="15">
                                                    <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Max. Price is Required" />
                                                    </ValidationSettings>
                                                    <NullTextStyle Font-Size="Small" />
                                                    <MaskSettings Mask="<0..99999>.<00..99>" />
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ClientVisible="false" runat="server" ID="lblUoM3"></dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <dx:ASPxTextBox ClientVisible="false" ID="txtPriceUoM3" runat="server" Width="170px" TabIndex="3">
                                                <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RegularExpression ValidationExpression="^[0-9]((\d+)?(.\d+)?)?$" ErrorText="Please enter a valid number." />
                                                    <RequiredField IsRequired="true" ErrorText="Price is Required." />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td></td>
                                        <td>
                                            <dx:ASPxLabel ClientVisible="false" runat="server" ID="lblMinPrice3" Text="Min Price : "></dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <div class="DivReqDepc">
                                                <dx:ASPxTextBox ClientVisible="false" ID="txtMinPrice3" ClientInstanceName="txtMinPrice3" runat="server" Width="170px" TabIndex="11" MaxLength="15">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Min. Price is Required" />
                                                    </ValidationSettings>
                                                    <NullTextStyle Font-Size="Small" />
                                                    <MaskSettings Mask="<0..99999>.<00..99>" />
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                        <td></td>
                                        <td>
                                            <dx:ASPxLabel ClientVisible="false" runat="server" ID="lblMaxPrice3" Text="Max Price : "></dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <div class="DivReqDepc">
                                                <dx:ASPxTextBox ClientVisible="false" ID="txtMaxPrice3" ClientInstanceName="txtMaxPrice3" runat="server" Width="170px" TabIndex="12" MaxLength="15">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Max. Price is Required" />
                                                    </ValidationSettings>
                                                    <NullTextStyle Font-Size="Small" />
                                                    <MaskSettings Mask="<0..99999>.<00..99>" />
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <dx:ASPxLabel ClientVisible="false" runat="server" ID="lblUoM4"></dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <dx:ASPxTextBox ClientVisible="false" ID="txtPriceUoM4" runat="server" Width="170px" TabIndex="3">
                                                <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RegularExpression ValidationExpression="^[0-9]((\d+)?(.\d+)?)?$" ErrorText="Please enter a valid number." />
                                                    <RequiredField IsRequired="true" ErrorText="Price is Required." />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>
                                        </td>
                                        <td></td>
                                        <td>
                                            <dx:ASPxLabel ClientVisible="false" runat="server" ID="lblMinPrice4" Text="Min Price : "></dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <div class="DivReqDepc">
                                                <dx:ASPxTextBox ClientVisible="false" ID="txtMinPrice4" ClientInstanceName="txtMinPrice4" runat="server" Width="170px" TabIndex="11" MaxLength="15">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Min. Price is Required" />
                                                    </ValidationSettings>
                                                    <NullTextStyle Font-Size="Small" />
                                                    <MaskSettings Mask="<0..99999>.<00..99>" />
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                        <td></td>
                                        <td>
                                            <dx:ASPxLabel ClientVisible="false" runat="server" ID="lblMaxPrice4" Text="Max Price : "></dx:ASPxLabel>
                                        </td>
                                        <td>
                                            <div class="DivReqDepc">
                                                <dx:ASPxTextBox ClientVisible="false" ID="txtMaxPrice4" ClientInstanceName="txtMaxPrice4" runat="server" Width="170px" TabIndex="12" MaxLength="15">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Max. Price is Required" />
                                                    </ValidationSettings>
                                                    <NullTextStyle Font-Size="Small" />
                                                    <MaskSettings Mask="<0..99999>.<00..99>" />
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                            <hr style="color: skyblue" />
                            <dx:ASPxLabel ID="lblErrorMessage" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
                            <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                                <dx:ASPxButton ID="btnAdd1" runat="server" Text="Save" CssClass="btnPopup btn btn-success" CausesValidation="true" OnClick="btnAdd1_Click" />
                                <dx:ASPxButton ID="btnPopUpClear" runat="server" Text="Cancel" CssClass="btnPopup btn btn-warning" CausesValidation="false" AutoPostBack="false">
                                    <ClientSideEvents Click="function(s, e) { CallBackCode.PerformCallback(CatClientEditCode.GetText()); }" />
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="btnPopUpDelete" runat="server" CommandArgument='<%# Eval("ALU_PRLST") + "|" + Eval("ALU_ITMCD") + "|" + Eval("ALU_UOMCD") %>' CommandName="PopUpDelete" Text="Delete" CssClass="btnPopup btn btn-danger btn-danger-variant" CausesValidation="false">
                                    <ClientSideEvents Click="function(s, e) { if (!confirm('Confirm Delete?')) { e.processOnServer = false; } }" />
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="btnPopUpPrint" runat="server" Text="Print" CssClass="btnPopup btn btn-info btn-info-variant" AutoPostBack="false" CausesValidation="false">
                                    <ClientSideEvents Click="function(s, e) { printDoc(s, e); }" />
                                </dx:ASPxButton>
                                <dx:ASPxGridViewTemplateReplacement ID="btnPopUpClose" ReplacementType="EditFormCancelButton" runat="server" />
                            </div>
                        </div>
                    </div>
                </EditForm>
            </Templates>

            <Templates>
                <StatusBar>
                    <table>
                        <tr>
                            <td>&nbsp;
                            </td>
                            <td>Search: </td>
                            <td>
                                <dx:ASPxButtonEdit runat="server" ID="CustomSearchPanelTxtBx" />
                            </td>
                            <td>&nbsp;</td>
                            <td>Search In:</td>
                            <td>
                                <dx:ASPxComboBox AutoPostBack="true" ID="DDLFilterByColumn" runat="server" OnSelectedIndexChanged="DDLFilterByColumn_SelectedIndexChanged">
                                    <Items>
                                        <dx:ListEditItem Text="All" Value="*" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>

                        </tr>
                    </table>
                </StatusBar>
            </Templates>

        </dx:ASPxGridView>
    </div>
    <script type="text/javascript">
        window.onload = function () {
            $('form').on('click', '.btnPopupClose', function () {
                $('.overlay').hide();
            });
        };

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //if (prm != null) {
        prm.add_endRequest(function (sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.overlay').show();
            }
        });
        prm.add_endRequest(GridOnEndCallBack);
        //};

    </script>
</asp:Content>

