﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;

public partial class Admin_PriceChangeReasons_PriceChangeReasons : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT PRS_CODE, PRS_TITLE FROM STP_MST_PRSNS ";
    }

    protected void categoryListingGridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvRsnCdInquery_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvRsnCdInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string rsnCode = e.Keys["PRS_CODE"].ToString().Trim();

        dbHlpr.ExecuteNonQuery("DELETE FROM STP_MST_PRSNS WHERE PRS_CODE = '" + rsnCode + "'");

        DataTable dt = dbHlpr.FetchData("SELECT '" + rsnCode + "' AS TEXT01 ");
        dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MST_PRSNS", "DLT");

        e.Cancel = true;
        gvRsnCdInquery.CancelEdit();
    }
    protected void ACB_CodeCheck_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (e.Parameter.Length > 0)
        {
            DataTable myDT = new DataTable();
            myDT = dbHlpr.FetchData("SELECT * FROM STP_MST_PRSNS WHERE PRS_CODE = '" + e.Parameter.Trim() + "'");
            JavaScriptSerializer myserializer = new JavaScriptSerializer();
            if (myDT.Rows.Count > 0)
            {
                e.Result = myserializer.Serialize(myDT.Rows[0].ItemArray);
            }
            else
            {
                e.Result = myserializer.Serialize("");
            }
        }
    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxTextBox txtBxCode = ((ASPxTextBox)gvRsnCdInquery.FindEditFormTemplateControl("RsnEditCode"));
        string rsnCode = "0";

        if (txtBxCode.Text.Length > 0)
        {
            rsnCode = txtBxCode.Text.Trim();
        }   

        string RsnCdName = ((ASPxTextBox)gvRsnCdInquery.FindEditFormTemplateControl("RsnEditName")).Text;
       
        DataTable myDT = new DataTable();

        myDT = myDT = dbHlpr.FetchData("SELECT * FROM STP_MST_PRSNS WHERE PRS_CODE = '" + rsnCode + "'");
        if (myDT.Rows.Count <= 0)
        {

            dbHlpr.ExecuteNonQuery("INSERT INTO STP_MST_PRSNS (PRS_CODE, PRS_TITLE) "
            + " VALUES ('" + rsnCode + "', '" + RsnCdName + "')");

            DataTable dt = dbHlpr.FetchData("SELECT "
                + " PRS_CODE AS TEXT01, PRS_TITLE AS TEXT02 "
                + " FROM STP_MST_PRSNS "
                + " WHERE PRS_CODE = '" + rsnCode + "' ");
            dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MST_PRSNS", "CRT");
        }
        else
        {
            dbHlpr.ExecuteNonQuery("UPDATE STP_MST_PRSNS SET "
            + " PRS_TITLE = '" + RsnCdName + "' "
            + " WHERE PRS_CODE = '" + rsnCode + "'");

            DataTable dt = dbHlpr.FetchData("SELECT "
                + " PRS_CODE AS TEXT01, PRS_TITLE AS TEXT02 "
                + " FROM STP_MST_PRSNS "
                + " WHERE PRS_CODE = '" + rsnCode + "' ");
            dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MST_PRSNS", "UPD");
        }

        gvRsnCdInquery.CancelEdit();
        gvRsnCdInquery.DataBind();
    }
    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvRsnCdInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvRsnCdInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

        ////html export here
        //gridExport.ExportHtmlToResponse();
        //string str;
        //MemoryStream ms = new MemoryStream();
        //try
        //{
        //    gridExport.ExportToHtml(ms);
        //    ms.Seek(0, SeekOrigin.Begin);
        //    StreamReader sr = new StreamReader(ms);
        //    str = sr.ReadToEnd();
        //}
        //finally
        //{
        //    ms.Close();
        //}

    }
    protected void gvRsnCdInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            string rsnCd = e.CommandArgs.CommandArgument.ToString();

            if (rsnCd != "") // Code is sent via request
            {
                dbHlpr.ExecuteNonQuery("DELETE FROM STP_MST_PRSNS WHERE PRS_CODE = '" + rsnCd + "'");

                DataTable dt = dbHlpr.FetchData("SELECT '" + rsnCd + "' AS TEXT01 ");
                dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MST_PRSNS", "DLT");

                gvRsnCdInquery.CancelEdit();
                gvRsnCdInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvRsnCdInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid Reason Code. Unable to Delete Reason Code.";
            }
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvRsnCdInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvRsnCdInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvRsnCdInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvRsnCdInquery.SearchPanelFilter = txtFilter.Text;
        gvRsnCdInquery.DataBind();
    }
    protected void gvRsnCdInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvRsnCdInquery, CBX_filter);
    }
    protected void gvRsnCdInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvRsnCdInquery, CBX_filter);
    }


    protected void RsnEditCode_Init(object sender, EventArgs e)
    {
        if (!gvRsnCdInquery.IsNewRowEditing)
        {
            ASPxTextBox txtCode = (ASPxTextBox)sender;
            txtCode.ReadOnly = true;
        }
        else
        {
            ASPxTextBox txtCode1 = (ASPxTextBox)sender;
            txtCode1.ReadOnly = false;
        }     
    }
}