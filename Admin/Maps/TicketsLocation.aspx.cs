﻿using DevExpress.Web;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

public partial class Admin_Maps_TicketsLocation : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    public static string markers = "";
    public static string TicketInfo = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadMapMarkers();
    }

    protected void MapTstStrNo_Init(object sender, EventArgs e)
    {
        DataTable dtStores = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE WHERE STM_CODE IN (" + Session["userBranchCode"].ToString() + ") ORDER BY STM_NAME ");

        MapTstStrNo.DataSource = dtStores;
        MapTstStrNo.ValueField = "STM_CODE";
        MapTstStrNo.TextField = "STM_NAME";
        MapTstStrNo.DataBind();
    }
    protected void MapTstRegNo_Init(object sender, EventArgs e)
    {
        DataTable dtVans = dbHlpr.FetchData("SELECT * FROM STP_MSTR_SLREP WHERE SRP_STRNO IN (" + Session["userBranchCode"].ToString() + ") ORDER BY SRP_CODE ");

        MapTstRegNo.DataSource = dtVans;
        MapTstRegNo.ValueField = "SRP_CODE";
        MapTstRegNo.TextField = "SRP_NAME";
        MapTstRegNo.DataBind();
    }

    protected void MapTstStrNo_ValueChanged(object sender, EventArgs e)
    {
        ASPxComboBox value = (ASPxComboBox)sender;
        string strNo = value.Value == null ? "" : value.Value.ToString();
        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_SLREP WHERE SRP_STRNO IN (" + strNo + ") ORDER BY SRP_CODE ");

        MapTstRegNo.DataSource = dtRegisters;
        MapTstRegNo.ValueField = "SRP_CODE";
        MapTstRegNo.TextField = "SRP_NAME";
        MapTstRegNo.DataBind();
    }

    private void LoadMapMarkers()
    {
        markers = "";
        TicketInfo = "";

        string oneStore = MapTstStrNo.Value != null ? MapTstStrNo.Value.ToString() : "";
        string oneRegister = MapTstRegNo.Value != null ? MapTstRegNo.Value.ToString() : "";
        string StartDate = MapTstDateFrom.Value != null ? ((DateTime)MapTstDateFrom.Value).ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");
        string EndDate = MapTstDateTo.Value != null ? ((DateTime)MapTstDateTo.Value).ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");

        string qryMarkers = " SELECT STUFF((SELECT ',{lat:' + tbl.TKH_GEOLA + ',lng:' + tbl.TKH_GEOLN + '}' FROM ( ";

        qryMarkers += "SELECT TKH_HKEY, TKH_TKTNO, TKH_DATE, TKH_STRNO, TKH_SREP, TKH_CUSNO, TKH_CUSNM, TKH_GEOLA, TKH_GEOLN "
            + " FROM SLS_TRX_THDR "
            + " WHERE (TKH_GEOLA <> '0.0' AND TKH_GEOLA <> 'TIMEOUT') ";
        if (oneStore.Length > 0)
        {
            qryMarkers += " AND TKH_STRNO = '" + oneStore + "' ";
        }

        if (oneRegister.Length > 0)
        {
            qryMarkers += " AND TKH_SREP = '" + oneRegister + "' ";
        }

        qryMarkers += " AND TKH_DATE >= CONVERT(datetime, '" + StartDate + "') ";
        qryMarkers += " AND TKH_DATE <= CONVERT(datetime, '" + EndDate + " 23:59:59.998') ";

        qryMarkers += " UNION ALL ";

        qryMarkers += "SELECT TKH_HKEY, TKH_TKTNO, TKH_DATE, TKH_STRNO, TKH_SREP, TKH_CUSNO, TKH_CUSNM, TKH_GEOLA, TKH_GEOLN "
            + " FROM SLS_TKH_HIST "
            + " WHERE (TKH_GEOLA <> '0.0' AND TKH_GEOLA <> 'TIMEOUT') ";
        if (oneStore.Length > 0)
        {
            qryMarkers += " AND TKH_STRNO = '" + oneStore + "' ";
        }

        if (oneRegister.Length > 0)
        {
            qryMarkers += " AND TKH_SREP = '" + oneRegister + "' ";
        }

        qryMarkers += " AND TKH_DATE >= CONVERT(datetime, '" + StartDate + "') ";
        qryMarkers += " AND TKH_DATE <= CONVERT(datetime, '" + EndDate + " 23:59:59.998') ";

        qryMarkers += " ) tbl ";

        qryMarkers += " ORDER BY tbl.TKH_DATE DESC ";

        qryMarkers += "  FOR XML PATH('')), 1, 1, '') ";

        DataTable dtMarkers = dbHlpr.FetchData(qryMarkers);
        markers = dtMarkers.Rows[0][0].ToString().Trim(',');


        string qryTicketInfo = " SELECT STUFF((SELECT ',{tktno:\"' + CONVERT(nvarchar(16), tbl.TKH_TKTNO) + '\",tktdate:\"' + CONVERT(nvarchar(25), tbl.TKH_DATE) + '\",tktstr:\"' + tbl.TKH_STRNO + '\",tktvan:\"' + tbl.TKH_SREP + '\",tktcust:\"' + tbl.TKH_CUSNO + ' - ' + tbl.TKH_CUSNM + '\"}' FROM ( ";

        qryTicketInfo += "SELECT TKH_HKEY, TKH_TKTNO, TKH_DATE, TKH_STRNO, TKH_SREP, TKH_CUSNO, TKH_CUSNM, TKH_GEOLA, TKH_GEOLN "
            + " FROM SLS_TRX_THDR "
            + " WHERE (TKH_GEOLA <> '0.0' AND TKH_GEOLA <> 'TIMEOUT') ";
        if (oneStore.Length > 0)
        {
            qryTicketInfo += " AND TKH_STRNO = '" + oneStore + "' ";
        }

        if (oneRegister.Length > 0)
        {
            qryTicketInfo += " AND TKH_SREP = '" + oneRegister + "' ";
        }

        qryTicketInfo += " AND TKH_DATE >= CONVERT(datetime, '" + StartDate + "') ";
        qryTicketInfo += " AND TKH_DATE <= CONVERT(datetime, '" + EndDate + " 23:59:59.998') ";

        qryTicketInfo += " UNION ALL ";

        qryTicketInfo += "SELECT TKH_HKEY, TKH_TKTNO, TKH_DATE, TKH_STRNO, TKH_SREP, TKH_CUSNO, TKH_CUSNM, TKH_GEOLA, TKH_GEOLN "
            + " FROM SLS_TKH_HIST "
            + " WHERE (TKH_GEOLA <> '0.0' AND TKH_GEOLA <> 'TIMEOUT') ";
        if (oneStore.Length > 0)
        {
            qryTicketInfo += " AND TKH_STRNO = '" + oneStore + "' ";
        }

        if (oneRegister.Length > 0)
        {
            qryTicketInfo += " AND TKH_SREP = '" + oneRegister + "' ";
        }

        qryTicketInfo += " AND TKH_DATE >= CONVERT(datetime, '" + StartDate + "') ";
        qryTicketInfo += " AND TKH_DATE <= CONVERT(datetime, '" + EndDate + " 23:59:59.998') ";

        qryTicketInfo += " ) tbl ";

        qryTicketInfo += " ORDER BY tbl.TKH_DATE DESC ";

        qryTicketInfo += "  FOR XML PATH('')), 1, 1, '') ";

        DataTable dtTicketInfo = dbHlpr.FetchData(qryTicketInfo);
        TicketInfo = dtTicketInfo.Rows[0][0].ToString().Trim(',');

        string AllTicketsInPeriod = "SELECT SUM(CNTS) FROM ( SELECT COUNT(TKH_HKEY) AS CNTS "
            + " FROM SLS_TRX_THDR WHERE ";
        AllTicketsInPeriod += " TKH_DATE >= CONVERT(datetime, '" + StartDate + "') ";
        AllTicketsInPeriod += " AND TKH_DATE <= CONVERT(datetime, '" + EndDate + " 23:59:59.998') ";
        if (oneStore.Length > 0)
        {
            AllTicketsInPeriod += " AND TKH_STRNO = '" + oneStore + "' ";
        }

        if (oneRegister.Length > 0)
        {
            AllTicketsInPeriod += " AND TKH_SREP = '" + oneRegister + "' ";
        }

        AllTicketsInPeriod += " UNION ALL ";

        AllTicketsInPeriod += "SELECT COUNT(TKH_HKEY) AS CNTS "
            + " FROM SLS_TKH_HIST WHERE ";
        AllTicketsInPeriod += " TKH_DATE >= CONVERT(datetime, '" + StartDate + "') ";
        AllTicketsInPeriod += " AND TKH_DATE <= CONVERT(datetime, '" + EndDate + " 23:59:59.998') ";
        if (oneStore.Length > 0)
        {
            AllTicketsInPeriod += " AND TKH_STRNO = '" + oneStore + "' ";
        }

        if (oneRegister.Length > 0)
        {
            AllTicketsInPeriod += " AND TKH_SREP = '" + oneRegister + "' ";
        }

        AllTicketsInPeriod += ") tbl ";

        DataTable dtAllTicketsInPeriod = dbHlpr.FetchData(AllTicketsInPeriod);
        all_tkts.InnerText = dtAllTicketsInPeriod.Rows[0][0].ToString();
    }
}