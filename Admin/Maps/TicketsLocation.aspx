﻿<%@ Page Title="Admin Panel - Ticketing GPS" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="TicketsLocation.aspx.cs" Inherits="Admin_Maps_TicketsLocation" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <style>
        #map-wrapper {
            width: 100%;
            height: 200px;
        }
    </style>

    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJFjo4J5876rFLEOvI1ZuNCM1eb9lmqQY&callback=initMap&libraries=&v=weekly" defer></script>

    <script type="text/javascript">
        jQuery = jQuery.noConflict();
        $ = jQuery.noConflict();
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">
    <div class="formDiv">
        <div class="formHeaderDiv">
            <h3>Ticketing GPS</h3>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="fieldsetWrapper">
                        <fieldset>
                            <legend></legend>
                            <table style="margin: 0 auto;">
                                <tr>
                                    <td>Date From :&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        <div class="DivReq">
                                            <dx:ASPxDateEdit runat="server" ID="MapTstDateFrom" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy" NullText="Today" NullTextStyle-ForeColor="Black" AutoPostBack="true"></dx:ASPxDateEdit>
                                        </div>
                                    </td>
                                    <td></td>

                                    <td>Date To :&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        <div class="DivReq">
                                            <dx:ASPxDateEdit runat="server" ID="MapTstDateTo" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy" NullText="Today" NullTextStyle-ForeColor="Black" AutoPostBack="true"></dx:ASPxDateEdit>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Branch :&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        <div class="DivReq">
                                            <dx:ASPxComboBox NullText="Select..." ID="MapTstStrNo" OnInit="MapTstStrNo_Init" ClientInstanceName="MapTstClientStrNo" runat="server" AnimationType="Auto" OnValueChanged="MapTstStrNo_ValueChanged" AutoPostBack="true">
                                                <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RequiredField IsRequired="true" ErrorText="Branch is Required." />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </div>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>Van # :&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        <div class="DivReq">
                                            <dx:ASPxComboBox NullText="Select..." ID="MapTstRegNo" OnInit="MapTstRegNo_Init" ClientInstanceName="MapTstClientRegNo" runat="server" AnimationType="Auto" AutoPostBack="true">
                                                <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RequiredField IsRequired="true" ErrorText="Van No. is Required." />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </fieldset>
                        <hr style="color: skyblue" />
                        <dx:ASPxLabel ID="lblErrorMessage" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div style="height: 700px;" id="map"></div>
                    </div>
                    <div class="col-md-3">
                        <h3>Summary</h3>
                        <table width="80%">
                            <tr>
                                <td>
                                    <strong>Total Tickets:</strong>
                                </td>
                                <td align="right">
                                    <span runat="server" style="font-weight: normal;" id="all_tkts"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Tickets with Location:</strong>
                                </td>
                                <td align="right">
                                    <span style="font-weight: normal;" id="tkts_with_location"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script type="application/javascript">
            function initMap() {
                const myLatLng = [<%= markers %>];
                const tickets = [<%= TicketInfo %>];
                const map = new google.maps.Map(document.getElementById("map"), {
                    zoom: 16,
                    center: myLatLng[0],
                });

                jQuery('#tkts_with_location').text(myLatLng.length);

                console.log("No of Markers: " + myLatLng.length);
                console.log("No of Tickets: " + tickets.length);

                var bounds = new google.maps.LatLngBounds();
                for (var i = 0; i < myLatLng.length; i++) {
                    bounds.extend(myLatLng[i]);
                    const myMark = new google.maps.Marker({
                        position: myLatLng[i],
                        map: map,
                        title: tickets[i].tktno + "\n" + tickets[i].tktstr,
                    });
                    const contentString =
                        '<div id="content">' +
                        '<div id="siteNotice"></div>' +
                        '<h1 id="firstHeading" class="firstHeading">Ticket # : ' + tickets[i].tktno + '</h1>' +
                        '<div id="bodyContent">' +
                        "<p><b>Store # : </b>" + tickets[i].tktstr +
                        "<p><b>Van # : </b>" + tickets[i].tktvan +
                        "<p><b>Customer : </b>" + tickets[i].tktcust +
                        "<p><b>Date : </b>" + tickets[i].tktdate +
                        "</div>" +
                        "</div>";
                    const infowindow = new google.maps.InfoWindow({
                        content: contentString,
                    });
                    myMark.addListener("click", function () {
                        infowindow.open(map, myMark);
                    });
                }
                map.fitBounds(bounds);
            }
        </script>
</asp:Content>

