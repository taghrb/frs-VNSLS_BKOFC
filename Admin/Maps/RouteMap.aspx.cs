﻿using DevExpress.Web;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

public partial class Admin_Maps_RouteMap : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    public static string path_coordinates = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadCoordinates();
    }

    protected void MapTstStrNo_Init(object sender, EventArgs e)
    {
        DataTable dtStores = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE WHERE STM_CODE IN (" + Session["userBranchCode"].ToString() + ") ORDER BY STM_NAME ");

        MapTstStrNo.DataSource = dtStores;
        MapTstStrNo.ValueField = "STM_CODE";
        MapTstStrNo.TextField = "STM_NAME";
        MapTstStrNo.DataBind();
    }
    protected void MapTstSlsRepNo_Init(object sender, EventArgs e)
    {
        DataTable dtVans = dbHlpr.FetchData("SELECT * FROM STP_MSTR_SLREP WHERE SRP_STRNO IN (" + Session["userBranchCode"].ToString() + ") ORDER BY SRP_CODE ");

        MapTstSlsRepNo.DataSource = dtVans;
        MapTstSlsRepNo.ValueField = "SRP_CODE";
        MapTstSlsRepNo.TextField = "SRP_NAME";
        MapTstSlsRepNo.DataBind();
    }

    protected void MapTstStrNo_ValueChanged(object sender, EventArgs e)
    {
        ASPxComboBox value = (ASPxComboBox)sender;
        string strNo = value.Value == null ? "" : value.Value.ToString();
        DataTable dtVans = dbHlpr.FetchData("SELECT * FROM STP_MSTR_SLREP WHERE SRP_STRNO IN (" + strNo + ") ORDER BY SRP_CODE ");

        MapTstSlsRepNo.DataSource = dtVans;
        MapTstSlsRepNo.ValueField = "SRP_CODE";
        MapTstSlsRepNo.TextField = "SRP_NAME";
        MapTstSlsRepNo.DataBind();
    }

    private void LoadCoordinates()
    {
        path_coordinates = "";

        string oneStore = MapTstStrNo.Value != null ? MapTstStrNo.Value.ToString() : "";
        string oneVan = MapTstSlsRepNo.Value != null ? MapTstSlsRepNo.Value.ToString() : "";
        string StartDate = MapTstDateFrom.Value != null ? ((DateTime)MapTstDateFrom.Value).ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");
        string EndDate = MapTstDateTo.Value != null ? ((DateTime)MapTstDateTo.Value).ToString("yyyy-MM-dd") : DateTime.Now.ToString("yyyy-MM-dd");


        string qry = "SELECT STUFF((SELECT ',{lat:' + tbl.CRD_LAT + ',lng:' + tbl.CRD_LNG + ',capAt:\"' + CONVERT(nvarchar(25), tbl.CRD_CAPAT) + '\"}' FROM ( " 
            + " SELECT " 
            + " CRD_ID, CRD_VAN, CRD_LAT, CRD_LNG, CRD_CAPAT, CRD_WRTAT, CRD_SYCAT, CRD_RAW "
            + " FROM GPS_LIV_CORDS "
            + " WHERE CRD_VAN = '" + oneVan + "' ";
        qry += " AND CRD_CAPAT >= CONVERT(datetime, '" + StartDate + "') ";
        qry += " AND CRD_CAPAT <= CONVERT(datetime, '" + EndDate + " 23:59:59.998') ";

        qry += " ) tbl ";
        qry += " ORDER BY tbl.CRD_CAPAT ";
        qry += " FOR XML PATH('')), 1, 1, '') ";

        DataTable dtLocations = dbHlpr.FetchData(qry);
        path_coordinates = dtLocations.Rows[0][0].ToString().Trim(',');
    }
}