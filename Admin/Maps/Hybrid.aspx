﻿<%@ Page Title="Admin Panel - Maps" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="Hybrid.aspx.cs" Inherits="Admin_Maps_Hybrid" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <style>
        #map-wrapper {
            width: 100%;
            height: 200px;
        }
    </style>

    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJFjo4J5876rFLEOvI1ZuNCM1eb9lmqQY&callback=initMap&libraries=&v=weekly" defer></script>

    <script type="text/javascript">
        jQuery = jQuery.noConflict();
        $ = jQuery.noConflict();
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">
    <div class="formDiv">
        <div class="formHeaderDiv">
            <h3>Maps</h3>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="fieldsetWrapper">
                        <fieldset>
                            <legend></legend>
                            <table style="margin: 0 auto;">
                                <tr>
                                    <td>Date From :&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        <div class="DivReq">
                                            <dx:ASPxDateEdit runat="server" ID="MapTstDateFrom" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy" NullText="Today" NullTextStyle-ForeColor="Black" AutoPostBack="true"></dx:ASPxDateEdit>
                                        </div>
                                    </td>
                                    <td></td>

                                    <td>Date To :&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        <div class="DivReq">
                                            <dx:ASPxDateEdit runat="server" ID="MapTstDateTo" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy" NullText="Today" NullTextStyle-ForeColor="Black" AutoPostBack="true"></dx:ASPxDateEdit>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Branch :&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        <div class="DivReq">
                                            <dx:ASPxComboBox NullText="Select..." ID="MapTstStrNo" OnInit="MapTstStrNo_Init" ClientInstanceName="MapTstClientStrNo" runat="server" AnimationType="Auto" OnValueChanged="MapTstStrNo_ValueChanged" AutoPostBack="true">
                                                <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RequiredField IsRequired="true" ErrorText="Branch is Required." />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </div>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>Van # :&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        <div class="DivReq">
                                            <dx:ASPxComboBox NullText="Select..." ID="MapTstRegNo" OnInit="MapTstRegNo_Init" ClientInstanceName="MapTstClientRegNo" runat="server" AnimationType="Auto" AutoPostBack="true">
                                                <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RequiredField IsRequired="true" ErrorText="Van No. is Required." />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </fieldset>
                        <hr style="color: skyblue" />
                        <dx:ASPxLabel ID="lblErrorMessage" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div style="height: 700px;" id="map"></div>
                    </div>
                    <div class="col-md-3">
                        <h3>Summary</h3>
                        <button class="play">Play</button>
                        <button class="pause">Pause</button>

                        <button class="slow_down">Slow Down</button>
                        <button class="speed_up">Speed Up</button>
                        <table width="80%">
                            <tr>
                                <td>
                                    <strong>Location captured at:</strong>
                                </td>
                                <td align="right">
                                    <span style="font-weight: normal;" id="location_ts"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Total Tickets:</strong>
                                </td>
                                <td align="right">
                                    <span runat="server" style="font-weight: normal;" id="all_tkts"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Tickets with Location:</strong>
                                </td>
                                <td align="right">
                                    <span style="font-weight: normal;" id="tkts_with_location"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script type="application/javascript">
            //with jquery
            var van_speed = 10;
            $('.speed_up').on('click', function (e) {
                e.preventDefault();
                van_speed = van_speed + 5;
            });
            $('.slow_down').on('click', function (e) {
                e.preventDefault();
                if (van_speed > 5) {
                    van_speed = van_speed - 5;
                }
            });

            $('.pause').on('click', function (e) {
                e.preventDefault();
                isPaused = true;
            });

            $('.play').on('click', function (e) {
                e.preventDefault();
                isPaused = false;
            });
            var isPaused = false;

            function initMap() {
                const myLatLng = [<%= markers %>];
                const tickets = [<%= TicketInfo %>];
                const van_path = [<%= path_coordinates %>];
                const map = new google.maps.Map(document.getElementById("map"), {
                    zoom: 16,
                    center: myLatLng[0],
                });

                jQuery('#tkts_with_location').text(myLatLng.length);

                const lineSymbolCircle = {
                    path: 'M37.409,18.905l-4.946-7.924c-0.548-0.878-1.51-1.411-2.545-1.411H3c-1.657,0-3,1.343-3,3v16.86c0,1.657,1.343,3,3,3h0.787   c0.758,1.618,2.391,2.75,4.293,2.75s3.534-1.132,4.292-2.75h20.007c0.758,1.618,2.391,2.75,4.293,2.75   c1.9,0,3.534-1.132,4.291-2.75h0.787c1.656,0,3-1.343,3-3v-2.496C44.75,22.737,41.516,19.272,37.409,18.905z M8.087,32.395   c-1.084,0-1.963-0.879-1.963-1.963s0.879-1.964,1.963-1.964s1.963,0.88,1.963,1.964S9.171,32.395,8.087,32.395z M26.042,21.001   V15.57v-2.999h3.876l5.264,8.43H26.042z M36.671,32.395c-1.084,0-1.963-0.879-1.963-1.963s0.879-1.964,1.963-1.964   s1.963,0.88,1.963,1.964S37.755,32.395,36.671,32.395z',
                    scale: 1,
                    fillOpacity: 1,
                    size: new google.maps.Size(45, 45),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(22.5, 22.5),
                    strokeColor: "#ffb431",
                    fillColor: "#000000",
                    rotation: 270
                };

                var lineSymbolArrow = {
                    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                    scale: 3,
                    strokeColor: getRandomColor(),
                };

                const flightPath = new google.maps.Polyline({
                    path: van_path,
                    geodesic: true,
                    strokeColor: "#FF0000",
                    strokeOpacity: 1.0,
                    strokeWeight: 2,
                    icons: [
                        {
                            icon: lineSymbolArrow,
                            repeat: '100px',
                            offset: '100%'
                        },
                        {
                            icon: lineSymbolCircle,
                            offset: "100%",
                        },
                    ],
                });
                flightPath.setMap(map);

                var bounds = new google.maps.LatLngBounds();
                for (var i = 0; i < myLatLng.length; i++) {
                    bounds.extend(myLatLng[i]);
                    const myMark = new google.maps.Marker({
                        position: myLatLng[i],
                        map: map,
                        title: tickets[i].tktno + "\n" + tickets[i].tktstr,
                    });
                    const contentString =
                        '<div id="content">' +
                        '<div id="siteNotice"></div>' +
                        '<h1 id="firstHeading" class="firstHeading">Ticket # : ' + tickets[i].tktno + '</h1>' +
                        '<div id="bodyContent">' +
                        "<p><b>Store # : </b>" + tickets[i].tktstr +
                        "<p><b>Van # : </b>" + tickets[i].tktvan +
                        "<p><b>Customer : </b>" + tickets[i].tktcust +
                        "<p><b>Date : </b>" + tickets[i].tktdate +
                        "</div>" +
                        "</div>";
                    const infowindow = new google.maps.InfoWindow({
                        content: contentString,
                    });
                    myMark.addListener("click", function () {
                        infowindow.open(map, myMark);
                    });
                }
                map.fitBounds(bounds);

                if (van_path.length > 0) {
                    animateCircle(flightPath, van_path);
                }

                function animateCircle(line, allLatLng) {
                    let count = 0;
                    let crIdx = 0;
                    window.setInterval(function () {
                        if (!isPaused) {
                            count = (count + van_speed); // % allLatLng.length;
                            const icons = line.get("icons");
                            icons[1].offset = (count / allLatLng.length) + "%";
                            line.set("icons", icons);
                            crIdx = ((count / allLatLng.length) / 100) * allLatLng.length;
                            crIdx = parseInt(crIdx);

                            if (crIdx < allLatLng.length - 1) {
                                jQuery('#location_ts').text(allLatLng[crIdx].capAt);
                            }
                            if ((count / allLatLng.length) > 100) {
                                count = 0;
                                crIdx = 0;
                            }
                        }
                    }, 20);
                }
            }

            function getRandomColor() {
                var letters = '0123456789ABCDEF';
                var color = '#';
                for (var i = 0; i < 6; i++) {
                    color += letters[Math.floor(Math.random() * 16)];
                }
                return color;
            }
        </script>
</asp:Content>

