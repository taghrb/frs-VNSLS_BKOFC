﻿<%@ Page Title="Admin Panel - Route Map" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="RouteMap.aspx.cs" Inherits="Admin_Maps_RouteMap" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <style>
        #map-wrapper {
            width: 100%;
            height: 200px;
        }
    </style>
    <script src="https://npmcdn.com/cheap-ruler@1.3.0/cheap-ruler.js"></script>
    <script src='<%# ResolveUrl("~/js/geosimplify-js.js") %>' type="text/javascript"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJFjo4J5876rFLEOvI1ZuNCM1eb9lmqQY&callback=initMap&libraries=&v=weekly" defer></script>

    <script type="text/javascript">
        jQuery = jQuery.noConflict();
        $ = jQuery.noConflict();
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">
    <div class="formDiv">
        <div class="formHeaderDiv">
            <h3>Route Map</h3>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="fieldsetWrapper">
                        <fieldset>
                            <legend></legend>
                            <table style="margin: 0 auto;">
                                <tr>
                                    <td>Date From :&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        <div class="DivReq">
                                            <dx:ASPxDateEdit runat="server" ID="MapTstDateFrom" ClientInstanceName="MapTstDateFrom" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy" NullText="Today" NullTextStyle-ForeColor="Black" AutoPostBack="true"></dx:ASPxDateEdit>
                                        </div>
                                    </td>
                                    <td></td>

                                    <td>Date To :&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        <div class="DivReq">
                                            <dx:ASPxDateEdit runat="server" ID="MapTstDateTo" ClientInstanceName="MapTstDateTo" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy" NullText="Today" NullTextStyle-ForeColor="Black" AutoPostBack="true"></dx:ASPxDateEdit>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Branch :&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        <div class="DivReq">
                                            <dx:ASPxComboBox NullText="Select..." ID="MapTstStrNo" OnInit="MapTstStrNo_Init" ClientInstanceName="MapTstClientStrNo" runat="server" AnimationType="Auto" OnValueChanged="MapTstStrNo_ValueChanged" AutoPostBack="true">
                                                <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RequiredField IsRequired="true" ErrorText="Branch is Required." />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </div>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>Van # :&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        <div class="DivReq">
                                            <dx:ASPxComboBox NullText="Select..." ID="MapTstSlsRepNo" OnInit="MapTstSlsRepNo_Init" ClientInstanceName="MapTstClientSlsRepNo" runat="server" AnimationType="Auto" AutoPostBack="true">
                                                <ValidationSettings Display="Dynamic" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                    <ErrorFrameStyle Font-Size="Smaller" />
                                                    <RequiredField IsRequired="true" ErrorText="Van No. is Required." />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </fieldset>
                        <hr style="color: skyblue" />
                        <dx:ASPxLabel ID="lblErrorMessage" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div style="height: 700px;" id="map"></div>
                    </div>
                    <div class="col-md-3">
                        <h3>Summary</h3>
                        <button class="play">Play</button>
                        <button class="pause">Pause</button>

                        <button class="slow_down">Slow Down</button>
                        <button class="speed_up">Speed Up</button>
                        <table>
                            <tr>
                                <td>
                                    <strong>Location captured at:</strong>
                                </td>
                                <td>
                                    <span style="font-weight: normal;" id="location_ts"></span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script type="application/javascript">
            //with jquery
            var van_speed = 10;
            $('.speed_up').on('click', function (e) {
                e.preventDefault();
                van_speed = van_speed + 5;
            });
            $('.slow_down').on('click', function (e) {
                e.preventDefault();
                if (van_speed > 5) {
                    van_speed = van_speed - 5;
                }
            });

            $('.pause').on('click', function (e) {
                e.preventDefault();
                isPaused = true;
            });

            $('.play').on('click', function (e) {
                e.preventDefault();
                isPaused = false;
            });
            var isPaused = false;

            function initMap() {
                const pathLatLng = [<%= path_coordinates %>];
                if (pathLatLng.length <= 0) {
                    return;
                }

                var bounds = new google.maps.LatLngBounds();
                for (var i = 0; i < pathLatLng.length; i++) {
                    bounds.extend(pathLatLng[i]);
                }

                /*var simplePath = [];
                for (var i = 0; i < pathLatLng.length; i++) {
                    simplePath.push([pathLatLng[i].lat, pathLatLng[i].lng]);
                }
                const simpleLatLngTemp = simplify(simplePath, 0.1, 0.1);
                var simpleLatLng = [];
                for (var i = 0; i < simpleLatLngTemp.length; i++) {
                    simpleLatLng.push({ "lat": simpleLatLngTemp[i][0], "lng": simpleLatLngTemp[i][1] });
                }*/

                mapLatLng = pathLatLng;
                const map = new google.maps.Map(document.getElementById("map"), {
                    zoom: 16,
                    center: mapLatLng[0],
                });
                console.log("No of Markers: " + mapLatLng.length);
                /*new google.maps.Marker({
                    position: mapLatLng[0],
                    map: map,
                    title: "Start",
                });
                new google.maps.Marker({
                    position: mapLatLng[mapLatLng.length - 1],
                    map: map,
                    title: "End",
                });*/
                const lineSymbolCircle = {
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: 8,
                    fillOpacity: 1,
                    strokeColor: "#ffb431",
                };

                var lineSymbolArrow = {
                    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                    scale: 3,
                    fillOpacity: 1,
                    strokeColor: getRandomColor(),
                };

                var lineSymbolArrowMoving = {
                    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                    scale: 3,
                    fillOpacity: 1,
                    strokeColor: getRandomColor(),
                };

                var lineSymbolCar = {
                    path: 'M25.395,0H17.636c-3.117,0-5.643,3.467-5.643,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759' +
                'c3.116,0,5.644-2.527,5.644-5.644V6.584C35.037,3.467,32.511,0,29.395,0z M34.05,14.188v11.665l-2.729,0.351v-4.806L34.05,14.188z' +
                'M32.618,10.773c-1.016,3.9-2.219,8.51-2.219,8.51H16.631l-2.222-8.51C14.41,10.773,23.293,7.755,32.618,10.773z M15.741,21.713' +
                'v4.492l-2.73-0.349V14.502L15.741,21.713z M13.011,37.938V27.579l2.73,0.343v8.196L13.011,37.938z M14.568,40.882l2.218-3.336' +
                'h13.771l2.219,3.336H14.568z M31.321,35.805v-7.872l2.729-0.355v10.048L31.321,35.805z',
                    scale: 0.8,
                    fillOpacity: 1,
                    size: new google.maps.Size(20, 32),
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (0, 32).
                    anchor: new google.maps.Point(20, 32),
                    strokeColor: "#ffb431",
                };

                var lineSymbolVan = {
                    path: 'M37.409,18.905l-4.946-7.924c-0.548-0.878-1.51-1.411-2.545-1.411H3c-1.657,0-3,1.343-3,3v16.86c0,1.657,1.343,3,3,3h0.787   c0.758,1.618,2.391,2.75,4.293,2.75s3.534-1.132,4.292-2.75h20.007c0.758,1.618,2.391,2.75,4.293,2.75   c1.9,0,3.534-1.132,4.291-2.75h0.787c1.656,0,3-1.343,3-3v-2.496C44.75,22.737,41.516,19.272,37.409,18.905z M8.087,32.395   c-1.084,0-1.963-0.879-1.963-1.963s0.879-1.964,1.963-1.964s1.963,0.88,1.963,1.964S9.171,32.395,8.087,32.395z M26.042,21.001   V15.57v-2.999h3.876l5.264,8.43H26.042z M36.671,32.395c-1.084,0-1.963-0.879-1.963-1.963s0.879-1.964,1.963-1.964   s1.963,0.88,1.963,1.964S37.755,32.395,36.671,32.395z',
                    scale: 1,
                    fillOpacity: 1,
                    size: new google.maps.Size(45, 45),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(22.5, 22.5),
                    strokeColor: "#ffb431",
                    fillColor: "#000000",
                    rotation: 270
                }

                const flightPath = new google.maps.Polyline({
                    path: mapLatLng,
                    geodesic: true,
                    strokeColor: "#FF0000",
                    strokeOpacity: 1.0,
                    strokeWeight: 2,
                    icons: [
                        {
                            icon: lineSymbolArrow,
                            repeat: '100px',
                            offset: '100%'
                        },
                        {
                            icon: lineSymbolCircle,
                            offset: "100%",
                        },
                        {
                            icon: lineSymbolVan,
                            offset: "100%",
                        },
                    ],
                });
                flightPath.setMap(map);

                if (mapLatLng.length > 0) {
                    // animateMarker(mapLatLng);
                    animateCircle(flightPath, mapLatLng);
                }

                //var myMark = new google.maps.Marker({
                //    position: mapLatLng[0],
                //    map: map,
                //    title: mapLatLng[0].lat + "\n" + mapLatLng[0].lng,
                //});
                //function animateMarker(allLatLng) {
                //    let count = 1;
                //    window.setInterval(function () {
                //        var pos = new google.maps.LatLng(allLatLng[count].lat, allLatLng[count].lng);
                //        myMark.setPosition(pos);
                //        myMark.setTitle(allLatLng[count].capAt);

                //        count = count + 1;
                //        if (count > allLatLng.length) {
                //            count = 0;
                //        }
                //    }, 20);
                //}

                function animateCircle(line, allLatLng) {
                    let count = 0;
                    let crIdx = 0;
                    window.setInterval(function () {
                        if (!isPaused) {
                            count = (count + van_speed); // % allLatLng.length;
                            const icons = line.get("icons");
                            icons[1].offset = (count / allLatLng.length) + "%";
                            icons[2].offset = (count / allLatLng.length) + "%";
                            line.set("icons", icons);
                            crIdx = ((count / allLatLng.length) / 100) * allLatLng.length;
                            crIdx = parseInt(crIdx);

                            if (crIdx < allLatLng.length - 1) {
                                jQuery('#location_ts').text(allLatLng[crIdx].capAt);
                            }
                            if ((count / allLatLng.length) > 100) {
                                count = 0;
                                crIdx = 0;
                            }
                        }
                    }, 20);
                }


                map.fitBounds(bounds);
            }

            function getRandomColor() {
                var letters = '0123456789ABCDEF';
                var color = '#';
                for (var i = 0; i < 6; i++) {
                    color += letters[Math.floor(Math.random() * 16)];
                }
                return color;
            }
        </script>
</asp:Content>

