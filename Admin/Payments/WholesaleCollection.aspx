﻿<%@ Page Title="Wholesale Collection" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="WholesaleCollection.aspx.cs" Inherits="Admin_Payments_WholesaleCollection" %>


<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <script type="text/javascript">

        function grid_customizationWindowCloseUp(s, e) {
            gvUOMClientInquery.ShowCustomizationWindow();
        }

        function JS_rbPayCodeValueChanged(s, e) {
            jQuery('#BankInfoDiv').hide();
            jQuery('#ChqInfoDiv').hide();
            if (s.GetValue() == "CHQ") {
                jQuery('#ChqInfoDiv').show();
            } else if (s.GetValue() == "BANK") {
                jQuery('#BankInfoDiv').show();
            }
        }
    </script>
    <style type="text/css">
        .fieldsetWrapper {
            padding: 10px;
            width: 50%;
            margin: 0 auto;
            background-color: #F7F5F1;
            margin-top: 5px;
            border-radius: 10px;
        }

        .txt-pay-info {
            display: inline-table;
        }
    </style>
</asp:Content>
<asp:Content ID="contentTopHere1" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
            <li>&nbsp;
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">

    <div class="formDiv" style="background-color: transparent;">
        <div class="formHeaderDiv">
            <h3>Wholesale Collection</h3>
        </div>
        <div class="fieldsetWrapper">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <fieldset>
                        <legend></legend>
                        <table>
                            <tr>
                                <td>Date:&nbsp;&nbsp;</td>
                                <td>
                                    <div class="DivReq">
                                        <dx:ASPxDateEdit ID="dtTmTicketDate" runat="server" Width="170px" TabIndex="1" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy">
                                            <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                <ErrorFrameStyle Font-Size="Smaller" />
                                                <RequiredField IsRequired="true" ErrorText="Ticket Number is Required" />
                                            </ValidationSettings>
                                        </dx:ASPxDateEdit>
                                    </div>
                                </td>
                                <td style="width: 100px;">&nbsp;</td>
                                <td>Receipt # :</td>
                                <td>
                                    <dx:ASPxTextBox ID="txtReceiptNo" runat="server" Width="170px" TabIndex="10" HorizontalAlign="Right">
                                        <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                            <ErrorFrameStyle Font-Size="Smaller" />
                                            <RegularExpression ValidationExpression="\d+" ErrorText="Invalid Receipt Number" />
                                            <RequiredField IsRequired="true" ErrorText="Receipt Number is Required" />
                                        </ValidationSettings>
                                        <MaskSettings Mask="<999999>" />
                                    </dx:ASPxTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Cust No.:&nbsp;&nbsp;</td>
                                <td>
                                    <div class="DivReq">
                                        <dx:ASPxGridLookup ID="lkUpEdtCust" runat="server" Width="170px" TabIndex="1" TextFormatString="{0}" SelectionMode="Single" DataSourceID="CustNoSqlDataSource1" KeyFieldName="CUS_NO" OnValueChanged="lkUpEdtCust_ValueChanged" AutoPostBack="true">
                                            <GridViewProperties>
                                                <Settings ShowFilterRow="true" ShowStatusBar="Visible" />
                                                <SettingsPager PageSize="10" />
                                            </GridViewProperties>
                                            <Columns>
                                                <dx:GridViewDataColumn Caption="Cust #" FieldName="CUS_NO" Width="20%" />
                                                <dx:GridViewDataColumn Caption="Name" FieldName="CUS_NAME" Width="30%" />
                                                <dx:GridViewDataColumn Caption="Address" FieldName="CUS_ADDR1" Visible="false" Width="30%" />
                                                <dx:GridViewDataColumn Caption="Catg" FieldName="CUS_CATG" Width="10%" />
                                                <dx:GridViewDataColumn Caption="S. Rep" FieldName="CUS_SLSRP" Width="10%" />
                                            </Columns>
                                            <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                <ErrorFrameStyle Font-Size="Smaller" />
                                                <RequiredField IsRequired="true" ErrorText="Cust number is Required" />
                                            </ValidationSettings>
                                        </dx:ASPxGridLookup>
                                        <asp:SqlDataSource ID="CustNoSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT CUS_NO, CUS_NAME, CUS_ADDR1, CUS_CATG, CUS_SLSRP FROM STP_MST_CSTMR ORDER BY CUS_NO"></asp:SqlDataSource>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                                <td>Name : </td>
                                <td>
                                    <small>
                                        <dx:ASPxLabel Text="" ID="lblCustName" runat="server" />
                                    </small>
                                </td>
                            </tr>
                            <tr>
                                <td>Pay Code :&nbsp;&nbsp;</td>
                                <td>
                                    <dx:ASPxRadioButtonList ClientSideEvents-Init="JS_rbPayCodeValueChanged" TabIndex="2" ClientSideEvents-ValueChanged="JS_rbPayCodeValueChanged" RepeatDirection="Horizontal" runat="server" ID="rbPayCode">
                                        <Items>
                                            <dx:ListEditItem Text="Cash" Value="CASH" Selected="true" />
                                            <dx:ListEditItem Text="Cheque" Value="CHQ" />
                                            <dx:ListEditItem Text="Bank" Value="BANK" />
                                        </Items>
                                    </dx:ASPxRadioButtonList>
                                </td>
                                <td>&nbsp;</td>
                                <td colspan="2">
                                    <div style="display: none;" id="ChqInfoDiv">
                                        <span>Cheque No. :</span>
                                        <dx:ASPxTextBox runat="server" ID="txtChequeNo" TabIndex="3" CssClass="txt-pay-info"></dx:ASPxTextBox>
                                    </div>
                                    <div style="display: none;" id="BankInfoDiv">
                                        <span>Transfer No. :</span>
                                        <dx:ASPxTextBox runat="server" ID="txtTransferNo" TabIndex="4" CssClass="txt-pay-info"></dx:ASPxTextBox>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Amt. to Collect:&nbsp;&nbsp;</td>
                                <td>
                                    <div class="DivReq">
                                        <dx:ASPxTextBox ID="txtTotalAmt" runat="server" Width="170px" TabIndex="5" HorizontalAlign="Right" OnTextChanged="txtTotalAmt_TextChanged" AutoPostBack="true">
                                            <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                <ErrorFrameStyle Font-Size="Smaller" />
                                                <RequiredField IsRequired="true" ErrorText="Amt. to Collect is Required" />
                                            </ValidationSettings>
                                            <MaskSettings Mask="<0..99999>.<00..99>" />
                                        </dx:ASPxTextBox>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                                <td>Discount : </td>
                                <td rowspan="2">
                                    <dx:ASPxTextBox ID="txtDscntFactr" runat="server" Width="170px" TabIndex="6" HorizontalAlign="Right" OnTextChanged="txtDscntFactr_TextChanged" AutoPostBack="true">
                                        <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                            <ErrorFrameStyle Font-Size="Smaller" />
                                            <RequiredField IsRequired="false" ErrorText="Discount is Required" />
                                        </ValidationSettings>
                                        <MaskSettings Mask="<0..99999>.<00..99>" />
                                    </dx:ASPxTextBox>
                                    <dx:ASPxRadioButtonList runat="server" ID="rbDscnt" RepeatDirection="Horizontal" OnValueChanged="DiscountCheckedChanged" AutoPostBack="true">
                                        <Items>
                                            <dx:ListEditItem Value="P" Text="Percent" Selected="true" />
                                            <dx:ListEditItem Value="A" Text="Amount" />
                                        </Items>
                                    </dx:ASPxRadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>Amt. Collected:&nbsp;&nbsp;</td>
                                <td>
                                    <div class="DivReq">
                                        <dx:ASPxTextBox ID="txtAmtPaid" runat="server" Width="170px" TabIndex="7" HorizontalAlign="Right" OnTextChanged="txtAmtPaid_TextChanged" AutoPostBack="true">
                                            <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                <ErrorFrameStyle Font-Size="Smaller" />
                                                <RequiredField IsRequired="true" ErrorText="Amt. Collected is Required" />
                                            </ValidationSettings>
                                            <MaskSettings Mask="<0..99999>.<00..99>" />
                                        </dx:ASPxTextBox>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Comments :</td>
                                <td colspan="4">
                                    <dx:ASPxMemo ID="memoComments" runat="server" Width="100%" TabIndex="8">
                                        <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                            <ErrorFrameStyle Font-Size="Smaller" />
                                            <RequiredField IsRequired="false" ErrorText="Amt. Collected is Required" />
                                        </ValidationSettings>
                                    </dx:ASPxMemo>
                                </td>
                            </tr>
                        </table>
                        <h3><u>Apply Tos:</u></h3>
                        <table>
                            <tr>
                                <td>Invoice Date:&nbsp;&nbsp;</td>
                                <td>
                                    <div class="DivReq">
                                        <dx:ASPxDateEdit ID="DTE_INVCDATE" runat="server" Width="170px" TabIndex="9" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy">
                                            <ValidationSettings ValidationGroup="vldGrpApplyTo" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                <ErrorFrameStyle Font-Size="Smaller" />
                                                <RequiredField IsRequired="true" ErrorText="Invoice Date is Required" />
                                            </ValidationSettings>
                                        </dx:ASPxDateEdit>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td rowspan="3" style="vertical-align: top;">
                                    <dx:ASPxGridView SettingsPager-Mode="ShowAllRecords" runat="server" ID="grdVwApplyTos">
                                        <Columns>
                                            <dx:GridViewDataDateColumn Caption="Date" FieldName="APLY2_INVCDT" VisibleIndex="0"></dx:GridViewDataDateColumn>
                                            <dx:GridViewDataTextColumn Caption="Invoice #" FieldName="APLY2_INVCNO" VisibleIndex="1"></dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Amount" FieldName="APLY2_INVCAMT" VisibleIndex="2"></dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td>Invoice No. :&nbsp;&nbsp;</td>
                                <td>
                                    <div class="DivReq">
                                        <dx:ASPxTextBox ID="TXT_INVCNO" runat="server" Width="170px" TabIndex="10" HorizontalAlign="Right">
                                            <ValidationSettings ValidationGroup="vldGrpApplyTo" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                <ErrorFrameStyle Font-Size="Smaller" />
                                                <RegularExpression ValidationExpression="\d+" ErrorText="Invalid Invoice Number" />
                                                <RequiredField IsRequired="true" ErrorText="Invoice Number is Required" />
                                            </ValidationSettings>
                                            <MaskSettings Mask="<999999999>" />
                                        </dx:ASPxTextBox>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                                <td>
                                    <dx:ASPxButton Text="Add >> " OnClick="btnAddApplyTo_Click" ValidationGroup="vldGrpApplyTo" runat="server" ID="btnAddApplyTo"></dx:ASPxButton>
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Amount:&nbsp;&nbsp;</td>
                                <td>
                                    <div class="DivReq">
                                        <dx:ASPxTextBox ID="TXT_INVCAMT" runat="server" Width="170px" TabIndex="11" HorizontalAlign="Right">
                                            <ValidationSettings ValidationGroup="vldGrpApplyTo" EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Bottom" SetFocusOnError="true">
                                                <ErrorFrameStyle Font-Size="Smaller" />
                                                <RequiredField IsRequired="true" ErrorText="Old Price is Required" />
                                            </ValidationSettings>
                                            <MaskSettings Mask="<0..99999>.<00..99>" />
                                        </dx:ASPxTextBox>
                                    </div>
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </fieldset>
                </ContentTemplate>
            </asp:UpdatePanel>
            <hr style="color: skyblue" />
            <dx:ASPxLabel ID="lblErrorMessage" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
            <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                <dx:ASPxButton ID="btnSave" runat="server" Text="Save" CssClass="btnPopup btn btn-success" CausesValidation="true" OnClick="btnSave_Click" />
                <dx:ASPxButton ID="btnPopUpClear" runat="server" Text="Clear" CssClass="btnPopup btn btn-warning" CausesValidation="false" AutoPostBack="false">
                    <ClientSideEvents Click="function(s, e) { CallBackCode.PerformCallback(CatClientEditCode.GetText()); }" />
                </dx:ASPxButton>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        window.onload = function () {
            $('form').on('click', '.btnPopupClose', function () {
                $('.overlay').hide();
            });
        };

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //if (prm != null) {
        prm.add_endRequest(function (sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.overlay').show();
            }
        });
        prm.add_endRequest(GridOnEndCallBack);
        //};

    </script>
</asp:Content>

