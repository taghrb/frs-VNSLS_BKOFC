﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Payments_PaymentReceipts : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT "
            + " PR_NO, PR_RCPDT, PR_RCPTM, PR_CUSNO, PR_CUSNM, "
            + " PR_STRNO, PR_REGNO, PR_DRWNO, PR_SREP, PR_AMTPD, "
            + " PR_PAYCD, PR_CHQNO, PR_APLY2, PR_CMNTS, PR_STATS, "
            + " PR_TOTAL, PR_DSTYP, PR_DSFCT, "
            + " PR_CRTBY, PR_CRTDT, PR_CRTTM, PR_GEOLA, PR_GEOLN "
            + " FROM CUS_PAY_RCPTS "
            + " ORDER BY PR_NO DESC ";
    }

    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        //gvInquery.SettingsPager.PageSize = Convert.ToInt32(ddlNoOfRecords.SelectedItem.Value);
        //gvInquery.DataBind();
    }

    protected void ACB_CodeCheck_Callback(object source, CallbackEventArgs e)
    {

    }

    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {
    }

    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            int RcptNo;
            if (int.TryParse(e.CommandArgs.CommandArgument.ToString(), out RcptNo)) // Code is sent via request
            {
                dbHlpr.ExecuteNonQuery("DELETE FROM [CUS_PAY_RCPTS] WHERE PR_NO = '" + RcptNo + "'");

                gvInquery.CancelEdit();
                gvInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid Order ID. Unable to delete Order.";
            }
        }
        else if (e.CommandArgs.CommandName.Equals("VoidReceipt"))
        {
            Int64 ReceiptNo;
            if (Int64.TryParse(e.CommandArgs.CommandArgument.ToString(), out ReceiptNo)) // Code is sent via request
            {
                dbHlpr.ExecuteNonQuery("UPDATE CUS_PAY_RCPTS SET PR_STATS = 'Voided' WHERE PR_NO = '" + ReceiptNo + "' ");

                gvInquery.CancelEdit();
                gvInquery.DataBind();
            }
        }
    }

    // Common Methods

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }

    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void gvInquery_CustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        //  if(e.Column.FieldName == "")
    }

    protected void btnPostRecord_Click(object sender, EventArgs e)
    {
        DateTime dtNow = DateTime.Now;
        ASPxGridView mygvInquery = gvInquery;
        List<object> mySelectedList = mygvInquery.GetSelectedFieldValues("PR_NO");
        //      int A =mySelectedList.Count();
        for (int i = 0; i < mySelectedList.Count; i++)
        {
            dbHlpr.ExecuteNonQuery("INSERT INTO CUS_HST_PYMNT ( "
                + " PR_NO, PR_RCPDT, PR_RCPTM, PR_CUSNO, PR_CUSNM, PR_STRNO, "
                + " PR_REGNO, PR_DRWNO, PR_SREP, PR_AMTPD, PR_PAYCD, PR_CHQNO, "
                + " PR_APLY2, PR_CMNTS, PR_CRTBY, PR_CRTDT, PR_CRTTM, PR_STATS, "
                + " PR_TOTAL, PR_DSTYP, PR_DSFCT, "
                + " PR_GEOLA, PR_GEOLN, "
                + " PR_UPLDT, PR_UPLTM, PR_BANK, PR_BTREF, "
                + " PR_PSTBY, PR_PSTDT, PR_PSTIM "
                + " ) SELECT "
                + " PR_NO, PR_RCPDT, PR_RCPTM, PR_CUSNO, PR_CUSNM, PR_STRNO, "
                + " PR_REGNO, PR_DRWNO, PR_SREP, PR_AMTPD, PR_PAYCD, PR_CHQNO, "
                + " PR_APLY2, PR_CMNTS, PR_CRTBY, PR_CRTDT, PR_CRTTM, PR_STATS, "
                + " PR_TOTAL, PR_DSTYP, PR_DSFCT, "
                + " PR_GEOLA, PR_GEOLN, "
                + " PR_UPLDT, PR_UPLTM, PR_BANK, PR_BTREF, "
                + " '" + Session["UserId"] + "', '" + dtNow.ToString() + "', '" + dtNow.ToString() + "' "
                + " FROM CUS_PAY_RCPTS "
                + " WHERE CUS_PAY_RCPTS.PR_NO = '" + mySelectedList[i].ToString() + "' ");

            dbHlpr.ExecuteNonQuery("INSERT INTO CUS_HST_APLY2 ( "
                + " PA_KEY, PA_KEY2, PA_PRNO, PA_SEQNO, PA_INVNO, "
                + " PA_INVDT, PA_AMT, PA_UPLDT, PA_UPLTM "
                + " ) SELECT "
                + " PA_KEY, PA_KEY2, PA_PRNO, PA_SEQNO, PA_INVNO, "
                + " PA_INVDT, PA_AMT, PA_UPLDT, PA_UPLTM "
                + " FROM CUS_PAY_APLY2 "
                + " WHERE CUS_PAY_APLY2.PA_PRNO = '" + mySelectedList[i].ToString() + "' ");


            ///////////////////Delete code for Header data from master table //////////////////////////////////
            dbHlpr.ExecuteNonQuery("DELETE FROM CUS_PAY_RCPTS WHERE PR_NO = '" + mySelectedList[i].ToString().Trim() + "' ");
            dbHlpr.ExecuteNonQuery("DELETE FROM CUS_PAY_APLY2 WHERE PA_PRNO = '" + mySelectedList[i].ToString().Trim() + "' ");
        }

        gvInquery.CancelEdit();
        gvInquery.DataBind();

    }//end method


}//END CLASS