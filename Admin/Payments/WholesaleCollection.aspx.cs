﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;

public partial class Admin_Payments_WholesaleCollection : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        dtTmTicketDate.Date = DateTime.Now;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ValidateForm())
        {
            DataTable DT_APPLYTO_INVOICES = GetApplyTosDatasource();
            try
            {
                double rcptTotalForHeader = Convert.ToDouble(txtAmtPaid.Text);
                rcptTotalForHeader = Math.Round(rcptTotalForHeader, 2, MidpointRounding.AwayFromZero);
                double rcptTotalForLines = Convert.ToDouble(DT_APPLYTO_INVOICES.Compute("SUM(APLY2_INVCAMT)", string.Empty));
                rcptTotalForLines = Math.Round(rcptTotalForLines, 2, MidpointRounding.AwayFromZero);
                if (rcptTotalForHeader != rcptTotalForLines && Math.Abs(rcptTotalForHeader - rcptTotalForLines) > 0.02)
                {
                    lblErrorMessage.Text = "Alert ! Amt. Collected and Apply To Amounts should match. Please verify and try again.";
                    return;
                }
            }
            catch (Exception ex)
            {
                throw ex;
                return;
            }

            DataRow SessionUserDetails = ((DataTable)Session["userObject"]).Rows[0];

            string rcptSlsRep = "WHLSL";
            string rcptDate = dtTmTicketDate.Value.ToString();
            string rcptTime = dtTmTicketDate.Value.ToString();
            string rcptStore = SessionUserDetails["USR_PRMBR"].ToString();
            string rcptRegNo = "WHLSL";
            string rcptDrwNo = "WHLSL";
            string createdBy = Session["UserId"].ToString();
            string custNo = lkUpEdtCust.Value.ToString();
            string custName = lblCustName.Text;
            string rcptAmtTotal = txtTotalAmt.Value.ToString().Replace(",", "");
            string rcptDscFactr = txtDscntFactr.Value.ToString().Replace(",", "");
            string rcptAmtPaid = txtAmtPaid.Value.ToString().Replace(",", "");
            string rcptComments = memoComments.Text;

            string rcptChqNo = "";
            string rcptBankName = "";
            string rcptTransferRefNo = "";
            string paycode = "";
            if (rbPayCode.Value == "CHQ")
            {
                paycode = "CHQ";
                rcptChqNo = txtChequeNo.Text;
            }
            else if (rbPayCode.Value == "CASH")
            {
                paycode = "CASH";
            }
            else if (rbPayCode.Value == "BANK")
            {
                paycode = "BANK";
                rcptBankName = "YTDEF"; //cmBxBankName.SelectedValue.ToString();
                rcptTransferRefNo = txtTransferNo.Text;
            }

            string dscType = "";
            if (rbDscnt.Value == "P")
            {
                dscType = "P";
            }
            else if (rbDscnt.Value == "A")
            {
                dscType = "A";
            }

            string receiptNo = rcptStore + rcptRegNo + txtReceiptNo.Text;

            DataTable existingRcptNo = dbHlpr.FetchData("SELECT * FROM CUS_PAY_RCPTS WHERE PR_NO = '" + receiptNo + "' ");
            if (existingRcptNo.Rows.Count > 0)
            {
                lblErrorMessage.Text = "Receipt number already exists.";
                return;
            }

            string rcptQry = "INSERT INTO CUS_PAY_RCPTS ( "
                + " PR_NO, PR_RCPDT, PR_RCPTM, PR_CUSNO, PR_CUSNM, PR_STRNO, PR_REGNO, "
                + " PR_DRWNO, PR_SREP, PR_AMTPD, PR_PAYCD, "
                + " PR_CHQNO, PR_BANK, PR_BTREF, "
                + " PR_CMNTS, PR_STATS, "
                + " PR_TOTAL, PR_DSTYP, PR_DSFCT, "
                + " PR_CRTBY, PR_CRTDT, PR_CRTTM "
                + " ) VALUES ( "
                + " '" + receiptNo + "', '" + rcptDate + "', '" + rcptTime + "', '" + custNo + "', '" + custName + "', '" + rcptStore + "', '" + rcptRegNo + "', "
                + " '" + rcptDrwNo + "', '" + rcptSlsRep + "', '" + rcptAmtPaid + "', '" + paycode + "', "
                + " '" + rcptChqNo + "', '" + rcptBankName + "', '" + rcptTransferRefNo + "', "
                + " '" + rcptComments + "', '', "
                + " '" + rcptAmtTotal + "', '" + dscType + "', '" + rcptDscFactr + "', "
                + " '" + createdBy + "', GETDATE(), GETDATE() "
                + " )";
            dbHlpr.ExecuteNonQuery(rcptQry);

            string applyToString = "";
            for (int i = 0; i < DT_APPLYTO_INVOICES.Rows.Count; i++)
            {
                string invcNo = DT_APPLYTO_INVOICES.Rows[i]["APLY2_INVCNO"].ToString();
                string invcDateStr;
                string invcDateFormatted;
                try
                {
                    invcDateStr = DT_APPLYTO_INVOICES.Rows[i]["APLY2_INVCDT"].ToString();
                    invcDateFormatted = Convert.ToDateTime(DT_APPLYTO_INVOICES.Rows[i]["APLY2_INVCDT"].ToString()).ToString("dd-MMM-yyyy");
                }
                catch (Exception ex)
                {
                    invcDateStr = DT_APPLYTO_INVOICES.Rows[i]["APLY2_INVCDT"].ToString();
                    invcDateFormatted = DT_APPLYTO_INVOICES.Rows[i]["APLY2_INVCDT"].ToString();
                }
                string invcAmt = DT_APPLYTO_INVOICES.Rows[i]["APLY2_INVCAMT"].ToString();
                string seqNo = (i + 1).ToString();
                string key = receiptNo + invcNo + seqNo;
                string key2 = receiptNo + seqNo;

                applyToString += invcNo + "/" + invcDateFormatted + ", ";

                string applyToQry = "INSERT INTO CUS_PAY_APLY2 ( "
                    + " PA_KEY, PA_KEY2, "
                    + " PA_SEQNO, PA_PRNO, "
                    + " PA_INVNO, PA_INVDT, PA_AMT "
                    + " ) VALUES ( "
                    + " '" + key + "', '" + key2 + "', "
                    + " '" + seqNo + "', '" + receiptNo + "', "
                    + " '" + invcNo + "', '" + Convert.ToDateTime(invcDateStr) + "', '" + invcAmt + "' "
                    + " )";
                dbHlpr.ExecuteNonQuery(applyToQry);
            }
            applyToString = applyToString.Trim(new char[] { ',', ' ' });
            if (applyToString.Length > 255)
            {
                applyToString = applyToString.Substring(0, 255);
            }
            dbHlpr.ExecuteNonQuery("UPDATE CUS_PAY_RCPTS SET PR_APLY2 = '" + applyToString + "' WHERE PR_NO = '" + receiptNo + "' ");


            // Update Credit Customer Balance
            //dbHlpr.ExecuteNonQuery(" UPDATE STP_MST_CSTMR SET CUS_BALNC = CUS_BALNC + " + Convert.ToDouble(rcptAmtTotal) + " WHERE CUS_NO = '" + custNo + "' ");

            lblErrorMessage.Text = "Receipt Completed Successfully.";
        }
    }
    protected void btnAddApplyTo_Click(object sender, EventArgs e)
    {
        if (DTE_INVCDATE.Value == null || DTE_INVCDATE.Value.ToString().Trim().Length <= 0)
        {
            lblErrorMessage.Text = "Please enter Invoice Date";
            return;
        }

        if (TXT_INVCNO.Text.Trim().Length <= 0)
        {
            lblErrorMessage.Text = "Please enter Invoice Number";
            return;
        }

        DataTable DT_APPLYTO_INVOICES = GetApplyTosDatasource();

        DateTime InvcDate = Convert.ToDateTime(((DateTime)DTE_INVCDATE.Value).ToString("dd-MMM-yyyy"));
        string InvcNo = TXT_INVCNO.Text;
        float InvAmt = float.Parse(TXT_INVCAMT.Text);

        for (int i = 0; i < DT_APPLYTO_INVOICES.Rows.Count; i++)
        {
            DateTime InvcDateExstng = Convert.ToDateTime(DT_APPLYTO_INVOICES.Rows[i]["APLY2_INVCDT"].ToString());
            string InvcNoExstng = DT_APPLYTO_INVOICES.Rows[i]["APLY2_INVCNO"].ToString();
            if (InvcDateExstng.Equals(InvcDate) && InvcNoExstng.Trim().ToLower() == InvcNo.Trim().ToLower())
            {
                lblErrorMessage.Text = "Invoice already exists";
                return;
            }
        }

        DataRow dtApplyToRow = DT_APPLYTO_INVOICES.NewRow();
        dtApplyToRow["APLY2_INVCDT"] = InvcDate;
        dtApplyToRow["APLY2_INVCNO"] = InvcNo;
        dtApplyToRow["APLY2_INVCAMT"] = InvAmt;
        DT_APPLYTO_INVOICES.Rows.Add(dtApplyToRow);

        DTE_INVCDATE.Value = null;
        TXT_INVCNO.Text = "";
        TXT_INVCAMT.Text = "";

        grdVwApplyTos.DataSource = DT_APPLYTO_INVOICES;
        grdVwApplyTos.DataBind();
    }

    private DataTable GetApplyTosDatasource()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("APLY2_INVCDT", typeof(DateTime));
        dt.Columns.Add("APLY2_INVCNO");
        dt.Columns.Add("APLY2_INVCAMT", typeof(double));

        for (int i = 0; i < grdVwApplyTos.VisibleRowCount; i++)
        {
            DataRow oldRw = dt.NewRow();

            oldRw["APLY2_INVCDT"] = grdVwApplyTos.GetRowValues(i, "APLY2_INVCDT");
            oldRw["APLY2_INVCNO"] = grdVwApplyTos.GetRowValues(i, "APLY2_INVCNO");
            oldRw["APLY2_INVCAMT"] = grdVwApplyTos.GetRowValues(i, "APLY2_INVCAMT");

            dt.Rows.Add(oldRw);
        }
        return dt;
    }

    protected void lkUpEdtCust_ValueChanged(object sender, EventArgs e)
    {
        lblCustName.Text = "";
        if (lkUpEdtCust.Value != null)
        {
            DataTable dtCustInfo = dbHlpr.FetchData("SELECT * FROM STP_MST_CSTMR WHERE CUS_NO = '" + lkUpEdtCust.Value + "'");
            if (dtCustInfo.Rows.Count > 0)
            {
                lblCustName.Text = dtCustInfo.Rows[0]["CUS_NAME"].ToString();
            }
        }
    }


    ///////
    /*private string GetReceiptNo()
    {
        DataTable dtRcptNo = dbHlpr.FetchData(" SELECT MAX(PR_NO) FROM CUS_PAY_RCPTS ");
        Int64 RCPTNO = 1;
        if (!dtRcptNo.Rows[0][0].ToString().Equals(DBNull.Value) && !dtRcptNo.Rows[0][0].ToString().Equals("0") && !dtRcptNo.Rows[0][0].ToString().Equals(""))
        {
            RCPTNO = Convert.ToInt64(dtRcptNo.Rows[0][0].ToString()) + 1;
        }

        if (RCPTNO <= 1)
        {
            DataTable dtNextRcptNo = dbHlpr.FetchData(" SELECT RCPTNO FROM SYS_NXT_NUMBR ");
            if (dtNextRcptNo.Rows.Count > 0 && !dtNextRcptNo.Rows[0][0].ToString().Equals(DBNull.Value) && !dtNextRcptNo.Rows[0][0].ToString().Equals("0") && !dtNextRcptNo.Rows[0][0].ToString().Equals(""))
            {
                RCPTNO = Convert.ToInt64(dtNextRcptNo.Rows[0][0].ToString()) + 1;
            }
        }

        return RCPTNO.ToString();
    }*/

    private bool ValidateForm()
    {
        if (txtAmtPaid.Text.Trim().Equals(""))
        {
            lblErrorMessage.Text = "Please enter Receipt Amount";
            return false;
        }

        double amtPaid;
        if (!Double.TryParse(txtAmtPaid.Text, out amtPaid) || amtPaid <= 0)
        {
            lblErrorMessage.Text = "Please enter a valid Receipt Amount.";
            return false;
        }

        if (grdVwApplyTos.VisibleRowCount <= 0)
        {
            lblErrorMessage.Text = "Please enter Apply To";
            return false;
        }

        if (rbPayCode.Value == "CHQ")
        {
            if (txtChequeNo.Text.Trim().Length <= 0)
            {
                lblErrorMessage.Text = "Please enter Cheque number";
                return false;
            }
        }
        if (rbPayCode.Value == "BANK")
        {
            /*if (cmBxBankName.SelectedIndex <= 0)
            {
                lblErrorMessage.Text = "Please Select Bank Name");
                return false;
            }*/

            if (txtTransferNo.Text.Trim().Length <= 0)
            {
                lblErrorMessage.Text = "Please enter Bank Transfer Reference number";
                return false;
            }
        }

        return true;
    }

    protected void DiscountCheckedChanged(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            UpdateLineAmountsAndTotals(sender);
        }
    }

    protected void txtDscntFactr_TextChanged(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            UpdateLineAmountsAndTotals(sender);
        }
    }

    protected void txtTotalAmt_TextChanged(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            UpdateLineAmountsAndTotals(sender);
        }
    }

    protected void txtAmtPaid_TextChanged(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            UpdateLineAmountsAndTotals(sender);
        }
    }

    private void UpdateLineAmountsAndTotals(object sender)
    {
        double DiscFactr = 0;
        double TotalAmt = 0;
        double CollectedAmt = 0;

        if (double.TryParse(txtTotalAmt.Text, out TotalAmt) && double.TryParse(txtDscntFactr.Text, out DiscFactr) && double.TryParse(txtAmtPaid.Text, out CollectedAmt))
        {
            if (sender.GetType() != typeof(ASPxRadioButtonList) && ((ASPxTextEdit)sender).ID.Equals(txtAmtPaid.ID))
            {
                if (rbDscnt.Value == "P")
                {
                    DiscFactr = 100 - ((CollectedAmt / TotalAmt) * 100);
                }
                else if (rbDscnt.Value == "A")
                {
                    DiscFactr = TotalAmt - CollectedAmt;
                }
            }
            else
            {
                if (rbDscnt.Value == "P")
                {
                    CollectedAmt = TotalAmt - (TotalAmt * (DiscFactr / 100));
                }
                else if (rbDscnt.Value == "A")
                {
                    CollectedAmt = TotalAmt - DiscFactr;
                }
            }
        }

        try
        {
            if (!((ASPxTextBox)sender).ID.Equals(txtTotalAmt.ID))
                txtTotalAmt.Text = String.Format("{0:n}", TotalAmt);

            if (!((ASPxTextBox)sender).ID.Equals(txtDscntFactr.ID))
                txtDscntFactr.Text = String.Format("{0:n}", DiscFactr);

            if (!((ASPxTextBox)sender).ID.Equals(txtAmtPaid.ID))
                txtAmtPaid.Text = String.Format("{0:n}", CollectedAmt);
        }
        catch (Exception ex)
        {
            txtTotalAmt.Text = String.Format("{0:n}", TotalAmt);
            txtAmtPaid.Text = String.Format("{0:n}", CollectedAmt);
        }
    }
}