﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Users_Default : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT [DFG_KEY] ,[DFG_MAC] ,[DFG_MACAL] ,[DFG_DEVID] ,[DFG_USRID]  "
            + "  ,[DFG_ROUTE] ,[DFG_SLSRP] ,[DFG_RGSTR] ,[DFG_DRWER], DFG_STRNO, DFG_PRLST "
            + "  FROM [STP_MSTR_DCONFG] ";
    }

    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvInquery_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string MACCode = e.Keys["DFG_MAC"].ToString();
        string userCode = e.Keys["DFG_USRID"].ToString();
        string DevCode = e.Keys["DFG_DEVID"].ToString();
        string SLSRepCode = e.Keys["DFG_SLSRP"].ToString();

        dbHlpr.ExecuteNonQuery("DELETE FROM [STP_MSTR_DCONFG] WHERE DFG_MAC = '" + MACCode + "'    AND DFG_DEVID = '" + DevCode + "' AND DFG_SLSRP = '" + SLSRepCode + "' AND DFG_USRID = '" + userCode + "'");

        e.Cancel = true;
        gvInquery.CancelEdit();
    }//end method

    protected void ACB_CodeCheck_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (e.Parameter.Length > 0)
        {
            DataTable myDT = new DataTable();
            myDT = dbHlpr.FetchData("SELECT * FROM [STP_MSTR_DCONFG] WHERE DFG_MAC = '" + e.Parameter.ToString() + "'");
            JavaScriptSerializer myserializer = new JavaScriptSerializer();
            if (myDT.Rows.Count > 0)
            {
                e.Result = myserializer.Serialize(myDT.Rows[0].ItemArray);
            }
            else
            {
                e.Result = myserializer.Serialize("");
            }
        }
    }//end method

    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxComboBox cmbxMacAddress = (((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxMacAddress")));
        ASPxComboBox cmbxRouteID = (((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxRouteID")));
        ASPxComboBox cmbxSalesPerson = (((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxSalesPerson")));
        ASPxComboBox cmbxDeviceID = (((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxDeviceID")));
        ASPxComboBox cmbxRegisterID = (((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxRegisterID")));
        ASPxComboBox cmbxUserID = (((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxUserID")));
        ASPxComboBox cmbxDrawerID = (((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxDrawerID")));
        ASPxComboBox cmbxStrNo = (((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxStrNo")));
        ASPxComboBox cmbxPriceList = (((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxPriceList")));
        ASPxTextBox txtMACallotment = (((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtMACallotment")));

        string QMAC = cmbxMacAddress.Value.ToString();
        string QRoute = cmbxRouteID.Value == null ? "" : cmbxRouteID.Value.ToString();
        string QSlsPR = cmbxSalesPerson.Value.ToString();
        string QDevice = cmbxDeviceID.Value == null ? "" : cmbxDeviceID.Value.ToString();
        string QRegister = cmbxRegisterID.Value.ToString();
        string QUser = cmbxUserID.Value.ToString();
        string QDrawer = cmbxDrawerID.Value.ToString();
        string QStore = cmbxStrNo.Value.ToString();
        string QPriceList = cmbxPriceList.Value.ToString();
        string QMacAllotment = txtMACallotment.Text;

        DataTable myDT = new DataTable();

        myDT = dbHlpr.FetchData("SELECT * FROM [STP_MSTR_DCONFG] WHERE DFG_MAC = '" + QMAC + "'  AND DFG_DEVID = '" + QDevice + "' AND DFG_SLSRP = '" + QSlsPR + "' AND DFG_USRID = '" + QUser + "'");
        if (myDT.Rows.Count <= 0)
        {
            dbHlpr.ExecuteNonQuery("INSERT INTO [STP_MSTR_DCONFG] ("
                + " [DFG_MAC] ,[DFG_MACAL] ,[DFG_DEVID] ,[DFG_USRID] ,[DFG_ROUTE] ,[DFG_SLSRP] ,[DFG_RGSTR] ,[DFG_DRWER], DFG_STRNO, DFG_PRLST) "
                + " VALUES ('" + QMAC + "','" + QMacAllotment + "', '" + QDevice + "', '" + QUser + "', '" + QRoute + "', '" + QSlsPR + "', '" + QRegister + "', '" + QDrawer + "', '" + QStore + "', '" + QPriceList + "' )");
        }
        else
        {
            dbHlpr.ExecuteNonQuery("UPDATE [STP_MSTR_DCONFG] SET "
                //  + " DFG_MAC   = '" + QMAC + "', "
                + " DFG_MACAL = '" + QMacAllotment + "', "
                //    + " DFG_DEVID = '" + QDevice + "', "
                //     + " DFG_USRID = '" + QUser + "', "
                + " DFG_ROUTE = '" + QRoute + "', "
                //    + " DFG_SLSRP = '" + QSlsPR + "', "
                + " DFG_RGSTR = '" + QRegister + "', "
                + " DFG_DRWER = '" + QDrawer + "', "
                + " DFG_STRNO = '" + QStore + "', "
                + " DFG_PRLST = '" + QPriceList + "' "
                + " WHERE DFG_MAC = '" + QMAC + "'   AND DFG_DEVID = '" + QDevice + "' AND DFG_SLSRP = '" + QSlsPR + "' AND DFG_USRID = '" + QUser + "'");
        }
        gvInquery.CancelEdit();
        gvInquery.DataBind();
    }//end method

    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

        ////html export here
        //gridExport.ExportHtmlToResponse();
        //string str;
        //MemoryStream ms = new MemoryStream();
        //try
        //{
        //    gridExport.ExportToHtml(ms);
        //    ms.Seek(0, SeekOrigin.Begin);
        //    StreamReader sr = new StreamReader(ms);
        //    str = sr.ReadToEnd();
        //}
        //finally
        //{
        //    ms.Close();
        //}
    }//end method

    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            string userId = e.CommandArgs.CommandArgument.ToString().Trim();
            string userId2 = e.CommandArgs.CommandArgument.ToString().Trim();
            string userId3 = e.CommandArgs.CommandArgument.ToString().Trim();
            string userId4 = e.CommandArgs.CommandArgument.ToString().Trim();

            if (userId.Length > 0) // Code is sent via request
            {
                dbHlpr.ExecuteNonQuery("DELETE FROM [STP_MSTR_DCONFG] WHERE DFG_MAC = '" + userId + "' AND DFG_DEVID = '" + userId2 + "' AND DFG_SLSRP = '" + userId3 + "' AND DFG_USRID = '" + userId4 + "' ");
                //DFG_MAC;DFG_DEVID;DFG_SLSRP;DFG_USRID
                gvInquery.CancelEdit();
                gvInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid User ID. Unable to delete User.";
            }
        }
    }//end method

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }//end method
    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }//end method
    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }//end method

    private string EditMACAddress_SLCTD = string.Empty;
    private string EditRouteID_SLCTD = string.Empty;
    private string EditSalesPerson_SLCTD = string.Empty;
    private string EditDeviceID_SLCTD = string.Empty;
    private string EditRegisterID_SLCTD = string.Empty;
    private string EditUserID_SLCTD = string.Empty;
    private string EditDrawerID_SLCTD = string.Empty;
    private string EditStoreNo_SLCTD = string.Empty;
    private string EditPriceList_SLCTD = string.Empty;

    protected void gvInquery_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        ASPxGridView gv = (ASPxGridView)sender;
        EditMACAddress_SLCTD = gv.GetRowValues(gv.EditingRowVisibleIndex, "DFG_MAC").ToString();
        EditRouteID_SLCTD = gv.GetRowValues(gv.EditingRowVisibleIndex, "DFG_ROUTE").ToString();
        EditSalesPerson_SLCTD = gv.GetRowValues(gv.EditingRowVisibleIndex, "DFG_SLSRP").ToString();
        EditDeviceID_SLCTD = gv.GetRowValues(gv.EditingRowVisibleIndex, "DFG_DEVID").ToString();
        EditRegisterID_SLCTD = gv.GetRowValues(gv.EditingRowVisibleIndex, "DFG_RGSTR").ToString();
        EditUserID_SLCTD = gv.GetRowValues(gv.EditingRowVisibleIndex, "DFG_USRID").ToString();
        EditDrawerID_SLCTD = gv.GetRowValues(gv.EditingRowVisibleIndex, "DFG_DRWER").ToString();
        EditStoreNo_SLCTD = gv.GetRowValues(gv.EditingRowVisibleIndex, "DFG_STRNO").ToString();
        EditPriceList_SLCTD = gv.GetRowValues(gv.EditingRowVisibleIndex, "DFG_PRLST").ToString();

        //[DFG_KEY] ,[DFG_MAC] ,[DFG_MACAL] ,[DFG_DEVID] ,[DFG_USRID]   
        //    + "  ,[DFG_ROUTE] ,[DFG_SLSRP] ,[DFG_RGSTR] ,[DFG_DRWER]

    }//end method

    protected void cmbxRouteID_Init(object sender, EventArgs e)
    {
        // Get Categories for current active division
        ASPxComboBox cbBrnch = (ASPxComboBox)sender;

        cbBrnch.DataSource = dbHlpr.FetchData("SELECT  [RUT_CODE] ,[RUT_NAME] ,[RUT_CMNTS]  FROM [STP_MSTR_ROUTE]");
        cbBrnch.ValueField = "RUT_CODE";
        cbBrnch.TextField = "RUT_NAME";
        cbBrnch.DataBind();
        cbBrnch.Value = EditRouteID_SLCTD;
    }//end method

    protected void cmbxSalesPerson_Init(object sender, EventArgs e)
    {
        if (!gvInquery.IsNewRowEditing)
        {
            ASPxComboBox txtSLRep = (ASPxComboBox)sender;
            txtSLRep.ReadOnly = true;
        }
        else
        {
            ASPxComboBox txtSLRep = (ASPxComboBox)sender;
            txtSLRep.ReadOnly = false;
        }
        // Get Categories for current active division
        ASPxComboBox cbBrnch = (ASPxComboBox)sender;

        cbBrnch.DataSource = dbHlpr.FetchData("SELECT  [SRP_CODE] ,[SRP_NAME] ,[SRP_CMNTS]  FROM [STP_MSTR_SLREP]");
        cbBrnch.ValueField = "SRP_CODE";
        cbBrnch.TextField = "SRP_NAME";
        cbBrnch.DataBind();
        cbBrnch.Value = EditSalesPerson_SLCTD;
    }//end method

    protected void cmbxDeviceID_Init(object sender, EventArgs e)
    {
        if (!gvInquery.IsNewRowEditing)
        {
            ASPxComboBox txtDevice = (ASPxComboBox)sender;
            txtDevice.ReadOnly = true;
        }
        else
        {
            ASPxComboBox txtDevice = (ASPxComboBox)sender;
            txtDevice.ReadOnly = false;
        }
        // Get Categories for current active division
        ASPxComboBox cbBrnch = (ASPxComboBox)sender;

        cbBrnch.DataSource = dbHlpr.FetchData("SELECT [DEV_CODE] ,[DEV_NAME] ,[DEV_CMNTS] FROM [STP_MSTR_DEVIC]");
        cbBrnch.ValueField = "DEV_CODE";
        cbBrnch.TextField = "DEV_NAME";
        cbBrnch.DataBind();
        cbBrnch.Value = EditDeviceID_SLCTD;
    }//end method

    protected void cmbxRegisterID_Init(object sender, EventArgs e)
    {
        // Get Categories for current active division
        ASPxComboBox cbBrnch = (ASPxComboBox)sender;

        cbBrnch.DataSource = dbHlpr.FetchData("SELECT [REG_CODE] ,[REG_NAME] ,[REG_CMNTS] FROM [STP_MSTR_RGSTR] WHERE REG_STRNO IN (" + Session["userBranchCode"].ToString() + ") ");
        cbBrnch.ValueField = "REG_CODE";
        cbBrnch.TextField = "REG_NAME";
        cbBrnch.DataBind();
        cbBrnch.Value = EditRegisterID_SLCTD;
    }//end method

    protected void cmbxMACAddress_Init(object sender, EventArgs e)
    {
        if (!gvInquery.IsNewRowEditing)
        {
            ASPxComboBox txtCode = (ASPxComboBox)sender;
            txtCode.ReadOnly = true;
        }
        else
        {
            ASPxComboBox txtCode1 = (ASPxComboBox)sender;
            txtCode1.ReadOnly = false;
        }
        // Get Categories for current active division
        ASPxComboBox cbBrnch = (ASPxComboBox)sender;

        cbBrnch.DataSource = dbHlpr.FetchData("SELECT  [MAC_ADDRS],[MAC_DESC1],[MAC_CMNTS] FROM [STP_MSTR_MAC]");
        cbBrnch.ValueField = "MAC_ADDRS";
        cbBrnch.TextField = "MAC_ADDRS";
        cbBrnch.DataBind();
        cbBrnch.Value = EditMACAddress_SLCTD;

    }//end method

    protected void cmbxUserID_Init(object sender, EventArgs e)
    {
        if (!gvInquery.IsNewRowEditing)
        {
            ASPxComboBox txtUsr1 = (ASPxComboBox)sender;
            txtUsr1.ReadOnly = true;
        }
        else
        {
            ASPxComboBox txtUsr1 = (ASPxComboBox)sender;
            txtUsr1.ReadOnly = false;
        }
        // Get Categories for current active division
        ASPxComboBox cbBrnch = (ASPxComboBox)sender;

        cbBrnch.DataSource = dbHlpr.FetchData("SELECT  [USR_ID],[USR_NAME],[USR_INITL] FROM [STP_MSTR_USERS] WHERE USR_VSACS = '1' ");
        cbBrnch.ValueField = "USR_ID";
        cbBrnch.TextField = "USR_NAME";
        cbBrnch.DataBind();
        cbBrnch.Value = EditUserID_SLCTD;

    }//end method

    protected void cmbxPriceList_Init(object sender, EventArgs e)
    {
        // Get Categories for current active division
        ASPxComboBox cbBrnch = (ASPxComboBox)sender;

        cbBrnch.DataSource = dbHlpr.FetchData("SELECT PRLST_KEY, PRLST_NAME FROM SYS_STP_PRLST ");
        cbBrnch.ValueField = "PRLST_KEY";
        cbBrnch.TextField = "PRLST_NAME";
        cbBrnch.DataBind();
        cbBrnch.Value = EditPriceList_SLCTD;
    }//end method

    protected void cmbxDrawerID_Init(object sender, EventArgs e)
    {
        // Get Categories for current active division
        ASPxComboBox cbBrnch = (ASPxComboBox)sender;

        cbBrnch.DataSource = dbHlpr.FetchData("SELECT  [DWR_CODE],[DWR_NAME],[DWR_CMNTS] FROM [STP_MSTR_DRWAR]");
        cbBrnch.ValueField = "DWR_CODE";
        cbBrnch.TextField = "DWR_NAME";
        cbBrnch.DataBind();
        cbBrnch.Value = EditDrawerID_SLCTD;

    }//end method

    protected void cmbxStrNo_Init(object sender, EventArgs e)
    {
        // Get Categories for current active division
        ASPxComboBox cbStrNo = (ASPxComboBox)sender;

        cbStrNo.DataSource = dbHlpr.FetchData("SELECT STM_CODE, STM_NAME, STM_ADRS1, STM_CNCT1, STM_PHON1 FROM STP_MSTR_STORE WHERE STM_CODE IN (" + Session["userBranchCode"].ToString() + ") ");
        cbStrNo.ValueField = "STM_CODE";
        cbStrNo.TextField = "STM_NAME";
        cbStrNo.DataBind();
        cbStrNo.Value = EditStoreNo_SLCTD;
    }//end method

    protected void cmBxSecBrnch_Init(object sender, EventArgs e)
    {
        //ASPxDropDownEdit cbSecBrnch = (ASPxDropDownEdit)sender;
        //ASPxListBox lstBxSecBrnch = (ASPxListBox)cbSecBrnch.FindControl("lstBxSecBrnch"); 
        //DataTable dtBrnchs = dbHlpr.FetchData("SELECT  [RUT_CODE] ,[RUT_NAME] ,[RUT_CMNTS]  FROM [STP_MSTR_ROUTE]"); 
        //DataRow dr = dtBrnchs.NewRow();
        //dr["BRN_NAME"] = "All";
        //dr["BRN_CODE"] = "0";
        //dtBrnchs.Rows.InsertAt(dr, 0);

        //lstBxSecBrnch.DataSource = dtBrnchs;
        //lstBxSecBrnch.ValueField = "BRN_CODE";
        //lstBxSecBrnch.TextField = "BRN_NAME";
        //lstBxSecBrnch.DataBind();
        //cbSecBrnch.Value = EditUserSecBranch_SLCTD;
    }//end method




}//end class