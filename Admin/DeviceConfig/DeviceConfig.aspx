﻿<%@ Page Title="Device Config" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="DeviceConfig.aspx.cs" Inherits="Admin_Users_Default" %>

<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content4" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <script type="text/javascript">

        function grid_customizationWindowCloseUp(s, e) {
            //if(FromGridView.IsCustomizationWindowVisible())
            FromGridView.ShowCustomizationWindow();
        }

        $(function () {
            $('.testing123').dialog();
        });


        // Code to create a custom multiselect ComboBox, Dropdown
        var textSeparator = ","; // Between each checked item in ComboBox
        function OnListBoxSelectionChanged(listBox, args) {
            if (args.index == 0)
                args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
            UpdateSelectAllItemState();
            UpdateText();
        }
        function UpdateSelectAllItemState() {
            IsAllSelected() ? checkListBox.SelectIndices([0]) : checkListBox.UnselectIndices([0]);
        }
        function IsAllSelected() {
            var selectedDataItemCount = checkListBox.GetItemCount() - (checkListBox.GetItem(0).selected ? 0 : 1);
            return checkListBox.GetSelectedItems().length == selectedDataItemCount;
        }
        function UpdateText() {
            var selectedItems = checkListBox.GetSelectedItems();
            checkComboBox.SetText(GetSelectedItemsText(selectedItems));
        }
        function SynchronizeListBoxValues(dropDown, args) {
            checkListBox.UnselectAll();
            var texts = dropDown.GetText().split(textSeparator);
            //var values = GetValuesByTexts(texts);
            checkListBox.SelectValues(texts);
            UpdateSelectAllItemState();
            UpdateText(); // for remove non-existing texts
        }
        function GetSelectedItemsText(items) {
            var texts = [];
            for (var i = 0; i < items.length; i++)
                if (items[i].index != 0)
                    texts.push(items[i].value);
            return texts.join(textSeparator);
        }
        function GetValuesByTexts(texts) {
            var actualValues = [];
            var item;
            for (var i = 0; i < texts.length; i++) {
                item = checkListBox.FindItemByText(texts[i]);
                if (item != null)
                    actualValues.push(item.value);
            }
            return actualValues;
        }


    </script>

</asp:Content>
<asp:Content ID="contentTopHere1" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
            <li>
                <asp:LinkButton ID="LinkBtnPDF" OnClick="LinkBtnPDF_Click" runat="server">
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonXLX" OnClick="LinkButtonXLX_Click" runat="server">
                <img src="~/img/FilesIcons/excel.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonXxls" OnClick="LinkButtonXxls_Click" runat="server">
                <img src="~/img/FilesIcons/xlsx.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonRtf" OnClick="LinkButtonRtf_Click" runat="server">
                <img src="~/img/FilesIcons/rtf.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonCvs" OnClick="LinkButtonCvs_Click" runat="server">
                <img src="~/img/FilesIcons/cvs.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButtonWord" OnClick="LinkButtonRtf_Click" runat="server">
                <img src="~/img/FilesIcons/word.png" runat="server" />
                </asp:LinkButton>
            </li>

            <li>
                <asp:LinkButton ID="LinkButtonHtml" OnClick="LinkButtonHtml_Click" runat="server">
                <img src="~/img/icons/searchtop.jpg" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton6" OnClick="LinkButton3_Click" runat="server">               
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton7" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/word.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton8" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/excel.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton9" OnClick="LinkButton3_Click" OnClientClick="window.print();" runat="server">
                <img src="~/img/FilesIcons/acrobat.png" runat="server" />
                </asp:LinkButton>
            </li>
            <li>
                <asp:LinkButton ID="LinkButton10" OnClick="LinkButton3_Click" runat="server">
                <img src="~/img/FilesIcons/torrent.png" runat="server" />
                </asp:LinkButton>
            </li>
        </ul>
        <dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="gvInquery" ExportedRowType="All" FileName="sdsdd">
        </dx:ASPxGridViewExporter>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">

    <div class="formDiv"> 
        <div class="formHeaderDiv">
            <h3>Device Configurations</h3>
        </div>
        <asp:SqlDataSource runat="server" ID="DxGridSqlDataSource1"
            ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
        <dx:ASPxGridView ID="gvInquery" CssClass="FromGridView" ClientInstanceName="FromGridView" Theme="Office2010Black" runat="server" AutoGenerateColumns="False" KeyFieldName="DFG_MAC;DFG_DEVID;DFG_SLSRP;DFG_USRID"
            DataSourceID="DxGridSqlDataSource1" OnCustomButtonCallback="gvInquery_CustomButtonCallback"
            OnRowCommand="gvInquery_RowCommand"
            OnStartRowEditing="gvInquery_StartRowEditing"
            OnDataBound="gvInquery_DataBound"
            OnBeforeGetCallbackResult="gvInquery_BeforeGetCallbackResult"
            OnRowDeleting="gvInquery_RowDeleting">

            <SettingsContextMenu Enabled="True" EnableColumnMenu="True" EnableRowMenu="True"></SettingsContextMenu>
            <SettingsSearchPanel Visible="False" CustomEditorID="CustomSearchPanelTxtBx" />
            <Columns>
                <dx:GridViewCommandColumn ShowNewButtonInHeader="false" ShowSelectButton="true" Width="80" Caption=" " AllowDragDrop="False" VisibleIndex="1"></dx:GridViewCommandColumn>
                <dx:GridViewCommandColumn ShowNewButtonInHeader="true" ShowEditButton="true" Width="80" VisibleIndex="2" AllowDragDrop="False"></dx:GridViewCommandColumn>
                <dx:GridViewCommandColumn ShowNewButtonInHeader="false" ShowDeleteButton="true" Width="80" Caption=" " AllowDragDrop="False" VisibleIndex="3"></dx:GridViewCommandColumn>

                <dx:GridViewDataTextColumn FieldName="DFG_KEY" Caption="Key" Width="50"  VisibleIndex="4">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="DFG_MAC" Caption="MAC Address"  Width="150" VisibleIndex="5">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="DFG_MACAL" Caption="MAC Allocation" Width="120" VisibleIndex="6">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="DFG_DEVID" Caption="Device ID" VisibleIndex="7">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <%--<dx:GridViewDataTextColumn FieldName="DFG_USRID" Caption="User ID" VisibleIndex="10">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                    <DataItemTemplate>
                        <dx:ASPxLabel runat="server" Text='<%# Eval("DFG_USRID").ToString().Equals("1") ? "Allowed" : "Not Allowed" %>'>
                        </dx:ASPxLabel>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>--%>
                <dx:GridViewDataTextColumn FieldName="DFG_USRID" Caption="User ID" VisibleIndex="8">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="DFG_PRLST" Caption="Price List" VisibleIndex="9">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="DFG_ROUTE" Caption="Route" VisibleIndex="10">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="DFG_SLSRP" Caption="Sales person" VisibleIndex="11" Visible="true">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="DFG_RGSTR" Caption="Register" VisibleIndex="12">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="DFG_DRWER" Caption="Drawer" VisibleIndex="13" Visible="true">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="DFG_STRNO" Caption="Store" VisibleIndex="14" Visible="true">
                    <Settings AllowHeaderFilter="True" AllowAutoFilter="True" HeaderFilterMode="CheckedList" />
                </dx:GridViewDataTextColumn>
            </Columns>

            <Settings HorizontalScrollBarMode="Auto" />

            <SettingsCommandButton>
                <NewButton ButtonType="Button" Text="New" Styles-Style-CssClass="btna btn btn-primary  btn-sm">
                    <Image runat="server" Url="~/img/icons/white-icn/Add.png" Width="15px"></Image>
                </NewButton>

                <SelectButton ButtonType="Button" Text="View" Styles-Style-CssClass="btna btn btn-success  btn-sm">
                    <Image ToolTip="View button here" Url="~/img/icons/white-icn/eyee1.png" Width="15px" />
                </SelectButton>

                <EditButton ButtonType="Button" Text="Edit" Styles-Style-CssClass="btna btn btn-info  btn-sm">
                    <Image runat="server" Url="~/img/icons/white-icn/Pen Tool.png" Width="15px"></Image>
                </EditButton>

                <DeleteButton ButtonType="Button" Text="Delete" Styles-Style-CssClass="btna btn btn-danger btn-sm btn-danger-variant">
                    <Image runat="server" Url="~/img/icons/white-icn/Trash.png" Width="15px"></Image>
                </DeleteButton>

                <CancelButton ButtonType="Button" Text="Close" Styles-Style-CssClass="btnPopup btn btn-primary">
                </CancelButton>
                <UpdateButton ButtonType="Button" Text="Update">
                </UpdateButton>
            </SettingsCommandButton>
            <SettingsEditing EditFormColumnCount="1" />

            <SettingsPopup>
                <EditForm VerticalAlign="WindowCenter" AllowResize="True" Modal="True" HorizontalAlign="WindowCenter"></EditForm>
            </SettingsPopup>

            <SettingsPager Position="Bottom">
                <PageSizeItemSettings Caption="Records per Page" Items="10, 20, 50, 100" ShowAllItem="true" Position="Right" Visible="true">
                </PageSizeItemSettings>
            </SettingsPager>
            <Styles>
                <CommandColumn Spacing="0px" Wrap="False" />
                <EditFormDisplayRow HorizontalAlign="Center" VerticalAlign="Middle"></EditFormDisplayRow>
                <AlternatingRow CssClass="alterRowGV" Enabled="True"></AlternatingRow>
                <Row CssClass="rowHeight"></Row>
            </Styles>
            <Settings ShowGroupPanel="true" VerticalScrollBarMode="Auto" />
            <SettingsLoadingPanel Mode="ShowOnStatusBar" />
            <SettingsBehavior EnableCustomizationWindow="true" AllowDragDrop="true" ConfirmDelete="True" ColumnResizeMode="Control" />
            <ClientSideEvents ColumnStartDragging="grid_customizationWindowCloseUp" EndCallback="GridOnEndCallBack" />

            <Templates>
                <EditForm>
                    <div class="overlay">
                        <div class="FormPopup">
                            <div class="formHeaderDiv">
                                <h3>Device Config</h3>
                            </div>
                            <fieldset>
                                <legend></legend>
                                <table>
                                    <tr>
                                        <td>MAC Address :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmbxMacAddress" OnInit="cmbxMACAddress_Init" ClientInstanceName="cmbxClientMacAddress" runat="server" Width="170px" ValueType="System.String" TabIndex="6">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="MAC Address is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                                <asp:SqlDataSource ID="cmbxMacAddressSqlDataSource1" runat="server" ConnectionString="<% $ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM STP_MSTR_MAC"></asp:SqlDataSource>
                                            </div>
                                        </td>
                                        <td>Route ID :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmbxRouteID" OnInit="cmbxRouteID_Init" ClientInstanceName="cmbxClientRouteID" runat="server" Width="170px" ValueType="System.String" TabIndex="7">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="false" ErrorText="Route is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>MAC Allotment: </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxTextBox ID="txtMACallotment" ClientInstanceName="txtClientMACallotment" runat="server" ReadOnly="true" Width="170px" Text='<%# Bind("DFG_MACAL") %>' TabIndex="8" MaxLength="250">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="false" ErrorText="MAC Allotment is Required" />
                                                    </ValidationSettings>
                                                </dx:ASPxTextBox>
                                            </div>
                                        </td>
                                        <td>Sales Person :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmbxSalesPerson" OnInit="cmbxSalesPerson_Init" ClientInstanceName="cmbxClientSalesPerson" runat="server" Width="170px" ValueType="System.String" TabIndex="9">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Sales Person is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Device ID :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmbxDeviceID" OnInit="cmbxDeviceID_Init" ClientInstanceName="cmbxClientDeviceID" runat="server" Width="170px" ValueType="System.String" TabIndex="10">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="false" ErrorText="Device ID is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>
                                        <td>Register  :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmbxRegisterID" OnInit="cmbxRegisterID_Init" ClientInstanceName="cmbxClientRegisterID" runat="server" Width="170px" ValueType="System.String" TabIndex="11">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Register is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>User ID :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmbxUserID" OnInit="cmbxUserID_Init" ClientInstanceName="cmbxClientUserID" runat="server" Width="170px" ValueType="System.String" TabIndex="10">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="User ID is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>
                                        <td>Store No :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmbxStrNo" OnInit="cmbxStrNo_Init" ClientInstanceName="cmbxClientStrNo" runat="server" Width="170px" ValueType="System.String" TabIndex="7">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Store No. is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Price List : </td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmbxPriceList" OnInit="cmbxPriceList_Init" ClientInstanceName="cmbxClientPriceList" runat="server" Width="170px" ValueType="System.String" TabIndex="10">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Price List is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>
                                        <td>Drawer # :</td>
                                        <td>
                                            <div class="DivReq">
                                                <dx:ASPxComboBox ID="cmbxDrawerID" OnInit="cmbxDrawerID_Init" ClientInstanceName="cmbxClientDrawerID" runat="server" Width="170px" ValueType="System.String" TabIndex="11">
                                                    <ValidationSettings EnableCustomValidation="true" ErrorDisplayMode="ImageWithText" ErrorTextPosition="Right" SetFocusOnError="true">
                                                        <ErrorFrameStyle Font-Size="Smaller" />
                                                        <RequiredField IsRequired="true" ErrorText="Drawer Number is Required." />
                                                    </ValidationSettings>
                                                </dx:ASPxComboBox>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                            <hr style="color: skyblue" />
                            <dx:ASPxLabel ID="lblErrorMessage" runat="server" ForeColor="Red" />
                            <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                                <dx:ASPxButton ID="btnAdd1" runat="server" Text="Save" CssClass="btnPopup btn btn-success" CausesValidation="true" OnClick="btnAdd1_Click" TabIndex="10" />
                                <dx:ASPxButton ID="btnPopUpClear" runat="server" Text="Cancel" CssClass="btnPopup btn btn-warning" CausesValidation="false" AutoPostBack="false">
                                    <ClientSideEvents Click="function(s, e) { CallBackCode.PerformCallback(txtClientCode.GetText()); }" />
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="btnPopUpDelete" runat="server" CommandArgument='<%# Eval("[DFG_MAC]") %>' CommandName="PopUpDelete" Text="Delete" CssClass="btnPopup btn btn-danger btn-danger-variant" CausesValidation="false">
                                    <ClientSideEvents Click="function(s, e) { 
                                                if (!confirm('Confirm Delete?')) { 
                                                    e.processOnServer = false;  
                                                } 
                                        }" />
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="btnPopUpPrint" runat="server" Text="Print" CssClass="btnPopup btn btn-info btn-info-variant" AutoPostBack="false" CausesValidation="false">
                                    <ClientSideEvents Click="function(s, e) { printDoc(s, e); }" />
                                </dx:ASPxButton>
                                <dx:ASPxGridViewTemplateReplacement ID="btnPopUpClose" ReplacementType="EditFormCancelButton" runat="server" />
                            </div>
                        </div>
                    </div>
                </EditForm>
            </Templates>

            <Templates>
                <StatusBar>
                    <table>
                        <tr>
                            <td>
                                <%--<dx:ASPxButton ID="btnNewRecord" runat="server" Text="Add New Item" AutoPostBack="false">
                                    <ClientSideEvents Click="function (s, e){ FromGridView.AddNewRow();  }" />
                                </dx:ASPxButton>--%>
                            </td>
                            <td>Search: </td>
                            <td>
                                <dx:ASPxButtonEdit runat="server" ID="CustomSearchPanelTxtBx" />
                            </td>
                            <td>&nbsp;</td>
                            <td>Search In:</td>
                            <td>
                                <dx:ASPxComboBox AutoPostBack="true" ID="DDLFilterByColumn" runat="server" OnSelectedIndexChanged="DDLFilterByColumn_SelectedIndexChanged">
                                    <Items>
                                        <dx:ListEditItem Text="All" Value="*" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>

                        </tr>
                    </table>
                </StatusBar>
            </Templates>

        </dx:ASPxGridView>

        </section>       
           <%-- <asp:SqlDataSource runat="server" ID="ItemSQLDS" ConnectionString='<%$ ConnectionStrings:MyConn %>' SelectCommand="SELECT [ITEM_NO], [DESCRP], [DESCRP2], [TRACK], [BARCODE], [CATG], [SUB_CATG], [USR_DEF_1], [USR_DEF_2], [USR_DEF_3], [USR_DEF_4], [STK_UNIT],[STK_UNIT2], [REG_PRC], [ALIAS] FROM [IM_STP_ITEM]" ProviderName='<%$ ConnectionStrings:MyConn.ProviderName %>'></asp:SqlDataSource>--%>
    </div>

    <script type="text/javascript">
        window.onload = function () {
            $('form').on('click', '.btnPopupClose', function () {
                //$('.overlay').hide();
            });
        };

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //if (prm != null) {
        prm.add_endRequest(function (sender, e) {
            //if (sender._postBackSettings.panelsToUpdate != null) {            
            //$('.overlay').show();
            //}
        });
        //};


        function OnTestValidation(s, e) {
            //var testData = e.value;
            //if (!testData) {
            lblError.SetVisible(!e.isValid);
            //  return;
        }

        function CB_userCodeLostFocus(s, e) {
            var RowVals = JSON.parse(e.result);
            if (RowVals.length > 0) {
                txtClientEmail.SetText(RowVals[2]);
                txtClientPass.SetText(RowVals[3]);
                txtClientStatus.SetValue(RowVals[4]);
                txtClientType.SetValue(RowVals[5]);
            } else {
                txtClientEmail.SetText("");
                txtClientPass.SetText("");
                txtClientStatus.SetText("");
                txtClientType.SetValue("");
            }
        }
    </script>
</asp:Content>
