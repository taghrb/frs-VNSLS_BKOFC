﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Customers_Default : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT [REG_CODE] ,[REG_NAME]  ,[REG_CMNTS], [REG_STRNO] FROM [STP_MSTR_RGSTR]";
        //+ "  ,[CUS_CNTC1] ,[CUS_CNTC2] ,[CUS_PHON1] ,[CUS_PHON2]"
        //+ "  ,[CUS_EMAIL] ,[CUS_SLSRP] ,[CUS_TYPE]  ,[CUS_CATG]"
        //+ "  ,[CUS_VATNO] ,[CUS_TXABL] ,[CUS_GRPCD] ,[REG_STORE]"
        //+ "   FROM [STP_MSTR_RGSTR]";
    }

    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxTextBox txtBxCode = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtCode"));
        string RCode = "0";
        //  string CatTyp = "0";
        if (txtBxCode.Text.Length > 0)
        {
            RCode = txtBxCode.Text.Trim();
        }

        string RName = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtName")).Text;
        string RComents = ((ASPxMemo)gvInquery.FindEditFormTemplateControl("txtComments")).Text;
        string StrNoCode = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxStoreNo")).Value == null ? "" : ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("cmbxStoreNo")).Value.ToString();

        DataTable myDT = new DataTable();

        myDT = myDT = dbHlpr.FetchData("SELECT * FROM [STP_MSTR_RGSTR] WHERE REG_CODE = '" + RCode + "' ");
        if (myDT.Rows.Count <= 0)
        {

            dbHlpr.ExecuteNonQuery("INSERT INTO [STP_MSTR_RGSTR] ([REG_CODE] ,[REG_NAME] ,[REG_CMNTS], [REG_STRNO])"
            + " VALUES ('" + RCode + "',  '" + RName + "' , '" + RComents + "', '" + StrNoCode + "' )");
        }
        else
        {
            dbHlpr.ExecuteNonQuery("UPDATE [STP_MSTR_RGSTR] SET "
            + "  REG_NAME = '" + RName + "' "
            + " ,REG_CMNTS = '" + RComents + "' "
            + " ,REG_STRNO = '" + StrNoCode + "' "
            + " WHERE REG_CODE = '" + RCode + "'");
        }

        gvInquery.CancelEdit();
        gvInquery.DataBind();
    }//end method

    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }//end method

    protected void gvInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string RCode = e.Keys["REG_CODE"].ToString().Trim();

        dbHlpr.ExecuteNonQuery("DELETE FROM [STP_MSTR_RGSTR] WHERE REG_CODE =  '" + RCode + "' ");

        e.Cancel = true;
        gvInquery.CancelEdit();
    }//end method

    protected void gvInquery_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }//end method

    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }//end method

    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked

            int CHK = dbHlpr.ExecuteNonQuery("DELETE FROM STP_MSTR_RGSTR WHERE REG_CODE = '" + e.CommandArgs.CommandArgument.ToString().Trim() + "'");
            if (CHK == 1) // Code is sent via request
            {
                gvInquery.CancelEdit();
                gvInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid   ID. Unable to delete.";
            }
        }
    }


    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }

    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }
    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

        ////html export here
        //gridExport.ExportHtmlToResponse();
        //string str;
        //MemoryStream ms = new MemoryStream();
        //try
        //{
        //    gridExport.ExportToHtml(ms);
        //    ms.Seek(0, SeekOrigin.Begin);
        //    StreamReader sr = new StreamReader(ms);
        //    str = sr.ReadToEnd();
        //}
        //finally
        //{
        //    ms.Close();
        //}

    }
}//end class