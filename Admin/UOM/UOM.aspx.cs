﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;

public partial class Admin_Categories_Default : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT [UOM_CODE]      ,[UOM_NAME]       FROM [STP_MSTR_UOM] ";
    }

    protected void categoryListingGridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvUOMInquery_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvUOMInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string uomCode = e.Keys["UOM_CODE"].ToString().Trim();

        dbHlpr.ExecuteNonQuery("DELETE FROM [STP_MSTR_UOM] WHERE UOM_CODE = '" + uomCode + "'");

        DataTable dt = dbHlpr.FetchData("SELECT '" + uomCode + "' AS TEXT01 ");
        dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MSTR_UOM", "DLT");

        e.Cancel = true;
        gvUOMInquery.CancelEdit();
    }
    protected void ACB_CodeCheck_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (e.Parameter.Length > 0)
        {
            DataTable myDT = new DataTable();
            myDT = dbHlpr.FetchData("SELECT * FROM [STP_MSTR_UOM] WHERE UOM_CODE = '" + e.Parameter.Trim() + "'");
            JavaScriptSerializer myserializer = new JavaScriptSerializer();
            if (myDT.Rows.Count > 0)
            {
                e.Result = myserializer.Serialize(myDT.Rows[0].ItemArray);
            }
            else
            {
                e.Result = myserializer.Serialize("");
            }
        }
    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxTextBox txtBxCode = ((ASPxTextBox)gvUOMInquery.FindEditFormTemplateControl("CatEditCode"));
        string uomCode = "0";
        //  string CatTyp = "0";
        if (txtBxCode.Text.Length > 0)
        {
            uomCode = txtBxCode.Text.Trim();
        }

        string UomName = ((ASPxTextBox)gvUOMInquery.FindEditFormTemplateControl("CatEditName")).Text;
        //       string CatType = ((ASPxComboBox)gvUOMInquery.FindEditFormTemplateControl("CatEditType")).SelectedItem.Value.ToString();

        DataTable myDT = new DataTable();

        myDT = myDT = dbHlpr.FetchData("SELECT * FROM [STP_MSTR_UOM] WHERE UOM_CODE = '" + uomCode + "'");
        if (myDT.Rows.Count <= 0)
        {

            dbHlpr.ExecuteNonQuery("INSERT INTO [STP_MSTR_UOM] (UOM_CODE,  UOM_NAME   ) "
            + " VALUES ('" + uomCode + "',  '" + UomName + "')");

            DataTable dt = dbHlpr.FetchData("SELECT "
                + " UOM_CODE AS TEXT01, UOM_NAME AS TEXT02, UOM_ANAME AS TEXT03 "
                + " FROM STP_MSTR_UOM "
                + " WHERE UOM_CODE = '" + uomCode + "' ");
            dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MSTR_UOM", "CRT");
        }
        else
        {
            dbHlpr.ExecuteNonQuery("UPDATE [STP_MSTR_UOM] SET "
            + " UOM_NAME = '" + UomName + "' "
            + " WHERE UOM_CODE = '" + uomCode + "'");

            DataTable dt = dbHlpr.FetchData("SELECT "
                + " UOM_CODE AS TEXT01, UOM_NAME AS TEXT02, UOM_ANAME AS TEXT03 "
                + " FROM STP_MSTR_UOM "
                + " WHERE UOM_CODE = '" + uomCode + "' ");
            dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MSTR_UOM", "UPD");
        }

        gvUOMInquery.CancelEdit();
        gvUOMInquery.DataBind();
    }
    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvUOMInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvUOMInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

        ////html export here
        //gridExport.ExportHtmlToResponse();
        //string str;
        //MemoryStream ms = new MemoryStream();
        //try
        //{
        //    gridExport.ExportToHtml(ms);
        //    ms.Seek(0, SeekOrigin.Begin);
        //    StreamReader sr = new StreamReader(ms);
        //    str = sr.ReadToEnd();
        //}
        //finally
        //{
        //    ms.Close();
        //}

    }
    protected void gvUOMInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            string uomId = e.CommandArgs.CommandArgument.ToString();

            if (uomId != "") // Code is sent via request
            {
                dbHlpr.ExecuteNonQuery("DELETE FROM [STP_MSTR_UOM] WHERE UOM_CODE = '" + uomId + "'");

                DataTable dt = dbHlpr.FetchData("SELECT '" + uomId + "' AS TEXT01 ");
                dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MSTR_UOM", "DLT");

                gvUOMInquery.CancelEdit();
                gvUOMInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvUOMInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid Category ID. Unable to delete Category.";
            }
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvUOMInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvUOMInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvUOMInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvUOMInquery.SearchPanelFilter = txtFilter.Text;
        gvUOMInquery.DataBind();
    }
    protected void gvUOMInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvUOMInquery, CBX_filter);
    }
    protected void gvUOMInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvUOMInquery, CBX_filter);
    }


    protected void CatEditCode_Init(object sender, EventArgs e)
    {
        if (!gvUOMInquery.IsNewRowEditing)
        {
            ASPxTextBox txtCode = (ASPxTextBox)sender;
            txtCode.ReadOnly = true;
        }
        else
        {
            ASPxTextBox txtCode1 = (ASPxTextBox)sender;
            txtCode1.ReadOnly = false;
        }
    }
}