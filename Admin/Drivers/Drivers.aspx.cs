﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Drivers_Default : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT * FROM STP_MSTR_DRIVR";
    }

    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvDriverInquery_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvDriverInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string driverCode = e.Keys["DRV_CODE"].ToString();

        dbHlpr.ExecuteNonQuery("DELETE FROM STP_MSTR_DRIVR WHERE DRV_CODE = '" + driverCode + "'");

        e.Cancel = true;
        gvDriverInquery.CancelEdit();
    }
    protected void ACB_CodeCheck_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (e.Parameter.Length > 0)
        {
            DataTable myDT = new DataTable();
            myDT = dbHlpr.FetchData("SELECT * FROM STP_MSTR_DRIVR WHERE DRV_CODE = '" + e.Parameter.ToString() + "'");
            JavaScriptSerializer myserializer = new JavaScriptSerializer();
            if (myDT.Rows.Count > 0)
            {
                e.Result = myserializer.Serialize(myDT.Rows[0].ItemArray);
            }
            else
            {
                e.Result = myserializer.Serialize("");
            }
        }
    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxTextBox txtEmpNo = (((ASPxTextBox)gvDriverInquery.FindEditFormTemplateControl("txtEmpNo")));
        ASPxTextBox txtId = (((ASPxTextBox)gvDriverInquery.FindEditFormTemplateControl("txtCode")));
        ASPxTextBox txtInitials = (((ASPxTextBox)gvDriverInquery.FindEditFormTemplateControl("txtInitials")));
        ASPxTextBox txtUserName = (((ASPxTextBox)gvDriverInquery.FindEditFormTemplateControl("txtUserName")));
        ASPxTextBox txtCellNumber = (((ASPxTextBox)gvDriverInquery.FindEditFormTemplateControl("txtCellNumber")));
        ASPxTextBox txtNationality = (((ASPxTextBox)gvDriverInquery.FindEditFormTemplateControl("txtNationality")));
        ASPxTextBox txtPass = (((ASPxTextBox)gvDriverInquery.FindEditFormTemplateControl("txtPass")));
        //    ASPxComboBox cmBxBrnch = (((ASPxComboBox)gvDriverInquery.FindEditFormTemplateControl("cmBxBrnch")));
        ASPxMemo txtNotes = (((ASPxMemo)gvDriverInquery.FindEditFormTemplateControl("txtNotes")));


        string driverEmpNo = txtEmpNo.Text;
        string driverId = txtId.Text;
        string driverInitials = txtInitials.Text;
        string driverName = txtUserName.Text;
        string driverPassword = txtPass.Text;
        //  string driverBranch = cmBxBrnch.Value.ToString();
        string driverNotes = txtNotes.Value.ToString();
        string driverCellNumber = txtCellNumber.Value.ToString();
        string driverNationality = txtNationality.Value.ToString();

        DataTable myDT = new DataTable();

        myDT = dbHlpr.FetchData("SELECT * FROM STP_MSTR_DRIVR WHERE DRV_CODE = '" + driverId + "'");
        if (myDT.Rows.Count <= 0)
        {
            dbHlpr.ExecuteNonQuery("INSERT INTO STP_MSTR_DRIVR (DRV_CODE, DRV_INITL, DRV_NAME, DRV_PSWRD, DRV_EMPNO, DRV_CELNO, DRV_CNTRY,  DRV_NOTES) "
                + " VALUES ('" + driverId + "', '" + driverInitials + "', '" + driverName + "', '" + driverPassword + "', '" + driverEmpNo + "', '" + driverCellNumber + "', '" + driverNationality + "',  '" + driverNotes + "')");
        }
        else
        {
            dbHlpr.ExecuteNonQuery("UPDATE STP_MSTR_DRIVR SET "
                + " DRV_INITL = '" + driverInitials + "', "
                + " DRV_NAME = '" + driverName + "', "
                + " DRV_PSWRD = '" + driverPassword + "', "
                + " DRV_EMPNO = '" + driverEmpNo + "', "
                + " DRV_CELNO = '" + driverCellNumber + "', "
                + " DRV_CNTRY = '" + driverNationality + "', "
                //       + " DRV_PRMBR = '" + driverBranch + "', "
                + " DRV_NOTES = '" + driverNotes + "' "
                + " WHERE DRV_CODE = '" + driverId + "'");
        }
        gvDriverInquery.CancelEdit();
        gvDriverInquery.DataBind();
    }
    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvDriverInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvDriverInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

        ////html export here
        //gridExport.ExportHtmlToResponse();
        //string str;
        //MemoryStream ms = new MemoryStream();
        //try
        //{
        //    gridExport.ExportToHtml(ms);
        //    ms.Seek(0, SeekOrigin.Begin);
        //    StreamReader sr = new StreamReader(ms);
        //    str = sr.ReadToEnd();
        //}
        //finally
        //{
        //    ms.Close();
        //}

    }

    protected void gvDriverInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            string driverId = e.CommandArgs.CommandArgument.ToString().Trim();
            if (driverId.Length > 0) // Code is sent via request
            {
                dbHlpr.ExecuteNonQuery("DELETE FROM STP_MSTR_DRIVR WHERE DRV_CODE = '" + driverId + "'");

                gvDriverInquery.CancelEdit();
                gvDriverInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvDriverInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid Driver ID. Unable to delete driver.";
            }
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvDriverInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvDriverInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvDriverInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvDriverInquery.SearchPanelFilter = txtFilter.Text;
        gvDriverInquery.DataBind();
    }
    protected void gvDriverInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvDriverInquery, CBX_filter);
    }
    protected void gvDriverInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvDriverInquery, CBX_filter);
    }

    private string EditDriverBranch_SLCTD = string.Empty;

    protected void gvDriverInquery_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        ASPxGridView gv = (ASPxGridView)sender;
        EditDriverBranch_SLCTD = gv.GetRowValues(gv.EditingRowVisibleIndex, "DRV_PRMBR").ToString();

    }
    //protected void cmBxBrnch_Init(object sender, EventArgs e)
    //{
    //    // Get Categories for current active division
    //    ASPxComboBox cbBrnch = (ASPxComboBox)sender;

    //    cbBrnch.DataSource = dbHlpr.FetchData("SELECT * FROM " + Session["ADC"] + "STP_BRNCH");
    //    cbBrnch.ValueField = "BRN_CODE";
    //    cbBrnch.TextField = "BRN_NAME";
    //    cbBrnch.DataBind();
    //    cbBrnch.Value = EditDriverBranch_SLCTD;
    //}

    protected void txtEmpNo_Init(object sender, EventArgs e)
    {
        if (!gvDriverInquery.IsNewRowEditing)
        {
            ASPxTextBox txtCode = (ASPxTextBox)sender;
            txtCode.ReadOnly = true;
        }
        else
        {
            ASPxTextBox txtCode1 = (ASPxTextBox)sender;
            txtCode1.ReadOnly = false;
        }     
    }
}