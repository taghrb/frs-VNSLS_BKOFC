﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;

public partial class Admin_Categories_Default : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT [TAX_CODE] ,[TAX_TITLE] ,[TAX_FACTR] FROM [STP_MSTR_TAXES] ";
    }

    protected void categoryListingGridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvInquery_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string TaxCode = e.Keys["TAX_CODE"].ToString().Trim();

        DataTable dtItemsWithTaxCode = dbHlpr.FetchData("SELECT * FROM INV_MSTR_PRODT WHERE PRO_TAXCD = '" + TaxCode + "' ");
        if (dtItemsWithTaxCode.Rows.Count > 0)
        {
            gvInquery.Settings.ShowTitlePanel = true;
            gvInquery.SettingsText.Title = "Tax Code is assigned to " + dtItemsWithTaxCode.Rows.Count + " Items. Unable to delete Tax Code.";
            gvInquery.Styles.TitlePanel.CssClass = "alert alert-danger";
        }
        else
        {
            dbHlpr.ExecuteNonQuery("DELETE FROM [STP_MSTR_TAXES] WHERE TAX_CODE = '" + TaxCode + "'");

            DataTable dt = dbHlpr.FetchData("SELECT '" + TaxCode + "' AS TEXT01 ");
            dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MSTR_TAXES", "DLT");
        }
        e.Cancel = true;
        gvInquery.CancelEdit();
    }
    protected void ACB_CodeCheck_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (e.Parameter.Length > 0)
        {
            DataTable myDT = new DataTable();
            myDT = dbHlpr.FetchData("SELECT * FROM [STP_MSTR_TAXES] WHERE TAX_CODE = '" + e.Parameter.Trim() + "'");
            JavaScriptSerializer myserializer = new JavaScriptSerializer();
            if (myDT.Rows.Count > 0)
            {
                e.Result = myserializer.Serialize(myDT.Rows[0].ItemArray);
            }
            else
            {
                e.Result = myserializer.Serialize("");
            }
        }
    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxTextBox txtBxCode = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("EditCode"));
        string TaxCode = "0";
      //  string CatTyp = "0";
        if (txtBxCode.Text.Length > 0)
        {
            TaxCode = txtBxCode.Text.Trim();
        }   

        string TaxName = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("EditName")).Text;
        string TaxFactor= ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("EditFactor")).Text;
 //       string CatType = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("CatEditType")).SelectedItem.Value.ToString();
       
        DataTable myDT = new DataTable();

        myDT = myDT = dbHlpr.FetchData("SELECT * FROM [STP_MSTR_TAXES] WHERE TAX_CODE = '" + TaxCode + "'");
        if (myDT.Rows.Count <= 0)
        {

            dbHlpr.ExecuteNonQuery("INSERT INTO [STP_MSTR_TAXES] (TAX_CODE,  TAX_TITLE,TAX_FACTR   ) "
            + " VALUES ('" + TaxCode + "',  '" + TaxName + "', '" + TaxFactor + "')");

            DataTable dt = dbHlpr.FetchData("SELECT "
                + " TAX_CODE AS TEXT01, TAX_TITLE AS TEXT02, TAX_FACTR AS NUM01 "
                + " FROM STP_MSTR_TAXES "
                + " WHERE TAX_CODE = '" + TaxCode + "' ");
            dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MSTR_TAXES", "CRT");
        }
        else
        {
            dbHlpr.ExecuteNonQuery("UPDATE [STP_MSTR_TAXES] SET "
            + " TAX_TITLE = '" + TaxName + "', "
            + " TAX_FACTR = '" + TaxFactor + "' "
            + " WHERE TAX_CODE = '" + TaxCode + "'");

            DataTable dt = dbHlpr.FetchData("SELECT "
                + " TAX_CODE AS TEXT01, TAX_TITLE AS TEXT02, TAX_FACTR AS NUM01 "
                + " FROM STP_MSTR_TAXES "
                + " WHERE TAX_CODE = '" + TaxCode + "' ");
            dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MSTR_TAXES", "UPD");
        }

        gvInquery.CancelEdit();
        gvInquery.DataBind();
    }
    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

        ////html export here
        //gridExport.ExportHtmlToResponse();
        //string str;
        //MemoryStream ms = new MemoryStream();
        //try
        //{
        //    gridExport.ExportToHtml(ms);
        //    ms.Seek(0, SeekOrigin.Begin);
        //    StreamReader sr = new StreamReader(ms);
        //    str = sr.ReadToEnd();
        //}
        //finally
        //{
        //    ms.Close();
        //}

    }
    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            string TaxId = e.CommandArgs.CommandArgument.ToString();

            if (TaxId != "") // Code is sent via request
            {
                DataTable dtItemsWithTaxCode = dbHlpr.FetchData("SELECT * FROM INV_MSTR_PRODT WHERE PRO_TAXCD = '" + TaxId + "' ");
                if (dtItemsWithTaxCode.Rows.Count > 0)
                {
                    ASPxLabel lblErrMsg = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblErrorMessage");
                    lblErrMsg.Text = "Tax Code is assigned to " + dtItemsWithTaxCode.Rows.Count + " Items. Unable to delete Tax Code.";
                }
                else
                {
                    dbHlpr.ExecuteNonQuery("DELETE FROM [STP_MSTR_TAXES] WHERE TAX_CODE = '" + TaxId + "'");

                    DataTable dt = dbHlpr.FetchData("SELECT '" + TaxId + "' AS TEXT01 ");
                    dbHlpr.CreateDownloadRecord(dt, "ALL", "STP_MSTR_TAXES", "DLT");
                    
                    gvInquery.CancelEdit();
                    gvInquery.DataBind();
                }
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid Category ID. Unable to delete Category.";
            }
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }
    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }
    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }


    protected void EditCode_Init(object sender, EventArgs e)
    {
        if (!gvInquery.IsNewRowEditing)
        {
            ASPxTextBox txtCode = (ASPxTextBox)sender;
            txtCode.ReadOnly = true;
        }
        else
        {
            ASPxTextBox txtCode1 = (ASPxTextBox)sender;
            txtCode1.ReadOnly = false;
        }     
    }
}