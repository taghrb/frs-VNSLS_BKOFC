﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;

public partial class Admin_Categories_Default : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();
    private string Country_SLCTD = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
        DxGridSqlDataSource1.SelectCommand = "SELECT STM_CODE, STM_NAME, STM_ADRS1, STM_CNCT1, STM_PHON1, STM_CNTRY FROM STP_MSTR_STORE";
    }

    protected void categoryListingGridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void ddlNoOfRecords_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvInquery_CustomButtonCallback(object sender, DevExpress.Web.ASPxGridViewCustomButtonCallbackEventArgs e)
    {

    }
    protected void gvInquery_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        string SCode = e.Keys["STM_CODE"].ToString().Trim();

        dbHlpr.ExecuteNonQuery("DELETE FROM [STP_MSTR_STORE] WHERE STM_CODE = '" + SCode + "'");

        e.Cancel = true;
        gvInquery.CancelEdit();
    }
    protected void ACB_CodeCheck_Callback(object source, DevExpress.Web.CallbackEventArgs e)
    {
        if (e.Parameter.Length > 0)
        {
            DataTable myDT = new DataTable();
            myDT = dbHlpr.FetchData("SELECT * FROM [STP_MSTR_STORE] WHERE STM_CODE = '" + e.Parameter.Trim() + "'");
            JavaScriptSerializer myserializer = new JavaScriptSerializer();
            if (myDT.Rows.Count > 0)
            {
                e.Result = myserializer.Serialize(myDT.Rows[0].ItemArray);
            }
            else
            {
                e.Result = myserializer.Serialize("");
            }
        }
    }
    protected void btnAdd1_Click(object sender, EventArgs e)
    {
        ASPxTextBox txtBxCode = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtCode"));
        string SCode = "0";
        //  string CatTyp = "0";
        if (txtBxCode.Text.Length > 0)
        {
            SCode = txtBxCode.Text.Trim();
        }

        string SName = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtName")).Text;
        string SAddress = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtAddress1")).Text;
        string SContact = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtContact1")).Text;
        string SPhone = ((ASPxTextBox)gvInquery.FindEditFormTemplateControl("txtPhone1")).Text;
        string CountryCode = ((ASPxComboBox)gvInquery.FindEditFormTemplateControl("ddlCountry")).SelectedItem.Value.ToString();

        DataTable myDT = new DataTable();

        myDT = myDT = dbHlpr.FetchData("SELECT * FROM [STP_MSTR_STORE] WHERE STM_CODE = '" + SCode + "'");
        if (myDT.Rows.Count <= 0)
        {

            dbHlpr.ExecuteNonQuery("INSERT INTO STP_MSTR_STORE (STM_CODE,  STM_NAME , STM_ADRS1, STM_CNCT1, STM_PHON1, STM_CNTRY) "
            + " VALUES ('" + SCode + "',  '" + SName + "',  '" + SAddress + "',  '" + SContact + "',  '" + SPhone + "', '" + CountryCode + "')");
        }//[STM_CODE] ,[STM_NAME] ,[STM_ADRS1] ,[STM_CNCT1] ,[STM_PHON1]
        else
        {
            dbHlpr.ExecuteNonQuery("UPDATE [STP_MSTR_STORE] SET "
            + " STM_NAME = '" + SName + "', "
            + " STM_ADRS1 = '" + SAddress + "', "
            + " STM_CNCT1 = '" + SContact + "', "
            + " STM_PHON1 = '" + SPhone + "', "
            + " STM_CNTRY = '" + CountryCode + "' "
            + " WHERE STM_CODE = '" + SCode + "'");
        }

        gvInquery.CancelEdit();
        gvInquery.DataBind();
    }
    protected void btnUpdate1_Click(object sender, EventArgs e)
    {

    }

    protected void LinkBtnPDF_Click(object sender, EventArgs e)
    {
        gridExport.WritePdfToResponse();
    }

    protected void LinkButtonXLX_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXls1997(this, gvInquery);
    }
    protected void LinkButtonWord_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }
    protected void LinkButtonXxls_Click(object sender, EventArgs e)
    {
        dbHlpr.ExportToFormattedXlsx2007(this, gvInquery);
    }
    protected void LinkButtonRtf_Click(object sender, EventArgs e)
    {
        gridExport.WriteRtfToResponse();
    }

    protected void LinkButtonCvs_Click(object sender, EventArgs e)
    {
        gridExport.WriteCsvToResponse();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {

    }
    protected void LinkButtonHtml_Click(object sender, EventArgs e)
    {

    }
    protected void gvInquery_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        if (e.CommandArgs.CommandName.Equals("PopUpDelete"))
        { // Delete button clicked
            string SId = e.CommandArgs.CommandArgument.ToString();

            if (SId != "") // Code is sent via request
            {
                dbHlpr.ExecuteNonQuery("DELETE FROM [STP_MSTR_STORE] WHERE STM_CODE = '" + SId + "'");

                gvInquery.CancelEdit();
                gvInquery.DataBind();
            }
            else
            {
                ASPxLabel lblErrMsg = (ASPxLabel)gvInquery.FindEditFormTemplateControl("lblErrorMessage");
                lblErrMsg.Text = "Invalid Category ID. Unable to delete Category.";
            }
        }
    }

    ASPxComboBox CBX_filter;
    protected void DDLFilterByColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        CBX_filter = (ASPxComboBox)gvInquery.FindStatusBarTemplateControl("DDLFilterByColumn");
        ASPxButtonEdit txtFilter = (ASPxButtonEdit)gvInquery.FindStatusBarTemplateControl("CustomSearchPanelTxtBx");
        gvInquery.SettingsSearchPanel.ColumnNames = CBX_filter.Text;
        gvInquery.SearchPanelFilter = txtFilter.Text;
        gvInquery.DataBind();
    }
    protected void gvInquery_DataBound(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }
    protected void gvInquery_BeforeGetCallbackResult(object sender, EventArgs e)
    {
        ForsanHelperMethods.FillColumnsDropDown(gvInquery, CBX_filter);
    }


    protected void txtCode_Init(object sender, EventArgs e)
    {
        if (!gvInquery.IsNewRowEditing)
        {
            ASPxTextBox txtCode = (ASPxTextBox)sender;
            txtCode.ReadOnly = true;
        }
        else
        {
            ASPxTextBox txtCode1 = (ASPxTextBox)sender;
            txtCode1.ReadOnly = false;
        }
    }

    protected void ddlCountry_Init(object sender, EventArgs e)
    {
        ASPxComboBox CBX_Country = (ASPxComboBox)sender;
        CBX_Country.DataSource = dbHlpr.FetchData("SELECT * FROM STP_MST_CNTRY");
        CBX_Country.DataBind();

        CBX_Country.Value = Country_SLCTD;
    }

    protected void gvInquery_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        Country_SLCTD = ((ASPxGridView)sender).GetRowValues(((ASPxGridView)sender).EditingRowVisibleIndex, "STM_CNTRY").ToString();
    }
}