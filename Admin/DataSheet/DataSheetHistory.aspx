﻿<%@ Page Title="Data Sheet History" Language="C#" MasterPageFile="~/MasterPages/SiteAdmin.master" AutoEventWireup="true" CodeFile="DataSheetHistory.aspx.cs" Inherits="Admin_DataSheet_DataSheetHistory" %>


<%@ Register Assembly="DevExpress.Web.v15.1, Version=15.1.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content3" ContentPlaceHolderID="headContentPlaceholder" runat="Server">
    <script type="text/javascript">

        function grid_customizationWindowCloseUp(s, e) {
            gvUOMClientInquery.ShowCustomizationWindow();
        }

        // Code to create a custom multiselect ComboBox, Dropdown
        var textSeparator = ","; // Between each checked item in ComboBox
        function OnListBoxSelectionChanged(listBox, args) {
            if (args.index == 0)
                args.isSelected ? listBox.SelectAll() : listBox.UnselectAll();
            if (listBox.name.indexOf("lstBxStrItems") != -1) {
                IsAllSelected(checkStrListBox) ? checkStrListBox.SelectIndices([0]) : checkStrListBox.UnselectIndices([0]);
                UpdateText(DtShtClientStrNo, checkStrListBox);
            } else if (listBox.name.indexOf("lstBxRegItems") != -1) {
                IsAllSelected(checkRegListBox) ? checkRegListBox.SelectIndices([0]) : checkRegListBox.UnselectIndices([0]);
                UpdateText(DtShtClientRegNo, checkRegListBox);
            }
        }
        function IsAllSelected(chkLstBx) {
            var selectedDataItemCount = chkLstBx.GetItemCount() - (chkLstBx.GetItem(0).selected ? 0 : 1);
            return chkLstBx.GetSelectedItems().length == selectedDataItemCount;
        }
        function UpdateText(ddObj, lstBxObj) {
            var selectedItems = lstBxObj.GetSelectedItems();
            ddObj.SetText(GetSelectedItemsText(selectedItems));
        }
        function SynchronizeListBoxValues(dropDown, args) {
            var texts = dropDown.GetText().split(textSeparator);
            if (dropDown.name.indexOf("DtShtStrNo") != -1) {
                checkStrListBox.UnselectAll();
                checkStrListBox.SelectValues(texts);
                IsAllSelected(checkStrListBox) ? checkStrListBox.SelectIndices([0]) : checkStrListBox.UnselectIndices([0]);
                UpdateText(DtShtClientStrNo, checkStrListBox);
            } else if (dropDown.name.indexOf("DtShtRegNo") != -1) {
                checkRegListBox.UnselectAll();
                checkRegListBox.SelectValues(texts);
                IsAllSelected(checkRegListBox) ? checkRegListBox.SelectIndices([0]) : checkRegListBox.UnselectIndices([0]);
                UpdateText(DtShtClientRegNo, checkRegListBox);
            }
        }
        function GetSelectedItemsText(items) {
            var texts = [];
            for (var i = 0; i < items.length; i++)
                if (items[i].index != 0)
                    texts.push(items[i].value);
            return texts.join(textSeparator);
        }
        function GetValuesByTexts(texts, chkLstBx) {
            var actualValues = [];
            var item;
            for (var i = 0; i < texts.length; i++) {
                item = chkLstBx.FindItemByText(texts[i]);
                if (item != null)
                    actualValues.push(item.value);
            }
            return actualValues;
        }

    </script>
    <style type="text/css">
        .fieldsetWrapper {
            padding: 10px;
            width: 50%;
            margin: 0 auto;
            background-color: #F7F5F1;
            margin-top: 5px;
            border-radius: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="contentTopHere1" ContentPlaceHolderID="topBarContent1" runat="server">
    <div class="topContentBar">
        <ul>
            <li>&nbsp;
            </li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContentPlaceholder" runat="Server">

    <div class="formDiv" style="background-color: transparent;">
        <div class="formHeaderDiv">
            <h3>Data Sheet</h3>
        </div>
        <div class="fieldsetWrapper">
            <asp:UpdatePanel ID="dsds" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="DtShtStrNo" EventName="TextChanged" />
                    <asp:AsyncPostBackTrigger ControlID="DtShtRegNo" EventName="TextChanged" />
                    <%--<asp:AsyncPostBackTrigger ControlID="lstBxStrItems" EventName="ValueChanged" />
                    <asp:AsyncPostBackTrigger ControlID="lstBxRegItems" EventName="ValueChanged" />--%>
                    <asp:AsyncPostBackTrigger ControlID="DtShtDateFrom" EventName="ValueChanged" />
                    <asp:AsyncPostBackTrigger ControlID="DtShtDateTo" EventName="ValueChanged" />
                </Triggers>

                <ContentTemplate>
                    <fieldset>
                        <legend></legend>
                        <table>
                            <tr>
                                <td>Store # :&nbsp;&nbsp;&nbsp;</td>
                                <td>
                                    <div class="DivReq">
                                        <dx:ASPxDropDownEdit ID="DtShtStrNo" OnInit="DtShtStrNo_Init" ClientInstanceName="DtShtClientStrNo" CssClass="multiselect_combobox" runat="server" AnimationType="Auto">
                                            <DropDownWindowTemplate>
                                                <dx:ASPxListBox Width="170px" ID="lstBxStrItems" ClientInstanceName="checkStrListBox" SelectionMode="CheckColumn" runat="server" OnValueChanged="StoreChanged" AutoPostBack="true">
                                                    <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged" />
                                                </dx:ASPxListBox>
                                            </DropDownWindowTemplate>
                                            <ClientSideEvents TextChanged="SynchronizeListBoxValues" DropDown="SynchronizeListBoxValues" />
                                        </dx:ASPxDropDownEdit>
                                    </div>
                                </td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td>Register # :&nbsp;&nbsp;&nbsp;</td>
                                <td>
                                    <div class="DivReq">
                                        <dx:ASPxDropDownEdit ID="DtShtRegNo" OnInit="DtShtRegNo_Init" ClientInstanceName="DtShtClientRegNo" CssClass="multiselect_combobox" runat="server" AnimationType="Auto">
                                            <DropDownWindowTemplate>
                                                <dx:ASPxListBox Width="170px" ID="lstBxRegItems" ClientInstanceName="checkRegListBox" SelectionMode="CheckColumn" runat="server">
                                                    <ClientSideEvents SelectedIndexChanged="OnListBoxSelectionChanged" />
                                                </dx:ASPxListBox>
                                            </DropDownWindowTemplate>
                                            <ClientSideEvents TextChanged="SynchronizeListBoxValues" DropDown="SynchronizeListBoxValues" />
                                        </dx:ASPxDropDownEdit>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Date From :&nbsp;&nbsp;&nbsp;</td>
                                <td>
                                    <div class="DivReq">
                                        <dx:ASPxDateEdit runat="server" ID="DtShtDateFrom" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy" NullText="Earliest" NullTextStyle-ForeColor="Black"></dx:ASPxDateEdit>
                                    </div>
                                </td>
                                <td></td>

                                <td>Date To :&nbsp;&nbsp;&nbsp;</td>
                                <td>
                                    <div class="DivReq">
                                        <dx:ASPxDateEdit runat="server" ID="DtShtDateTo" DisplayFormatString="dd-MMM-yyyy" EditFormatString="dd-MMM-yyyy" NullText="Latest" NullTextStyle-ForeColor="Black"></dx:ASPxDateEdit>
                                    </div>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>

                    </fieldset>

                </ContentTemplate>
            </asp:UpdatePanel>


            <asp:SqlDataSource runat="server" ID="DxGridSqlDataSource1"
                ConnectionString='<%$ ConnectionStrings:DefaultConnection %>'></asp:SqlDataSource>
            <dx:ASPxGridView ClientVisible="false" ID="gvExportData" ClientInstanceName="gvClientExportData" Theme="Office2010Black" runat="server"
                OnHtmlDataCellPrepared="gvExportData_HtmlDataCellPrepared"
                AutoGenerateColumns="false" DataSourceID="DxGridSqlDataSource1">

                <Columns>
                    <dx:GridViewDataDateColumn FieldName="TKL_DATE" Caption="Ticket Date" VisibleIndex="0"
                        PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy">
                        <Settings />
                    </dx:GridViewDataDateColumn>

                    <dx:GridViewDataTextColumn FieldName="TKL_TTYPE" Caption="Ticket Type" VisibleIndex="0">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="TKL_DRWNO" Caption="Drawer #" VisibleIndex="0">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="TKL_REGNO" Caption="Register #" VisibleIndex="0">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="TKH_CUSNO" Caption="Cust#" VisibleIndex="0">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="TKH_CUSNM" Caption="Cust Name" VisibleIndex="0">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="TKL_SREP" Caption="Sales Rep" VisibleIndex="0">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="SHPTO" Caption="Ship To Address" VisibleIndex="0">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="PONUM" Caption="PO #" VisibleIndex="0">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="SHPVIA" Caption="Ship Via" VisibleIndex="0">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="TKL_TKTNO" Caption="Doc#" VisibleIndex="0">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="TKL_ITMNO" Caption="Item#" VisibleIndex="0">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="TKL_DESC" Caption="Item Desc" VisibleIndex="0">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="TKL_UOM" Caption="UOM" VisibleIndex="0">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="TKL_QTY" Caption="Qty" VisibleIndex="0"
                        PropertiesTextEdit-DisplayFormatString="n3">
                        <Settings />
                    </dx:GridViewDataTextColumn>
                    
                    <dx:GridViewDataTextColumn FieldName="TKL_RSNCD" Caption="Reason Code" VisibleIndex="0">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="TKL_TAXCD" Caption="Tax Code" VisibleIndex="0">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="TKL_PRICE" Caption="Price" VisibleIndex="0"
                        PropertiesTextEdit-DisplayFormatString="n2">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="REFNUM" Caption="Reference #" VisibleIndex="0">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataDateColumn FieldName="TKL_EXPDT" Caption="LOT" VisibleIndex="0"
                        PropertiesDateEdit-DisplayFormatString="dd-MMM-yyyy">
                        <Settings />
                    </dx:GridViewDataDateColumn>

                    <dx:GridViewDataTextColumn FieldName="TKL_VATPRC" Caption="Amount" VisibleIndex="0"
                        PropertiesTextEdit-DisplayFormatString="n2">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="TKL_PAYCD" Caption="Pay Code" VisibleIndex="0">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn FieldName="TKH_AMT" Caption="Amount" VisibleIndex="0"
                        PropertiesTextEdit-DisplayFormatString="n2">
                        <Settings />
                    </dx:GridViewDataTextColumn>

                </Columns>


            </dx:ASPxGridView>
            <dx:ASPxGridViewExporter OnRenderBrick="gvExportDataExporter_RenderBrick" ID="gvExportDataExporter" GridViewID="gvExportData" runat="server"></dx:ASPxGridViewExporter>

            <hr style="color: skyblue" />
            <dx:ASPxLabel ID="lblErrorMessage" ClientInstanceName="lblError" runat="server" CssClass="MsgError" Text=""></dx:ASPxLabel>
            <div style="text-align: center; padding: 10px 0px; margin: 0 auto; width: 100%;">
                <dx:ASPxButton ID="btnExportDataSheetHistory" runat="server" Text="Export Datasheet History" CssClass="btnPopup btn btn-success" CausesValidation="true" OnClick="btnExportDataSheetHistory_Click" />
                <dx:ASPxButton ID="btnPopUpClear" runat="server" Text="Clear" CssClass="btnPopup btn btn-warning" CausesValidation="false" AutoPostBack="false">
                    <ClientSideEvents Click="function(s, e) { CallBackCode.PerformCallback(CatClientEditCode.GetText()); }" />
                </dx:ASPxButton>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        window.onload = function () {
            $('form').on('click', '.btnPopupClose', function () {
                $('.overlay').hide();
            });
        };

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        //if (prm != null) {
        prm.add_endRequest(function (sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.overlay').show();
            }
        });
        prm.add_endRequest(GridOnEndCallBack);
        //};

    </script>
</asp:Content>

