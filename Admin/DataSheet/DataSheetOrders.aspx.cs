﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using System.Drawing;
using DevExpress.XtraPrinting;
using DevExpress.Export.Xl;

public partial class Admin_DataSheet_DataSheetOrders : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    protected void Page_Init(object sender, EventArgs e)
    {
        // DtShtDateFrom.Value = new DateTime(1900, 1, 1);
        // DtShtDateTo.Value = new DateTime(9999, 12, 31);
    }

    protected void btnExportDataSheetOrders_Click(object sender, EventArgs e)
    {
        string dateFrom = DtShtDateFrom.Value != null ? ((DateTime)DtShtDateFrom.Value).ToString("yyyy-MM-dd") : new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");
        string dateTo = DtShtDateTo.Value != null ? ((DateTime)DtShtDateTo.Value).ToString("yyyy-MM-dd") : new DateTime(9999, 12, 31).ToString("yyyy-MM-dd");

        string stores = CommaSeperatedToQuery(DtShtStrNo.Value.ToString());
        string registers = CommaSeperatedToQuery(DtShtRegNo.Value.ToString());

        DataTable dtExport = new DataTable();
        string qry = "SELECT "
            + " ORL_DATE, ORL_STATS, ORL_DRWNO, ORL_REGNO, "
            + " ORL_CUSNO, ORL_CUSNM, ORL_SHPTO, ORL_SREP, "
            + " ORL_ORDNO, ORL_ITMNO, ORL_DESC, "
            + " ORL_UOM, ORL_QTY, ORL_PRICE, "
            + " ORL_VTPRC, ORL_PAYCD, ORH_AMT "
            + " FROM SLS_ORD_LIN "
            + " JOIN SLS_ORD_HDR ON ORH_HKEY = ORL_HKEY "
            + " WHERE "
            + " ORL_DATE BETWEEN '" + dateFrom + "' AND CONVERT(datetime, '" + dateTo + " 23:59:59.998') "
            + " AND ORL_STRNO IN (" + stores + ") "
            + " AND ORL_REGNO IN (" + registers + ") ";

        dtExport = dbHlpr.FetchData(qry);

        DxGridSqlDataSource1.SelectCommand = qry;

        string filename = "VS_DS_ORD_" + DateTime.Now.ToString("ddMMMyyyy$HH:mm:ss") + ".xls";

        var exportOptions = new XlsxExportOptionsEx();
        exportOptions.ShowGridLines = false;
        exportOptions.ExportType = DevExpress.Export.ExportType.WYSIWYG;
        //exportOptions.CustomizeCell += exportOptions_CustomizeCell;
        gvExportDataExporter.Styles.Cell.Wrap = DevExpress.Utils.DefaultBoolean.False;
        gvExportDataExporter.Styles.Default.Wrap = DevExpress.Utils.DefaultBoolean.False;
        gvExportDataExporter.Styles.Header.Wrap = DevExpress.Utils.DefaultBoolean.False;
        gvExportDataExporter.WriteXlsxToResponse(filename, exportOptions);
        //ExportToExcel(dtExport, filename);
        //ExportToExcelNormal(dtExport, filename);
        //ExportToExcelDevExpress(dtExport, filename);
    }

    protected void gvExportDataExporter_RenderBrick(object sender, ASPxGridViewExportRenderingEventArgs e)
    {
        e.BrickStyle.BorderWidth = 1;
        e.BrickStyle.BorderColor = Color.Black;

        if (e.RowType == GridViewRowType.Header)
        {
            e.BrickStyle.BackColor = Color.FromArgb(198, 89, 17);
            e.BrickStyle.ForeColor = Color.White;
            e.BrickStyle.Font = new Font("Segoe UI", 12, FontStyle.Bold);
        }
        else if (e.RowType == GridViewRowType.Data)
        {
            e.BrickStyle.Font = new Font("Segoe UI", 12, FontStyle.Regular);
            if (e.VisibleIndex % 2 == 0)
            {
                e.BrickStyle.BackColor = Color.FromArgb(221, 235, 247);
            }
            else
            {
                e.BrickStyle.BackColor = Color.White;
            }
        }
        else
        {
            e.BrickStyle.BorderWidth = 0;
            e.BrickStyle.BorderColor = Color.Transparent;
        }
    }

    /*protected void exportOptions_CustomizeCell(DevExpress.Export.CustomizeCellEventArgs ea)
    {
        if (ea.AreaType == DevExpress.Export.SheetAreaType.Header)
        {
            ea.Formatting.BackColor = Color.FromArgb(198, 89, 17);
            ea.Formatting.Font.Color = Color.White;
            ea.Formatting.Font.Name = "Segoe UI";
            ea.Formatting.Font.Size = 12;
            ea.Formatting.Font.Bold = true;
        }

        if (ea.AreaType == DevExpress.Export.SheetAreaType.DataArea)
        {
            ea.Formatting.Font.Name = "Segoe UI";
            ea.Formatting.Font.Size = 12;
            if (ea.DocumentRow % 2 == 0)
            {
                ea.Formatting.BackColor = Color.FromArgb(221, 235, 247);
            }
            else
            {
                ea.Formatting.BackColor = Color.White;
            }

            if (ea.ColumnFieldName == "Order Date" || ea.ColumnFieldName == "LOT")
            {
                try
                {
                    ea.Value = Convert.ToDateTime(ea.Value).ToString("dd-MMM-yyyy");
                    //ea.Formatting.FormatString = "dd-MMM-yyyy";
                }
                catch (Exception) { }
            }
            else if (ea.ColumnFieldName == "Qty")
            {
                ea.Formatting.FormatString = "n3";
                //ea.Formatting.NumberFormat = XlNumberFormat.NumberWithThousandSeparator2;
            }
            else if (ea.ColumnFieldName == "Price" || ea.ColumnFieldName == "Amount")
            {
                ea.Formatting.FormatString = "n2";
                // ea.Formatting.NumberFormat = XlNumberFormat.NumberWithThousandSeparator2;
            }
        }
        ea.Handled = true;
    }*/

    private void ExportToExcelDevExpress(DataTable dtExport, string filename)
    {
        System.IO.StringWriter tw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
        ASPxGridView dgGrid = new ASPxGridView();
        dgGrid.Theme = "Office2010Black";
        dgGrid.DataSource = dtExport;
        dgGrid.DataBind();

        ASPxGridViewExporter exporter = new ASPxGridViewExporter();
        exporter.GridViewID = dgGrid.ID;
        exporter.WriteXlsToResponse(filename);
    }

    private void ExportToExcelNormal(DataTable dtExport, string filename)
    {
        System.IO.StringWriter tw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
        DataGrid dgGrid = new DataGrid();
        dgGrid.DataSource = dtExport;
        dgGrid.DataBind();

        //Get the HTML for the control.
        dgGrid.RenderControl(hw);
        //Write the HTML back to the browser.
        //Response.ContentType = application/vnd.ms-excel;
        Response.ContentType = "application/vnd.ms-excel";
        Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "");
        this.EnableViewState = false;
        Response.Write(tw.ToString());
        Response.End();
    }

    protected void ExportToExcel(DataTable dtExport, string filename)
    {
        GridView GridView1 = new GridView();

        GridView1.RowStyle.BackColor = Color.White;
        GridView1.AlternatingRowStyle.BackColor = Color.White;

        GridView1.DataSource = dtExport;
        GridView1.DataBind();

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=" + filename);
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            //To Export all pages
            GridView1.AllowPaging = false;

            GridView1.HeaderRow.BackColor = Color.White;
            GridView1.HeaderRow.ForeColor = Color.White;

            foreach (TableCell cell in GridView1.HeaderRow.Cells)
            {
                cell.BackColor = Color.FromArgb(198, 89, 17);
                cell.ForeColor = Color.White;

                cell.CssClass = "header-cell";
            }

            foreach (GridViewRow row in GridView1.Rows)
            {
                row.BackColor = Color.White;
                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = Color.FromArgb(221, 235, 247);
                    }
                    else
                    {
                        cell.BackColor = GridView1.RowStyle.BackColor;
                    }
                    cell.CssClass = "body-cell";
                }
            }

            GridView1.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .header-cell { font-family: 'Segoe UI'; font-size: 12pt; font-style: bold; } .body-cell { font-family: 'Segoe UI'; font-size: 12pt; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    private string CommaSeperatedToQuery(string str)
    {
        string[] strArr = str.Split(',');
        string newStr = string.Empty;

        if (strArr.Length > 1)
        {
            for (int i = 0; i < strArr.Length; i++)
            {
                newStr += "'" + strArr[i] + "',";
            }
            newStr = newStr.TrimEnd(',');
            return newStr;
        }
        else
        {
            newStr = "'" + strArr[0] + "'";
            return newStr;
        }
    }

    protected void DtShtStrNo_Init(object sender, EventArgs e)
    {
        ASPxDropDownEdit ddStores = (ASPxDropDownEdit)sender;
        ASPxListBox lstBxStores = (ASPxListBox)ddStores.FindControl("lstBxStrItems");

        DataTable dtStores = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE WHERE STM_CODE IN (" + Session["userBranchCode"].ToString() + ") ");

        string tempStr = "";
        for (int i = 0; i < dtStores.Rows.Count; i++)
        {
            tempStr += dtStores.Rows[i]["STM_CODE"].ToString() + ",";
        }
        tempStr = tempStr.Trim(',');

        DataRow dr = dtStores.NewRow();
        dr["STM_NAME"] = "All";
        dr["STM_CODE"] = "0";
        dtStores.Rows.InsertAt(dr, 0);

        lstBxStores.DataSource = dtStores;
        lstBxStores.ValueField = "STM_CODE";
        lstBxStores.TextField = "STM_NAME";
        lstBxStores.DataBind();

        ddStores.Value = tempStr;
    }
    protected void DtShtRegNo_Init(object sender, EventArgs e)
    {
        ASPxDropDownEdit ddRegisters = (ASPxDropDownEdit)sender;
        ASPxListBox lstBxRegisters = (ASPxListBox)ddRegisters.FindControl("lstBxRegItems");

        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + Session["userBranchCode"].ToString() + ") ");

        string tempStr = "";
        for (int i = 0; i < dtRegisters.Rows.Count; i++)
        {
            tempStr += dtRegisters.Rows[i]["REG_CODE"].ToString() + ",";
        }
        tempStr = tempStr.Trim(',');

        DataRow dr = dtRegisters.NewRow();
        dr["REG_NAME"] = "All";
        dr["REG_CODE"] = "0";
        dtRegisters.Rows.InsertAt(dr, 0);

        lstBxRegisters.DataSource = dtRegisters;
        lstBxRegisters.ValueField = "REG_CODE";
        lstBxRegisters.TextField = "REG_NAME";
        lstBxRegisters.DataBind();

        ddRegisters.Value = tempStr;
    }

    protected void gvExportData_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        e.Cell.Height = Unit.Point(25);
    }
    protected void StoreChanged(object sender, EventArgs e)
    {
        ASPxListBox lstBxRegisters = (ASPxListBox)DtShtRegNo.FindControl("lstBxRegItems");

        string stores = "''";
        if (DtShtStrNo.Value != null)
        {
            stores = DtShtStrNo.Value.ToString();
        }

        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + stores + ") ");

        string tempStr = "";
        for (int i = 0; i < dtRegisters.Rows.Count; i++)
        {
            tempStr += dtRegisters.Rows[i]["REG_CODE"].ToString() + ",";
        }
        tempStr = tempStr.Trim(',');

        DataRow dr = dtRegisters.NewRow();
        dr["REG_NAME"] = "All";
        dr["REG_CODE"] = "0";
        dtRegisters.Rows.InsertAt(dr, 0);

        lstBxRegisters.DataSource = dtRegisters;
        lstBxRegisters.ValueField = "REG_CODE";
        lstBxRegisters.TextField = "REG_NAME";
        lstBxRegisters.DataBind();

        DtShtRegNo.Value = tempStr;
    }
}