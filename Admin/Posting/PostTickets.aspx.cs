﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;
using DevExpress.Web;
using System.Drawing;
using DevExpress.XtraPrinting;
using DevExpress.Export.Xl;

public partial class Admin_Posting_PostTickets : System.Web.UI.Page
{
    DatabaseHelperClass dbHlpr = new DatabaseHelperClass();

    DataTable dtHdrs = new DataTable();
    DataTable dtLins = new DataTable();

    protected void Page_Init(object sender, EventArgs e)
    {
        // DtShtDateFrom.Value = new DateTime(1900, 1, 1);
        // DtShtDateTo.Value = new DateTime(9999, 12, 31);
    }

    protected void btnPostTickets_Click(object sender, EventArgs e)
    {
        LoadTicketsBasedOnSelection();

        DateTime dtNow = DateTime.Now;

        int headersRemoved = 0;
        int linesRemoved = 0;
        
        for (int i = 0; i < dtHdrs.Rows.Count; i++)
        {
            ASPxLabel3.Text = DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss");
            ///////////////////insert code for Header data into History table //////////////////////////////////
            headersRemoved += dbHlpr.ExecuteNonQuery("INSERT INTO SLS_TKH_HIST ( "
                + " TKH_HKEY, TKH_TKTNO, TKH_STRNO, TKH_REGNO, TKH_DRWNO, TKH_CUSNO, TKH_CUSNM, "
                + " TKH_CUSVT, TKH_SREP, TKH_DATE, TKH_TIME, TKH_CRTDT, TKH_CRTIM, "
                + " TKH_CRTBY, TKH_SLAMT, TKH_DSCNT, TKH_NTAMT, TKH_VTAMT, TKH_AMT, "
                + " TKH_AMTPD, TKH_PAYCD, TKH_TTYPE, TKH_CHNGE, TKH_BLANC, TKH_GEOLA, TKH_GEOLN, "
                + " TKH_PSTBY, TKH_PSTDT, TKH_PSTIM "
                + " ) SELECT "
                + " TKH_HKEY, TKH_TKTNO, TKH_STRNO, TKH_REGNO, TKH_DRWNO, TKH_CUSNO, TKH_CUSNM, "
                + " TKH_CUSVT, TKH_SREP, TKH_DATE, TKH_TIME, TKH_CRTDT, TKH_CRTIM, "
                + " TKH_CRTBY, TKH_SLAMT, TKH_DSCNT, TKH_NTAMT, TKH_VTAMT, TKH_AMT, "
                + " TKH_AMTPD, TKH_PAYCD, TKH_TTYPE, TKH_CHNGE, TKH_BLANC, TKH_GEOLA, TKH_GEOLN, "
                + " '" + Session["UserId"] + "', '" + dtNow.ToString() + "', '" + dtNow.ToString() + "' "
                + " FROM SLS_TRX_THDR "
                + " WHERE SLS_TRX_THDR.TKH_HKEY = '" + dtHdrs.Rows[i]["TKH_HKEY"].ToString() + "' ");


            ///////////////////Delete code for Header data from master table //////////////////////////////////
            dbHlpr.ExecuteNonQuery("DELETE FROM SLS_TRX_THDR WHERE TKH_HKEY = '" + dtHdrs.Rows[i]["TKH_HKEY"].ToString() + "' ");
            hdrCounter.Value = headersRemoved.ToString();

            ///////////////////insert code for Line data into History table //////////////////////////////////
            linesRemoved += dbHlpr.ExecuteNonQuery("INSERT INTO SLS_TKL_HIST ( "
                + " TKL_LKEY, TKL_HKEY, TKL_SEQNO, TKL_TKTNO, TKL_STRNO, TKL_REGNO, TKL_DRWNO, TKL_ITMNO, TKL_DESC, "
                + " TKL_UOM, TKL_QTY, TKL_ITMPR, TKL_PRICE, TKL_EXPRC, TKL_TAXCD, "
                + " TKL_DSCNT, TKL_TXFAC, TKL_VATPRC, TKL_CUSNO, TKL_CUSNM, TKL_SREP, "
                + " TKL_DATE, TKL_TIME, TKL_CRTDT, TKL_CRTIM, TKL_CRTBY, TKL_TTYPE, "
                + " TKL_TEXMT, TKL_EXPDT, TKL_RSNCD, TKL_PAYCD, TKL_GEOLA, TKL_GEOLN, "
                + " TKL_ISFREE, TKL_PRCOF, TKL_PCRSN, "
                + " TKL_PSTBY, TKL_PSTDT, TKL_PSTIM "
                + " ) SELECT "

                + " TKL_LKEY, TKL_HKEY, TKL_SEQNO, TKL_TKTNO, TKL_STRNO, TKL_REGNO, TKL_DRWNO, TKL_ITMNO, TKL_DESC, "
                + " TKL_UOM, TKL_QTY, TKL_ITMPR, TKL_PRICE, TKL_EXPRC, TKL_TAXCD, "
                + " TKL_DSCNT, TKL_TXFAC, TKL_VATPRC, TKL_CUSNO, TKL_CUSNM, TKL_SREP, "
                + " TKL_DATE, TKL_TIME, TKL_CRTDT, TKL_CRTIM, TKL_CRTBY, TKL_TTYPE, "
                + " TKL_TEXMT, TKL_EXPDT, TKL_RSNCD, TKL_PAYCD, TKL_GEOLA, TKL_GEOLN, "
                + " TKL_ISFREE, TKL_PRCOF, TKL_PCRSN, "
                + " '" + Session["UserId"] + "', '" + dtNow.ToString() + "', '" + dtNow.ToString() + "' "
                + " FROM SLS_TRX_TLIN "
                + " WHERE SLS_TRX_TLIN.TKL_HKEY = '" + dtHdrs.Rows[i]["TKH_HKEY"].ToString() + "' ");

            ///////////////////Delete code for Line data from master table //////////////////////////////////
            dbHlpr.ExecuteNonQuery("DELETE FROM SLS_TRX_TLIN WHERE TKL_HKEY = '" + dtHdrs.Rows[i]["TKH_HKEY"].ToString() + "' ");
            linCounter.Value = linesRemoved.ToString();
        }

    }

    private string CommaSeperatedToQuery(string str)
    {
        string[] strArr = str.Split(',');
        string newStr = string.Empty;

        if (strArr.Length > 1)
        {
            for (int i = 0; i < strArr.Length; i++)
            {
                newStr += "'" + strArr[i] + "',";
            }
            newStr = newStr.TrimEnd(',');
            return newStr;
        }
        else
        {
            newStr = "'" + strArr[0] + "'";
            return newStr;
        }
    }

    protected void DtShtStrNo_Init(object sender, EventArgs e)
    {
        ASPxDropDownEdit ddStores = (ASPxDropDownEdit)sender;
        ASPxListBox lstBxStores = (ASPxListBox)ddStores.FindControl("lstBxStrItems");

        DataTable dtStores = dbHlpr.FetchData("SELECT * FROM STP_MSTR_STORE WHERE STM_CODE IN (" + Session["userBranchCode"].ToString() + ") ");

        string tempStr = "";
        for (int i = 0; i < dtStores.Rows.Count; i++)
        {
            //tempStr += dtStores.Rows[i]["STM_CODE"].ToString() + ",";
        }
        tempStr = tempStr.Trim(',');

        DataRow dr = dtStores.NewRow();
        dr["STM_NAME"] = "All";
        dr["STM_CODE"] = "0";
        dtStores.Rows.InsertAt(dr, 0);

        lstBxStores.DataSource = dtStores;
        lstBxStores.ValueField = "STM_CODE";
        lstBxStores.TextField = "STM_NAME";
        lstBxStores.DataBind();

        ddStores.Value = tempStr;
    }
    protected void DtShtRegNo_Init(object sender, EventArgs e)
    {
        ASPxDropDownEdit ddRegisters = (ASPxDropDownEdit)sender;
        ASPxListBox lstBxRegisters = (ASPxListBox)ddRegisters.FindControl("lstBxRegItems");

        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + Session["userBranchCode"].ToString() + ") ");

        string tempStr = "";
        for (int i = 0; i < dtRegisters.Rows.Count; i++)
        {
            //tempStr += dtRegisters.Rows[i]["REG_CODE"].ToString() + ",";
        }
        tempStr = tempStr.Trim(',');

        DataRow dr = dtRegisters.NewRow();
        dr["REG_NAME"] = "All";
        dr["REG_CODE"] = "0";
        dtRegisters.Rows.InsertAt(dr, 0);

        lstBxRegisters.DataSource = dtRegisters;
        lstBxRegisters.ValueField = "REG_CODE";
        lstBxRegisters.TextField = "REG_NAME";
        lstBxRegisters.DataBind();

        ddRegisters.Value = tempStr;
    }
    protected void UpdateLabels(object sender, EventArgs e)
    {
        lblErrorMessage.Text = DateTime.Now.ToString();

        LoadRegistersByStore();

        LoadTicketsBasedOnSelection();

        string msgToShow = "";
        msgToShow += " No. of Tickets to Post : " + dtHdrs.Rows.Count;
        msgToShow += " \nNo. of Lines to Post : " + dtLins.Rows.Count;

        lblErrorMessage.Text = msgToShow;
    }

    private void LoadRegistersByStore()
    {
        ASPxListBox lstBxRegisters = (ASPxListBox)DtShtRegNo.FindControl("lstBxRegItems");

        string stores = "''";
        if (DtShtStrNo.Value != null)
        {
            stores = DtShtStrNo.Value.ToString();
        }

        DataTable dtRegisters = dbHlpr.FetchData("SELECT * FROM STP_MSTR_RGSTR WHERE REG_STRNO IN (" + stores + ") ");

        string tempStr = "";
        for (int i = 0; i < dtRegisters.Rows.Count; i++)
        {
            tempStr += dtRegisters.Rows[i]["REG_CODE"].ToString() + ",";
        }
        tempStr = tempStr.Trim(',');

        DataRow dr = dtRegisters.NewRow();
        dr["REG_NAME"] = "All";
        dr["REG_CODE"] = "0";
        dtRegisters.Rows.InsertAt(dr, 0);

        lstBxRegisters.DataSource = dtRegisters;
        lstBxRegisters.ValueField = "REG_CODE";
        lstBxRegisters.TextField = "REG_NAME";
        lstBxRegisters.DataBind();

        DtShtRegNo.Value = tempStr;
    }

    private void LoadTicketsBasedOnSelection()
    {
        string dateFrom = DtShtDateFrom.Value != null ? ((DateTime)DtShtDateFrom.Value).ToString("yyyy-MM-dd") : new DateTime(1900, 1, 1).ToString("yyyy-MM-dd");
        string dateTo = DtShtDateTo.Value != null ? ((DateTime)DtShtDateTo.Value).ToString("yyyy-MM-dd") : new DateTime(9999, 12, 31).ToString("yyyy-MM-dd");

        string stores = CommaSeperatedToQuery(DtShtStrNo.Value != null ? DtShtStrNo.Value.ToString() : "");
        string registers = CommaSeperatedToQuery(DtShtRegNo.Value != null ? DtShtRegNo.Value.ToString() : "");
        
        string qryHdr = "SELECT * FROM SLS_TRX_THDR "
            + " WHERE "
            + " TKH_DATE BETWEEN '" + dateFrom + "' AND CONVERT(datetime, '" + dateTo + " 23:59:59.998') "
            + " AND TKH_STRNO IN (" + stores + ") "
            + " AND TKH_REGNO IN (" + registers + ") ";

        string qryLin = "SELECT * FROM SLS_TRX_TLIN "
            + " WHERE "
            + " TKL_DATE BETWEEN '" + dateFrom + "' AND CONVERT(datetime, '" + dateTo + " 23:59:59.998') "
            + " AND TKL_STRNO IN (" + stores + ") "
            + " AND TKL_REGNO IN (" + registers + ") ";

        dtHdrs = dbHlpr.FetchData(qryHdr);
        dtLins = dbHlpr.FetchData(qryLin);
    }
    protected void timer1_Tick(object sender, EventArgs e)
    {
        ASPxLabel1.Text = hdrCounter.Value.ToString();
        ASPxLabel2.Text = linCounter.Value.ToString();

        ASPxLabel3.Text = DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss");
    }
}