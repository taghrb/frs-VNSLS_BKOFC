﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AuthMasterPage.master" AutoEventWireup="true" CodeFile="404.aspx.cs" Inherits="ErrorPages_404" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceholderMaster" runat="Server">
    <style>
        .notfound_wrapper img {
            display: block;
            margin: 0 auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="wrapper notfound_wrapper" style="padding-top: 65px; text-align: center; height: 750px; background: none !important; background-color: #d3d3d3 !important;">
        <h1>Under Development!!!</h1>
        <img src="~/img/under-construction.png" runat="server" />
        
        <%--<img src="~/img/pageunderconstruction.png" runat="server" />--%>
    </div>
</asp:Content>

