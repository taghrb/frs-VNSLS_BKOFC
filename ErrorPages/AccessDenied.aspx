﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/AuthMasterPage.master" AutoEventWireup="true" CodeFile="AccessDenied.aspx.cs" Inherits="ErrorPages_404" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContentPlaceholderMaster" runat="Server">
    <style>
        .notfound_wrapper img {
            display: block;
            margin: 0 auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="wrapper notfound_wrapper" style="padding-top: 65px; text-align: center; height: 750px; background: none !important; background-color: #d3d3d3 !important;">

        <img src="../img/icons/cancel.png" runat="server" style="width: 5%;" />
        <br />
        <h1 style="display: inline-block;">Access Denied!!!</h1>

    </div>
</asp:Content>

